/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "secretprusa.h"
#include <NetworkManagerQt/Connection>
#include <NetworkManagerQt/ConnectionSettings>
#include <NetworkManagerQt/Settings>
#include <NetworkManagerQt/WirelessSecuritySetting>
#include <NetworkManagerQt/WirelessSetting>
#include <QDBusMessage>
#include <QDBusConnection>
#include <QDebug>


void SecretPrusa::respond(QString psk)
{
    if(m_Request_pending) {
        NetworkManager::ConnectionSettings::Ptr connectionSettings = NetworkManager::ConnectionSettings::Ptr(new NetworkManager::ConnectionSettings(m_Connection));
        NetworkManager::WirelessSecuritySetting::Ptr wifiSecurity = connectionSettings->setting(NetworkManager::Setting::WirelessSecurity).dynamicCast<NetworkManager::WirelessSecuritySetting>();
        NetworkManager::WirelessSetting::Ptr wifiSetting = connectionSettings->setting(NetworkManager::Setting::Wireless).dynamicCast<NetworkManager::WirelessSetting>();
        wifiSecurity->setPsk(psk);
        wifiSetting->setInitialized(true);
        QDBusMessage reply;
        reply = m_Message.createReply(QVariant::fromValue(connectionSettings->toMap()));
        if (!QDBusConnection::systemBus().send(reply)) {
            qWarning() << "Failed put the secret into the queue";
        }
        setProperty("request_pending", false);
    }
    else {
        qWarning() << "Attempt to respond when no request is pending";
    }
}

void SecretPrusa::cancel()
{
    // Send an empty response -> no secret to give
    if(m_Request_pending) {
        setProperty("request_pending", false);

    }
    QDBusMessage reply;
    reply = m_Message.createReply(QVariant::fromValue(NMVariantMapMap()));
    if (!QDBusConnection::systemBus().send(reply)) {
        qWarning() << "Failed put the secret into the queue";
    }
}

SecretPrusa::SecretPrusa(QObject *parent) : NetworkManager::SecretAgent("org.freedesktop.NetworkManager.SecretAgent", parent)
{
    m_Request_pending = false;
    m_Connection_name = "";
}

NMVariantMapMap SecretPrusa::GetSecrets(const NMVariantMapMap & connection,
                                        const QDBusObjectPath & connection_path,
                                        const QString & setting_name,
                                        const QStringList & hints,
                                        uint flags)
{

    if(setting_name == "hotspot") {
        qDebug() << "Refusing password request of the local hotspot";
        return NMVariantMapMap();
    }

    if(request_pending()) { return NMVariantMapMap(); } // Return empty - only one request at a time
    this->m_Message = message();
    setDelayedReply(true); // Will be responded to when a PSK is known

    qDebug() << "SecretPrusa::GetSecrets";

    // Make backup
    m_Connection = connection;
    m_Connection_path = connection_path;
    m_Setting_name = setting_name;
    m_Hints = hints;
    m_Flags = flags;


    NetworkManager::ConnectionSettings::Ptr connectionSettings = NetworkManager::ConnectionSettings::Ptr(new NetworkManager::ConnectionSettings(connection));
    NetworkManager::WirelessSetting::Ptr wifiSetting = connectionSettings->setting(NetworkManager::Setting::Wireless).dynamicCast<NetworkManager::WirelessSetting>();



    setProperty("connection_name", wifiSetting->ssid());
    //m_Connection_name = wifiSetting->ssid();
    //emit connection_nameChanged();
    setProperty("request_pending", true);
    return connectionSettings->toMap();
}

// The rest is not needed at this time - left empty

void SecretPrusa::SaveSecrets(const NMVariantMapMap &connection, const QDBusObjectPath &connection_path)
{
    Q_UNUSED(connection)
    Q_UNUSED(connection_path)
    //qDebug() << "SecretPrusa::SaveSecrets - not implemented(not needed)";
}

void SecretPrusa::DeleteSecrets(const NMVariantMapMap &connection, const QDBusObjectPath &connection_path)
{
    Q_UNUSED(connection)
    Q_UNUSED(connection_path)
    //qDebug() << "SecretPrusa::DeleteSecrets - not implemented(not needed)";
}

void SecretPrusa::CancelGetSecrets(const QDBusObjectPath &connection_path, const QString &setting_name)
{
    Q_UNUSED(setting_name)
    Q_UNUSED(connection_path)
    if(request_pending()) cancel();
    emit canceled_by_networkManager();
}
