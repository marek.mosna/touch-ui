/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "wifi.h"
#include <NetworkManagerQt/Settings>
#include <NetworkManagerQt/Utils>
#include <NetworkManagerQt/Ipv6Setting>
#include <QDebug>

#include <QJSEngine>
#include <QJsonArray>
#include <exception>

Wifi::Wifi(QQmlApplicationEngine *_enginePtr, QObject *parent) : QObject(parent), enginePtr(_enginePtr)
{

    NetworkManager::ConnectionSettings::Ptr settings(new NetworkManager::ConnectionSettings(NetworkManager::ConnectionSettings::Wireless));
    NetworkManager::Device::List deviceList = NetworkManager::networkInterfaces();
    NetworkManager::WirelessDevice::Ptr wifiDevice(nullptr);



    // Assuming exactly one Wi-Fi device
    Q_FOREACH (NetworkManager::Device::Ptr dev, deviceList) {
        if (dev->type() == NetworkManager::Device::Wifi) {
            wifiDevice = qobject_cast<NetworkManager::WirelessDevice *>(dev);
            break;
        }
    }
    if (!wifiDevice) {
        qWarning() << "No wireless device detected, there should be at least one !";
        return;
    }
    m_WifiDevice = wifiDevice;
    connect(wifiDevice.data(), &NetworkManager::WirelessDevice::hardwareAddressChanged, this, [=](QString addr) {emit hardwareAddressChanged(addr); });
}

/**
    @param method: NetworkManager::Ipv4Setting::enum ConfigMethod {Automatic, LinkLocal, Manual, Shared, Disabled};
*/
bool Wifi::addConnectionAuto(QString ssid,
                   QString psk, bool hidden, bool connect, QJSValue callbackSuccess, QJSValue callbackFailure)

{
    if(! m_WifiDevice) return false;
    Q_ASSERT_X(callbackSuccess.isNull() || callbackSuccess.isUndefined() || callbackSuccess.isCallable(), "connectToNetwork", "Invalid value for doneCallback");
    Q_ASSERT_X(callbackFailure.isNull() || callbackFailure.isUndefined() || callbackFailure.isCallable(), "connectToNetwork", "Invalid value for callbackFailure");
    auto wifiDevice = m_WifiDevice;
    // If the connection of that name already exists, modify it, otherwise create a new one
    auto connections = findConnectionsForSssidPtr(ssid);
    NetworkManager::ConnectionSettings::Ptr backup;
    if(connections.length() > 1) {
        qWarning() << "Found multiple("<<connections.length() <<") connections with the same ssid: " << ssid;

    }
    if(connections.length() > 0) {
        backup = connections.at(0)->settings();
        bool oldHidden = backup->setting(NetworkManager::Setting::Wireless).dynamicCast<NetworkManager::WirelessSetting>()->hidden();
        if(oldHidden) {
            hidden = oldHidden;
            qDebug() << "addConnectionAuto: Keeping old hidden flag";
        }
    }
    this->remove_connections(NetworkManager::WirelessSetting::Infrastructure, ssid);

    NetworkManager::ConnectionSettings::Ptr settings(new NetworkManager::ConnectionSettings(NetworkManager::ConnectionSettings::Wireless));
    settings->setId(ssid);
    QString uuid = QUuid::createUuid().toString();
    QString modifiedUuid = uuid.mid(1, uuid.length() - 2);
    settings->setUuid(uuid.mid(1, uuid.length() - 2));
    settings->setConnectionType(NetworkManager::ConnectionSettings::Wireless);


    NetworkManager::WirelessSetting::Ptr wirelessSetting = settings->setting(NetworkManager::Setting::Wireless).dynamicCast<NetworkManager::WirelessSetting>();
    wirelessSetting->setSsid(ssid.toUtf8());
    wirelessSetting->setSecurity("802-11-wireless-security");
    wirelessSetting->setHidden(hidden);
    wirelessSetting->setInitialized(true);

    NetworkManager::Ipv4Setting::Ptr ipv4Setting = settings->setting(NetworkManager::Setting::Ipv4).dynamicCast<NetworkManager::Ipv4Setting>();
    ipv4Setting->setMethod(NetworkManager::Ipv4Setting::Automatic);
    ipv4Setting->setInitialized(true);

    // Optional password setting.
    NetworkManager::WirelessSecuritySetting::Ptr wifiSecurity = settings->setting(NetworkManager::Setting::WirelessSecurity).dynamicCast<NetworkManager::WirelessSecuritySetting>();
    wifiSecurity->setKeyMgmt(NetworkManager::WirelessSecuritySetting::WpaPsk);
    wifiSecurity->setPsk(psk);
    wifiSecurity->setGroup({});
    wifiSecurity->setAuthAlg(NetworkManager::WirelessSecuritySetting::Open);
    wifiSecurity->setInitialized(true);


    // Create new connection and if needed, activate it
    QDBusPendingReply <QDBusObjectPath> reply = NetworkManager::addConnection(settings->toMap());
    QDBusPendingCallWatcher *watcher = new QDBusPendingCallWatcher(reply, this);
    QObject::connect(watcher, &QDBusPendingCallWatcher::finished,
                     this, [=](QDBusPendingCallWatcher *watcher) mutable {
        QDBusPendingReply<QDBusObjectPath> reply = *watcher;
        if (reply.isError()) {
            qDebug() << "addConnectionAuto: connection creation error: " << QDBusError::errorString( reply.error().type());
            if(backup) {
                qDebug() << "Recreating the original connection: ssid=" << ssid << " parameters: " << backup->toMap();
                // Try to recreate the original connection
                QDBusPendingReply <QDBusObjectPath> reply = NetworkManager::addConnection(backup->toMap());
                QDBusPendingCallWatcher *watcher = new QDBusPendingCallWatcher(reply, this);
                QObject::connect(watcher, &QDBusPendingCallWatcher::finished,
                                 this, [=](QDBusPendingCallWatcher *watcher) mutable {
                    QDBusPendingReply<QDBusObjectPath> reply = *watcher;
                    emit connectionsChanged();
                    watcher->deleteLater();
                });
            }
            else {
                emit connectionsChanged();
            }
            if(callbackFailure.isCallable()) callbackFailure.call({reply.error().message()});
        } else { // Creating connection succeeded
            qDebug() << "addConnectionAuto: connection created: " << reply.argumentAt<0>().path() ;
            if(! connect) {
                emit connectionsChanged();
                if(callbackSuccess.isCallable()) callbackSuccess.call({reply.argumentAt<0>().path()});
            }
            else {
                //emit connectionsChanged();
                QString apPath = "/";
                auto network = m_WifiDevice->findNetwork(ssid);
                if(network) {
                    auto ap = network->referenceAccessPoint();
                    if(ap) {
                        apPath = ap->uni();
                    }
                }
                //auto connection = NetworkManager::findConnection(reply.argumentAt<0>().path());
                qDebug() << reply.argumentAt<0>().path();
                // Now try to connect
                QDBusPendingReply<> r2 = NetworkManager::activateConnection(reply.argumentAt<0>().path(), m_WifiDevice->uni(), apPath);
                QDBusPendingCallWatcher *watcher2 = new QDBusPendingCallWatcher(r2, this);
                QObject::connect(watcher2, &QDBusPendingCallWatcher::finished,
                                 this, [=](QDBusPendingCallWatcher* watcher) mutable {

                    QDBusPendingReply<> reply = *watcher;
                    emit connectionsChanged();
                    if (reply.isError()) {
                        qDebug() << "addConnectionAuto: connection activation: " << ssid <<" failed: " << reply.error().message();
                        if(callbackFailure.isCallable()) callbackFailure.call({reply.error().message()});
                    } else {
                        qDebug() << "addConnectionAuto: connection: " << ssid <<" activated";
                        if(callbackSuccess.isCallable()) {
                            callbackSuccess.call({});
                        }
                    }
                    watcher->deleteLater();
                } );
            }

        }
        watcher->deleteLater();
    } );

    //emit connectionsChanged();
    return true;
}


// FIXME: if I don't want to connect immediately, network and AP should not be required to exist
bool Wifi::addConnectionManual(QString ssid, QString psk, QString ip, QString mask, QString gateway, QString dns, bool hidden, bool connect, QJSValue callbackSuccess, QJSValue callbackFailure)
{
    if(! m_WifiDevice) return false;
    Q_ASSERT_X(callbackSuccess.isNull() || callbackSuccess.isUndefined() || callbackSuccess.isCallable(), "connectToNetwork", "Invalid value for callbackSuccess");
    Q_ASSERT_X(callbackFailure.isNull() || callbackFailure.isUndefined() || callbackFailure.isCallable(), "connectToNetwork", "Invalid value for callbackFailure");
    // If the connection of that name already exists, modify it, otherwise create a new one
    auto connections = findConnectionsForSssidPtr(ssid);
    NetworkManager::ConnectionSettings::Ptr backup;
    if(connections.length() > 1) {
        qWarning() << "addConnectionManual: found multiple("<<connections.length() <<") connections with the same ssid: " << ssid;
    }
    if(connections.length() > 0) {
        backup = connections.at(0)->settings();
        bool oldHidden = backup->setting(NetworkManager::Setting::Wireless).dynamicCast<NetworkManager::WirelessSetting>()->hidden();
        if(oldHidden) {
            hidden = oldHidden;
            qDebug() << "addConnectionAuto: Keeping old hidden flag";
        }
    }
    this->remove_connections(NetworkManager::WirelessSetting::Infrastructure, ssid);
    NetworkManager::ConnectionSettings::Ptr settings(new NetworkManager::ConnectionSettings(NetworkManager::ConnectionSettings::Wireless));
    NetworkManager::WirelessDevice::Ptr wifiDevice = m_WifiDevice;

    NetworkManager::IpAddresses ipList;
    NetworkManager::IpAddress ip1;
    ip1.setGateway(QHostAddress(gateway));
    ip1.setIp(QHostAddress(ip));
    ip1.setNetmask(QHostAddress(mask));
    ipList.append(ip1);

    QList<QHostAddress> dnsList;
    dnsList.append(QHostAddress(dns));


    auto network = m_WifiDevice->findNetwork(ssid);
    if(!network) {
        qWarning() << "Network " << ssid << " not found!";
        return false;
    }
    auto accessPoint = network->referenceAccessPoint();;
    if(!accessPoint) {
        qWarning() << "Network " << ssid << " has no accesspoint! ";
        return false;
    }

    settings->setId(ssid);
    QString uuid = QUuid::createUuid().toString();
    QString modifiedUuid = uuid.mid(1, uuid.length() - 2);
    settings->setUuid(uuid.mid(1, uuid.length() - 2));
    settings->setConnectionType(NetworkManager::ConnectionSettings::Wireless);

    NetworkManager::WirelessSetting::Ptr wirelessSetting = settings->setting(NetworkManager::Setting::Wireless).dynamicCast<NetworkManager::WirelessSetting>();
    wirelessSetting->setSsid(ssid.toUtf8());
    wirelessSetting->setSecurity("802-11-wireless-security");
    wirelessSetting->setHidden(hidden);
    wirelessSetting->setInitialized(true);

    NetworkManager::Ipv4Setting::Ptr ipv4Setting = settings->setting(NetworkManager::Setting::Ipv4).dynamicCast<NetworkManager::Ipv4Setting>();
    ipv4Setting->setMethod(NetworkManager::Ipv4Setting::Manual);
    ipv4Setting->setGateway(gateway);
    ipv4Setting->setAddresses(ipList);
    ipv4Setting->setDns(dnsList);
    ipv4Setting->setInitialized(true);


    // Optional password setting.
    NetworkManager::WirelessSecuritySetting::Ptr wifiSecurity = settings->setting(NetworkManager::Setting::WirelessSecurity).dynamicCast<NetworkManager::WirelessSecuritySetting>();

    // TODO: variable security settings
    wifiSecurity->setKeyMgmt(NetworkManager::WirelessSecuritySetting::WpaPsk);
    wifiSecurity->setPsk(psk);
    wifiSecurity->setInitialized(true);



    // Create new connection and if needed, activate it
    QDBusPendingReply <QDBusObjectPath> reply = NetworkManager::addConnection(settings->toMap());
    QDBusPendingCallWatcher *watcher = new QDBusPendingCallWatcher(reply, this);
    QObject::connect(watcher, &QDBusPendingCallWatcher::finished,
                     this, [=](QDBusPendingCallWatcher *watcher) mutable {
        QDBusPendingReply<QDBusObjectPath> reply = *watcher;
        if (reply.isError()) {
            qDebug() << "addConnectionManual: connection creation error: " << QDBusError::errorString( reply.error().type());
            qWarning() << "here it should recreate the connection";
            if(backup) {
                qDebug() << "Recreating the original connection: ssid=" << ssid << " parameters: " << backup->toMap();
                // Try to recreate the original connection
                QDBusPendingReply <QDBusObjectPath> reply = NetworkManager::addConnection(backup->toMap());
                QDBusPendingCallWatcher *watcher = new QDBusPendingCallWatcher(reply, this);
                QObject::connect(watcher, &QDBusPendingCallWatcher::finished,
                                 this, [=](QDBusPendingCallWatcher *watcher) mutable {
                    QDBusPendingReply<QDBusObjectPath> reply = *watcher;
                    qWarning() << "Recreating the connection " << ssid << " and emiting signal";
                    emit connectionsChanged();
                    watcher->deleteLater();
                });
            }
            else {
                emit connectionsChanged();
            }
            if(callbackFailure.isCallable()) callbackFailure.call({reply.error().message()});
        } else { // Creating connection succeeded
            qDebug() << "addConnectionManual: connection created: " << reply.argumentAt<0>().path() ;
            if(! connect) {
                emit connectionsChanged();
                if(callbackSuccess.isCallable()) callbackSuccess.call({reply.argumentAt<0>().path()});
            }
            else {
                //emit connectionsChanged();
                QString apPath = "/";
                auto network = m_WifiDevice->findNetwork(ssid);
                if(network) {
                    auto ap = network->referenceAccessPoint();
                    if(ap) {
                        apPath = ap->uni();
                    }
                }
                //auto connection = NetworkManager::findConnection(reply.argumentAt<0>().path());
                qDebug() << reply.argumentAt<0>().path();
                // Now try to connect
                QDBusPendingReply<> r2 = NetworkManager::activateConnection(reply.argumentAt<0>().path(), m_WifiDevice->uni(), apPath);
                QDBusPendingCallWatcher *watcher2 = new QDBusPendingCallWatcher(r2, this);
                QObject::connect(watcher2, &QDBusPendingCallWatcher::finished,
                                 this, [=](QDBusPendingCallWatcher* watcher) mutable {

                    QDBusPendingReply<> reply = *watcher;
                    emit connectionsChanged();
                    if (reply.isError()) {
                        qDebug() << "addConnectionManual: connection activation: " << ssid <<" failed: " << reply.error().message();
                        if(callbackFailure.isCallable()) callbackFailure.call({reply.error().message()});
                    } else {
                        qDebug() << "addConnectionManual: connection: " << ssid <<" activated";
                        if(callbackSuccess.isCallable()) {
                            callbackSuccess.call({});
                        }
                    }
                    watcher->deleteLater();
                } );
            }

        }
        watcher->deleteLater();
    } );
    return true;

}


bool Wifi::addConnectionAP(QString ssid,
                   QString psk, bool hidden, bool connect)

{

    Q_UNUSED(hidden)
    Q_UNUSED(connect)

    if(! m_WifiDevice) return false;

    // Allow only one hotspot connection to exist at a time
    this->remove_connections(NetworkManager::WirelessSetting::Ap);

    NetworkManager::ConnectionSettings::Ptr settings(new NetworkManager::ConnectionSettings(NetworkManager::ConnectionSettings::Wireless));
    NetworkManager::Device::List deviceList = NetworkManager::networkInterfaces();
    NetworkManager::WirelessDevice::Ptr wifiDevice;


    // Assuming exactly one Wi-Fi device
    Q_FOREACH (NetworkManager::Device::Ptr dev, deviceList) {
        if (dev->type() == NetworkManager::Device::Wifi) {
            wifiDevice = qobject_cast<NetworkManager::WirelessDevice *>(dev);
            break;
        }
    }
    if (!wifiDevice) {
        qDebug() << "No wifi device found";
        return false;
    }

    settings->setId("hotspot");
    //settings->setConnectionType(NetworkManager::ConnectionSettings::Wireless);
    settings->setConnectionType(NetworkManager::ConnectionSettings::typeFromString("802-11-wireless"));
    QString uuid = QUuid::createUuid().toString();
    settings->setUuid(uuid.mid(1, uuid.length() - 2));
    settings->setAutoconnect(false);
    settings->setAutoconnectRetries(0);


    qDebug() << "conSettings " << settings->toMap();

    NetworkManager::WirelessSetting::Ptr wirelessSetting = settings->setting(NetworkManager::Setting::Wireless).dynamicCast<NetworkManager::WirelessSetting>();
    wirelessSetting->setMode(NetworkManager::WirelessSetting::Ap);
    wirelessSetting->setSsid(ssid.toUtf8());
    wirelessSetting->setBand(NetworkManager::WirelessSetting::Bg);
    wirelessSetting->setType(NetworkManager::WirelessSetting::Wireless);
    wirelessSetting->setInitialized(true);
    qDebug() << "wifiSettings " << wirelessSetting->toMap();

    NetworkManager::Ipv4Setting::Ptr ipv4Setting = settings->setting(NetworkManager::Setting::Ipv4).dynamicCast<NetworkManager::Ipv4Setting>();
    NetworkManager::Ipv6Setting::Ptr ipv6Setting = settings->setting(NetworkManager::Setting::Ipv6).dynamicCast<NetworkManager::Ipv6Setting>();
    ipv4Setting->setMethod(NetworkManager::Ipv4Setting::Shared);
    ipv6Setting->setMethod(NetworkManager::Ipv6Setting::Automatic);
    ipv4Setting->setInitialized(true);
    qDebug() << "ipv4Settings " << ipv4Setting->toMap();

    // TODO: if manual, set IP addresses here

    // Optional password setting.
    NetworkManager::WirelessSecuritySetting::Ptr wifiSecurity = settings->setting(NetworkManager::Setting::WirelessSecurity).dynamicCast<NetworkManager::WirelessSecuritySetting>();
    wifiSecurity->setKeyMgmt(NetworkManager::WirelessSecuritySetting::WpaPsk);
    wifiSecurity->setPsk(psk);
    wifiSecurity->setProto(QList<NetworkManager::WirelessSecuritySetting::WpaProtocolVersion>({NetworkManager::WirelessSecuritySetting::Rsn}));
    wifiSecurity->setInitialized(true);

    //qDebug() << "Working with: " << settings->toMap();

    // We try to add and activate our new wireless connection
    QDBusPendingReply <QDBusObjectPath, QDBusObjectPath > reply = NetworkManager::addAndActivateConnection(settings->toMap(), wifiDevice->uni(), "/");

    reply.waitForFinished();

    // Check if this connection was added successfuly
        if (reply.isValid()) {
//            // Now our connection should be added in NetworkManager and we can print all settings pre-filled from NetworkManager
//            NetworkManager::Connection connection(reply.value().path());
//            NetworkManager::ConnectionSettings::Ptr newSettings = connection.settings();
//            // Print resulting settings
//            qDebug() << (*newSettings.data());

//            // Continue with adding secrets
//            NetworkManager::WirelessSecuritySetting::Ptr wirelessSecuritySetting = newSettings->setting(NetworkManager::Setting::WirelessSecurity).dynamicCast<NetworkManager::WirelessSecuritySetting>();
//            if (!wirelessSecuritySetting->needSecrets().isEmpty()) {
//                qDebug() << "Need secrets: " << wirelessSecuritySetting->needSecrets();
//                // TODO: fill missing secrets
//            }

        } else {
            qDebug() << "Connection failed: " << reply.error();
            return false;
        }

        return true;
}

bool Wifi::isHotspotActive()
{
    if(! m_WifiDevice) return false;
    NetworkManager::ActiveConnection::Ptr activeConnection = m_WifiDevice->activeConnection();
    NetworkManager::Connection::Ptr connection = activeConnection->connection();
    NetworkManager::WirelessSetting::Ptr wirelessSetting = connection->settings()->setting(NetworkManager::Setting::Wireless).dynamicCast<NetworkManager::WirelessSetting>();
    return wirelessSetting->mode() == NetworkManager::WirelessSetting::Ap;
}

bool Wifi::isClientActive()
{
    if(! m_WifiDevice) return false;
    NetworkManager::ActiveConnection::Ptr activeConnection = m_WifiDevice->activeConnection();
    NetworkManager::Connection::Ptr connection = activeConnection->connection();
    NetworkManager::WirelessSetting::Ptr wirelessSetting = connection->settings()->setting(NetworkManager::Setting::Wireless).dynamicCast<NetworkManager::WirelessSetting>();
    return wirelessSetting->mode() == NetworkManager::WirelessSetting::Infrastructure;
}

void Wifi::deactivateCurrentConnection()
{
    if(! m_WifiDevice) return;
    NetworkManager::ActiveConnection::Ptr activeConnection = m_WifiDevice->activeConnection();
    if(activeConnection) {
        NetworkManager::Connection::Ptr connection = activeConnection->connection();
        if(connection) {
            QDBusPendingReply <QDBusObjectPath, QDBusObjectPath > reply = NetworkManager::deactivateConnection(connection->path());
            reply.waitForFinished();

            // Check if this connection was added successfuly
                if (reply.isValid()) {
                    qDebug() <<  "deactivateCurrentConnection - no connection(reply is not valid)";
                }
            return;
        }
    }
    qDebug() << "deactivateCurrentConnection - no connection";
}

bool Wifi::modifyCurrent(QString ip, QString netmask, QString gateway, QString dns)
{
    if(! m_WifiDevice) return false;

    NetworkManager::WirelessDevice::Ptr wifiDevice = m_WifiDevice;

    NetworkManager::ActiveConnection::Ptr activeConnection = wifiDevice->activeConnection();
    NetworkManager::Connection::Ptr connection = activeConnection->connection();

    NetworkManager::ConnectionSettings::Ptr settings = connection->settings();
    NetworkManager::Ipv4Setting::Ptr ipv4Setting = settings->setting(NetworkManager::Setting::Ipv4).dynamicCast<NetworkManager::Ipv4Setting>();
    NetworkManager::WirelessSetting::Ptr wirelessSetting = settings->setting(NetworkManager::Setting::Wireless).dynamicCast<NetworkManager::WirelessSetting>();
    NetworkManager::WirelessSecuritySetting::Ptr wirelessSecuritySetting = settings->setting(NetworkManager::Setting::WirelessSecurity).dynamicCast<NetworkManager::WirelessSecuritySetting>();
    QString activeAccessPointPath = wifiDevice->activeAccessPoint()->uni();
    ipv4Setting->setMethod(NetworkManager::Ipv4Setting::Manual);

    ipv4Setting->setDns(QList<QHostAddress>({QHostAddress(dns)}));
    NetworkManager::IpAddress addr;
    addr.setIp(QHostAddress(ip));
    addr.setNetmask(QHostAddress(netmask));
    ipv4Setting->setAddresses(QList<NetworkManager::IpAddress>({addr}));
    ipv4Setting->setGateway(gateway);
    //connection->update(settings);

    qDebug() << "PSK: " << wirelessSecuritySetting->psk();

    // -------------- Get PSK of the connection
    NetworkManager::WirelessSecurityType securityType = NetworkManager::securityTypeFromConnectionSetting(settings);//static_cast<NetworkManager::WirelessSecurityType>(_securityType);
    const auto key = QStringLiteral("802-11-wireless-security");
    auto reply = connection->secrets(key);

    const auto secret = reply.argumentAt<0>()[key];
    QString pass;
    switch (securityType) {
        case NetworkManager::NoneSecurity:
            break;
        case NetworkManager::WpaPsk:
        case NetworkManager::Wpa2Psk:
            pass = secret["psk"].toString();
            qDebug() << "PSK: " << pass;
            break;
        default:
            return {};
    }
    // -----

    QDBusPendingReply<> replyUpdate = connection->update(settings->toMap());
    replyUpdate.waitForFinished();
    NetworkManager::deactivateConnection(activeConnection->path());
    NetworkManager::activateConnection(connection->path(), wifiDevice->uni(), activeAccessPointPath);
    return true;
}

QJSValue Wifi::getPSK(QString connectionPath)
{
    if(! m_WifiDevice) return QJSValue::NullValue;
    NetworkManager::Connection::Ptr connection(new NetworkManager::Connection(connectionPath));
    if(!connection->isValid()) {
        qDebug() << "Wifi::getPSK path " << connectionPath << " is invalid";
        return QJSValue::NullValue;
    }
    NetworkManager::ConnectionSettings::Ptr settings = connection->settings();
    if(connection->settings()->connectionType() != NetworkManager::ConnectionSettings::Wireless) {
        qDebug() << "Wifi::getPSK path " << connectionPath <<" is not wifi connection";
        return QJSValue::NullValue;
    }

    NetworkManager::WirelessSecurityType securityType = NetworkManager::securityTypeFromConnectionSetting(settings);
    const auto key = QStringLiteral("802-11-wireless-security");
    auto reply = connection->secrets(key);
    reply.waitForFinished();
    const auto response = reply.argumentAt<0>();
    if(!response.contains(key)) {
        qDebug() << "Wifi::getPSK path " << connectionPath << " does not contain 802-11-wireless-security";
        return QJSValue::NullValue;
    }
    const auto secret = response[key];
    QString pass;
    switch (securityType) {
        case NetworkManager::NoneSecurity:
            break;
        case NetworkManager::WpaPsk:
        case NetworkManager::Wpa2Psk:
            pass = secret["psk"].toString();
            break;
        default:
            qDebug() << "Wifi::getPSK path " << connectionPath << " does not have a PSK set";
            return "";
    }
    return pass;
}

QJSValue Wifi::getPSKForId(QString connectionId)
{
    if(! m_WifiDevice) return QJSValue::NullValue;
    NetworkManager::Connection::List list = NetworkManager::listConnections();
    NetworkManager::Connection::Ptr connection;
    QList<QString> paths;
    Q_FOREACH(connection, list) {
        NetworkManager::ConnectionSettings::Ptr settings = connection->settings();
        if(settings->connectionType() == NetworkManager::ConnectionSettings::Wireless) {
            NetworkManager::WirelessSetting::Ptr wirelessSetting = settings->setting(NetworkManager::Setting::Wireless).dynamicCast<NetworkManager::WirelessSetting>();
            if(settings->id() == connectionId) {
                paths.append(connection->path());
            }
        }
    }
    if(paths.length() == 0) {
        return QJSValue::NullValue;
    }
    else {
        return getPSK(paths[0]);
    }

}

QList<QString> Wifi::listConnections()
{
    if(! m_WifiDevice) return {};
    NetworkManager::Connection::List list = NetworkManager::listConnections();
    NetworkManager::Connection::Ptr connection;
    QList<QString> ret;
    Q_FOREACH(connection, list) {
        if(connection->settings()->connectionType() == NetworkManager::ConnectionSettings::Wireless) {
            ret.append(connection->path());
        }
    }
    return ret;
}

QList<QString> Wifi::findConnectionsForSSID(QString ssid)
{
    if(! m_WifiDevice) return {};
    NetworkManager::Connection::List list = NetworkManager::listConnections();
    NetworkManager::Connection::Ptr connection;
    QList<QString> ret;
    Q_FOREACH(connection, list) {
        NetworkManager::ConnectionSettings::Ptr settings = connection->settings();
        if(settings->connectionType() == NetworkManager::ConnectionSettings::Wireless) {
            NetworkManager::WirelessSetting::Ptr wirelessSetting = settings->setting(NetworkManager::Setting::Wireless).dynamicCast<NetworkManager::WirelessSetting>();
            if(wirelessSetting->ssid() == ssid) {
                ret.append(connection->path());
            }
        }
    }
    return ret;
}

NetworkManager::Connection::List  Wifi::findConnectionsForSssidPtr(QString ssid)
{
    if(! m_WifiDevice) return {};
    NetworkManager::Connection::List list = NetworkManager::listConnections();
    NetworkManager::Connection::Ptr connection;
    NetworkManager::Connection::List ret;
    Q_FOREACH(connection, list) {
        NetworkManager::ConnectionSettings::Ptr settings = connection->settings();
        if(settings->connectionType() == NetworkManager::ConnectionSettings::Wireless) {
            NetworkManager::WirelessSetting::Ptr wirelessSetting = settings->setting(NetworkManager::Setting::Wireless).dynamicCast<NetworkManager::WirelessSetting>();
            if(wirelessSetting->ssid() == ssid) {
                ret.append(connection);
            }

        }
    }
    return ret;
}

QList<QString> Wifi::findConnectionsForId(QString connectionId)
{
    if(! m_WifiDevice) return {};
    NetworkManager::Connection::List list = NetworkManager::listConnections();
    NetworkManager::Connection::Ptr connection;
    QList<QString> ret;
    Q_FOREACH(connection, list) {
        NetworkManager::ConnectionSettings::Ptr settings = connection->settings();
        if(settings->connectionType() == NetworkManager::ConnectionSettings::Wireless) {
            NetworkManager::WirelessSetting::Ptr wirelessSetting = settings->setting(NetworkManager::Setting::Wireless).dynamicCast<NetworkManager::WirelessSetting>();
            if(settings->id() == connectionId) {
                ret.append(connection->path());
            }
        }
    }
    return ret;
}

QList<QString> Wifi::getSsidAndPskForConnection(QString connectionPath)
{
    if(! m_WifiDevice) return {};
    NetworkManager::Connection::Ptr connection(new NetworkManager::Connection(connectionPath));
    if(!connection->isValid()) {
        qDebug() << "Wifi::getPSK path " << connectionPath << " is invalid";
        return QList<QString>();
    }
    NetworkManager::ConnectionSettings::Ptr settings = connection->settings();
    if(connection->settings()->connectionType() != NetworkManager::ConnectionSettings::Wireless) {
        qDebug() << "Wifi::getPSK path " << connectionPath <<" is not wifi connection";
        return QList<QString>();
    }

    NetworkManager::WirelessSecurityType securityType = NetworkManager::securityTypeFromConnectionSetting(settings);
    const auto key = QStringLiteral("802-11-wireless-security");
    auto reply = connection->secrets(key);
    reply.waitForFinished();
    const auto response = reply.argumentAt<0>();
    if(!response.contains(key)) {
        qDebug() << "Wifi::getPSK path " << connectionPath << " does not contain 802-11-wireless-security";
        return QList<QString>();
    }
    const auto secret = response[key];
    QString pass;
    switch (securityType) {
        case NetworkManager::NoneSecurity:
            break;
        case NetworkManager::WpaPsk:
        case NetworkManager::Wpa2Psk:
            pass = secret["psk"].toString();
            break;
        default:
            qDebug() << "Wifi::getPSK path " << connectionPath << " does not have a PSK set";
            return QList<QString>();
    }
    NetworkManager::WirelessSetting::Ptr wirelessSettings = settings->setting(NetworkManager::Setting::Wireless).dynamicCast<NetworkManager::WirelessSetting>();
    QList<QString> pair({wirelessSettings->ssid(), pass});
    return pair;
}

QList<QString> Wifi::getSsidAndPskForId(QString connectionId)
{
    if(! m_WifiDevice) return {};
    QList<QString> ret;
    QList<QString> paths = findConnectionsForId(connectionId);
    QString path;
    Q_FOREACH(path, paths) {
        ret = getSsidAndPskForConnection(path);
        break;
    }
    return ret;
}

void Wifi::remove_connections(NetworkManager::WirelessSetting::NetworkMode mode, QString ssid)
{
    if(! m_WifiDevice) return;
    NetworkManager::Connection::List list = NetworkManager::listConnections();
    NetworkManager::Connection::Ptr connection;
    QList<QString> ret;
    Q_FOREACH(connection, list) {
        NetworkManager::ConnectionSettings::Ptr settings = connection->settings();
        if(settings->connectionType() == NetworkManager::ConnectionSettings::Wireless) {
            NetworkManager::WirelessSetting::Ptr wirelessSetting = settings->setting(NetworkManager::Setting::Wireless).dynamicCast<NetworkManager::WirelessSetting>();
            if(wirelessSetting->mode() == mode) {
                if(ssid.isEmpty() || ssid == wirelessSetting->ssid()) {
                    qDebug() << "Removing network: " << wirelessSetting->ssid();
                    auto reply = connection->remove();
                    reply.waitForFinished();
                }
            }
        }
    }
}

QString Wifi::hardwareAddress() const {
    if(! m_WifiDevice) return "NO-ADDRESS";
    return m_WifiDevice->hardwareAddress();
}
