/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SECRETPRUSA_H
#define SECRETPRUSA_H
#include <QSharedPointer>
#include <NetworkManagerQt/SecretAgent>
#include <QDebug>
class SecretPrusaRepresentant;

/** Implements subset of SecretAgent to allow supplying a new PSK in case it is wrong/missing */
class SecretPrusa : public NetworkManager::SecretAgent
{
    Q_OBJECT

    friend class SecretPrusaRepresentant;
    /// Backed-up parameters of the last request
    NMVariantMapMap m_Connection;
    QDBusObjectPath m_Connection_path;
    QString m_Setting_name;
    QStringList m_Hints;
    uint m_Flags;

    QString m_Connection_name;
    QDBusMessage m_Message;

    /** Needs to handle only one request at a time,
     * if another arrives while the first one is still pending,
     * it will be ignored.
     */
    bool m_Request_pending;

public:
    typedef  QSharedPointer<SecretPrusa> Ptr;

    Q_PROPERTY(bool request_pending MEMBER m_Request_pending READ request_pending NOTIFY request_pendingChanged)
    bool request_pending() const {return m_Request_pending;  }

    Q_PROPERTY(QString connection_name MEMBER m_Connection_name READ connection_name NOTIFY connection_nameChanged)
    QString connection_name() const { return m_Connection_name; }

    /// Sends back the wifiSecurity structure with the secret filled-in
    Q_INVOKABLE void respond(QString psk);

    /// Send an empty structure back
    Q_INVOKABLE void cancel();

    explicit SecretPrusa(QObject * parent = nullptr);
public slots:
    NMVariantMapMap GetSecrets(const NMVariantMapMap&connection,
                               const QDBusObjectPath&connection_path,
                               const QString&setting_name,
                               const QStringList&hints,
                               uint flags) override;

    /// Ignored
    void SaveSecrets(const NMVariantMapMap &connection, const QDBusObjectPath &connection_path) override;

    /// Ignored
    void DeleteSecrets(const NMVariantMapMap &, const QDBusObjectPath &) override;

    /// Emits signal
    void CancelGetSecrets(const QDBusObjectPath &, const QString &) override;
signals:
    void request_pendingChanged();
    void connection_nameChanged();
    void canceled_by_networkManager();
};


class SecretPrusaRepresentant: public QObject {
    Q_OBJECT

    SecretPrusa::Ptr m_SecretPrusa;
public:
    typedef QSharedPointer<SecretPrusaRepresentant> Ptr;
    explicit SecretPrusaRepresentant(SecretPrusa::Ptr secretPrusa, QObject * parent = nullptr) : QObject(parent), m_SecretPrusa(secretPrusa) {
        connect(m_SecretPrusa.data(), &SecretPrusa::request_pendingChanged, this, [&](){emit request_pendingChanged(); qDebug() << "request_pending" << this->getRequestPending(); });
        connect(m_SecretPrusa.data(), &SecretPrusa::connection_nameChanged, this, [&](){emit connection_nameChanged();qDebug() << "connection_name" << this->getConnectionName();});
        connect(m_SecretPrusa.data(), &SecretPrusa::canceled_by_networkManager, this, [&](){emit canceled_by_networkManager(); qDebug() << "canceled_by_networkManager"; });

    }
    Q_PROPERTY(bool request_pending READ getRequestPending NOTIFY request_pendingChanged)
    Q_PROPERTY(QString connection_name READ getConnectionName NOTIFY connection_nameChanged)
    Q_INVOKABLE void respond(QString psk) {m_SecretPrusa->respond(psk); }
    Q_INVOKABLE void cancel() {m_SecretPrusa->cancel(); }

    Q_INVOKABLE bool getRequestPending() {return m_SecretPrusa->request_pending(); }
    Q_INVOKABLE QString getConnectionName() {return m_SecretPrusa->connection_name(); }


signals:
    void request_pendingChanged();
    void connection_nameChanged();
    void canceled_by_networkManager();

};

#endif // SECRETPRUSA_H
