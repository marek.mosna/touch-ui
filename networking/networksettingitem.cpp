/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "networksettingitem.h"

SettingItem::SettingItem(int _listPosition,
                         const Sections &_section,
                         const DelegateType &_delegateType,
                         const QString &_identifier,
                         const QString &_label,
                         const QVariant &_data,
                         bool _readOnly,
                         bool _editable,
                         QObject *parent) :
    QObject (parent),
    listPosition(_listPosition),
    section(_section),
    delegateType(_delegateType),
    identifier(_identifier),
    label(_label),
    data(_data),
    readOnly(_readOnly),
    editable(_editable)
{

}

SettingItem::Ptr SettingItem::create(int _listPosition, const SettingItem::Sections &_section, const SettingItem::DelegateType &_delegateType, const QString &_identifier, const QString &_label, const QVariant &_data, bool _readOnly, bool _editable, QObject *parent)
{
    SettingItem * ptr = new SettingItem(_listPosition, _section, _delegateType, _identifier, _label, _data, _readOnly, _editable, parent);
    return Ptr(ptr);
}
