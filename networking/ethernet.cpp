/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "ethernet.h"

Ethernet::Ethernet(QObject *parent) : QObject(parent)
{
    // Find the first Wired device(there should only be one)
    NetworkManager::Device::List devices = NetworkManager::networkInterfaces();
    NetworkManager::Device::Ptr dev;
    Q_FOREACH(dev, devices) {
        if(dev->type() == NetworkManager::Device::Ethernet) {
            m_Device = qobject_cast<QSharedPointer<NetworkManager::WiredDevice> >(dev);
            break;
        }
    }

    connect((NetworkManager::Device*)m_Device.data(), &NetworkManager::Device::activeConnectionChanged, this, &Ethernet::slotActiveConnectionChanged);

    m_ActiveConnection = m_Device->activeConnection();
    if(m_ActiveConnection) {
        connect(m_ActiveConnection.data(), &NetworkManager::ActiveConnection::stateChangedReason, this, &Ethernet::slotActiveConnectionStateChangedReason);
        connect(m_ActiveConnection.data(), &NetworkManager::ActiveConnection::stateChanged, this, &Ethernet::slotActiveConnectionStateChanged);
    }

    connect((NetworkManager::Device*)m_Device.data(), &NetworkManager::Device::ipV4AddressChanged, this, &Ethernet::refreshScratchpad);
    connect((NetworkManager::Device*)m_Device.data(), &NetworkManager::Device::dhcp4ConfigChanged, this, &Ethernet::refreshScratchpad);
    connect(m_Device.data(), &NetworkManager::WiredDevice::hardwareAddressChanged, this, &Ethernet::slotHwAddressChanged);
    connect(m_Device.data(), &NetworkManager::WiredDevice::carrierChanged, this, &Ethernet::slotCarrierChanged);
    connect((NetworkManager::Device*)m_Device.data(), &NetworkManager::Device::ipV4ConfigChanged, this, [=](){emit ipAddressChanged(ipAddress()); });
    if(m_ActiveConnection) {
        connect(m_ActiveConnection.data(), &NetworkManager::ActiveConnection::stateChanged,
            this, [=](NetworkManager::ActiveConnection::State s) { emit activeConnectionStateChanged(ActiveConnectionState(static_cast<int>(s))); });
    }
}


void Ethernet::refreshScratchpad()
{
    if(m_ActiveConnection) {
        NetworkManager::IpConfig ipconfig =  m_ActiveConnection->ipV4Config();
        NetworkManager::Dhcp4Config::Ptr dhcpconfig = m_ActiveConnection->dhcp4Config();
        NetworkManager::Ipv4Setting::Ptr ipsetting = m_ActiveConnection->connection()->settings()->setting(NetworkManager::Setting::Ipv4).dynamicCast<NetworkManager::Ipv4Setting>();
        if(ipsetting->method() == NetworkManager::Ipv4Setting::Automatic) {
            m_ScratchpadDhcp = true;
        }
        else {
            m_ScratchpadDhcp = false;
        }

        QVariantMap info = getActiveConnectionInfo();
        m_ScratchpadDns = (info.find("dns") != info.end() ? info["dns"].toString() : "");
        m_ScratchpadGateway = (info.find("gateway") != info.end() ? info["gateway"].toString() : "");
        m_ScratchpadIp = (info.find("address") != info.end() ? info["address"].toString() : "");
        m_ScratchpadMask = (info.find("mask") != info.end() ? info["mask"].toString() : "");
    }
    //NetworkManager::Ipv4Setting::Ptr ipsetting = m_ActiveConnection->connection()->settings()->setting(NetworkManager::Setting::Ipv4).dynamicCast<NetworkManager::Ipv4Setting>();

    emit scratchPadChanged();
    emit dhcpChanged(m_ScratchpadDhcp);
}

void Ethernet::applyScratchpad(QJSValue callbackSuccess, QJSValue callbackFail)
{
    auto availableConnections = m_Device->availableConnections();
    NetworkManager::Connection::Ptr connection;
    if(m_ActiveConnection.isNull() && availableConnections.isEmpty()) {
        if(callbackFail.isCallable()) callbackFail.call({"No active connection, cannot apply scratchpad"});
        return;
    }
    else if(m_ActiveConnection.isNull()) { // Pick the first available connection
        connection = availableConnections.first();
    }
    else { // Pick the active connection
        connection = m_ActiveConnection->connection();
    }
    Q_ASSERT( ! connection.isNull() );
    auto settings = connection->settings();
    NetworkManager::Ipv4Setting::Ptr ipsetting = settings->setting(NetworkManager::Setting::Ipv4).dynamicCast<NetworkManager::Ipv4Setting>();
    NetworkManager::Ipv4Setting::Ptr newIpSetting = NetworkManager::Ipv4Setting::Ptr(new NetworkManager::Ipv4Setting());

    if(m_ScratchpadDhcp) {
        ipsetting->setMethod(NetworkManager::Ipv4Setting::Automatic);
    }
    else {
        QHostAddress adr;
        if(! adr.setAddress(m_ScratchpadDns)) {
            if(callbackFail.isCallable()) {
                callbackFail.call({"Bad DNS"});
            }
        }
        if(! adr.setAddress(m_ScratchpadIp)) {
            if(callbackFail.isCallable()) {
                callbackFail.call({"Bad IP"});
            }
        }
        if(! adr.setAddress(m_ScratchpadMask)) {
            if(callbackFail.isCallable()) {
                callbackFail.call({"Bad Mask"});
            }
        }
        if(! adr.setAddress(m_ScratchpadGateway)) {
            if(callbackFail.isCallable()) {
                callbackFail.call({"Bad Gateway"});
            }
        }
    }
    ipsetting->setInitialized(true);

    NMVariantMapMap toUpdateMap = settings->toMap();


    //NetworkManager::Ipv4Setting::Ptr ipSetting = connection->settings()->setting(NetworkManager::Setting::Ipv4).staticCast<NetworkManager::Ipv4Setting>();
    if (ipsetting->method() == NetworkManager::Ipv4Setting::Automatic || ipsetting->method() == NetworkManager::Ipv4Setting::Manual) {
        if (m_ScratchpadDhcp == true) {
            newIpSetting->setMethod(NetworkManager::Ipv4Setting::Automatic);
        }

        if (m_ScratchpadDhcp == false) {
            newIpSetting->setMethod(NetworkManager::Ipv4Setting::Manual);
            NetworkManager::IpAddress ipaddr;
            ipaddr.setIp(QHostAddress(m_ScratchpadIp));
            //ipaddr.setPrefixLength(map["prefix"].toInt());
            ipaddr.setNetmask(QHostAddress(m_ScratchpadMask));
            ipaddr.setGateway(QHostAddress(m_ScratchpadGateway));
            newIpSetting->setAddresses(QList<NetworkManager::IpAddress>({ipaddr}));
            newIpSetting->setDns(QList<QHostAddress>({QHostAddress(m_ScratchpadDns)}));
        }
        toUpdateMap.insert("ipv4", newIpSetting->toMap());
    }
    qWarning() << toUpdateMap;
    QDBusPendingReply<> reply = connection->update(toUpdateMap);
    QDBusPendingCallWatcher *watcher = new QDBusPendingCallWatcher(reply, this);
    connect(watcher, &QDBusPendingCallWatcher::finished, this, [=](QDBusPendingCallWatcher *watcher) mutable {
        QDBusPendingReply<> reply = *watcher;
        if(reply.isValid()) {
                if(callbackSuccess.isCallable()) callbackSuccess.call();
                auto replyX = NetworkManager::deactivateConnection(connection->path());

                QDBusPendingCallWatcher *watcherX = new QDBusPendingCallWatcher(replyX, this);
                connect(watcherX, &QDBusPendingCallWatcher::finished, this, [=](QDBusPendingCallWatcher *watcher) mutable {
                    QDBusPendingReply<> reply = *watcher;
                    auto replyY = NetworkManager::activateConnection(connection->path(), m_Device->uni(), "/");
                    QDBusPendingCallWatcher *watcherY = new QDBusPendingCallWatcher(replyY, this);

                    connect(watcherY, &QDBusPendingCallWatcher::finished, this, [=](QDBusPendingCallWatcher *watcher) mutable {
                        QDBusPendingReply<> reply = *watcher;
                        if(reply.isValid() && callbackSuccess.isCallable()) callbackSuccess.call();
                        if(reply.isError() && callbackFail.isCallable()) callbackFail.call({"Could not reactivate connection after changing parameters: "  + reply.error().message()});
                        watcher->deleteLater();
                    });


                    watcher->deleteLater();
                });
        }
        if(reply.isError() && callbackFail.isCallable()) callbackFail.call({"Could not reactivate connection after changing parameters: "  + reply.error().message()});
        watcher->deleteLater();
    });
}

QVariantMap Ethernet::getActiveConnectionInfo() const
{
    if (m_ActiveConnection.isNull())
            return QVariantMap();
    QVariantMap map;
    auto ipconfig = m_Device->ipV4Config();
    if (ipconfig.addresses().count() > 0) {
        map.insert("address",QVariant(ipconfig.addresses().constFirst().ip().toString()));
        map.insert("mask",QVariant(ipconfig.addresses().constFirst().netmask().toString()));
    }

    map.insert("gateway",QVariant(ipconfig.gateway()));
    if (ipconfig.nameservers().count() > 0)
        map.insert("dns",QVariant(ipconfig.nameservers().constFirst().toString()));
    return map;
}

QString Ethernet::hwAddress() const
{
    return m_Device->hardwareAddress();
}

bool Ethernet::carrier() const
{
    return m_Device->carrier();
}

QString Ethernet::activeConnectionName() const
{
    if(m_ActiveConnection) {
        return m_ActiveConnection->connection()->name();
    }
    return "";
}

QString Ethernet::ipAddress() const
{
    QVariantMap map = getActiveConnectionInfo();
    if(map.find("address") != map.end()) {
        return map["address"].toString();
    }
    else return "";

}

Ethernet::ActiveConnectionState Ethernet::activeConnectionState() const
{
    if(m_ActiveConnection) {
        return ActiveConnectionState(static_cast<int>(m_ActiveConnection->state()));
    }
    else return ActiveConnectionState::Unknown;
}

int Ethernet::activeConnectionReason() const
{
    if(m_ActiveConnection) {
        return static_cast<int>(m_ActiveConnectionStateReason);
    }
    else return 0;
}

void Ethernet::slotActiveConnectionChanged()
{
    m_ActiveConnection = m_Device->activeConnection();
    if(m_ActiveConnection) {
        connect(m_ActiveConnection.data(), &NetworkManager::ActiveConnection::stateChangedReason, this, &Ethernet::slotActiveConnectionStateChangedReason);
        connect(m_ActiveConnection.data(), &NetworkManager::ActiveConnection::stateChanged, this, &Ethernet::slotActiveConnectionStateChanged);
    }
    refreshScratchpad();
    emit activeConnectionNameChanged();
    emit ipAddressChanged(ipAddress());
}

void Ethernet::slotActiveConnectionStateChanged(NetworkManager::ActiveConnection::State state)
{
    emit activeConnectionStateChanged(ActiveConnectionState(static_cast<int>(state)));

}

void Ethernet::slotActiveConnectionStateChangedReason(NetworkManager::ActiveConnection::State state, NetworkManager::ActiveConnection::Reason reason)
{
    refreshScratchpad();
    emit activeConnectionStateChangedReason(ActiveConnectionState(static_cast<int>(state)), ActiveConnectionReason(static_cast<int>(reason)));
}

void Ethernet::slotHwAddressChanged()
{
    emit hwAddressChanged();
}

void Ethernet::slotCarrierChanged()
{
    emit carrierChanged();
}



const QString &Ethernet::defaultStaticIpAddress() const
{
    return m_defaultStaticIpAddress;
}

const QString &Ethernet::defaultStaticNetmask() const
{
    return m_defaultStaticNetmask;
}
