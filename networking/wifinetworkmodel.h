/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef WIFINETWORKMODEL_H
#define WIFINETWORKMODEL_H

#include <QAbstractListModel>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QDebug>
#include <QObject>
#include <QJSValue>

#include <arpa/inet.h>

#include <NetworkManagerQt/Manager>
#include <NetworkManagerQt/Device>
#include <NetworkManagerQt/WirelessDevice>
#include <NetworkManagerQt/AccessPoint>
#include <NetworkManagerQt/Connection>
#include <NetworkManagerQt/ConnectionSettings>
#include <NetworkManagerQt/WirelessSetting>
#include <NetworkManagerQt/WirelessSecuritySetting>
#include <NetworkManagerQt/Ipv4Setting>
#include <NetworkManagerQt/WiredDevice>
//#include <QDBusMetaType>
#include <QTimer>

#include "networksettingmodel.h"
class WifiNetworkModel;

class NetworkItem : public QObject {
protected:
    Q_OBJECT
public:
    typedef QSharedPointer< NetworkItem > 	Ptr;
    QString networkName;

    // These are not meant for the View
    NetworkManager::WirelessNetwork::Ptr network;


    bool active; // Is the network active(connected/connecting) ?
    int signalStrength; // To be refreshed when needed

    int listPosition; // Position in the container, not to be exposed
    int section;
    QString connectionPath;
    NetworkManager::Connection::Ptr connection;
    QString psk;

    NetworkItem(QObject * parent = nullptr) : QObject(parent),
        networkName(QStringLiteral("")),
        network(nullptr),
        active(false),
        signalStrength(0),
        listPosition(0),
        section(0),
        connectionPath(""),
        connection(nullptr),
        psk("") {}
    NetworkItem(QString _networkName,
                NetworkManager::WirelessNetwork::Ptr _network,
                bool _active,
                int _signalStrength,
                int _listPosition,
                QString _connectionPath,
                NetworkManager::Connection::Ptr _connection,
                QString _psk,
                QObject * parent = nullptr) :
        QObject(parent),
        networkName(_networkName),
        network(_network),
        active(_active),
        signalStrength(_signalStrength),
        listPosition(_listPosition),
        section(0),
        connectionPath(_connectionPath),
        connection(_connection),
        psk(_psk){}

    /** @brief LessEqual comparison function to sort items
     * Items are compared by:
     *  1) section
     *  2) items without network come always after items with a connection(within section)
     *  3) items without network come after all other items(within section)
     *  4) signal strength
     */
    static bool ItemLessEqual(const Ptr a, const Ptr b);
public slots:
    void slot_SignalStrengthChanged(int _strength);
};


class WifiNetworkModel : public QAbstractListModel
{
    Q_OBJECT

public:
    // Attribute description
    enum ItemRoles {
        RoleName = Qt::UserRole + 1, // SSID == ID
        RoleStrength,   // Signal strength
        RoleConnection, // Could be null, not all SSIDs will have a connection ready. ConnectionPath is also stored as part of the item.
        RoleReferenceAp,// D-Bus path to the AP
        RoleActive,     // Is its connection active ?
        RoleSecurity,   // WpaFlags
        RoleSection,    // Allows to separate items into sections and assign special delegates depending on section
        RoleNetwork,    // Null if the network is not active, otherwise networkName
        RoleHidden,     // The "hidden" flag of the wifi network setting
        RolePsk,
    };



    enum ItemSections {
        SectionUnknown = 0,
        SectionWifiOnOff = 1,
        SectionWifiClientOnOff = 2,
        SectionNetwork = 3,
        SectionAddNetwork = 4,
        SectionQrcode = 5,
    };
    Q_ENUM(ItemSections)

    // Enums copied from networkmanager to make them accessible to qml
    enum  	WpaFlag {
        PairWep40 = 0x1, PairWep104 = 0x2, PairTkip = 0x4, PairCcmp = 0x8,
        GroupWep40 = 0x10, GroupWep104 = 0x20, GroupTkip = 0x40, GroupCcmp = 0x80,
        KeyMgmtPsk = 0x100, KeyMgmt8021x = 0x200, KeyMgmtSAE = 0x400
    };
    Q_ENUM(WpaFlag)

    enum  	ActiveConnectionReason {
        UknownReason = 0, None, UserDisconnected, DeviceDisconnected,
        ServiceStopped, IpConfigInvalid, ConnectTimeout, ServiceStartTimeout,
        ServiceStartFailed, NoSecrets, LoginFailed, ConnectionRemoved,
        DependencyFailed, DeviceRealizeFailed, DeviceRemoved
    };
    Q_ENUM(ActiveConnectionReason)

    enum  	ActiveConnectionState {
        Unknown = 0, Activating, Activated, Deactivating,
        Deactivated
    };
    Q_ENUM(ActiveConnectionState)

    explicit WifiNetworkModel(QObject *parent = nullptr);

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    virtual QHash<int, QByteArray> roleNames() const override;

    //Q_PROPERTY(QDateTime lastScan READ lastScan NOTIFY lastScanChanged)
    Q_PROPERTY(ActiveConnectionState activeConnectionState READ activeConnectionState NOTIFY activeConnectionStateChanged)
    Q_PROPERTY(QString activeConnectionName READ activeConnectionName NOTIFY activeConnectionNameChanged)
    Q_PROPERTY(QString activeConnectionSsid READ activeConnectionSsid NOTIFY activeConnectionSsidChanged)

    Q_PROPERTY(bool isHotspotRunning READ isHotspotRunning WRITE setHotspotRunning NOTIFY isHotspotRunningChanged)
    Q_PROPERTY(NetworkSettingModel * networkSettingModel READ getNetworkSettingModel NOTIFY networkSettingModelChanged)
    Q_PROPERTY(QString ipAddress READ ipAddress NOTIFY ipAddressChanged)
    Q_PROPERTY(int activeConnectionSignalStrength READ activeConnectionSignalStrength NOTIFY activeConnectionSignalStrengthChanged)
    Q_PROPERTY(bool wifiEnabled READ wifiEnabled WRITE wifiEnabledSet NOTIFY wifiEnabledChanged)
    Q_PROPERTY(QDateTime lastScan READ lastScan NOTIFY lastScanChanged)




    // properties to set/get ip4 network parameter. TODO: move into its own data model
    Q_PROPERTY(bool scratchpadDhcp MEMBER m_ScratchpadDhcp NOTIFY scratchPadChanged)
    Q_PROPERTY(QString scratchpadIp MEMBER m_ScratchpadIp NOTIFY scratchPadChanged)
    Q_PROPERTY(QString scratchpadMask MEMBER m_ScratchpadMask NOTIFY scratchPadChanged)
    Q_PROPERTY(QString scratchpadGateway MEMBER m_ScratchpadGateway NOTIFY scratchPadChanged)
    Q_PROPERTY(QString scratchpadDns MEMBER m_ScratchpadDns NOTIFY scratchPadChanged)
    Q_INVOKABLE void refreshScratchpad();
    Q_INVOKABLE void applyScratchpad(QJSValue callbackSuccess, QJSValue callbackFail);



    Q_INVOKABLE bool connectToNetwork(QString ssid, QJSValue psk, QJSValue doneCallback, QJSValue errorCallback);

    /** @return true if the connection exists, false if it doesn't. The actual password, or lack of thereof
     * will be passed to the doneCallback(string password), or an errorCallback(string error) will be executed.
     */
    Q_INVOKABLE QJSValue getPSK(QString connectionPath, QJSValue doneCallback, QJSValue errorCallback) ;

    /** @return true if the connection exists, false if it doesn't. The actual password, or lack of thereof
     * will be passed to the doneCallback(string password), or an errorCallback(string error) will be executed.
     */
    Q_INVOKABLE QJSValue getPSKbyName(QString name, QJSValue doneCallback, QJSValue errorCallback) ;

    Q_INVOKABLE void requestScan() const;
    Q_INVOKABLE QString getLocalHotspotSSID() const;
    Q_INVOKABLE void removeConnection(QString name, QJSValue callbackSuccess , QJSValue callbackFailure );
    Q_INVOKABLE void disconnectWifi();
    Q_INVOKABLE void requestScan();

    void refreshPsk(NetworkItem::Ptr item);
    Q_INVOKABLE void refreshPsk(QString ssid);
    Q_INVOKABLE QString getPskForNetwork(QString ssid);


    //QDateTime lastScan() const;

    WifiNetworkModel::ActiveConnectionState activeConnectionState() const;
    QString activeConnectionName() const;
    QString activeConnectionSsid() const;
    bool isHotspotRunning() const;
    void setHotspotRunning(bool);
    NetworkSettingModel *getNetworkSettingModel() const;

    QVector<NetworkItem::Ptr> *items();
    QModelIndex publicCreateIndex(int row, int col) { return createIndex(row, col); }
    QVariantMap getActiveConnectionInfo() const;
    QString ipAddress() const;
    int activeConnectionSignalStrength() const;
    bool wifiEnabled() const;
    void wifiEnabledSet(bool value);
    QDateTime lastScan() const;

    NetworkManager::Connection::Ptr connectionBySsid(const QString& ssid);
    NetworkItem::Ptr itemByConnection(const QString & connectionPath);
    NetworkItem::Ptr getItem(const QString& name);
    /** Adds an item, or if it already exists, returns the existing one */
    NetworkItem::Ptr insertOrGetItem(const QString& name);


    bool tryRemoveItem(const QString& name);
    bool tryRemoveItem(NetworkItem::Ptr item);
    NetworkItem::Ptr addConnectionToItem(const QString& name, const QString& connectionPath);
    NetworkItem::Ptr addConnectionToItem(const QString& name, NetworkManager::Connection::Ptr connection);
    NetworkItem::Ptr addConnectionToItem( NetworkManager::Connection::Ptr connection);
    NetworkItem::Ptr addNetworkToItem(const QString& name);
    NetworkItem::Ptr addNetworkToItem(const QString& name, NetworkManager::WirelessNetwork::Ptr network);
    NetworkItem::Ptr itemRemoveNetwork(const QString & name);
    NetworkItem::Ptr itemRemoveConnection(const QString & path);



protected:
    QString m_ScratchpadIp;
    QString m_ScratchpadMask;
    QString m_ScratchpadGateway;
    QString m_ScratchpadDns;
    bool m_ScratchpadDhcp;
    QTimer m_SignalRefresher;
    int m_PermanentTail; ///< Convenient variable, how many permanent items are attached to the end of the network list
    int m_PermanentHead; ///< How many items will always preceed the networks in the list(switches, other controls...)

    /// Add all connection(create items for those that don't have a visible network)
    void refreshItems();

    /// Sort the NetworkItem items in m_Network
    void sortItems();

signals:
    void scratchPadChanged();
    void networkSettingModelChanged(NetworkSettingModel*);
    void isHotspotRunningChanged(bool value);
    void activeConnectionNameChanged(QString);
    void activeConnectionSsidChanged(QString);
    //void lastScanChanged(const QDateTime);
    void activeConnectionStateChanged(ActiveConnectionState state);
    void ipAddressChanged(QString ipAddress);
    void activeConnectionStateChangedReason (int state, int reason);
    void activeConnectionSignalStrengthChanged();
    void hotspotPskChanged();
    void wifiEnabledChanged(bool wifiEnabled);
    void lastScanChanged(const QDateTime & when);
public slots:
    void accessPointAppeared (const QString &uni);
    void accessPointDisappeared (const QString &uni);
    void activeAccessPointChanged (const QString &);

    /** @brief Network mode changed to one of the:
    Unknown = 0 - not associated with a network
    Adhoc - part of an adhoc network
    Infra - a station in an infrastructure wireless network
    ApMode - access point in an infrastructure network
    */
    void modeChanged (NetworkManager::WirelessDevice::OperationMode);

    void networkAppeared (const QString &ssid);
    void networkDisappeared (const QString &ssid);

    void slotActiveConnectionChanged();
    void slotActiveConnectionStateChanged(NetworkManager::ActiveConnection::State state);
    void slotActiveConnectionStateChangedReason(NetworkManager::ActiveConnection::State state, NetworkManager::ActiveConnection::Reason reason);
    void slotAvailableConnectionAppeared(const QString & path);
    void slotAvailableConnectionDisappeared(const QString & path);


    /// Will handle only connection used & connected by the model
    void slotConnectionRemoved(const QString & path);

    /// When a new connection is added - relatively expensive operation
    void slotReloadConnections();
private:
    QVector<NetworkItem::Ptr> m_Networks;
    NetworkManager::WirelessDevice::Ptr m_Device;
    NetworkManager::WirelessDevice::OperationMode m_OperationMode;
    NetworkManager::ActiveConnection::Ptr m_ActiveConnection;
    NetworkSettingModel * m_NetworkSettingModel;
    bool m_WifiEnabled; // to keep previous value

};
//Q_DECLARE_METATYPE(WifiNetworkModel::ItemSections)
#endif // WIFINETWORKMODEL_H
