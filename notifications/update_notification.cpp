/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "update_notification.h"

UpdateNotification::UpdateNotification(QObject *parent) : Notification(Notification::UpdateNotification, parent)
{

}

QString UpdateNotification::bundlePath() const
{
    return m_bundlePath;
}

QString UpdateNotification::currentVersion() const
{
    return m_currentVersion;
}

QString UpdateNotification::nextVersion() const
{
    return m_nextVersion;
}

QString UpdateNotification::url() const
{
    return m_url;
}

QByteArray UpdateNotification::digest() const
{
    return m_digest;
}

QDateTime UpdateNotification::date() const
{
    return m_date;
}

QString UpdateNotification::releaseNotes() const
{
    return m_releaseNotes;
}

void UpdateNotification::setBundlePath(QString bundlePath)
{
    if (m_bundlePath == bundlePath)
        return;

    m_bundlePath = bundlePath;
    emit bundlePathChanged(m_bundlePath);
}

void UpdateNotification::setCurrentVersion(QString currentVersion)
{
    if (m_currentVersion == currentVersion)
        return;

    m_currentVersion = currentVersion;
    emit currentVersionChanged(m_currentVersion);
}

void UpdateNotification::setNextVersion(QString nextVersion)
{
    if (m_nextVersion == nextVersion)
        return;

    m_nextVersion = nextVersion;
    emit nextVersionChanged(m_nextVersion);
}

void UpdateNotification::setUrl(QString url)
{
    if (m_url == url)
        return;

    m_url = url;
    emit urlChanged(m_url);
}

void UpdateNotification::setDigest(QByteArray digest)
{
    if (m_digest == digest)
        return;

    m_digest = digest;
    emit digestChanged(m_digest);
}

void UpdateNotification::setDate(QDateTime date)
{
    if (m_date == date)
        return;

    m_date = date;
    emit dateChanged(m_date);
}

void UpdateNotification::setReleaseNotes(QString releaseNotes)
{
    if (m_releaseNotes == releaseNotes)
        return;

    m_releaseNotes = releaseNotes;
    emit releaseNotesChanged(m_releaseNotes);
}
