/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef UPDATE_NOTIFICATION_H
#define UPDATE_NOTIFICATION_H

#include <QObject>
#include "notification.h"

class UpdateNotification : public Notification
{
    Q_OBJECT
    QString m_bundlePath;

    QString m_currentVersion;

    QString m_nextVersion;

    QString m_url;

    QByteArray m_digest;

    QDateTime m_date;

    QString m_releaseNotes;

public:
    explicit UpdateNotification(QObject *parent = nullptr);
    Q_PROPERTY(QString bundlePath READ bundlePath WRITE setBundlePath NOTIFY bundlePathChanged)
    Q_PROPERTY(QString currentVersion READ currentVersion WRITE setCurrentVersion NOTIFY currentVersionChanged)
    Q_PROPERTY(QString nextVersion READ nextVersion WRITE setNextVersion NOTIFY nextVersionChanged)
    Q_PROPERTY(QString url READ url WRITE setUrl NOTIFY urlChanged)
    Q_PROPERTY(QByteArray digest READ digest WRITE setDigest NOTIFY digestChanged)
    Q_PROPERTY(QDateTime date READ date WRITE setDate NOTIFY dateChanged)
    Q_PROPERTY(QString releaseNotes READ releaseNotes WRITE setReleaseNotes NOTIFY releaseNotesChanged)



    QString nextVersion() const;

    /** @property UpdateNotification::url
     *  @brief URL of the available update bundle
     *  @default ""
     **/
    QString url() const;

    /** @property UpdateNotification::digest
     *  @brief Digest of this update bundle
     *  @default ""
     **/
    QByteArray digest() const;

    /** @property UpdateNotification::date
     *  @brief Time when this update was found(by the updater)
     *  @default ""
     **/
    QDateTime date() const;

    /** @property UpdateNotification::releaseNotes
     *  @brief Release notes for the available update bundle
     *  @default ""
     **/
    QString releaseNotes() const;

    /** @property UpdateNotification::bundlePath
     *  @brief Local filesystem path to the update bundle
     *  @default ""
     **/
    QString bundlePath() const;

    /** @property UpdateNotification::currentVersion
     *  @brief Current version string
     *  @default ""
     **/
    QString currentVersion() const;

signals:

    void bundlePathChanged(QString bundlePath);

    void currentVersionChanged(QString currentVersion);

    void nextVersionChanged(QString nextVersion);

    void urlChanged(QString url);

    void digestChanged(QByteArray digest);

    void dateChanged(QDateTime date);

    void releaseNotesChanged(QString releaseNotes);

public slots:
    void setBundlePath(QString bundlePath);
    void setCurrentVersion(QString currentVersion);
    void setNextVersion(QString nextVersion);
    void setUrl(QString url);
    void setDigest(QByteArray digest);
    void setDate(QDateTime date);
    void setReleaseNotes(QString releaseNotes);
};

#endif // UPDATE_NOTIFICATION_H
