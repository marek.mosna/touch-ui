/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FILEUPLOAD_H
#define FILEUPLOAD_H

#include <QObject>
#include "notification.h"
class FileUploadNotification : public Notification
{
    Q_OBJECT
    QString m_filename;

    quint64 m_size;

    double m_progress;

    bool m_done;

    QString m_path;

public:
    explicit FileUploadNotification(QObject *parent = nullptr);
    Q_PROPERTY(QString filename READ filename WRITE setFilename NOTIFY filenameChanged)
    Q_PROPERTY(quint64 size READ size WRITE setSize NOTIFY sizeChanged)
    Q_PROPERTY(double progress READ progress WRITE setProgress NOTIFY progressChanged)
    Q_PROPERTY(bool done READ done WRITE setDone NOTIFY doneChanged)
    Q_PROPERTY(QString path READ path WRITE setPath NOTIFY pathChanged)

    /** @property FileUploadNotification::filename
     *  @brief Filename of the file being uploaded/downloaded
     *  @default ""
    **/
    QString filename() const;

    /** @property FileUploadNotification::size
     *  @brief Size of the file being uploaded/downloaded in bytes
     *  @default 0
    **/
    quint64 size() const;

    /** @property FileUploadNotification::progress
     *  @brief Progress of current upload/download/fw update
     *  @default 0.0
    **/
    double progress() const;

    /** @property FileUploadNotification::done
     *  @brief State of the current operation
     *  Progress updates are periodic, meaning that it
     *  does not necessarily reach 100%, this property reliably
     *  determines whether the operation has finished.
     *
     *  false - still in progress, true - finished
     *  @default false
    **/
    bool done() const;

    /** @property FileUploadNotification::path
     * @brief Path to the local file
     * @default ""
     */
    QString path() const
    {
        return m_path;
    }

signals:

    void filenameChanged(QString filename);

    void sizeChanged(quint64 size);

    void progressChanged(double progress);

    void doneChanged(bool done);

    void pathChanged(QString path);

public slots:
    void setFilename(QString filename);
    void setSize(quint64 size);
    void setProgress(double progress);
    void setDone(bool done);
    void setPath(QString path)
    {
        if (m_path == path)
            return;

        m_path = path;
        emit pathChanged(m_path);
    }
};

#endif // FILEUPLOAD_H
