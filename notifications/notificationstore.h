/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NOTIFICATIONSTORE_H
#define NOTIFICATIONSTORE_H

#include <QObject>
#include "notifications/notification.h"

/** @brief Stores received notification
 *  Stores notification until they are removed.
 *  Notification are refered to by their index, but method to find notifications
 *  by their handle is also available.
 *
 *  Handle is the unique identifier of the notification while its index
 *  in the container may possibly change. It is used to refer to existing
 *  notifications, update them and possibly get feedback from them(todo).
 *
 *  Instance emmits signals when notification is added, modified or removed.
**/
class NotificationStore : public QObject
{
    Q_OBJECT
public:

    Q_PROPERTY(QList<Notification*> data READ data)
    Q_PROPERTY(Notification * top WRITE setTop READ top NOTIFY topChanged)
    Q_PROPERTY(int count READ count NOTIFY countChanged)

    explicit NotificationStore(QObject *parent = nullptr);

    /** @Access data by index
        If you want to modify an item, use the changeNotification method
    */
    const Notification & at(int index) const;

    /** Translates handle into index
        @return -1 on error, index otherwise
    */
    int indexByHandle(int handle) const;

    /** Translates index into handle
        @return -1 on error, handle otherwise
    */
    int handleByIndex(int idx) const;

    /** @property The top item in the container or nullptr
     *  Read-only, updated on insert/remove.
    **/
    Notification * top() const;
    void setTop(Notification * notification);

    /// Return index of the first item of this type, or -1 if no such item exists
    int indexOf(Notification::NotificationType type);

signals:
    void preAddNotification(int idx);
    void postAddNotification();

    void preChangeNotification(int idx);
    void postChangeNotification();

    void preRemoveNotification(int idx);
    void postRemoveNotification();

    /// Passes to the model and is re-emited from there
    void dataChanged(int idx, QVector<int> roles);

    void topChanged(Notification * notification);
    void countChanged(int count);



public slots:

    /** @brief Add notification into the storage
     *  Notification will be appended to the internal conteiner.
     *  @param notification data to store
     *  @return handle of the notification to be used for future reference
     *
     *  NotificationStore will take ownership of the inserted notification
     *  and will delete it when it's removed.
     **/
    int addNotification(Notification *notification);

    /** @brief Change content of a notification
     *  @param idx index of the notification to be modified
     *  @param notification new content of the notification
     *  @return true for success, false if there is no notification with handle
     *
     *  Relies on the fact, that the roleString(determined by roleNames() method)
     *  returns is the same as the property name.
     *
     *  If the property "done" is set, progress is automatically set to 1.0. This
     *  avoids possible confusion of the user if the progress does not reach
     *  hundred percent.
     */
    bool changeNotification(int idx, const QString &name, const QVariant &value);

    /** @brief Remove notification
     *  @param handle of the notification to be removed
     *  @return true on success, false if there is no notification with handle
     */
    bool removeNotification(int idx);

    /** Getter for notifications */
    QList<Notification *> data() const;

    /** The number of stored notifications */
    int count() const;



private:

    /** @brief Generates unique number to serve as a handle
        Handle is always a positive integer, a negative value is used to signify an error.
    **/
    int createHandle();

    /// Notifications are stored here
    QList<Notification*> m_data;

    Notification * m_top;
};

#endif // NOTIFICATIONSTORE_H
