/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "notificationstore.h"
#include <QDate>
#include <QDateTime>
#include <QDebug>
#include <QMetaProperty>

NotificationStore::NotificationStore(QObject *parent) : QObject(parent), m_top(nullptr)
{

}

const Notification &NotificationStore::at(int index) const
{
    Q_ASSERT(index >= 0 && index < data().count());
    return * data().at(index);
}

int NotificationStore::indexByHandle(int handle) const {
    for(int i = 0; i < count(); ++i) {
        if(data().at(i)->handle() == handle) return i;
    }
    qDebug() << "NotificationStore::indexByHandle(" << handle << ") not found";
    return -1;
}

int NotificationStore::handleByIndex(int idx) const
{
    if(idx >= 0 && idx < count()) return at(idx).handle();
    else {
        qDebug() << "NotificationStore::handleByIndex(" << idx << ") not found";
        return -1;
    }
}

Notification *NotificationStore::top() const
{
    return m_top;
}

void NotificationStore::setTop(Notification *notification)
{
    if(m_top != notification) {
        m_top = notification;
        emit topChanged(m_top);
    }
}

int NotificationStore::indexOf(Notification::NotificationType type) {
    int idx = 0;
    foreach(Notification * notif, m_data) {
        if(notif->type() == type) return idx;
        ++ idx;
    }
    return -1;
}

bool NotificationStore::removeNotification(int idx)
{

    if(idx >= 0 && idx < count()) {
        if(idx == 0) {
            if(count() == 1) setTop(nullptr); // If the container is going to be emptied, set the top to nullptr
            else {
                setTop(m_data.at(1)); // If there are still some notifications, set the second one as the top before removing the first
            }
        }

        emit preRemoveNotification(idx);
        Notification * tmp = m_data.at(idx);
        m_data.removeAt(idx);
        delete tmp;
        emit postRemoveNotification();
        emit countChanged(m_data.count());
        return true;
    }
    else {
        return false;
    }
}

int NotificationStore::addNotification(Notification * notification)
{
    emit preAddNotification(0); // Append
    notification->setParent(this);
    notification->setHandle(createHandle());
    notification->setTimestamp(QDateTime::currentDateTimeUtc().toSecsSinceEpoch());
    qDebug() << "New " << *notification;
    m_data.push_front(notification);
    emit postAddNotification();
    setTop(m_data.at(0));

    emit countChanged(m_data.count());
    return notification->handle();
}

bool NotificationStore::changeNotification(int idx, const QString &name, const QVariant &value)
{
    QByteArray _name = QByteArray::fromStdString(name.toStdString());
    if(idx < 0 || idx >= count()) {
        return false;
    }

    Notification * modifiedObject = m_data[idx];
    bool propertyExisted = modifiedObject->setProperty(_name, value);
    if(! propertyExisted){
        qWarning() << "Modified nonexistent property" << name << ":" << value << "of notification:" << *modifiedObject;
        return false;
    }

    // Only some properties have assigned roles in the model, emit dataChanged signal only for those.
    if(Notification::roleNames().values().contains(_name)) {
        emit dataChanged(idx, QVector<int>::fromList(Notification::roleNames().keys(_name)));
    }

    return true;
}

QList<Notification *> NotificationStore::data() const
{
    return m_data;
}

int NotificationStore::count() const { return data().count(); }

int NotificationStore::createHandle() {
    static int handle = 10000; // Helps with debugging
    ++handle;
    handle = handle < 0 ? 10000 : handle; // Reset on overflow
    return handle;
}
