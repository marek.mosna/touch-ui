/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NOTIFICATIONDBUS_H
#define NOTIFICATIONDBUS_H

#include <QObject>
#include <QDBusVariant>
#include "notificationstore.h"

class NotificationDBusAdaptor;
/** @brief The middle-man between the DBus interface and NotificationStore */
class NotificationDBus : public QObject
{
    Q_OBJECT
public:
    explicit NotificationDBus(NotificationStore * store, QObject *parent = nullptr);
    /** @brief Service needs to be registered with the DBus to work correctly
     *  Call this method before use.
     *  Both service and object path are derived from the interface name
     *  written in the .xml specification.
    **/
    bool registerDBusService();


    void test();

signals:

public slots:
    /** @brief Add new file download notification
     *  @param filename Name of the file being downloaded
     *  @param progress Progress of the download, double in range <0.0, 1.0>
     *  @param size Size of the file in bytes
     *  @param path Path to the local file
     *  @return Handle of the new notification
    **/
    int addFileDownloadNotification(const QString &filename, double progress, qulonglong size, QString path = QStringLiteral(""));

    /** @brief Add new file upload notification
     *  @param filename Name of the file being uploaded
     *  @param progress Progress of the download, double in range <0.0, 1.0>
     *  @param size Size of the file in bytes
     *  @param path Path to the local file
     *  @return Handle of the new notification
    **/
    int addFileUploadNotification(const QString &filename, double progress, qulonglong size, QString path /*= QStringLiteral("")*/);

    /** @brief Method to add notification in a generic way
     *  Error on the notification creation will be signaled by returning negative handle.
     *  @param notificatinonType One of the Notification::NotificationType enum
     *  @param parameters Parameters as "key" -> "value"
     *  @return Valid handle, or a negative integer
    **/
    int addGenericNotification(int notificationType, const QVariantMap &parameters);

    /** @brief Create new notification about an available software update
     * Error on the notification creation will be signaled by returning negative handle.
     *  @param bundlePath Path to the update file on the local filesystem(will only be valid
     *      when the update bundle is downloaded)
     *  @param currentVersion Current version of the SW as a string
     *  @param nextVersion Version of the available SW update as a string
     *  @param url URL of update (remote server)
     *  @param digest Digest of the update bundle
     *  @param date Date of the update bundle creation
     *  @param releaseNotes Release notes of the new version of the SW
     *  @return Valid handle, or a negative integer(error)
     **/
    int addSwUpdateAvailableNotification(QString bundlePath,
                                         QString currentVersion,
                                         QString nextVersion,
                                         QString url,
                                         QByteArray digest,
                                         qlonglong date,
                                         QString releaseNotes);

    /** Modify arbitary parameter of the notification */
    bool modify(int handle, const QString &name, const QDBusVariant &value);

    /** @brief Remove an existing notification
     *  @param handle
     *  @return true on success, false if notification could not be deleted(does not exist)
    **/
    bool removeNotification(int handle);
protected:
    /** Generated adaptor object for the notification DBus service */
    NotificationDBusAdaptor * m_adaptor;

    /** Pointer to NotificationStore object, that is holding the list of current notifications */
    NotificationStore * m_store;
};

#endif // NOTIFICATIONDBUS_H
