/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QDate>
#include <QDBusVariant>
#include <QVariant>
#include <QDBusConnection>
#include <QMetaObject>
#include <QMetaClassInfo>
#include "notificationdbus.h"
#include "notificationdbus_adaptor.h"

#include "fileupload_notification.h"
#include "filedownload_notification.h"
#include "update_notification.h"

NotificationDBus::NotificationDBus(NotificationStore * store, QObject *parent) :
    QObject(parent),
    m_adaptor(new NotificationDBusAdaptor(this)),
    m_store(store)
{
}

bool NotificationDBus::registerDBusService()
{
    QString dbusInterfaceName = "";
    QString dbusObjectName = "";

    // Find the DBus interface name set in the adapter and use it to register the service
    // on the SystemBus.
    const QMetaObject * adaptorMeta = m_adaptor->metaObject();
    for(int i = 0; i < adaptorMeta->classInfoCount(); ++i ){
        const QMetaClassInfo  adaptorInfo = adaptorMeta->classInfo(i);
        if(QString(adaptorInfo.name()) == QStringLiteral("D-Bus Interface")) {
            dbusInterfaceName = adaptorInfo.value();
            break;
        }
    }

    // Derive object name from interface name
    dbusObjectName = QStringLiteral("/") + dbusInterfaceName;
    dbusObjectName.replace(".", "/", Qt::CaseSensitive);

    Q_ASSERT_X(dbusInterfaceName != "", "bool NotificationDBus::registerDBusService()", "DBus adaptor does not have the interface name set!");
    Q_ASSERT(dbusObjectName.length() > 1); // Make sure that it's not empty(and does not contain just a /)

    bool r =  QDBusConnection::systemBus().registerObject(dbusObjectName, m_adaptor, QDBusConnection::ExportAllContents);
    bool r1 = QDBusConnection::systemBus().registerService(dbusInterfaceName);
    if( ! r || ! r1) {
        qWarning() << "NotificationDBus::registerDBusService(): Could not register notification service on DBus, notifications will not work!";
        qWarning() <<  QDBusConnection::systemBus().lastError().message();
        return false;
    }
    else return true;
}

void NotificationDBus::test()
{
    // Will test the notification-manipulation methods, not the actual DBus interface
    bool r = false;
    int handle = -10000;

    Q_ASSERT_X(m_store->count() == 0, "NotificationDBus::test()", "For the test to run, store must be empty!");

    handle = addFileUploadNotification("testfile.txt", 0.1, 1024, "/var/projects/testfile.txt");
    Q_ASSERT(handle == 10001);
    Q_ASSERT(m_store->count() == 1);
    Q_ASSERT(m_store->at(0).type() == Notification::FileUpload);
    Q_ASSERT(m_store->at(0).property("filename").toString() == "testfile.txt");
    Q_ASSERT( qFuzzyCompare(m_store->at(0).property("progress").toDouble(), 0.1));
    Q_ASSERT( m_store->at(0).property("size").toInt() == 1024);
    Q_ASSERT(m_store->at(0).property("done").toBool() == false); // Default value


    handle = addFileDownloadNotification("downFile.txt", 0.3, 256, "/var/projects/downFile.txt");
    Q_ASSERT(handle == 10002);
    Q_ASSERT(m_store->count() == 2);
    Q_ASSERT(m_store->at(1).type() == Notification::FileDownload);
    Q_ASSERT(m_store->at(1).property("filename").toString() == "downFile.txt");
    Q_ASSERT( qFuzzyCompare(m_store->at(1).property("progress").toDouble(), 0.3));
    Q_ASSERT( m_store->at(1).property("size").toInt() == 256);
    Q_ASSERT(m_store->at(1).property("done").toBool() == false); // Default value

    // Try modifying an already created notification
    QDBusVariant dbusvar;
    dbusvar.setVariant( QVariant(0.9));
    r = modify(handle, "progress", dbusvar);
    Q_ASSERT(r);
    Q_ASSERT( qFuzzyCompare(m_store->at(1).property("progress").toDouble(), 0.9));

    // Try removing notifications
    r = removeNotification(handle);
    Q_ASSERT(r);
    Q_ASSERT(m_store->count() == 1);
    r = removeNotification(handle -1);
    Q_ASSERT(r);
    Q_ASSERT(m_store->count() == 0);

    // This must softly fail
    r = modify(handle, "progress", dbusvar);
    Q_ASSERT(! r);

    // Parameters argument
    QVariantMap parameters;
    parameters["filename"] = "somefile.txt";
    parameters["progress"] = 0.42;

    // Test generic notification
    handle = addGenericNotification(Notification::FileDownload, parameters);
    Q_ASSERT(handle == 10003); // Growing sequence
    Q_ASSERT(m_store->count() == 1);
    Q_ASSERT(m_store->at(0).property("filename").toString() == "somefile.txt");
    Q_ASSERT( qFuzzyCompare(m_store->at(0).property("progress").toDouble(), 0.42));

    // This must softly fail
    parameters["InvalidOnPurpose"] = 999;
    handle = addGenericNotification(Notification::FileDownload, parameters);
    Q_ASSERT(handle == -1); // This must fail

    // Try modifying an invalid property
    dbusvar.setVariant(QVariant::fromValue(0.666));
    r = modify(10003, "InvalidOnPurpose", dbusvar);
    Q_ASSERT(!r);

    // Try modifying a property with invalid value
    QDBusVariant dbusvar2;
    dbusvar2.setVariant(QVariant::fromValue(QStringLiteral("InvalidValueForThisProperty")));
    r = modify(10003, "progress", dbusvar2);
    Q_ASSERT(!r);


    // Remove the last "general" notification
    r = removeNotification(10003);
    Q_ASSERT(r);
    Q_ASSERT(m_store->count() == 0);

}



int NotificationDBus::addFileDownloadNotification(const QString &filename, double progress, qulonglong size, QString path)
{
    auto notification = new FileDownloadNotification();
    notification->setType(Notification::FileDownload);
    notification->setFilename(filename);
    notification->setProgress(progress);
    notification->setSize(size);
    notification->setPath(path);
    return m_store->addNotification(notification);
}

int NotificationDBus::addFileUploadNotification(const QString &filename, double progress, qulonglong size, QString path)
{
    auto notification = new FileUploadNotification();
    notification->setType(Notification::FileUpload);
    notification->setFilename(filename);
    notification->setProgress(progress);
    notification->setSize(size);
    notification->setPath(path);
    return m_store->addNotification(notification);
}

int NotificationDBus::addGenericNotification(int notificationType, const QVariantMap &parameters)
{
    if(notificationType >= 0 && notificationType < Notification::NotificationTypeCount) {
        Notification * notification = new Notification();
        int originalPropertyCount = notification->metaObject()->propertyCount();
        notification->setType(Notification::NotificationType(notificationType));
        foreach(QString name, parameters.keys()) {
            // Try setting the property(name), it will fail if the new value/type is not appropriate for it(or nonexistent property)
            if(! notification->setProperty(name.toStdString().c_str(), parameters[name])) {
                qWarning() << "NotificationDBus::addGenericNotification: Invalid property name(" << name << ") or value(" << parameters[name] << ").";
                delete notification;
                return -1;
            }
        }

        // Make absolutely sure that no new property has been defined
        if(notification->metaObject()->propertyCount() != originalPropertyCount) {
            qWarning() << "NotificationDBus::addGenericNotification: Attempt to add new property, not supported!";
            delete notification;
            return -1;
        }

        return m_store->addNotification(notification);
    }
    else return -1;

}

int NotificationDBus::addSwUpdateAvailableNotification(
        QString bundlePath,
        QString currentVersion,
        QString nextVersion,
        QString url,
        QByteArray digest,
        qlonglong date,
        QString releaseNotes)
{

    UpdateNotification * notification = new UpdateNotification();
    notification->setBundlePath(bundlePath);
    notification->setCurrentVersion(currentVersion);
    notification->setNextVersion(nextVersion);
    notification->setUrl(url);
    notification->setDigest(digest);
    notification->setDate(QDateTime::fromSecsSinceEpoch(date));
    notification->setReleaseNotes(releaseNotes);

    // Only one update notification should be present at a time
    int existingUpdateNotificationIndex = m_store->indexOf(Notification::UpdateNotification);
    if(existingUpdateNotificationIndex != -1) m_store->removeNotification(existingUpdateNotificationIndex);

    return m_store->addNotification(notification);
}

bool NotificationDBus::modify(int handle, const QString &name, const QDBusVariant &value)
{
    QVariant transformedValue = value.variant();

    // Some properties need type conversion
    if(name == "errorCode") {
        if( transformedValue.userType() == QMetaType::Double ){
            double doubleValue = transformedValue.value<double>();
            int intValue = static_cast<int>(doubleValue);
            transformedValue = QVariant::fromValue(ErrorsSL1::Errors(intValue));
        }
        else if( transformedValue.userType() == QMetaType::Int ){
            transformedValue = QVariant::fromValue(transformedValue.value<ErrorsSL1::Errors>());
        }
        else if(transformedValue.userType() == QMetaType::QString) {
            transformedValue = QVariant::fromValue(ErrorsSL1::static_fromStringCode(transformedValue.toString()));
        }
    }

    int idx = m_store->indexByHandle(handle);
    if(idx >= 0) {
        return m_store->changeNotification(idx, name, transformedValue);
    }
    return false;
}

bool NotificationDBus::removeNotification(int handle)
{
    int idx = m_store->indexByHandle(handle);
    if(idx >= 0) {
        return m_store->removeNotification(idx);
    } else return false;
}
