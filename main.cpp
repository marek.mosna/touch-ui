/*
    Copyright 2019-2020, Prusa Research s.r.o.
    Copyright 2021, Prusa Research a.s.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QDebug>
#include <QTranslator>
#include <QFile>
#include <QProcessEnvironment>
#include <QtDBus>
#include <QtCore/QProcess>
#include <QtDBus/QtDBus>
#include <QtCore/QObject>
#include <QtDBus/QDBusInterface>
#include <QQmlContext>
#include <QObject>
#include <QtDBus/QDBusConnection>

#ifdef BUILD_NETWORKING
  #include "wifi.h"
  #include "networksettingmodel.h"
  #include "wifinetworkmodel.h"
  #include "ethernet.h"
  #include "secretprusa.h"
#endif // BUILD_NETWORKING

#ifdef BUILD_LICENSE
  #include "license/licensemodel.h"
#endif // BUILD_LICENSE

#ifdef BUILD_LOCALIZATION
  #include "localesetting.h"
  #include "translator.h"
#endif // BUILD_LOCALIZATION

#include "updater.h"
#include "rauc.h"
#include "custom_types.h"

#include "notificationmodel.h"
#include "notificationdbus.h"
#include "notification.h"

#include "printer.h"
#include "exposure.h"
#include "config.h"
#include "connect.h"
#include "examples.h"

#include "backlight.h"
#include "hostname.h"
#include "timedate.h"
#include "nativefunctions.h"
#include "wizard.h"
#include "logs.h"
#include "dbusutils.h"
#include "errors_sl1.h"
#include "admin.h"

#ifdef BUILD_FILEMANAGER
  #include "filemanager.h"
  #include "fmdata.h"
#endif // BUILD_FILEMANAGER

#include <gitversion.h> // Generated at build-time

#ifdef QT_DEBUG
#include <QQmlDebuggingEnabler>
#endif // QT_DEBUG


static QJSValue appVersionSingletonProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    QJSValue appInfo = scriptEngine->newObject();
    appInfo.setProperty("version", GIT_REVISION); // From gitversion.h

    QJSValue buildYear = scriptEngine->newObject();
    QJSValue buildMonth = scriptEngine->newObject();
    QJSValue buildDay = scriptEngine->newObject();
    QStringList buildDate = QStringLiteral(__DATE__).simplified().split(" ");
    Q_ASSERT(buildDate.length() == 3);
    appInfo.setProperty("buildYear", buildDate[2]);
    appInfo.setProperty("buildMonth", buildDate[0]);
    appInfo.setProperty("buildDay", buildDate[1]);
    return appInfo;
}

int main(int argc, char *argv[])
{
    Q_INIT_RESOURCE(errorcodes);
    Q_INIT_RESOURCE(PrusaComponents);
    Q_INIT_RESOURCE(pictures);

    qDebug() << "Last commit: " << GIT_REVISION;
    qputenv("QT_IM_MODULE", QByteArray("qtvirtualkeyboard"));

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);
    app.setOrganizationName("Prusa Research");
    app.setOrganizationDomain("prusa3d.cz");
    app.setApplicationName("SL1 Touch UI");

    QSettings::setPath(QSettings::NativeFormat, QSettings::Scope::SystemScope, QProcessEnvironment::systemEnvironment().value("CONFIGURATION_DIRECTORY", "/etc/touch-ui"));
    QSettings settings(QSettings::Scope::SystemScope, "touch-ui");

    QLoggingCategory::setFilterRules("*.trajectory=true\n");

    qmlRegisterSingletonType(QUrl("qrc:/Defines.qml"), "cz.prusa3d.sl1.defines", 1, 0, "Defines");
    qmlRegisterSingletonType(QUrl("qrc:/ErrorcodesText.qml"), "cz.prusa3d.sl1.errorcodestext", 1, 0, "ErrorcodesText");

    ErrorsSL1::registerTypes();
    qmlRegisterSingletonType<ErrorsSL1>("ErrorsSL1",  1, 0, "ErrorsSL1",[](QQmlEngine * engine, QJSEngine * jsengine) {
        Q_UNUSED(jsengine);
        return qobject_cast<QObject*>(new ErrorsSL1(engine));
    });


    NotificationModel notificationModel;
    NotificationDBus notificationDBus(notificationModel.notificationStore());

    // Register the notification service on DBus
    notificationDBus.registerDBusService();

    qmlRegisterSingletonType("Native", 1, 0, "AppInfo", appVersionSingletonProvider);
    qmlRegisterUncreatableType<Notification>("Notification", 1, 0, "Notification", "Notification, if required will be provided by special notificationModel.");


#ifdef BUILD_NETWORKING
    qmlRegisterUncreatableType<NetworkSettingModel>("Native", 1, 0, "NetworkSettingModel", "There is no possible reason for QML to create this type of model");
    qmlRegisterUncreatableType<WifiNetworkModel>("Native", 1, 0, "WifiNetworkModel", "No reason to instantiate it from QML, just need the enums");
    qmlRegisterUncreatableType<Ethernet>("Native", 1, 0, "Ethernet", "No reason to instantiate it from QML, just need the enums");
#endif // BUILD_NETWORKING

    QQmlApplicationEngine engine;
    engine.addImportPath(":/.");
    engine.rootContext()->setContextProperty("configPath", settings.fileName());

#ifdef BUILD_FILEMANAGER
    FMData::qmlRegister(engine);
#endif // BUILD_FILEMANAGER

    // Make notifications accessible to the qml
    engine.rootContext()->setContextProperty("notificationModel", &notificationModel);

    Printer printer(engine, notificationModel, &engine);
    printer.setObjectName("printer0");
    engine.rootContext()->setContextProperty("printer0", &printer);
    qmlRegisterUncreatableType<Printer>("cz.prusa3d.sl1.printer0", 1, 0, "Printer0", "For enum access only");
    qmlRegisterUncreatableType<Exposure>("Native", 1, 0, "Exposure", "Valid exposure objects are returned from Print object methods");
    qmlRegisterUncreatableType<ChecksModel>("Native", 1, 0, "ChecksModel", "Valid ChecksModel is only ever exposed from C++(Exposure)");

    Config printerConfig(engine, &engine);
    printerConfig.setObjectName("printerConfig0");
    engine.rootContext()->setContextProperty("printerConfig0", &printerConfig);

    // TODO: update connect api
    // PrusaConnect prusaConnect(engine, &engine);
    // prusaConnect.setObjectName("connect0");
    // engine.rootContext()->setContextProperty("connect0", &prusaConnect);

    qmlRegisterUncreatableType<Examples>("cz.prusa3d.sl1.examples0", 1, 0, "Examples0", "For enum access only");

#ifdef BUILD_LOCALIZATION
    Q_INIT_RESOURCE(translations);
    // Object handling locale changes - changing the QTranslator
    // Has properties and methods exposed to QML.
    LocaleSetting * localeSetting = new LocaleSetting(engine, &app);
    Translator * translator = new Translator(app, engine, &app);
    qmlRegisterSingletonInstance<Translator>("Native", 1, 0, "Translator", translator);
    qmlRegisterSingletonInstance<LocaleSetting>("Native", 1, 0, "LocaleSetting", localeSetting);
    localeSetting->connect(localeSetting, &LocaleSetting::localeChanged, translator, &Translator::setLanguageByLocale);
#endif // BUILD_LOCALIZATION

#ifdef BUILD_NETWORKING
    // NetworkManager wrapper init
    WifiNetworkModel wifiNetworkModel(&app);
    engine.rootContext()->setContextProperty("wifiNetworkModel", &wifiNetworkModel);

    Ethernet ethernet(&app);
    engine.rootContext()->setContextProperty("ethernet", &ethernet);

    // Make engine accessible for Wifi so it can create more complicated QJSValue objects
    // and register wifiObject to the engine
    Wifi wifiObject(&engine, &app);
    engine.rootContext()->setContextProperty("Wifi", &wifiObject);
    QObject::connect(&wifiObject, &Wifi::connectionsChanged, &wifiNetworkModel, &WifiNetworkModel::slotReloadConnections);

    SecretPrusaRepresentant::Ptr secretPrusa = SecretPrusaRepresentant::Ptr(new SecretPrusaRepresentant(SecretPrusa::Ptr(new SecretPrusa(nullptr))));
    engine.rootContext()->setContextProperty("SecretPrusa", secretPrusa.data());
#endif // BUILD_NETWORKING



    Updater updater(engine, &engine);
    updater.setObjectName("updater");
    engine.rootContext()->setContextProperty("updater", &updater);
    qmlRegisterUncreatableType<Updater>("cz.prusa3d.updater", 1, 0, "Updater", "Just for enums");
    QObject::connect(&updater, &Updater::updateArrived, &notificationDBus, &NotificationDBus::addSwUpdateAvailableNotification);

    Rauc rauc(engine, &engine);
    rauc.setObjectName("rauc");
    engine.rootContext()->setContextProperty("rauc", &rauc);
    qmlRegisterUncreatableType<Rauc>("cz.prusa3d.rauc", 1, 0, "Rauc", "Just for enums");

    Backlight backlight(&engine);
    backlight.setObjectName("backlight");
    engine.rootContext()->setContextProperty("backlight", &backlight);
    qmlRegisterType<Backlight>("cz.prusa3d.backlight", 1, 0, "Backlight");

    Wizard wizard(engine, &engine);
    QObject::connect(&printer, &Printer::stateChanged, &wizard, [&wizard](Printer::PrinterState state){
        if(state != Printer::WIZARD) wizard.resetWizardChecksModel();
    });
    wizard.setObjectName("wizard0");
    engine.rootContext()->setContextProperty("wizard0", &wizard);
    qmlRegisterUncreatableType<Wizard>("cz.prusa3d.wizard0", 1, 0, "Wizard0", "Valid Wizard is only ever exposed from C++(Printer)");
    qmlRegisterUncreatableType<WizardChecksModel>("cz.prusa3d.wizardcheckmodel", 1, 0, "WizardChecksModel", "Valid WizardChecksModel is only ever exposed from C++(Wizard)");

    Logs logs(engine, &engine);
    logs.setObjectName("logs0");
    engine.rootContext()->setContextProperty("logs0", &logs);
    qmlRegisterUncreatableType<Logs>("cz.prusa3d.logs0", 1, 0, "Logs0", "Valid Logs is only ever exposed from C++(Printer)");

	Admin admin(&engine);
	engine.rootContext()->setContextProperty("adminApi", &admin);
	qmlRegisterType<Admin>("cz.prusa3d.sl1.admin", 1, 0, "AdminAPI");
    qmlRegisterType<ActionItem>("cz.prusa3d.wizard0.item.action", 1, 0, "AdminActionItem");
    qmlRegisterType<IntValueItem>("cz.prusa3d.wizard0.item.value.int", 1, 0, "AdminIntValueItem");
    qmlRegisterType<FixedValueItem>("cz.prusa3d.wizard0.item.value.fixed", 1, 0, "AdminFixedValueItem");
    qmlRegisterType<FloatValueItem>("cz.prusa3d.wizard0.item.value.float", 1, 0, "AdminFloatValueItem");
    qmlRegisterType<BoolValueItem>("cz.prusa3d.wizard0.item.value.bool", 1, 0, "AdminBoolValueItem");
    qmlRegisterType<TextValueItem>("cz.prusa3d.wizard0.item.value.text", 1, 0, "AdminTextValueItem");
    qmlRegisterUncreatableType<AdminModel>("cz.prusa3d.sl1.admin", 1, 0, "AdminModel", "Only exposed from the Admin object");


#ifdef BUILD_LICENSE
    // An instance of the LicenseModel providing a list of packages and their licenses
    LicenseModel licenseModel;
    engine.rootContext()->setContextProperty("licenseModel", &licenseModel);
#endif // BUILD_LICENSE

    Hostname hostname(engine);
    engine.rootContext()->setContextProperty("hostname", &hostname);

    Timedate timedate(engine);
    engine.rootContext()->setContextProperty("timedate", &timedate);
    NativeFunctions nativeFunctions(engine);
    engine.rootContext()->setContextProperty("nativeFunctions", &nativeFunctions);


#ifdef QT_DEBUG
    engine.rootContext()->setContextProperty("_debug", true);
    engine.rootContext()->setContextProperty("_debug_rectangles", false);
#else
    engine.rootContext()->setContextProperty("_debug", false);
    engine.rootContext()->setContextProperty("_debug_rectangles", false);
#endif // QT_DEBUG


    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
