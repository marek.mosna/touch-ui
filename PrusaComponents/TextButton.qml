
/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Window 2.10
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import "."

Rectangle {
    id: root
    implicitWidth: 220
    implicitHeight: 60
    color: mouse.pressed ? Theme.color.highlight : background
    border.width: 3
    radius: 10
    property int textMargin: 5
    property alias text: label.text
    property string background: Theme.color.buttonBackground
    property alias showText: label.visible
    property alias textSize: label.font.pixelSize
    signal clicked()

    property string name

    property bool beepOnPressed: false
    property bool beepOnReleased: true

    function pressedBeep() {
        if(beepOnPressed) printer0.beep_button()
    }
    function releasedBeep() {
        if(beepOnReleased) printer0.beep_button()
    }

    Text {
        id: label
        visible: true
        anchors {
           centerIn: root
           margins: textMargin
        }
        horizontalAlignment: Text.AlignHCenter
         font.pixelSize: 20
    }

    MouseArea {
        id: mouse
        anchors.fill: parent
        onClicked: () => {
                       console.log(LoggingCategories.trajectory, "TextButton ", name, " clicked")
                       root.clicked()
                   }
        onPressed: () => pressedBeep()
        onReleased: () => releasedBeep()
    }
}
