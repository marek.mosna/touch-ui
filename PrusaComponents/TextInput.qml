/*
    Copyright 2021, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12 as Q
import QtQuick.Controls 2.12
import QtGraphicalEffects 1.0
import "."

/* Simple item for puting directly into columns */
Q.Item {
    id: root
    property string name: "_noname_"
    property alias value: txtValue.text
    property string label: ""
    property string regexp: ".*"
    property bool enabled: true
    signal accepted(string value)

    property alias txtValue: txtValue
    property int rightColWidth: 200
    property alias acceptableInput: txtValue.acceptableInput

    implicitWidth: Theme.sizing.defaultComponent.width
    implicitHeight: Theme.sizing.defaultComponent.height

    Text {
        id: txtLabel
        anchors {
            left: parent.left
            verticalCenter: parent.verticalCenter
        }
        font.pixelSize: Theme.font.normal
        text:  root.label
    }

    Q.TextInput {
        id: txtValue
        anchors {
            right: imgEditable.left
            rightMargin: 20
            verticalCenter: parent.verticalCenter
        }

        clip: true
        width: rightColWidth
        font.pixelSize: 28
        color: Theme.color.text
        //selectedTextColor: Theme.color.highlight
        validator: Q.RegExpValidator { regExp: new RegExp(regexp) }
        font.family: Theme.font.fontName
        enabled: root.enabled

//        onAccepted: {
//            if(acceptableInput) {
//                root.accepted(value)
//                console.log("TextInput " + root.name + " accepted \"" + value + "\"")
//            }
//            else {
//                console.log("TextInput " + root.name + " refused \"" + value + "\"")
//            }
//        }
        onEditingFinished: ()=>{
            if(acceptableInput) {
                root.accepted(value)
                console.log("TextInput " + root.name + " EditingFinished accepted \"" + value + "\"")
            }
            else {
                console.log("TextInput " + root.name + " EditingFinished refused \"" + value + "\"")
            }
        }

        onAcceptableInputChanged: ()=>{
            if(acceptableInput) {
                root.accepted(value)
            }
        }

        horizontalAlignment:  Q.TextInput.AlignRight
        onActiveFocusChanged: ()=>{
            if(activeFocus) {
                //txtValue.selectAll()
            }
        }
    }

    Q.Item {
        id: imgEditable
        height: txtValue.height
        width: visible ? height : 0
        visible: root.enabled
        anchors {
            right: parent.right
            verticalCenter: parent.verticalCenter
        }

        Q.Image {
            id: pencil
            anchors {
                fill: parent
            }
            sourceSize.width: parent.height
            sourceSize.height: parent.height
            fillMode: Q.Image.PreserveAspectFit
            source: Theme.pictogram.path + "/" + Theme.pictogram.edit
        }

        ColorOverlay {
            visible: ! txtValue.acceptableInput
            anchors.fill: pencil
            source: pencil
            color: "red"  // make image like it lays under red glass
        }

        Q.MouseArea {
            anchors.fill: parent
            onClicked: () => {
                txtValue.forceActiveFocus()
                txtValue.cursorPosition = txtValue.text.length
            }
        }
    }

    Q.Rectangle {
        color: Theme.color.separator
        height: 1
        width: parent.width
        anchors.bottom: parent.bottom
    }
}
