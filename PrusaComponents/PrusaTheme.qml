
/*
    Copyright 2021, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12

/** Please, do not put in any javascript or excessive bindings */
QtObject {
    property FontLoader _prusaFont: FontLoader {
        source: "/fonts/black_grotesk/AtlasGrotesk-Light.otf"
        onStatusChanged: {
            if(prusaFont.status === FontLoader.Ready) console.log("Prusa Font status changed: Ready");
            else if(prusaFont.status === FontLoader.Error) console.log("Prusa Font status changed: Error");
            else if(prusaFont.status === FontLoader.Loading) console.log("Prusa Font status changed: Loading");
            else if(prusaFont.status === FontLoader.Null) console.log("Prusa Font status changed: Null");
            else console.log("FontLoader - control should never reach this place")
        }
    }

    property QtObject color: QtObject {
        property color grey: defaultColors.prusaGrey
        property color red: defaultColors.prusaRed

        property color background: "black"
        property color backgroundInverse: "white"
        property color buttonBackground: defaultColors.prusaButtonBackground
        property color buttonHighlight: defaultColors.prusaOrange
        property color selection: defaultColors.prusaOrange
        property color selectionBackground: "black"
        property color highlight: defaultColors.prusaOrange

        property color text: "white"
        property color disabledText: "#dddddd"
        property color statusTextColor: "grey"
        property color placeholderText: "darkgrey"
        property color textInverse: "black"
        property color textGrey: defaultColors.prusaAlmostLightGrey


        property color errorBackground: defaultColors.prusaRed

        property color separator: defaultColors.prusaButtonBackground

        property color controlBackground: buttonBackground
        property color controlLighterBackground:  "#f0f0f0"
        property color controlDarkerBackground: "grey"
        property color controlBorder: "#e0e0e0"

        property color debug: "pink"

        property QtObject defaultColors: QtObject {
            readonly property color prusaOrange: "#ed6b21"
            readonly property color prusaRed: "#ffb8381e"
            readonly property color prusaGrey: "#313131"
            readonly property color prusaLightGrey: "#d9d9d9"
            readonly property color prusaAlmostLightGrey: "#9e9e9e"
            readonly property color exceptionBackgroundColor: "#e74626"
            readonly property color prusaButtonBackground: "#5B5B5B"
            readonly property color invalidText: "#dddddd"

            readonly property color controlBorder: "#e0e0e0"
            readonly property color controlLightBackground: "#f0f0f0"
            readonly property color controlDarkBackground: "grey"

            readonly property string test_music_file: "chromag_-_the_prophecy.xm"
        }
    }

    property QtObject font: QtObject {
        property string fontName: _prusaFont.name
        property int small: 12
        property int normal: 24
        property int labelSize: 28
        property int big: 32
        property int extraBig: 42
        property int bannerSize: 48

    }

    property QtObject pictogram: QtObject {
        property string path: "/pictograms"

        property string next: "next"
        property string prev: "prev"
        property string nextGrey: "right_grey"
        property string prevGrey: "left_grey"
        property string about_us: "about_us_color"

        property string back: "back_arrow_white"
        property string cancel: "cancel_color"
        property string cancelSimple: "cancel_small_white"
        property string cancelInverse: "cancel_small_black"


        property string edit: "edit_white"
        property string key: "key_color"
        property string save: "save_color"
        property string saveInverse: "save_color_black"
        property string uploadToCloud: "upload_cloud_color"
        property string uploadToCloudInverse: "upload_cloud_color_black"

        property string plus: "plus_color"
        property string minus: "minus_color"

        property string yes: "yes_color"
        property string no: "no_color"

        property string home: "home_small_white"
        property string play: "print_color"
        property string control: "control_color"
        property string settings: "settings_color"
        property string powerOff: "turn_off_color"
        property string powerOffInverse: "turn_off_color_black"

        property string message: "message_white"
        property string working: "circular_arrows"
        property string clock: "time_color"

        property string homePlatform: "home_platform_color"
        property string homeTank: "tank_reset_color"
        property string disableSteppers: "disable_steppers_color"

        property string wifi: "wifi_on_color"
        property string wifiSimple: "wifi_small_white"
        property string ethernet: "lan_color"
        property string ethernetSimple: "lan_small_white"
        property string accessPoint: "ap_color"
        property string accessPointSimple: "ap_small_white"
        property string noConnection: "no_connection_small_white"
        property string error: "error_white"
        property string errorSimple: "error_small_white"
        property string errorInverse: "error_black"

        property string folderSimple: "projects_small_white"
        property string deleteSimple: "delete_small_white"

        property string updateInverse: "update_black"
        property string finish: "finish_white"
        property string finishInverse: "finish_black"

        property string up: "up_arrow_white"
        property string upInverse: "up_arrow_black"
        property string down: "down_arrow_white"
        property string downInverse: "down_arrow_black"
        property string fastUp: "fast-up_color"
        property string fastDown: "fast-down_color"
        property string fastForward: "fast-forward_color"
        property string fastBackward: "fast-backward_color"
        property string moveUp: "move_up_color"
        property string moveDown: "move_down_color"

        property string exposureTime: "exposure_times_color"
        property string calibrationSimple: "calibration_small_white"
        property string calibration: "calibration_color"
        property string uvCalibration: "uv_calibration"
        property string displayTest: "display_test_color"
        property string sandclock: "sandclock_color.svg"
        property string controlSimple: "control_small_white"
        property string download: "download"
        property string finishSimple: "finish_white"
        property string firmware: "firmware-icon"
        property string firmwareInverse: "firmware-icon_black"
        property string hostname: "hostname_color"
        property string infoSimple: "info_off_small_white"
        property string language: "language_color"
        property string logs: "logs-icon"
        property string manual: "manual_color"
        property string manualSimple: "manual_small_white"
        property string movePlatform: "move_platform_color"
        property string moveResinTank: "move_resin_tank_color"
        property string network: "network-icon"
        property string platformTank: "platform-tank-icon"
        property string printSimple: "print_small_white"
        property string refill: "refill_color"
        property string sensor: "sensor-icon"
        property string settingsSimple: "settings_small_white"
        property string support: "support_color"
        property string supportSimple: "support_small_white"
        property string systemInfo: "system_info_color"

        property string timezone: "timezone_color"
        property string touchscreen: "touchscreen-icon"
        property string video: "videos_color"
        property string videoSimple: "videos_small_white"
        property string warning: "warning_white"
        property string wizard: "wizard_color"

        property string adminSimple: "admin_small_white"
        property string update: "update_color"
        property string testtube: "testtube"
        property string factory: "factory_color"
        property string search: "search"
        property string reprint:  "reprint_color"

        property string screensaverTimeout: "screensaver_timeout_color"
        property string brightness: "brightness_color"
        property string wifiQuestion: "wifi_question_grey"
        property string wifiOff: "wifi_off_grey"
        property string nok: "nok_color"
        property string ok: "ok_color"
        property string cover: "cover_color"
        property string fan: "fan_color"
        property string busy: "running_color"
        property string waiting: "waiting_color"
        property string justPlus: "just_plus"
        property string limit: "limit_color"
        property string thumbnailPlaceholder: "thumbnail_placeholder"
        property string disabled: "disabled_color"
        property string handQr: "hand_qr"
        property string usb: "usb_color"
        property string towerSensitivity: "tower_sensitivity_color"
        property string tiltSensitivity: "tilt_sensitivity_color"
        property string towerOffset: "tower_offset_color"
        property string swipeRight: "swipe_right"
        property string swipeRightSimple: "swipe_right_white"
        property string wizardTankSurfaceCleaner: "clean-tank-icon"

        property var wifiPictogramsLight: [
            "wifi_strength_0",
            "wifi_strength_1_light",
            "wifi_strength_2_light",
            "wifi_strength_3_light",
        ]

        property var wifiPictogramsColor: [
            "wifi_strength_0",
            "wifi_strength_1",
            "wifi_strength_2",
            "wifi_strength_3",
        ]
    }

    property QtObject sound: QtObject {
        property string path: printer0.multimediaSounds
        property string testFile: "chromag_-_the_prophecy.xm"
    }

    property QtObject sizing: QtObject {
        property size defaultComponent: Qt.size(200, 60)
    }

    Component.onCompleted: () => console.log("PrusaTheme created.")
}
