/*
    Copyright 2019-2020, Prusa Research s.r.o.
              2021, Prusa Research a.s.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import PrusaComponents 1.0

Item {
    id: root
    property bool checked: false
    property string name
    property bool clickSwitch: true // Switch on click
    property bool enabled: true
    property int fontPixelSize: 28
    signal changed(bool state)
    signal changedOn()
    signal changedOff()
    signal clicked()
    signal clickSwitched() // Only when switch is caused by user clicking the control

    height: content.height
    width: content.width
    states: [
        State {
            name: ""
            when: ! root.checked
            AnchorChanges {
                target: knob

                anchors.right: undefined
                anchors.left: sw.left
            }
            PropertyChanges {
                target: offText
                opacity: 1.0
            }
            PropertyChanges {
                target: onText
                opacity: 0.0

            }
        },
        State {
            name: "checked"
            when: root.checked
            AnchorChanges {
                target: knob
                anchors.left: undefined
                anchors.right: sw.right
            }
            PropertyChanges {
                target: offText
                opacity: 0.0
            }
            PropertyChanges {
                target: onText
                opacity: 1.0

            }
        }
    ]
    transitions: [
        Transition {
            from: ""
            to: "checked"
            reversible: true
            AnchorAnimation { duration: 500 ; easing.type: Easing.InOutQuad}
            NumberAnimation {
                target: onText
                properties: "opacity"
                duration: 300
                easing.type: Easing.InOutQuad
            }
            NumberAnimation {
                target: offText
                properties: "opacity"
                duration: 300
                easing.type: Easing.InOutQuad
            }

        }
    ]

    onCheckedChanged: {
        console.log(LoggingCategories.trajectory, "Switch \"" + name + "\" changed to " + checked)
    }

    RowLayout {
        id: content
        anchors.centerIn: parent
        spacing: 5

        Text {
            id: offText;
            text: qsTr("Off");
            anchors.margins: 5;
            color: "white";
            opacity: 1.0
            Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
            font.pixelSize: fontPixelSize
        }

        Rectangle {
            id: sw
            width: 80
            height: 40
            radius: 10
            color: "black"

            Rectangle {
                id: darkArea
                height: 20
                radius: 10
                anchors {
                    verticalCenter: parent.verticalCenter
                    right: parent.right
                    left: knob.horizontalCenter
                }
                color: Theme.color.controlDarkerBackground
                z:7
            }

            Rectangle {
                id: knob
                width: 40
                height: 40
                radius: 20
                border.width: 1
                border.color: Theme.color.controlBorder
                anchors {
                    verticalCenter: sw.verticalCenter
                    left: sw.left
                }
                color: "white"
                z: 10
            }

            Rectangle {
                id: lightArea
                radius: 10
                height: 20
                anchors {
                    verticalCenter: parent.verticalCenter
                    left: parent.left
                    right: knob.horizontalCenter
                }
                color: Theme.color.controlLighterBackground
                z:7
            }
        }

        Text {
            id: onText;
            text: qsTr("On");
            anchors.margins: 5;
            color: "white";
            opacity: 0.0
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            font.pixelSize: fontPixelSize
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: () => {
            if(root.enabled) {
                printer0.beep_button()
                root.forceActiveFocus()
                root.clicked()
                if(clickSwitch) {
                    root.checked = !root.checked
                    root.changed(root.checked)
                    if(root.checked) { root.changedOn(); } else { root.changedOff(); }
                    root.clickSwitched()
                }
            }
        }
    }
}
