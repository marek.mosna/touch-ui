
import QtQuick 2.12
import QtQml 2.12

pragma Singleton
Item {
    property alias trajectory: trajectory
    property alias debug: debug

    LoggingCategory {
        id: trajectory
        name: ".trajectory"
        defaultLogLevel: LoggingCategory.Info
    }

    LoggingCategory {
        id: debug
        name: ".debug"
        defaultLogLevel: LoggingCategory.Debug
    }
}
