import QtQuick 2.12
import QtQuick.Window 2.2
import "."

Item {
    id: root
    property int frameCounter: 0
    property int frameCounterAvg: 0
    property int counter: 0
    property int fps: 0
    property int fpsAvg: 0


    implicitWidth:  childrenRect.width + 10
    implicitHeight: spinnerImage.height + 10

    Image {
        id: spinnerImage
        anchors.verticalCenter: parent.verticalCenter
        x: 4
        width: 28
        height: width
        source: Theme.pictogram.path + "/" + Theme.pictogram.busy
        NumberAnimation on rotation {
            running: root.visible
            from:0
            to: 360
            duration: 800
            loops: Animation.Infinite
        }
        onRotationChanged: frameCounter++;
    }

    Text {
        anchors.left: spinnerImage.right
        anchors.leftMargin: 8
        anchors.verticalCenter: spinnerImage.verticalCenter
        text: "Ø " + root.fpsAvg + " | " + root.fps + " fps"
    }

    Timer {
        interval: 2000
        repeat: true
        running: root.visible
        onTriggered: {
            frameCounterAvg += frameCounter;
            root.fps = frameCounter/2;
            counter++;
            frameCounter = 0;
            if (counter >= 3) {
                root.fpsAvg = frameCounterAvg/(2*counter)
                frameCounterAvg = 0;
                counter = 0;
            }
        }
    }
}
