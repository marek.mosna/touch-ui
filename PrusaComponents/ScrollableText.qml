import QtQuick 2.12
import "."

/* This component creates a Text, that can be shifted by finger
   to show longer text, than actually fits the area
*/
Item {
    id: root
    height: txt.height
    property alias text: txt.text
    property alias font: txt.font
    property alias color: txt.color
    property bool centerH: false
    property bool centerV: false
    property int verticalOffset: 0
    property int horizontalOffset: 0
    property bool swing: false // If too long, text is swinging from side to side
    property int animationVelocity: 60
    property alias interactive: flick.interactive

    function doesFit() {
        return flick.width >= flick.contentWidth
    }

    onHorizontalOffsetChanged: flick.refreshHorizontalAlignment()
    onCenterHChanged: flick.refreshHorizontalAlignment()

    Flickable {
        id: flick
        width: parent.width
        height: parent.height
        contentWidth: txt.width
        // Cannot use binding, because contentX might be animated
        function refreshHorizontalAlignment() {
            if(!root.centerH) flick.contentX = horizontalOffset
            // Fits into the flickable?
            else if(width/2 + horizontalOffset >= width/2 || (width/2 - txt.width/2 - horizontalOffset) <= 0) {
                flick.contentX = horizontalOffset
            }
            else {
                flick.contentX = flick.width/2 - txt.width/2 + horizontalOffset
            }
        }
        onWidthChanged: refreshHorizontalAlignment()

        Text {
            id: txt
            clip: false
            y: centerV ? flick.height/2 - (txt.height/2) + verticalOffset : verticalOffset
            onWidthChanged: flick.refreshHorizontalAlignment()
        }
        clip: true
    }

    SequentialAnimation {
        id: swingAnimation
        running: root.swing && ! doesFit()
        loops: Animation.Infinite

        PauseAnimation {
            duration: 800
        }

        SmoothedAnimation {
            target: flick
            properties: "contentX"
            to: flick.contentWidth - flick.width
            velocity: root.animationVelocity
        }

        PauseAnimation {
            duration: 800
        }

        SmoothedAnimation {
            target: flick
            properties: "contentX"
            to: 0
            velocity: root.animationVelocity
        }
    }
}
