/*
    Copyright 2021, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Templates 2.12 as T
import "."

T.Button {
    id: root
    property string name
    property string pictogram
    property string backgroundColor: Theme.color.buttonBackground
    property string highlight: Theme.color.buttonHighlight
    property bool enabled: true
    property alias image: img
    font.family: Theme.font.fontName
    font.pixelSize: Theme.font.normal

    implicitWidth:  row.width + 10
    implicitHeight: row.height

    background: Rectangle {
        anchors.fill: parent
        radius: 10
        color: root.pressed ? root.highlight : root.backgroundColor
    }


    Item {
        anchors.fill: parent

        Row {
            id: row
            anchors {
                verticalCenter: parent.verticalCenter
            }

            leftPadding: 5

            Image {
                id: img
                anchors.verticalCenterOffset: 1
                anchors.verticalCenter: parent.verticalCenter
                width:  height + 10
                height: txt.height + 6
                source: Theme.pictogram.path + "/" + root.pictogram
                sourceSize.width: width
                sourceSize.height: height
                fillMode: Image.PreserveAspectFit
                smooth: true
            }

            Text {
                id: txt
                anchors.verticalCenter: parent.verticalCenter
                text: root.text
                font: root.font
            }
        }
    }

    onClicked: () => {
                   console.log(LoggingCategories.trajectory, "Button \"" + root.name + "\" clicked")
                   printer0.beep_button()
               }
}

