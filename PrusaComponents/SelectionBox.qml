/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the Qt Quick Controls 2 module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL3$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see http://www.qt.io/terms-conditions. For further
** information use the contact form at http://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPLv3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or later as published by the Free
** Software Foundation and appearing in the file LICENSE.GPL included in
** the packaging of this file. Please review the following information to
** ensure the GNU General Public License version 2.0 requirements will be
** met: http://www.gnu.org/licenses/gpl-2.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.12 as Q
import QtQuick.Templates 2.12 as T
import "."

T.SpinBox {
    id: control
    property string name
    property var items: [""]
    property int innerMargin: 0
    property alias index: control.value

    property alias upPressed: control.up.pressed
    property alias downPressed: control.down.pressed
    textFromValue: (value, locale) => {return items[value] }
    from: 0
    to: items.length - 1

    onUpPressedChanged: () => { if(up.pressed) printer0.beep_button() }
    onDownPressedChanged: () => { if(down.pressed) printer0.beep_button() }
    editable: false
    implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
                            contentItem.implicitWidth + 2 * padding +
                            up.implicitIndicatorWidth +
                            down.implicitIndicatorWidth)
    implicitHeight: Math.max(implicitContentHeight + topPadding + bottomPadding,
                             implicitBackgroundHeight,
                             up.implicitIndicatorHeight,
                             down.implicitIndicatorHeight)

    padding: 6
    leftPadding: padding + (control.mirrored ? (up.indicator ? up.indicator.width : 0) : (down.indicator ? down.indicator.width : 0))
    rightPadding: padding + (control.mirrored ? (down.indicator ? down.indicator.width : 0) : (up.indicator ? up.indicator.width : 0))

    validator: Q.IntValidator {
        locale: control.locale.name
        bottom: Math.min(control.from, control.to)
        top: Math.max(control.from, control.to)
    }

    contentItem: Q.TextInput {
        z: 2
        text: control.displayText

        font.family: Theme.font.fontName
        font.pixelSize: Theme.font.normal
        color: Theme.color.text
        selectionColor: Theme.color.text
        selectedTextColor: Theme.color.selection
        horizontalAlignment: Qt.AlignHCenter
        verticalAlignment: Qt.AlignVCenter

        readOnly: !control.editable
        validator: control.validator
        inputMethodHints: control.inputMethodHints
        clip: true
    }

    up.indicator: Q.Rectangle {
        x: control.mirrored ? 0 : parent.width - width
        height: parent.height
        width: height
        implicitWidth: 40
        implicitHeight: 40
        color: up.pressed ? Theme.color.buttonHighlight : "transparent"
        radius: height / 4

        Q.Image {
            anchors.margins: control.innerMargin
            anchors.fill: parent
            source:  Theme.pictogram.path + "/" + Theme.pictogram.next
            sourceSize.width: width
            sourceSize.height: height
            fillMode: Q.Image.PreserveAspectFit
            mipmap: true
            smooth: true
        }
    }

    down.indicator: Q.Rectangle {
        x: control.mirrored ? parent.width - width : 0
        height: parent.height
        width: height
        implicitWidth: 40
        implicitHeight: 40
        color: down.pressed ? Theme.color.buttonHighlight : "transparent"
        radius: height / 4

        Q.Image {
            anchors.margins: control.innerMargin
            anchors.fill: parent
            source: Theme.pictogram.path + "/" + Theme.pictogram.prev
            sourceSize.width: width
            sourceSize.height: height
            fillMode: Q.Image.PreserveAspectFit
            mipmap: true
            smooth: true
        }
    }

    background: Q.Rectangle {
        implicitWidth: 200
        implicitHeight: 40
        border.width: 1
        color: Theme.color.background
        radius: height / 4
    }

    onValueModified: () => console.log(LoggingCategories.trajectory, "SelectionBox \"" + control.name + "\" changed: \"" + value + "\", \"" + control.items[value] + "\"")
}
