/*
    Copyright 2021, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12 as Q
import QtQuick.Controls 2.12
import QtQuick.Controls.impl 2.12
import QtQuick.Templates 2.12 as T
import "."

T.SpinBox {
    id: control
    property string name
    property int innerMargin: 0
    property alias upPressed: control.up.pressed
    property alias downPressed: control.down.pressed
    property real step: 1.0

    stepSize: step
    onUpPressedChanged: () => {stepSize = step; if(up.pressed) printer0.beep_button() }
    onDownPressedChanged: () => {stepSize = step; if(down.pressed) printer0.beep_button() }

    editable: true
    implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
                            contentItem.implicitWidth + 2 * padding +
                            up.implicitIndicatorWidth +
                            down.implicitIndicatorWidth)
    implicitHeight: Math.max(implicitContentHeight + topPadding + bottomPadding,
                             implicitBackgroundHeight,
                             up.implicitIndicatorHeight,
                             down.implicitIndicatorHeight)

    padding: 6
    leftPadding: padding + (control.mirrored ? (up.indicator ? up.indicator.width : 0) : (down.indicator ? down.indicator.width : 0))
    rightPadding: padding + (control.mirrored ? (down.indicator ? down.indicator.width : 0) : (up.indicator ? up.indicator.width : 0))

    validator: Q.IntValidator {
        locale: control.locale.name
        bottom: Math.min(control.from, control.to)
        top: Math.max(control.from, control.to)
    }

    contentItem: Q.TextInput {
        z: 2
        text: control.displayText

        font.family: Theme.font.fontName
        font.pixelSize: Theme.font.normal
        color: Theme.color.text
        selectionColor: Theme.color.text
        selectedTextColor: Theme.color.selection
        horizontalAlignment: Qt.AlignHCenter
        verticalAlignment: Qt.AlignVCenter

        readOnly: !control.editable
        validator: control.validator
        inputMethodHints: control.inputMethodHints
        enabled: false
        clip: true
    }

    up.indicator: Q.Rectangle {
        x: control.mirrored ? 0 : parent.width - width
        height: parent.height
        width: height
        implicitWidth: 40
        implicitHeight: 40
        color: up.pressed ? Theme.color.buttonHighlight : "transparent"
        radius: height / 4

        Q.Image {
            anchors.margins: control.innerMargin
            anchors.fill: parent
            source:  Theme.pictogram.path + "/" + Theme.pictogram.plus
            sourceSize.width: width
            sourceSize.height: height
            fillMode: Q.Image.PreserveAspectFit
            mipmap: true
            smooth: true
        }
    }

    down.indicator: Q.Rectangle {
        x: control.mirrored ? parent.width - width : 0
        height: parent.height
        width: height
        implicitWidth: 40
        implicitHeight: 40
        color: down.pressed ? Theme.color.buttonHighlight : "transparent"
        radius: height / 4

        Q.Image {
            anchors.margins: control.innerMargin
            anchors.fill: parent
            source: Theme.pictogram.path + "/" + Theme.pictogram.minus
            sourceSize.width: width
            sourceSize.height: height
            fillMode: Q.Image.PreserveAspectFit
            mipmap: true
            smooth: true
        }
    }

    background: Q.Rectangle {
        implicitWidth: 200
        color: Theme.color.background
    }

    onValueChanged: () => console.log(LoggingCategories.trajectory, "SpinBox \"" + name + "\" changed: \"" + value + "\"")

    Q.Timer {
        running: up.pressed || down.pressed
        repeat: true
        interval: 400
        onTriggered: () => stepSize = stepSize * 2
    }
}
