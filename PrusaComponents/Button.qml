/*
    Copyright 2021, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Templates 2.12 as T
import QtQuick.Controls 2.12
import "."

T.Button {
    id: root

    // Basic settings
    property string name
    property string backgroundColor: Theme.color.buttonBackground
    property string highlight: Theme.color.buttonHighlight
    property bool enabled: true

    font.family: Theme.font.fontName
    font.pixelSize: Theme.font.normal

    implicitWidth: txtLabel.width
    implicitHeight: Theme.sizing.defaultComponent.height

    background: Rectangle {
        anchors.fill: parent
        radius: 10
        color: root.pressed ? root.highlight : root.backgroundColor
    }

    Text {
        id: txtLabel
        anchors.centerIn: parent
        text: root.text
        font: root.font
    }

    onClicked: () => {
                   console.log(LoggingCategories.trajectory, "Button \"" + root.name + "\" clicked")
                   printer0.beep_button()
               }
}
