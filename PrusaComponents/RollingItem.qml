/*
    Copyright 2021, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick.Controls 2.12
import QtQuick 2.12
import "."

Item {
    id: root
    property bool tryCenter: true
    property int overshoot: 30
    property alias contentItem: rollingText.sourceComponent
    property int _difference: rollingText.width - root.width
    property int _realCenter: (root.parent.x  + root.parent.width/2) - root.x

    implicitHeight: 40
    implicitWidth: 200

    clip: true

    Loader {
        id: rollingText

        height: parent.height
        x: tryCenter ? (_realCenter - rollingText.width/2) - Math.max(0, root._realCenter + rollingText.width/2 - root.width) : 0

        Rectangle {
            visible: _debug_rectangles
            width: 3
            height: parent.height
            border {
                width: 1
                color: Theme.color.debug
            }
            anchors.centerIn: parent
        }
    }

    SequentialAnimation {
        id: animator
        running: root._difference > 0 && root.visible
        loops: Animation.Infinite

        PropertyAnimation {
            property: "x"
            target: rollingText
            from: root.overshoot
            to: - root._difference - overshoot
            duration: Math.max(0, 1000 + 50 * root._difference)
            easing.type: Easing.InOutCubic
        }

        PropertyAnimation {
            property: "x"
            target: rollingText
            from: - root._difference - overshoot
            to: root.overshoot
            duration: Math.max(0, 1000 + 50 * root._difference)
            easing.type: Easing.InOutCubic
        }
    }

    Rectangle {
        visible: _debug_rectangles
        anchors.fill: parent
        color: "transparent"
        border {
            width: 1
            color: Theme.color.debug
        }
    }


    Rectangle {
        visible: _debug_rectangles
        color: Theme.color.debug
        width: 9
        height: 30
        x: _realCenter - width/2
    }
}

