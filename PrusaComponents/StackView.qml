/*
    Copyright 2021, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Templates 2.12 as T

/** Customized StackView without animations */
T.StackView {
    id: control
    property alias name: control.objectName
    popEnter: Transition {
        PropertyAnimation {
            property: "opacity"
            from: 0
            to: 1
            duration: 0
        }
    }

    popExit: Transition {
        PropertyAnimation {
            property: "opacity"
            from: 0
            to: 1
            duration: 0
        }
    }

    pushEnter: Transition {
        PropertyAnimation {
            property: "opacity"
            from: 0
            to: 1
            duration: 0
        }
    }

    pushExit: Transition {
        PropertyAnimation {
            property: "opacity"
            from: 0
            to: 1
            duration: 0
        }
    }

    replaceEnter: Transition {
        PropertyAnimation {
            property: "opacity"
            from: 0
            to: 1
            duration: 0
        }
    }

    replaceExit: Transition {
        PropertyAnimation {
            property: "opacity"
            from: 0
            to: 1
            duration: 0
        }
    }

    onCurrentItemChanged: () => {
                              var pages = ""
                              for(var i = 0; i < depth; ++i) {
                                  var item = get(i, StackView.DontLoad)
                                  if(item) {
                                    pages += "->" + item.name
                                  }
                                  else pages += "->NA"
                              }

                              console.log(LoggingCategories.trajectory, "StackView \"" + control.name +"\": " + pages)
                          }
}
