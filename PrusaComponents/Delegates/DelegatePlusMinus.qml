
/*
    Copyright 2021, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import ".."

/*
  PlusMinus delegate based on SimpleDelegate

  Accepts only integers !!!
*/
SimpleDelegate {
    id: root
    property color textColor: Theme.color.text
    property int from: - 0x7fffffff
    property int to: 0x7fffffff
    property alias value: layout.spinBoxValue
    property alias step: layout.spinBoxStep
    property alias text: layout.txtText
    property alias initValue:  layout.spinBoxValue
    property alias spinBox: layout.spinBox
    property int initValue
    property string units: ""
    property var valuePresentation: function(value) { return value } // Change this property to transform value for presentation
    signal valueModified()

    textComponent: null

    RowLayout {
        id: layout
        property alias txtText: txt.text
        property alias spinBoxValue: spinBox.value
        property alias spinBoxStep: spinBox.step
        property alias spinBox: spinBox

        anchors {
            verticalCenter: root.verticalCenter
            left: root.left
            leftMargin: 100
            right: root.right
            rightMargin: 15
        }

        RollingText {
            id: txt
            color: root.textColor
            Layout.maximumWidth: Layout.width - txtUnits.width - spinBox.width
            Layout.alignment: Qt.AlignLeft
            Layout.fillWidth: true
            overshoot: 0
        }

        Text {
            id: txtUnits
            visible: root.units !== ""
            text: "[" + root.units + "]"
        }

        SpinBox {
            id: spinBox
            name: root.name
            from: root.from
            to: root.to
            Layout.preferredWidth: width
            textFromValue: valuePresentation
            onValueModified: root.valueModified()
        }
    }

    Rectangle {
        color: "black"
        opacity: 0.5
        visible: ! root.enabled
        anchors.fill: parent
    }
}

