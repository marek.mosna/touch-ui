
/*
    Copyright 2021, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import ".."

/*
  PlusMinus delegate based on SimpleDelegate

  Value must fit into into interval <INT_NIM, INT_MAX> / pow(10, decimalPlaces)

*/
SimpleDelegate {
    id: root
    property color textColor: Theme.color.text
    property int from: - 0x7fffffff
    property int to: 0x7fffffff

    // Use setValue to set value
    readonly property real value: layout.spinBoxValue / decimalPlaces
    property real step: 1
    property alias text: layout.txtText
    property alias initValue:  layout.spinBoxValue
    property int initValue: 0
    property string units: ""
    property var valuePresentation: function(value) { return value.toFixed(root.decimalPlaces) } // Change this property to transform value for presentation
    property int decimalPlaces: 1
    function setValue(val) { layout.spinBoxValue = val  }
    signal valueModified()

    RowLayout {
        id: layout
        property alias txtText: txt.text
        property alias spinBoxValue: spinBox.value
        anchors {
            verticalCenter: root.verticalCenter
            left: root.left
            leftMargin: 100
            right: root.right
            rightMargin: 15
        }

        RollingText {
            id: txt
            color: root.textColor

            Layout.maximumWidth: Layout.width - txtUnits.width - spinBox.width
            Layout.alignment: Qt.AlignLeft
            Layout.fillWidth: true
            overshoot: 0
        }

        Text {
            id: txtUnits
            visible: root.units !== ""
            text: "[" + root.units + "]"
        }

        SpinBoxDouble {
            id: spinBox
            name: root.name
            from: root.from
            to: root.to
            step: root.step
            onValueModified: () => root.valueModified()
            decimals: root.decimalPlaces
            Component.onCompleted: () => { if(root.initValue) {root.setValue(root.initValue)} }
            editable: false
        }
    }

    Rectangle {
        color: "black"
        opacity: 0.5
        visible: ! root.enabled
        anchors.fill: parent
    }
}

