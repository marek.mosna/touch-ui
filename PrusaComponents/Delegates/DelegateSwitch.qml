
/*
    Copyright 2021, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import ".."

/*
    SwitchDelegate replacement
    - pictogram on the left
    - click does not automatically switch the checked state
    - emits clicked() signal when clicked
*/

SimpleDelegate {
    id: root

    property string textColor: Theme.color.text
    property string text
    property bool checked
    signal clicked()

    textComponent: Component {
        RowLayout {

            RollingText {
                color: root.textColor
                font.pixelSize: Theme.font.normal
                text: root.text
                overshoot: 0
                Layout.maximumWidth: Layout.width - theSwitch.width
                Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                Layout.fillWidth: true
            }

            PrusaSwitch {
                id: theSwitch
                enabled: root.enabled
                Layout.alignment: Qt.AlignVCenter
                anchors {
                    rightMargin:20
                    margins: 15
                }

                name: root.name
                clickSwitch: false
                onClicked: root.clicked()
                checked: root.checked
            }
        }
    }

    Rectangle {
        color: "black"
        opacity: 0.5
        visible: ! root.enabled
        anchors.fill: parent
    }
}

