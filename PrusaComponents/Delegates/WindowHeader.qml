/*
    Copyright 2021, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Controls 2.12
import ".."

Item {
    id: root
    implicitWidth: 800
    implicitHeight: 60

    // Page title
    property string title

    // Page pictogram
    property string pictogram

    property int leftPadding: 0
    property int rightPadding: 0

    // Is ethernet connected?
    property bool ethernetConnected: false

    // Is Wifi connected?
    property bool wifiConnected: false

    // Wifi signal strength
    property int wifiStrength: 0 // 0-3

    // Should "network busy" be reported?
    property bool networkWorking: false

    // Current time as a string, should be updated periodically.
    property string clock: "00:00"

    // Show the back button?
    property bool backButtonVisible: true

    // Show the cancel button?
    property bool cancelButtonVisible: false

    // Show the close button?
    property bool closeButtonVisible: false

    // MessageCount to be reported, anything > 0 means show the message button.
    property int messageCount: 0

    property color bgColor: Theme.color.background

    signal backClicked()
    signal cancelClicked()
    signal closeClicked()
    signal messageClicked()
    signal networkClicked()
    signal clockClicked()
    signal titleLongPressed()
    signal titleDoubleClicked()
    signal swipeDown()

    property bool _backButtonVisible: root.backButtonVisible
    property bool _cancelButtonVisible: root.cancelButtonVisible && ! root.backButtonVisible && ! root.closeButtonVisible
    property bool _closeButtonVisible:  !root.cancelButtonVisible && ! root.backButtonVisible && root.closeButtonVisible

    readonly property string _title: root.title.toUpperCase()
    property string _networkPictogram: {
        if(root.networkWorking) return Theme.pictogram.working
        else if(root.ethernetConnected) return Theme.pictogram.ethernetSimple
        else if(root.wifiConnected) return Theme.pictogram.wifiPictogramsLight[wifiStrength]
        else return Theme.pictogram.noConnection
    }

    Rectangle {
        anchors.fill: parent
        color: parent.bgColor
    }

    Item {
        id: backContainer
        x: root.leftPadding
        visible: root._backButtonVisible || root._cancelButtonVisible || root._closeButtonVisible
        width: ! visible ? 0 : (root._backButtonVisible ? btnBack.width : 0) + (root._cancelButtonVisible ? btnCancel.width : 0) + (root._closeButtonVisible ? btnClose.width : 0)
        height: parent.height

        IconButton {
            name: "Back"
            id: btnBack
            visible: root._backButtonVisible
            height: parent.height
            pictogram: Theme.pictogram.back
            text: qsTr("back")
            //font.bold: true
            font.pixelSize: height / 2 + 2
            image.height: parent.height * (2/3) + 3
            //image.anchors.verticalCenterOffset: -3
            onClicked: root.backClicked()
            backgroundColor: "transparent"
        }

        IconButton {
            id: btnCancel
            name: "Cancel"
            visible: root._cancelButtonVisible
            height: parent.height
            pictogram: Theme.pictogram.cancelSimple
            text: qsTr("cancel")
            //font.bold: true
            font.pixelSize: height / 2 + 2
            image.anchors.verticalCenterOffset: -1
            onClicked: root.backClicked()
            backgroundColor: "transparent"
        }

        IconButton {
            id: btnClose
            name: "Close"
            visible: root._closeButtonVisible
            height: parent.height
            pictogram: Theme.pictogram.cancelSimple
            text: qsTr("close")
            //font.bold: true
            font.pixelSize: height / 2 + 2
            image.anchors.verticalCenterOffset: -1
            onClicked: root.closeClicked()
            backgroundColor: "transparent"
        }
    }

    RollingItem {
        height: parent.height
        anchors {
            left: backContainer.right
            right: btnNotification.left
        }
        tryCenter: true

        contentItem: Row {
            height: parent.height
            spacing: 5

            Image {
                id: img
                anchors.verticalCenterOffset: -3
                anchors.verticalCenter: parent.verticalCenter
                width: root.height * (1/2)
                height: width
                source: Theme.pictogram.path + "/" + root.pictogram
                sourceSize.width: width
                sourceSize.height: height
                fillMode: Image.PreserveAspectFit
                smooth: true
            }

            Text {
                id: txtTitle
                anchors.verticalCenter: parent.verticalCenter
                text: root._title
                font.pixelSize: root.height/2 - 2
            }
        }

        SwipeArea {
            anchors.fill: parent
            onSwipeDown: () => root.swipeDown()
            onPressAndHold: () => {
                           console.log(LoggingCategories.trajectory, "Button \"" + "Title"+ "\" longPressed")
                           root.titleLongPressed()
                       }
            onDoubleClicked: () => root.titleDoubleClicked()
        }
    }

    Button {
        id: btnNotification
        visible: root.messageCount > 0
        anchors {
            verticalCenter: parent.verticalCenter
            right: btnNetwork.left
        }

        name: "Notification"
        width: visible ? parent.height - 10 : 0
        height: parent.height
        contentItem:Item {

            Image {
                anchors.centerIn: parent
                anchors.verticalCenterOffset: -3
                width: height
                height: root.height * (1/2)
                source: Theme.pictogram.path + "/" + Theme.pictogram.message
                sourceSize.width: width
                sourceSize.height: height
                fillMode: Image.PreserveAspectFit
                smooth: true
            }
        }

        backgroundColor: "transparent"
        onClicked: () => root.messageClicked()
    }

    Button {
        id: btnNetwork
        anchors {
            verticalCenter: parent.verticalCenter
            right: txtClock.left
            rightMargin: 10
        }

        name: "Network"
        width: parent.height - 10
        height: parent.height
        contentItem: Item {

            Image {
                id: imgNetwork
                anchors.centerIn: parent
                anchors.verticalCenterOffset: -3
                width: height
                height: root.height * (1/2)
                source: Theme.pictogram.path + "/" + root._networkPictogram
                sourceSize.width: width
                sourceSize.height: height
                fillMode: Image.PreserveAspectFit
                smooth: true

                RotationAnimator {
                    target: imgNetwork
                    running: root.networkWorking
                    from: 0
                    to: 359
                    duration: 1000
                    loops: Animator.Infinite
                    onRunningChanged: target.rotation = 0
                }
            }
        }
        backgroundColor: "transparent"
        onClicked: () => root.networkClicked()
    }

    Button {
        id: txtClock
        name: "Clock"
        height: parent.height

        anchors {
            verticalCenter: parent.verticalCenter
            right: parent.right
            rightMargin: root.rightPadding
        }

        text: root.clock
        font.pixelSize: root.height / 2 - 2

        onClicked: () => root.clockClicked()
        backgroundColor: "transparent"
    }

    Rectangle {
        anchors.fill: root
        visible: _debug_rectangles
        color: "transparent"
        border {
            width: 1
            color: Theme.color.debug
        }
    }
}
