
/*
    Copyright 2021, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import ".."

/*
  PlusMinus delegate based on SimpleDelegate
*/
SimpleDelegate {
    id: root
    property color textColor: Theme.color.text
    property int index: 0
    property var items
    property string text
    property bool wrap: false
    property alias selectionBox: layout.selectionBox

    textComponent: null
    RowLayout {
        id: layout
        property alias selectionBox: selectionBox

        anchors {
            verticalCenter: root.verticalCenter
            left: root.left
            leftMargin: 100
            right: root.right
            rightMargin: 15
        }

        RollingText {
            id: txt
            color: root.textColor
            text: root.text
            overshoot: 0
            Layout.maximumWidth: Layout.width - selectionBox.width
            Layout.alignment: Qt.AlignLeft
            Layout.fillWidth: true
        }

        SelectionBox {
            id: selectionBox
            items: root.items
            index: root.index
            wrap: root.wrap
            name: root.name
            onIndexChanged: root.index = index
            Component.onCompleted: () => index = root.index
            Layout.preferredWidth: width

            Connections {
                target: root
                function onIndexChanged() {
                    index = root.index
                }
            }
        }
    }

    Rectangle {
        color: "black"
        opacity: 0.5
        visible: ! root.enabled
        anchors.fill: parent
    }
}

