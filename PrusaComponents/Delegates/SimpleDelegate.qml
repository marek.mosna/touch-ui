
/*
    Copyright 2022, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Controls 2.12
import ".."
/*
  Base for more complex delegates
  - does not handle clicks
  - content passed via textComponent
  - loaded() singnal when content is loaded
  - don't forget to set the name

*/
Item {
    id: root

    property string name
    property string pictogram:   Theme.pictogram.cancel
    property int pictogramSize: 40
    property Component textComponent
    property alias item: loader.item
    signal loaded() // when the component is loaded

    implicitWidth: 760
    implicitHeight: visible ? 70 : 0

    Rectangle {
        id: background
        color: Theme.color.background
        anchors.fill: parent
    }

    Item {
        anchors {
            left: root.left
            bottom:  root.bottom
            top: root.top
            leftMargin: 20
            margins: 15

        }
        width: pictogramSize * 1.5

        Image {
            id: img
            height: pictogramSize
            anchors.centerIn: parent
            sourceSize.width: parent.height
            sourceSize.height: parent.height
            fillMode: Image.PreserveAspectFit
            source: Theme.pictogram.path + "/" + root.pictogram
        }
    }

    Loader {
        id: loader
        anchors {
            verticalCenter: root.verticalCenter
            left: root.left
            leftMargin: 100
            right: root.right
            rightMargin: 15
        }
        sourceComponent: root.textComponent
    }

    HorizontalSeparator {
        anchors.bottom: parent.bottom
        width: parent.width
    }
}
