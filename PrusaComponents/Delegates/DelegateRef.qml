
/*
    Copyright 2021, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import ".."

ListDelegate {
    id: root

    property string textColor: Theme.color.text
    readonly property int textWidth: width - loader.x - rightArrow.width
    property Component textComponent: Component {
        RollingText {
            color: root.textColor
            font.pixelSize: Theme.font.normal
            text: root.text
            width: textWidth
            overshoot: 0
        }
    }

    property string pictogram:   Theme.pictogram.cancel
    property bool arrowVisible: true

    Item {
        anchors {
            left: root.left
            bottom:  root.bottom
            top: root.top
            leftMargin: 20
            margins: 15
        }

        width: height * 1.5

        Image {
            id: img
            height: parent.height
            anchors.centerIn: parent
            sourceSize.width: parent.height
            sourceSize.height: parent.height
            fillMode: Image.PreserveAspectFit
            source: Theme.pictogram.path + "/" + root.pictogram
            rotation: root.rotation
        }
    }

    Loader {
        id: loader
        anchors {
            verticalCenter: root.verticalCenter
            left: root.left
            leftMargin: 100
            right: rightArrow.left
            rightMargin: 15
        }
        sourceComponent: root.textComponent
    }

    /* Pass all the stolen events */
    MouseArea {
        anchors.fill: loader
        onClicked: ()=>root.clicked()
    }

    Image {
        id: rightArrow
        visible: root.arrowVisible
        anchors {
            right: root.right
            bottom:  root.bottom
            top: root.top
            rightMargin:20
            margins: 15
        }

        sourceSize.width: height
        sourceSize.height: height
        fillMode: Image.PreserveAspectFit
        source: Theme.pictogram.path + "/" + Theme.pictogram.nextGrey
    }

    HorizontalSeparator {
        width: root.width
        anchors.bottom: root.bottom
    }

    Rectangle {
        color: "black"
        opacity: 0.5
        visible: ! root.enabled
        anchors.fill: parent
    }

    onClicked: () => { printer0.beep_button() }
}

