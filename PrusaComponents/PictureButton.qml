/*
    Copyright 2021, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Templates 2.12 as T
import "."

T.Button {
    id: root
    width: 150
    height: 150

    // Basic settings
    property alias name: root.objectName
    property string pictogram: ""
    property string backgroundColor: Theme.color.buttonBackground
    property string highlight: Theme.color.buttonHighlight
    property int innerMargin: height / (150/40)

    // Aliases for further customization
    property alias label: label
    property alias image: img
    property alias disabledOverlay: disabledOverlay

    signal buttonPressed()
    signal buttonReleased()

    background: Rectangle {
        id: backgroundObject
        color: root.pressed ? root.highlight : root.backgroundColor
        anchors.fill: parent
        radius: root.height/10
        border.width: root.highlighted ? 1  : 0
        border.color: root.highlight
    }

    Text {
        id: label
        visible: true
        width: parent.width + 20
        height: parent.height / 4
        horizontalAlignment: Text.AlignHCenter
        anchors {
            margins: 5
            top:  root.bottom
            left: parent.left
            right: parent.right
            topMargin:  15
        }
        text: root.text
        wrapMode: Text.WordWrap
        color: Theme.color.text
    }

    Image {
        id: img
        anchors.margins: root.innerMargin
        anchors.fill: parent
        source: root.pictogram ? Theme.pictogram.path + "/" + root.pictogram : ""
        sourceSize.width: width
        sourceSize.height: height
        fillMode: Image.PreserveAspectFit
        mipmap: true
        smooth: true
    }

    Rectangle {
        id: disabledOverlay
        visible: ! root.enabled
        opacity: 0.5
        anchors.fill: parent
        radius: 15
        color: Theme.color.background
    }

    onPressedChanged: () => {
        if(pressed) root.buttonPressed()
        else root.buttonReleased()
    }

    onClicked: () => {
                   console.log(LoggingCategories.trajectory, "Button \"" + root.name + "\" clicked")
                   printer0.beep_button()
               }
}
