/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "localesetting.h"
#include <QRegularExpression>
LocaleSetting::LocaleSetting(QJSEngine &engine, QObject *parent) :
    BaseDBusObject(engine, service, interface, path, QDBusConnection::systemBus(), parent),
    m_locale1 (service, path, QDBusConnection::systemBus(), this )
{
    m_locale1.setTimeout(2000);
    setServiceIsOnDemand(true);
    reload_properties();
    this->getCurrentLocale();
}

LocaleSetting::~LocaleSetting() {}

QString LocaleSetting::locale() const
{
    if(m_locale == "C") return "en_US";
    return m_locale;
}

void LocaleSetting::setLocale(QString locale)
{
    if (m_locale == locale)
        return;
    // Tell the localed to set locale, the local locale property will
    // be updated when localed sends the ProperitesChanged signal
    m_locale1.SetLocale({"LANG=" + locale + ".UTF-8"}, false);
}

bool LocaleSetting::set(const QString &name, const QVariant &value)
{
    if(name == QStringLiteral("Locale")) {
        auto strList =  value.toStringList();
        for(const QString &str : strList) {
            QString loc = parseLocale(str);
            if(loc != "") {
                // Cannot use setLocale, that is used to directly set the localed.Locale
                m_locale = loc;
                emit localeChanged(loc);
            }
        }
    }
    // Note: Ignore all other properties, we are only interested in locale - language
    return true;
}



QString LocaleSetting::parseLocale(QString str)
{
    if(str == "LANG=C") return "C";

    QRegularExpression re("LANG=([a-zA-Z]+)_([a-zA-Z]+)\\.([a-zA-Z0-9,\\-]+)");
    QRegularExpressionMatch match = re.match(str);
    if(match.hasMatch() && match.lastCapturedIndex() == 3) {
        // Extract the language code from string: "LANG=cs_CZ.UTF-8" -> cs_CZ
        const QString lang_code = match.captured(1) + "_" + match.captured(2);
        return lang_code;
    }
    else return "";
}

void LocaleSetting::getCurrentLocale() {
    QDBusPendingReply<QDBusVariant> reply = m_properties.Get("org.freedesktop.locale1", "Locale");

    // Don't wait for the result, make watcher call callbacks when done
    QDBusPendingCallWatcher *watcher = new QDBusPendingCallWatcher(reply, this);
    QObject::connect(watcher, &QDBusPendingCallWatcher::finished,
                     this, [=](QDBusPendingCallWatcher* watcher) mutable {

        QDBusPendingReply<QDBusVariant> reply = *watcher;
        if (reply.isError()) {
            qDebug() << "Get(Locale): error: " << QDBusError::errorString( reply.error().type());

        } else {
            //qDebug() << "Get(Locale): returned " << reply.value().variant().toString();
            const QString str =  reply.value().variant().toString();
            QString loc( parseLocale(str));
            if(loc != "") {
                // Cannot use setLocale, that us used to directly set the localed.Locale
                m_locale = loc;
                emit localeChanged(loc);
            }
        }
        watcher->deleteLater();
    } );
}
