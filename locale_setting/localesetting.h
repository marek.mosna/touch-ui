/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef LOCALESETTING_H
#define LOCALESETTING_H
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QTranslator>
#include "locale_proxy.h"
#include "basedbusobject.h"
//#include "properties_proxy.h"
#include <locale>

/** @brief Handles communication with localed */
class LocaleSetting : public BaseDBusObject
{
    Q_OBJECT
    LocaleProxy m_locale1;
//    PropertiesProxy m_properties;
    QString m_locale;
    Q_PROPERTY(QString locale READ locale WRITE setLocale NOTIFY localeChanged)
public:
    inline static const QString service{"org.freedesktop.locale1"};
    inline static const QString interface{"org.freedesktop.locale1"};
    inline static const QString path{"/org/freedesktop/locale1"};

    explicit LocaleSetting(QJSEngine &engine, QObject * parent = nullptr);
    ~LocaleSetting();

    QString locale() const;

private:
    /** @brief Process the LANG=cs_CZ.UTF-8 string into something like "cs_CZ" */
    QString parseLocale(QString str);

    /// @brief Initiate reading of the current locale,
    /// response will arrive asynchronously and will be stored in the language property.
    void getCurrentLocale();

public slots:
    void setLocale(QString locale);

signals:

    void localeChanged(QString locale);

    // BaseDBusObject interface
protected:
    virtual bool set(const QString &name, const QVariant &value) override;
};

#endif // LOCALESETTING_H
