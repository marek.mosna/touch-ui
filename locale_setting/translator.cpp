/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QDebug>
#include "translator.h"

Translator::Language Translator::localeToLanguage(QString loc)
{
    if(languageLocales().values().contains(loc)) {
        Language lang = Language(languageLocales().key(loc));
        return lang;
    }
    return Default;
}

Translator::Translator(QGuiApplication &app, QQmlApplicationEngine &engine, QObject *parent) : QObject(parent), m_translator(new QTranslator(this)), m_Application(app), m_Engine(engine), m_language(Default)
{

}

void Translator::setLanguage(Translator::Language language)
{
    if (m_language == language)
        return;

    m_language = language;

    QString languageFile =  languageFiles()[m_language];

    qDebug() << "Setting language" << m_language;
    qDebug() << "Language file is" <<languageFile;
    bool result = m_translator->load(languageFiles()[language]);
    if(! result) qWarning() << "Loading language file " << languageFile << " FAILED!";

    m_Application.installTranslator(m_translator);
    this->m_Engine.retranslate();
    emit languageChanged(m_language);
}

