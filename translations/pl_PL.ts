<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl-PL">
<context>
    <name>DelegateAddHiddenNetwork</name>
    <message>
        <location filename="../qml/DelegateAddHiddenNetwork.qml" line="91"/>
        <source>Add Hidden Network</source>
        <translation type="unfinished">Dodaj ukrytą sieć</translation>
    </message>
</context>
<context>
    <name>DelegateAdminPlusMinus</name>
    <message>
        <location filename="../qml/DelegateAdminPlusMinus.qml" line="41"/>
        <source>N/A</source>
        <translation type="unfinished">N/D</translation>
    </message>
</context>
<context>
    <name>DelegateEthNetwork</name>
    <message>
        <location filename="../qml/DelegateEthNetwork.qml" line="110"/>
        <source>Plugged in</source>
        <translation type="unfinished">Podłączono</translation>
    </message>
    <message>
        <location filename="../qml/DelegateEthNetwork.qml" line="110"/>
        <source>Unplugged</source>
        <translation type="unfinished">Nie podłączono</translation>
    </message>
</context>
<context>
    <name>DelegateRef</name>
    <message>
        <location filename="../qml/DelegateRef.qml" line="28"/>
        <source>N/A</source>
        <translation type="unfinished">N/D</translation>
    </message>
</context>
<context>
    <name>DelegateState</name>
    <message>
        <location filename="../qml/DelegateState.qml" line="38"/>
        <source>Network Info</source>
        <translation type="unfinished">Informacje o sieci</translation>
    </message>
</context>
<context>
    <name>DelegateWifiClientOnOff</name>
    <message>
        <location filename="../qml/DelegateWifiClientOnOff.qml" line="39"/>
        <source>Wi-Fi Client</source>
        <translation type="unfinished">Klient Wi-Fi</translation>
    </message>
</context>
<context>
    <name>DelegateWifiNetwork</name>
    <message>
        <location filename="../qml/DelegateWifiNetwork.qml" line="106"/>
        <source>Forget network?</source>
        <translation type="unfinished">Usunąć sieć?</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWifiNetwork.qml" line="107"/>
        <source>Do you really want to forget this network&apos;s settings?</source>
        <translation type="unfinished">Czy na pewno chcesz zapomnieć ustawienia tej sieci?</translation>
    </message>
</context>
<context>
    <name>DelegateWifiOnOff</name>
    <message>
        <location filename="../qml/DelegateWifiOnOff.qml" line="36"/>
        <source>Wi-Fi</source>
        <translation type="unfinished">Wi-Fi</translation>
    </message>
</context>
<context>
    <name>DelegateWizardCheck</name>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="74"/>
        <source>Platform range</source>
        <translation type="unfinished">Zakres platformy</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="75"/>
        <source>Platform home</source>
        <translation type="unfinished">Bazowanie platformy</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="76"/>
        <source>Tank range</source>
        <translation type="unfinished">Zakres zbiornika</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="77"/>
        <source>Tank home</source>
        <translation type="unfinished">Bazowanie zbiornika</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="78"/>
        <source>Display test</source>
        <translation type="unfinished">Test wyświetlacza</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="79"/>
        <source>Printer calibration</source>
        <translation type="unfinished">Kalibracja drukarki</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="80"/>
        <source>Sound test</source>
        <translation type="unfinished">Test dźwięku</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="81"/>
        <source>UV LED</source>
        <translation type="unfinished">UV LED</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="82"/>
        <source>UV LED and fans</source>
        <translation type="unfinished">UV LED i wentylatory</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="83"/>
        <source>Release foam</source>
        <translation type="unfinished">Wyciągnij piankę</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="84"/>
        <source>Make tank accessible</source>
        <translation type="unfinished">Zapewnij dostęp do zbiornika</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="85"/>
        <source>Resin sensor</source>
        <translation type="unfinished">Czujnik poziomu żywicy</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="86"/>
        <source>Serial number</source>
        <translation type="unfinished">Numer seryjny</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="87"/>
        <source>Temperature</source>
        <translation type="unfinished">Temperatura</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="88"/>
        <source>Tank calib. start</source>
        <translation type="unfinished">Start kalib. zbiornika</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="89"/>
        <location filename="../qml/DelegateWizardCheck.qml" line="120"/>
        <source>Tank level</source>
        <translation type="unfinished">Poziom zbiornika</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="90"/>
        <source>Platform calibration</source>
        <translation type="unfinished">Kalibracja platformy</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="91"/>
        <source>Tilt timming</source>
        <translation type="unfinished">Czas tiltu</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="92"/>
        <source>Obtain system info</source>
        <translation type="unfinished">Wyciągnij informacje systemowe</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="93"/>
        <source>Obtain calibration info</source>
        <translation type="unfinished">Wyciągnij informacje o kalibracji</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="94"/>
        <source>Erase projects</source>
        <translation type="unfinished">Usuń projekty</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="95"/>
        <source>Reset hostname</source>
        <translation type="unfinished">Reset nazwy hosta</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="96"/>
        <source>Reset API key</source>
        <translation type="unfinished">Reset klucza API</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="97"/>
        <source>Reset remote config</source>
        <translation type="unfinished">Reset zdalnej konfiguracji</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="98"/>
        <source>Reset HTTP digest</source>
        <translation type="unfinished">Reset HTTP digest</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="99"/>
        <source>Reset Wi-Fi settings</source>
        <translation type="unfinished">Reset ustawień Wi-Fi</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="100"/>
        <source>Reset timezone</source>
        <translation type="unfinished">Reset strefy czasowej</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="101"/>
        <source>Reset NTP state</source>
        <translation type="unfinished">Reset stanu NTP</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="102"/>
        <source>Reset system locale</source>
        <translation type="unfinished">Reset języka systemu</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="103"/>
        <source>Clear UV calibration data</source>
        <translation type="unfinished">Wyczyść dane kalibracyjne UV</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="104"/>
        <source>Clear downloaded Slicer profiles</source>
        <translation type="unfinished">Wyczyść pobrane profile slicera</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="105"/>
        <source>Reset print configuration</source>
        <translation type="unfinished">Reset konfiguracji wydruku</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="106"/>
        <source>Erase motion controller EEPROM</source>
        <translation type="unfinished">Wyczyść EEPROM Kontrolera Ruchu</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="107"/>
        <source>Reset homing profiles</source>
        <translation type="unfinished">Reset profili bazowania</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="108"/>
        <source>Send printer data to MQTT</source>
        <translation type="unfinished">Wyślij dane drukarki do MQTT</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="109"/>
        <source>Disable factory mode</source>
        <translation type="unfinished">Wyłącz tryb fabryczny</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="110"/>
        <source>Moving printer to accept protective foam</source>
        <translation type="unfinished">Zmiana pozycji do włożenia pianki ochronnej</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="111"/>
        <source>Pressing protective foam</source>
        <translation type="unfinished">Dociskanie pianki ochronnej</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="112"/>
        <source>Disable ssh, serial</source>
        <translation type="unfinished">Wyłącz SSH, szeregowe</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="113"/>
        <source>Check for UV calibrator</source>
        <translation type="unfinished">Sprawdź kalibrator UV</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="114"/>
        <source>UV LED warmup</source>
        <translation type="unfinished">Nagrzewanie UV LED</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="115"/>
        <source>UV calibrator placed</source>
        <translation type="unfinished">Kalibrator UV umieszczony</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="116"/>
        <source>Calibrate center</source>
        <translation type="unfinished">Kalibruj środek</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="117"/>
        <source>Calibrate edge</source>
        <translation type="unfinished">Kalibruj krawędź</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="118"/>
        <source>Apply calibration results</source>
        <translation type="unfinished">Zastosuj wyniki kalibracji</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="119"/>
        <source>Waiting for UV calibrator to be removed</source>
        <translation type="unfinished">Czekam na usunięcie kalibratora UV</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="121"/>
        <source>Reset UI settings</source>
        <translation type="unfinished">Reset ustawień interfejsu</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="122"/>
        <source>Erase UV PWM settings</source>
        <translation type="unfinished">Czyszczenie ustawień UV PWM</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="123"/>
        <source>Reset selftest status</source>
        <translation type="unfinished">Reset stanu Selftestu</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="124"/>
        <source>Reset printer calibration status</source>
        <translation type="unfinished">Reset stanu kalibracji drukarki</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="125"/>
        <source>Set new printer model</source>
        <translation type="unfinished">Ustaw nowy model drukarki</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="126"/>
        <source>Resetting hardware counters</source>
        <translation type="unfinished">Resetowanie liczników sprzętowych</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="127"/>
        <source>Recording changes</source>
        <translation type="unfinished">Zapisywanie zmian</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="128"/>
        <source>Unknown</source>
        <translation type="unfinished">Nieznany</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="129"/>
        <source>Check ID:</source>
        <translation type="unfinished">Sprawdź ID:</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="141"/>
        <source>Waiting</source>
        <translation type="unfinished">Czekam</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="142"/>
        <source>Running</source>
        <translation type="unfinished">Pracuje</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="143"/>
        <source>Passed</source>
        <translation type="unfinished">Zgodne</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="144"/>
        <source>Failure</source>
        <translation type="unfinished">Niepowodzenie</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="145"/>
        <source>With Warning</source>
        <translation type="unfinished">Z ostrzeżeniem</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="146"/>
        <source>User action pending</source>
        <translation type="unfinished">Czekam na użytkownika</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="147"/>
        <source>Canceled</source>
        <translation type="unfinished">Anulowano</translation>
    </message>
</context>
<context>
    <name>ErrorPopup</name>
    <message>
        <location filename="../qml/ErrorPopup.qml" line="40"/>
        <source>Unknown error</source>
        <translation type="unfinished">Nieznany błąd</translation>
    </message>
    <message>
        <location filename="../qml/ErrorPopup.qml" line="55"/>
        <source>Understood</source>
        <translation type="unfinished">Zrozumiano</translation>
    </message>
</context>
<context>
    <name>ErrorcodesText</name>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="6"/>
        <source>Tilt homing failed, check its surroundings and repeat the action.</source>
        <translation type="unfinished">Niepowodzenie bazowania tiltu. Sprawdź otoczenie mechanizmu i spróbuj ponownie.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="7"/>
        <source>Tower homing failed, make sure there is no obstacle in its path and repeat the action.</source>
        <translation type="unfinished">Niepowodzenie bazowania kolumny. Upewnij się, że nic jej nie blokuje i spróbuj ponownie.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="8"/>
        <source>Moving the tower failed. Make sure there is no obstacle in its path and repeat the action.</source>
        <translation type="unfinished">Błąd ruchu kolumny. Upewnij się, że nic jej nie blokuje i spróbuj ponownie.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="9"/>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="115"/>
        <source>Incorrect RPM reading of the %(failed_fans_text)s fan. Please check its wiring and connection.</source>
        <translation type="unfinished">Niewłaściwe obroty wentylatora: %(failed_fans_text)s . Sprawdź okablowanie i podłączenie.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="10"/>
        <source>Measured resin volume %(volume_ml)d ml is lower than required for this print. Refill the tank and restart the print.</source>
        <translation type="unfinished">Zmierzona objętość żywicy %(volume_ml)d ml jest niższa od wymaganej do tego wydruku. Uzupełnij żywicę i zrestartuj wydruk.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="11"/>
        <source>Measured resin volume %(volume_ml)d ml is higher than required for this print. Make sure that the resin level does not exceed the 100% mark and restart the print.</source>
        <translation type="unfinished">Zmierzona objętość żywicy %(volume_ml)d ml jest wyższa od wymaganej do tego wydruku. Upewnij się, że poziom nie przekracza znacznika 100% i zrestartuj wydruk.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="12"/>
        <source>The printer is not calibrated. Please run the Wizard first.</source>
        <translation type="unfinished">Drukarka nie została skalibrowana. Najpierw uruchom Asystenta.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="13"/>
        <source>Failed to reach the tower endstop, check that the tower motor is connected and repeat the action.</source>
        <translation type="unfinished">Niepowodzenie aktywacji krańcówki kolumny. Sprawdź, czy silnik jest podłączony i powtórz czynność.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="14"/>
        <source>Failed to reach the tilt endstop, check that the cable is connected and repeat the action.</source>
        <translation type="unfinished">Niepowodzenie aktywacji krańcówki tiltu. Sprawdź, czy przewód jest podłączony i powtórz czynność.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="15"/>
        <source>Tower axis check failed!

Current position: %(position_nm)d nm

Check if the ballscrew can move smoothly in its entire range.</source>
        <translation type="unfinished">Niepowodzenie testu osi kolumny!

Obecna pozycja: %(position_nm)d nm

Sprawdź, czy śruba kulowa może poruszać się w całym zakresie.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="16"/>
        <source>Tilt axis check failed!

Current position: %(position)d steps

Check if the tilt can move smoothly in its entire range.</source>
        <translation type="unfinished">Niepowodzenie testu osi tiltu!

Obecna pozycja: %(position)d w krokach

Sprawdź, czy tilt może poruszać się w całym zakresie.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="17"/>
        <source>Display test failed, check the connection between the display and the A64 board.</source>
        <translation type="unfinished">Niepowodzenie testu wyświetlacza. Sprawdź połączenie wyświetlacza z płytą A64.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="18"/>
        <source>Invalid tilt alignment position. Check the tilt mechanism and repeat the action.</source>
        <translation type="unfinished">Niewłaściwa pozycja wyrównania tiltu. Przejrzyj mechanizm i spróbuj ponownie.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="19"/>
        <source>RPM of %(fan)s not in range!

Check if the fan is connected correctly.

RPM data: %(rpm)s
Average: %(avg)s</source>
        <translation type="unfinished">%(fan)s - obroty poza zakresem!

Sprawdź, czy wentylator jest prawidłowo podłączony.

Obr./min: %(rpm)s
Średnia: %(avg)s</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="20"/>
        <source>Tower not at the expected position.

Are the platform and tank mounted and secured correctly?</source>
        <translation type="unfinished">Kolumna w nieoczekiwanej pozycji.

Czy platforma i zbiornik są zamocowane w odpowiednich miejscach?</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="21"/>
        <source>Measuring the resin failed. Check the presence of the platform and the amount of resin in the tank.</source>
        <translation type="unfinished">Niepowodzenie pomiaru poziomu żywicy. Sprawdź obecność platformy i ilość żywicy w zbiorniku.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="22"/>
        <source>The %(sensor)s sensor failed. Check the wiring and connection.</source>
        <translation type="unfinished">Błąd czujnika %(sensor)s . Sprawdź okablowanie i podłączenie.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="23"/>
        <source>UV LED is overheating! Check whether the heatsink is installed correctly.</source>
        <translation type="unfinished">UV LED się przegrzewa! Sprawdź, czy radiator jest zamontowany prawidłowo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="24"/>
        <source>A64 temperature is too high. Measured: %(temperature).1f °C! Shutting down in 10 seconds...</source>
        <translation type="unfinished">Temperatura A64 jest zbyt wysoka. Zmierzono %(temperature).1f °C! Wyłączenie za 10 sekund...</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="25"/>
        <source>%(sensor)s not in range! Measured temperature: %(temperature).1f °C. Keep the printer out of direct sunlight at room temperature (18 - 32 °C).</source>
        <translation type="unfinished">%(sensor)s poza zakresem! Zmierzona temperatura: %(temperature).1f °C. Trzymaj drukarkę z dala od bezp. światła słonecznego w temperaturze pokojowej (18-32°C).</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="26"/>
        <source>Reading of UV LED temperature has failed! This value is essential for the UV LED lifespan and printer safety. Please contact tech support! Current print job will be canceled.</source>
        <translation type="unfinished">Niepowodzenie odczytu temperatury UV LED! Pomiar jest konieczny do niezawodnego działania UV LED i bezpieczeństwa drukarki. Skontaktuj się ze Wsparciem Klienta! Obecne zadanie zostanie anulowane.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="27"/>
        <source>Wrong revision of the Motion Controller (MC). Contact our support.</source>
        <translation type="unfinished">Nieprawidłowa rewizja Kontrolera Ruchu (MC). Skontaktuj się ze Wsparciem Klienta.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="28"/>
        <source>The Motion Controller (MC) has encountered an unexpected error. Restart the printer.</source>
        <translation type="unfinished">Kontroler Ruchu (MC) napotkał nieznany błąd. Zrestartuj drukarkę.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="29"/>
        <source>The resin sensor was not triggered. Check whether the tank and the platform are properly secured. Inspect the wiring of the sensor.</source>
        <translation type="unfinished">Czujnik żywicy nie został wyzwolony. Sprawdź czy zbiornik i platforma są prawidłowo zamocowane. Przejrzyj okablowanie czujnika.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="30"/>
        <source>The printer is not UV calibrated. Connect the UV calibrator and complete the calibration.</source>
        <translation type="unfinished">UV drukarki nie zostało skalibrowane. Podłącz kalibrator UV i ukończ kalibrację.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="31"/>
        <source>UV LED voltages differ too much. The LED module might be faulty. Contact our support.</source>
        <translation type="unfinished">Zakres różnic napięć UV LED jest zbyt duży. Moduł LED może być uszkodzony. Skontaktuj się ze Wsparciem Klienta.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="32"/>
        <source>Speaker test failed, check the connection and repeat the action.</source>
        <translation type="unfinished">Niepowodzenie testu głośnika. Sprawdź połączenie i spróbuj ponownie.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="33"/>
        <source>The UV LED calibrator is not detected. Check the connection and try again.</source>
        <translation type="unfinished">Kalibrator UV LED nie został wykryty. Sprawdź połączenie i spróbuj ponownie.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="34"/>
        <source>Cannot connect to the UV LED calibrator. Check the connection and try again.</source>
        <translation type="unfinished">Nie można połączyć się z kalibratorem UV LED. Sprawdź podłączenie i spróbuj ponownie.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="35"/>
        <source>Communication with the UV LED calibrator has failed. Check the connection and try again.</source>
        <translation type="unfinished">Niepowodzenie komunikacji z kalibratorem UV LED. Sprawdź podłączenie i spróbuj ponownie.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="36"/>
        <source>The UV LED calibrator detected some light on a dark display. This means there is a light &apos;leak&apos; under the UV calibrator, or your display does not block the UV light enough. Check the UV calibrator placement on the screen or replace the exposure display.</source>
        <translation type="unfinished">Kalibrator UV LED wykrywa pewną ilość światła przebijającego się przez maskę ekranową. To oznacza, że światło wdziera się w jakiś sposób pod kalibrator lub Twój ekran nie blokuje światła UV w dostatecznym stopniu. Sprawdź umiejscowienie kalibratora UV na ekranie lub wymień wyświetlacz naświetlający.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="37"/>
        <source>The UV LED calibrator failed to read expected UV light intensity. Check the UV calibrator placement on the screen.</source>
        <translation type="unfinished">Kalibrator UV LED nie był w stanie uzyskać prawidłowego odczytu intensywności światła UV. Sprawdź umiejscowienie kalibratora UV na ekranie.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="38"/>
        <source>Unknown UV LED calibrator error code: %(code)d</source>
        <translation type="unfinished">Nieznany kod błędu kalibratora UV LED: %(code)d</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="39"/>
        <source>Requested intensity cannot be reached by min. allowed PWM.</source>
        <translation type="unfinished">Nie można osiągnąć zadanej intensywności przez minimalne dopuszczalne PWM.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="40"/>
        <source>Requested intensity cannot be reached by max. allowed PWM.</source>
        <translation type="unfinished">Nie można osiągnąć zadanej intensywności przez maksymalne dopuszczalne PWM.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="41"/>
        <source>Correct settings were found, but the standard deviation
(%(found).1f) is greater than the allowed value (%(allowed).1f).
Verify the UV LED calibrator&apos;s position and calibration, then try again.</source>
        <translation type="unfinished">Odnaleziono prawidłowe ustawienia, jednak odchylenie standardowe
(%(found).1f) jest większe od dopuszczalnego (%(allowed).1f).
Sprawdź pozycję kalibratora UV LED i spróbuj ponownie.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="42"/>
        <source>Communication with the Booster board failed. Check the connection and restart the printer.</source>
        <translation type="unfinished">Niepowodzenie komunikacji ze wzmacniaczem. Sprawdź podłączenie i zrestartuj drukarkę.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="43"/>
        <source>The UV LED panel is not detected. Check the connection.</source>
        <translation type="unfinished">Nie wykryto panelu UV LED. Sprawdź podłączenie.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="44"/>
        <source>A part of the LED panel is disconnected. Check the connection and the LED panel.</source>
        <translation type="unfinished">Część panelu UV LED jest odłączona. Sprawdź podłączenie i panel LED.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="45"/>
        <source>The printer model was not detected. Check the connection of the exposure display and restart the printer.</source>
        <translation type="unfinished">Nie wykryto modelu drukarki. Sprawdź podłączenie wyświetlacza druku i zrestartuj drukarkę.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="46"/>
        <source>Cannot send factory config to the database (MQTT)! Check the network connection. Please, contact support.</source>
        <translation type="unfinished">Nie można wysłać fabrycznej konfiguracji do bazy danych (MQTT)! Sprawdź połączenie z Internetem. Skontaktuj się ze wsparciem klienta.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="47"/>
        <source>The printer is not connected to the internet. Check the connection in the Settings.</source>
        <translation type="unfinished">Drukarka nie jest połączona z Internetem. Sprawdź połączenie w Ustawieniach.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="48"/>
        <source>Connection to Prusa servers failed, please try again later.</source>
        <translation type="unfinished">Niepowodzenie połączenia z serwerami Prusa. Spróbuj ponownie później.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="49"/>
        <source>The download failed. Check the connection to the internet and try again.</source>
        <translation type="unfinished">Pobieranie nieudane. Sprawdź połączenie z Internetem i spróbuj ponownie.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="50"/>
        <source>Please turn on the HTTP digest (which is the recommended security option) or update the API key. You can find it in Settings &gt; Network &gt; Login credentials.</source>
        <translation type="unfinished">Włącz HTTP digest (zalecana opcja zabezpieczeń) lub popraw klucz API. Znajdziesz go w sekcji Ustawienia -&gt; Sieć -&gt; Dane logowania.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="51"/>
        <source>The printer uses HTTP digest security. Please enable it also in your slicer (recommended), or turn off this security option in the printer. You can find it in Settings &gt; Network &gt; Login credentials.</source>
        <translation type="unfinished">Ta drukarka używa zabezpieczeń HTTP digest. Włącz je również w oprogramowaniu do cięcia (zalecane) lub wyłącz tę opcję w ustawieniach drukarki, w sekcji Ustawienia -&gt; Sieć -&gt; Dane logowania.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="52"/>
        <source>This request is not compatible with the Prusa remote API. See our documentation for more details.</source>
        <translation type="unfinished">To zapytanie nie jest kompatybilne ze zdalnym interfejsem API Prusa. Zapoznaj się z dokumentacją.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="53"/>
        <source>No problem detected. You can continue using the printer.</source>
        <translation type="unfinished">Nie wykryto problemów. Możesz kontynuować używanie drukarki.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="54"/>
        <source>An unexpected error has occurred :-(.
If print job is in progress, it should be finished.
You can turn the printer off by pressing the front power button.
See the handbook to learn how to save a log file and send it to us.</source>
        <translation type="unfinished">Wystąpił nieznany błąd :-(
Jeśli trwa drukowanie, powinno zostać dokończone.
Możesz wyłączyć drukarkę przednim przyciskiem.
Sprawdź w &quot;Podręczniku&quot; jak zapisać log i prześlij go do nas.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="55"/>
        <source>Image preloader did not finish successfully!</source>
        <translation type="unfinished">Nie udało się wstępnie załadować obrazu!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="56"/>
        <source>Opening the project failed, the file may be corrupted. Re-slice or re-export the project and try again.</source>
        <translation type="unfinished">Nie udało się otworzyć pliku projektu - plik może być uszkodzony. Potnij go od nowa lub wyeksportuj i spróbuj ponownie.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="57"/>
        <source>Failed to read the configuration file. Try to reset the printer. If the problem persists, contact our support.</source>
        <translation type="unfinished">Niepowodzenie odczytu pliku konfiguracyjnego. Spróbuj zrestartować drukarkę. Jeśli problem pozostaje, skontaktuj się ze wsparciem klienta.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="58"/>
        <source>Another action is already running. Finish this action directly using the printer&apos;s touchscreen.</source>
        <translation type="unfinished">Inne zadanie jest już w toku. Zakończ trwające zadanie bezpośrednio z ekranu dotykowego drukarki.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="59"/>
        <source>Internal error (DBUS mapping failed), restart the printer. Contact support if the problem persists.</source>
        <translation type="unfinished">Błąd wewnętrzny (niepowodzenie mapowania DBUS). Zrestartuj drukarkę. Skontaktuj się z naszym Wsparciem Klienta, jeśli problem się powtórzy.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="60"/>
        <source>Error, there is no file to reprint.</source>
        <translation type="unfinished">Błąd: brak pliku do ponownego wydrukowania.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="61"/>
        <source>The wizard did not finish successfully!</source>
        <translation type="unfinished">Asystent nie zakończył pracy z powodzeniem!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="62"/>
        <source>The calibration did not finish successfully! Run the calibration again.</source>
        <translation type="unfinished">Kalibracja nie została zakończona pomyślnie! Uruchom kalibrację ponownie.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="63"/>
        <source>The automatic UV LED calibration did not finish successfully! Run the calibration again.</source>
        <translation type="unfinished">Automatyczna kalibracja UV LED nie została zakończona pomyślnie! Uruchom kalibrację ponownie.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="64"/>
        <source>UV intensity not set. Please run the UV calibration before starting a print.</source>
        <translation type="unfinished">Intensywność UV nie została ustawiona. Uruchom kalibrację UV przed drukowaniem.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="65"/>
        <source>Cannot set the update channel. Restart the printer and try again.</source>
        <translation type="unfinished">Nie można ustawić kanału aktualizacji. Zrestartuj drukarkę i spróbuj ponownie.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="66"/>
        <source>Cannot get the update channel. Restart the printer and try again.</source>
        <translation type="unfinished">Nie można pobrać kanału aktualizacji. Zrestartuj drukarkę i spróbuj ponownie.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="67"/>
        <source>The print job cancelled by the user.</source>
        <translation type="unfinished">Drukowanie zostało anulowane przez użytkownika.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="68"/>
        <source>Internal memory is full. Delete some of your projects first.</source>
        <translation type="unfinished">Pamięć wewnętrzna jest pełna. Usuń niektóre projekty przed kontynuacją.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="69"/>
        <source>The admin menu is not available.</source>
        <translation type="unfinished">Menu admina jest niedostępne.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="70"/>
        <source>Cannot find the selected file!</source>
        <translation type="unfinished">Nie można wczytać wybranego pliku!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="71"/>
        <source>File has an invalid extension! See the article for supported file extensions.</source>
        <translation type="unfinished">Plik ma niewłaściwe rozszerzenie! Obsługiwane typy plików znajdziesz w artykule.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="72"/>
        <source>File already exists! Delete it in the printer first and try again.</source>
        <translation type="unfinished">Plik już istnieje! Skasuj go z drukarki i spróbuj ponownie.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="73"/>
        <source>The project file is invalid!</source>
        <translation type="unfinished">Plik projektu jest błędny!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="74"/>
        <source>This Wizard cannot be canceled, finish the steps first.</source>
        <translation type="unfinished">Nie można przerwać pracy Asystenta. Zakończ najpierw wszystkie etapy.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="75"/>
        <source>Examples (any projects) are missing in the user storage. Redownload them from the &apos;Settings&apos; menu.</source>
        <translation type="unfinished">Brak plików przykładowych (jakichkolwiek projektów) w pamięci użytkownika. Pobierz je ponownie z menu &quot;Ustawienia&quot;.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="76"/>
        <source>Failed to load fans and LEDs factory calibration.</source>
        <translation type="unfinished">Niepowodzenie wczytywania fabrycznych ustawień kalibracji wentylatorów i LED.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="77"/>
        <source>Failed to serialize Wizard data. Restart the printer and try again.</source>
        <translation type="unfinished">Niepowodzenie serializowania danych Asystenta. Zrestartuj drukarkę i spróbuj ponownie.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="78"/>
        <source>Failed to save Wizard data. Restart the printer and try again.</source>
        <translation type="unfinished">Niepowodzenie zapisu danych Asystenta. Zrestartuj drukarkę i spróbuj ponownie.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="79"/>
        <source>Serial numbers in wrong format! A64: %(a64)s MC: %(mc)s Please contact tech support!</source>
        <translation type="unfinished">Numery seryjne mają niewłaściwy format! A64: %(a64)s MC: %(mc)s Skontaktuj się ze Wsparciem Klienta!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="80"/>
        <source>No USB storage present</source>
        <translation type="unfinished">Brak nośnika USB</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="81"/>
        <source>Failed to change the log level (detail). Restart the printer and try again.</source>
        <translation type="unfinished">Niepowodzenie zmiany poziomu logowania (szczegółowości). Zrestartuj drukarkę i spróbuj ponownie.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="82"/>
        <source>Saving the new factory default value failed. Restart the printer and try again.</source>
        <translation type="unfinished">Niepowodzenie zapisywania nowych domyślnych wartości fabrycznych. Zrestartuj drukarkę i spróbuj ponownie.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="83"/>
        <source>Error displaying test image.</source>
        <translation type="unfinished">Błąd wyświetlania obrazu testowego.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="84"/>
        <source>No calibration data to show!</source>
        <translation type="unfinished">Brak danych kalibracyjnych do pokazania!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="85"/>
        <source>Data is from unknown UV LED sensor!</source>
        <translation type="unfinished">Dane pochodzą z nieznanego czujnika UV LED!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="86"/>
        <source>The update of the firmware failed! Restart the printer and try again.</source>
        <translation type="unfinished">Niepowodzenie aktualizacji firmware! Zrestartuj drukarkę i spróbuj ponownie.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="87"/>
        <source>No display usage data to show</source>
        <translation type="unfinished">Brak danych o użytkowaniu do wyświetlenia</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="88"/>
        <source>Failed to set hostname</source>
        <translation type="unfinished">Niepowodzenie ustawienia nazwy hosta</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="89"/>
        <source>Cannot import profile</source>
        <translation type="unfinished">Nie można zaimportować profilu</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="90"/>
        <source>Cannot export profile</source>
        <translation type="unfinished">Nie można wyeksportować profilu</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="91"/>
        <source>Opening the project failed. The file is possibly corrupted. Please re-slice or re-export the project and try again.</source>
        <translation type="unfinished">Nie udało się otworzyć pliku projektu - plik prawdopodobnie jest uszkodzony. Potnij go od nowa lub wyeksportuj i spróbuj ponownie.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="92"/>
        <source>The project must have at least one layer</source>
        <translation type="unfinished">Projekt musi mieć co najmniej jedną warstwę</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="93"/>
        <source>Opening the project failed. The file is corrupted. Please re-slice or re-export the project and try again.</source>
        <translation type="unfinished">Nie udało się otworzyć pliku projektu - plik jest uszkodzony. Potnij go od nowa lub wyeksportuj i spróbuj ponownie.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="94"/>
        <source>Analysis of the project failed</source>
        <translation type="unfinished">Analiza projektu nieudana</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="95"/>
        <source>Calibration project is invalid</source>
        <translation type="unfinished">Projekt kalibracyjny jest nieprawidłowy</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="96"/>
        <source>This project was prepared for a different printer</source>
        <translation type="unfinished">Projekt został przygotowany dla innej drukarki</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="97"/>
        <source>Removing this project is not possible. The project is locked by a print job.</source>
        <translation type="unfinished">Usunięcie tego projektu jest niemożliwe. Projekt jest zablokowany przez trwające zadanie.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="98"/>
        <source>The directory is not empty.</source>
        <translation type="unfinished">Katalog nie jest pusty.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="99"/>
        <source>The language is not set. Go to Settings -&gt; Language &amp; Time -&gt; Set Language and pick preferred language.</source>
        <translation type="unfinished">Nie ustawiono języka. Przejdź do Ustawienia -&gt; Język i czas -&gt; Język i wybierz preferowany.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="100"/>
        <source>Exposure screen that is currently connected has already been used on this printer. This screen was last used for approximately %(counter_h)d hours.

If you do not want to use this screen: turn the printer off, replace the screen and turn the printer back on.</source>
        <translation type="unfinished">Aktualnie podłączony ekran naświetlający był już używany w tej drukarce. Ten ekran pracował przez około %(counter_h)d godzin.

Jeśli nie chcesz używać tego ekranu: wyłącz drukarkę, wymień ekran i włącz drukarkę ponownie.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="101"/>
        <source>The printer has booted from an alternative slot due to failed boot attempts using the primary slot.
Update the printer with up-to-date firmware ASAP to recover the primary slot.
This usually happens after a failed update, or due to a hardware failure. Printer settings may have been reset.</source>
        <translation type="unfinished">Drukarka została uruchomiona z drugiego slotu przez nieudane próby uruchomienia z pierwszego.
Natychmiast zaktualizuj drukarkę najnowszą wersją firmware, aby odzyskać pierwszy slot.
Dzieje się to zazwyczaj po nieudanej aktualizacji lub przez błąd sprzętowy. Ustawienia drukarki mogły zostać zresetowane.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="102"/>
        <source>There is no warning</source>
        <translation type="unfinished">Nie ma ostrzeżenia</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="103"/>
        <source>An unknown warning has occured. Restart the printer and try again. Contact our tech support if the problem persists.</source>
        <translation type="unfinished">Wystąpiło nieznane ostrzeżenie. Zrestartuj drukarkę i spróbuj ponownie. Skontaktuj się ze Wsparciem Klienta, jeśli problem się powtórzy.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="104"/>
        <source>The ambient temperature is too high, the print can continue, but it might fail.</source>
        <translation type="unfinished">Temperatura otoczenia jest zbyt wysoka. Można kontynuować drukowanie, ale może się to nie udać.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="105"/>
        <source>The ambient temperature is too low, the print can continue, but it might fail.</source>
        <translation type="unfinished">Temperatura otoczenia jest zbyt niska. Można kontynuować drukowanie, ale może się to nie udać.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="106"/>
        <source>The internal memory is full, project cannot be copied. You can continue printing. However, you must not remove the USB drive during the print, otherwise the process will fail.</source>
        <translation type="unfinished">Nie można skopiować projektu przez brak miejsca w pamięci wewnętrznej. Możesz kontynuować drukowanie, jednak nie możesz wyciągnąć pamięci USB w trakcie, ponieważ spowoduje to jego przerwanie.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="107"/>
        <source>The model was sliced for a different printer model. Reslice the model using the correct settings.</source>
        <translation type="unfinished">Model został pocięty dla innego modelu drukarki. Potnij model ponownie używając poprawnych ustawień.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="108"/>
        <source>The amount of resin in the tank is not enough for the current project. Adding more resin will be required during the print.</source>
        <translation type="unfinished">Ilość żywicy w zbiorniku jest niewystarczająca do ukończenia wybranego projektu. Konieczne będzie jej uzupełnienie w trakcie drukowania.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="109"/>
        <source>The print parameters are out of range of the printer, the system can try to fix the project. Proceed?</source>
        <translation type="unfinished">Parametry druku są poza zakresem drukarki. System może podjąć próbę naprawienia projektu. Kontynuować?</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="110"/>
        <source>Per-partes print not available.</source>
        <translation type="unfinished">Per-partes print not available.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="111"/>
        <source>Print mask is missing.</source>
        <translation type="unfinished">Brak maski wydruku.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="112"/>
        <source>Object was cropped because it does not fit the print area.</source>
        <translation type="unfinished">Obiekt został przycięty, ponieważ nie mieści się w obszarze roboczym.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="113"/>
        <source>The model was sliced for a different printer variant %(project_variant)s. Your printer variant is %(printer_variant)s.</source>
        <translation type="unfinished">Model został pocięty dla innego wariantu drukarki - %(project_variant)s. Wariant Twojej drukarki to %(printer_variant)s.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="114"/>
        <source>Measured resin volume is too low. The print can continue, however, a refill might be required.</source>
        <translation type="unfinished">Zmierzona ilość żywicy jest zbyt niska. Drukowanie można kontynuować, jednak konieczne może być uzupełnienie w trakcie.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="116"/>
        <source>Incorrect RPM reading of the %(failed_fans_text)s fan. Please check its wiring. The print may continue, however, there&apos;s a risk of overheating.</source>
        <translation type="unfinished">Nieprawidłowy odczyt prędkości obrotowej wentylatora: %(failed_fans_text)s. Sprawdź jego okablowanie. Można kontynuować drukowanie, jednak istnieje ryzyko przegrzania.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="119"/>
        <source>TILT HOMING FAILED</source>
        <translation type="unfinished">NIEPOWODZENIE BAZOWANIA TILT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="120"/>
        <source>TOWER HOMING FAILED</source>
        <translation type="unfinished">NIEPOWODZENIE BAZOWANIA KOLUMNY</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="121"/>
        <source>TOWER MOVING FAILED</source>
        <translation type="unfinished">BŁĄD RUCHU KOLUMNY</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="122"/>
        <source>FAN FAILURE</source>
        <translation type="unfinished">BŁĄD WENTYLATORA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="123"/>
        <source>RESIN TOO LOW</source>
        <translation type="unfinished">ZBYT NISKI POZIOM ŻYWICY</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="124"/>
        <source>RESIN TOO HIGH</source>
        <translation type="unfinished">ZBYT WYSOKI POZIOM ŻYWICY</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="125"/>
        <source>CALIBRATION ERROR</source>
        <translation type="unfinished">BŁĄD KALIBRACJI</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="126"/>
        <source>TOWER ENDSTOP NOT REACHED</source>
        <translation type="unfinished">NIE AKTYWOWANO KRAŃCÓWKI KOLUMNY</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="127"/>
        <source>TILT ENDSTOP NOT REACHED</source>
        <translation type="unfinished">NIE AKTYWOWANO KRAŃCÓWKI TILT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="128"/>
        <source>TOWER AXIS CHECK FAILED</source>
        <translation type="unfinished">NIEPOWODZENIE TESTU OSI KOLUMNY</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="129"/>
        <source>TILT AXIS CHECK FAILED</source>
        <translation type="unfinished">NIEPOWODZENIE TESTU OSI TILT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="130"/>
        <source>DISPLAY TEST FAILED</source>
        <translation type="unfinished">NIEPOWODZENIE TESTU WYŚWIETLACZA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="131"/>
        <source>INVALID TILT ALIGN POSITION</source>
        <translation type="unfinished">NIEWŁAŚCIWA POZYCJA TILTU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="132"/>
        <source>FAN RPM OUT OF TEST RANGE</source>
        <translation type="unfinished">OBROTY WENTYLATORA POZA ZAKRESEM</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="133"/>
        <source>TOWER POSITION ERROR</source>
        <translation type="unfinished">BŁĄD POZYCJI KOLUMNY</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="134"/>
        <source>RESIN MEASURING FAILED</source>
        <translation type="unfinished">NIEPOWODZENIE POMIARU POZIOMU ŻYWICY</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="135"/>
        <source>TEMPERATURE SENSOR FAILED</source>
        <translation type="unfinished">BŁĄD CZUJNIKA TEMPERATURY</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="136"/>
        <source>UVLED HEAT SINK FAILED</source>
        <translation type="unfinished">BŁĄD RADIATORA UV LED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="137"/>
        <source>A64 OVERHEAT</source>
        <translation type="unfinished">PRZEGRZANIE A64</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="138"/>
        <source>TEMPERATURE OUT OF RANGE</source>
        <translation type="unfinished">TEMPERATURA POZA ZAKRESEM</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="139"/>
        <source>UV LED TEMP. ERROR</source>
        <translation type="unfinished">BŁĄD TEMPERATURY UV LED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="140"/>
        <source>MC WRONG REVISION</source>
        <translation type="unfinished">NIEPRAWIDŁOWA REWIZJA MC</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="141"/>
        <source>UNEXPECTED MC ERROR</source>
        <translation type="unfinished">NIEOCZEKIWANY BŁĄD MC</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="142"/>
        <source>RESIN SENSOR ERROR</source>
        <translation type="unfinished">BŁĄD CZUJNIKA ŻYWICY</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="143"/>
        <source>PRINTER NOT UV CALIBRATED</source>
        <translation type="unfinished">UV DRUKARKI NIE SKALIBROWANY</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="144"/>
        <source>UVLED VOLTAGE ERROR</source>
        <translation type="unfinished">BŁĄD NAPIĘCIA UV LED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="145"/>
        <source>SPEAKER TEST FAILED</source>
        <translation type="unfinished">NIEPOWODZENIE TESTU GŁOŚNIKA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="146"/>
        <source>UV LED CALIBRATOR NOT DETECTED</source>
        <translation type="unfinished">NIE WYKRYTO KALIBRATORA UV LED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="147"/>
        <source>UV LED CALIBRATOR CONNECTION ERROR</source>
        <translation type="unfinished">BŁĄD POŁĄCZENIA Z KALIBRATOREM UV LED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="148"/>
        <source>UV LED CALIBRATOR LINK ERROR</source>
        <translation type="unfinished">BŁĄD POŁĄCZENIA Z KALIBRATOREM UV LED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="149"/>
        <source>UV LED CALIBRATOR ERROR</source>
        <translation type="unfinished">BŁĄD KALIBRATORA UV LED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="150"/>
        <source>UV LED CALIBRATOR READINGS ERROR</source>
        <translation type="unfinished">NIEPOWODZENIE ODCZYTU KALIBRATORA UV LED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="151"/>
        <source>UV LED CALIBRATOR UNKNONW ERROR</source>
        <translation type="unfinished">NIEZNANY BŁĄD KALIBRATORA UV LED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="152"/>
        <source>UV INTENSITY TOO HIGH</source>
        <translation type="unfinished">ZBYT WYSOKA INTENSYWNOŚĆ UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="153"/>
        <source>UV INTENSITY TOO LOW</source>
        <translation type="unfinished">ZBYT NISKA INTENSYWNOŚĆ UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="154"/>
        <source>UV CALIBRATION ERROR</source>
        <translation type="unfinished">BŁĄD KALIBRATORA UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="155"/>
        <source>BOOSTER BOARD PROBLEM</source>
        <translation type="unfinished">PROBLEM ZE WZMACNIACZEM</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="156"/>
        <source>Disconnected UV LED panel</source>
        <translation type="unfinished">Odłączony panel UV LED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="157"/>
        <source>Broken UV LED panel</source>
        <translation type="unfinished">Uszkodzony panel UV LED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="158"/>
        <source>Unknown printer model</source>
        <translation type="unfinished">Nieznany model drukarki</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="159"/>
        <source>MQTT UPLOAD FAILED</source>
        <translation type="unfinished">NIEPOWODZENIE PRZESYŁANIA MQTT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="160"/>
        <source>NO INTERNET CONNECTION</source>
        <translation type="unfinished">BRAK POŁĄCZENIA Z INTERNETEM</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="161"/>
        <source>CONNECTION FAILED</source>
        <translation type="unfinished">BŁĄD POŁĄCZENIA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="162"/>
        <source>DOWNLOAD FAILED</source>
        <translation type="unfinished">POBIERANIE NIEUDANE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="163"/>
        <source>INVALID API KEY</source>
        <translation type="unfinished">BŁĘDNY KLUCZ API</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="164"/>
        <source>UNAUTHORIZED</source>
        <translation type="unfinished">BRAK AUTORYZACJI</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="165"/>
        <source>REMOTE API ERROR</source>
        <translation type="unfinished">BŁĄD ZDALNEGO API</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="166"/>
        <source>PRINTER IS OK</source>
        <translation type="unfinished">DRUKARKA OK</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="167"/>
        <source>UNEXPECTED ERROR</source>
        <translation type="unfinished">NIEOCZEKIWANY BŁĄD</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="168"/>
        <source>PRELOAD FAILED</source>
        <translation type="unfinished">WSTĘPNE WCZYTANIE NIEUDANE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="169"/>
        <source>OPENING PROJECT FAILED</source>
        <translation type="unfinished">NIE UDAŁO SIĘ OTWORZYĆ PROJEKTU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="170"/>
        <source>CONFIG FILE READ ERROR</source>
        <translation type="unfinished">BŁĄD ODCZYTU PLIKU KONFIGURACYJNEGO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="171"/>
        <source>PRINTER IS BUSY</source>
        <translation type="unfinished">DRUKARKA JEST ZAJĘTA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="172"/>
        <source>INTERNAL ERROR</source>
        <translation type="unfinished">BŁĄD WEWNĘTRZNY</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="173"/>
        <source>NO FILE TO REPRINT</source>
        <translation type="unfinished">BRAK PLIKU DO PONOWNEGO WYDRUKOWANIA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="174"/>
        <source>WIZARD FAILED</source>
        <translation type="unfinished">BŁĄD ASYSTENTA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="175"/>
        <source>CALIBRATION FAILED</source>
        <translation type="unfinished">NIEPOWODZENIE KALIBRACJI</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="176"/>
        <source>UV CALIBRATION FAILED</source>
        <translation type="unfinished">NIEPOWODZENIE KALIBRACJI UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="177"/>
        <source>UV INTENSITY ERROR</source>
        <translation type="unfinished">BŁĄD INTENSYWNOŚCI UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="178"/>
        <source>SETTING UPDATE CHANNEL FAILED</source>
        <translation type="unfinished">NIEPOWODZENIE ZMIANY KANAŁU AKTUALIZACJI</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="179"/>
        <source>UPDATE CHANNEL FAILED</source>
        <translation type="unfinished">BŁĄD KANAŁU AKTUALIZACJI</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="180"/>
        <source>PRINT JOB CANCELLED</source>
        <translation type="unfinished">ANULOWANO DRUK</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="181"/>
        <source>INTERNAL MEMORY FULL</source>
        <translation type="unfinished">PAMIĘĆ WEWNĘTRZNA JEST PEŁNA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="182"/>
        <source>ADMIN NOT AVAILABLE</source>
        <translation type="unfinished">ADMIN NIEDOSTĘPNY</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="183"/>
        <source>FILE NOT FOUND</source>
        <translation type="unfinished">NIE ZNALEZIONO PLIKU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="184"/>
        <source>INVALID FILE EXTENSION</source>
        <translation type="unfinished">NIEWŁAŚCIWE ROZSZERZENIE PLIKU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="185"/>
        <source>FILE ALREADY EXISTS</source>
        <translation type="unfinished">PLIK JUŻ ISTNIEJE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="186"/>
        <source>INVALID PROJECT</source>
        <translation type="unfinished">BŁĘDNY PROJEKT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="187"/>
        <source>YOU SHALL NOT PASS</source>
        <translation type="unfinished">NIE POZWOLĘ CI PRZEJŚĆ</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="188"/>
        <source>PRINT EXAMPLES MISSING</source>
        <translation type="unfinished">BRAK PLIKÓW PRZYKŁADOWYCH</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="189"/>
        <source>CALIBRATION LOAD FAILED</source>
        <translation type="unfinished">NIEPOWODZENIE WCZYTANIA DANYCH KALIBRACJI</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="190"/>
        <source>DATA PREPARATION FAILURE</source>
        <translation type="unfinished">NIEPOWODZENIE PRZYGOTOWYWANIA DANYCH</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="191"/>
        <source>WIZARD DATA FAILURE</source>
        <translation type="unfinished">BŁĄD DANYCH ASYSTENTA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="192"/>
        <source>SERIAL NUMBER ERROR</source>
        <translation type="unfinished">BŁĄD NUMERU SERYJNEGO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="193"/>
        <source>USB DRIVE NOT DETECTED</source>
        <translation type="unfinished">NIE WYKRYTO PAMIĘCI USB</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="194"/>
        <source>SETTING LOG DETAIL FAILED</source>
        <translation type="unfinished">NIEPOWODZENIE USTAWIENIA SZCZEGÓŁOWOŚCI LOGA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="195"/>
        <source>DATA OVERWRITE FAILED</source>
        <translation type="unfinished">NIEPOWODZENIE NADPISYWANIA DANYCH</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="196"/>
        <source>DISPLAY TEST ERROR</source>
        <translation type="unfinished">BŁĄD TESTU WYŚWIETLACZA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="197"/>
        <source>NO UV CALIBRATION DATA</source>
        <translation type="unfinished">BRAK DANYCH KALIBRACYJNYCH UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="198"/>
        <source>UV DATA EROR</source>
        <translation type="unfinished">BŁĄD DANYCH UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="199"/>
        <source>FIRMWARE UPDATE FAILED</source>
        <translation type="unfinished">NIEPOWODZENIE AKTUALIZACJI FIRMWARE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="200"/>
        <source>Display usage error</source>
        <translation type="unfinished">Błąd użycia wyświetlacza</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="201"/>
        <source>HOSTNAME ERROR</source>
        <translation type="unfinished">BŁĄD NAZWY HOSTA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="202"/>
        <source>PROFILE IMPORT ERROR</source>
        <translation type="unfinished">BŁĄD IMPORTOWANIA PROFILU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="203"/>
        <source>PROFILE EXPORT ERROR</source>
        <translation type="unfinished">BŁĄD EKSPORTOWANIA PROFILU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="204"/>
        <source>CANNOT READ PROJECT</source>
        <translation type="unfinished">NIE MOŻNA ODCZYTAĆ PROJEKTU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="205"/>
        <source>NOT ENOUGHT LAYERS</source>
        <translation type="unfinished">ZA MAŁO WARSTW</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="206"/>
        <source>PROJECT IS CORRUPTED</source>
        <translation type="unfinished">PROJEKT JEST USZKODZONY</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="207"/>
        <source>PROJECT ANALYSIS FAILED</source>
        <translation type="unfinished">NIEPOWODZENIE ANALIZY PROJEKTU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="208"/>
        <source>CALIBRATION PROJECT IS INVALID</source>
        <translation type="unfinished">PROJEKT KALIBRACYJNY JEST NIEPRAWIDŁOWY</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="209"/>
        <source>WRONG PRINTER MODEL</source>
        <translation type="unfinished">NIEPRAWIDŁOWY MODEL DRUKARKI</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="210"/>
        <source>CANNOT REMOVE PROJECT</source>
        <translation type="unfinished">NIE MOŻNA USUNĄĆ PROJEKTU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="211"/>
        <source>DIRECTORY NOT EMPTY</source>
        <translation type="unfinished">KATALOG NIE JEST PUSTY</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="212"/>
        <source>LANGUAGE NOT SET</source>
        <translation type="unfinished">NIE USTAWIONO JĘZYKA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="213"/>
        <source>OLD EXPO PANEL</source>
        <translation type="unfinished">POPRZEDNI PANEL NAŚWIETLAJĄCY</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="214"/>
        <source>BOOTED SLOT CHANGED</source>
        <translation type="unfinished">ZMIENIONO SLOT ROZRUCHU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="215"/>
        <source>NO WARNING</source>
        <translation type="unfinished">BRAK OSTRZEŻENIA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="216"/>
        <source>UNKNOWN WARNING</source>
        <translation type="unfinished">NIEZNANE OSTRZEŻENIE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="217"/>
        <source>AMBIENT TEMP. TOO HIGH</source>
        <translation type="unfinished">ZBYT WYSOKA TEMP. OTOCZENIA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="218"/>
        <source>AMBIENT TEMP. TOO LOW</source>
        <translation type="unfinished">ZBYT NISKA TEMP. OTOCZENIA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="219"/>
        <source>CAN&apos;T COPY PROJECT</source>
        <translation type="unfinished">NIE MOŻNA SKOPIOWAĆ PROJEKTU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="220"/>
        <source>INCORRECT PRINTER MODEL</source>
        <translation type="unfinished">NIEWŁAŚCIWY MODEL DRUKARKI</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="221"/>
        <source>NOT ENOUGH RESIN</source>
        <translation type="unfinished">ZA MAŁO ŻYWICY</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="222"/>
        <source>PARAMETERS OUT OF RANGE</source>
        <translation type="unfinished">PARAMETRY POZA ZAKRESEM</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="223"/>
        <source>PERPARTES NOAVAIL WARNING</source>
        <translation type="unfinished">PERPARTES NOAVAIL WARNING</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="224"/>
        <source>MASK NOAVAIL WARNING</source>
        <translation type="unfinished">OSTRZEŻENIE MASKA NIEDOSTĘPNA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="225"/>
        <source>OBJECT CROPPED WARNING</source>
        <translation type="unfinished">OSTRZEŻENIE O PRZYCIĘCIU OBIEKTU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="226"/>
        <source>PRINTER VARIANT MISMATCH WARNING</source>
        <translation type="unfinished">OSTRZEŻENIE O NIEZGODNOŚCI WARIANTU DRUKARKI</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="227"/>
        <source>RESIN LOW</source>
        <translation type="unfinished">NISKI POZIOM ŻYWICY</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="228"/>
        <source>FAN WARNING</source>
        <translation type="unfinished">OSTRZEŻENIE WENTYLATORA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="229"/>
        <source>EXPECT OVERHEATING</source>
        <translation type="unfinished">OCZEKUJ PRZEGRZANIA</translation>
    </message>
</context>
<context>
    <name>NotificationProgressDelegate</name>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="89"/>
        <source>Error: %1</source>
        <translation type="unfinished">Błąd: %1</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="99"/>
        <source>Storage full</source>
        <translation type="unfinished">Pamięć pełna</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="101"/>
        <source>File not found</source>
        <translation type="unfinished">Nie znaleziono pliku</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="103"/>
        <source>Already exists</source>
        <translation type="unfinished">Już istnieje</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="105"/>
        <source>Invalid extension</source>
        <translation type="unfinished">Niewłaściwe rozszerzenie</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="107"/>
        <source>Can&apos;t read</source>
        <translation type="unfinished">Nie można odczytać</translation>
    </message>
</context>
<context>
    <name>PageAPSettings</name>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="34"/>
        <source>AP Settings</source>
        <translation type="unfinished">Ustawenia hotspotu</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="51"/>
        <source>(unchanged)</source>
        <translation type="unfinished">(niezmieniono)</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="68"/>
        <source>Stop AP</source>
        <translation type="unfinished">Wyłącz hotspot</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="136"/>
        <source>State:</source>
        <translation type="unfinished">Stan:</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="147"/>
        <source>Inactive</source>
        <translation type="unfinished">Nieaktywne</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="151"/>
        <source>SSID</source>
        <translation type="unfinished">SSID</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="173"/>
        <source>Password</source>
        <translation type="unfinished">Hasło</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="214"/>
        <source>Show Password</source>
        <translation type="unfinished">Pokaż hasło</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="230"/>
        <source>Security</source>
        <translation type="unfinished">Zabezpieczenia</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="245"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation type="unfinished">PSK musi zawierać przynajmniej 8 znaków.</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="251"/>
        <source>SSID must not be empty</source>
        <translation type="unfinished">SSID nie może być puste</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="263"/>
        <source>Start AP</source>
        <translation type="unfinished">Włącz hotspot</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="294"/>
        <source>Swipe for QRCode</source>
        <translation type="unfinished">Przesuń, aby wyświetlić kod QR</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="372"/>
        <source>Swipe for settings</source>
        <translation type="unfinished">Przesuń, aby przejść do ustawień</translation>
    </message>
</context>
<context>
    <name>PageAbout</name>
    <message>
        <location filename="../qml/PageAbout.qml" line="22"/>
        <source>About Us</source>
        <translation type="unfinished">O nas</translation>
    </message>
    <message>
        <location filename="../qml/PageAbout.qml" line="25"/>
        <source>To find out more about us please scan the QR code or use the link below:</source>
        <translation type="unfinished">Aby dowiedzieć się więcej o nas, zeskanuj kod QR lub użyj poniższego linku:</translation>
    </message>
    <message>
        <location filename="../qml/PageAbout.qml" line="29"/>
        <source>Prusa Research a.s.</source>
        <translation type="unfinished">Prusa Research a.s.</translation>
    </message>
</context>
<context>
    <name>PageAddWifiNetwork</name>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="31"/>
        <source>Add Hidden Network</source>
        <translation type="unfinished">Dodaj ukrytą sieć</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="62"/>
        <source>SSID</source>
        <translation type="unfinished">SSID</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="84"/>
        <source>PASS</source>
        <translation type="unfinished">PASS</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="129"/>
        <source>Show Password</source>
        <translation type="unfinished">Pokaż hasło</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="147"/>
        <source>Security</source>
        <translation type="unfinished">Zabezpieczenia</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="163"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation type="unfinished">PSK musi zawierać przynajmniej 8 znaków.</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="169"/>
        <source>SSID must not be empty.</source>
        <translation type="unfinished">SSID nie może być puste.</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="182"/>
        <source>Connect</source>
        <translation type="unfinished">Połącz</translation>
    </message>
</context>
<context>
    <name>PageAdminAPI</name>
    <message>
        <location filename="../qml/PageAdminAPI.qml" line="36"/>
        <source>Admin API</source>
        <translation type="unfinished">Admin API</translation>
    </message>
</context>
<context>
    <name>PageAskForPassword</name>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="66"/>
        <source>Please, enter the correct password for network &quot;%1&quot;</source>
        <translation type="unfinished">Wprowadź poprawne hasło dla sieci &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="82"/>
        <source>Password</source>
        <translation type="unfinished">Hasło</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="92"/>
        <source>(unchanged)</source>
        <translation type="unfinished">(niezmieniono)</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="128"/>
        <source>Show Password</source>
        <translation type="unfinished">Pokaż hasło</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="154"/>
        <source>Cancel</source>
        <translation type="unfinished">Anuluj</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="166"/>
        <source>Ok</source>
        <translation type="unfinished">Ok</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="193"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation type="unfinished">PSK musi zawierać przynajmniej 8 znaków.</translation>
    </message>
</context>
<context>
    <name>PageBasicWizard</name>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="34"/>
        <source>Wizard</source>
        <translation type="unfinished">Asystent</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="55"/>
        <source>Are you sure?</source>
        <translation type="unfinished">Na pewno?</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="56"/>
        <source>Do you really want to cancel the wizard?</source>
        <translation type="unfinished">Czy na pewno chcesz anulować działanie Asystenta?</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="58"/>
        <source>The machine will not work without a completed wizard procedure.</source>
        <translation type="unfinished">Urządzenie nie będzie działać bez dokończenia pracy Asystenta.</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="106"/>
        <source>Can you see the company logo on the exposure display through the cover?</source>
        <translation type="unfinished">Czy patrząc przez pokrywę widzisz logo firmy na wyświetlaczu?</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="108"/>
        <source>Tip: If you can&apos;t see the logo clearly, try placing a sheet of paper onto the screen.</source>
        <translation type="unfinished">Porada: Jeśli nie widzisz logo wyraźnie, spróbuj położyć kartkę papieru na ekranie.</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="110"/>
        <source>Once you place the paper inside the printer, do not forget to CLOSE THE COVER!</source>
        <translation type="unfinished">Po włożeniu kartki do środka, nie zapomnij zamknąć pokrywy!</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="120"/>
        <source>Carefully peel off the protective sticker from the exposition display.</source>
        <translation type="unfinished">Ostrożnie zdejmij naklejkę ochronną z ekranu naświetlającego.</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="129"/>
        <source>Wizard finished sucessfuly!</source>
        <translation type="unfinished">Asystent ukończył pracę z powodzeniem!</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="137"/>
        <source>Wizard failed</source>
        <translation type="unfinished">Błąd Asystenta</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="145"/>
        <source>Wizard canceled</source>
        <translation type="unfinished">Anulowano pracę Asystenta</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="153"/>
        <source>Wizard stopped due to a problem, retry?</source>
        <translation type="unfinished">Asystent został zatrzymany z powodu problemu. Spróbować ponownie?</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="162"/>
        <source>Cover</source>
        <translation type="unfinished">Pokrywa</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="163"/>
        <source>Close the cover.</source>
        <translation type="unfinished">Zamknij pokrywę.</translation>
    </message>
</context>
<context>
    <name>PageCalibrationTilt</name>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="28"/>
        <source>Tank Movement</source>
        <translation type="unfinished">Ruch zbiornika</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="82"/>
        <source>Move Up</source>
        <translation type="unfinished">Ruch w górę</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="91"/>
        <source>Move Down</source>
        <translation type="unfinished">Ruch w dół</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="105"/>
        <source>Tilt position:</source>
        <translation type="unfinished">Pozycja tiltu:</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="116"/>
        <source>Done</source>
        <translation type="unfinished">Gotowe</translation>
    </message>
</context>
<context>
    <name>PageCalibrationWizard</name>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="31"/>
        <source>Printer Calibration</source>
        <translation type="unfinished">Kalibracja drukarki</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="49"/>
        <source>If the platform is not yet inserted, insert it according to the picture at 0° degrees angle and secure it with the black knob.</source>
        <translation type="unfinished">Jeśli platforma nie jest jeszcze zamontowana, to umieść ją na miejscu zgodnie z ilustracją (pod kątem 0°) i przymocuj używając czarnego pokrętła.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="60"/>
        <source>Loosen the small screw on the cantilever with an allen key. Be careful not to unscrew it completely.</source>
        <translation type="unfinished">Poluzuj małą śrubę na wsporniku za pomocą klucza imbusowego. Uważaj, aby nie wykręcić jej całkowicie.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="62"/>
        <source>Some SL1 printers may have two screws - see the handbook for more information.</source>
        <translation type="unfinished">Niektóre wersje SL1 mogą mieć dwie śruby - zajrzyj do Podręcznika po więcej informacji.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="75"/>
        <source>Unscrew the tank, rotate it by 90° and place it flat across the tilt bed. Remove the tank screws completely.</source>
        <translation type="unfinished">Odkręć zbiornik, przekręć go o 90 stopni i połóż płasko nad przechylaną platformą. Wykręć śruby zbiornika całkowicie.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="84"/>
        <source>In the next step, move the tilt bed up/down until it is in direct contact with the resin tank. The tilt bed and tank have to be aligned in a perfect line.</source>
        <translation type="unfinished">W następnym etapie poruszaj mechanizmem przechylania w górę i w dół, aby jego ramka była w bezpośrednim kontakcie ze zbiornikiem. Ramka mechanizmu przechylania i zbiornik powinny być ze sobą idealnie wyrównane.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="96"/>
        <source>Set the tilt bed against the resin tank</source>
        <translation type="unfinished">Ustaw ramkę tiltu zgodnie ze zbiornikiem</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="105"/>
        <source>Make sure that the platfom, tank and tilt bed are PERFECTLY clean.</source>
        <translation type="unfinished">Upewnij się, że platforma, zbiornik i mechanizm przechylania są IDEALNIE czyste.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="107"/>
        <source>The image is for illustration purposes only.</source>
        <translation type="unfinished">Ilustracja służy tylko jako przykład.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="116"/>
        <source>Return the tank to the original position and secure it with tank screws. Make sure that you tighten both screws evenly and with the same amount of force.</source>
        <translation type="unfinished">Ustaw zbiornik z powrotem na ramce i przykręć go za pomocą śrub. Upewnij się, że obydwie śruby są dokręcane równomiernie i z tą samą siłą.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="125"/>
        <source>Check whether the platform is properly secured with the black knob(hold it in place and tighten the knob if needed).</source>
        <translation type="unfinished">Sprawdź czy platforma jest prawidłowo przykręcona czarnym pokrętłem (przytrzymaj ją i dokręć pokrętło, jeśli to konieczne).</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="127"/>
        <source>Do not rotate the platform. It should be positioned according to the picture.</source>
        <translation type="unfinished">Nie obracaj platformy. Powinna być umieszczona zgodnie z ilustracją.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="141"/>
        <source>Close the cover.</source>
        <translation type="unfinished">Zamknij pokrywę.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="155"/>
        <source>Adjust the platform so it is aligned with the exposition display.</source>
        <translation type="unfinished">Wyrównaj platformę z wyświetlaczem.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="157"/>
        <source>Front edges of the platform and exposition display need to be parallel.</source>
        <translation type="unfinished">Krawędzie platformy powinny być równoległe do krawędzi wyświetlacza.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="170"/>
        <source>Hold the platform still with one hand and apply a slight downward force on it, so it maintains good contact with the screen.


Next, use an Allen key to tighten the screw on the cantilever.

Then release the platform.</source>
        <translation type="unfinished">Przytrzymaj platformę w miejscu i naciśnij lekko w dół, aby była dobrze dociśnięta do ekranu.


Następnie przykręć śrubę na wsporniku kluczem imbusowym.

Po dokręceniu śruby puść platformę.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="173"/>
        <source>Tighten the small screw on the cantilever with an allen key.</source>
        <translation type="unfinished">Dokręć małą śrubę na wsporniku przy pomocy klucza imbusowego.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="175"/>
        <source>Some SL1 printers may have two screws - tighten them evenly, little by little. See the handbook for more information.</source>
        <translation type="unfinished">Niektóre wersje SL1 mogą mieć dwie śrubki - dokręć je równomiernie w niewielkim zakresie, przechodząc od jednej do drugiej i z powrotem. W razie potrzeby zajrzyj do Podręcznika po więcej informacji.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="190"/>
        <source>Tilt settings for Prusa Slicer:</source>
        <translation type="unfinished">Ustawienia tiltu dla PrusaSlicer:</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="192"/>
        <source>Tilt time fast: %1 s</source>
        <translation type="unfinished">Czas szybkiego tiltu: %1 s</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="194"/>
        <source>Tilt time slow: %1 s</source>
        <translation type="unfinished">Czas wolnego tiltu: %1 s</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="196"/>
        <source>Area fill: %1 %</source>
        <translation type="unfinished">Wypełnienie powierzchni: %1 %</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="198"/>
        <source>All done, happy printing!</source>
        <translation type="unfinished">Wszystko ustawione, udanego drukowania!</translation>
    </message>
</context>
<context>
    <name>PageChange</name>
    <message>
        <location filename="../qml/PageChange.qml" line="36"/>
        <source>Print Settings</source>
        <translation type="unfinished">Ustawienia druku</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="64"/>
        <source>Exposure</source>
        <translation type="unfinished">Naświetlanie</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="81"/>
        <location filename="../qml/PageChange.qml" line="107"/>
        <location filename="../qml/PageChange.qml" line="129"/>
        <source>second</source>
        <translation type="unfinished">s</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="87"/>
        <source>Exposure Time Incr.</source>
        <translation type="unfinished">Przyrost czasu naświetl.</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="112"/>
        <source>First Layer Expo.</source>
        <translation type="unfinished">Naśw. 1. warstwy</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="134"/>
        <source>Print Profile</source>
        <translation type="unfinished">Profil druku</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="145"/>
        <source>Faster</source>
        <comment>Default print profile. Printer behaves as before.</comment>
        <extracomment>Default print profile. Printer behaves as before.</extracomment>
        <translation type="unfinished">Szybciej</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="147"/>
        <source>Slower</source>
        <comment>Safe print profile with slow tilts and pause before each exposure.</comment>
        <extracomment>Safe print profile with slow tilts and pause before each exposure.</extracomment>
        <translation type="unfinished">Wolniej</translation>
    </message>
</context>
<context>
    <name>PageConfirm</name>
    <message>
        <location filename="../qml/PageConfirm.qml" line="120"/>
        <source>Swipe for a picture</source>
        <translation type="unfinished">Przesuń do ilustracji</translation>
    </message>
    <message>
        <location filename="../qml/PageConfirm.qml" line="187"/>
        <source>Continue</source>
        <translation type="unfinished">Kontynuuj</translation>
    </message>
</context>
<context>
    <name>PageConnectHiddenNetwork</name>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="34"/>
        <source>Hidden Network</source>
        <translation type="unfinished">Ukryta sieć</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="62"/>
        <source>SSID</source>
        <translation type="unfinished">SSID</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="78"/>
        <source>Password</source>
        <translation type="unfinished">Hasło</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="121"/>
        <source>Show Password</source>
        <translation type="unfinished">Pokaż hasło</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="128"/>
        <source>Security</source>
        <translation type="unfinished">Zabezpieczenia</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="145"/>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="153"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation type="unfinished">PSK musi zawierać przynajmniej 8 znaków.</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="191"/>
        <source>Working...</source>
        <translation type="unfinished">Pracuję...</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="202"/>
        <source>Connection to %1 failed.</source>
        <translation type="unfinished">Niepowodzenie połączenia z %1 .</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="213"/>
        <source>Connected to %1</source>
        <translation type="unfinished">Połączono z %1</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="241"/>
        <source>Start AP</source>
        <translation type="unfinished">Włącz hotspot</translation>
    </message>
</context>
<context>
    <name>PageContinue</name>
    <message>
        <location filename="../qml/PageContinue.qml" line="27"/>
        <location filename="../qml/PageContinue.qml" line="59"/>
        <source>Continue</source>
        <translation type="unfinished">Kontynuuj</translation>
    </message>
</context>
<context>
    <name>PageCoolingDown</name>
    <message>
        <location filename="../qml/PageCoolingDown.qml" line="27"/>
        <location filename="../qml/PageCoolingDown.qml" line="37"/>
        <source>UV LED OVERHEAT!</source>
        <translation type="unfinished">PRZEGRZANIE UV LED!</translation>
    </message>
    <message>
        <location filename="../qml/PageCoolingDown.qml" line="40"/>
        <source>Cooling down</source>
        <translation type="unfinished">Chłodzenie</translation>
    </message>
    <message>
        <location filename="../qml/PageCoolingDown.qml" line="40"/>
        <source>Temperature is %1 C</source>
        <translation type="unfinished">Temperatura wynosi %1 C</translation>
    </message>
</context>
<context>
    <name>PageDisplayTest</name>
    <message>
        <location filename="../qml/PageDisplayTest.qml" line="31"/>
        <source>Display Test</source>
        <translation type="unfinished">Test wyświetlacza</translation>
    </message>
</context>
<context>
    <name>PageDisplaytestWizard</name>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="31"/>
        <source>Display Test</source>
        <translation type="unfinished">Test wyświetlacza</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="47"/>
        <source>Welcome to the display wizard.</source>
        <translation type="unfinished">Witaj w Asystencie Ekranu.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="49"/>
        <source>This procedure will help you make sure that your exposure display is working correctly.</source>
        <translation type="unfinished">Ta procedura pomoże Ci upewnić się, że ekran naświetlający działa prawidłowo.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="57"/>
        <source>Please unscrew and remove the resin tank.</source>
        <translation type="unfinished">Odkręć i wyciągnij zbiornik na żywicę.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="66"/>
        <source>Loosen the black knob and remove the platform.</source>
        <translation type="unfinished">Poluzuj czarne pokrętło i wyciągnij platformę.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="75"/>
        <source>Close the cover.</source>
        <translation type="unfinished">Zamknij pokrywę.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="88"/>
        <source>All done, happy printing!</source>
        <translation type="unfinished">Wszystko ustawione, udanego drukowania!</translation>
    </message>
</context>
<context>
    <name>PageDowngradeWizard</name>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="31"/>
        <source>Hardware Downgrade</source>
        <translation type="unfinished">Zejście na niższą wersję sprzętu</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="45"/>
        <source>SL1 components detected (downgrade from SL1S).</source>
        <translation type="unfinished">Wykryto komponenty SL1 (wersja niżej od SL1S).</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="46"/>
        <source>To complete the downgrade procedure, printer needs to clear the configuration and reboot.</source>
        <translation type="unfinished">Do zakończenia procedury obniżenia wersji firmware konieczne jest wyczyszczenie konfiguracji i restart drukarki.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="47"/>
        <source>Proceed?</source>
        <translation type="unfinished">Kontynuować?</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="56"/>
        <source>The printer will power off now.</source>
        <translation type="unfinished">Drukarka zostanie teraz wyłączona.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="57"/>
        <source>Reassemble SL1S components and power on the printer. This will restore the original state.</source>
        <translation type="unfinished">Ponownie zmontuj komponenty SL1S i włącz urządzenie. Zostanie przywrócone do oryginalnego stanu.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="66"/>
        <source>Please note that downgrading is not supported. 

Downgrading your printer will erase your UV calibration and your printer will not work properly. 

You will need to recalibrate it using an external UV calibrator.</source>
        <translation type="unfinished">Weź pod uwagę, że zejście na niższą wersję nie jest wspierane.

Zejście na niższą wersję spowoduje wyczyszczenie danych kalibracji UV, a drukarka nie będzie działać poprawnie.

Konieczna będzie ponowna kalibracja przy pomocy zewnętrznego kalibratora UV.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="74"/>
        <source>Current configuration is going to be cleared now.</source>
        <translation type="unfinished">Obecna konfiguracja zostanie teraz wyczyszczona.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="75"/>
        <source>The printer will ask for the inital setup after reboot.</source>
        <translation type="unfinished">Drukarka poprosi o konfigurację początkową po uruchomieniu.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="84"/>
        <source>Use only the metal resin tank supplied. Using the different resin tank may cause resin to spill and damage your printer!</source>
        <translation type="unfinished">Używaj tylko metalowego zbiornika na żywicę dostarczonego razem z drukarką. Używanie innego zbiornika może doprowadzić do rozlania żywicy i uszkodzenia drukarki!</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="93"/>
        <source>Only use the platform supplied. Using a different platform may cause resin to spill and damage your printer!</source>
        <translation type="unfinished">Używaj tylko platformy dostarczonej razem z drukarką. Używanie innej platformy może doprowadzić do rozlania żywicy i uszkodzenia drukarki!</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="101"/>
        <source>Downgrade done. In the next step, the printer will be restarted.</source>
        <translation type="unfinished">Zejście na niższą wersję zakończone. W kolejnym kroku drukarka zostanie zrestartowana.</translation>
    </message>
</context>
<context>
    <name>PageDownloadingExamples</name>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="10"/>
        <source>Examples</source>
        <translation type="unfinished">Pliki przykładowe</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="54"/>
        <source>Downloading examples...</source>
        <translation type="unfinished">Pobieranie plików przykładowych...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="122"/>
        <source>Initializing...</source>
        <translation type="unfinished">Inicjowanie...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="123"/>
        <source>Downloading ...</source>
        <translation type="unfinished">Pobieranie...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="124"/>
        <source>Copying...</source>
        <translation type="unfinished">Kopiowanie...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="125"/>
        <source>Unpacking...</source>
        <translation type="unfinished">Rozpakowanie...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="126"/>
        <source>Done.</source>
        <translation type="unfinished">Gotowe.</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="127"/>
        <source>Cleanup...</source>
        <translation type="unfinished">Czyszczenie...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="128"/>
        <source>Failure.</source>
        <translation type="unfinished">Niepowodzenie.</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="131"/>
        <source>Unknown state.</source>
        <translation type="unfinished">Nieznany stan.</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="139"/>
        <source>View Examples</source>
        <translation type="unfinished">Pokaż pliki przykładowe</translation>
    </message>
</context>
<context>
    <name>PageError</name>
    <message>
        <location filename="../qml/PageError.qml" line="33"/>
        <source>Error</source>
        <translation type="unfinished">Błąd</translation>
    </message>
    <message>
        <location filename="../qml/PageError.qml" line="42"/>
        <source>Error code:</source>
        <translation type="unfinished">Kod błędu:</translation>
    </message>
</context>
<context>
    <name>PageEthernetSettings</name>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="34"/>
        <source>Wired Settings</source>
        <translation type="unfinished">Ustawienia sieci</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="104"/>
        <source>Network Info</source>
        <translation type="unfinished">Informacje o sieci</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="110"/>
        <source>DHCP</source>
        <translation type="unfinished">DHCP</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="120"/>
        <source>IP Address</source>
        <translation type="unfinished">Adres IP</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="133"/>
        <source>Gateway</source>
        <comment>default gateway address</comment>
        <translation type="unfinished">Brama sieciowa</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="186"/>
        <source>Apply</source>
        <translation type="unfinished">Zastosuj</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="196"/>
        <source>Configuring the connection,</source>
        <translation type="unfinished">Konfiguracja połączenia,</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="196"/>
        <source>please wait...</source>
        <translation type="unfinished">Proszę czekać...</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="203"/>
        <source>Revert</source>
        <comment>Turn back the changes and go back to the previous configuration.</comment>
        <translation type="unfinished">Cofnij</translation>
    </message>
</context>
<context>
    <name>PageException</name>
    <message>
        <location filename="../qml/PageException.qml" line="36"/>
        <source>System Error</source>
        <translation type="unfinished">Błąd Systemowy</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="59"/>
        <source>Error</source>
        <translation type="unfinished">Błąd</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="66"/>
        <source>Error code:</source>
        <translation type="unfinished">Kod błędu:</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="75"/>
        <source>Swipe to proceed</source>
        <translation type="unfinished">Przesuń ekran, aby kontynuować</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="100"/>
        <source>Save Logs to USB</source>
        <translation type="unfinished">Zapisz logi na USB</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="113"/>
        <source>Send Logs to Cloud</source>
        <translation type="unfinished">Wyślij logi do chmury</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="127"/>
        <source>Update Firmware</source>
        <translation type="unfinished">Aktualizacja Firmware</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="134"/>
        <source>Turn Off</source>
        <translation type="unfinished">Wyłącz</translation>
    </message>
</context>
<context>
    <name>PageFactoryResetWizard</name>
    <message>
        <location filename="../qml/PageFactoryResetWizard.qml" line="31"/>
        <source>Factory Reset</source>
        <translation type="unfinished">Przywrócenie ustawień fabrycznych</translation>
    </message>
    <message>
        <location filename="../qml/PageFactoryResetWizard.qml" line="43"/>
        <source>Factory Reset done.</source>
        <translation type="unfinished">Zakończono przywracanie ustawień fabrycznych.</translation>
    </message>
</context>
<context>
    <name>PageFeedme</name>
    <message>
        <location filename="../qml/PageFeedme.qml" line="34"/>
        <source>Feed Me</source>
        <translation type="unfinished">Napełnij mnie</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="42"/>
        <source>Manual resin refill.</source>
        <translation type="unfinished">Ręczne uzupełnienie żywicy.</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="43"/>
        <source>Refill the tank up to the 100% mark and press Done.</source>
        <translation type="unfinished">Uzupełnij żywicę do znacznika 100% i wciśnij &apos;Gotowe&apos;.</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="44"/>
        <source>If you do not want to refill, press the Back button at top of the screen.</source>
        <translation type="unfinished">Jeśli nie chcesz dolewać żywicy, to wciśnij przycisk &apos;Wstecz&apos; na górze ekranu.</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="74"/>
        <source>Done</source>
        <translation type="unfinished">Gotowe</translation>
    </message>
</context>
<context>
    <name>PageFileBrowser</name>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="33"/>
        <source>Projects</source>
        <translation type="unfinished">Projekty</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="43"/>
        <source>Selftest</source>
        <translation type="unfinished">Selftest</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="44"/>
        <source>Printer Calibration</source>
        <translation type="unfinished">Kalibracja drukarki</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="45"/>
        <source>UV Calibration</source>
        <translation type="unfinished">Kalibracja UV</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="94"/>
        <source>Local</source>
        <translation type="unfinished">Lokalnie</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="95"/>
        <source>USB</source>
        <translation type="unfinished">USB</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="96"/>
        <source>Remote</source>
        <translation type="unfinished">Zdalnie</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="97"/>
        <source>Previous Prints</source>
        <comment>a directory with previously printed projects</comment>
        <translation type="unfinished">Poprzednie wydruki</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="98"/>
        <source>Update Bundles</source>
        <comment>a directory containing firmware update bundles</comment>
        <translation type="unfinished">Aktualizacja paczek</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="148"/>
        <source>Install?</source>
        <translation type="unfinished">Zainstalować?</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="150"/>
        <source>Do you really want to install %1?</source>
        <translation type="unfinished">Czy chcesz zainstalować %1?</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="152"/>
        <source>Current system will still be available via Settings -&gt; Firmware -&gt; Downgrade</source>
        <translation type="unfinished">Obecny system będzie dostępny z menu Ustawienia -&gt; Firmware -&gt; Zejście na niższą wersję</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="164"/>
        <source>Calibrate?</source>
        <translation type="unfinished">Kalibrować?</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="167"/>
        <source>The printer is not fully calibrated.</source>
        <translation type="unfinished">Drukarka nie została kompletnie skalibrowana.</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="169"/>
        <source>Before printing, the following steps are required to pass:</source>
        <translation type="unfinished">Przed drukowaniem konieczne jest wykonanie następujących czynności:</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="173"/>
        <source>Do you want to start now?</source>
        <translation type="unfinished">Czy chcesz rozpocząć?</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/PageFileBrowser.qml" line="229"/>
        <source>%n item(s)</source>
        <comment>number of items in a directory</comment>
        <translation type="unfinished">
            <numerusform>%n pozycji</numerusform>
            <numerusform>%n pozycji</numerusform>
            <numerusform>%n pozycja</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="241"/>
        <source>Remote</source>
        <comment>File is stored in remote storage, i.e. a cloud</comment>
        <translation type="unfinished">Zdalnie</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="242"/>
        <location filename="../qml/PageFileBrowser.qml" line="244"/>
        <source>Local</source>
        <comment>File is stored in a local storage</comment>
        <translation type="unfinished">Lokalnie</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="243"/>
        <source>USB</source>
        <comment>File is stored on USB flash disk</comment>
        <translation type="unfinished">USB</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="245"/>
        <source>Updates</source>
        <comment>File is in a repository of raucb files(update bundles)</comment>
        <translation type="unfinished">Aktualizacje</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="246"/>
        <source>Root</source>
        <comment>Directory is a root of the directory tree, its subdirectories are different sources of projects</comment>
        <translation type="unfinished">Root</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="267"/>
        <source>Unknown</source>
        <translation type="unfinished">Nieznany</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="316"/>
        <source>No usable projects were found,</source>
        <translation type="unfinished">Nie znaleziono zgodnych projektów,</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="318"/>
        <source>insert a USB drive or download examples in Settings -&gt; Support.</source>
        <translation type="unfinished">podłącz pamięć USB lub pobierz pliki przykładowe z menu Ustawienia -&gt; Wsparcie.</translation>
    </message>
</context>
<context>
    <name>PageFinished</name>
    <message>
        <location filename="../qml/PageFinished.qml" line="33"/>
        <source>Finished</source>
        <translation type="unfinished">Zakończono</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="109"/>
        <location filename="../qml/PageFinished.qml" line="112"/>
        <source>FINISHED</source>
        <translation type="unfinished">ZAKOŃCZONE</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="110"/>
        <source>FAILED</source>
        <translation type="unfinished">NIEPOWODZENIE</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="111"/>
        <source>CANCELED</source>
        <translation type="unfinished">ANULOWANO</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="136"/>
        <source>Print Time</source>
        <translation type="unfinished">Czas druku</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="157"/>
        <source>Layers</source>
        <translation type="unfinished">Warstwy</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="175"/>
        <source>Consumed Resin</source>
        <translation type="unfinished">Użyta żywica</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="193"/>
        <source>Layer height</source>
        <translation type="unfinished">Wysokość warstwy</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="211"/>
        <source>Layer Exposure</source>
        <translation type="unfinished">Naświetlanie warstwy</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="216"/>
        <location filename="../qml/PageFinished.qml" line="234"/>
        <source>s</source>
        <translation type="unfinished">s</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="229"/>
        <source>First Layer Exposure</source>
        <translation type="unfinished">Naświetlanie pierwszej warstwy</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="249"/>
        <source>Swipe to proceed</source>
        <translation type="unfinished">Przesuń ekran, aby kontynuować</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="275"/>
        <source>Home</source>
        <translation type="unfinished">Ekran główny</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="286"/>
        <source>Reprint</source>
        <translation type="unfinished">Drukuj ponownie</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="304"/>
        <source>Turn Off</source>
        <translation type="unfinished">Wyłącz</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="339"/>
        <source>Loading, please wait...</source>
        <translation type="unfinished">Ładowanie, proszę czekać...</translation>
    </message>
</context>
<context>
    <name>PageFullscreenImage</name>
    <message>
        <location filename="../qml/PageFullscreenImage.qml" line="27"/>
        <source>Fullscreen Image</source>
        <translation type="unfinished">Ilustracja na pełnym ekranie</translation>
    </message>
</context>
<context>
    <name>PageHome</name>
    <message>
        <location filename="../qml/PageHome.qml" line="29"/>
        <source>Home</source>
        <translation type="unfinished">Ekran główny</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="37"/>
        <source>Print</source>
        <translation type="unfinished">Druk</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="50"/>
        <source>Control</source>
        <translation type="unfinished">Sterowanie</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="57"/>
        <source>Settings</source>
        <translation type="unfinished">Ustawienia</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="64"/>
        <source>Turn Off</source>
        <translation type="unfinished">Wyłącz</translation>
    </message>
</context>
<context>
    <name>PageLanguage</name>
    <message>
        <location filename="../qml/PageLanguage.qml" line="31"/>
        <source>Set Language</source>
        <translation type="unfinished">Język</translation>
    </message>
    <message>
        <location filename="../qml/PageLanguage.qml" line="79"/>
        <source>Set</source>
        <translation type="unfinished">Ustaw</translation>
    </message>
</context>
<context>
    <name>PageLogs</name>
    <message>
        <location filename="../qml/PageLogs.qml" line="29"/>
        <source>Logs export</source>
        <translation type="unfinished">Eksportowanie logów</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="50"/>
        <source>Log upload finished</source>
        <translation type="unfinished">Zakończono przesyłanie logu</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="52"/>
        <source>Logs has been successfully uploaded to the Prusa server.&lt;br /&gt;&lt;br /&gt;Please contact the Prusa support and share the following code with them:</source>
        <translation type="unfinished">Logi zostały pomyślne przesłane na serwer Prusa.&lt;br /&gt;&lt;br /&gt;Skontaktuj się ze Wsparciem Klienta Prusa i podaj ten kod:</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="57"/>
        <source>Logs export finished</source>
        <translation type="unfinished">Zakończono eksportowanie logów</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="61"/>
        <source>Logs export canceled</source>
        <translation type="unfinished">Anulowano eksportowanie logów</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="64"/>
        <source>Logs export failed.

Please check your flash drive / internet connection.</source>
        <translation type="unfinished">Niepowodzenie eksportowania logu. Sprawdź połączenie z pamięcią USB / Internetem.</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="88"/>
        <source>Extracting log data</source>
        <translation type="unfinished">Wypakowywanie danych logu</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="91"/>
        <source>Saving data to USB drive</source>
        <translation type="unfinished">Zapis danych w pamięci USB</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="92"/>
        <source>Uploading data to server</source>
        <translation type="unfinished">Przesyłanie danych na serwer</translation>
    </message>
</context>
<context>
    <name>PageManual</name>
    <message>
        <location filename="../qml/PageManual.qml" line="25"/>
        <source>Manual</source>
        <translation type="unfinished">Instrukcje</translation>
    </message>
    <message>
        <location filename="../qml/PageManual.qml" line="43"/>
        <source>Scanning the QR code will load the handbook for this device.

Alternatively, use this link:</source>
        <translation type="unfinished">Zeskanowanie kodu QR przekieruje Cię do &quot;Podręcznika&quot; dla tego urządzenia.

Możesz również użyć tego linku:</translation>
    </message>
</context>
<context>
    <name>PageMovementControl</name>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="27"/>
        <source>Control</source>
        <translation type="unfinished">Sterowanie</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="38"/>
        <source>Home
Platform</source>
        <translation type="unfinished">Bazowanie
Platformy</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="49"/>
        <source>Home
Tank</source>
        <translation type="unfinished">Bazowanie
Zbiornika</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="60"/>
        <source>Disable
Steppers</source>
        <translation type="unfinished">Wyłącz
silniki</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="69"/>
        <source>Homing the tank, please wait...</source>
        <translation type="unfinished">Bazowanie zbiornika, proszę czekać...</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="69"/>
        <source>Homing the tower, please wait...</source>
        <translation type="unfinished">Bazowanie kolumny, proszę czekać...</translation>
    </message>
</context>
<context>
    <name>PageNetworkEthernetList</name>
    <message>
        <location filename="../qml/PageNetworkEthernetList.qml" line="35"/>
        <source>Network</source>
        <translation type="unfinished">Sieć</translation>
    </message>
</context>
<context>
    <name>PageNetworkMain</name>
    <message>
        <location filename="../qml/PageNetworkMain.qml" line="28"/>
        <source>Network</source>
        <translation type="unfinished">Sieć</translation>
    </message>
    <message>
        <location filename="../qml/PageNetworkMain.qml" line="46"/>
        <source>Wi-Fi</source>
        <translation type="unfinished">Wi-Fi</translation>
    </message>
    <message>
        <location filename="../qml/PageNetworkMain.qml" line="55"/>
        <source>Ethernet</source>
        <translation type="unfinished">Ethernet</translation>
    </message>
    <message>
        <location filename="../qml/PageNetworkMain.qml" line="64"/>
        <source>Hotspot</source>
        <translation type="unfinished">Hotspot</translation>
    </message>
    <message>
        <location filename="../qml/PageNetworkMain.qml" line="85"/>
        <source>IP Address</source>
        <translation type="unfinished">Adres IP</translation>
    </message>
    <message>
        <location filename="../qml/PageNetworkMain.qml" line="97"/>
        <source>Not connected to network</source>
        <translation type="unfinished">Brak połączenia z siecią</translation>
    </message>
</context>
<context>
    <name>PageNetworkWifiList</name>
    <message>
        <location filename="../qml/PageNetworkWifiList.qml" line="29"/>
        <source>Network</source>
        <translation type="unfinished">Sieć</translation>
    </message>
</context>
<context>
    <name>PageNewExpoPanelWizard</name>
    <message>
        <location filename="../qml/PageNewExpoPanelWizard.qml" line="31"/>
        <source>New Exposure Panel</source>
        <translation type="unfinished">Nowy panel naświetlający</translation>
    </message>
    <message>
        <location filename="../qml/PageNewExpoPanelWizard.qml" line="43"/>
        <source>New exposure screen has been detected.

The printer will ask for the inital setup (selftest and calibration) to make sure everything works correctly.</source>
        <translation type="unfinished">Wykryto nowy ekran naświetlający.

Drukarka zapyta o początkową konfigurację (Selftest i kalibracja), aby mieć pewność, że wszystko działa poprawnie.</translation>
    </message>
</context>
<context>
    <name>PageNotificationList</name>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="37"/>
        <source>Notifications</source>
        <translation type="unfinished">Powiadomienia</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="61"/>
        <source>Available update to %1</source>
        <translation type="unfinished">Dostępna aktualizacja do wersji %1</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="79"/>
        <source>Print failed: %1</source>
        <translation type="unfinished">Druk nieudany: %1</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="80"/>
        <source>Print canceled: %1</source>
        <translation type="unfinished">Anulowano druk: %1</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="81"/>
        <source>Print finished: %1</source>
        <translation type="unfinished">Druk zakończony: %1</translation>
    </message>
</context>
<context>
    <name>PagePackingWizard</name>
    <message>
        <location filename="../qml/PagePackingWizard.qml" line="31"/>
        <source>Packing Wizard</source>
        <translation type="unfinished">Asystent Pakowania</translation>
    </message>
    <message>
        <location filename="../qml/PagePackingWizard.qml" line="44"/>
        <source>Packing done.</source>
        <translation type="unfinished">Pakowanie zakończone.</translation>
    </message>
    <message>
        <location filename="../qml/PagePackingWizard.qml" line="52"/>
        <source>Insert protective foam</source>
        <translation type="unfinished">Włóż piankę zabezpieczającą</translation>
    </message>
</context>
<context>
    <name>PagePowerOffDialog</name>
    <message>
        <location filename="../qml/PagePowerOffDialog.qml" line="5"/>
        <source>Power Off?</source>
        <translation type="unfinished">Wyłączyć?</translation>
    </message>
    <message>
        <location filename="../qml/PagePowerOffDialog.qml" line="6"/>
        <source>Do you really want to turn off the printer?</source>
        <translation type="unfinished">Czy na pewno chcesz wyłączyć drukarkę?</translation>
    </message>
    <message>
        <location filename="../qml/PagePowerOffDialog.qml" line="9"/>
        <source>Powering Off...</source>
        <translation type="unfinished">Wyłączanie...</translation>
    </message>
</context>
<context>
    <name>PagePrePrintChecks</name>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="32"/>
        <source>Please Wait</source>
        <translation type="unfinished">Proszę czekać</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="111"/>
        <source>Temperature</source>
        <translation type="unfinished">Temperatura</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="112"/>
        <source>Project</source>
        <translation type="unfinished">Projekt</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="114"/>
        <source>Fan</source>
        <translation type="unfinished">Wentylator</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="115"/>
        <source>Cover</source>
        <translation type="unfinished">Pokrywa</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="116"/>
        <source>Resin</source>
        <translation type="unfinished">Żywica</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="117"/>
        <source>Starting Positions</source>
        <translation type="unfinished">Pozycje początkowe</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="118"/>
        <source>Stirring</source>
        <translation type="unfinished">Mieszanie</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="133"/>
        <source>Do not touch the printer!</source>
        <translation type="unfinished">Nie dotykaj drukarki!</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="143"/>
        <source>With Warning</source>
        <translation type="unfinished">Z ostrzeżeniem</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="144"/>
        <source>Disabled</source>
        <translation type="unfinished">Wyłączone</translation>
    </message>
</context>
<context>
    <name>PagePrint</name>
    <message>
        <location filename="../qml/PagePrint.qml" line="146"/>
        <source>Continue?</source>
        <translation type="unfinished">Kontynuować?</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="149"/>
        <source>Check Warning</source>
        <translation type="unfinished">Sprawdź ostrzeżenie</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="159"/>
        <location filename="../qml/PagePrint.qml" line="275"/>
        <source>Stuck Recovery</source>
        <translation type="unfinished">Wznawianie pracy</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="160"/>
        <source>The printer got stuck and needs user assistance.</source>
        <translation type="unfinished">Drukarka utknęła i wymaga uwagi użytkownika.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="161"/>
        <source>Release the tank mechanism and press Continue.</source>
        <translation type="unfinished">Poluzuj mechanizm zbiornika i wciśnij &apos;Kontynuuj&apos;.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="162"/>
        <source>If you do not want to continue, press the Back button on top of the screen and the current job will be canceled.</source>
        <translation type="unfinished">Jeśli nie chcesz kontynuować, to naciśnij przycisk &apos;Wstecz&apos; na górze ekranu, aby anulować bieżące zadanie.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="181"/>
        <source>Close Cover!</source>
        <translation type="unfinished">Zamknij pokrywę!</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="182"/>
        <source>Please, close the cover! UV radiation is harmful.</source>
        <translation type="unfinished">Zamknij pokrywę! Promieniowanie UV jest szkodliwe.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="238"/>
        <source>Going up</source>
        <translation type="unfinished">Podnoszenie</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="238"/>
        <source>Moving platform to the top position</source>
        <translation type="unfinished">Podnoszenie platformy do szczytu</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="241"/>
        <source>Going down</source>
        <translation type="unfinished">Opuszczanie</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="241"/>
        <source>Moving platform to the bottom position</source>
        <translation type="unfinished">Opuszczanie platformy</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="244"/>
        <source>Project</source>
        <translation type="unfinished">Projekt</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="244"/>
        <source>Getting the printer ready to add resin. Please wait.</source>
        <translation type="unfinished">Przygotowywanie drukarki do uzupełnienia żywicy. Proszę czekać...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="247"/>
        <location filename="../qml/PagePrint.qml" line="301"/>
        <source>Please wait...</source>
        <translation type="unfinished">Proszę czekać...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="250"/>
        <source>Cover Open</source>
        <translation type="unfinished">Pokrywa otwarta</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="250"/>
        <source>Paused.</source>
        <translation type="unfinished">Pauza.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="250"/>
        <source>Please close the cover to continue</source>
        <translation type="unfinished">Zamknij pokrywę, aby kontynuować</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="262"/>
        <source>Stirring</source>
        <translation type="unfinished">Mieszanie</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="262"/>
        <source>Stirring resin</source>
        <translation type="unfinished">Mieszanie żywicy</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="265"/>
        <source>Action Pending</source>
        <translation type="unfinished">Czynność w trakcie</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="265"/>
        <source>Requested actions will be executed after layer finish, please wait...</source>
        <translation type="unfinished">Żądana akcja zostanie wykonana po zakończeniu warstwy. Proszę czekać...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="268"/>
        <source>Reading data...</source>
        <translation type="unfinished">Odczyt danych...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="275"/>
        <source>Setting start positions...</source>
        <translation type="unfinished">Ustawienie pozycji startowych...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="278"/>
        <source>Tank Moving Down</source>
        <translation type="unfinished">Opuszczanie zbiornika</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="278"/>
        <source>Moving the resin tank down...</source>
        <translation type="unfinished">Opuszczanie zbiornika...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="314"/>
        <source>Loading, please wait...</source>
        <translation type="unfinished">Ładowanie, proszę czekać...</translation>
    </message>
</context>
<context>
    <name>PagePrintPreviewSwipe</name>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="33"/>
        <source>Project</source>
        <translation type="unfinished">Projekt</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="165"/>
        <source>Swipe to project</source>
        <translation type="unfinished">Przesuń do projektu</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="206"/>
        <source>Unknown</source>
        <translation type="unfinished">Nieznany</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="279"/>
        <source>Layers</source>
        <translation type="unfinished">Warstwy</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="279"/>
        <source>Layer Height</source>
        <translation type="unfinished">Wysokość warstwy</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="298"/>
        <source>Exposure Times</source>
        <translation type="unfinished">Czasy naświetlania</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="323"/>
        <source>Print Time Estimate</source>
        <translation type="unfinished">Szacowany czas druku</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="340"/>
        <source>Last Modified</source>
        <translation type="unfinished">Ostatnia modyfikacja</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="347"/>
        <source>Unknown</source>
        <comment>Unknow time of last modification of a file</comment>
        <translation type="unfinished">Nieznany</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="364"/>
        <source>Swipe to continue</source>
        <translation type="unfinished">Przesuń, aby kontynuować</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="433"/>
        <source>The project requires %1 % of the resin. It will be necessary to refill the resin during print.</source>
        <translation type="unfinished">Projekt wymaga&lt;br/&gt;%1 % żywicy. Konieczne będzie uzupełnienie żywicy podczas drukowania.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="436"/>
        <source>Please fill the resin tank to at least %1 % and close the cover.</source>
        <translation type="unfinished">Uzupełnij żywicę do poziomu co najmniej&lt;br/&gt;%1 % i zamknij pokrywę.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="471"/>
        <source>Print Settings</source>
        <translation type="unfinished">Ustawienia druku</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="481"/>
        <source>Print</source>
        <translation type="unfinished">Druk</translation>
    </message>
</context>
<context>
    <name>PagePrintPrinting</name>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="33"/>
        <source>Print</source>
        <translation type="unfinished">Druk</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="104"/>
        <source>Today at</source>
        <translation type="unfinished">Dzisiaj o</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="107"/>
        <source>Tomorrow at</source>
        <translation type="unfinished">Jutro o</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="150"/>
        <source>Layer:</source>
        <translation type="unfinished">Warstwa:</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="162"/>
        <source>Layer Height:</source>
        <translation type="unfinished">Wysokość warstwy:</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="162"/>
        <source>N/A</source>
        <translation type="unfinished">N/D</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="251"/>
        <source>Remaining Time</source>
        <translation type="unfinished">Pozostały czas</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="256"/>
        <source>Unknown</source>
        <comment>Remaining time is unknown</comment>
        <translation type="unfinished">Nieznany</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="266"/>
        <source>Estimated End</source>
        <translation type="unfinished">Szacowane zakończenie</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="281"/>
        <source>Printing Time</source>
        <translation type="unfinished">Czas drukowania</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="294"/>
        <source>Layer</source>
        <translation type="unfinished">Warstwa</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="306"/>
        <source>Remaining Resin</source>
        <translation type="unfinished">Pozostała żywica</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="310"/>
        <location filename="../qml/PagePrintPrinting.qml" line="324"/>
        <source>ml</source>
        <translation type="unfinished">ml</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="310"/>
        <location filename="../qml/PagePrintPrinting.qml" line="324"/>
        <source>Unknown</source>
        <translation type="unfinished">Nieznany</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="320"/>
        <source>Consumed Resin</source>
        <translation type="unfinished">Użyta żywica</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="359"/>
        <source>Print Settings</source>
        <translation type="unfinished">Ustawienia druku</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="365"/>
        <source>Refill Resin</source>
        <translation type="unfinished">Ręczne uzupełnienie żywicy</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="378"/>
        <location filename="../qml/PagePrintPrinting.qml" line="384"/>
        <source>Cancel Print</source>
        <translation type="unfinished">Anuluj</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="385"/>
        <source>To make sure the print is not stopped accidentally,
please swipe the screen to move to the next step,
where you can cancel the print.</source>
        <translation type="unfinished">Przesuń ekran, aby anulować wydruk. Jest to zabezpieczenie przed przypadkowym anulowaniem.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="402"/>
        <source>Enter Admin</source>
        <translation type="unfinished">Otwórz menu Admina</translation>
    </message>
</context>
<context>
    <name>PagePrintResinIn</name>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="30"/>
        <source>Pour in resin</source>
        <translation type="unfinished">Wlej żywicę</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="61"/>
        <source>The project requires %1 % of the resin. It will be necessary to refill the resin during print.</source>
        <translation type="unfinished">Projekt wymaga&lt;br/&gt;%1 % żywicy. Konieczne będzie uzupełnienie żywicy podczas drukowania.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="64"/>
        <source>Please fill the resin tank to at least %1 % and close the cover.</source>
        <translation type="unfinished">Uzupełnij żywicę do poziomu co najmniej&lt;br/&gt;%1 % i zamknij pokrywę.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="74"/>
        <source>Continue</source>
        <translation type="unfinished">Kontynuuj</translation>
    </message>
</context>
<context>
    <name>PagePrusaConnect</name>
    <message>
        <location filename="../qml/PagePrusaConnect.qml" line="27"/>
        <source>Prusa Connect</source>
        <translation type="unfinished">Prusa Connect</translation>
    </message>
    <message>
        <location filename="../qml/PagePrusaConnect.qml" line="50"/>
        <location filename="../qml/PagePrusaConnect.qml" line="92"/>
        <location filename="../qml/PagePrusaConnect.qml" line="113"/>
        <source>Failed to establish a new connection.
Please try later.</source>
        <translation type="unfinished">Niepowodzenie ustanowienia nowego połączenia.
Spróbuj ponownie później.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrusaConnect.qml" line="70"/>
        <source>It&apos;s already registered!</source>
        <translation type="unfinished">Już zarejestrowano!</translation>
    </message>
    <message>
        <location filename="../qml/PagePrusaConnect.qml" line="70"/>
        <source>Start Your Registration!</source>
        <translation type="unfinished">Rozpocznij rejestrację!</translation>
    </message>
    <message>
        <location filename="../qml/PagePrusaConnect.qml" line="78"/>
        <source>Restore</source>
        <translation type="unfinished">Przywróć</translation>
    </message>
    <message>
        <location filename="../qml/PagePrusaConnect.qml" line="78"/>
        <source>Continue</source>
        <translation type="unfinished">Kontynuuj</translation>
    </message>
</context>
<context>
    <name>PageQrCode</name>
    <message>
        <location filename="../qml/PageQrCode.qml" line="30"/>
        <source>Page QR code</source>
        <translation type="unfinished">Kod QR strony</translation>
    </message>
</context>
<context>
    <name>PageReleaseNotes</name>
    <message>
        <location filename="../qml/PageReleaseNotes.qml" line="9"/>
        <source>Release Notes</source>
        <translation type="unfinished">Lista zmian</translation>
    </message>
    <message>
        <location filename="../qml/PageReleaseNotes.qml" line="111"/>
        <source>Later</source>
        <translation type="unfinished">Później</translation>
    </message>
    <message>
        <location filename="../qml/PageReleaseNotes.qml" line="120"/>
        <source>Install Now</source>
        <translation type="unfinished">Zainstaluj</translation>
    </message>
</context>
<context>
    <name>PageSelftestWizard</name>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="32"/>
        <source>Selftest</source>
        <translation type="unfinished">Selftest</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="48"/>
        <source>Welcome to the selftest wizard.</source>
        <translation type="unfinished">Witaj w Asystencie Selftestu.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="50"/>
        <source>This procedure is mandatory and it will check all components of the printer.</source>
        <translation type="unfinished">Ta procedura jest konieczna i sprawdzi wszystkie komponenty drukarki.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="58"/>
        <source>Please unscrew and remove the resin tank.</source>
        <translation type="unfinished">Odkręć i wyciągnij zbiornik na żywicę.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="67"/>
        <source>Loosen the black knob and remove the platform.</source>
        <translation type="unfinished">Poluzuj czarne pokrętło i wyciągnij platformę.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="76"/>
        <location filename="../qml/PageSelftestWizard.qml" line="137"/>
        <location filename="../qml/PageSelftestWizard.qml" line="169"/>
        <source>Close the cover.</source>
        <translation type="unfinished">Zamknij pokrywę.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="100"/>
        <source>Can you hear the music?</source>
        <translation type="unfinished">Czy słyszysz muzykę?</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="110"/>
        <source>Please install the resin tank and tighten the screws evenly.</source>
        <translation type="unfinished">Zamontuj zbiornik na żywicę i równomiernie dokręć śruby.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="119"/>
        <source>Please install the platform under 60° angle and tighten it with the black knob.</source>
        <translation type="unfinished">Umieść platformę pod kątem 60° 
i przymocuj używając czarnego pokrętła.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="128"/>
        <source>Release the platform from the cantilever and place it onto the center of the resin tank at a 90° angle.</source>
        <translation type="unfinished">Poluzuj śrubę na wsporniku i umieść platformę na środku zbiornika, pod kątem 90°.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="151"/>
        <source>Make sure that the resin tank is installed and the screws are tight.</source>
        <translation type="unfinished">Upewnij się, że zbiornik jest na miejscu, a śruby są dobrze dokręcone.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="160"/>
        <source>Please install the platform under 0° angle and tighten it with the black knob.</source>
        <translation type="unfinished">Umieść platformę pod kątem 0° i przymocuj używając czarnego pokrętła.</translation>
    </message>
</context>
<context>
    <name>PageSetDate</name>
    <message>
        <location filename="../qml/PageSetDate.qml" line="37"/>
        <source>Set Date</source>
        <translation type="unfinished">Ustaw Datę</translation>
    </message>
    <message>
        <location filename="../qml/PageSetDate.qml" line="665"/>
        <source>Set</source>
        <translation type="unfinished">Ustaw</translation>
    </message>
</context>
<context>
    <name>PageSetHostname</name>
    <message>
        <location filename="../qml/PageSetHostname.qml" line="30"/>
        <source>Set Hostname</source>
        <translation type="unfinished">Zapisz</translation>
    </message>
    <message>
        <location filename="../qml/PageSetHostname.qml" line="47"/>
        <source>Hostname</source>
        <translation type="unfinished">Nazwa Hosta</translation>
    </message>
    <message>
        <location filename="../qml/PageSetHostname.qml" line="73"/>
        <source>Can contain only characters a-z, A-Z, 0-9 and  &quot;-&quot;.</source>
        <translation type="unfinished">Może zawierać jedynie znaki a-z, A-Z, 0-9 oraz &quot;-&quot;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSetHostname.qml" line="84"/>
        <source>Set</source>
        <translation type="unfinished">Ustaw</translation>
    </message>
</context>
<context>
    <name>PageSetLoginCredentials</name>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="32"/>
        <source>Login Credentials</source>
        <translation type="unfinished">Dane do logowania</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="42"/>
        <source>Are you sure?</source>
        <translation type="unfinished">Na pewno?</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="43"/>
        <source>Disable the HTTP Digest?&lt;br/&gt;&lt;br/&gt;CAUTION: This may be insecure!</source>
        <translation type="unfinished">Wyłączyć HTTP Digest? &lt;br/&gt;&lt;br/&gt;UWAGA: Może to spowodować zagrożenie bezpieczeństwa!</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="66"/>
        <source>HTTP Digest</source>
        <translation type="unfinished">HTTP Digest</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="109"/>
        <source>User Name</source>
        <translation type="unfinished">Użytkownik</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="146"/>
        <source>Printer Password</source>
        <translation type="unfinished">Hasło</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="171"/>
        <location filename="../qml/PageSetLoginCredentials.qml" line="179"/>
        <location filename="../qml/PageSetLoginCredentials.qml" line="229"/>
        <source>Must be at least 8 chars long</source>
        <translation type="unfinished">Musi mieć co najmniej 8 znaków</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="185"/>
        <source>Can contain only characters a-z, A-Z, 0-9 and  &quot;-&quot;.</source>
        <translation type="unfinished">Może zawierać jedynie znaki a-z, A-Z, 0-9 oraz &quot;-&quot;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="205"/>
        <source>Printer API Key</source>
        <translation type="unfinished">Klucz API drukarki</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="236"/>
        <source>Can contain only ASCII characters.</source>
        <translation type="unfinished">Może zawierać tylko znaki ASCII.</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="249"/>
        <source>Save</source>
        <translation type="unfinished">Zapis</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="266"/>
        <source>Failed change login settings.</source>
        <translation type="unfinished">Niepowodzenie zmiany ustawień logowania.</translation>
    </message>
</context>
<context>
    <name>PageSetTime</name>
    <message>
        <location filename="../qml/PageSetTime.qml" line="32"/>
        <source>Set Time</source>
        <translation type="unfinished">Ustaw Czas</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTime.qml" line="98"/>
        <source>Hour</source>
        <translation type="unfinished">Godzina</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTime.qml" line="105"/>
        <source>Minute</source>
        <translation type="unfinished">Minuta</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTime.qml" line="112"/>
        <source>Set</source>
        <translation type="unfinished">Ustaw</translation>
    </message>
</context>
<context>
    <name>PageSetTimezone</name>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="29"/>
        <source>Set Timezone</source>
        <translation type="unfinished">Wybór strefy</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="129"/>
        <source>Region</source>
        <translation type="unfinished">Region</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="135"/>
        <source>City</source>
        <translation type="unfinished">Miasto</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="141"/>
        <source>Set</source>
        <translation type="unfinished">Ustaw</translation>
    </message>
</context>
<context>
    <name>PageSettings</name>
    <message>
        <location filename="../qml/PageSettings.qml" line="28"/>
        <source>Settings</source>
        <translation type="unfinished">Ustawienia</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="58"/>
        <source>Calibration</source>
        <translation type="unfinished">Kalibracja</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="66"/>
        <source>Network</source>
        <translation type="unfinished">Sieć</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="75"/>
        <source>Platform &amp; Resin Tank</source>
        <translation type="unfinished">Platforma i zbiornik na żywicę</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="84"/>
        <source>Firmware</source>
        <translation type="unfinished">Firmware</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="93"/>
        <source>Settings &amp; Sensors</source>
        <translation type="unfinished">Ustawienia i czujniki</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="102"/>
        <source>Touchscreen</source>
        <translation type="unfinished">Ekran dotykowy</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="111"/>
        <source>Language &amp; Time</source>
        <translation type="unfinished">Język i czas</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="120"/>
        <source>System Logs</source>
        <translation type="unfinished">Logi systemowe</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="129"/>
        <source>Support</source>
        <translation type="unfinished">Wsparcie</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="139"/>
        <source>Enter Admin</source>
        <translation type="unfinished">Otwórz menu Admina</translation>
    </message>
</context>
<context>
    <name>PageSettingsCalibration</name>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="31"/>
        <source>Calibration</source>
        <translation type="unfinished">Kalibracja</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="50"/>
        <source>Selftest</source>
        <translation type="unfinished">Selftest</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="57"/>
        <source>Printer Calibration</source>
        <translation type="unfinished">Kalibracja drukarki</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="67"/>
        <source>UV Calibration</source>
        <translation type="unfinished">Kalibracja UV</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="75"/>
        <source>Display Test</source>
        <translation type="unfinished">Test wyświetlacza</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="92"/>
        <source>New Display?</source>
        <translation type="unfinished">Nowy wyświetlacz?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="93"/>
        <source>Did you replace the EXPOSITION DISPLAY?</source>
        <translation type="unfinished">Czy EKRAN NAŚWIETLAJĄCY
został wymieniony na nowy?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="105"/>
        <source>New UV LED SET?</source>
        <translation type="unfinished">Nowy zestaw UV LED?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="106"/>
        <source>Did you replace the UV LED SET?</source>
        <translation type="unfinished">Czy moduł UV LED został wymieniony na nowy?</translation>
    </message>
</context>
<context>
    <name>PageSettingsFirmware</name>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="35"/>
        <source>Firmware</source>
        <translation type="unfinished">Firmware</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="45"/>
        <source>Unknown</source>
        <comment>Unknown operating system version on alternative slot</comment>
        <translation type="unfinished">Nieznany</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="74"/>
        <source>Download Now</source>
        <translation type="unfinished">Pobierz teraz</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="76"/>
        <source>Install Now</source>
        <translation type="unfinished">Zainstaluj</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="78"/>
        <source>Check for Update</source>
        <translation type="unfinished">Sprawdź aktualizacje</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="85"/>
        <source>Check for update failed</source>
        <translation type="unfinished">Niepowodzenie sprawdzenia dostępności aktualizacji</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="87"/>
        <source>Checking for updates...</source>
        <translation type="unfinished">Sprawdzanie dostępności aktualizacji...</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="89"/>
        <source>System is up-to-date</source>
        <translation type="unfinished">System jest aktualny</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="91"/>
        <source>Update available</source>
        <translation type="unfinished">Aktualizacja dostępna</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="93"/>
        <source>Update is downloading</source>
        <translation type="unfinished">Pobieranie aktualizacji</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="95"/>
        <source>Update is downloaded</source>
        <translation type="unfinished">Pobieranie aktualizacji</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="97"/>
        <source>Update download failed</source>
        <translation type="unfinished">Niepowodzenie pobierania aktualizacji</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="99"/>
        <source>Updater service idle</source>
        <translation type="unfinished">Usługa aktualizacji jest bezczynna</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="111"/>
        <source>Installed version</source>
        <translation type="unfinished">Zainstalowana wersja</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="137"/>
        <source>Receive Beta Updates</source>
        <translation type="unfinished">Otrzymuj aktualizacje Beta</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="155"/>
        <source>Switch to beta?</source>
        <translation type="unfinished">Przełączyć na kanał beta?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="156"/>
        <source>Warning! The beta updates can be unstable.&lt;br/&gt;&lt;br/&gt;Not recommended for production printers.&lt;br/&gt;&lt;br/&gt;Continue?</source>
        <translation type="unfinished">Uwaga! Wersje beta mogą być niestabilne. &lt;br/&gt;&lt;br/&gt;Nie zalecamy ich w środowisku produkcyjnym. &lt;br/&gt;&lt;br/&gt;Kontynuować?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="168"/>
        <source>Switch to version:</source>
        <translation type="unfinished">Przełącz na wersję:</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="180"/>
        <source>Downgrade?</source>
        <translation type="unfinished">Zejść na niższą wersję?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="181"/>
        <source>Do you really want to downgrade to FW</source>
        <translation type="unfinished">Czy na pewno chcesz zejść na niższą wersję FW</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="209"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="415"/>
        <source>Incompatible FW!</source>
        <translation type="unfinished">Niekompatybilne FW!</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="210"/>
        <source>The alternative FW version &lt;b&gt;%1&lt;/b&gt; is not compatible with your printer model - &lt;b&gt;%2&lt;/b&gt;.</source>
        <translation type="unfinished">Alternatywna wersja FW &lt;b&gt;%1&lt;/b&gt; nie jest kompatybilna z wersją drukarki - &lt;b&gt;%2&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="212"/>
        <source>If you switch, you can update to another FW version afterwards but &lt;b&gt;you will not be able to print.&lt;/b&gt;</source>
        <translation type="unfinished">Po zmianie możesz zaktualizować FW do innej wersji, jednak &lt;b&gt;drukowanie nie będzie możliwe.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="214"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="420"/>
        <source>Continue anyway?</source>
        <translation type="unfinished">Czy na pewno chcesz kontynuować?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="234"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="427"/>
        <source>None</source>
        <comment>Printer model is not known/can&apos;t be determined</comment>
        <translation type="unfinished">Brak</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="238"/>
        <source>Newer than SL1S</source>
        <comment>Printer model is unknown, but better than SL1S</comment>
        <translation type="unfinished">Nowsza niż SL1S</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="247"/>
        <source>Factory Reset</source>
        <translation type="unfinished">Przywrócenie ustawień fabrycznych</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="256"/>
        <source>Are you sure?</source>
        <translation type="unfinished">Na pewno?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="257"/>
        <source>Do you really want to perform the factory reset?

All settings will be erased!
Projects will stay untouched.</source>
        <translation type="unfinished">Czy na pewno chcesz przeprowadzić reset ustawień fabrycznych?

Wszystkie ustawienia zostają wymazane!
Projekty pozostaną nietknięte.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="292"/>
        <source>FW Info</source>
        <comment>page title, information about the selected update bundle</comment>
        <translation type="unfinished">Info o FW</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="293"/>
        <source>Loading FW file meta information
(may take up to 20 seconds) ...</source>
        <translation type="unfinished">Wczytywanie meta danych pliku z FW
(może zająć do 20 sekund) ...</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="335"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="384"/>
        <source>Install Firmware?</source>
        <translation type="unfinished">Zainstalować firmware?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="336"/>
        <source>FW file meta information not found.&lt;br/&gt;&lt;br/&gt;Do you want to install&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;anyway?</source>
        <translation type="unfinished">Nie znaleziono metadanych w pliku FW.&lt;br/&gt;&lt;br/&gt;Czy mimo to chcesz go zainstalować&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="349"/>
        <source>Downgrade Firmware?</source>
        <translation type="unfinished">Zejść na niższą wersję firmware?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="352"/>
        <source>Version of selected FW file&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;,</source>
        <translation type="unfinished">Wersja wybranego pliku FW &lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;,</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="354"/>
        <source>is lower or equal than current&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;.</source>
        <translation type="unfinished">jest niższa lub równa obecnej &lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="356"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="389"/>
        <source>Do you want to continue?</source>
        <translation type="unfinished">Czy chcesz kontynuować?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="385"/>
        <source>You have selected update bundle &lt;b&gt;&quot;%1&quot;&lt;/b&gt;,</source>
        <translation type="unfinished">Została wybrana paczka aktualizacji &lt;b&gt;&quot;%1&quot;&lt;/b&gt;,</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="387"/>
        <source>which has version &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation type="unfinished">która ma wersję &lt;b&gt;%1&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="416"/>
        <source>&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;is not compatible with your current hardware model - &lt;b&gt;%2&lt;/b&gt;.</source>
        <translation type="unfinished">&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt; - niekompatybilność z obecną wersją sprzętową - &lt;b&gt;%2&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="418"/>
        <source>After installing this update, the printer can be updated to another FW but &lt;b&gt;printing won&apos;t work.&lt;/b&gt;</source>
        <translation type="unfinished">Po zainstalowaniu tej wersji drukarkę będzie można zaktualizować do innej wersji FW, ale &lt;b&gt;drukowanie nie będzie działać.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="430"/>
        <source>Unknown</source>
        <comment>Printer model is unknown, but likely better than SL1S</comment>
        <translation type="unfinished">Nieznany</translation>
    </message>
</context>
<context>
    <name>PageSettingsLanguageTime</name>
    <message>
        <location filename="../qml/PageSettingsLanguageTime.qml" line="28"/>
        <source>Language &amp; Time</source>
        <translation type="unfinished">Język i czas</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsLanguageTime.qml" line="34"/>
        <source>Set Language</source>
        <translation type="unfinished">Język</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsLanguageTime.qml" line="40"/>
        <source>Time Settings</source>
        <translation type="unfinished">Ustawienia czasu</translation>
    </message>
</context>
<context>
    <name>PageSettingsNetwork</name>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="27"/>
        <source>Network</source>
        <translation type="unfinished">Sieć</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="33"/>
        <source>Ethernet</source>
        <translation type="unfinished">Ethernet</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="41"/>
        <source>Wi-Fi</source>
        <translation type="unfinished">Wi-Fi</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="49"/>
        <source>Hot Spot</source>
        <translation type="unfinished">Hot Spot</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="58"/>
        <source>Set Hostname</source>
        <translation type="unfinished">Zapisz</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="67"/>
        <source>Login Credentials</source>
        <translation type="unfinished">Dane do logowania</translation>
    </message>
</context>
<context>
    <name>PageSettingsPlatformResinTank</name>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="27"/>
        <source>Platform &amp; Resin Tank</source>
        <translation type="unfinished">Platforma i zbiornik na żywicę</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="33"/>
        <source>Move Platform</source>
        <translation type="unfinished">Ruch platformy</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="40"/>
        <source>Move Resin Tank</source>
        <translation type="unfinished">Ruch zbiornika</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="47"/>
        <source>Disable Steppers</source>
        <translation type="unfinished">Wyłącz silniki krokowe</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="55"/>
        <source>Platform Axis Sensitivity</source>
        <translation type="unfinished">Czułość osi platformy</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="77"/>
        <source>Tank Axis Sensitivity</source>
        <translation type="unfinished">Czułość osi zbiornika</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="99"/>
        <source>Limit for Fast Tilt</source>
        <translation type="unfinished">Limit szybkiego przechylania</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="106"/>
        <source>%</source>
        <translation type="unfinished">%</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="125"/>
        <source>Platform Offset</source>
        <translation type="unfinished">Odsadzenie platformy</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="132"/>
        <source>um</source>
        <translation type="unfinished">um</translation>
    </message>
</context>
<context>
    <name>PageSettingsSensors</name>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="27"/>
        <source>Settings &amp; Sensors</source>
        <translation type="unfinished">Ustawienia i czujniki</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="35"/>
        <source>Device hash in QR</source>
        <translation type="unfinished">Identyfikator urządzenia w QR</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="60"/>
        <source>Auto Power Off</source>
        <translation type="unfinished">Automatyczne wyłączanie</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="86"/>
        <source>Cover Check</source>
        <translation type="unfinished">Czujnik otwarcia pokrywy</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="110"/>
        <location filename="../qml/PageSettingsSensors.qml" line="145"/>
        <source>Are you sure?</source>
        <translation type="unfinished">Na pewno?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="111"/>
        <source>Disable the cover sensor?&lt;br/&gt;&lt;br/&gt;CAUTION: This may lead to unwanted exposure to UV light or personal injury due to moving parts. This action is not recommended!</source>
        <translation type="unfinished">Wyłączyć czujnik pokrywy? &lt;br/&gt;&lt;br/&gt;UWAGA: Może to powodować niepożądane narażenie na światło ultrafioletowe. Nie jest to zalecane!</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="123"/>
        <source>Resin Sensor</source>
        <translation type="unfinished">Czujnik poziomu żywicy</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="146"/>
        <source>Disable the resin sensor?&lt;br/&gt;&lt;br/&gt;CAUTION: This may lead to failed prints or resin tank overflow! This action is not recommended!</source>
        <translation type="unfinished">Wyłączyć czujnik poziomu żywicy?&lt;br/&gt;&lt;br/&gt;UWAGA: Może to prowadzić do nieudanych wydruków lub przelania żywicy. Nie jest to zalecane!</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="158"/>
        <source>Rear Fan Speed</source>
        <translation type="unfinished">Prędkość tylnego went.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="165"/>
        <source>RPM</source>
        <translation type="unfinished">Obr./min</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="177"/>
        <source>Off</source>
        <translation type="unfinished">Wył.</translation>
    </message>
</context>
<context>
    <name>PageSettingsSupport</name>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="29"/>
        <source>Support</source>
        <translation type="unfinished">Wsparcie</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="35"/>
        <source>Download Examples</source>
        <translation type="unfinished">Pobierz przykładowe modele</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="49"/>
        <source>Manual</source>
        <translation type="unfinished">Instrukcje</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="59"/>
        <source>Videos</source>
        <translation type="unfinished">Wideo</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="68"/>
        <source>System Information</source>
        <translation type="unfinished">System - informacje</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="77"/>
        <source>About Us</source>
        <translation type="unfinished">O nas</translation>
    </message>
</context>
<context>
    <name>PageSettingsSystemLogs</name>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="28"/>
        <source>System Logs</source>
        <translation type="unfinished">Logi systemowe</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="39"/>
        <source>Last Seen Logs:</source>
        <translation type="unfinished">Ostatnio oglądane logi:</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="39"/>
        <source>&lt;b&gt;No logs have been uploaded yet.&lt;/b&gt;</source>
        <translation type="unfinished">&lt;b&gt;Nie przesłano jeszcze żadnych logów.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="54"/>
        <source>Save to USB Drive</source>
        <translation type="unfinished">Zapisz na USB</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="68"/>
        <source>Upload to Server</source>
        <translation type="unfinished">Prześlij na serwer</translation>
    </message>
</context>
<context>
    <name>PageSettingsTouchscreen</name>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="27"/>
        <source>Touchscreen</source>
        <translation type="unfinished">Ekran dotykowy</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="35"/>
        <source>Screensaver timer</source>
        <translation type="unfinished">Czas wygaszacza ekranu</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="55"/>
        <source>h</source>
        <comment>hours short</comment>
        <translation type="unfinished">g</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="56"/>
        <source>min</source>
        <comment>minutes short</comment>
        <translation type="unfinished">min</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="57"/>
        <source>s</source>
        <comment>seconds short</comment>
        <translation type="unfinished">s</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="62"/>
        <source>Off</source>
        <translation type="unfinished">Wył.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="79"/>
        <source>Touch Screen Brightness</source>
        <translation type="unfinished">Jasność ekranu dotykowego</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="97"/>
        <source>N/A</source>
        <translation type="unfinished">N/D</translation>
    </message>
</context>
<context>
    <name>PageShowToken</name>
    <message>
        <location filename="../qml/PageShowToken.qml" line="27"/>
        <source>Prusa Connect</source>
        <translation type="unfinished">Prusa Connect</translation>
    </message>
    <message>
        <location filename="../qml/PageShowToken.qml" line="31"/>
        <source>Temporary Token</source>
        <translation type="unfinished">Tymczasowy Token</translation>
    </message>
    <message>
        <location filename="../qml/PageShowToken.qml" line="61"/>
        <source>N/A</source>
        <translation type="unfinished">N/D</translation>
    </message>
    <message>
        <location filename="../qml/PageShowToken.qml" line="80"/>
        <source>Continue</source>
        <translation type="unfinished">Kontynuuj</translation>
    </message>
</context>
<context>
    <name>PageSoftwareLicenses</name>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="28"/>
        <source>Software Packages</source>
        <translation type="unfinished">Paczki z oprogramowaniem</translation>
    </message>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="112"/>
        <location filename="../qml/PageSoftwareLicenses.qml" line="233"/>
        <source>Package Name</source>
        <translation type="unfinished">Nazwa paczki</translation>
    </message>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="242"/>
        <source>Version</source>
        <translation type="unfinished">Wersja</translation>
    </message>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="250"/>
        <source>License</source>
        <translation type="unfinished">Licencja</translation>
    </message>
</context>
<context>
    <name>PageSysinfo</name>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="30"/>
        <source>System Information</source>
        <translation type="unfinished">System - informacje</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="52"/>
        <source>System</source>
        <translation type="unfinished">System</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="57"/>
        <source>OS Image Version</source>
        <translation type="unfinished">Wersja obrazu systemu</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="62"/>
        <source>Printer Model</source>
        <translation type="unfinished">Model drukarki</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="65"/>
        <source>None</source>
        <comment>Printer model is not known/can&apos;t be determined</comment>
        <translation type="unfinished">Brak</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="69"/>
        <source>Newer than SL1S</source>
        <comment>Printer model is unknown, but better than SL1S</comment>
        <translation type="unfinished">Nowsza niż SL1S</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="75"/>
        <source>A64 Controller SN</source>
        <translation type="unfinished">Numer seryjny kontrolera A64</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="80"/>
        <source>API Key / Printer Password</source>
        <translation type="unfinished">Klucz API / Hasło</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="92"/>
        <source>Other Components</source>
        <translation type="unfinished">Inne komponenty</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="97"/>
        <source>Motion Controller SN</source>
        <translation type="unfinished">Numer seryjny kontrolera ruchu</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="102"/>
        <source>Motion Controller SW Version</source>
        <translation type="unfinished">Wersja SW kontrolera ruchu</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="107"/>
        <source>Motion Controller HW Revision</source>
        <translation type="unfinished">Rewizja HW kontrolera ruchu</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="112"/>
        <source>Booster Board SN</source>
        <translation type="unfinished">Numer seryjny wzmacniacza</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="118"/>
        <source>Exposure display SN</source>
        <translation type="unfinished">Numer seryjny wyświetlacza druku</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="124"/>
        <source>GUI Version</source>
        <translation type="unfinished">Wersja GUI</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="136"/>
        <source>Hardware State</source>
        <translation type="unfinished">Stan sprzętu</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="141"/>
        <source>Network State</source>
        <translation type="unfinished">Stan sieci</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="142"/>
        <source>Online</source>
        <translation type="unfinished">Online</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="142"/>
        <source>Offline</source>
        <translation type="unfinished">Offline</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="146"/>
        <source>Ethernet IP Address</source>
        <translation type="unfinished">Adres IP Ethernet</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="147"/>
        <location filename="../qml/PageSysinfo.qml" line="152"/>
        <location filename="../qml/PageSysinfo.qml" line="230"/>
        <location filename="../qml/PageSysinfo.qml" line="263"/>
        <location filename="../qml/PageSysinfo.qml" line="268"/>
        <location filename="../qml/PageSysinfo.qml" line="273"/>
        <location filename="../qml/PageSysinfo.qml" line="278"/>
        <location filename="../qml/PageSysinfo.qml" line="283"/>
        <source>N/A</source>
        <translation type="unfinished">N/D</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="151"/>
        <source>Wifi IP Address</source>
        <translation type="unfinished">Adres IP Wi-Fi</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="156"/>
        <source>Time of Fast Tilt</source>
        <translation type="unfinished">Czas szybkiego przechylania</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="157"/>
        <location filename="../qml/PageSysinfo.qml" line="162"/>
        <source>seconds</source>
        <translation type="unfinished">Sekundy</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="161"/>
        <source>Time of Slow Tilt</source>
        <translation type="unfinished">Czas wolnego przechylania</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="166"/>
        <source>Resin Sensor State</source>
        <translation type="unfinished">Stan czujnika poziomu żywicy</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="167"/>
        <source>Triggered</source>
        <translation type="unfinished">Aktywowano</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="167"/>
        <source>Not triggered</source>
        <translation type="unfinished">Nie aktywowano</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="171"/>
        <source>Cover State</source>
        <translation type="unfinished">Stan pokrywy</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="172"/>
        <source>Closed</source>
        <translation type="unfinished">Zamknięta</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="172"/>
        <source>Open</source>
        <translation type="unfinished">Otwarta</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="176"/>
        <source>CPU Temperature</source>
        <translation type="unfinished">Temperatura CPU</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="177"/>
        <location filename="../qml/PageSysinfo.qml" line="182"/>
        <location filename="../qml/PageSysinfo.qml" line="187"/>
        <source>°C</source>
        <translation type="unfinished">°C</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="181"/>
        <source>UV LED Temperature</source>
        <translation type="unfinished">Temperatura UV LED</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="186"/>
        <source>Ambient Temperature</source>
        <translation type="unfinished">Temperatura otoczenia</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="191"/>
        <source>UV LED Fan</source>
        <translation type="unfinished">Wentylator UV LED</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="194"/>
        <location filename="../qml/PageSysinfo.qml" line="204"/>
        <location filename="../qml/PageSysinfo.qml" line="214"/>
        <source>Fan Error!</source>
        <translation type="unfinished">Błąd wentylatora!</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="196"/>
        <location filename="../qml/PageSysinfo.qml" line="206"/>
        <location filename="../qml/PageSysinfo.qml" line="216"/>
        <source>RPM</source>
        <translation type="unfinished">Obr./min</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="201"/>
        <source>Blower Fan</source>
        <translation type="unfinished">Wentylator boczny</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="211"/>
        <source>Rear Fan</source>
        <translation type="unfinished">Wentylator tylny</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="221"/>
        <source>UV LED</source>
        <translation type="unfinished">UV LED</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="235"/>
        <source>Power Supply Voltage</source>
        <translation type="unfinished">Napięcie zasilacza</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="236"/>
        <source>V</source>
        <translation type="unfinished">V</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="247"/>
        <source>Statistics</source>
        <translation type="unfinished">Statystyki</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="252"/>
        <source>UV LED Time Counter</source>
        <translation type="unfinished">Licznik czasu UV LED</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="253"/>
        <location filename="../qml/PageSysinfo.qml" line="258"/>
        <location filename="../qml/PageSysinfo.qml" line="278"/>
        <source>d</source>
        <translation type="unfinished">d</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="253"/>
        <location filename="../qml/PageSysinfo.qml" line="258"/>
        <location filename="../qml/PageSysinfo.qml" line="278"/>
        <source>h</source>
        <translation type="unfinished">g</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="253"/>
        <location filename="../qml/PageSysinfo.qml" line="258"/>
        <location filename="../qml/PageSysinfo.qml" line="278"/>
        <source>m</source>
        <translation type="unfinished">m</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="257"/>
        <source>Print Display Time Counter</source>
        <translation type="unfinished">Licznik czasu wyświetlacza druku</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="262"/>
        <source>Started Projects</source>
        <translation type="unfinished">Rozpoczęte projekty</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="267"/>
        <source>Finished Projects</source>
        <translation type="unfinished">Ukończone projekty</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="272"/>
        <source>Total Layers Printed</source>
        <translation type="unfinished">Całkowita liczba wydrukowanych warstw</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="277"/>
        <source>Total Print Time</source>
        <translation type="unfinished">Całkowity czas druku</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="282"/>
        <source>Total Resin Consumed</source>
        <translation type="unfinished">Całkowite zużycie żywicy</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="283"/>
        <source>ml</source>
        <translation type="unfinished">ml</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="293"/>
        <source>Show software packages &amp; licenses</source>
        <translation type="unfinished">Pokaż paczki z oprogramowaniem i licencje</translation>
    </message>
</context>
<context>
    <name>PageTiltmove</name>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="32"/>
        <source>Move Resin Tank</source>
        <translation type="unfinished">Ruch zbiornika</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="76"/>
        <source>Fast Up</source>
        <translation type="unfinished">W górę szybko</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="84"/>
        <source>Fast Down</source>
        <translation type="unfinished">W dół szybko</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="92"/>
        <source>Up</source>
        <translation type="unfinished">W górę</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="100"/>
        <source>Down</source>
        <translation type="unfinished">W dół</translation>
    </message>
</context>
<context>
    <name>PageTimeSettings</name>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="34"/>
        <source>Time Settings</source>
        <translation type="unfinished">Ustawienia czasu</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="49"/>
        <source>Use NTP</source>
        <translation type="unfinished">Automatyczne ustawienie czasu</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="67"/>
        <source>Time</source>
        <translation type="unfinished">Czas</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="87"/>
        <source>Date</source>
        <translation type="unfinished">Data</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="107"/>
        <source>Timezone</source>
        <translation type="unfinished">Strefa czasowa</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="124"/>
        <source>Time Format</source>
        <translation type="unfinished">Format czasu</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="135"/>
        <source>Native</source>
        <comment>Default time format determined by the locale</comment>
        <translation type="unfinished">Natywny</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="136"/>
        <source>12-hour</source>
        <comment>12h time format</comment>
        <translation type="unfinished">12-godzinny</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="137"/>
        <source>24-hour</source>
        <comment>24h time format</comment>
        <translation type="unfinished">24-godzinny</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="158"/>
        <source>Automatic time settings using NTP ...</source>
        <translation type="unfinished">Automatyczne ustawienie czasu z NTP ...</translation>
    </message>
</context>
<context>
    <name>PageTowermove</name>
    <message>
        <location filename="../qml/PageTowermove.qml" line="32"/>
        <source>Move Platform</source>
        <translation type="unfinished">Ruch platformy</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="77"/>
        <source>Fast Up</source>
        <translation type="unfinished">W górę szybko</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="85"/>
        <source>Fast Down</source>
        <translation type="unfinished">W dół szybko</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="93"/>
        <source>Up</source>
        <translation type="unfinished">W górę</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="101"/>
        <source>Down</source>
        <translation type="unfinished">W dół</translation>
    </message>
</context>
<context>
    <name>PageUnpackingCompleteWizard</name>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="31"/>
        <source>Unpacking</source>
        <translation type="unfinished">Rozpakowanie</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="47"/>
        <source>Unscrew and remove the resin tank and remove the black foam underneath it.</source>
        <translation type="unfinished">Odkręć i wyciągnij zbiornik na żywicę i wyjmij czarną piankę leżącą pod nim.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="56"/>
        <source>Remove the black foam from both sides of the platform.</source>
        <translation type="unfinished">Wyciągnij czarną piankę z obydwóch stron platformy.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="65"/>
        <source>Please remove the safety sticker and open the cover.</source>
        <translation type="unfinished">Odklej naklejkę zabezpieczającą i otwórz pokrywę.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="74"/>
        <source>Carefully peel off the protective sticker from the exposition display.</source>
        <translation type="unfinished">Ostrożnie zdejmij naklejkę ochronną z ekranu naświetlającego.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="83"/>
        <source>Unpacking done.</source>
        <translation type="unfinished">Rozpakowanie zakończone.</translation>
    </message>
</context>
<context>
    <name>PageUnpackingKitWizard</name>
    <message>
        <location filename="../qml/PageUnpackingKitWizard.qml" line="31"/>
        <source>Unpacking</source>
        <translation type="unfinished">Rozpakowanie</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingKitWizard.qml" line="44"/>
        <source>Carefully peel off the protective sticker from the exposition display.</source>
        <translation type="unfinished">Ostrożnie zdejmij naklejkę ochronną z ekranu naświetlającego.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingKitWizard.qml" line="53"/>
        <source>Unpacking done.</source>
        <translation type="unfinished">Rozpakowanie zakończone.</translation>
    </message>
</context>
<context>
    <name>PageUpdatingFirmware</name>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="36"/>
        <source>Printer Update</source>
        <translation type="unfinished">Aktualizacja drukarki</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="42"/>
        <source>Unknown</source>
        <translation type="unfinished">Nieznany</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="170"/>
        <source>Downloading</source>
        <translation type="unfinished">Pobieranie</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="211"/>
        <source>Downloading firmware, installation will begin immediately after.</source>
        <translation type="unfinished">Pobieranie firmware. Instalacja zacznie się natychmiast po zakończeniu.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="228"/>
        <source>Download failed.</source>
        <translation type="unfinished">Pobieranie nieudane.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="254"/>
        <source>Installing Firmware</source>
        <translation type="unfinished">Instalowanie firmware</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="295"/>
        <source>Do not power off the printer while updating!&lt;br/&gt;Printer will be rebooted after a successful update.</source>
        <translation type="unfinished">Nie wyłączaj drukarki podczas aktualizowania!&lt;br/&gt;Drukarka zostanie zrestartowana po pomyślnej instalacji.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="345"/>
        <source>Updated to %1 failed.</source>
        <translation type="unfinished">Niepowodzenie aktualizacji do %1 .</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="362"/>
        <source>Printer is being restarted into the new firmware(%1), please wait</source>
        <translation type="unfinished">Drukarka jest restartowana na nowym firmware (%1), proszę czekać</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="387"/>
        <source>Update to %1 failed.</source>
        <translation type="unfinished">Niepowodzenie aktualizacji do %1 .</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="420"/>
        <source>A problem has occurred while updating, please let us know (send us the logs). Do not panic, your printer is still working the same as it did before. Continue by pressing &quot;back&quot;</source>
        <translation type="unfinished">Wystąpił problem podczas aktualizacji. Daj nam znać, co się stało (wysyłając logi). Nie panikuj, Twoja drukarka wciąż działa tak samo, jak wcześniej. Wciśnij &apos;Wstecz&apos;, aby kontynuować</translation>
    </message>
</context>
<context>
    <name>PageUpgradeWizard</name>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="31"/>
        <source>Hardware Upgrade</source>
        <translation type="unfinished">Aktualizacja sprzętowa</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="45"/>
        <source>SL1S components detected (upgrade from SL1).</source>
        <translation type="unfinished">Wykryto komponenty SL1S (wersja wyższa od SL1).</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="46"/>
        <source>To complete the upgrade procedure, printer needs to clear the configuration and reboot.</source>
        <translation type="unfinished">Do zakończenia procedury aktualizacji wersji konieczne jest wyczyszczenie konfiguracji i restart drukarki.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="47"/>
        <source>Proceed?</source>
        <translation type="unfinished">Kontynuować?</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="56"/>
        <source>The printer will power off now.</source>
        <translation type="unfinished">Drukarka zostanie teraz wyłączona.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="57"/>
        <source>Reassemble SL1 components and power on the printer. This will restore the original state.</source>
        <translation type="unfinished">Ponownie zmontuj komponenty SL1 i włącz urządzenie. Zostanie przywrócone do oryginalnego stanu.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="65"/>
        <source>The configuration is going to be cleared now.</source>
        <translation type="unfinished">Konfiguracja zostanie teraz wyczyszczona.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="66"/>
        <source>The printer will ask for the inital setup after reboot.</source>
        <translation type="unfinished">Drukarka poprosi o konfigurację początkową po uruchomieniu.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="75"/>
        <source>Use only the plastic resin tank supplied. Using the old metal resin tank may cause resin to spill and damage your printer!</source>
        <translation type="unfinished">Używaj tylko plastikowego zbiornika na żywicę dostarczonego razem z drukarką. Używanie innego zbiornika może doprowadzić do rozlania żywicy i uszkodzenia drukarki!</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="84"/>
        <source>Only use the platform supplied. Using a different platform may cause resin to spill and damage your printer!</source>
        <translation type="unfinished">Używaj tylko platformy dostarczonej razem z drukarką. Używanie innej platformy może doprowadzić do rozlania żywicy i uszkodzenia drukarki!</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="93"/>
        <source>Please note that downgrading is not supported. 

Downgrading your printer will erase your UV calibration and your printer will not work properly. 

You will need to recalibrate it using an external UV calibrator.</source>
        <translation type="unfinished">Weź pod uwagę, że zejście na niższą wersję nie jest wspierane.

Zejście na niższą wersję spowoduje wyczyszczenie danych kalibracji UV, a drukarka nie będzie działać poprawnie.

Konieczna będzie ponowna kalibracja przy pomocy zewnętrznego kalibratora UV.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="101"/>
        <source>Upgrade done. In the next step, the printer will be restarted.</source>
        <translation type="unfinished">Aktualizacja zakończona. W kolejnym kroku drukarka zostanie zrestartowana.</translation>
    </message>
</context>
<context>
    <name>PageUvCalibrationWizard</name>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="31"/>
        <source>UV Calibration</source>
        <translation type="unfinished">Kalibracja UV</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="46"/>
        <source>Welcome to the UV calibration.</source>
        <translation type="unfinished">Witaj w kalibracji UV.</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="48"/>
        <source>1. If the resin tank is in the printer, remove it along with the screws.</source>
        <translation type="unfinished">Jeśli zbiornik na żywicę jest w drukarce, wyciągnij go ze śrubami.</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="50"/>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="70"/>
        <source>2. Close the cover, don&apos;t open it! UV radiation is harmful!</source>
        <translation type="unfinished">2. Zamknij pokrywę i nie otwieraj! Promieniowanie UV jest szkodliwe!</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="53"/>
        <source>Intensity: center %1, edge %2</source>
        <translation type="unfinished">Intensywność: środek %1, krawędź %2</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="55"/>
        <source>Warm-up: %1 s</source>
        <translation type="unfinished">Nagrzewanie: %1 s</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="68"/>
        <source>1. Place the UV calibrator on the print display and connect it to the front USB.</source>
        <translation type="unfinished">1. Połóż miernik UV na wyświetlaczu druku i podłącz do portu USB z przodu drukarki.</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="80"/>
        <source>Open the cover, &lt;b&gt;remove and disconnect&lt;/b&gt; the UV calibrator.</source>
        <translation type="unfinished">Otwórz pokrywę, następnie &lt;b&gt;zdejmij i odłącz&lt;/b&gt; kalibrator UV.</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="95"/>
        <source>The result of calibration:</source>
        <translation type="unfinished">Wyniki kalibracji:</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="96"/>
        <source>UV PWM: %1</source>
        <translation type="unfinished">UV PWM: %1</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="97"/>
        <source>UV Intensity: %1, σ = %2</source>
        <translation type="unfinished">Intensywność UV: %1, σ = %2</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="98"/>
        <source>UV Intensity min: %1, max: %2</source>
        <translation type="unfinished">Intensywność UV: min %1, max %2</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="102"/>
        <source>The printer has been successfully calibrated!
Would you like to apply the calibration results?</source>
        <translation type="unfinished">Drukarka została pomyślnie skalibrowana!
Czy chcesz zastosować wyniki kalibracji?</translation>
    </message>
</context>
<context>
    <name>PageVerticalList</name>
    <message>
        <location filename="../qml/PageVerticalList.qml" line="57"/>
        <source>Loading, please wait...</source>
        <translation type="unfinished">Ładowanie, proszę czekać...</translation>
    </message>
</context>
<context>
    <name>PageVideos</name>
    <message>
        <location filename="../qml/PageVideos.qml" line="23"/>
        <source>Videos</source>
        <translation type="unfinished">Wideo</translation>
    </message>
    <message>
        <location filename="../qml/PageVideos.qml" line="26"/>
        <source>Scanning the QR code will take you to our YouTube playlist with videos about this device.

Alternatively, use this link:</source>
        <translation type="unfinished">Zeskanowanie kodu QR przekieruje Cię do playlisty na YouTube z materiałami dot. tego urządzenia.

Możesz również użyć tego linku:</translation>
    </message>
</context>
<context>
    <name>PageWait</name>
    <message>
        <location filename="../qml/PageWait.qml" line="27"/>
        <source>Please Wait</source>
        <translation type="unfinished">Proszę czekać</translation>
    </message>
</context>
<context>
    <name>PageWifiNetworkSettings</name>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="34"/>
        <source>Wireless Settings</source>
        <translation type="unfinished">Ustawienia Wi-Fi</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="90"/>
        <source>Connected</source>
        <translation type="unfinished">Połączono</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="90"/>
        <source>Disconnected</source>
        <translation type="unfinished">Rozłączono</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="102"/>
        <source>Network Info</source>
        <translation type="unfinished">Informacje o sieci</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="109"/>
        <source>DHCP</source>
        <translation type="unfinished">DHCP</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="191"/>
        <source>Apply</source>
        <translation type="unfinished">Zastosuj</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="201"/>
        <source>Configuring the connection,
please wait...</source>
        <comment>This is horizontal-center aligned, ideally 2 lines</comment>
        <translation type="unfinished">Konfigurowanie połączenia, proszę czekać...</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="208"/>
        <source>Revert</source>
        <comment>Turn back the changes and go back to the previous configuration.</comment>
        <translation type="unfinished">Cofnij</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="218"/>
        <source>Forget network</source>
        <comment>Removes all information about the network(settings, passwords,...)</comment>
        <translation type="unfinished">Usuń sieć</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="226"/>
        <source>Forget network?</source>
        <translation type="unfinished">Usunąć sieć?</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="227"/>
        <source>Do you really want to forget this network&apos;s settings?</source>
        <translation type="unfinished">Czy na pewno chcesz zapomnieć ustawienia tej sieci?</translation>
    </message>
</context>
<context>
    <name>PageYesNoSimple</name>
    <message>
        <location filename="../qml/PageYesNoSimple.qml" line="28"/>
        <source>Are You Sure?</source>
        <translation type="unfinished">Na pewno?</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSimple.qml" line="60"/>
        <source>Yes</source>
        <translation type="unfinished">Tak</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSimple.qml" line="76"/>
        <source>No</source>
        <translation type="unfinished">Nie</translation>
    </message>
</context>
<context>
    <name>PageYesNoSwipe</name>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="29"/>
        <source>Are You Sure?</source>
        <translation type="unfinished">Na pewno?</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="76"/>
        <source>Swipe to proceed</source>
        <translation type="unfinished">Przesuń ekran, aby kontynuować</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="97"/>
        <source>Yes</source>
        <translation type="unfinished">Tak</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="110"/>
        <source>No</source>
        <translation type="unfinished">Nie</translation>
    </message>
</context>
<context>
    <name>PageYesNoWithPicture</name>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="28"/>
        <source>Are You Sure?</source>
        <translation type="unfinished">Na pewno?</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="176"/>
        <source>Swipe for a picture</source>
        <translation type="unfinished">Przesuń do ilustracji</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="254"/>
        <source>Yes</source>
        <translation type="unfinished">Tak</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="267"/>
        <source>No</source>
        <translation type="unfinished">Nie</translation>
    </message>
</context>
<context>
    <name>PrusaPicturePictureButtonItem</name>
    <message>
        <location filename="../qml/PrusaPicturePictureButtonItem.qml" line="58"/>
        <source>Plugged in</source>
        <translation type="unfinished">Podłączono</translation>
    </message>
    <message>
        <location filename="../qml/PrusaPicturePictureButtonItem.qml" line="58"/>
        <source>Unplugged</source>
        <translation type="unfinished">Nie podłączono</translation>
    </message>
</context>
<context>
    <name>PrusaSwitch</name>
    <message>
        <location filename="../qml/PrusaSwitch.qml" line="112"/>
        <source>Off</source>
        <translation type="unfinished">Wył.</translation>
    </message>
    <message>
        <location filename="../qml/PrusaSwitch.qml" line="170"/>
        <source>On</source>
        <translation type="unfinished">Wł.</translation>
    </message>
</context>
<context>
    <name>PrusaWaitOverlay</name>
    <message>
        <location filename="../qml/PrusaWaitOverlay.qml" line="74"/>
        <source>Please wait...</source>
        <comment>can be on multiple lines</comment>
        <translation type="unfinished">Proszę czekać...</translation>
    </message>
</context>
<context>
    <name>SwipeSign</name>
    <message>
        <location filename="../qml/SwipeSign.qml" line="98"/>
        <source>Swipe to confirm</source>
        <translation type="unfinished">Przesuń, aby potwierdzić</translation>
    </message>
</context>
<context>
    <name>WarningText</name>
    <message>
        <location filename="../qml/WarningText.qml" line="45"/>
        <source>Must not be empty, only 0-9, a-z, A-Z, _ and - are allowed here!</source>
        <translation type="unfinished">Nie może być puste, dozwolone są tylko 0-9, a-z, A-Z, _ i - są tutaj dozwolone!</translation>
    </message>
</context>
<context>
    <name>WindowHeader</name>
    <message>
        <location filename="../PrusaComponents/Delegates/WindowHeader.qml" line="86"/>
        <source>back</source>
        <translation type="unfinished">wstecz</translation>
    </message>
    <message>
        <location filename="../PrusaComponents/Delegates/WindowHeader.qml" line="99"/>
        <source>cancel</source>
        <translation type="unfinished">anuluj</translation>
    </message>
    <message>
        <location filename="../PrusaComponents/Delegates/WindowHeader.qml" line="111"/>
        <source>close</source>
        <translation type="unfinished">zamknij</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="50"/>
        <source>Prusa SL1 Touchscreen User Interface</source>
        <translation type="unfinished">Dotykowy interfejs użytkownika Prusa SL1</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="64"/>
        <location filename="../qml/main.qml" line="69"/>
        <source>Unknown</source>
        <translation type="unfinished">Nieznany</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="65"/>
        <source>Activating</source>
        <translation type="unfinished">Łączenie</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="66"/>
        <source>Connected</source>
        <translation type="unfinished">Połączono</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="67"/>
        <source>Deactivating</source>
        <translation type="unfinished">Dezaktywacja</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="68"/>
        <source>Deactivated</source>
        <translation type="unfinished">Niepołączona</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="277"/>
        <source>Notifications</source>
        <translation type="unfinished">Powiadomienia</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="320"/>
        <source>Initializing...</source>
        <translation type="unfinished">Inicjowanie...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="321"/>
        <source>The printer is initializing, please wait ...</source>
        <translation type="unfinished">Drukarka uruchamia się, proszę czekać...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="330"/>
        <source>MC Update</source>
        <translation type="unfinished">Aktualizacja MC</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="331"/>
        <source>The Motion Controller firmware is being updated.

Please wait...</source>
        <translation type="unfinished">Trwa aktualizacja firmware Kontrolera Ruchu.

Proszę czekać...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="493"/>
        <source>Turn Off?</source>
        <translation type="unfinished">Wyłączyć?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="506"/>
        <source>Cancel?</source>
        <translation type="unfinished">Anuluj</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="521"/>
        <source>Cancel the current print job?</source>
        <translation type="unfinished">Przerwać bieżące zadanie?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="544"/>
        <source>Printer should not be turned off in this state.
Finish or cancel the current action and try again.</source>
        <translation type="unfinished">Drukarki nie można wyłączyć w tym stanie. Zakończ lub anuluj obecnie trwającą akcję i spróbuj ponownie.</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="555"/>
        <location filename="../qml/main.qml" line="579"/>
        <source>DEPRECATED PROJECTS</source>
        <translation type="unfinished">PRZESTARZAŁE PROJEKTY</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="556"/>
        <source>Some incompatible projects were found, you can download them at 

http://%1/old-projects

Do you want to remove them?</source>
        <translation type="unfinished">Odnaleziono niekompatybilne projekty. Możesz je pobrać z 

http://%1/old-projects

Czy chcesz je usunąć?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="570"/>
        <location filename="../qml/main.qml" line="593"/>
        <source>&lt;printer IP&gt;</source>
        <translation type="unfinished">&lt;IP drukarki&gt;</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="580"/>
        <source>Some incompatible file were found, you can view them at http://%1/old-projects.

Would you like to remove them?</source>
        <translation type="unfinished">Znaleziono niekompatybilne projekty. Możesz je ściągnąć z

http://%1/old-projects.

Czy chcesz je usunąć?</translation>
    </message>
</context>
<context>
    <name>utils</name>
    <message>
        <location filename="../qml/utils.js" line="47"/>
        <source>Less than a minute</source>
        <translation type="unfinished">Mniej niż minuta</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="50"/>
        <source>%n h</source>
        <comment>how many hours</comment>
        <translation type="unfinished">
            <numerusform>%n g</numerusform>
            <numerusform>%n g</numerusform>
            <numerusform>%n g</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="51"/>
        <source>%n min</source>
        <comment>how many minutes</comment>
        <translation type="unfinished">
            <numerusform>%n min</numerusform>
            <numerusform>%n min</numerusform>
            <numerusform>%n min</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="55"/>
        <source>%n hour(s)</source>
        <comment>how many hours</comment>
        <translation type="unfinished">
            <numerusform>%n godzin</numerusform>
            <numerusform>%n godzin</numerusform>
            <numerusform>%n godzina</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="56"/>
        <source>%n minute(s)</source>
        <comment>how many minutes</comment>
        <translation type="unfinished">
            <numerusform>%n minut</numerusform>
            <numerusform>%n minut</numerusform>
            <numerusform>%n minuta</numerusform>
        </translation>
    </message>
</context>
</TS>
