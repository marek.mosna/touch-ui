<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en-US">
<context>
    <name>DelegateAddHiddenNetwork</name>
    <message>
        <location filename="../qml/DelegateAddHiddenNetwork.qml" line="91"/>
        <source>Add Hidden Network</source>
        <translation type="unfinished">Add Hidden Network</translation>
    </message>
</context>
<context>
    <name>DelegateAdminPlusMinus</name>
    <message>
        <location filename="../qml/DelegateAdminPlusMinus.qml" line="41"/>
        <source>N/A</source>
        <translation type="unfinished">N/A</translation>
    </message>
</context>
<context>
    <name>DelegateEthNetwork</name>
    <message>
        <location filename="../qml/DelegateEthNetwork.qml" line="110"/>
        <source>Plugged in</source>
        <translation type="unfinished">Plugged in</translation>
    </message>
    <message>
        <location filename="../qml/DelegateEthNetwork.qml" line="110"/>
        <source>Unplugged</source>
        <translation type="unfinished">Unplugged</translation>
    </message>
</context>
<context>
    <name>DelegateRef</name>
    <message>
        <location filename="../qml/DelegateRef.qml" line="28"/>
        <source>N/A</source>
        <translation type="unfinished">N/A</translation>
    </message>
</context>
<context>
    <name>DelegateState</name>
    <message>
        <location filename="../qml/DelegateState.qml" line="38"/>
        <source>Network Info</source>
        <translation type="unfinished">Network info</translation>
    </message>
</context>
<context>
    <name>DelegateWifiClientOnOff</name>
    <message>
        <location filename="../qml/DelegateWifiClientOnOff.qml" line="39"/>
        <source>Wi-Fi Client</source>
        <translation type="unfinished">Wi-Fi Client</translation>
    </message>
</context>
<context>
    <name>DelegateWifiNetwork</name>
    <message>
        <location filename="../qml/DelegateWifiNetwork.qml" line="106"/>
        <source>Forget network?</source>
        <translation type="unfinished">Forget network?</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWifiNetwork.qml" line="107"/>
        <source>Do you really want to forget this network&apos;s settings?</source>
        <translation type="unfinished">Do you really want to forget this network&apos;s settings?</translation>
    </message>
</context>
<context>
    <name>DelegateWifiOnOff</name>
    <message>
        <location filename="../qml/DelegateWifiOnOff.qml" line="36"/>
        <source>Wi-Fi</source>
        <translation type="unfinished">Wi-Fi</translation>
    </message>
</context>
<context>
    <name>DelegateWizardCheck</name>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="74"/>
        <source>Platform range</source>
        <translation type="unfinished">Platform range</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="75"/>
        <source>Platform home</source>
        <translation type="unfinished">Platform home</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="76"/>
        <source>Tank range</source>
        <translation type="unfinished">Tank range</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="77"/>
        <source>Tank home</source>
        <translation type="unfinished">Tank home</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="78"/>
        <source>Display test</source>
        <translation type="unfinished">Display test</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="79"/>
        <source>Printer calibration</source>
        <translation type="unfinished">Printer calibration</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="80"/>
        <source>Sound test</source>
        <translation type="unfinished">Sound test</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="81"/>
        <source>UV LED</source>
        <translation type="unfinished">UV LED</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="82"/>
        <source>UV LED and fans</source>
        <translation type="unfinished">UV LED and fans</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="83"/>
        <source>Release foam</source>
        <translation type="unfinished">Release foam</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="84"/>
        <source>Make tank accessible</source>
        <translation type="unfinished">Make tank accessible</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="85"/>
        <source>Resin sensor</source>
        <translation type="unfinished">Resin sensor</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="86"/>
        <source>Serial number</source>
        <translation type="unfinished">Serial number</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="87"/>
        <source>Temperature</source>
        <translation type="unfinished">Temperature</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="88"/>
        <source>Tank calib. start</source>
        <translation type="unfinished">Tank calib. start</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="89"/>
        <location filename="../qml/DelegateWizardCheck.qml" line="120"/>
        <source>Tank level</source>
        <translation type="unfinished">Tank level</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="90"/>
        <source>Platform calibration</source>
        <translation type="unfinished">Platform calibration</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="91"/>
        <source>Tilt timming</source>
        <translation type="unfinished">Tilt timing</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="92"/>
        <source>Obtain system info</source>
        <translation type="unfinished">Obtain system info</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="93"/>
        <source>Obtain calibration info</source>
        <translation type="unfinished">Obtain calibration info</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="94"/>
        <source>Erase projects</source>
        <translation type="unfinished">Erase projects</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="95"/>
        <source>Reset hostname</source>
        <translation type="unfinished">Reset hostname</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="96"/>
        <source>Reset API key</source>
        <translation type="unfinished">Reset API key</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="97"/>
        <source>Reset remote config</source>
        <translation type="unfinished">Reset remote config</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="98"/>
        <source>Reset HTTP digest</source>
        <translation type="unfinished">Reset HTTP digest</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="99"/>
        <source>Reset Wi-Fi settings</source>
        <translation type="unfinished">Reset Wi-Fi settings</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="100"/>
        <source>Reset timezone</source>
        <translation type="unfinished">Reset timezone</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="101"/>
        <source>Reset NTP state</source>
        <translation type="unfinished">Reset NTP state</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="102"/>
        <source>Reset system locale</source>
        <translation type="unfinished">Reset system locale</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="103"/>
        <source>Clear UV calibration data</source>
        <translation type="unfinished">Clear UV calibration data</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="104"/>
        <source>Clear downloaded Slicer profiles</source>
        <translation type="unfinished">Clear downloaded Slicer profiles</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="105"/>
        <source>Reset print configuration</source>
        <translation type="unfinished">Reset print configuration</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="106"/>
        <source>Erase motion controller EEPROM</source>
        <translation type="unfinished">Erase motion controller EEPROM</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="107"/>
        <source>Reset homing profiles</source>
        <translation type="unfinished">Reset homing profiles</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="108"/>
        <source>Send printer data to MQTT</source>
        <translation type="unfinished">Send printer data to MQTT</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="109"/>
        <source>Disable factory mode</source>
        <translation type="unfinished">Disable factory mode</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="110"/>
        <source>Moving printer to accept protective foam</source>
        <translation type="unfinished">Moving printer to accept the protective foam</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="111"/>
        <source>Pressing protective foam</source>
        <translation type="unfinished">Pressing protective foam</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="112"/>
        <source>Disable ssh, serial</source>
        <translation type="unfinished">Disable ssh, serial</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="113"/>
        <source>Check for UV calibrator</source>
        <translation type="unfinished">Check for UV calibrator</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="114"/>
        <source>UV LED warmup</source>
        <translation type="unfinished">UV LED warmup</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="115"/>
        <source>UV calibrator placed</source>
        <translation type="unfinished">UV calibrator placed</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="116"/>
        <source>Calibrate center</source>
        <translation type="unfinished">Calibrate center</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="117"/>
        <source>Calibrate edge</source>
        <translation type="unfinished">Calibrate edge</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="118"/>
        <source>Apply calibration results</source>
        <translation type="unfinished">Apply calibration results</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="119"/>
        <source>Waiting for UV calibrator to be removed</source>
        <translation type="unfinished">Waiting for UV calibrator to be removed</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="121"/>
        <source>Reset UI settings</source>
        <translation type="unfinished">Reset UI settings</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="122"/>
        <source>Erase UV PWM settings</source>
        <translation type="unfinished">Erase UV PWM settings</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="123"/>
        <source>Reset selftest status</source>
        <translation type="unfinished">Reset selftest status</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="124"/>
        <source>Reset printer calibration status</source>
        <translation type="unfinished">Reset printer calibration status</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="125"/>
        <source>Set new printer model</source>
        <translation type="unfinished">Set new printer model</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="126"/>
        <source>Resetting hardware counters</source>
        <translation type="unfinished">Resetting hardware counters</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="127"/>
        <source>Recording changes</source>
        <translation type="unfinished">Recording changes</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="128"/>
        <source>Unknown</source>
        <translation type="unfinished">Unknown</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="129"/>
        <source>Check ID:</source>
        <translation type="unfinished">Check ID:</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="141"/>
        <source>Waiting</source>
        <translation type="unfinished">Waiting</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="142"/>
        <source>Running</source>
        <translation type="unfinished">Running</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="143"/>
        <source>Passed</source>
        <translation type="unfinished">Passed</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="144"/>
        <source>Failure</source>
        <translation type="unfinished">Failure</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="145"/>
        <source>With Warning</source>
        <translation type="unfinished">With Warning</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="146"/>
        <source>User action pending</source>
        <translation type="unfinished">User action pending</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="147"/>
        <source>Canceled</source>
        <translation type="unfinished">Canceled</translation>
    </message>
</context>
<context>
    <name>ErrorPopup</name>
    <message>
        <location filename="../qml/ErrorPopup.qml" line="40"/>
        <source>Unknown error</source>
        <translation type="unfinished">Unknown error</translation>
    </message>
    <message>
        <location filename="../qml/ErrorPopup.qml" line="55"/>
        <source>Understood</source>
        <translation type="unfinished">Understood</translation>
    </message>
</context>
<context>
    <name>ErrorcodesText</name>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="6"/>
        <source>Tilt homing failed, check its surroundings and repeat the action.</source>
        <translation type="unfinished">Tilt homing failed, check its surroundings and repeat the action.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="7"/>
        <source>Tower homing failed, make sure there is no obstacle in its path and repeat the action.</source>
        <translation type="unfinished">Tower homing failed, make sure there is no obstacle in its path and repeat the action.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="8"/>
        <source>Moving the tower failed. Make sure there is no obstacle in its path and repeat the action.</source>
        <translation type="unfinished">Moving the tower failed. Make sure there is no obstacle in its path and repeat the action.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="9"/>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="115"/>
        <source>Incorrect RPM reading of the %(failed_fans_text)s fan. Please check its wiring and connection.</source>
        <translation type="unfinished">Incorrect RPM reading of the %(failed_fans_text)s fan. Please check its wiring and connection.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="10"/>
        <source>Measured resin volume %(volume_ml)d ml is lower than required for this print. Refill the tank and restart the print.</source>
        <translation type="unfinished">Measured resin volume %(volume_ml)d ml is lower than required for this print. Refill the tank and restart the print.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="11"/>
        <source>Measured resin volume %(volume_ml)d ml is higher than required for this print. Make sure that the resin level does not exceed the 100% mark and restart the print.</source>
        <translation type="unfinished">Measured resin volume %(volume_ml)d ml is higher than required for this print. Make sure that the resin level does not exceed the 100% mark and restart the print.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="12"/>
        <source>The printer is not calibrated. Please run the Wizard first.</source>
        <translation type="unfinished">The printer is not calibrated. Please run the Wizard first.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="13"/>
        <source>Failed to reach the tower endstop, check that the tower motor is connected and repeat the action.</source>
        <translation type="unfinished">Failed to reach the tower endstop, check that the tower motor is connected and repeat the action.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="14"/>
        <source>Failed to reach the tilt endstop, check that the cable is connected and repeat the action.</source>
        <translation type="unfinished">Failed to reach the tilt endstop, check that the cable is connected and repeat the action.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="15"/>
        <source>Tower axis check failed!

Current position: %(position_nm)d nm

Check if the ballscrew can move smoothly in its entire range.</source>
        <translation type="unfinished">Tower axis check failed!

Current position: %(position_nm)d nm

Check if the ballscrew can move smoothly in its entire range.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="16"/>
        <source>Tilt axis check failed!

Current position: %(position)d steps

Check if the tilt can move smoothly in its entire range.</source>
        <translation type="unfinished">Tilt axis check failed!

Current position: %(position)d steps

Check if the tilt can move smoothly in its entire range.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="17"/>
        <source>Display test failed, check the connection between the display and the A64 board.</source>
        <translation type="unfinished">Display test failed, check the connection between the display and the A64 board.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="18"/>
        <source>Invalid tilt alignment position. Check the tilt mechanism and repeat the action.</source>
        <translation type="unfinished">Invalid tilt alignment position. Check the tilt mechanism and repeat the action.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="19"/>
        <source>RPM of %(fan)s not in range!

Check if the fan is connected correctly.

RPM data: %(rpm)s
Average: %(avg)s</source>
        <translation type="unfinished">RPM of %(fan)s not in range!

Check if the fan is connected correctly.

RPM data: %(rpm)s
Average: %(avg)s</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="20"/>
        <source>Tower not at the expected position.

Are the platform and tank mounted and secured correctly?</source>
        <translation type="unfinished">Tower not at the expected position.

Are the platform and tank mounted and secured correctly?</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="21"/>
        <source>Measuring the resin failed. Check the presence of the platform and the amount of resin in the tank.</source>
        <translation type="unfinished">Measuring the resin failed. Check the presence of the platform and the amount of resin in the tank.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="22"/>
        <source>The %(sensor)s sensor failed. Check the wiring and connection.</source>
        <translation type="unfinished">The %(sensor)s sensor failed. Check the wiring and connection.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="23"/>
        <source>UV LED is overheating! Check whether the heatsink is installed correctly.</source>
        <translation type="unfinished">UV LED is overheating! Check whether the heatsink is installed correctly.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="24"/>
        <source>A64 temperature is too high. Measured: %(temperature).1f °C! Shutting down in 10 seconds...</source>
        <translation type="unfinished">A64 temperature is too high. Measured: %(temperature).1f °C! Shutting down in 10 seconds...</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="25"/>
        <source>%(sensor)s not in range! Measured temperature: %(temperature).1f °C. Keep the printer out of direct sunlight at room temperature (18 - 32 °C).</source>
        <translation type="unfinished">%(sensor)s not in range! Measured temperature: %(temperature).1f °C. Keep the printer out of direct sunlight at room temperature (18 - 32 °C).</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="26"/>
        <source>Reading of UV LED temperature has failed! This value is essential for the UV LED lifespan and printer safety. Please contact tech support! Current print job will be canceled.</source>
        <translation type="unfinished">Reading of UV LED temperature has failed! This value is essential for the UV LED lifespan and printer safety. Please contact tech support! Current print job will be canceled.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="27"/>
        <source>Wrong revision of the Motion Controller (MC). Contact our support.</source>
        <translation type="unfinished">Wrong revision of the Motion Controller (MC). Contact our support.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="28"/>
        <source>The Motion Controller (MC) has encountered an unexpected error. Restart the printer.</source>
        <translation type="unfinished">The Motion Controller (MC) has encountered an unexpected error. Restart the printer.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="29"/>
        <source>The resin sensor was not triggered. Check whether the tank and the platform are properly secured. Inspect the wiring of the sensor.</source>
        <translation type="unfinished">The resin sensor was not triggered. Check whether the tank and the platform are properly secured. Inspect the wiring of the sensor.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="30"/>
        <source>The printer is not UV calibrated. Connect the UV calibrator and complete the calibration.</source>
        <translation type="unfinished">The printer is not UV calibrated. Connect the UV calibrator and complete the calibration.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="31"/>
        <source>UV LED voltages differ too much. The LED module might be faulty. Contact our support.</source>
        <translation type="unfinished">UV LED voltages differ too much. The LED module might be faulty. Contact our support.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="32"/>
        <source>Speaker test failed, check the connection and repeat the action.</source>
        <translation type="unfinished">Speaker test failed, check the connection and repeat the action.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="33"/>
        <source>The UV LED calibrator is not detected. Check the connection and try again.</source>
        <translation type="unfinished">The UV LED calibrator is not detected. Check the connection and try again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="34"/>
        <source>Cannot connect to the UV LED calibrator. Check the connection and try again.</source>
        <translation type="unfinished">Cannot connect to the UV LED calibrator. Check the connection and try again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="35"/>
        <source>Communication with the UV LED calibrator has failed. Check the connection and try again.</source>
        <translation type="unfinished">Communication with the UV LED calibrator has failed. Check the connection and try again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="36"/>
        <source>The UV LED calibrator detected some light on a dark display. This means there is a light &apos;leak&apos; under the UV calibrator, or your display does not block the UV light enough. Check the UV calibrator placement on the screen or replace the exposure display.</source>
        <translation type="unfinished">The UV LED calibrator detected some light on a dark display. This means there is a light &apos;leak&apos; under the UV calibrator, or your display does not block the UV light enough. Check the UV calibrator placement on the screen or replace the exposure display.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="37"/>
        <source>The UV LED calibrator failed to read expected UV light intensity. Check the UV calibrator placement on the screen.</source>
        <translation type="unfinished">The UV LED calibrator failed to read expected UV light intensity. Check the UV calibrator placement on the screen.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="38"/>
        <source>Unknown UV LED calibrator error code: %(code)d</source>
        <translation type="unfinished">Unknown UV LED calibrator error code: %(code)d</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="39"/>
        <source>Requested intensity cannot be reached by min. allowed PWM.</source>
        <translation type="unfinished">Requested intensity cannot be reached with min. allowed PWM.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="40"/>
        <source>Requested intensity cannot be reached by max. allowed PWM.</source>
        <translation type="unfinished">Requested intensity cannot be reached by max. allowed PWM.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="41"/>
        <source>Correct settings were found, but the standard deviation
(%(found).1f) is greater than the allowed value (%(allowed).1f).
Verify the UV LED calibrator&apos;s position and calibration, then try again.</source>
        <translation type="unfinished">Correct settings were found, but the standard deviation
(%(found).1f) is greater than the allowed value (%(allowed).1f).
Verify the UV LED calibrator&apos;s position and calibration, then try again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="42"/>
        <source>Communication with the Booster board failed. Check the connection and restart the printer.</source>
        <translation type="unfinished">Communication with the Booster board failed. Check the connection and restart the printer.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="43"/>
        <source>The UV LED panel is not detected. Check the connection.</source>
        <translation type="unfinished">The UV LED panel is not detected. Check the connection.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="44"/>
        <source>A part of the LED panel is disconnected. Check the connection and the LED panel.</source>
        <translation type="unfinished">A part of the LED panel is disconnected. Check the connection and the LED panel.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="45"/>
        <source>The printer model was not detected. Check the connection of the exposure display and restart the printer.</source>
        <translation type="unfinished">The printer model was not detected. Check the connection of the exposure display and restart the printer.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="46"/>
        <source>Cannot send factory config to the database (MQTT)! Check the network connection. Please, contact support.</source>
        <translation type="unfinished">Cannot send factory config to the database (MQTT)! Check the network connection. Please, contact support.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="47"/>
        <source>The printer is not connected to the internet. Check the connection in the Settings.</source>
        <translation type="unfinished">The printer is not connected to the internet. Check the connection in the Settings.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="48"/>
        <source>Connection to Prusa servers failed, please try again later.</source>
        <translation type="unfinished">Connection to Prusa servers failed, please try again later.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="49"/>
        <source>The download failed. Check the connection to the internet and try again.</source>
        <translation type="unfinished">The download failed. Check the connection to the internet and try again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="50"/>
        <source>Please turn on the HTTP digest (which is the recommended security option) or update the API key. You can find it in Settings &gt; Network &gt; Login credentials.</source>
        <translation type="unfinished">Please turn on the HTTP digest (which is the recommended security option) or update the API key. You can find it in Settings &gt; Network &gt; Login credentials.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="51"/>
        <source>The printer uses HTTP digest security. Please enable it also in your slicer (recommended), or turn off this security option in the printer. You can find it in Settings &gt; Network &gt; Login credentials.</source>
        <translation type="unfinished">The printer uses HTTP digest security. Please enable it also in your slicer (recommended), or turn off this security option in the printer. You can find it in Settings &gt; Network &gt; Login credentials.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="52"/>
        <source>This request is not compatible with the Prusa remote API. See our documentation for more details.</source>
        <translation type="unfinished">This request is not compatible with the Prusa remote API. See our documentation for more details.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="53"/>
        <source>No problem detected. You can continue using the printer.</source>
        <translation type="unfinished">No problem detected. You can continue using the printer.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="54"/>
        <source>An unexpected error has occurred :-(.
If print job is in progress, it should be finished.
You can turn the printer off by pressing the front power button.
See the handbook to learn how to save a log file and send it to us.</source>
        <translation type="unfinished">An unexpected error has occurred :-(.
If a print job is in progress, it should be finished.
You can turn the printer off by pressing the front power button.
See the handbook to learn how to save a log file and send it to us.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="55"/>
        <source>Image preloader did not finish successfully!</source>
        <translation type="unfinished">Image preloader did not finish successfully!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="56"/>
        <source>Opening the project failed, the file may be corrupted. Re-slice or re-export the project and try again.</source>
        <translation type="unfinished">Opening the project failed, the file may be corrupted. Re-slice or re-export the project and try again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="57"/>
        <source>Failed to read the configuration file. Try to reset the printer. If the problem persists, contact our support.</source>
        <translation type="unfinished">Failed to read the configuration file. Try to reset the printer. If the problem persists, contact our support.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="58"/>
        <source>Another action is already running. Finish this action directly using the printer&apos;s touchscreen.</source>
        <translation type="unfinished">Another action is already running. Finish this action directly using the printer&apos;s touchscreen.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="59"/>
        <source>Internal error (DBUS mapping failed), restart the printer. Contact support if the problem persists.</source>
        <translation type="unfinished">Internal error (DBUS mapping failed), restart the printer. Contact support if the problem persists.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="60"/>
        <source>Error, there is no file to reprint.</source>
        <translation type="unfinished">Error, there is no file to reprint.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="61"/>
        <source>The wizard did not finish successfully!</source>
        <translation type="unfinished">The wizard did not finish successfully!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="62"/>
        <source>The calibration did not finish successfully! Run the calibration again.</source>
        <translation type="unfinished">The calibration did not finish successfully! Run the calibration again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="63"/>
        <source>The automatic UV LED calibration did not finish successfully! Run the calibration again.</source>
        <translation type="unfinished">The automatic UV LED calibration did not finish successfully! Run the calibration again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="64"/>
        <source>UV intensity not set. Please run the UV calibration before starting a print.</source>
        <translation type="unfinished">UV intensity not set. Please run the UV calibration before starting a print.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="65"/>
        <source>Cannot set the update channel. Restart the printer and try again.</source>
        <translation type="unfinished">Cannot set the update channel. Restart the printer and try again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="66"/>
        <source>Cannot get the update channel. Restart the printer and try again.</source>
        <translation type="unfinished">Cannot get the update channel. Restart the printer and try again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="67"/>
        <source>The print job cancelled by the user.</source>
        <translation type="unfinished">The print job cancelled by the user.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="68"/>
        <source>Internal memory is full. Delete some of your projects first.</source>
        <translation type="unfinished">Internal memory is full. Delete some of your projects first.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="69"/>
        <source>The admin menu is not available.</source>
        <translation type="unfinished">The admin menu is not available.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="70"/>
        <source>Cannot find the selected file!</source>
        <translation type="unfinished">Cannot find the selected file!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="71"/>
        <source>File has an invalid extension! See the article for supported file extensions.</source>
        <translation type="unfinished">File has an invalid extension! See the article for supported file extensions.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="72"/>
        <source>File already exists! Delete it in the printer first and try again.</source>
        <translation type="unfinished">File already exists! Delete it in the printer first and try again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="73"/>
        <source>The project file is invalid!</source>
        <translation type="unfinished">The project file is invalid!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="74"/>
        <source>This Wizard cannot be canceled, finish the steps first.</source>
        <translation type="unfinished">This Wizard cannot be canceled, finish the steps first.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="75"/>
        <source>Examples (any projects) are missing in the user storage. Redownload them from the &apos;Settings&apos; menu.</source>
        <translation type="unfinished">Examples (any projects) are missing in the user storage. Redownload them from the &apos;Settings&apos; menu.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="76"/>
        <source>Failed to load fans and LEDs factory calibration.</source>
        <translation type="unfinished">Failed to load fans and LEDs factory calibration.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="77"/>
        <source>Failed to serialize Wizard data. Restart the printer and try again.</source>
        <translation type="unfinished">Failed to serialize Wizard data. Restart the printer and try again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="78"/>
        <source>Failed to save Wizard data. Restart the printer and try again.</source>
        <translation type="unfinished">Failed to save Wizard data. Restart the printer and try again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="79"/>
        <source>Serial numbers in wrong format! A64: %(a64)s MC: %(mc)s Please contact tech support!</source>
        <translation type="unfinished">Serial numbers in wrong format! A64: %(a64)s MC: %(mc)s Please contact tech support!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="80"/>
        <source>No USB storage present</source>
        <translation type="unfinished">No USB storage present</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="81"/>
        <source>Failed to change the log level (detail). Restart the printer and try again.</source>
        <translation type="unfinished">Failed to change the log level (detail). Restart the printer and try again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="82"/>
        <source>Saving the new factory default value failed. Restart the printer and try again.</source>
        <translation type="unfinished">Saving the new factory default value failed. Restart the printer and try again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="83"/>
        <source>Error displaying test image.</source>
        <translation type="unfinished">Error displaying test image.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="84"/>
        <source>No calibration data to show!</source>
        <translation type="unfinished">No calibration data to show!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="85"/>
        <source>Data is from unknown UV LED sensor!</source>
        <translation type="unfinished">Data is from unknown UV LED sensor!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="86"/>
        <source>The update of the firmware failed! Restart the printer and try again.</source>
        <translation type="unfinished">The update of the firmware failed! Restart the printer and try again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="87"/>
        <source>No display usage data to show</source>
        <translation type="unfinished">No display usage data to show</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="88"/>
        <source>Failed to set hostname</source>
        <translation type="unfinished">Failed to set hostname</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="89"/>
        <source>Cannot import profile</source>
        <translation type="unfinished">Cannot import profile</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="90"/>
        <source>Cannot export profile</source>
        <translation type="unfinished">Cannot export profile</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="91"/>
        <source>Opening the project failed. The file is possibly corrupted. Please re-slice or re-export the project and try again.</source>
        <translation type="unfinished">Opening the project failed. The file is possibly corrupted. Please re-slice or re-export the project and try again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="92"/>
        <source>The project must have at least one layer</source>
        <translation type="unfinished">The project must have at least one layer</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="93"/>
        <source>Opening the project failed. The file is corrupted. Please re-slice or re-export the project and try again.</source>
        <translation type="unfinished">Opening the project failed. The file is corrupted. Please re-slice or re-export the project and try again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="94"/>
        <source>Analysis of the project failed</source>
        <translation type="unfinished">Analysis of the project failed</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="95"/>
        <source>Calibration project is invalid</source>
        <translation type="unfinished">Calibration project is invalid</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="96"/>
        <source>This project was prepared for a different printer</source>
        <translation type="unfinished">This project was prepared for a different printer</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="97"/>
        <source>Removing this project is not possible. The project is locked by a print job.</source>
        <translation type="unfinished">Removing this project is not possible. The project is locked by a print job.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="98"/>
        <source>The directory is not empty.</source>
        <translation type="unfinished">The directory is not empty.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="99"/>
        <source>The language is not set. Go to Settings -&gt; Language &amp; Time -&gt; Set Language and pick preferred language.</source>
        <translation type="unfinished">The language is not set. Go to Settings -&gt; Language &amp; Time -&gt; Set Language and pick your preferred language.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="100"/>
        <source>Exposure screen that is currently connected has already been used on this printer. This screen was last used for approximately %(counter_h)d hours.

If you do not want to use this screen: turn the printer off, replace the screen and turn the printer back on.</source>
        <translation type="unfinished">The exposure screen that is currently connected has already been used on this printer. This screen was last used for approximately %(counter_h)d hours.

If you do not want to use this screen: turn the printer off, replace the screen and turn the printer back on.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="101"/>
        <source>The printer has booted from an alternative slot due to failed boot attempts using the primary slot.
Update the printer with up-to-date firmware ASAP to recover the primary slot.
This usually happens after a failed update, or due to a hardware failure. Printer settings may have been reset.</source>
        <translation type="unfinished">The printer has booted from an alternative slot due to failed boot attempts using the primary slot.
Update the printer with up-to-date firmware ASAP to recover the primary slot.
This usually happens after a failed update, or due to a hardware failure. Printer settings may have been reset.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="102"/>
        <source>There is no warning</source>
        <translation type="unfinished">There is no warning</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="103"/>
        <source>An unknown warning has occured. Restart the printer and try again. Contact our tech support if the problem persists.</source>
        <translation type="unfinished">An unknown warning has occured. Restart the printer and try again. Contact our tech support if the problem persists.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="104"/>
        <source>The ambient temperature is too high, the print can continue, but it might fail.</source>
        <translation type="unfinished">The ambient temperature is too high, the print can continue, but it might fail.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="105"/>
        <source>The ambient temperature is too low, the print can continue, but it might fail.</source>
        <translation type="unfinished">The ambient temperature is too low, the print can continue, but it might fail.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="106"/>
        <source>The internal memory is full, project cannot be copied. You can continue printing. However, you must not remove the USB drive during the print, otherwise the process will fail.</source>
        <translation type="unfinished">The internal memory is full, project cannot be copied. You can continue printing. However, you must not remove the USB drive during the print, otherwise the process will fail.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="107"/>
        <source>The model was sliced for a different printer model. Reslice the model using the correct settings.</source>
        <translation type="unfinished">The model was sliced for a different printer model. Reslice the model using the correct settings.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="108"/>
        <source>The amount of resin in the tank is not enough for the current project. Adding more resin will be required during the print.</source>
        <translation type="unfinished">The amount of resin in the tank is not enough for the current project. Adding more resin will be required during the print.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="109"/>
        <source>The print parameters are out of range of the printer, the system can try to fix the project. Proceed?</source>
        <translation type="unfinished">The print parameters are out of range of the printer, the system can try to fix the project. Proceed?</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="110"/>
        <source>Per-partes print not available.</source>
        <translation type="unfinished">Per-partes print not available.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="111"/>
        <source>Print mask is missing.</source>
        <translation type="unfinished">Print mask is missing.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="112"/>
        <source>Object was cropped because it does not fit the print area.</source>
        <translation type="unfinished">Object was cropped because it does not fit the print area.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="113"/>
        <source>The model was sliced for a different printer variant %(project_variant)s. Your printer variant is %(printer_variant)s.</source>
        <translation type="unfinished">The model was sliced for a different printer variant %(project_variant)s. Your printer variant is %(printer_variant)s.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="114"/>
        <source>Measured resin volume is too low. The print can continue, however, a refill might be required.</source>
        <translation type="unfinished">Measured resin volume is too low. The print can continue, however, a refill might be required.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="116"/>
        <source>Incorrect RPM reading of the %(failed_fans_text)s fan. Please check its wiring. The print may continue, however, there&apos;s a risk of overheating.</source>
        <translation type="unfinished">Incorrect RPM reading of the %(failed_fans_text)s fan. Please check its wiring. The print may continue, however, there&apos;s a risk of overheating.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="119"/>
        <source>TILT HOMING FAILED</source>
        <translation type="unfinished">TILT HOMING FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="120"/>
        <source>TOWER HOMING FAILED</source>
        <translation type="unfinished">TOWER HOMING FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="121"/>
        <source>TOWER MOVING FAILED</source>
        <translation type="unfinished">TOWER MOVING FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="122"/>
        <source>FAN FAILURE</source>
        <translation type="unfinished">FAN FAILURE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="123"/>
        <source>RESIN TOO LOW</source>
        <translation type="unfinished">RESIN LEVEL TOO LOW</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="124"/>
        <source>RESIN TOO HIGH</source>
        <translation type="unfinished">RESIN LEVEL TOO HIGH</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="125"/>
        <source>CALIBRATION ERROR</source>
        <translation type="unfinished">CALIBRATION ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="126"/>
        <source>TOWER ENDSTOP NOT REACHED</source>
        <translation type="unfinished">TOWER ENDSTOP NOT REACHED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="127"/>
        <source>TILT ENDSTOP NOT REACHED</source>
        <translation type="unfinished">TILT ENDSTOP NOT REACHED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="128"/>
        <source>TOWER AXIS CHECK FAILED</source>
        <translation type="unfinished">TOWER AXIS CHECK FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="129"/>
        <source>TILT AXIS CHECK FAILED</source>
        <translation type="unfinished">TILT AXIS CHECK FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="130"/>
        <source>DISPLAY TEST FAILED</source>
        <translation type="unfinished">DISPLAY TEST FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="131"/>
        <source>INVALID TILT ALIGN POSITION</source>
        <translation type="unfinished">INVALID TILT ALIGN POSITION</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="132"/>
        <source>FAN RPM OUT OF TEST RANGE</source>
        <translation type="unfinished">FAN RPM OUT OF TEST RANGE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="133"/>
        <source>TOWER POSITION ERROR</source>
        <translation type="unfinished">TOWER POSITION ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="134"/>
        <source>RESIN MEASURING FAILED</source>
        <translation type="unfinished">RESIN MEASURING FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="135"/>
        <source>TEMPERATURE SENSOR FAILED</source>
        <translation type="unfinished">TEMPERATURE SENSOR FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="136"/>
        <source>UVLED HEAT SINK FAILED</source>
        <translation type="unfinished">UVLED HEAT SINK FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="137"/>
        <source>A64 OVERHEAT</source>
        <translation type="unfinished">A64 OVERHEAT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="138"/>
        <source>TEMPERATURE OUT OF RANGE</source>
        <translation type="unfinished">TEMPERATURE OUT OF RANGE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="139"/>
        <source>UV LED TEMP. ERROR</source>
        <translation type="unfinished">UV LED TEMP. ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="140"/>
        <source>MC WRONG REVISION</source>
        <translation type="unfinished">MC WRONG REVISION</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="141"/>
        <source>UNEXPECTED MC ERROR</source>
        <translation type="unfinished">UNEXPECTED MC ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="142"/>
        <source>RESIN SENSOR ERROR</source>
        <translation type="unfinished">RESIN SENSOR ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="143"/>
        <source>PRINTER NOT UV CALIBRATED</source>
        <translation type="unfinished">PRINTER NOT UV CALIBRATED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="144"/>
        <source>UVLED VOLTAGE ERROR</source>
        <translation type="unfinished">UVLED VOLTAGE ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="145"/>
        <source>SPEAKER TEST FAILED</source>
        <translation type="unfinished">SPEAKER TEST FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="146"/>
        <source>UV LED CALIBRATOR NOT DETECTED</source>
        <translation type="unfinished">UV LED CALIBRATOR NOT DETECTED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="147"/>
        <source>UV LED CALIBRATOR CONNECTION ERROR</source>
        <translation type="unfinished">UV LED CALIBRATOR CONNECTION ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="148"/>
        <source>UV LED CALIBRATOR LINK ERROR</source>
        <translation type="unfinished">UV LED CALIBRATOR LINK ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="149"/>
        <source>UV LED CALIBRATOR ERROR</source>
        <translation type="unfinished">UV LED CALIBRATOR ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="150"/>
        <source>UV LED CALIBRATOR READINGS ERROR</source>
        <translation type="unfinished">UV LED CALIBRATOR READINGS ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="151"/>
        <source>UV LED CALIBRATOR UNKNONW ERROR</source>
        <translation type="unfinished">UV LED CALIBRATOR UNKNOWN ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="152"/>
        <source>UV INTENSITY TOO HIGH</source>
        <translation type="unfinished">UV INTENSITY TOO HIGH</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="153"/>
        <source>UV INTENSITY TOO LOW</source>
        <translation type="unfinished">UV INTENSITY TOO LOW</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="154"/>
        <source>UV CALIBRATION ERROR</source>
        <translation type="unfinished">UV CALIBRATION ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="155"/>
        <source>BOOSTER BOARD PROBLEM</source>
        <translation type="unfinished">BOOSTER BOARD PROBLEM</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="156"/>
        <source>Disconnected UV LED panel</source>
        <translation type="unfinished">Disconnected UV LED panel</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="157"/>
        <source>Broken UV LED panel</source>
        <translation type="unfinished">Broken UV LED panel</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="158"/>
        <source>Unknown printer model</source>
        <translation type="unfinished">Unknown printer model</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="159"/>
        <source>MQTT UPLOAD FAILED</source>
        <translation type="unfinished">MQTT UPLOAD FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="160"/>
        <source>NO INTERNET CONNECTION</source>
        <translation type="unfinished">NO INTERNET CONNECTION</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="161"/>
        <source>CONNECTION FAILED</source>
        <translation type="unfinished">CONNECTION FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="162"/>
        <source>DOWNLOAD FAILED</source>
        <translation type="unfinished">DOWNLOAD FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="163"/>
        <source>INVALID API KEY</source>
        <translation type="unfinished">INVALID API KEY</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="164"/>
        <source>UNAUTHORIZED</source>
        <translation type="unfinished">UNAUTHORIZED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="165"/>
        <source>REMOTE API ERROR</source>
        <translation type="unfinished">REMOTE API ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="166"/>
        <source>PRINTER IS OK</source>
        <translation type="unfinished">PRINTER IS OK</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="167"/>
        <source>UNEXPECTED ERROR</source>
        <translation type="unfinished">UNEXPECTED ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="168"/>
        <source>PRELOAD FAILED</source>
        <translation type="unfinished">PRELOAD FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="169"/>
        <source>OPENING PROJECT FAILED</source>
        <translation type="unfinished">OPENING PROJECT FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="170"/>
        <source>CONFIG FILE READ ERROR</source>
        <translation type="unfinished">CONFIG FILE READ ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="171"/>
        <source>PRINTER IS BUSY</source>
        <translation type="unfinished">PRINTER IS BUSY</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="172"/>
        <source>INTERNAL ERROR</source>
        <translation type="unfinished">INTERNAL ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="173"/>
        <source>NO FILE TO REPRINT</source>
        <translation type="unfinished">NO FILE TO REPRINT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="174"/>
        <source>WIZARD FAILED</source>
        <translation type="unfinished">WIZARD FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="175"/>
        <source>CALIBRATION FAILED</source>
        <translation type="unfinished">CALIBRATION FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="176"/>
        <source>UV CALIBRATION FAILED</source>
        <translation type="unfinished">UV CALIBRATION FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="177"/>
        <source>UV INTENSITY ERROR</source>
        <translation type="unfinished">UV INTENSITY ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="178"/>
        <source>SETTING UPDATE CHANNEL FAILED</source>
        <translation type="unfinished">SETTING UPDATE CHANNEL FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="179"/>
        <source>UPDATE CHANNEL FAILED</source>
        <translation type="unfinished">UPDATE CHANNEL FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="180"/>
        <source>PRINT JOB CANCELLED</source>
        <translation type="unfinished">PRINT JOB CANCELLED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="181"/>
        <source>INTERNAL MEMORY FULL</source>
        <translation type="unfinished">INTERNAL MEMORY FULL</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="182"/>
        <source>ADMIN NOT AVAILABLE</source>
        <translation type="unfinished">ADMIN NOT AVAILABLE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="183"/>
        <source>FILE NOT FOUND</source>
        <translation type="unfinished">FILE NOT FOUND</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="184"/>
        <source>INVALID FILE EXTENSION</source>
        <translation type="unfinished">INVALID FILE EXTENSION</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="185"/>
        <source>FILE ALREADY EXISTS</source>
        <translation type="unfinished">FILE ALREADY EXISTS</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="186"/>
        <source>INVALID PROJECT</source>
        <translation type="unfinished">INVALID PROJECT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="187"/>
        <source>YOU SHALL NOT PASS</source>
        <translation type="unfinished">YOU SHALL NOT PASS</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="188"/>
        <source>PRINT EXAMPLES MISSING</source>
        <translation type="unfinished">PRINT EXAMPLES MISSING</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="189"/>
        <source>CALIBRATION LOAD FAILED</source>
        <translation type="unfinished">CALIBRATION LOAD FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="190"/>
        <source>DATA PREPARATION FAILURE</source>
        <translation type="unfinished">DATA PREPARATION FAILURE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="191"/>
        <source>WIZARD DATA FAILURE</source>
        <translation type="unfinished">WIZARD DATA FAILURE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="192"/>
        <source>SERIAL NUMBER ERROR</source>
        <translation type="unfinished">SERIAL NUMBER ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="193"/>
        <source>USB DRIVE NOT DETECTED</source>
        <translation type="unfinished">USB DRIVE NOT DETECTED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="194"/>
        <source>SETTING LOG DETAIL FAILED</source>
        <translation type="unfinished">SETTING LOG DETAIL FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="195"/>
        <source>DATA OVERWRITE FAILED</source>
        <translation type="unfinished">DATA OVERWRITE FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="196"/>
        <source>DISPLAY TEST ERROR</source>
        <translation type="unfinished">DISPLAY TEST ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="197"/>
        <source>NO UV CALIBRATION DATA</source>
        <translation type="unfinished">NO UV CALIBRATION DATA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="198"/>
        <source>UV DATA EROR</source>
        <translation type="unfinished">UV DATA ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="199"/>
        <source>FIRMWARE UPDATE FAILED</source>
        <translation type="unfinished">FIRMWARE UPDATE FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="200"/>
        <source>Display usage error</source>
        <translation type="unfinished">Display usage error</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="201"/>
        <source>HOSTNAME ERROR</source>
        <translation type="unfinished">HOSTNAME ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="202"/>
        <source>PROFILE IMPORT ERROR</source>
        <translation type="unfinished">PROFILE IMPORT ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="203"/>
        <source>PROFILE EXPORT ERROR</source>
        <translation type="unfinished">PROFILE EXPORT ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="204"/>
        <source>CANNOT READ PROJECT</source>
        <translation type="unfinished">CANNOT READ PROJECT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="205"/>
        <source>NOT ENOUGHT LAYERS</source>
        <translation type="unfinished">NOT ENOUGH LAYERS</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="206"/>
        <source>PROJECT IS CORRUPTED</source>
        <translation type="unfinished">PROJECT IS CORRUPTED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="207"/>
        <source>PROJECT ANALYSIS FAILED</source>
        <translation type="unfinished">PROJECT ANALYSIS FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="208"/>
        <source>CALIBRATION PROJECT IS INVALID</source>
        <translation type="unfinished">CALIBRATION PROJECT IS INVALID</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="209"/>
        <source>WRONG PRINTER MODEL</source>
        <translation type="unfinished">WRONG PRINTER MODEL</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="210"/>
        <source>CANNOT REMOVE PROJECT</source>
        <translation type="unfinished">CANNOT REMOVE PROJECT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="211"/>
        <source>DIRECTORY NOT EMPTY</source>
        <translation type="unfinished">DIRECTORY NOT EMPTY</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="212"/>
        <source>LANGUAGE NOT SET</source>
        <translation type="unfinished">LANGUAGE NOT SET</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="213"/>
        <source>OLD EXPO PANEL</source>
        <translation type="unfinished">OLD EXPO PANEL</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="214"/>
        <source>BOOTED SLOT CHANGED</source>
        <translation type="unfinished">BOOTED SLOT CHANGED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="215"/>
        <source>NO WARNING</source>
        <translation type="unfinished">NO WARNING</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="216"/>
        <source>UNKNOWN WARNING</source>
        <translation type="unfinished">UNKNOWN WARNING</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="217"/>
        <source>AMBIENT TEMP. TOO HIGH</source>
        <translation type="unfinished">AMBIENT TEMP. TOO HIGH</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="218"/>
        <source>AMBIENT TEMP. TOO LOW</source>
        <translation type="unfinished">AMBIENT TEMP. TOO LOW</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="219"/>
        <source>CAN&apos;T COPY PROJECT</source>
        <translation type="unfinished">CAN&apos;T COPY PROJECT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="220"/>
        <source>INCORRECT PRINTER MODEL</source>
        <translation type="unfinished">INCORRECT PRINTER MODEL</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="221"/>
        <source>NOT ENOUGH RESIN</source>
        <translation type="unfinished">NOT ENOUGH RESIN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="222"/>
        <source>PARAMETERS OUT OF RANGE</source>
        <translation type="unfinished">PARAMETERS OUT OF RANGE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="223"/>
        <source>PERPARTES NOAVAIL WARNING</source>
        <translation type="unfinished">PERPARTES NOAVAIL WARNING</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="224"/>
        <source>MASK NOAVAIL WARNING</source>
        <translation type="unfinished">MASK NOAVAIL WARNING</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="225"/>
        <source>OBJECT CROPPED WARNING</source>
        <translation type="unfinished">OBJECT CROPPED WARNING</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="226"/>
        <source>PRINTER VARIANT MISMATCH WARNING</source>
        <translation type="unfinished">PRINTER VARIANT MISMATCH WARNING</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="227"/>
        <source>RESIN LOW</source>
        <translation type="unfinished">RESIN LOW</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="228"/>
        <source>FAN WARNING</source>
        <translation type="unfinished">FAN WARNING</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="229"/>
        <source>EXPECT OVERHEATING</source>
        <translation type="unfinished">EXPECT OVERHEATING</translation>
    </message>
</context>
<context>
    <name>NotificationProgressDelegate</name>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="89"/>
        <source>Error: %1</source>
        <translation type="unfinished">Error: %1</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="99"/>
        <source>Storage full</source>
        <translation type="unfinished">Storage full</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="101"/>
        <source>File not found</source>
        <translation type="unfinished">File not found</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="103"/>
        <source>Already exists</source>
        <translation type="unfinished">Already exists</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="105"/>
        <source>Invalid extension</source>
        <translation type="unfinished">Invalid extension</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="107"/>
        <source>Can&apos;t read</source>
        <translation type="unfinished">Can&apos;t read</translation>
    </message>
</context>
<context>
    <name>PageAPSettings</name>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="34"/>
        <source>AP Settings</source>
        <translation type="unfinished">AP Settings</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="51"/>
        <source>(unchanged)</source>
        <translation type="unfinished">(unchanged)</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="68"/>
        <source>Stop AP</source>
        <translation type="unfinished">Stop AP</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="136"/>
        <source>State:</source>
        <translation type="unfinished">State:</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="147"/>
        <source>Inactive</source>
        <translation type="unfinished">Inactive</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="151"/>
        <source>SSID</source>
        <translation type="unfinished">SSID</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="173"/>
        <source>Password</source>
        <translation type="unfinished">Password</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="214"/>
        <source>Show Password</source>
        <translation type="unfinished">Show Password</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="230"/>
        <source>Security</source>
        <translation type="unfinished">Security</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="245"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation type="unfinished">PSK must be at least 8 characters long.</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="251"/>
        <source>SSID must not be empty</source>
        <translation type="unfinished">SSID must not be empty</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="263"/>
        <source>Start AP</source>
        <translation type="unfinished">Start AP</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="294"/>
        <source>Swipe for QRCode</source>
        <translation type="unfinished">Swipe for QR Code</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="372"/>
        <source>Swipe for settings</source>
        <translation type="unfinished">Swipe for settings</translation>
    </message>
</context>
<context>
    <name>PageAbout</name>
    <message>
        <location filename="../qml/PageAbout.qml" line="22"/>
        <source>About Us</source>
        <translation type="unfinished">About Us</translation>
    </message>
    <message>
        <location filename="../qml/PageAbout.qml" line="25"/>
        <source>To find out more about us please scan the QR code or use the link below:</source>
        <translation type="unfinished">To find out more about us please scan the QR code or use the link below:</translation>
    </message>
    <message>
        <location filename="../qml/PageAbout.qml" line="29"/>
        <source>Prusa Research a.s.</source>
        <translation type="unfinished">Prusa Research a.s.</translation>
    </message>
</context>
<context>
    <name>PageAddWifiNetwork</name>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="31"/>
        <source>Add Hidden Network</source>
        <translation type="unfinished">Add Hidden Network</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="62"/>
        <source>SSID</source>
        <translation type="unfinished">SSID</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="84"/>
        <source>PASS</source>
        <translation type="unfinished">PASS</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="129"/>
        <source>Show Password</source>
        <translation type="unfinished">Show Password</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="147"/>
        <source>Security</source>
        <translation type="unfinished">Security</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="163"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation type="unfinished">PSK must be at least 8 characters long.</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="169"/>
        <source>SSID must not be empty.</source>
        <translation type="unfinished">SSID must not be empty</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="182"/>
        <source>Connect</source>
        <translation type="unfinished">Connect</translation>
    </message>
</context>
<context>
    <name>PageAdminAPI</name>
    <message>
        <location filename="../qml/PageAdminAPI.qml" line="36"/>
        <source>Admin API</source>
        <translation type="unfinished">Admin API</translation>
    </message>
</context>
<context>
    <name>PageAskForPassword</name>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="66"/>
        <source>Please, enter the correct password for network &quot;%1&quot;</source>
        <translation type="unfinished">Please, enter the correct password for network &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="82"/>
        <source>Password</source>
        <translation type="unfinished">Password</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="92"/>
        <source>(unchanged)</source>
        <translation type="unfinished">(unchanged)</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="128"/>
        <source>Show Password</source>
        <translation type="unfinished">Show Password</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="154"/>
        <source>Cancel</source>
        <translation type="unfinished">Cancel</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="166"/>
        <source>Ok</source>
        <translation type="unfinished">Ok</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="193"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation type="unfinished">PSK must be at least 8 characters long.</translation>
    </message>
</context>
<context>
    <name>PageBasicWizard</name>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="34"/>
        <source>Wizard</source>
        <translation type="unfinished">Wizard</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="55"/>
        <source>Are you sure?</source>
        <translation type="unfinished">Are you sure?</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="56"/>
        <source>Do you really want to cancel the wizard?</source>
        <translation type="unfinished">Do you really want to cancel the wizard?</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="58"/>
        <source>The machine will not work without a completed wizard procedure.</source>
        <translation type="unfinished">The machine will not work without a completed Wizard procedure.</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="106"/>
        <source>Can you see the company logo on the exposure display through the cover?</source>
        <translation type="unfinished">Can you see the company logo on the exposure display through the lid?</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="108"/>
        <source>Tip: If you can&apos;t see the logo clearly, try placing a sheet of paper onto the screen.</source>
        <translation type="unfinished">Tip: If you can&apos;t see the logo clearly, try placing a sheet of paper onto the screen.</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="110"/>
        <source>Once you place the paper inside the printer, do not forget to CLOSE THE COVER!</source>
        <translation type="unfinished">Once you place the paper inside the printer, do not forget to close the cover!</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="120"/>
        <source>Carefully peel off the protective sticker from the exposition display.</source>
        <translation type="unfinished">Carefully peel off the protective sticker from the exposition display.</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="129"/>
        <source>Wizard finished sucessfuly!</source>
        <translation type="unfinished">Wizard finished successfully</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="137"/>
        <source>Wizard failed</source>
        <translation type="unfinished">Wizard failed</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="145"/>
        <source>Wizard canceled</source>
        <translation type="unfinished">Wizard canceled</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="153"/>
        <source>Wizard stopped due to a problem, retry?</source>
        <translation type="unfinished">Wizard stopped due to a problem, retry?</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="162"/>
        <source>Cover</source>
        <translation type="unfinished">Cover</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="163"/>
        <source>Close the cover.</source>
        <translation type="unfinished">Close the cover.</translation>
    </message>
</context>
<context>
    <name>PageCalibrationTilt</name>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="28"/>
        <source>Tank Movement</source>
        <translation type="unfinished">Tank Movement</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="82"/>
        <source>Move Up</source>
        <translation type="unfinished">Move Up</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="91"/>
        <source>Move Down</source>
        <translation type="unfinished">Move Down</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="105"/>
        <source>Tilt position:</source>
        <translation type="unfinished">Tilt position:</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="116"/>
        <source>Done</source>
        <translation type="unfinished">Done</translation>
    </message>
</context>
<context>
    <name>PageCalibrationWizard</name>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="31"/>
        <source>Printer Calibration</source>
        <translation type="unfinished">Printer Calibration</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="49"/>
        <source>If the platform is not yet inserted, insert it according to the picture at 0° degrees angle and secure it with the black knob.</source>
        <translation type="unfinished">If the platform is not yet inserted, insert it according to the picture at a 0° degree angle and secure it with the black knob.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="60"/>
        <source>Loosen the small screw on the cantilever with an allen key. Be careful not to unscrew it completely.</source>
        <translation type="unfinished">Loosen the small screw on the cantilever with an allen key. Be careful not to unscrew it completely.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="62"/>
        <source>Some SL1 printers may have two screws - see the handbook for more information.</source>
        <translation type="unfinished">Some SL1 printers may have two screws - see the handbook for more information.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="75"/>
        <source>Unscrew the tank, rotate it by 90° and place it flat across the tilt bed. Remove the tank screws completely.</source>
        <translation type="unfinished">Unscrew the tank, rotate it by 90° and place it flat across the tilt bed. Remove the tank screws completely.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="84"/>
        <source>In the next step, move the tilt bed up/down until it is in direct contact with the resin tank. The tilt bed and tank have to be aligned in a perfect line.</source>
        <translation type="unfinished">In the next step, move the tilt bed up/down until it is in direct contact with the resin tank. The tilt bed and tank have to be aligned in a perfect line.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="96"/>
        <source>Set the tilt bed against the resin tank</source>
        <translation type="unfinished">Set the tilt bed against the resin tank</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="105"/>
        <source>Make sure that the platfom, tank and tilt bed are PERFECTLY clean.</source>
        <translation type="unfinished">Make sure that the platform, tank and tilt bed are PERFECTLY clean.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="107"/>
        <source>The image is for illustration purposes only.</source>
        <translation type="unfinished">The image is for illustration purposes only.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="116"/>
        <source>Return the tank to the original position and secure it with tank screws. Make sure that you tighten both screws evenly and with the same amount of force.</source>
        <translation type="unfinished">Return the tank to the original position and secure it with tank screws. Make sure that you tighten both screws evenly and with the same amount of force.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="125"/>
        <source>Check whether the platform is properly secured with the black knob(hold it in place and tighten the knob if needed).</source>
        <translation type="unfinished">Check whether the platform is properly secured with the black knob(hold it in place and tighten the knob if needed).</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="127"/>
        <source>Do not rotate the platform. It should be positioned according to the picture.</source>
        <translation type="unfinished">Do not rotate the platform. It should be positioned according to the picture.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="141"/>
        <source>Close the cover.</source>
        <translation type="unfinished">Close the cover.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="155"/>
        <source>Adjust the platform so it is aligned with the exposition display.</source>
        <translation type="unfinished">Adjust the platform so it is aligned with the exposition display.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="157"/>
        <source>Front edges of the platform and exposition display need to be parallel.</source>
        <translation type="unfinished">Front edges of the platform and exposition display need to be parallel.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="170"/>
        <source>Hold the platform still with one hand and apply a slight downward force on it, so it maintains good contact with the screen.


Next, use an Allen key to tighten the screw on the cantilever.

Then release the platform.</source>
        <translation type="unfinished">Hold the platform still with one hand and apply a slight downward force on it, so it maintains good contact with the screen.


Next, use an Allen key to tighten the screw on the cantilever.

Then release the platform.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="173"/>
        <source>Tighten the small screw on the cantilever with an allen key.</source>
        <translation type="unfinished">Tighten the small screw on the cantilever with an allen key.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="175"/>
        <source>Some SL1 printers may have two screws - tighten them evenly, little by little. See the handbook for more information.</source>
        <translation type="unfinished">Some SL1 printers may have two screws - tighten them evenly, little by little. See the handbook for more information.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="190"/>
        <source>Tilt settings for Prusa Slicer:</source>
        <translation type="unfinished">Tilt settings for PrusaSlicer:</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="192"/>
        <source>Tilt time fast: %1 s</source>
        <translation type="unfinished">Tilt time fast: %1 s</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="194"/>
        <source>Tilt time slow: %1 s</source>
        <translation type="unfinished">Tilt time slow: %1 s</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="196"/>
        <source>Area fill: %1 %</source>
        <translation type="unfinished">Area fill: %1 %</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="198"/>
        <source>All done, happy printing!</source>
        <translation type="unfinished">All done, happy printing!</translation>
    </message>
</context>
<context>
    <name>PageChange</name>
    <message>
        <location filename="../qml/PageChange.qml" line="36"/>
        <source>Print Settings</source>
        <translation type="unfinished">Print Settings</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="64"/>
        <source>Exposure</source>
        <translation type="unfinished">Exposure</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="81"/>
        <location filename="../qml/PageChange.qml" line="107"/>
        <location filename="../qml/PageChange.qml" line="129"/>
        <source>second</source>
        <translation type="unfinished">second</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="87"/>
        <source>Exposure Time Incr.</source>
        <translation type="unfinished">Exposure time incr.</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="112"/>
        <source>First Layer Expo.</source>
        <translation type="unfinished">First Layer Expo.</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="134"/>
        <source>Print Profile</source>
        <translation type="unfinished">Print Profile</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="145"/>
        <source>Faster</source>
        <comment>Default print profile. Printer behaves as before.</comment>
        <extracomment>Default print profile. Printer behaves as before.</extracomment>
        <translation type="unfinished">Faster</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="147"/>
        <source>Slower</source>
        <comment>Safe print profile with slow tilts and pause before each exposure.</comment>
        <extracomment>Safe print profile with slow tilts and pause before each exposure.</extracomment>
        <translation type="unfinished">Slower</translation>
    </message>
</context>
<context>
    <name>PageConfirm</name>
    <message>
        <location filename="../qml/PageConfirm.qml" line="120"/>
        <source>Swipe for a picture</source>
        <translation type="unfinished">Swipe for a picture</translation>
    </message>
    <message>
        <location filename="../qml/PageConfirm.qml" line="187"/>
        <source>Continue</source>
        <translation type="unfinished">Continue</translation>
    </message>
</context>
<context>
    <name>PageConnectHiddenNetwork</name>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="34"/>
        <source>Hidden Network</source>
        <translation type="unfinished">Hidden Network</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="62"/>
        <source>SSID</source>
        <translation type="unfinished">SSID</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="78"/>
        <source>Password</source>
        <translation type="unfinished">Password</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="121"/>
        <source>Show Password</source>
        <translation type="unfinished">Show Password</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="128"/>
        <source>Security</source>
        <translation type="unfinished">Security</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="145"/>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="153"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation type="unfinished">PSK must be at least 8 characters long.</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="191"/>
        <source>Working...</source>
        <translation type="unfinished">Working...</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="202"/>
        <source>Connection to %1 failed.</source>
        <translation type="unfinished">Connection to %1 failed.</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="213"/>
        <source>Connected to %1</source>
        <translation type="unfinished">Connected to %1</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="241"/>
        <source>Start AP</source>
        <translation type="unfinished">Start AP</translation>
    </message>
</context>
<context>
    <name>PageContinue</name>
    <message>
        <location filename="../qml/PageContinue.qml" line="27"/>
        <location filename="../qml/PageContinue.qml" line="59"/>
        <source>Continue</source>
        <translation type="unfinished">Continue</translation>
    </message>
</context>
<context>
    <name>PageCoolingDown</name>
    <message>
        <location filename="../qml/PageCoolingDown.qml" line="27"/>
        <location filename="../qml/PageCoolingDown.qml" line="37"/>
        <source>UV LED OVERHEAT!</source>
        <translation type="unfinished">UV LED OVERHEAT!</translation>
    </message>
    <message>
        <location filename="../qml/PageCoolingDown.qml" line="40"/>
        <source>Cooling down</source>
        <translation type="unfinished">Cooling down</translation>
    </message>
    <message>
        <location filename="../qml/PageCoolingDown.qml" line="40"/>
        <source>Temperature is %1 C</source>
        <translation type="unfinished">Temperature is %1 C</translation>
    </message>
</context>
<context>
    <name>PageDisplayTest</name>
    <message>
        <location filename="../qml/PageDisplayTest.qml" line="31"/>
        <source>Display Test</source>
        <translation type="unfinished">Display Test</translation>
    </message>
</context>
<context>
    <name>PageDisplaytestWizard</name>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="31"/>
        <source>Display Test</source>
        <translation type="unfinished">Display Test</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="47"/>
        <source>Welcome to the display wizard.</source>
        <translation type="unfinished">Welcome to the display wizard.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="49"/>
        <source>This procedure will help you make sure that your exposure display is working correctly.</source>
        <translation type="unfinished">This procedure will help you make sure that your exposure display is working correctly.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="57"/>
        <source>Please unscrew and remove the resin tank.</source>
        <translation type="unfinished">Please unscrew and remove the resin tank.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="66"/>
        <source>Loosen the black knob and remove the platform.</source>
        <translation type="unfinished">Loosen the black knob and remove the platform.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="75"/>
        <source>Close the cover.</source>
        <translation type="unfinished">Close the cover.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="88"/>
        <source>All done, happy printing!</source>
        <translation type="unfinished">All done, happy printing!</translation>
    </message>
</context>
<context>
    <name>PageDowngradeWizard</name>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="31"/>
        <source>Hardware Downgrade</source>
        <translation type="unfinished">Hardware Downgrade</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="45"/>
        <source>SL1 components detected (downgrade from SL1S).</source>
        <translation type="unfinished">SL1 components detected (downgrade from SL1S).</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="46"/>
        <source>To complete the downgrade procedure, printer needs to clear the configuration and reboot.</source>
        <translation type="unfinished">To complete the downgrade procedure, the printer needs to clear the configuration and reboot.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="47"/>
        <source>Proceed?</source>
        <translation type="unfinished">Proceed?</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="56"/>
        <source>The printer will power off now.</source>
        <translation type="unfinished">The printer will power off now.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="57"/>
        <source>Reassemble SL1S components and power on the printer. This will restore the original state.</source>
        <translation type="unfinished">Reassemble SL1S components and power on the printer. This will restore the original state.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="66"/>
        <source>Please note that downgrading is not supported. 

Downgrading your printer will erase your UV calibration and your printer will not work properly. 

You will need to recalibrate it using an external UV calibrator.</source>
        <translation type="unfinished">Please note that downgrading is not supported. 

Downgrading your printer will erase your UV calibration and your printer will not work properly. 

You will need to recalibrate it using an external UV calibrator.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="74"/>
        <source>Current configuration is going to be cleared now.</source>
        <translation type="unfinished">The current configuration is going to be cleared now.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="75"/>
        <source>The printer will ask for the inital setup after reboot.</source>
        <translation type="unfinished">The printer will ask for the inital setup after reboot.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="84"/>
        <source>Use only the metal resin tank supplied. Using the different resin tank may cause resin to spill and damage your printer!</source>
        <translation type="unfinished">Use only the metal resin tank supplied. Using the different resin tanks may cause the resin to spill and damage your printer!</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="93"/>
        <source>Only use the platform supplied. Using a different platform may cause resin to spill and damage your printer!</source>
        <translation type="unfinished">Only use the platform supplied. Using a different platform may cause the resin to spill and damage your printer!</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="101"/>
        <source>Downgrade done. In the next step, the printer will be restarted.</source>
        <translation type="unfinished">Downgrade done. In the next step, the printer will be restarted.</translation>
    </message>
</context>
<context>
    <name>PageDownloadingExamples</name>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="10"/>
        <source>Examples</source>
        <translation type="unfinished">Examples</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="54"/>
        <source>Downloading examples...</source>
        <translation type="unfinished">Downloading examples...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="122"/>
        <source>Initializing...</source>
        <translation type="unfinished">Initializing...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="123"/>
        <source>Downloading ...</source>
        <translation type="unfinished">Downloading ...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="124"/>
        <source>Copying...</source>
        <translation type="unfinished">Copying...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="125"/>
        <source>Unpacking...</source>
        <translation type="unfinished">Unpacking...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="126"/>
        <source>Done.</source>
        <translation type="unfinished">Done.</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="127"/>
        <source>Cleanup...</source>
        <translation type="unfinished">Cleanup...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="128"/>
        <source>Failure.</source>
        <translation type="unfinished">Failure.</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="131"/>
        <source>Unknown state.</source>
        <translation type="unfinished">Unknown state.</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="139"/>
        <source>View Examples</source>
        <translation type="unfinished">View Examples</translation>
    </message>
</context>
<context>
    <name>PageError</name>
    <message>
        <location filename="../qml/PageError.qml" line="33"/>
        <source>Error</source>
        <translation type="unfinished">Error</translation>
    </message>
    <message>
        <location filename="../qml/PageError.qml" line="42"/>
        <source>Error code:</source>
        <translation type="unfinished">Error code:</translation>
    </message>
</context>
<context>
    <name>PageEthernetSettings</name>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="34"/>
        <source>Wired Settings</source>
        <translation type="unfinished">Wired Settings</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="104"/>
        <source>Network Info</source>
        <translation type="unfinished">Network info</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="110"/>
        <source>DHCP</source>
        <translation type="unfinished">DHCP</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="120"/>
        <source>IP Address</source>
        <translation type="unfinished">IP Address</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="133"/>
        <source>Gateway</source>
        <comment>default gateway address</comment>
        <translation type="unfinished">Gateway</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="186"/>
        <source>Apply</source>
        <translation type="unfinished">Apply</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="196"/>
        <source>Configuring the connection,</source>
        <translation type="unfinished">Configuring the connection,</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="196"/>
        <source>please wait...</source>
        <translation type="unfinished">please wait...</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="203"/>
        <source>Revert</source>
        <comment>Turn back the changes and go back to the previous configuration.</comment>
        <translation type="unfinished">Revert</translation>
    </message>
</context>
<context>
    <name>PageException</name>
    <message>
        <location filename="../qml/PageException.qml" line="36"/>
        <source>System Error</source>
        <translation type="unfinished">System Error</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="59"/>
        <source>Error</source>
        <translation type="unfinished">Error</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="66"/>
        <source>Error code:</source>
        <translation type="unfinished">Error code:</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="75"/>
        <source>Swipe to proceed</source>
        <translation type="unfinished">Swipe to proceed</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="100"/>
        <source>Save Logs to USB</source>
        <translation type="unfinished">Save Logs to USB</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="113"/>
        <source>Send Logs to Cloud</source>
        <translation type="unfinished">Send Logs to Cloud</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="127"/>
        <source>Update Firmware</source>
        <translation type="unfinished">Update Firmware</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="134"/>
        <source>Turn Off</source>
        <translation type="unfinished">Turn off</translation>
    </message>
</context>
<context>
    <name>PageFactoryResetWizard</name>
    <message>
        <location filename="../qml/PageFactoryResetWizard.qml" line="31"/>
        <source>Factory Reset</source>
        <translation type="unfinished">Factory Reset</translation>
    </message>
    <message>
        <location filename="../qml/PageFactoryResetWizard.qml" line="43"/>
        <source>Factory Reset done.</source>
        <translation type="unfinished">Factory reset done.</translation>
    </message>
</context>
<context>
    <name>PageFeedme</name>
    <message>
        <location filename="../qml/PageFeedme.qml" line="34"/>
        <source>Feed Me</source>
        <translation type="unfinished">Feed Me</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="42"/>
        <source>Manual resin refill.</source>
        <translation type="unfinished">Manual resin refill.</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="43"/>
        <source>Refill the tank up to the 100% mark and press Done.</source>
        <translation type="unfinished">Refill the tank up to the 100% mark and press Done.</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="44"/>
        <source>If you do not want to refill, press the Back button at top of the screen.</source>
        <translation type="unfinished">If you do not want to refill, press the Back button at top of the screen.</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="74"/>
        <source>Done</source>
        <translation type="unfinished">Done</translation>
    </message>
</context>
<context>
    <name>PageFileBrowser</name>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="33"/>
        <source>Projects</source>
        <translation type="unfinished">Projects</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="43"/>
        <source>Selftest</source>
        <translation type="unfinished">Selftest</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="44"/>
        <source>Printer Calibration</source>
        <translation type="unfinished">Printer Calibration</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="45"/>
        <source>UV Calibration</source>
        <translation type="unfinished">UV calibration</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="94"/>
        <source>Local</source>
        <translation type="unfinished">Local</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="95"/>
        <source>USB</source>
        <translation type="unfinished">USB</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="96"/>
        <source>Remote</source>
        <translation type="unfinished">Remote</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="97"/>
        <source>Previous Prints</source>
        <comment>a directory with previously printed projects</comment>
        <translation type="unfinished">Previous Prints</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="98"/>
        <source>Update Bundles</source>
        <comment>a directory containing firmware update bundles</comment>
        <translation type="unfinished">Update Bundles</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="148"/>
        <source>Install?</source>
        <translation type="unfinished">Install?</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="150"/>
        <source>Do you really want to install %1?</source>
        <translation type="unfinished">Do you really want to install %1?</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="152"/>
        <source>Current system will still be available via Settings -&gt; Firmware -&gt; Downgrade</source>
        <translation type="unfinished">Current system will still be available via Settings -&gt; Firmware -&gt; Downgrade</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="164"/>
        <source>Calibrate?</source>
        <translation type="unfinished">Calibrate?</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="167"/>
        <source>The printer is not fully calibrated.</source>
        <translation type="unfinished">The printer is not fully calibrated.</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="169"/>
        <source>Before printing, the following steps are required to pass:</source>
        <translation type="unfinished">Before printing, the following steps are required to pass:</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="173"/>
        <source>Do you want to start now?</source>
        <translation type="unfinished">Do you want to start now?</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/PageFileBrowser.qml" line="229"/>
        <source>%n item(s)</source>
        <comment>number of items in a directory</comment>
        <translation type="unfinished">
            <numerusform>%n item</numerusform>
            <numerusform>%n items</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="241"/>
        <source>Remote</source>
        <comment>File is stored in remote storage, i.e. a cloud</comment>
        <translation type="unfinished">Remote</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="242"/>
        <location filename="../qml/PageFileBrowser.qml" line="244"/>
        <source>Local</source>
        <comment>File is stored in a local storage</comment>
        <translation type="unfinished">Local</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="243"/>
        <source>USB</source>
        <comment>File is stored on USB flash disk</comment>
        <translation type="unfinished">USB</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="245"/>
        <source>Updates</source>
        <comment>File is in a repository of raucb files(update bundles)</comment>
        <translation type="unfinished">Updates</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="246"/>
        <source>Root</source>
        <comment>Directory is a root of the directory tree, its subdirectories are different sources of projects</comment>
        <translation type="unfinished">Root</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="267"/>
        <source>Unknown</source>
        <translation type="unfinished">Unknown</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="316"/>
        <source>No usable projects were found,</source>
        <translation type="unfinished">No usable projects were found,</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="318"/>
        <source>insert a USB drive or download examples in Settings -&gt; Support.</source>
        <translation type="unfinished">insert a USB drive or download examples in Settings -&gt; Support.</translation>
    </message>
</context>
<context>
    <name>PageFinished</name>
    <message>
        <location filename="../qml/PageFinished.qml" line="33"/>
        <source>Finished</source>
        <translation type="unfinished">Finished</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="109"/>
        <location filename="../qml/PageFinished.qml" line="112"/>
        <source>FINISHED</source>
        <translation type="unfinished">FINISHED</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="110"/>
        <source>FAILED</source>
        <translation type="unfinished">FAILED</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="111"/>
        <source>CANCELED</source>
        <translation type="unfinished">CANCELED</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="136"/>
        <source>Print Time</source>
        <translation type="unfinished">Print Time</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="157"/>
        <source>Layers</source>
        <translation type="unfinished">Layers</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="175"/>
        <source>Consumed Resin</source>
        <translation type="unfinished">Consumed resin</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="193"/>
        <source>Layer height</source>
        <translation type="unfinished">Layer height</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="211"/>
        <source>Layer Exposure</source>
        <translation type="unfinished">Layer Exposure</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="216"/>
        <location filename="../qml/PageFinished.qml" line="234"/>
        <source>s</source>
        <translation type="unfinished">s</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="229"/>
        <source>First Layer Exposure</source>
        <translation type="unfinished">First Layer Exposure</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="249"/>
        <source>Swipe to proceed</source>
        <translation type="unfinished">Swipe to proceed</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="275"/>
        <source>Home</source>
        <translation type="unfinished">Home</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="286"/>
        <source>Reprint</source>
        <translation type="unfinished">Reprint</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="304"/>
        <source>Turn Off</source>
        <translation type="unfinished">Turn off</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="339"/>
        <source>Loading, please wait...</source>
        <translation type="unfinished">Loading, please wait...</translation>
    </message>
</context>
<context>
    <name>PageFullscreenImage</name>
    <message>
        <location filename="../qml/PageFullscreenImage.qml" line="27"/>
        <source>Fullscreen Image</source>
        <translation type="unfinished">Fullscreen Image</translation>
    </message>
</context>
<context>
    <name>PageHome</name>
    <message>
        <location filename="../qml/PageHome.qml" line="29"/>
        <source>Home</source>
        <translation type="unfinished">Home</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="37"/>
        <source>Print</source>
        <translation type="unfinished">Print</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="50"/>
        <source>Control</source>
        <translation type="unfinished">Control</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="57"/>
        <source>Settings</source>
        <translation type="unfinished">Settings</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="64"/>
        <source>Turn Off</source>
        <translation type="unfinished">Turn off</translation>
    </message>
</context>
<context>
    <name>PageLanguage</name>
    <message>
        <location filename="../qml/PageLanguage.qml" line="31"/>
        <source>Set Language</source>
        <translation type="unfinished">Set Language</translation>
    </message>
    <message>
        <location filename="../qml/PageLanguage.qml" line="79"/>
        <source>Set</source>
        <translation type="unfinished">Set</translation>
    </message>
</context>
<context>
    <name>PageLogs</name>
    <message>
        <location filename="../qml/PageLogs.qml" line="29"/>
        <source>Logs export</source>
        <translation type="unfinished">Logs export</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="50"/>
        <source>Log upload finished</source>
        <translation type="unfinished">Log upload finished</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="52"/>
        <source>Logs has been successfully uploaded to the Prusa server.&lt;br /&gt;&lt;br /&gt;Please contact the Prusa support and share the following code with them:</source>
        <translation type="unfinished">Logs has been successfully uploaded to the Prusa server.&lt;br /&gt;&lt;br /&gt;Please contact the Prusa support and share the following code with them:</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="57"/>
        <source>Logs export finished</source>
        <translation type="unfinished">Logs export finished</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="61"/>
        <source>Logs export canceled</source>
        <translation type="unfinished">Logs export canceled</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="64"/>
        <source>Logs export failed.

Please check your flash drive / internet connection.</source>
        <translation type="unfinished">Log export failed. Please check your flash drive / internet connection.</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="88"/>
        <source>Extracting log data</source>
        <translation type="unfinished">Extracting log data</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="91"/>
        <source>Saving data to USB drive</source>
        <translation type="unfinished">Saving data to USB drive</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="92"/>
        <source>Uploading data to server</source>
        <translation type="unfinished">Uploading data to server</translation>
    </message>
</context>
<context>
    <name>PageManual</name>
    <message>
        <location filename="../qml/PageManual.qml" line="25"/>
        <source>Manual</source>
        <translation type="unfinished">Manual</translation>
    </message>
    <message>
        <location filename="../qml/PageManual.qml" line="43"/>
        <source>Scanning the QR code will load the handbook for this device.

Alternatively, use this link:</source>
        <translation type="unfinished">Scanning the QR code will load the handbook for this device.

Alternatively, use this link:</translation>
    </message>
</context>
<context>
    <name>PageMovementControl</name>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="27"/>
        <source>Control</source>
        <translation type="unfinished">Control</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="38"/>
        <source>Home
Platform</source>
        <translation type="unfinished">Home
Platform</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="49"/>
        <source>Home
Tank</source>
        <translation type="unfinished">Home
Tank</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="60"/>
        <source>Disable
Steppers</source>
        <translation type="unfinished">Disable
Steppers</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="69"/>
        <source>Homing the tank, please wait...</source>
        <translation type="unfinished">Homing the tank, please wait...</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="69"/>
        <source>Homing the tower, please wait...</source>
        <translation type="unfinished">Homing the tower, please wait...</translation>
    </message>
</context>
<context>
    <name>PageNetworkEthernetList</name>
    <message>
        <location filename="../qml/PageNetworkEthernetList.qml" line="35"/>
        <source>Network</source>
        <translation type="unfinished">Network</translation>
    </message>
</context>
<context>
    <name>PageNetworkMain</name>
    <message>
        <location filename="../qml/PageNetworkMain.qml" line="28"/>
        <source>Network</source>
        <translation type="unfinished">Network</translation>
    </message>
    <message>
        <location filename="../qml/PageNetworkMain.qml" line="46"/>
        <source>Wi-Fi</source>
        <translation type="unfinished">Wi-Fi</translation>
    </message>
    <message>
        <location filename="../qml/PageNetworkMain.qml" line="55"/>
        <source>Ethernet</source>
        <translation type="unfinished">Ethernet</translation>
    </message>
    <message>
        <location filename="../qml/PageNetworkMain.qml" line="64"/>
        <source>Hotspot</source>
        <translation type="unfinished">Hotspot</translation>
    </message>
    <message>
        <location filename="../qml/PageNetworkMain.qml" line="85"/>
        <source>IP Address</source>
        <translation type="unfinished">IP Address</translation>
    </message>
    <message>
        <location filename="../qml/PageNetworkMain.qml" line="97"/>
        <source>Not connected to network</source>
        <translation type="unfinished">Not connected to network</translation>
    </message>
</context>
<context>
    <name>PageNetworkWifiList</name>
    <message>
        <location filename="../qml/PageNetworkWifiList.qml" line="29"/>
        <source>Network</source>
        <translation type="unfinished">Network</translation>
    </message>
</context>
<context>
    <name>PageNewExpoPanelWizard</name>
    <message>
        <location filename="../qml/PageNewExpoPanelWizard.qml" line="31"/>
        <source>New Exposure Panel</source>
        <translation type="unfinished">New Exposure Panel</translation>
    </message>
    <message>
        <location filename="../qml/PageNewExpoPanelWizard.qml" line="43"/>
        <source>New exposure screen has been detected.

The printer will ask for the inital setup (selftest and calibration) to make sure everything works correctly.</source>
        <translation type="unfinished">A new exposure screen has been detected.

The printer will ask for the initial setup (selftest and calibration) to make sure everything works correctly.</translation>
    </message>
</context>
<context>
    <name>PageNotificationList</name>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="37"/>
        <source>Notifications</source>
        <translation type="unfinished">Notifications</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="61"/>
        <source>Available update to %1</source>
        <translation type="unfinished">Available update to %1</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="79"/>
        <source>Print failed: %1</source>
        <translation type="unfinished">Print failed: %1</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="80"/>
        <source>Print canceled: %1</source>
        <translation type="unfinished">Print canceled: %1</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="81"/>
        <source>Print finished: %1</source>
        <translation type="unfinished">Print finished: %1</translation>
    </message>
</context>
<context>
    <name>PagePackingWizard</name>
    <message>
        <location filename="../qml/PagePackingWizard.qml" line="31"/>
        <source>Packing Wizard</source>
        <translation type="unfinished">Packing Wizard</translation>
    </message>
    <message>
        <location filename="../qml/PagePackingWizard.qml" line="44"/>
        <source>Packing done.</source>
        <translation type="unfinished">Packing done.</translation>
    </message>
    <message>
        <location filename="../qml/PagePackingWizard.qml" line="52"/>
        <source>Insert protective foam</source>
        <translation type="unfinished">Insert protective foam</translation>
    </message>
</context>
<context>
    <name>PagePowerOffDialog</name>
    <message>
        <location filename="../qml/PagePowerOffDialog.qml" line="5"/>
        <source>Power Off?</source>
        <translation type="unfinished">Power Off?</translation>
    </message>
    <message>
        <location filename="../qml/PagePowerOffDialog.qml" line="6"/>
        <source>Do you really want to turn off the printer?</source>
        <translation type="unfinished">Do you really want to turn off the printer?</translation>
    </message>
    <message>
        <location filename="../qml/PagePowerOffDialog.qml" line="9"/>
        <source>Powering Off...</source>
        <translation type="unfinished">Powering Off...</translation>
    </message>
</context>
<context>
    <name>PagePrePrintChecks</name>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="32"/>
        <source>Please Wait</source>
        <translation type="unfinished">Please Wait</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="111"/>
        <source>Temperature</source>
        <translation type="unfinished">Temperature</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="112"/>
        <source>Project</source>
        <translation type="unfinished">Project</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="114"/>
        <source>Fan</source>
        <translation type="unfinished">Fan</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="115"/>
        <source>Cover</source>
        <translation type="unfinished">Cover</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="116"/>
        <source>Resin</source>
        <translation type="unfinished">Resin</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="117"/>
        <source>Starting Positions</source>
        <translation type="unfinished">Starting Positions</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="118"/>
        <source>Stirring</source>
        <translation type="unfinished">Stirring</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="133"/>
        <source>Do not touch the printer!</source>
        <translation type="unfinished">Do not touch the printer!</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="143"/>
        <source>With Warning</source>
        <translation type="unfinished">With Warning</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="144"/>
        <source>Disabled</source>
        <translation type="unfinished">Disabled</translation>
    </message>
</context>
<context>
    <name>PagePrint</name>
    <message>
        <location filename="../qml/PagePrint.qml" line="146"/>
        <source>Continue?</source>
        <translation type="unfinished">Continue?</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="149"/>
        <source>Check Warning</source>
        <translation type="unfinished">Check Warning</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="159"/>
        <location filename="../qml/PagePrint.qml" line="275"/>
        <source>Stuck Recovery</source>
        <translation type="unfinished">Stuck Recovery</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="160"/>
        <source>The printer got stuck and needs user assistance.</source>
        <translation type="unfinished">The printer got stuck and needs user assistance.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="161"/>
        <source>Release the tank mechanism and press Continue.</source>
        <translation type="unfinished">Release the tank mechanism and press Continue.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="162"/>
        <source>If you do not want to continue, press the Back button on top of the screen and the current job will be canceled.</source>
        <translation type="unfinished">If you do not want to continue, press the Back button on top of the screen and the current job will be canceled.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="181"/>
        <source>Close Cover!</source>
        <translation type="unfinished">Close the cover!</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="182"/>
        <source>Please, close the cover! UV radiation is harmful.</source>
        <translation type="unfinished">Please, close the lid! UV radiation is harmful.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="238"/>
        <source>Going up</source>
        <translation type="unfinished">Going up</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="238"/>
        <source>Moving platform to the top position</source>
        <translation type="unfinished">Moving platform to the top position</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="241"/>
        <source>Going down</source>
        <translation type="unfinished">Going down</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="241"/>
        <source>Moving platform to the bottom position</source>
        <translation type="unfinished">Moving platform to the bottom position</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="244"/>
        <source>Project</source>
        <translation type="unfinished">Project</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="244"/>
        <source>Getting the printer ready to add resin. Please wait.</source>
        <translation type="unfinished">Getting the printer ready to add resin. Please wait.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="247"/>
        <location filename="../qml/PagePrint.qml" line="301"/>
        <source>Please wait...</source>
        <translation type="unfinished">Please wait...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="250"/>
        <source>Cover Open</source>
        <translation type="unfinished">Cover Open</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="250"/>
        <source>Paused.</source>
        <translation type="unfinished">Paused.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="250"/>
        <source>Please close the cover to continue</source>
        <translation type="unfinished">Please close the cover to continue</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="262"/>
        <source>Stirring</source>
        <translation type="unfinished">Stirring</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="262"/>
        <source>Stirring resin</source>
        <translation type="unfinished">Stirring resin</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="265"/>
        <source>Action Pending</source>
        <translation type="unfinished">Action Pending</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="265"/>
        <source>Requested actions will be executed after layer finish, please wait...</source>
        <translation type="unfinished">Requested actions will be executed once this layer finishes, please wait...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="268"/>
        <source>Reading data...</source>
        <translation type="unfinished">Reading data...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="275"/>
        <source>Setting start positions...</source>
        <translation type="unfinished">Setting start positions...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="278"/>
        <source>Tank Moving Down</source>
        <translation type="unfinished">Tank Moving Down</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="278"/>
        <source>Moving the resin tank down...</source>
        <translation type="unfinished">Moving the resin tank down...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="314"/>
        <source>Loading, please wait...</source>
        <translation type="unfinished">Loading, please wait...</translation>
    </message>
</context>
<context>
    <name>PagePrintPreviewSwipe</name>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="33"/>
        <source>Project</source>
        <translation type="unfinished">Project</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="165"/>
        <source>Swipe to project</source>
        <translation type="unfinished">Swipe to project</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="206"/>
        <source>Unknown</source>
        <translation type="unfinished">Unknown</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="279"/>
        <source>Layers</source>
        <translation type="unfinished">Layers</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="279"/>
        <source>Layer Height</source>
        <translation type="unfinished">Layer Height</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="298"/>
        <source>Exposure Times</source>
        <translation type="unfinished">Exposure Times</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="323"/>
        <source>Print Time Estimate</source>
        <translation type="unfinished">Print Time Estimate</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="340"/>
        <source>Last Modified</source>
        <translation type="unfinished">Last Modified</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="347"/>
        <source>Unknown</source>
        <comment>Unknow time of last modification of a file</comment>
        <translation type="unfinished">Unknown</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="364"/>
        <source>Swipe to continue</source>
        <translation type="unfinished">Swipe to continue</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="433"/>
        <source>The project requires %1 % of the resin. It will be necessary to refill the resin during print.</source>
        <translation type="unfinished">The project requires&lt;br/&gt;%1 % of the resin. It will be necessary to refill the resin during print.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="436"/>
        <source>Please fill the resin tank to at least %1 % and close the cover.</source>
        <translation type="unfinished">Please fill the resin tank to at least %1 % and close the cover.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="471"/>
        <source>Print Settings</source>
        <translation type="unfinished">Print Settings</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="481"/>
        <source>Print</source>
        <translation type="unfinished">Print</translation>
    </message>
</context>
<context>
    <name>PagePrintPrinting</name>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="33"/>
        <source>Print</source>
        <translation type="unfinished">Print</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="104"/>
        <source>Today at</source>
        <translation type="unfinished">Today at</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="107"/>
        <source>Tomorrow at</source>
        <translation type="unfinished">Tomorrow at</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="150"/>
        <source>Layer:</source>
        <translation type="unfinished">Layer:</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="162"/>
        <source>Layer Height:</source>
        <translation type="unfinished">Layer Height:</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="162"/>
        <source>N/A</source>
        <translation type="unfinished">N/A</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="251"/>
        <source>Remaining Time</source>
        <translation type="unfinished">Remaining time</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="256"/>
        <source>Unknown</source>
        <comment>Remaining time is unknown</comment>
        <translation type="unfinished">Unknown</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="266"/>
        <source>Estimated End</source>
        <translation type="unfinished">Estimated End</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="281"/>
        <source>Printing Time</source>
        <translation type="unfinished">Printing time</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="294"/>
        <source>Layer</source>
        <translation type="unfinished">Layer</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="306"/>
        <source>Remaining Resin</source>
        <translation type="unfinished">Remaining resin</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="310"/>
        <location filename="../qml/PagePrintPrinting.qml" line="324"/>
        <source>ml</source>
        <translation type="unfinished">ml</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="310"/>
        <location filename="../qml/PagePrintPrinting.qml" line="324"/>
        <source>Unknown</source>
        <translation type="unfinished">Unknown</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="320"/>
        <source>Consumed Resin</source>
        <translation type="unfinished">Consumed resin</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="359"/>
        <source>Print Settings</source>
        <translation type="unfinished">Print Settings</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="365"/>
        <source>Refill Resin</source>
        <translation type="unfinished">Refill Resin</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="378"/>
        <location filename="../qml/PagePrintPrinting.qml" line="384"/>
        <source>Cancel Print</source>
        <translation type="unfinished">Cancel Print</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="385"/>
        <source>To make sure the print is not stopped accidentally,
please swipe the screen to move to the next step,
where you can cancel the print.</source>
        <translation type="unfinished">To make sure the print is not stopped accidentally,
please swipe the screen to move to the next step,
where you can cancel the print.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="402"/>
        <source>Enter Admin</source>
        <translation type="unfinished">Enter Admin</translation>
    </message>
</context>
<context>
    <name>PagePrintResinIn</name>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="30"/>
        <source>Pour in resin</source>
        <translation type="unfinished">Pour in resin</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="61"/>
        <source>The project requires %1 % of the resin. It will be necessary to refill the resin during print.</source>
        <translation type="unfinished">The project requires&lt;br/&gt;%1 % of the resin. It will be necessary to refill the resin during print.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="64"/>
        <source>Please fill the resin tank to at least %1 % and close the cover.</source>
        <translation type="unfinished">Please fill the resin tank to at least %1 % and close the cover.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="74"/>
        <source>Continue</source>
        <translation type="unfinished">Continue</translation>
    </message>
</context>
<context>
    <name>PagePrusaConnect</name>
    <message>
        <location filename="../qml/PagePrusaConnect.qml" line="27"/>
        <source>Prusa Connect</source>
        <translation type="unfinished">Prusa Connect</translation>
    </message>
    <message>
        <location filename="../qml/PagePrusaConnect.qml" line="50"/>
        <location filename="../qml/PagePrusaConnect.qml" line="92"/>
        <location filename="../qml/PagePrusaConnect.qml" line="113"/>
        <source>Failed to establish a new connection.
Please try later.</source>
        <translation type="unfinished">Failed to establish a new connection.
Please try again later.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrusaConnect.qml" line="70"/>
        <source>It&apos;s already registered!</source>
        <translation type="unfinished">It&apos;s already registered!</translation>
    </message>
    <message>
        <location filename="../qml/PagePrusaConnect.qml" line="70"/>
        <source>Start Your Registration!</source>
        <translation type="unfinished">Start Your Registration!</translation>
    </message>
    <message>
        <location filename="../qml/PagePrusaConnect.qml" line="78"/>
        <source>Restore</source>
        <translation type="unfinished">Restore</translation>
    </message>
    <message>
        <location filename="../qml/PagePrusaConnect.qml" line="78"/>
        <source>Continue</source>
        <translation type="unfinished">Continue</translation>
    </message>
</context>
<context>
    <name>PageQrCode</name>
    <message>
        <location filename="../qml/PageQrCode.qml" line="30"/>
        <source>Page QR code</source>
        <translation type="unfinished">Page QR code</translation>
    </message>
</context>
<context>
    <name>PageReleaseNotes</name>
    <message>
        <location filename="../qml/PageReleaseNotes.qml" line="9"/>
        <source>Release Notes</source>
        <translation type="unfinished">Release Notes</translation>
    </message>
    <message>
        <location filename="../qml/PageReleaseNotes.qml" line="111"/>
        <source>Later</source>
        <translation type="unfinished">Later</translation>
    </message>
    <message>
        <location filename="../qml/PageReleaseNotes.qml" line="120"/>
        <source>Install Now</source>
        <translation type="unfinished">Install Now</translation>
    </message>
</context>
<context>
    <name>PageSelftestWizard</name>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="32"/>
        <source>Selftest</source>
        <translation type="unfinished">Selftest</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="48"/>
        <source>Welcome to the selftest wizard.</source>
        <translation type="unfinished">Welcome to the selftest wizard.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="50"/>
        <source>This procedure is mandatory and it will check all components of the printer.</source>
        <translation type="unfinished">This procedure is mandatory and it will check all components of the printer.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="58"/>
        <source>Please unscrew and remove the resin tank.</source>
        <translation type="unfinished">Please unscrew and remove the resin tank.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="67"/>
        <source>Loosen the black knob and remove the platform.</source>
        <translation type="unfinished">Loosen the black knob and remove the platform.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="76"/>
        <location filename="../qml/PageSelftestWizard.qml" line="137"/>
        <location filename="../qml/PageSelftestWizard.qml" line="169"/>
        <source>Close the cover.</source>
        <translation type="unfinished">Close the cover.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="100"/>
        <source>Can you hear the music?</source>
        <translation type="unfinished">Can you hear the music?</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="110"/>
        <source>Please install the resin tank and tighten the screws evenly.</source>
        <translation type="unfinished">Please install the resin tank and tighten the screws evenly.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="119"/>
        <source>Please install the platform under 60° angle and tighten it with the black knob.</source>
        <translation type="unfinished">Please install the platform under 60° angle and tighten it with the black knob.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="128"/>
        <source>Release the platform from the cantilever and place it onto the center of the resin tank at a 90° angle.</source>
        <translation type="unfinished">Release the platform from the cantilever and place it onto the center of the resin tank at a 90° angle.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="151"/>
        <source>Make sure that the resin tank is installed and the screws are tight.</source>
        <translation type="unfinished">Make sure that the resin tank is installed and the screws are tight.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="160"/>
        <source>Please install the platform under 0° angle and tighten it with the black knob.</source>
        <translation type="unfinished">Please install the platform under 0° angle and tighten it with the black knob.</translation>
    </message>
</context>
<context>
    <name>PageSetDate</name>
    <message>
        <location filename="../qml/PageSetDate.qml" line="37"/>
        <source>Set Date</source>
        <translation type="unfinished">Set Date</translation>
    </message>
    <message>
        <location filename="../qml/PageSetDate.qml" line="665"/>
        <source>Set</source>
        <translation type="unfinished">Set</translation>
    </message>
</context>
<context>
    <name>PageSetHostname</name>
    <message>
        <location filename="../qml/PageSetHostname.qml" line="30"/>
        <source>Set Hostname</source>
        <translation type="unfinished">Set Hostname</translation>
    </message>
    <message>
        <location filename="../qml/PageSetHostname.qml" line="47"/>
        <source>Hostname</source>
        <translation type="unfinished">Hostname</translation>
    </message>
    <message>
        <location filename="../qml/PageSetHostname.qml" line="73"/>
        <source>Can contain only characters a-z, A-Z, 0-9 and  &quot;-&quot;.</source>
        <translation type="unfinished">Can contain only characters a-z, A-Z, 0-9 and  &quot;-&quot;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSetHostname.qml" line="84"/>
        <source>Set</source>
        <translation type="unfinished">Set</translation>
    </message>
</context>
<context>
    <name>PageSetLoginCredentials</name>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="32"/>
        <source>Login Credentials</source>
        <translation type="unfinished">Login Credentials</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="42"/>
        <source>Are you sure?</source>
        <translation type="unfinished">Are you sure?</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="43"/>
        <source>Disable the HTTP Digest?&lt;br/&gt;&lt;br/&gt;CAUTION: This may be insecure!</source>
        <translation type="unfinished">Disable the HTTP Digest?&lt;br/&gt;&lt;br/&gt;CAUTION: This may be insecure!</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="66"/>
        <source>HTTP Digest</source>
        <translation type="unfinished">HTTP Digest</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="109"/>
        <source>User Name</source>
        <translation type="unfinished">User Name</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="146"/>
        <source>Printer Password</source>
        <translation type="unfinished">Printer Password</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="171"/>
        <location filename="../qml/PageSetLoginCredentials.qml" line="179"/>
        <location filename="../qml/PageSetLoginCredentials.qml" line="229"/>
        <source>Must be at least 8 chars long</source>
        <translation type="unfinished">Must be at least 8 chars long</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="185"/>
        <source>Can contain only characters a-z, A-Z, 0-9 and  &quot;-&quot;.</source>
        <translation type="unfinished">Can contain only characters a-z, A-Z, 0-9 and  &quot;-&quot;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="205"/>
        <source>Printer API Key</source>
        <translation type="unfinished">Printer API Key</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="236"/>
        <source>Can contain only ASCII characters.</source>
        <translation type="unfinished">Can contain only ASCII characters.</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="249"/>
        <source>Save</source>
        <translation type="unfinished">Save</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="266"/>
        <source>Failed change login settings.</source>
        <translation type="unfinished">Failed change login settings.</translation>
    </message>
</context>
<context>
    <name>PageSetTime</name>
    <message>
        <location filename="../qml/PageSetTime.qml" line="32"/>
        <source>Set Time</source>
        <translation type="unfinished">Set Time</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTime.qml" line="98"/>
        <source>Hour</source>
        <translation type="unfinished">Hour</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTime.qml" line="105"/>
        <source>Minute</source>
        <translation type="unfinished">Minute</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTime.qml" line="112"/>
        <source>Set</source>
        <translation type="unfinished">Set</translation>
    </message>
</context>
<context>
    <name>PageSetTimezone</name>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="29"/>
        <source>Set Timezone</source>
        <translation type="unfinished">Set Timezone</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="129"/>
        <source>Region</source>
        <translation type="unfinished">Region</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="135"/>
        <source>City</source>
        <translation type="unfinished">City</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="141"/>
        <source>Set</source>
        <translation type="unfinished">Set</translation>
    </message>
</context>
<context>
    <name>PageSettings</name>
    <message>
        <location filename="../qml/PageSettings.qml" line="28"/>
        <source>Settings</source>
        <translation type="unfinished">Settings</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="58"/>
        <source>Calibration</source>
        <translation type="unfinished">Calibration</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="66"/>
        <source>Network</source>
        <translation type="unfinished">Network</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="75"/>
        <source>Platform &amp; Resin Tank</source>
        <translation type="unfinished">Platform &amp; Resin Tank</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="84"/>
        <source>Firmware</source>
        <translation type="unfinished">Firmware</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="93"/>
        <source>Settings &amp; Sensors</source>
        <translation type="unfinished">Settings &amp; Sensors</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="102"/>
        <source>Touchscreen</source>
        <translation type="unfinished">Touchscreen</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="111"/>
        <source>Language &amp; Time</source>
        <translation type="unfinished">Language &amp; Time</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="120"/>
        <source>System Logs</source>
        <translation type="unfinished">System Logs</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="129"/>
        <source>Support</source>
        <translation type="unfinished">Support</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="139"/>
        <source>Enter Admin</source>
        <translation type="unfinished">Enter Admin</translation>
    </message>
</context>
<context>
    <name>PageSettingsCalibration</name>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="31"/>
        <source>Calibration</source>
        <translation type="unfinished">Calibration</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="50"/>
        <source>Selftest</source>
        <translation type="unfinished">Selftest</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="57"/>
        <source>Printer Calibration</source>
        <translation type="unfinished">Printer Calibration</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="67"/>
        <source>UV Calibration</source>
        <translation type="unfinished">UV calibration</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="75"/>
        <source>Display Test</source>
        <translation type="unfinished">Display Test</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="92"/>
        <source>New Display?</source>
        <translation type="unfinished">New Display?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="93"/>
        <source>Did you replace the EXPOSITION DISPLAY?</source>
        <translation type="unfinished">Did you replace the EXPOSITION DISPLAY?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="105"/>
        <source>New UV LED SET?</source>
        <translation type="unfinished">New UV LED SET?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="106"/>
        <source>Did you replace the UV LED SET?</source>
        <translation type="unfinished">Did you replace the UV LED SET?</translation>
    </message>
</context>
<context>
    <name>PageSettingsFirmware</name>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="35"/>
        <source>Firmware</source>
        <translation type="unfinished">Firmware</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="45"/>
        <source>Unknown</source>
        <comment>Unknown operating system version on alternative slot</comment>
        <translation type="unfinished">Unknown</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="74"/>
        <source>Download Now</source>
        <translation type="unfinished">Download Now</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="76"/>
        <source>Install Now</source>
        <translation type="unfinished">Install Now</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="78"/>
        <source>Check for Update</source>
        <translation type="unfinished">Check for Updates</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="85"/>
        <source>Check for update failed</source>
        <translation type="unfinished">Check for update failed</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="87"/>
        <source>Checking for updates...</source>
        <translation type="unfinished">Checking for updates...</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="89"/>
        <source>System is up-to-date</source>
        <translation type="unfinished">System is up-to-date</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="91"/>
        <source>Update available</source>
        <translation type="unfinished">Update available</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="93"/>
        <source>Update is downloading</source>
        <translation type="unfinished">Update is downloading</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="95"/>
        <source>Update is downloaded</source>
        <translation type="unfinished">Update is being downloaded</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="97"/>
        <source>Update download failed</source>
        <translation type="unfinished">Update download failed</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="99"/>
        <source>Updater service idle</source>
        <translation type="unfinished">Updater service idle</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="111"/>
        <source>Installed version</source>
        <translation type="unfinished">Installed version</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="137"/>
        <source>Receive Beta Updates</source>
        <translation type="unfinished">Receive Beta Updates</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="155"/>
        <source>Switch to beta?</source>
        <translation type="unfinished">Switch to beta?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="156"/>
        <source>Warning! The beta updates can be unstable.&lt;br/&gt;&lt;br/&gt;Not recommended for production printers.&lt;br/&gt;&lt;br/&gt;Continue?</source>
        <translation type="unfinished">Warning! The beta updates can be unstable.&lt;br/&gt;&lt;br/&gt;Not recommended for production printers.&lt;br/&gt;&lt;br/&gt;Continue?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="168"/>
        <source>Switch to version:</source>
        <translation type="unfinished">Switch to version:</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="180"/>
        <source>Downgrade?</source>
        <translation type="unfinished">Downgrade?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="181"/>
        <source>Do you really want to downgrade to FW</source>
        <translation type="unfinished">Do you really want to downgrade to FW</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="209"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="415"/>
        <source>Incompatible FW!</source>
        <translation type="unfinished">Incompatible FW!</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="210"/>
        <source>The alternative FW version &lt;b&gt;%1&lt;/b&gt; is not compatible with your printer model - &lt;b&gt;%2&lt;/b&gt;.</source>
        <translation type="unfinished">The alternative FW version &lt;b&gt;%1&lt;/b&gt; is not compatible with your printer model - &lt;b&gt;%2&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="212"/>
        <source>If you switch, you can update to another FW version afterwards but &lt;b&gt;you will not be able to print.&lt;/b&gt;</source>
        <translation type="unfinished">If you switch, you can update to another FW version afterwards but &lt;b&gt;you will not be able to print.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="214"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="420"/>
        <source>Continue anyway?</source>
        <translation type="unfinished">Continue anyway?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="234"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="427"/>
        <source>None</source>
        <comment>Printer model is not known/can&apos;t be determined</comment>
        <translation type="unfinished">None</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="238"/>
        <source>Newer than SL1S</source>
        <comment>Printer model is unknown, but better than SL1S</comment>
        <translation type="unfinished">Newer than SL1S</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="247"/>
        <source>Factory Reset</source>
        <translation type="unfinished">Factory Reset</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="256"/>
        <source>Are you sure?</source>
        <translation type="unfinished">Are you sure?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="257"/>
        <source>Do you really want to perform the factory reset?

All settings will be erased!
Projects will stay untouched.</source>
        <translation type="unfinished">Do you really want to perform the factory reset?

All settings will be erased!
Projects will stay untouched.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="292"/>
        <source>FW Info</source>
        <comment>page title, information about the selected update bundle</comment>
        <translation type="unfinished">FW Info</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="293"/>
        <source>Loading FW file meta information
(may take up to 20 seconds) ...</source>
        <translation type="unfinished">Loading FW file meta information
(may take up to 20 seconds) ...</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="335"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="384"/>
        <source>Install Firmware?</source>
        <translation type="unfinished">Install Firmware?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="336"/>
        <source>FW file meta information not found.&lt;br/&gt;&lt;br/&gt;Do you want to install&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;anyway?</source>
        <translation type="unfinished">FW file meta information not found.&lt;br/&gt;&lt;br/&gt;Do you want to install&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;anyway?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="349"/>
        <source>Downgrade Firmware?</source>
        <translation type="unfinished">Downgrade Firmware?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="352"/>
        <source>Version of selected FW file&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;,</source>
        <translation type="unfinished">Version of selected FW file&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;,</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="354"/>
        <source>is lower or equal than current&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;.</source>
        <translation type="unfinished">is lower or equal than current&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="356"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="389"/>
        <source>Do you want to continue?</source>
        <translation type="unfinished">Do you want to continue?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="385"/>
        <source>You have selected update bundle &lt;b&gt;&quot;%1&quot;&lt;/b&gt;,</source>
        <translation type="unfinished">You have selected update bundle &lt;b&gt;&quot;%1&quot;&lt;/b&gt;,</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="387"/>
        <source>which has version &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation type="unfinished">which has version &lt;b&gt;%1&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="416"/>
        <source>&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;is not compatible with your current hardware model - &lt;b&gt;%2&lt;/b&gt;.</source>
        <translation type="unfinished">&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;is not compatible with your current hardware model - &lt;b&gt;%2&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="418"/>
        <source>After installing this update, the printer can be updated to another FW but &lt;b&gt;printing won&apos;t work.&lt;/b&gt;</source>
        <translation type="unfinished">After installing this update, the printer can be updated to another FW but &lt;b&gt;printing won&apos;t work.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="430"/>
        <source>Unknown</source>
        <comment>Printer model is unknown, but likely better than SL1S</comment>
        <translation type="unfinished">Unknown</translation>
    </message>
</context>
<context>
    <name>PageSettingsLanguageTime</name>
    <message>
        <location filename="../qml/PageSettingsLanguageTime.qml" line="28"/>
        <source>Language &amp; Time</source>
        <translation type="unfinished">Language &amp; Time</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsLanguageTime.qml" line="34"/>
        <source>Set Language</source>
        <translation type="unfinished">Set Language</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsLanguageTime.qml" line="40"/>
        <source>Time Settings</source>
        <translation type="unfinished">Time Settings</translation>
    </message>
</context>
<context>
    <name>PageSettingsNetwork</name>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="27"/>
        <source>Network</source>
        <translation type="unfinished">Network</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="33"/>
        <source>Ethernet</source>
        <translation type="unfinished">Ethernet</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="41"/>
        <source>Wi-Fi</source>
        <translation type="unfinished">Wi-Fi</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="49"/>
        <source>Hot Spot</source>
        <translation type="unfinished">Hot Spot</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="58"/>
        <source>Set Hostname</source>
        <translation type="unfinished">Set Hostname</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="67"/>
        <source>Login Credentials</source>
        <translation type="unfinished">Login Credentials</translation>
    </message>
</context>
<context>
    <name>PageSettingsPlatformResinTank</name>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="27"/>
        <source>Platform &amp; Resin Tank</source>
        <translation type="unfinished">Platform &amp; Resin Tank</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="33"/>
        <source>Move Platform</source>
        <translation type="unfinished">Move Platform</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="40"/>
        <source>Move Resin Tank</source>
        <translation type="unfinished">Move Resin Tank</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="47"/>
        <source>Disable Steppers</source>
        <translation type="unfinished">Disable Steppers</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="55"/>
        <source>Platform Axis Sensitivity</source>
        <translation type="unfinished">Platform Axis Sensitivity</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="77"/>
        <source>Tank Axis Sensitivity</source>
        <translation type="unfinished">Tank Axis Sensitivity</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="99"/>
        <source>Limit for Fast Tilt</source>
        <translation type="unfinished">Limit for Fast Tilt</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="106"/>
        <source>%</source>
        <translation type="unfinished">%</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="125"/>
        <source>Platform Offset</source>
        <translation type="unfinished">Platform Offset</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="132"/>
        <source>um</source>
        <translation type="unfinished">um</translation>
    </message>
</context>
<context>
    <name>PageSettingsSensors</name>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="27"/>
        <source>Settings &amp; Sensors</source>
        <translation type="unfinished">Settings &amp; Sensors</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="35"/>
        <source>Device hash in QR</source>
        <translation type="unfinished">Device hash in QR</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="60"/>
        <source>Auto Power Off</source>
        <translation type="unfinished">Auto Power Off</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="86"/>
        <source>Cover Check</source>
        <translation type="unfinished">Cover Check</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="110"/>
        <location filename="../qml/PageSettingsSensors.qml" line="145"/>
        <source>Are you sure?</source>
        <translation type="unfinished">Are you sure?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="111"/>
        <source>Disable the cover sensor?&lt;br/&gt;&lt;br/&gt;CAUTION: This may lead to unwanted exposure to UV light or personal injury due to moving parts. This action is not recommended!</source>
        <translation type="unfinished">Disable the cover sensor?&lt;br/&gt;&lt;br/&gt;CAUTION: This may lead to unwanted exposure to UV light or personal injury due to moving parts. This action is not recommended!</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="123"/>
        <source>Resin Sensor</source>
        <translation type="unfinished">Resin Sensor</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="146"/>
        <source>Disable the resin sensor?&lt;br/&gt;&lt;br/&gt;CAUTION: This may lead to failed prints or resin tank overflow! This action is not recommended!</source>
        <translation type="unfinished">Disable the resin sensor?&lt;br/&gt;&lt;br/&gt;CAUTION: This may lead to failed prints or resin tank overflow! This action is not recommended!</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="158"/>
        <source>Rear Fan Speed</source>
        <translation type="unfinished">Rear Fan Speed</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="165"/>
        <source>RPM</source>
        <translation type="unfinished">RPM</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="177"/>
        <source>Off</source>
        <translation type="unfinished">Off</translation>
    </message>
</context>
<context>
    <name>PageSettingsSupport</name>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="29"/>
        <source>Support</source>
        <translation type="unfinished">Support</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="35"/>
        <source>Download Examples</source>
        <translation type="unfinished">Download Examples</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="49"/>
        <source>Manual</source>
        <translation type="unfinished">Manual</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="59"/>
        <source>Videos</source>
        <translation type="unfinished">Videos</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="68"/>
        <source>System Information</source>
        <translation type="unfinished">System Information</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="77"/>
        <source>About Us</source>
        <translation type="unfinished">About Us</translation>
    </message>
</context>
<context>
    <name>PageSettingsSystemLogs</name>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="28"/>
        <source>System Logs</source>
        <translation type="unfinished">System Logs</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="39"/>
        <source>Last Seen Logs:</source>
        <translation type="unfinished">Last Seen Logs:</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="39"/>
        <source>&lt;b&gt;No logs have been uploaded yet.&lt;/b&gt;</source>
        <translation type="unfinished">&lt;b&gt;No logs have been uploaded yet.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="54"/>
        <source>Save to USB Drive</source>
        <translation type="unfinished">Save to USB Drive</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="68"/>
        <source>Upload to Server</source>
        <translation type="unfinished">Upload to Server</translation>
    </message>
</context>
<context>
    <name>PageSettingsTouchscreen</name>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="27"/>
        <source>Touchscreen</source>
        <translation type="unfinished">Touchscreen</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="35"/>
        <source>Screensaver timer</source>
        <translation type="unfinished">Screensaver timer</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="55"/>
        <source>h</source>
        <comment>hours short</comment>
        <translation type="unfinished">h</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="56"/>
        <source>min</source>
        <comment>minutes short</comment>
        <translation type="unfinished">min</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="57"/>
        <source>s</source>
        <comment>seconds short</comment>
        <translation type="unfinished">s</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="62"/>
        <source>Off</source>
        <translation type="unfinished">Off</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="79"/>
        <source>Touch Screen Brightness</source>
        <translation type="unfinished">Touch Screen Brightness</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="97"/>
        <source>N/A</source>
        <translation type="unfinished">N/A</translation>
    </message>
</context>
<context>
    <name>PageShowToken</name>
    <message>
        <location filename="../qml/PageShowToken.qml" line="27"/>
        <source>Prusa Connect</source>
        <translation type="unfinished">Prusa Connect</translation>
    </message>
    <message>
        <location filename="../qml/PageShowToken.qml" line="31"/>
        <source>Temporary Token</source>
        <translation type="unfinished">Temporary Token</translation>
    </message>
    <message>
        <location filename="../qml/PageShowToken.qml" line="61"/>
        <source>N/A</source>
        <translation type="unfinished">N/A</translation>
    </message>
    <message>
        <location filename="../qml/PageShowToken.qml" line="80"/>
        <source>Continue</source>
        <translation type="unfinished">Continue</translation>
    </message>
</context>
<context>
    <name>PageSoftwareLicenses</name>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="28"/>
        <source>Software Packages</source>
        <translation type="unfinished">Software Packages</translation>
    </message>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="112"/>
        <location filename="../qml/PageSoftwareLicenses.qml" line="233"/>
        <source>Package Name</source>
        <translation type="unfinished">Package Name</translation>
    </message>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="242"/>
        <source>Version</source>
        <translation type="unfinished">Version</translation>
    </message>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="250"/>
        <source>License</source>
        <translation type="unfinished">License</translation>
    </message>
</context>
<context>
    <name>PageSysinfo</name>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="30"/>
        <source>System Information</source>
        <translation type="unfinished">System Information</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="52"/>
        <source>System</source>
        <translation type="unfinished">System</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="57"/>
        <source>OS Image Version</source>
        <translation type="unfinished">OS Image Version</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="62"/>
        <source>Printer Model</source>
        <translation type="unfinished">Printer Model</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="65"/>
        <source>None</source>
        <comment>Printer model is not known/can&apos;t be determined</comment>
        <translation type="unfinished">None</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="69"/>
        <source>Newer than SL1S</source>
        <comment>Printer model is unknown, but better than SL1S</comment>
        <translation type="unfinished">Newer than SL1S</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="75"/>
        <source>A64 Controller SN</source>
        <translation type="unfinished">A64 Controller SN</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="80"/>
        <source>API Key / Printer Password</source>
        <translation type="unfinished">API Key / Printer Password</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="92"/>
        <source>Other Components</source>
        <translation type="unfinished">Other Components</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="97"/>
        <source>Motion Controller SN</source>
        <translation type="unfinished">Motion Controller SN</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="102"/>
        <source>Motion Controller SW Version</source>
        <translation type="unfinished">Motion Controller SW version</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="107"/>
        <source>Motion Controller HW Revision</source>
        <translation type="unfinished">Motion Controller HW Revision</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="112"/>
        <source>Booster Board SN</source>
        <translation type="unfinished">Booster Board SN</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="118"/>
        <source>Exposure display SN</source>
        <translation type="unfinished">Exposure display SN</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="124"/>
        <source>GUI Version</source>
        <translation type="unfinished">GUI Version</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="136"/>
        <source>Hardware State</source>
        <translation type="unfinished">Hardware State</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="141"/>
        <source>Network State</source>
        <translation type="unfinished">Network State</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="142"/>
        <source>Online</source>
        <translation type="unfinished">Online</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="142"/>
        <source>Offline</source>
        <translation type="unfinished">Offline</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="146"/>
        <source>Ethernet IP Address</source>
        <translation type="unfinished">Ethernet IP Address</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="147"/>
        <location filename="../qml/PageSysinfo.qml" line="152"/>
        <location filename="../qml/PageSysinfo.qml" line="230"/>
        <location filename="../qml/PageSysinfo.qml" line="263"/>
        <location filename="../qml/PageSysinfo.qml" line="268"/>
        <location filename="../qml/PageSysinfo.qml" line="273"/>
        <location filename="../qml/PageSysinfo.qml" line="278"/>
        <location filename="../qml/PageSysinfo.qml" line="283"/>
        <source>N/A</source>
        <translation type="unfinished">N/A</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="151"/>
        <source>Wifi IP Address</source>
        <translation type="unfinished">Wifi IP Address</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="156"/>
        <source>Time of Fast Tilt</source>
        <translation type="unfinished">Time of Fast Tilt</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="157"/>
        <location filename="../qml/PageSysinfo.qml" line="162"/>
        <source>seconds</source>
        <translation type="unfinished">seconds</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="161"/>
        <source>Time of Slow Tilt</source>
        <translation type="unfinished">Time of Slow Tilt</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="166"/>
        <source>Resin Sensor State</source>
        <translation type="unfinished">Resin Sensor State</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="167"/>
        <source>Triggered</source>
        <translation type="unfinished">Triggered</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="167"/>
        <source>Not triggered</source>
        <translation type="unfinished">Not triggered</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="171"/>
        <source>Cover State</source>
        <translation type="unfinished">Cover State</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="172"/>
        <source>Closed</source>
        <translation type="unfinished">Closed</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="172"/>
        <source>Open</source>
        <translation type="unfinished">Open</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="176"/>
        <source>CPU Temperature</source>
        <translation type="unfinished">CPU Temperature</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="177"/>
        <location filename="../qml/PageSysinfo.qml" line="182"/>
        <location filename="../qml/PageSysinfo.qml" line="187"/>
        <source>°C</source>
        <translation type="unfinished">°C</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="181"/>
        <source>UV LED Temperature</source>
        <translation type="unfinished">UV LED Temperature</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="186"/>
        <source>Ambient Temperature</source>
        <translation type="unfinished">Ambient Temperature</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="191"/>
        <source>UV LED Fan</source>
        <translation type="unfinished">UV LED Fan</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="194"/>
        <location filename="../qml/PageSysinfo.qml" line="204"/>
        <location filename="../qml/PageSysinfo.qml" line="214"/>
        <source>Fan Error!</source>
        <translation type="unfinished">Fan Error!</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="196"/>
        <location filename="../qml/PageSysinfo.qml" line="206"/>
        <location filename="../qml/PageSysinfo.qml" line="216"/>
        <source>RPM</source>
        <translation type="unfinished">RPM</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="201"/>
        <source>Blower Fan</source>
        <translation type="unfinished">Blower Fan</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="211"/>
        <source>Rear Fan</source>
        <translation type="unfinished">Rear Fan</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="221"/>
        <source>UV LED</source>
        <translation type="unfinished">UV LED</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="235"/>
        <source>Power Supply Voltage</source>
        <translation type="unfinished">Power Supply Voltage</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="236"/>
        <source>V</source>
        <translation type="unfinished">V</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="247"/>
        <source>Statistics</source>
        <translation type="unfinished">Statistics</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="252"/>
        <source>UV LED Time Counter</source>
        <translation type="unfinished">UV LED Time Counter</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="253"/>
        <location filename="../qml/PageSysinfo.qml" line="258"/>
        <location filename="../qml/PageSysinfo.qml" line="278"/>
        <source>d</source>
        <translation type="unfinished">d</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="253"/>
        <location filename="../qml/PageSysinfo.qml" line="258"/>
        <location filename="../qml/PageSysinfo.qml" line="278"/>
        <source>h</source>
        <translation type="unfinished">h</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="253"/>
        <location filename="../qml/PageSysinfo.qml" line="258"/>
        <location filename="../qml/PageSysinfo.qml" line="278"/>
        <source>m</source>
        <translation type="unfinished">m</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="257"/>
        <source>Print Display Time Counter</source>
        <translation type="unfinished">Print Display Time Counter</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="262"/>
        <source>Started Projects</source>
        <translation type="unfinished">Started Projects</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="267"/>
        <source>Finished Projects</source>
        <translation type="unfinished">Finished Projects</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="272"/>
        <source>Total Layers Printed</source>
        <translation type="unfinished">Total Layers Printed</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="277"/>
        <source>Total Print Time</source>
        <translation type="unfinished">Total Print Time</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="282"/>
        <source>Total Resin Consumed</source>
        <translation type="unfinished">Total Resin Consumed</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="283"/>
        <source>ml</source>
        <translation type="unfinished">ml</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="293"/>
        <source>Show software packages &amp; licenses</source>
        <translation type="unfinished">Show software packages &amp; licenses</translation>
    </message>
</context>
<context>
    <name>PageTiltmove</name>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="32"/>
        <source>Move Resin Tank</source>
        <translation type="unfinished">Move Resin Tank</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="76"/>
        <source>Fast Up</source>
        <translation type="unfinished">Fast Up</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="84"/>
        <source>Fast Down</source>
        <translation type="unfinished">Fast Down</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="92"/>
        <source>Up</source>
        <translation type="unfinished">Up</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="100"/>
        <source>Down</source>
        <translation type="unfinished">Down</translation>
    </message>
</context>
<context>
    <name>PageTimeSettings</name>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="34"/>
        <source>Time Settings</source>
        <translation type="unfinished">Time Settings</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="49"/>
        <source>Use NTP</source>
        <translation type="unfinished">Automatic time setting</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="67"/>
        <source>Time</source>
        <translation type="unfinished">Time</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="87"/>
        <source>Date</source>
        <translation type="unfinished">Date</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="107"/>
        <source>Timezone</source>
        <translation type="unfinished">Timezone</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="124"/>
        <source>Time Format</source>
        <translation type="unfinished">Time Format</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="135"/>
        <source>Native</source>
        <comment>Default time format determined by the locale</comment>
        <translation type="unfinished">Native</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="136"/>
        <source>12-hour</source>
        <comment>12h time format</comment>
        <translation type="unfinished">12-hour</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="137"/>
        <source>24-hour</source>
        <comment>24h time format</comment>
        <translation type="unfinished">24-hour</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="158"/>
        <source>Automatic time settings using NTP ...</source>
        <translation type="unfinished">Automatic time settings using NTP ...</translation>
    </message>
</context>
<context>
    <name>PageTowermove</name>
    <message>
        <location filename="../qml/PageTowermove.qml" line="32"/>
        <source>Move Platform</source>
        <translation type="unfinished">Move Platform</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="77"/>
        <source>Fast Up</source>
        <translation type="unfinished">Fast Up</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="85"/>
        <source>Fast Down</source>
        <translation type="unfinished">Fast Down</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="93"/>
        <source>Up</source>
        <translation type="unfinished">Up</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="101"/>
        <source>Down</source>
        <translation type="unfinished">Down</translation>
    </message>
</context>
<context>
    <name>PageUnpackingCompleteWizard</name>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="31"/>
        <source>Unpacking</source>
        <translation type="unfinished">Unpacking</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="47"/>
        <source>Unscrew and remove the resin tank and remove the black foam underneath it.</source>
        <translation type="unfinished">Unscrew and remove the resin tank and remove the black foam underneath it.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="56"/>
        <source>Remove the black foam from both sides of the platform.</source>
        <translation type="unfinished">Remove the black foam from both sides of the platform.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="65"/>
        <source>Please remove the safety sticker and open the cover.</source>
        <translation type="unfinished">Remove the safety sticker and open the lid</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="74"/>
        <source>Carefully peel off the protective sticker from the exposition display.</source>
        <translation type="unfinished">Carefully peel off the protective sticker from the exposition display.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="83"/>
        <source>Unpacking done.</source>
        <translation type="unfinished">Unpacking done.</translation>
    </message>
</context>
<context>
    <name>PageUnpackingKitWizard</name>
    <message>
        <location filename="../qml/PageUnpackingKitWizard.qml" line="31"/>
        <source>Unpacking</source>
        <translation type="unfinished">Unpacking</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingKitWizard.qml" line="44"/>
        <source>Carefully peel off the protective sticker from the exposition display.</source>
        <translation type="unfinished">Carefully peel off the protective sticker from the exposition display.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingKitWizard.qml" line="53"/>
        <source>Unpacking done.</source>
        <translation type="unfinished">Unpacking done.</translation>
    </message>
</context>
<context>
    <name>PageUpdatingFirmware</name>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="36"/>
        <source>Printer Update</source>
        <translation type="unfinished">Printer Update</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="42"/>
        <source>Unknown</source>
        <translation type="unfinished">Unknown</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="170"/>
        <source>Downloading</source>
        <translation type="unfinished">Downloading</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="211"/>
        <source>Downloading firmware, installation will begin immediately after.</source>
        <translation type="unfinished">Downloading firmware, installation will begin immediately after.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="228"/>
        <source>Download failed.</source>
        <translation type="unfinished">Download failed.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="254"/>
        <source>Installing Firmware</source>
        <translation type="unfinished">Installing Firmware</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="295"/>
        <source>Do not power off the printer while updating!&lt;br/&gt;Printer will be rebooted after a successful update.</source>
        <translation type="unfinished">Do not power off the printer while updating!&lt;br/&gt;The printer will be rebooted after a successful update.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="345"/>
        <source>Updated to %1 failed.</source>
        <translation type="unfinished">Updated to %1 failed.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="362"/>
        <source>Printer is being restarted into the new firmware(%1), please wait</source>
        <translation type="unfinished">Printer is being restarted into the new firmware(%1), please wait</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="387"/>
        <source>Update to %1 failed.</source>
        <translation type="unfinished">Update to %1 failed.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="420"/>
        <source>A problem has occurred while updating, please let us know (send us the logs). Do not panic, your printer is still working the same as it did before. Continue by pressing &quot;back&quot;</source>
        <translation type="unfinished">A problem has occurred while updating, please let us know (send us the logs). Do not panic, your printer is still working the same as it did before. Continue by pressing &quot;back&quot;</translation>
    </message>
</context>
<context>
    <name>PageUpgradeWizard</name>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="31"/>
        <source>Hardware Upgrade</source>
        <translation type="unfinished">Hardware Upgrade</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="45"/>
        <source>SL1S components detected (upgrade from SL1).</source>
        <translation type="unfinished">SL1S components detected (upgrade from SL1).</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="46"/>
        <source>To complete the upgrade procedure, printer needs to clear the configuration and reboot.</source>
        <translation type="unfinished">To complete the upgrade procedure, printer needs to clear the configuration and reboot.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="47"/>
        <source>Proceed?</source>
        <translation type="unfinished">Proceed?</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="56"/>
        <source>The printer will power off now.</source>
        <translation type="unfinished">The printer will power off now.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="57"/>
        <source>Reassemble SL1 components and power on the printer. This will restore the original state.</source>
        <translation type="unfinished">Reassemble SL1 components and power on the printer. This will restore the original state.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="65"/>
        <source>The configuration is going to be cleared now.</source>
        <translation type="unfinished">The configuration is going to be cleared now.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="66"/>
        <source>The printer will ask for the inital setup after reboot.</source>
        <translation type="unfinished">The printer will ask for the inital setup after reboot.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="75"/>
        <source>Use only the plastic resin tank supplied. Using the old metal resin tank may cause resin to spill and damage your printer!</source>
        <translation type="unfinished">Use only the plastic resin tank supplied. Using the old metal resin tank may cause resin to spill and damage your printer!</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="84"/>
        <source>Only use the platform supplied. Using a different platform may cause resin to spill and damage your printer!</source>
        <translation type="unfinished">Only use the platform supplied. Using a different platform may cause the resin to spill and damage your printer!</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="93"/>
        <source>Please note that downgrading is not supported. 

Downgrading your printer will erase your UV calibration and your printer will not work properly. 

You will need to recalibrate it using an external UV calibrator.</source>
        <translation type="unfinished">Please note that downgrading is not supported. 

Downgrading your printer will erase your UV calibration and your printer will not work properly. 

You will need to recalibrate it using an external UV calibrator.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="101"/>
        <source>Upgrade done. In the next step, the printer will be restarted.</source>
        <translation type="unfinished">Upgrade done. In the next step, the printer will be restarted.</translation>
    </message>
</context>
<context>
    <name>PageUvCalibrationWizard</name>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="31"/>
        <source>UV Calibration</source>
        <translation type="unfinished">UV calibration</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="46"/>
        <source>Welcome to the UV calibration.</source>
        <translation type="unfinished">Welcome to the UV calibration.</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="48"/>
        <source>1. If the resin tank is in the printer, remove it along with the screws.</source>
        <translation type="unfinished">1. If the resin tank is in the printer, remove it along with the screws.</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="50"/>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="70"/>
        <source>2. Close the cover, don&apos;t open it! UV radiation is harmful!</source>
        <translation type="unfinished">2. Close the lid, don&apos;t open it! UV radiation may be harmful!</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="53"/>
        <source>Intensity: center %1, edge %2</source>
        <translation type="unfinished">Intensity: center %1, edge %2</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="55"/>
        <source>Warm-up: %1 s</source>
        <translation type="unfinished">Warm-up: %1 s</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="68"/>
        <source>1. Place the UV calibrator on the print display and connect it to the front USB.</source>
        <translation type="unfinished">1. Place the UV calibrator on the print display and connect it to the front USB.</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="80"/>
        <source>Open the cover, &lt;b&gt;remove and disconnect&lt;/b&gt; the UV calibrator.</source>
        <translation type="unfinished">Open the lid, &lt;br&gt;remove and disconnect&lt;/b&gt; the UV calibrator.</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="95"/>
        <source>The result of calibration:</source>
        <translation type="unfinished">The result of calibration:</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="96"/>
        <source>UV PWM: %1</source>
        <translation type="unfinished">UV PWM: %1</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="97"/>
        <source>UV Intensity: %1, σ = %2</source>
        <translation type="unfinished">UV Intensity: %1, σ = %2</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="98"/>
        <source>UV Intensity min: %1, max: %2</source>
        <translation type="unfinished">UV Intensity min: %1, max: %2</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="102"/>
        <source>The printer has been successfully calibrated!
Would you like to apply the calibration results?</source>
        <translation type="unfinished">The printer has been successfully calibrated!
Would you like to apply the calibration results?</translation>
    </message>
</context>
<context>
    <name>PageVerticalList</name>
    <message>
        <location filename="../qml/PageVerticalList.qml" line="57"/>
        <source>Loading, please wait...</source>
        <translation type="unfinished">Loading, please wait...</translation>
    </message>
</context>
<context>
    <name>PageVideos</name>
    <message>
        <location filename="../qml/PageVideos.qml" line="23"/>
        <source>Videos</source>
        <translation type="unfinished">Videos</translation>
    </message>
    <message>
        <location filename="../qml/PageVideos.qml" line="26"/>
        <source>Scanning the QR code will take you to our YouTube playlist with videos about this device.

Alternatively, use this link:</source>
        <translation type="unfinished">Scanning the QR code will take you to our YouTube playlist with videos about this device.

Alternatively, use this link:</translation>
    </message>
</context>
<context>
    <name>PageWait</name>
    <message>
        <location filename="../qml/PageWait.qml" line="27"/>
        <source>Please Wait</source>
        <translation type="unfinished">Please Wait</translation>
    </message>
</context>
<context>
    <name>PageWifiNetworkSettings</name>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="34"/>
        <source>Wireless Settings</source>
        <translation type="unfinished">Wireless Settings</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="90"/>
        <source>Connected</source>
        <translation type="unfinished">Connected</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="90"/>
        <source>Disconnected</source>
        <translation type="unfinished">Disconnected</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="102"/>
        <source>Network Info</source>
        <translation type="unfinished">Network info</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="109"/>
        <source>DHCP</source>
        <translation type="unfinished">DHCP</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="191"/>
        <source>Apply</source>
        <translation type="unfinished">Apply</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="201"/>
        <source>Configuring the connection,
please wait...</source>
        <comment>This is horizontal-center aligned, ideally 2 lines</comment>
        <translation type="unfinished">Configuring the connection, please wait...</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="208"/>
        <source>Revert</source>
        <comment>Turn back the changes and go back to the previous configuration.</comment>
        <translation type="unfinished">Revert</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="218"/>
        <source>Forget network</source>
        <comment>Removes all information about the network(settings, passwords,...)</comment>
        <translation type="unfinished">Forget network</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="226"/>
        <source>Forget network?</source>
        <translation type="unfinished">Forget network?</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="227"/>
        <source>Do you really want to forget this network&apos;s settings?</source>
        <translation type="unfinished">Do you really want to forget this network&apos;s settings?</translation>
    </message>
</context>
<context>
    <name>PageYesNoSimple</name>
    <message>
        <location filename="../qml/PageYesNoSimple.qml" line="28"/>
        <source>Are You Sure?</source>
        <translation type="unfinished">Are You Sure?</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSimple.qml" line="60"/>
        <source>Yes</source>
        <translation type="unfinished">Yes</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSimple.qml" line="76"/>
        <source>No</source>
        <translation type="unfinished">No</translation>
    </message>
</context>
<context>
    <name>PageYesNoSwipe</name>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="29"/>
        <source>Are You Sure?</source>
        <translation type="unfinished">Are You Sure?</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="76"/>
        <source>Swipe to proceed</source>
        <translation type="unfinished">Swipe to proceed</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="97"/>
        <source>Yes</source>
        <translation type="unfinished">Yes</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="110"/>
        <source>No</source>
        <translation type="unfinished">No</translation>
    </message>
</context>
<context>
    <name>PageYesNoWithPicture</name>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="28"/>
        <source>Are You Sure?</source>
        <translation type="unfinished">Are You Sure?</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="176"/>
        <source>Swipe for a picture</source>
        <translation type="unfinished">Swipe for a picture</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="254"/>
        <source>Yes</source>
        <translation type="unfinished">Yes</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="267"/>
        <source>No</source>
        <translation type="unfinished">No</translation>
    </message>
</context>
<context>
    <name>PrusaPicturePictureButtonItem</name>
    <message>
        <location filename="../qml/PrusaPicturePictureButtonItem.qml" line="58"/>
        <source>Plugged in</source>
        <translation type="unfinished">Plugged in</translation>
    </message>
    <message>
        <location filename="../qml/PrusaPicturePictureButtonItem.qml" line="58"/>
        <source>Unplugged</source>
        <translation type="unfinished">Unplugged</translation>
    </message>
</context>
<context>
    <name>PrusaSwitch</name>
    <message>
        <location filename="../qml/PrusaSwitch.qml" line="112"/>
        <source>Off</source>
        <translation type="unfinished">Off</translation>
    </message>
    <message>
        <location filename="../qml/PrusaSwitch.qml" line="170"/>
        <source>On</source>
        <translation type="unfinished">On</translation>
    </message>
</context>
<context>
    <name>PrusaWaitOverlay</name>
    <message>
        <location filename="../qml/PrusaWaitOverlay.qml" line="74"/>
        <source>Please wait...</source>
        <comment>can be on multiple lines</comment>
        <translation type="unfinished">Please wait...</translation>
    </message>
</context>
<context>
    <name>SwipeSign</name>
    <message>
        <location filename="../qml/SwipeSign.qml" line="98"/>
        <source>Swipe to confirm</source>
        <translation type="unfinished">Swipe to confirm</translation>
    </message>
</context>
<context>
    <name>WarningText</name>
    <message>
        <location filename="../qml/WarningText.qml" line="45"/>
        <source>Must not be empty, only 0-9, a-z, A-Z, _ and - are allowed here!</source>
        <translation type="unfinished">Must not be empty, only 0-9, a-z, A-Z, _ and - are allowed here!</translation>
    </message>
</context>
<context>
    <name>WindowHeader</name>
    <message>
        <location filename="../PrusaComponents/Delegates/WindowHeader.qml" line="86"/>
        <source>back</source>
        <translation type="unfinished">back</translation>
    </message>
    <message>
        <location filename="../PrusaComponents/Delegates/WindowHeader.qml" line="99"/>
        <source>cancel</source>
        <translation type="unfinished">cancel</translation>
    </message>
    <message>
        <location filename="../PrusaComponents/Delegates/WindowHeader.qml" line="111"/>
        <source>close</source>
        <translation type="unfinished">close</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="50"/>
        <source>Prusa SL1 Touchscreen User Interface</source>
        <translation type="unfinished">Prusa SL1 Touchscreen User Interface</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="64"/>
        <location filename="../qml/main.qml" line="69"/>
        <source>Unknown</source>
        <translation type="unfinished">Unknown</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="65"/>
        <source>Activating</source>
        <translation type="unfinished">Activating</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="66"/>
        <source>Connected</source>
        <translation type="unfinished">Connected</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="67"/>
        <source>Deactivating</source>
        <translation type="unfinished">Deactivating</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="68"/>
        <source>Deactivated</source>
        <translation type="unfinished">Deactivated</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="277"/>
        <source>Notifications</source>
        <translation type="unfinished">Notifications</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="320"/>
        <source>Initializing...</source>
        <translation type="unfinished">Initializing...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="321"/>
        <source>The printer is initializing, please wait ...</source>
        <translation type="unfinished">The printer is initializing, please wait ...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="330"/>
        <source>MC Update</source>
        <translation type="unfinished">MC Update</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="331"/>
        <source>The Motion Controller firmware is being updated.

Please wait...</source>
        <translation type="unfinished">The Motion Controller firmware is being updated.

Please wait...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="493"/>
        <source>Turn Off?</source>
        <translation type="unfinished">Turn off?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="506"/>
        <source>Cancel?</source>
        <translation type="unfinished">Cancel</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="521"/>
        <source>Cancel the current print job?</source>
        <translation type="unfinished">Cancel the current print job?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="544"/>
        <source>Printer should not be turned off in this state.
Finish or cancel the current action and try again.</source>
        <translation type="unfinished">Printer cannot be turned off in this state. Finish or cancel the current action and try again.</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="555"/>
        <location filename="../qml/main.qml" line="579"/>
        <source>DEPRECATED PROJECTS</source>
        <translation type="unfinished">DEPRECATED PROJECTS</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="556"/>
        <source>Some incompatible projects were found, you can download them at 

http://%1/old-projects

Do you want to remove them?</source>
        <translation type="unfinished">Some incompatible projects were found, you can download them at 

http://%1/old-projects

Do you want to remove them?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="570"/>
        <location filename="../qml/main.qml" line="593"/>
        <source>&lt;printer IP&gt;</source>
        <translation type="unfinished">&lt;printer IP&gt;</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="580"/>
        <source>Some incompatible file were found, you can view them at http://%1/old-projects.

Would you like to remove them?</source>
        <translation type="unfinished">Some incompatible projects were found, you can download them at 

http://%1/old-projects.

Do you want to remove them?</translation>
    </message>
</context>
<context>
    <name>utils</name>
    <message>
        <location filename="../qml/utils.js" line="47"/>
        <source>Less than a minute</source>
        <translation type="unfinished">Less than a minute</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="50"/>
        <source>%n h</source>
        <comment>how many hours</comment>
        <translation type="unfinished">
            <numerusform>%n h</numerusform>
            <numerusform>%n h</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="51"/>
        <source>%n min</source>
        <comment>how many minutes</comment>
        <translation type="unfinished">
            <numerusform>%n min</numerusform>
            <numerusform>%n min</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="55"/>
        <source>%n hour(s)</source>
        <comment>how many hours</comment>
        <translation type="unfinished">
            <numerusform>%n hour</numerusform>
            <numerusform>%n hours</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="56"/>
        <source>%n minute(s)</source>
        <comment>how many minutes</comment>
        <translation type="unfinished">
            <numerusform>%n minute</numerusform>
            <numerusform>%n minutes</numerusform>
        </translation>
    </message>
</context>
</TS>
