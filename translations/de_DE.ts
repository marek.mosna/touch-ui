<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de-DE">
<context>
    <name>DelegateAddHiddenNetwork</name>
    <message>
        <location filename="../qml/DelegateAddHiddenNetwork.qml" line="91"/>
        <source>Add Hidden Network</source>
        <translation type="unfinished">Verstecktes Netzwerk hinzufügen</translation>
    </message>
</context>
<context>
    <name>DelegateAdminPlusMinus</name>
    <message>
        <location filename="../qml/DelegateAdminPlusMinus.qml" line="41"/>
        <source>N/A</source>
        <translation type="unfinished">N/V</translation>
    </message>
</context>
<context>
    <name>DelegateEthNetwork</name>
    <message>
        <location filename="../qml/DelegateEthNetwork.qml" line="110"/>
        <source>Plugged in</source>
        <translation type="unfinished">Eingesteckt</translation>
    </message>
    <message>
        <location filename="../qml/DelegateEthNetwork.qml" line="110"/>
        <source>Unplugged</source>
        <translation type="unfinished">Nicht angeschlossen</translation>
    </message>
</context>
<context>
    <name>DelegateRef</name>
    <message>
        <location filename="../qml/DelegateRef.qml" line="28"/>
        <source>N/A</source>
        <translation type="unfinished">N/V</translation>
    </message>
</context>
<context>
    <name>DelegateState</name>
    <message>
        <location filename="../qml/DelegateState.qml" line="38"/>
        <source>Network Info</source>
        <translation type="unfinished">Netzwerk Info</translation>
    </message>
</context>
<context>
    <name>DelegateWifiClientOnOff</name>
    <message>
        <location filename="../qml/DelegateWifiClientOnOff.qml" line="39"/>
        <source>Wi-Fi Client</source>
        <translation type="unfinished">Wi-Fi Client</translation>
    </message>
</context>
<context>
    <name>DelegateWifiNetwork</name>
    <message>
        <location filename="../qml/DelegateWifiNetwork.qml" line="106"/>
        <source>Forget network?</source>
        <translation type="unfinished">Netzwerk vergessen?</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWifiNetwork.qml" line="107"/>
        <source>Do you really want to forget this network&apos;s settings?</source>
        <translation type="unfinished">Möchten Sie die Einstellungen dieses Netzwerks wirklich vergessen?</translation>
    </message>
</context>
<context>
    <name>DelegateWifiOnOff</name>
    <message>
        <location filename="../qml/DelegateWifiOnOff.qml" line="36"/>
        <source>Wi-Fi</source>
        <translation type="unfinished">Wi-Fi</translation>
    </message>
</context>
<context>
    <name>DelegateWizardCheck</name>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="74"/>
        <source>Platform range</source>
        <translation type="unfinished">Plattform Bereich</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="75"/>
        <source>Platform home</source>
        <translation type="unfinished">Plattform Home</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="76"/>
        <source>Tank range</source>
        <translation type="unfinished">Tankbereich</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="77"/>
        <source>Tank home</source>
        <translation type="unfinished">Tank Home</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="78"/>
        <source>Display test</source>
        <translation type="unfinished">Display Test</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="79"/>
        <source>Printer calibration</source>
        <translation type="unfinished">Druckerkalibrierung</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="80"/>
        <source>Sound test</source>
        <translation type="unfinished">Ton-Test</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="81"/>
        <source>UV LED</source>
        <translation type="unfinished">UV LED</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="82"/>
        <source>UV LED and fans</source>
        <translation type="unfinished">UV LED und Lüfter</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="83"/>
        <source>Release foam</source>
        <translation type="unfinished">Schaumstoff lösen</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="84"/>
        <source>Make tank accessible</source>
        <translation type="unfinished">Tank zugänglich machen</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="85"/>
        <source>Resin sensor</source>
        <translation type="unfinished">Harzsensor</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="86"/>
        <source>Serial number</source>
        <translation type="unfinished">Seriennummer</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="87"/>
        <source>Temperature</source>
        <translation type="unfinished">Temperatur</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="88"/>
        <source>Tank calib. start</source>
        <translation type="unfinished">Tank kalib. starten</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="89"/>
        <location filename="../qml/DelegateWizardCheck.qml" line="120"/>
        <source>Tank level</source>
        <translation type="unfinished">Tankfüllstand</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="90"/>
        <source>Platform calibration</source>
        <translation type="unfinished">Plattformkalibrierung</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="91"/>
        <source>Tilt timming</source>
        <translation type="unfinished">Kipp-Timing</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="92"/>
        <source>Obtain system info</source>
        <translation type="unfinished">Systeminfo abrufen</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="93"/>
        <source>Obtain calibration info</source>
        <translation type="unfinished">Kalibrierungs-Info abrufen</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="94"/>
        <source>Erase projects</source>
        <translation type="unfinished">Projekte löschen</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="95"/>
        <source>Reset hostname</source>
        <translation type="unfinished">Hostnamen zurücksetzen</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="96"/>
        <source>Reset API key</source>
        <translation type="unfinished">API Key zurücksetzen</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="97"/>
        <source>Reset remote config</source>
        <translation type="unfinished">Remotekonfiguration zurücksetzen</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="98"/>
        <source>Reset HTTP digest</source>
        <translation type="unfinished">HTTP Digest zurücksetzen</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="99"/>
        <source>Reset Wi-Fi settings</source>
        <translation type="unfinished">Wi-Fi Einstellungen zurücksetzen</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="100"/>
        <source>Reset timezone</source>
        <translation type="unfinished">Zeitzone zurücksetzen</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="101"/>
        <source>Reset NTP state</source>
        <translation type="unfinished">NTP Status zurücksetzen</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="102"/>
        <source>Reset system locale</source>
        <translation type="unfinished">Systemgebietsschema zurücksetzen</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="103"/>
        <source>Clear UV calibration data</source>
        <translation type="unfinished">UV-Kalibrierungsdaten leeren</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="104"/>
        <source>Clear downloaded Slicer profiles</source>
        <translation type="unfinished">Heruntergeladene Slicer-Profile leeren</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="105"/>
        <source>Reset print configuration</source>
        <translation type="unfinished">Druckkonfiguration zurücksetzen</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="106"/>
        <source>Erase motion controller EEPROM</source>
        <translation type="unfinished">Motion Controller EEPROM löschen</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="107"/>
        <source>Reset homing profiles</source>
        <translation type="unfinished">Homing-Profile zurücksetzen</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="108"/>
        <source>Send printer data to MQTT</source>
        <translation type="unfinished">Druckerdaten an MQTT senden</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="109"/>
        <source>Disable factory mode</source>
        <translation type="unfinished">Werksmodus deaktivieren</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="110"/>
        <source>Moving printer to accept protective foam</source>
        <translation type="unfinished">Bewegen des Druckers zur Aufnahme des Schutzschaums</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="111"/>
        <source>Pressing protective foam</source>
        <translation type="unfinished">Pressen des Schutzschaums</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="112"/>
        <source>Disable ssh, serial</source>
        <translation type="unfinished">Ssh, seriell abschalten</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="113"/>
        <source>Check for UV calibrator</source>
        <translation type="unfinished">Prüfung auf UV-Kalibrator</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="114"/>
        <source>UV LED warmup</source>
        <translation type="unfinished">UV LED Aufwärmen</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="115"/>
        <source>UV calibrator placed</source>
        <translation type="unfinished">UV-Kalibrator angebracht</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="116"/>
        <source>Calibrate center</source>
        <translation type="unfinished">Zentrum kalibrieren</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="117"/>
        <source>Calibrate edge</source>
        <translation type="unfinished">Kante kalibrieren</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="118"/>
        <source>Apply calibration results</source>
        <translation type="unfinished">Kalibrierergebnisse anwenden</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="119"/>
        <source>Waiting for UV calibrator to be removed</source>
        <translation type="unfinished">Warte auf die Entnahme des UV-Kalibrators</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="121"/>
        <source>Reset UI settings</source>
        <translation type="unfinished">UI Einstellungen zurücksetzen</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="122"/>
        <source>Erase UV PWM settings</source>
        <translation type="unfinished">UV-PWM-Einstellungen löschen</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="123"/>
        <source>Reset selftest status</source>
        <translation type="unfinished">Selbstteststatus zurücksetzen</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="124"/>
        <source>Reset printer calibration status</source>
        <translation type="unfinished">Status der Druckerkalibrierung zurücksetzen</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="125"/>
        <source>Set new printer model</source>
        <translation type="unfinished">Neues Druckermodell einstellen</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="126"/>
        <source>Resetting hardware counters</source>
        <translation type="unfinished">Hardware-Zähler zurücksetzen</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="127"/>
        <source>Recording changes</source>
        <translation type="unfinished">Aufzeichnen der  Änderungen</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="128"/>
        <source>Unknown</source>
        <translation type="unfinished">Unbekannt</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="129"/>
        <source>Check ID:</source>
        <translation type="unfinished">Check ID:</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="141"/>
        <source>Waiting</source>
        <translation type="unfinished">Warte</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="142"/>
        <source>Running</source>
        <translation type="unfinished">Laufend</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="143"/>
        <source>Passed</source>
        <translation type="unfinished">Bestanden</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="144"/>
        <source>Failure</source>
        <translation type="unfinished">Fehler</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="145"/>
        <source>With Warning</source>
        <translation type="unfinished">Mit Warnungen</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="146"/>
        <source>User action pending</source>
        <translation type="unfinished">Benutzeraktion ausstehend</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="147"/>
        <source>Canceled</source>
        <translation type="unfinished">Abgebrochen</translation>
    </message>
</context>
<context>
    <name>ErrorPopup</name>
    <message>
        <location filename="../qml/ErrorPopup.qml" line="40"/>
        <source>Unknown error</source>
        <translation type="unfinished">Unbekannter Fehler</translation>
    </message>
    <message>
        <location filename="../qml/ErrorPopup.qml" line="55"/>
        <source>Understood</source>
        <translation type="unfinished">Verstanden</translation>
    </message>
</context>
<context>
    <name>ErrorcodesText</name>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="6"/>
        <source>Tilt homing failed, check its surroundings and repeat the action.</source>
        <translation type="unfinished">Referenzfahrt Kippen fehlgeschlagen, Umfeld prüfen und Aktion wiederholen.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="7"/>
        <source>Tower homing failed, make sure there is no obstacle in its path and repeat the action.</source>
        <translation type="unfinished">Referenzfahrt des Turms fehlgeschlagen, stellen Sie sicher, dass sich kein Hindernis im Weg befindet und wiederholen Sie die Aktion.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="8"/>
        <source>Moving the tower failed. Make sure there is no obstacle in its path and repeat the action.</source>
        <translation type="unfinished">Das Verfahren des Turms ist fehlgeschlagen. Stellen Sie sicher, dass sich kein Hindernis im Weg befindet und wiederholen Sie die Aktion.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="9"/>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="115"/>
        <source>Incorrect RPM reading of the %(failed_fans_text)s fan. Please check its wiring and connection.</source>
        <translation type="unfinished">Falsche Drehzahlanzeige des %(failed_fans_text)s Lüfters. Bitte überprüfen Sie dessen Verdrahtung und Anschluss.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="10"/>
        <source>Measured resin volume %(volume_ml)d ml is lower than required for this print. Refill the tank and restart the print.</source>
        <translation type="unfinished">Gemessenes Harzvolumen %(volume_ml)d ml ist niedriger als für diesen Druck erforderlich. Füllen Sie den Tank auf und starten Sie den Druck erneut.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="11"/>
        <source>Measured resin volume %(volume_ml)d ml is higher than required for this print. Make sure that the resin level does not exceed the 100% mark and restart the print.</source>
        <translation type="unfinished">Gemessenes Harzvolumen %(volume_ml)d ml ist höher als für diesen Druck erforderlich. Stellen Sie sicher, dass der Harzfüllstand die 100 %-Marke nicht überschreitet, und starten Sie den Druck erneut.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="12"/>
        <source>The printer is not calibrated. Please run the Wizard first.</source>
        <translation type="unfinished">Der Drucker ist nicht kalibriert. Bitte führen Sie zuerst den Assistenten aus.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="13"/>
        <source>Failed to reach the tower endstop, check that the tower motor is connected and repeat the action.</source>
        <translation type="unfinished">Der Turmendanschlag wurde nicht erreicht, prüfen Sie, ob der Turmmotor angeschlossen ist, und wiederholen Sie den Vorgang.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="14"/>
        <source>Failed to reach the tilt endstop, check that the cable is connected and repeat the action.</source>
        <translation type="unfinished">Der Kippendanschlag wurde nicht erreicht, prüfen Sie, ob das Kabel angeschlossen ist, und wiederholen Sie den Vorgang.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="15"/>
        <source>Tower axis check failed!

Current position: %(position_nm)d nm

Check if the ballscrew can move smoothly in its entire range.</source>
        <translation type="unfinished">Turmachsenprüfung fehlgeschlagen!

Aktuelle Position: %(position_nm)d nm

Prüfen Sie, ob sich der Kugelgewindetrieb in seinem gesamten Bereich leichtgängig bewegen kann.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="16"/>
        <source>Tilt axis check failed!

Current position: %(position)d steps

Check if the tilt can move smoothly in its entire range.</source>
        <translation type="unfinished">Kippachsenprüfung fehlgeschlagen!

Aktuelle Position: %(position)d Schritte

Prüfen Sie, ob sich die Kippachse in ihrem gesamten Bereich reibungslos bewegen kann.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="17"/>
        <source>Display test failed, check the connection between the display and the A64 board.</source>
        <translation type="unfinished">Displaytest fehlgeschlagen, überprüfen Sie die Verbindung zwischen dem Display und der A64-Platine.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="18"/>
        <source>Invalid tilt alignment position. Check the tilt mechanism and repeat the action.</source>
        <translation type="unfinished">Ungültige Kippausrichtungsposition. Überprüfen Sie den Mechanismus und wiederholen Sie den Vorgang.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="19"/>
        <source>RPM of %(fan)s not in range!

Check if the fan is connected correctly.

RPM data: %(rpm)s
Average: %(avg)s</source>
        <translation type="unfinished">Drehzahl des %(fan)s nicht im Bereich!

Prüfen Sie, ob der Lüfter richtig angeschlossen ist.

UPM-Daten: %(rpm)s
Durchschnitt: %(avg)s</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="20"/>
        <source>Tower not at the expected position.

Are the platform and tank mounted and secured correctly?</source>
        <translation type="unfinished">Turm nicht an der erwarteten Position.

Sind die Plattform und der Tank korrekt montiert und befestigt?</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="21"/>
        <source>Measuring the resin failed. Check the presence of the platform and the amount of resin in the tank.</source>
        <translation type="unfinished">Das Messen des Harzes ist fehlgeschlagen. Prüfen Sie das Vorhandensein der Plattform und die Menge des Harzes im Tank.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="22"/>
        <source>The %(sensor)s sensor failed. Check the wiring and connection.</source>
        <translation type="unfinished">Der %(sensor)s-Sensor ist ausgefallen. Überprüfen Sie die Verdrahtung und den Anschluss.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="23"/>
        <source>UV LED is overheating! Check whether the heatsink is installed correctly.</source>
        <translation type="unfinished">UV-LED überhitzt! Prüfen Sie, ob der Kühlkörper richtig montiert ist.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="24"/>
        <source>A64 temperature is too high. Measured: %(temperature).1f °C! Shutting down in 10 seconds...</source>
        <translation type="unfinished">A64 Temperatur ist zu hoch. Gemessen: %(temperature).1f °C! Abschaltung in 10 Sekunden...</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="25"/>
        <source>%(sensor)s not in range! Measured temperature: %(temperature).1f °C. Keep the printer out of direct sunlight at room temperature (18 - 32 °C).</source>
        <translation type="unfinished">%(sensor)s nicht im Bereich! Gemessene Temperatur: %(temperature).1f °C. Halten Sie den Drucker bei Raumtemperatur (18 - 32 °C) vor direkter Sonneneinstrahlung geschützt.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="26"/>
        <source>Reading of UV LED temperature has failed! This value is essential for the UV LED lifespan and printer safety. Please contact tech support! Current print job will be canceled.</source>
        <translation type="unfinished">Das Auslesen der UV-LED-Temperatur ist fehlgeschlagen! Dieser Wert ist wichtig für die Lebensdauer der UV-LED und die Sicherheit des Druckers. Bitte kontaktieren Sie den Technischen Support! Der aktuelle Druckauftrag wird abgebrochen.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="27"/>
        <source>Wrong revision of the Motion Controller (MC). Contact our support.</source>
        <translation type="unfinished">Wrong revision of the Motion Controller (MC). Contact our support.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="28"/>
        <source>The Motion Controller (MC) has encountered an unexpected error. Restart the printer.</source>
        <translation type="unfinished">Der Motion Controller (MC) ist auf einen unerwarteten Fehler gestoßen. Starten Sie den Drucker neu.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="29"/>
        <source>The resin sensor was not triggered. Check whether the tank and the platform are properly secured. Inspect the wiring of the sensor.</source>
        <translation type="unfinished">Der Harzsensor wurde nicht ausgelöst. Prüfen Sie, ob der Tank und die Plattform richtig gesichert sind. Überprüfen Sie die Verdrahtung des Sensors.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="30"/>
        <source>The printer is not UV calibrated. Connect the UV calibrator and complete the calibration.</source>
        <translation type="unfinished">Der Drucker ist nicht UV-kalibriert. Schließen Sie den UV-Kalibrator an und schließen Sie die Kalibrierung ab.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="31"/>
        <source>UV LED voltages differ too much. The LED module might be faulty. Contact our support.</source>
        <translation type="unfinished">UV-LED-Spannungen unterscheiden sich zu stark. Das LED-Modul könnte defekt sein. Kontaktieren Sie unseren Support.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="32"/>
        <source>Speaker test failed, check the connection and repeat the action.</source>
        <translation type="unfinished">Lautsprechertest fehlgeschlagen, überprüfen Sie die Verbindung und wiederholen Sie die Aktion.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="33"/>
        <source>The UV LED calibrator is not detected. Check the connection and try again.</source>
        <translation type="unfinished">Der UV-LED-Kalibrator wird nicht erkannt. Überprüfen Sie die Verbindung und versuchen Sie es erneut.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="34"/>
        <source>Cannot connect to the UV LED calibrator. Check the connection and try again.</source>
        <translation type="unfinished">Es kann keine Verbindung zum UV-LED-Kalibrator hergestellt werden. Überprüfen Sie die Verbindung und versuchen Sie es erneut.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="35"/>
        <source>Communication with the UV LED calibrator has failed. Check the connection and try again.</source>
        <translation type="unfinished">Kommunikation mit dem UV-LED-Kalibrator ist fehlgeschlagen. Überprüfen Sie die Verbindung und versuchen Sie es erneut.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="36"/>
        <source>The UV LED calibrator detected some light on a dark display. This means there is a light &apos;leak&apos; under the UV calibrator, or your display does not block the UV light enough. Check the UV calibrator placement on the screen or replace the exposure display.</source>
        <translation type="unfinished">Der UV-LED-Kalibrator hat etwas Licht auf einem dunklen Display erkannt. Dies bedeutet, dass es ein Licht-&quot;Leck&quot; unter dem UV-Kalibrator gibt, oder Ihr Display blockiert das UV-Licht nicht ausreichend. Überprüfen Sie die Platzierung des UV-Kalibrators auf dem Bildschirm oder tauschen Sie das Belichtungsdisplay aus.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="37"/>
        <source>The UV LED calibrator failed to read expected UV light intensity. Check the UV calibrator placement on the screen.</source>
        <translation type="unfinished">Der UV-LED-Kalibrator konnte die erwartete UV-Lichtintensität nicht ermitteln. Überprüfen Sie die Platzierung des UV-Kalibrators auf dem Bildschirm.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="38"/>
        <source>Unknown UV LED calibrator error code: %(code)d</source>
        <translation type="unfinished">Unbekannter UV-LED-Kalibrator-Fehlercode: %(code)d</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="39"/>
        <source>Requested intensity cannot be reached by min. allowed PWM.</source>
        <translation type="unfinished">Gewünschte Intensität kann mit der min. zulässigen PWM nicht erreicht werden.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="40"/>
        <source>Requested intensity cannot be reached by max. allowed PWM.</source>
        <translation type="unfinished">Gewünschte Intensität kann mit der max. zulässigen PWM nicht erreicht werden.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="41"/>
        <source>Correct settings were found, but the standard deviation
(%(found).1f) is greater than the allowed value (%(allowed).1f).
Verify the UV LED calibrator&apos;s position and calibration, then try again.</source>
        <translation type="unfinished">Es wurden korrekte Einstellungen gefunden, aber die Standardabweichung
(%(found).1f) ist größer als der erlaubte Wert (%(allowed).1f).
Überprüfen Sie die Position und Kalibrierung des UV-LED-Kalibrators und versuchen Sie es dann erneut.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="42"/>
        <source>Communication with the Booster board failed. Check the connection and restart the printer.</source>
        <translation type="unfinished">Kommunikation mit der Booster-Platine ist fehlgeschlagen. Überprüfen Sie die Verbindung und starten Sie den Drucker neu.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="43"/>
        <source>The UV LED panel is not detected. Check the connection.</source>
        <translation type="unfinished">Das UV-LED-Panel wird nicht erkannt. Überprüfen Sie den Anschluss.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="44"/>
        <source>A part of the LED panel is disconnected. Check the connection and the LED panel.</source>
        <translation type="unfinished">Ein Teil des LED-Panels ist abgeklemmt. Überprüfen Sie den Anschluss und das LED-Panel.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="45"/>
        <source>The printer model was not detected. Check the connection of the exposure display and restart the printer.</source>
        <translation type="unfinished">Das Druckermodell wurde nicht erkannt. Überprüfen Sie den Anschluss des Belichtungsdisplays und starten Sie den Drucker neu.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="46"/>
        <source>Cannot send factory config to the database (MQTT)! Check the network connection. Please, contact support.</source>
        <translation type="unfinished">Kann keine Werkskonfiguration an die Datenbank senden (MQTT)! Überprüfen Sie die Netzwerkverbindung. Bitte kontaktieren Sie den Support.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="47"/>
        <source>The printer is not connected to the internet. Check the connection in the Settings.</source>
        <translation type="unfinished">Der Drucker ist nicht mit dem Internet verbunden. Überprüfen Sie die Verbindung in den Einstellungen.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="48"/>
        <source>Connection to Prusa servers failed, please try again later.</source>
        <translation type="unfinished">Verbindung zu Prusa-Servern fehlgeschlagen, bitte versuchen Sie es später erneut.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="49"/>
        <source>The download failed. Check the connection to the internet and try again.</source>
        <translation type="unfinished">Der Download ist fehlgeschlagen. Überprüfen Sie die Verbindung zum Internet und versuchen Sie es erneut.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="50"/>
        <source>Please turn on the HTTP digest (which is the recommended security option) or update the API key. You can find it in Settings &gt; Network &gt; Login credentials.</source>
        <translation type="unfinished">Bitte schalten Sie den HTTP-Digest ein (die empfohlene Sicherheitsoption) oder aktualisieren Sie den API-Schlüssel. Sie finden ihn unter Einstellungen -&gt; Netzwerk -&gt; Anmeldedaten.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="51"/>
        <source>The printer uses HTTP digest security. Please enable it also in your slicer (recommended), or turn off this security option in the printer. You can find it in Settings &gt; Network &gt; Login credentials.</source>
        <translation type="unfinished">Der Drucker verwendet HTTP-Digest-Sicherheit. Bitte aktivieren Sie diese auch in Ihrem Slicer (empfohlen), oder deaktivieren Sie diese Sicherheitsoption im Drucker. Sie finden sie unter Einstellungen &gt; Netzwerk &gt; Anmeldedaten.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="52"/>
        <source>This request is not compatible with the Prusa remote API. See our documentation for more details.</source>
        <translation type="unfinished">Diese Anfrage ist nicht kompatibel mit der Prusa Remote-API. Siehe unsere Dokumentation für weitere Details.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="53"/>
        <source>No problem detected. You can continue using the printer.</source>
        <translation type="unfinished">Kein Problem erkannt. Sie können den Drucker weiter verwenden.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="54"/>
        <source>An unexpected error has occurred :-(.
If print job is in progress, it should be finished.
You can turn the printer off by pressing the front power button.
See the handbook to learn how to save a log file and send it to us.</source>
        <translation type="unfinished">Es ist ein unerwarteter Fehler aufgetreten :-(.
Wenn ein Druckauftrag in Bearbeitung ist, sollte er beendet sein.
Sie können den Drucker ausschalten, indem Sie die vordere Netztaste drücken.
Im Handbuch erfahren Sie, wie Sie eine Protokolldatei speichern und an uns senden können.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="55"/>
        <source>Image preloader did not finish successfully!</source>
        <translation type="unfinished">Der Bildvorlader wurde nicht erfolgreich beendet!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="56"/>
        <source>Opening the project failed, the file may be corrupted. Re-slice or re-export the project and try again.</source>
        <translation type="unfinished">Das Öffnen des Projekts ist fehlgeschlagen, die Datei ist möglicherweise beschädigt. Bitte slicen oder exportieren Sie das Projekt erneut und versuchen Sie es erneut.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="57"/>
        <source>Failed to read the configuration file. Try to reset the printer. If the problem persists, contact our support.</source>
        <translation type="unfinished">Die Konfigurationsdatei konnte nicht gelesen werden. Versuchen Sie, den Drucker zurückzusetzen. Wenn das Problem weiterhin besteht, wenden Sie sich an unseren Support.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="58"/>
        <source>Another action is already running. Finish this action directly using the printer&apos;s touchscreen.</source>
        <translation type="unfinished">Es läuft bereits eine andere Aktion. Beenden Sie diese Aktion direkt über den Touchscreen des Druckers.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="59"/>
        <source>Internal error (DBUS mapping failed), restart the printer. Contact support if the problem persists.</source>
        <translation type="unfinished">Interner Fehler (DBUS-Zuordnung fehlgeschlagen), starten Sie den Drucker neu. Wenden Sie sich an den Support, wenn das Problem weiterhin besteht.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="60"/>
        <source>Error, there is no file to reprint.</source>
        <translation type="unfinished">Fehler, es ist keine Datei zum Nachdrucken vorhanden.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="61"/>
        <source>The wizard did not finish successfully!</source>
        <translation type="unfinished">Der Assistent wurde nicht erfolgreich beendet!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="62"/>
        <source>The calibration did not finish successfully! Run the calibration again.</source>
        <translation type="unfinished">Die Kalibrierung wurde nicht erfolgreich beendet! Führen Sie die Kalibrierung erneut durch.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="63"/>
        <source>The automatic UV LED calibration did not finish successfully! Run the calibration again.</source>
        <translation type="unfinished">Die automatische UV-LED-Kalibrierung wurde nicht erfolgreich beendet! Führen Sie die Kalibrierung erneut durch.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="64"/>
        <source>UV intensity not set. Please run the UV calibration before starting a print.</source>
        <translation type="unfinished">UV-Intensität nicht eingestellt. Bitte führen Sie die UV-Kalibrierung durch, bevor Sie einen Druck starten.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="65"/>
        <source>Cannot set the update channel. Restart the printer and try again.</source>
        <translation type="unfinished">Der Aktualisierungskanal kann nicht gesetzt werden. Starten Sie den Drucker neu und versuchen Sie es erneut.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="66"/>
        <source>Cannot get the update channel. Restart the printer and try again.</source>
        <translation type="unfinished">Der Aktualisierungskanal kann nicht abgerufen werden. Starten Sie den Drucker neu und versuchen Sie es erneut.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="67"/>
        <source>The print job cancelled by the user.</source>
        <translation type="unfinished">Der Druckauftrag wurde vom Benutzer abgebrochen.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="68"/>
        <source>Internal memory is full. Delete some of your projects first.</source>
        <translation type="unfinished">Der interne Speicher ist voll. Löschen Sie zunächst einige Ihrer Projekte.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="69"/>
        <source>The admin menu is not available.</source>
        <translation type="unfinished">Das Admin-Menü ist nicht verfügbar.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="70"/>
        <source>Cannot find the selected file!</source>
        <translation type="unfinished">Ausgewählte Datei nicht gefunden!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="71"/>
        <source>File has an invalid extension! See the article for supported file extensions.</source>
        <translation type="unfinished">Datei hat eine ungültige Endung! Siehe den Artikel für unterstützte Dateiendungen.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="72"/>
        <source>File already exists! Delete it in the printer first and try again.</source>
        <translation type="unfinished">Datei ist bereits vorhanden! Löschen Sie sie zuerst im Drucker und versuchen Sie es erneut.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="73"/>
        <source>The project file is invalid!</source>
        <translation type="unfinished">Die Projektdatei ist ungültig!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="74"/>
        <source>This Wizard cannot be canceled, finish the steps first.</source>
        <translation type="unfinished">Dieser Assistent kann nicht abgebrochen werden, beenden Sie zuerst die Schritte.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="75"/>
        <source>Examples (any projects) are missing in the user storage. Redownload them from the &apos;Settings&apos; menu.</source>
        <translation type="unfinished">Es fehlen Beispiele ( irgendwelche Projekte) im Benutzerspeicher. Laden Sie sie über das Menü &apos;Einstellungen&apos; erneut herunter.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="76"/>
        <source>Failed to load fans and LEDs factory calibration.</source>
        <translation type="unfinished">Die Werks-Kalibrierung der Lüfter und der LEDs konnte nicht geladen werden.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="77"/>
        <source>Failed to serialize Wizard data. Restart the printer and try again.</source>
        <translation type="unfinished">Die Assistentendaten konnten nicht serialisiert werden. Starten Sie den Drucker neu und versuchen Sie es erneut.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="78"/>
        <source>Failed to save Wizard data. Restart the printer and try again.</source>
        <translation type="unfinished">Die Assistentendaten konnten nicht gespeichert werden. Starten Sie den Drucker neu und versuchen Sie es erneut.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="79"/>
        <source>Serial numbers in wrong format! A64: %(a64)s MC: %(mc)s Please contact tech support!</source>
        <translation type="unfinished">Seriennummern im falschen Format! A64: %(a64)s MC: %(mc)s Bitte kontaktieren Sie den technischen Support!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="80"/>
        <source>No USB storage present</source>
        <translation type="unfinished">Kein USB Speicher vorhanden</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="81"/>
        <source>Failed to change the log level (detail). Restart the printer and try again.</source>
        <translation type="unfinished">Das Ändern der Protokollebene (Detail) ist fehlgeschlagen. Starten Sie den Drucker neu und versuchen Sie es erneut.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="82"/>
        <source>Saving the new factory default value failed. Restart the printer and try again.</source>
        <translation type="unfinished">Das Speichern des neuen Werksstandardwerts ist fehlgeschlagen. Starten Sie den Drucker neu und versuchen Sie es erneut.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="83"/>
        <source>Error displaying test image.</source>
        <translation type="unfinished">Fehler beim Anzeigen des Testbildes.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="84"/>
        <source>No calibration data to show!</source>
        <translation type="unfinished">Keine Kalibrierdaten vorhanden!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="85"/>
        <source>Data is from unknown UV LED sensor!</source>
        <translation type="unfinished">Daten stammen von einem unbekannten UV-LED-Sensor!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="86"/>
        <source>The update of the firmware failed! Restart the printer and try again.</source>
        <translation type="unfinished">Das Update der Firmware ist fehlgeschlagen! Starten Sie den Drucker neu und versuchen Sie es erneut.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="87"/>
        <source>No display usage data to show</source>
        <translation type="unfinished">Keine Displaynutzungsdaten zum Anzeigen</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="88"/>
        <source>Failed to set hostname</source>
        <translation type="unfinished">Hostnamen einstellen fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="89"/>
        <source>Cannot import profile</source>
        <translation type="unfinished">Kann Profil nicht importieren</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="90"/>
        <source>Cannot export profile</source>
        <translation type="unfinished">Kann Profil nicht exportieren</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="91"/>
        <source>Opening the project failed. The file is possibly corrupted. Please re-slice or re-export the project and try again.</source>
        <translation type="unfinished">Das Öffnen des Projekts ist fehlgeschlagen. Die Datei ist möglicherweise beschädigt. Bitte slicen oder exportieren Sie das Projekt erneut und versuchen Sie es erneut.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="92"/>
        <source>The project must have at least one layer</source>
        <translation type="unfinished">Das Projekt muss mindestens eine Schicht haben</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="93"/>
        <source>Opening the project failed. The file is corrupted. Please re-slice or re-export the project and try again.</source>
        <translation type="unfinished">Das Öffnen des Projekts ist fehlgeschlagen. Die Datei ist beschädigt. Bitte slicen oder exportieren Sie das Projekt erneut und versuchen Sie es erneut.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="94"/>
        <source>Analysis of the project failed</source>
        <translation type="unfinished">Analyse des Projekts fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="95"/>
        <source>Calibration project is invalid</source>
        <translation type="unfinished">Kalibrierprojekt ist ungültig</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="96"/>
        <source>This project was prepared for a different printer</source>
        <translation type="unfinished">Dieses Projekt wurde für einen anderen Drucker vorbereitet</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="97"/>
        <source>Removing this project is not possible. The project is locked by a print job.</source>
        <translation type="unfinished">Das Entfernen dieses Projekts ist nicht möglich. Das Projekt ist durch einen Druckauftrag gesperrt.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="98"/>
        <source>The directory is not empty.</source>
        <translation type="unfinished">Das Verzeichnis ist nicht leer.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="99"/>
        <source>The language is not set. Go to Settings -&gt; Language &amp; Time -&gt; Set Language and pick preferred language.</source>
        <translation type="unfinished">Die Sprache ist nicht eingestellt. Gehen Sie zu Einstellungen -&gt; Sprache &amp; Zeit -&gt; Sprache einstellen und wählen Sie Ihre bevorzugte Sprache.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="100"/>
        <source>Exposure screen that is currently connected has already been used on this printer. This screen was last used for approximately %(counter_h)d hours.

If you do not want to use this screen: turn the printer off, replace the screen and turn the printer back on.</source>
        <translation type="unfinished">Das aktuell verbundene Belichtungsdisplay wurde bereits auf diesem Drucker verwendet. Dieses Display wurde zuletzt für ca. %(counter_h)d Stunden verwendet.

Wenn Sie dieses Display nicht mehr verwenden möchten: Schalten Sie den Drucker aus, tauschen Sie das Display aus und schalten Sie den Drucker wieder ein.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="101"/>
        <source>The printer has booted from an alternative slot due to failed boot attempts using the primary slot.
Update the printer with up-to-date firmware ASAP to recover the primary slot.
This usually happens after a failed update, or due to a hardware failure. Printer settings may have been reset.</source>
        <translation type="unfinished">Der Drucker hat von einem alternativen Bereich gebootet, da die Bootversuche über den primären Bootbereich fehlgeschlagen sind.
Aktualisieren Sie den Drucker so schnell wie möglich mit aktueller Firmware, um den primären Bootbereich wiederherzustellen.
Dies geschieht normalerweise nach einem fehlgeschlagenen Update oder aufgrund eines Hardwarefehlers. Die Druckereinstellungen wurden möglicherweise zurückgesetzt.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="102"/>
        <source>There is no warning</source>
        <translation type="unfinished">Keine Warnung vorhanden</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="103"/>
        <source>An unknown warning has occured. Restart the printer and try again. Contact our tech support if the problem persists.</source>
        <translation type="unfinished">Es ist eine unbekannte Warnung aufgetreten. Starten Sie den Drucker neu und versuchen Sie es erneut. Wenden Sie sich an unseren technischen Support, wenn das Problem weiterhin besteht.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="104"/>
        <source>The ambient temperature is too high, the print can continue, but it might fail.</source>
        <translation type="unfinished">Die Umgebungstemperatur ist zu hoch, der Druck kann zwar fortgesetzt werden, aber er könnte fehlschlagen.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="105"/>
        <source>The ambient temperature is too low, the print can continue, but it might fail.</source>
        <translation type="unfinished">Die Umgebungstemperatur ist zu niedrig, der Druck kann zwar fortgesetzt werden, aber er könnte fehlschlagen.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="106"/>
        <source>The internal memory is full, project cannot be copied. You can continue printing. However, you must not remove the USB drive during the print, otherwise the process will fail.</source>
        <translation type="unfinished">Der interne Speicher ist voll, das Projekt kann nicht kopiert werden. Sie können den Druckvorgang fortsetzen. Sie dürfen das USB-Laufwerk jedoch nicht während des Drucks entfernen, sonst schlägt der Vorgang fehl.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="107"/>
        <source>The model was sliced for a different printer model. Reslice the model using the correct settings.</source>
        <translation type="unfinished">Das Modell wurde für ein anderes Druckermodell gesliced. Slicen Sie das Modell mit den richtigen Einstellungen neu.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="108"/>
        <source>The amount of resin in the tank is not enough for the current project. Adding more resin will be required during the print.</source>
        <translation type="unfinished">Die Harzmenge im Tank ist für das aktuelle Projekt nicht ausreichend. Das Hinzufügen von mehr Harz wird während des Drucks erforderlich sein.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="109"/>
        <source>The print parameters are out of range of the printer, the system can try to fix the project. Proceed?</source>
        <translation type="unfinished">Die Druckparameter liegen außerhalb des Bereichs des Druckers, das System kann versuchen, das Projekt zu reparieren. Fortfahren?</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="110"/>
        <source>Per-partes print not available.</source>
        <translation type="unfinished">Per Partes Druck nicht verfügbar.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="111"/>
        <source>Print mask is missing.</source>
        <translation type="unfinished">Druckmaske fehlt.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="112"/>
        <source>Object was cropped because it does not fit the print area.</source>
        <translation type="unfinished">Objekt wurde beschnitten, da es nicht auf den Druckbereich passt.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="113"/>
        <source>The model was sliced for a different printer variant %(project_variant)s. Your printer variant is %(printer_variant)s.</source>
        <translation type="unfinished">Das Modell wurde für eine andere Druckervariante %(project_variant)s gesliced. Ihre Druckervariante ist %(printer_variant)s.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="114"/>
        <source>Measured resin volume is too low. The print can continue, however, a refill might be required.</source>
        <translation type="unfinished">Gemessene Harzmenge ist zu gering. Der Druck kann fortgesetzt werden, jedoch ist möglicherweise ein Nachfüllen erforderlich.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="116"/>
        <source>Incorrect RPM reading of the %(failed_fans_text)s fan. Please check its wiring. The print may continue, however, there&apos;s a risk of overheating.</source>
        <translation type="unfinished">Falsche Drehzahlanzeige des %(failed_fans_text)s Lüfters. Bitte überprüfen Sie dessen Verdrahtung. Der Druck kann fortgesetzt werden, es besteht jedoch die Gefahr einer Überhitzung.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="119"/>
        <source>TILT HOMING FAILED</source>
        <translation type="unfinished">KIPP-REFERENZIERUNG FEHLGESCHLAGEN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="120"/>
        <source>TOWER HOMING FAILED</source>
        <translation type="unfinished">TURM REFERENZFAHRT FEHLGESCHLAGEN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="121"/>
        <source>TOWER MOVING FAILED</source>
        <translation type="unfinished">TURMBEWEGUNG FEHLGESCHLAGEN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="122"/>
        <source>FAN FAILURE</source>
        <translation type="unfinished">LÜFTERFEHLER</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="123"/>
        <source>RESIN TOO LOW</source>
        <translation type="unfinished">HARZPEGEL ZU TIEF</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="124"/>
        <source>RESIN TOO HIGH</source>
        <translation type="unfinished">HARZPEGEL ZU HOCH</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="125"/>
        <source>CALIBRATION ERROR</source>
        <translation type="unfinished">KALIBRIERFEHLER</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="126"/>
        <source>TOWER ENDSTOP NOT REACHED</source>
        <translation type="unfinished">TURMENDANSCHLAG NICHT ERREICHT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="127"/>
        <source>TILT ENDSTOP NOT REACHED</source>
        <translation type="unfinished">KIPPENDANSCHLAG NICHT ERREICHT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="128"/>
        <source>TOWER AXIS CHECK FAILED</source>
        <translation type="unfinished">TURMACHSENPRÜFUNG FEHLGESCHLAGEN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="129"/>
        <source>TILT AXIS CHECK FAILED</source>
        <translation type="unfinished">KIPPACHSENPRÜFUNG FEHLGESCHLAGEN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="130"/>
        <source>DISPLAY TEST FAILED</source>
        <translation type="unfinished">DISPLAYTEST FEHLGESCHLAGEN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="131"/>
        <source>INVALID TILT ALIGN POSITION</source>
        <translation type="unfinished">UNGÜLTIGE KIPPAUSRICHTUNG POSITION</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="132"/>
        <source>FAN RPM OUT OF TEST RANGE</source>
        <translation type="unfinished">LÜFTERDREHZAHL AUSSERHALB DES TESTBEREICHS</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="133"/>
        <source>TOWER POSITION ERROR</source>
        <translation type="unfinished">TURMPOSITIONSFEHLER</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="134"/>
        <source>RESIN MEASURING FAILED</source>
        <translation type="unfinished">HARZMESSUNG FEHLGESCHLAGEN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="135"/>
        <source>TEMPERATURE SENSOR FAILED</source>
        <translation type="unfinished">TEMPERATURSENSOR DEFEKT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="136"/>
        <source>UVLED HEAT SINK FAILED</source>
        <translation type="unfinished">UVLED-KÜHLKÖRPER DEFEKT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="137"/>
        <source>A64 OVERHEAT</source>
        <translation type="unfinished">A64 ÜBERHITZUNG</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="138"/>
        <source>TEMPERATURE OUT OF RANGE</source>
        <translation type="unfinished">TEMPERATUR AUSSERHALB IHRES BEREICHS</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="139"/>
        <source>UV LED TEMP. ERROR</source>
        <translation type="unfinished">UV-LED TEMP.FEHLER</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="140"/>
        <source>MC WRONG REVISION</source>
        <translation type="unfinished">MC FALSCHE REVISION</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="141"/>
        <source>UNEXPECTED MC ERROR</source>
        <translation type="unfinished">UNERWARTETER MC-FEHLER</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="142"/>
        <source>RESIN SENSOR ERROR</source>
        <translation type="unfinished">HARZSENSORFEHLER</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="143"/>
        <source>PRINTER NOT UV CALIBRATED</source>
        <translation type="unfinished">DRUCKER NICHT UV-KALIBRIERT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="144"/>
        <source>UVLED VOLTAGE ERROR</source>
        <translation type="unfinished">UVLED-SPANNUNGSFEHLER</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="145"/>
        <source>SPEAKER TEST FAILED</source>
        <translation type="unfinished">LAUTSPRECHERTEST FEHLGESCHLAGEN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="146"/>
        <source>UV LED CALIBRATOR NOT DETECTED</source>
        <translation type="unfinished">UV LED KALIBRATOR NICHT ERKANNT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="147"/>
        <source>UV LED CALIBRATOR CONNECTION ERROR</source>
        <translation type="unfinished">UV LED KALIBRATOR VERBINDUNGSFEHLER</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="148"/>
        <source>UV LED CALIBRATOR LINK ERROR</source>
        <translation type="unfinished">UV LED KALIBRATOR LINK FEHLER</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="149"/>
        <source>UV LED CALIBRATOR ERROR</source>
        <translation type="unfinished">UV LED KALIBRATOR FEHLER</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="150"/>
        <source>UV LED CALIBRATOR READINGS ERROR</source>
        <translation type="unfinished">UV LED KALIBRATOR LESEFEHLER</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="151"/>
        <source>UV LED CALIBRATOR UNKNONW ERROR</source>
        <translation type="unfinished">UV LED KALIBRATOR UNBEKANNTER FEHLER</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="152"/>
        <source>UV INTENSITY TOO HIGH</source>
        <translation type="unfinished">UV-INTENSITÄT ZU HOCH</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="153"/>
        <source>UV INTENSITY TOO LOW</source>
        <translation type="unfinished">UV-INTENSITÄT ZU TIEF</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="154"/>
        <source>UV CALIBRATION ERROR</source>
        <translation type="unfinished">UV KALIBRIERFEHLER</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="155"/>
        <source>BOOSTER BOARD PROBLEM</source>
        <translation type="unfinished">BOOSTER BOARD PROBLEM</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="156"/>
        <source>Disconnected UV LED panel</source>
        <translation type="unfinished">Abgetrenntes UV-LED-Panel</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="157"/>
        <source>Broken UV LED panel</source>
        <translation type="unfinished">Defektes UV-LED-Panel</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="158"/>
        <source>Unknown printer model</source>
        <translation type="unfinished">Unbekanntes Druckermodell</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="159"/>
        <source>MQTT UPLOAD FAILED</source>
        <translation type="unfinished">MQTT UPLOAD FEHLGESCHLAGEN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="160"/>
        <source>NO INTERNET CONNECTION</source>
        <translation type="unfinished">KEIN INTERNETANSCHLUSS</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="161"/>
        <source>CONNECTION FAILED</source>
        <translation type="unfinished">VERBINDUNG FEHLGESCHLAGEN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="162"/>
        <source>DOWNLOAD FAILED</source>
        <translation type="unfinished">DOWNLOAD FEHLGESCHLAGEN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="163"/>
        <source>INVALID API KEY</source>
        <translation type="unfinished">UNGÜLTIGER API KEY</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="164"/>
        <source>UNAUTHORIZED</source>
        <translation type="unfinished">UNBERECHTIGT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="165"/>
        <source>REMOTE API ERROR</source>
        <translation type="unfinished">REMOTE API FEHLER</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="166"/>
        <source>PRINTER IS OK</source>
        <translation type="unfinished">DRUCKER IST OK</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="167"/>
        <source>UNEXPECTED ERROR</source>
        <translation type="unfinished">UNERWARTETER FEHLER</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="168"/>
        <source>PRELOAD FAILED</source>
        <translation type="unfinished">PRELOAD FEHLGESCHLAGEN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="169"/>
        <source>OPENING PROJECT FAILED</source>
        <translation type="unfinished">PROJEKT ÖFFNEN FEHLGESCHLAGEN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="170"/>
        <source>CONFIG FILE READ ERROR</source>
        <translation type="unfinished">KONFIGURATIONSDATEI LESEFEHLER</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="171"/>
        <source>PRINTER IS BUSY</source>
        <translation type="unfinished">DRUCKER IST BESCHÄFTIGT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="172"/>
        <source>INTERNAL ERROR</source>
        <translation type="unfinished">INTERNER FEHLER</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="173"/>
        <source>NO FILE TO REPRINT</source>
        <translation type="unfinished">KEINE DATEI ZUM NACHDRUCK</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="174"/>
        <source>WIZARD FAILED</source>
        <translation type="unfinished">ASSISTENT FEHLGESCHLAGEN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="175"/>
        <source>CALIBRATION FAILED</source>
        <translation type="unfinished">KALIBRIERUNG FEHLGESCHLAGEN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="176"/>
        <source>UV CALIBRATION FAILED</source>
        <translation type="unfinished">UV-KALIBRIERUNG FEHLGESCHLAGEN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="177"/>
        <source>UV INTENSITY ERROR</source>
        <translation type="unfinished">UV-INTENSITÄT FEHLER</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="178"/>
        <source>SETTING UPDATE CHANNEL FAILED</source>
        <translation type="unfinished">EINSTELLEN UPDATE-KANAL FEHLGESCHLAGEN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="179"/>
        <source>UPDATE CHANNEL FAILED</source>
        <translation type="unfinished">UPDATE-KANAL FEHLGESCHLAGEN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="180"/>
        <source>PRINT JOB CANCELLED</source>
        <translation type="unfinished">DRUCKAUFTRAG ABGEBROCHEN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="181"/>
        <source>INTERNAL MEMORY FULL</source>
        <translation type="unfinished">INTERNER SPEICHER VOLL</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="182"/>
        <source>ADMIN NOT AVAILABLE</source>
        <translation type="unfinished">ADMIN NICHT VERFÜGBAR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="183"/>
        <source>FILE NOT FOUND</source>
        <translation type="unfinished">DATEI NICHT GEFUNDEN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="184"/>
        <source>INVALID FILE EXTENSION</source>
        <translation type="unfinished">UNGÜLTIGE DATEIENDUNG</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="185"/>
        <source>FILE ALREADY EXISTS</source>
        <translation type="unfinished">DATEI EXISTIERT BEREITS</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="186"/>
        <source>INVALID PROJECT</source>
        <translation type="unfinished">UNGÜLTIGES PROJEKT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="187"/>
        <source>YOU SHALL NOT PASS</source>
        <translation type="unfinished">DU KANNST NICHT VORBEI</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="188"/>
        <source>PRINT EXAMPLES MISSING</source>
        <translation type="unfinished">DRUCKBEISPIELE FEHLEN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="189"/>
        <source>CALIBRATION LOAD FAILED</source>
        <translation type="unfinished">LADEN KALIBRIERUNG FEHLGESCHLAGEN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="190"/>
        <source>DATA PREPARATION FAILURE</source>
        <translation type="unfinished">DATENAUFBEREITUNGSFEHLER</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="191"/>
        <source>WIZARD DATA FAILURE</source>
        <translation type="unfinished">ASSISTENT DATENFEHLER</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="192"/>
        <source>SERIAL NUMBER ERROR</source>
        <translation type="unfinished">SERIENNUMMERNFEHLER</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="193"/>
        <source>USB DRIVE NOT DETECTED</source>
        <translation type="unfinished">USB-STICK NICHT ERKANNT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="194"/>
        <source>SETTING LOG DETAIL FAILED</source>
        <translation type="unfinished">EINSTELLEN PROTOKOLL-DETAIL FEHLGESCHLAGEN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="195"/>
        <source>DATA OVERWRITE FAILED</source>
        <translation type="unfinished">DATENÜBERSCHREIBUNG FEHLGESCHLAGEN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="196"/>
        <source>DISPLAY TEST ERROR</source>
        <translation type="unfinished">DISPLAY-TEST-FEHLER</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="197"/>
        <source>NO UV CALIBRATION DATA</source>
        <translation type="unfinished">KEINE UV-KALIBRIERDATEN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="198"/>
        <source>UV DATA EROR</source>
        <translation type="unfinished">UV-DATENFEHLER</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="199"/>
        <source>FIRMWARE UPDATE FAILED</source>
        <translation type="unfinished">FIRMWARE UPDATE FEHLGESCHLAGEN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="200"/>
        <source>Display usage error</source>
        <translation type="unfinished">Display-Verwendungsfehler</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="201"/>
        <source>HOSTNAME ERROR</source>
        <translation type="unfinished">HOSTNAMENFEHLER</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="202"/>
        <source>PROFILE IMPORT ERROR</source>
        <translation type="unfinished">PROFIL-IMPORT-FEHLER</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="203"/>
        <source>PROFILE EXPORT ERROR</source>
        <translation type="unfinished">PROFIL-EXPORT-FEHLER</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="204"/>
        <source>CANNOT READ PROJECT</source>
        <translation type="unfinished">KANN PROJEKT NICHT LESEN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="205"/>
        <source>NOT ENOUGHT LAYERS</source>
        <translation type="unfinished">NICHT GENUG SCHICHTEN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="206"/>
        <source>PROJECT IS CORRUPTED</source>
        <translation type="unfinished">PROJEKT IST BESCHÄDIGT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="207"/>
        <source>PROJECT ANALYSIS FAILED</source>
        <translation type="unfinished">PROJEKTANALYSE FEHLGESCHLAGEN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="208"/>
        <source>CALIBRATION PROJECT IS INVALID</source>
        <translation type="unfinished">KALIBRIERPROJEKT IST UNGÜLTIG</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="209"/>
        <source>WRONG PRINTER MODEL</source>
        <translation type="unfinished">FALSCHES DRUCKERMODELL</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="210"/>
        <source>CANNOT REMOVE PROJECT</source>
        <translation type="unfinished">KANN PROJEKT NICHT ENTFERNEN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="211"/>
        <source>DIRECTORY NOT EMPTY</source>
        <translation type="unfinished">VERZEICHNIS NICHT LEER</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="212"/>
        <source>LANGUAGE NOT SET</source>
        <translation type="unfinished">SPRACHE NICHT GESETZT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="213"/>
        <source>OLD EXPO PANEL</source>
        <translation type="unfinished">ALTES EXPO PANEL</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="214"/>
        <source>BOOTED SLOT CHANGED</source>
        <translation type="unfinished">BOOTBEREICH GEÄNDERT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="215"/>
        <source>NO WARNING</source>
        <translation type="unfinished">KEINE WARNUNG</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="216"/>
        <source>UNKNOWN WARNING</source>
        <translation type="unfinished">UNBEKANNTE WARNUNG</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="217"/>
        <source>AMBIENT TEMP. TOO HIGH</source>
        <translation type="unfinished">UMGEBUNGTEMP.ZU HOCH</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="218"/>
        <source>AMBIENT TEMP. TOO LOW</source>
        <translation type="unfinished">UMGEBUNGTEMP.ZU TIEF</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="219"/>
        <source>CAN&apos;T COPY PROJECT</source>
        <translation type="unfinished">KANN PROJEKT NICHT KOPIEREN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="220"/>
        <source>INCORRECT PRINTER MODEL</source>
        <translation type="unfinished">FALSCHES DRUCKERMODELL</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="221"/>
        <source>NOT ENOUGH RESIN</source>
        <translation type="unfinished">NICHT GENUG HARZ</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="222"/>
        <source>PARAMETERS OUT OF RANGE</source>
        <translation type="unfinished">PARAMETER AUSSERHALB IHRES BEREICHS</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="223"/>
        <source>PERPARTES NOAVAIL WARNING</source>
        <translation type="unfinished">PERPARTES N.V. WARNUNG</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="224"/>
        <source>MASK NOAVAIL WARNING</source>
        <translation type="unfinished">MASKE NOAVAIL-WARNUNG</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="225"/>
        <source>OBJECT CROPPED WARNING</source>
        <translation type="unfinished">OBJEKT ABGESCHNITTEN WARNUNG</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="226"/>
        <source>PRINTER VARIANT MISMATCH WARNING</source>
        <translation type="unfinished">WARNUNG DRUCKERVARIANTE PASST NICHT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="227"/>
        <source>RESIN LOW</source>
        <translation type="unfinished">HARZ NIEDRIG</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="228"/>
        <source>FAN WARNING</source>
        <translation type="unfinished">LÜFTERWARNUNG</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="229"/>
        <source>EXPECT OVERHEATING</source>
        <translation type="unfinished">ÜBERHITZUNG ERWARTET</translation>
    </message>
</context>
<context>
    <name>NotificationProgressDelegate</name>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="89"/>
        <source>Error: %1</source>
        <translation type="unfinished">Fehler: %1</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="99"/>
        <source>Storage full</source>
        <translation type="unfinished">Speicher voll</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="101"/>
        <source>File not found</source>
        <translation type="unfinished">Datei nicht gefunden</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="103"/>
        <source>Already exists</source>
        <translation type="unfinished">Existiert bereits</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="105"/>
        <source>Invalid extension</source>
        <translation type="unfinished">Ungültige Endung</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="107"/>
        <source>Can&apos;t read</source>
        <translation type="unfinished">Nicht lesbar</translation>
    </message>
</context>
<context>
    <name>PageAPSettings</name>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="34"/>
        <source>AP Settings</source>
        <translation type="unfinished">AP Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="51"/>
        <source>(unchanged)</source>
        <translation type="unfinished">(unverändert)</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="68"/>
        <source>Stop AP</source>
        <translation type="unfinished">AP stoppen</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="136"/>
        <source>State:</source>
        <translation type="unfinished">Staat:</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="147"/>
        <source>Inactive</source>
        <translation type="unfinished">Inaktiv</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="151"/>
        <source>SSID</source>
        <translation type="unfinished">SSID</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="173"/>
        <source>Password</source>
        <translation type="unfinished">Kennwort</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="214"/>
        <source>Show Password</source>
        <translation type="unfinished">Zeige Kennwort</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="230"/>
        <source>Security</source>
        <translation type="unfinished">Sicherheit</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="245"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation type="unfinished">PSK muss mindestens 8 Zeichen lang sein.</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="251"/>
        <source>SSID must not be empty</source>
        <translation type="unfinished">SSID darf nicht leer sein</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="263"/>
        <source>Start AP</source>
        <translation type="unfinished">AP starten</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="294"/>
        <source>Swipe for QRCode</source>
        <translation type="unfinished">Wischen für QR-Code</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="372"/>
        <source>Swipe for settings</source>
        <translation type="unfinished">Wischen für Einstellungen</translation>
    </message>
</context>
<context>
    <name>PageAbout</name>
    <message>
        <location filename="../qml/PageAbout.qml" line="22"/>
        <source>About Us</source>
        <translation type="unfinished">Über uns</translation>
    </message>
    <message>
        <location filename="../qml/PageAbout.qml" line="25"/>
        <source>To find out more about us please scan the QR code or use the link below:</source>
        <translation type="unfinished">Um mehr über uns zu erfahren, scannen Sie bitte den QR-Code oder nutzen Sie den untenstehenden Link:</translation>
    </message>
    <message>
        <location filename="../qml/PageAbout.qml" line="29"/>
        <source>Prusa Research a.s.</source>
        <translation type="unfinished">Prusa Research a.s.</translation>
    </message>
</context>
<context>
    <name>PageAddWifiNetwork</name>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="31"/>
        <source>Add Hidden Network</source>
        <translation type="unfinished">Verstecktes Netzwerk hinzufügen</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="62"/>
        <source>SSID</source>
        <translation type="unfinished">SSID</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="84"/>
        <source>PASS</source>
        <translation type="unfinished">PASS</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="129"/>
        <source>Show Password</source>
        <translation type="unfinished">Zeige Kennwort</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="147"/>
        <source>Security</source>
        <translation type="unfinished">Sicherheit</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="163"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation type="unfinished">PSK muss mindestens 8 Zeichen lang sein.</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="169"/>
        <source>SSID must not be empty.</source>
        <translation type="unfinished">SSID darf nicht leer sein.</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="182"/>
        <source>Connect</source>
        <translation type="unfinished">Verbunden</translation>
    </message>
</context>
<context>
    <name>PageAdminAPI</name>
    <message>
        <location filename="../qml/PageAdminAPI.qml" line="36"/>
        <source>Admin API</source>
        <translation type="unfinished">Admin API</translation>
    </message>
</context>
<context>
    <name>PageAskForPassword</name>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="66"/>
        <source>Please, enter the correct password for network &quot;%1&quot;</source>
        <translation type="unfinished">Bitte geben Sie das richtige Passwort für das Netzwerk &quot;%1&quot; ein</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="82"/>
        <source>Password</source>
        <translation type="unfinished">Kennwort</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="92"/>
        <source>(unchanged)</source>
        <translation type="unfinished">(unverändert)</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="128"/>
        <source>Show Password</source>
        <translation type="unfinished">Zeige Kennwort</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="154"/>
        <source>Cancel</source>
        <translation type="unfinished">Abbrechen</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="166"/>
        <source>Ok</source>
        <translation type="unfinished">Ok</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="193"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation type="unfinished">PSK muss mindestens 8 Zeichen lang sein.</translation>
    </message>
</context>
<context>
    <name>PageBasicWizard</name>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="34"/>
        <source>Wizard</source>
        <translation type="unfinished">Assistent</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="55"/>
        <source>Are you sure?</source>
        <translation type="unfinished">Sind Sie sicher?</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="56"/>
        <source>Do you really want to cancel the wizard?</source>
        <translation type="unfinished">Wollen Sie den Assistenten wirklich abbrechen?</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="58"/>
        <source>The machine will not work without a completed wizard procedure.</source>
        <translation type="unfinished">Das Gerät funktioniert nicht ohne einen abgeschlossenen Assistentenvorgang.</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="106"/>
        <source>Can you see the company logo on the exposure display through the cover?</source>
        <translation type="unfinished">Können Sie das Firmenlogo auf dem Belichtungsdisplay durch die orangefarbene Abdeckung sehen?</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="108"/>
        <source>Tip: If you can&apos;t see the logo clearly, try placing a sheet of paper onto the screen.</source>
        <translation type="unfinished">Tipp: Wenn Sie das Logo nicht deutlich sehen können, versuchen Sie, ein Blatt Papier auf den Bildschirm zu legen.</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="110"/>
        <source>Once you place the paper inside the printer, do not forget to CLOSE THE COVER!</source>
        <translation type="unfinished">Sobald Sie das Papier in den Drucker eingelegt haben, vergessen Sie nicht, die ABDECKUNG zu SCHLIEßEN!</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="120"/>
        <source>Carefully peel off the protective sticker from the exposition display.</source>
        <translation type="unfinished">Ziehen Sie die Schutz-Aufkleber vorsichtig vom Belichtungsdisplay ab.</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="129"/>
        <source>Wizard finished sucessfuly!</source>
        <translation type="unfinished">Assistent erfolgreich beendet!</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="137"/>
        <source>Wizard failed</source>
        <translation type="unfinished">Assistent fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="145"/>
        <source>Wizard canceled</source>
        <translation type="unfinished">Assistent abgebrochen</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="153"/>
        <source>Wizard stopped due to a problem, retry?</source>
        <translation type="unfinished">Assistent wurde aufgrund eines Problems angehalten, erneut versuchen?</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="162"/>
        <source>Cover</source>
        <translation type="unfinished">Abdeckung</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="163"/>
        <source>Close the cover.</source>
        <translation type="unfinished">Schließen Sie die Abdeckung.</translation>
    </message>
</context>
<context>
    <name>PageCalibrationTilt</name>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="28"/>
        <source>Tank Movement</source>
        <translation type="unfinished">Tankbewegung</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="82"/>
        <source>Move Up</source>
        <translation type="unfinished">Bewege Hoch</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="91"/>
        <source>Move Down</source>
        <translation type="unfinished">Bewege Runter</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="105"/>
        <source>Tilt position:</source>
        <translation type="unfinished">Kipp Position:</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="116"/>
        <source>Done</source>
        <translation type="unfinished">Beendet</translation>
    </message>
</context>
<context>
    <name>PageCalibrationWizard</name>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="31"/>
        <source>Printer Calibration</source>
        <translation type="unfinished">Druckerkalibrierung</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="49"/>
        <source>If the platform is not yet inserted, insert it according to the picture at 0° degrees angle and secure it with the black knob.</source>
        <translation type="unfinished">Wenn die Plattform noch nicht eingesetzt ist, setzen Sie sie gemäß Abbildung im Winkel von 0° ein und sichern Sie sie mit dem schwarzen Knopf.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="60"/>
        <source>Loosen the small screw on the cantilever with an allen key. Be careful not to unscrew it completely.</source>
        <translation type="unfinished">Lösen Sie die kleine Schraube am Kragarm mit einem Innensechskantschlüssel. Achten Sie darauf, dass Sie ihn nicht vollständig abschrauben.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="62"/>
        <source>Some SL1 printers may have two screws - see the handbook for more information.</source>
        <translation type="unfinished">Einige SL1-Drucker können mit zwei Schrauben ausgestattet sein - weitere Informationen finden Sie im Handbuch.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="75"/>
        <source>Unscrew the tank, rotate it by 90° and place it flat across the tilt bed. Remove the tank screws completely.</source>
        <translation type="unfinished">Lösen Sie den Tank, drehen Sie ihn um 90° und legen Sie ihn flach über die Kippfläche. Entfernen Sie die Tankschrauben vollständig.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="84"/>
        <source>In the next step, move the tilt bed up/down until it is in direct contact with the resin tank. The tilt bed and tank have to be aligned in a perfect line.</source>
        <translation type="unfinished">Bewegen Sie im nächsten Schritt das Kippbett nach oben/unten, bis es in direktem Kontakt mit dem Harztank steht. Das Kippbett und der Tank müssen in einer perfekten Linie ausgerichtet sein.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="96"/>
        <source>Set the tilt bed against the resin tank</source>
        <translation type="unfinished">Setzen Sie das Kippbett gegen den Harztank</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="105"/>
        <source>Make sure that the platfom, tank and tilt bed are PERFECTLY clean.</source>
        <translation type="unfinished">Vergewissern Sie sich, dass Plattform, Tank und Kippvorrichtung PERFEKT sauber sind.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="107"/>
        <source>The image is for illustration purposes only.</source>
        <translation type="unfinished">Das Bild ist nur zur Veranschaulichung.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="116"/>
        <source>Return the tank to the original position and secure it with tank screws. Make sure that you tighten both screws evenly and with the same amount of force.</source>
        <translation type="unfinished">Bringen Sie den Tank wieder in die Ausgangsposition und sichern Sie ihn mit Tankschrauben. Achten Sie darauf, dass Sie beide Schrauben gleichmäßig und mit gleicher Kraft anziehen.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="125"/>
        <source>Check whether the platform is properly secured with the black knob(hold it in place and tighten the knob if needed).</source>
        <translation type="unfinished">Überprüfen Sie, ob die Plattform mit dem schwarzen Knopf richtig gesichert ist (halten Sie ihn fest und ziehen Sie den Knopf bei Bedarf fest).</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="127"/>
        <source>Do not rotate the platform. It should be positioned according to the picture.</source>
        <translation type="unfinished">Drehen Sie die Plattform nicht. Sie sollte entsprechend der Abbildung positioniert werden.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="141"/>
        <source>Close the cover.</source>
        <translation type="unfinished">Schließen Sie die Abdeckung.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="155"/>
        <source>Adjust the platform so it is aligned with the exposition display.</source>
        <translation type="unfinished">Stellen Sie die Plattform so ein, dass sie mit der Belichtungsdisplay ausgerichtet ist.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="157"/>
        <source>Front edges of the platform and exposition display need to be parallel.</source>
        <translation type="unfinished">Die Vorderkanten der Plattform und des Belichtungsdisplays müssen parallel sein.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="170"/>
        <source>Hold the platform still with one hand and apply a slight downward force on it, so it maintains good contact with the screen.


Next, use an Allen key to tighten the screw on the cantilever.

Then release the platform.</source>
        <translation type="unfinished">Halten Sie die Plattform mit einer Hand ruhig und üben Sie eine leichte Kraft nach unten aus, damit sie einen guten Kontakt mit dem Bildschirm behält.


Ziehen Sie dann die Schraube am Ausleger mit einem Innensechskantschlüssel fest.

Lassen Sie dann die Plattform los.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="173"/>
        <source>Tighten the small screw on the cantilever with an allen key.</source>
        <translation type="unfinished">Ziehen Sie die kleine Schraube am Ausleger mit einem Innensechskantschlüssel fest.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="175"/>
        <source>Some SL1 printers may have two screws - tighten them evenly, little by little. See the handbook for more information.</source>
        <translation type="unfinished">Einige SL1-Drucker können zwei Schrauben haben - ziehen Sie sie gleichmäßig und nach und nach an. Weitere Informationen finden Sie im Handbuch.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="190"/>
        <source>Tilt settings for Prusa Slicer:</source>
        <translation type="unfinished">Kipp-Einstellungen für PrusaSlicer:</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="192"/>
        <source>Tilt time fast: %1 s</source>
        <translation type="unfinished">Kippzeit schnell: %1 s</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="194"/>
        <source>Tilt time slow: %1 s</source>
        <translation type="unfinished">Kippzeit langsam: %1 s</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="196"/>
        <source>Area fill: %1 %</source>
        <translation type="unfinished">Breichsfüllung: %1 %</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="198"/>
        <source>All done, happy printing!</source>
        <translation type="unfinished">Alles fertig, viel Spaß beim Drucken!</translation>
    </message>
</context>
<context>
    <name>PageChange</name>
    <message>
        <location filename="../qml/PageChange.qml" line="36"/>
        <source>Print Settings</source>
        <translation type="unfinished">Druckeinstellungen</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="64"/>
        <source>Exposure</source>
        <translation type="unfinished">Belichtung</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="81"/>
        <location filename="../qml/PageChange.qml" line="107"/>
        <location filename="../qml/PageChange.qml" line="129"/>
        <source>second</source>
        <translation type="unfinished">Sekunde</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="87"/>
        <source>Exposure Time Incr.</source>
        <translation type="unfinished">Belichtungszeit inkr.</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="112"/>
        <source>First Layer Expo.</source>
        <translation type="unfinished">Erste Schicht Bel.</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="134"/>
        <source>Print Profile</source>
        <translation type="unfinished">Druckprofil</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="145"/>
        <source>Faster</source>
        <comment>Default print profile. Printer behaves as before.</comment>
        <extracomment>Default print profile. Printer behaves as before.</extracomment>
        <translation type="unfinished">Schneller</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="147"/>
        <source>Slower</source>
        <comment>Safe print profile with slow tilts and pause before each exposure.</comment>
        <extracomment>Safe print profile with slow tilts and pause before each exposure.</extracomment>
        <translation type="unfinished">Langsamer</translation>
    </message>
</context>
<context>
    <name>PageConfirm</name>
    <message>
        <location filename="../qml/PageConfirm.qml" line="120"/>
        <source>Swipe for a picture</source>
        <translation type="unfinished">Wischen für ein Bild</translation>
    </message>
    <message>
        <location filename="../qml/PageConfirm.qml" line="187"/>
        <source>Continue</source>
        <translation type="unfinished">Fortfahren</translation>
    </message>
</context>
<context>
    <name>PageConnectHiddenNetwork</name>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="34"/>
        <source>Hidden Network</source>
        <translation type="unfinished">verstecktes Netzwerk</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="62"/>
        <source>SSID</source>
        <translation type="unfinished">SSID</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="78"/>
        <source>Password</source>
        <translation type="unfinished">Kennwort</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="121"/>
        <source>Show Password</source>
        <translation type="unfinished">Zeige Kennwort</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="128"/>
        <source>Security</source>
        <translation type="unfinished">Sicherheit</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="145"/>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="153"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation type="unfinished">PSK muss mindestens 8 Zeichen lang sein.</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="191"/>
        <source>Working...</source>
        <translation type="unfinished">Arbeite...</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="202"/>
        <source>Connection to %1 failed.</source>
        <translation type="unfinished">Verbindung zum %1 fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="213"/>
        <source>Connected to %1</source>
        <translation type="unfinished">Mit dem %1 verbunden</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="241"/>
        <source>Start AP</source>
        <translation type="unfinished">AP starten</translation>
    </message>
</context>
<context>
    <name>PageContinue</name>
    <message>
        <location filename="../qml/PageContinue.qml" line="27"/>
        <location filename="../qml/PageContinue.qml" line="59"/>
        <source>Continue</source>
        <translation type="unfinished">Fortfahren</translation>
    </message>
</context>
<context>
    <name>PageCoolingDown</name>
    <message>
        <location filename="../qml/PageCoolingDown.qml" line="27"/>
        <location filename="../qml/PageCoolingDown.qml" line="37"/>
        <source>UV LED OVERHEAT!</source>
        <translation type="unfinished">UV LED ÜBERHITZUNG!</translation>
    </message>
    <message>
        <location filename="../qml/PageCoolingDown.qml" line="40"/>
        <source>Cooling down</source>
        <translation type="unfinished">Kühle ab</translation>
    </message>
    <message>
        <location filename="../qml/PageCoolingDown.qml" line="40"/>
        <source>Temperature is %1 C</source>
        <translation type="unfinished">Temperatur ist %1 C</translation>
    </message>
</context>
<context>
    <name>PageDisplayTest</name>
    <message>
        <location filename="../qml/PageDisplayTest.qml" line="31"/>
        <source>Display Test</source>
        <translation type="unfinished">Display Test</translation>
    </message>
</context>
<context>
    <name>PageDisplaytestWizard</name>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="31"/>
        <source>Display Test</source>
        <translation type="unfinished">Display Test</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="47"/>
        <source>Welcome to the display wizard.</source>
        <translation type="unfinished">Willkommen beim Display-Assistenten.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="49"/>
        <source>This procedure will help you make sure that your exposure display is working correctly.</source>
        <translation type="unfinished">Mit diesem Verfahren können Sie sicherstellen, dass Ihr Belichtungsdisplay korrekt funktioniert.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="57"/>
        <source>Please unscrew and remove the resin tank.</source>
        <translation type="unfinished">Bitte lösen und entfernen Sie den Harztank.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="66"/>
        <source>Loosen the black knob and remove the platform.</source>
        <translation type="unfinished">Lösen Sie den schwarzen Dreh-Knopf und entfernen Sie die Plattform.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="75"/>
        <source>Close the cover.</source>
        <translation type="unfinished">Schließen Sie die Abdeckung.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="88"/>
        <source>All done, happy printing!</source>
        <translation type="unfinished">Alles fertig, viel Spaß beim Drucken!</translation>
    </message>
</context>
<context>
    <name>PageDowngradeWizard</name>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="31"/>
        <source>Hardware Downgrade</source>
        <translation type="unfinished">Hardware Downgrade</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="45"/>
        <source>SL1 components detected (downgrade from SL1S).</source>
        <translation type="unfinished">SL1-Komponenten erkannt (Downgrade von SL1S).</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="46"/>
        <source>To complete the downgrade procedure, printer needs to clear the configuration and reboot.</source>
        <translation type="unfinished">Um den Downgrade-Vorgang abzuschließen, muss der Drucker die Konfiguration löschen und neu starten.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="47"/>
        <source>Proceed?</source>
        <translation type="unfinished">Fortfahren?</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="56"/>
        <source>The printer will power off now.</source>
        <translation type="unfinished">Der Drucker wird nun ausgeschaltet.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="57"/>
        <source>Reassemble SL1S components and power on the printer. This will restore the original state.</source>
        <translation type="unfinished">Bauen Sie die SL1S-Komponenten wieder zusammen und schalten Sie den Drucker ein. Dadurch wird der ursprüngliche Zustand wiederhergestellt.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="66"/>
        <source>Please note that downgrading is not supported. 

Downgrading your printer will erase your UV calibration and your printer will not work properly. 

You will need to recalibrate it using an external UV calibrator.</source>
        <translation type="unfinished">Bitte beachten Sie, dass ein Downgrade nicht unterstützt wird. 

Durch ein Downgrade wird die UV-Kalibrierung Ihres Druckers gelöscht und Ihr Drucker funktioniert nicht mehr ordnungsgemäß. 

Sie müssen ihn mithilfe eines externen UV-Kalibrators neu kalibrieren.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="74"/>
        <source>Current configuration is going to be cleared now.</source>
        <translation type="unfinished">Die aktuelle Konfiguration wird nun gelöscht.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="75"/>
        <source>The printer will ask for the inital setup after reboot.</source>
        <translation type="unfinished">Der Drucker fragt nach dem Neustart nach der Ersteinrichtung.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="84"/>
        <source>Use only the metal resin tank supplied. Using the different resin tank may cause resin to spill and damage your printer!</source>
        <translation type="unfinished">Verwenden Sie nur den mitgelieferten Metall-Harztank. Die Verwendung anderer Harzbehälter kann zum Auslaufen des Harzes führen und Ihren Drucker beschädigen!</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="93"/>
        <source>Only use the platform supplied. Using a different platform may cause resin to spill and damage your printer!</source>
        <translation type="unfinished">Verwenden Sie nur die mitgelieferte Plattform. Die Verwendung einer anderen Plattform kann zum Auslaufen des Harzes führen und Ihren Drucker beschädigen!</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="101"/>
        <source>Downgrade done. In the next step, the printer will be restarted.</source>
        <translation type="unfinished">Das Downgrade ist abgeschlossen. Im nächsten Schritt wird der Drucker neu gestartet.</translation>
    </message>
</context>
<context>
    <name>PageDownloadingExamples</name>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="10"/>
        <source>Examples</source>
        <translation type="unfinished">Beispiele</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="54"/>
        <source>Downloading examples...</source>
        <translation type="unfinished">Lade Beispiele herunter...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="122"/>
        <source>Initializing...</source>
        <translation type="unfinished">Initialisierung...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="123"/>
        <source>Downloading ...</source>
        <translation type="unfinished">Lade herunter...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="124"/>
        <source>Copying...</source>
        <translation type="unfinished">Kopiere...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="125"/>
        <source>Unpacking...</source>
        <translation type="unfinished">Auspacken...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="126"/>
        <source>Done.</source>
        <translation type="unfinished">Erledigt.</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="127"/>
        <source>Cleanup...</source>
        <translation type="unfinished">Aufräumen...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="128"/>
        <source>Failure.</source>
        <translation type="unfinished">Fehler.</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="131"/>
        <source>Unknown state.</source>
        <translation type="unfinished">Unbekannter Zustand.</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="139"/>
        <source>View Examples</source>
        <translation type="unfinished">Beispiele zeigen</translation>
    </message>
</context>
<context>
    <name>PageError</name>
    <message>
        <location filename="../qml/PageError.qml" line="33"/>
        <source>Error</source>
        <translation type="unfinished">Fehler</translation>
    </message>
    <message>
        <location filename="../qml/PageError.qml" line="42"/>
        <source>Error code:</source>
        <translation type="unfinished">Fehler Code:</translation>
    </message>
</context>
<context>
    <name>PageEthernetSettings</name>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="34"/>
        <source>Wired Settings</source>
        <translation type="unfinished">Kabeleinstellungen</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="104"/>
        <source>Network Info</source>
        <translation type="unfinished">Netzwerk Info</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="110"/>
        <source>DHCP</source>
        <translation type="unfinished">DHCP</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="120"/>
        <source>IP Address</source>
        <translation type="unfinished">IP Adresse</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="133"/>
        <source>Gateway</source>
        <comment>default gateway address</comment>
        <translation type="unfinished">Gateway</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="186"/>
        <source>Apply</source>
        <translation type="unfinished">Anwenden</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="196"/>
        <source>Configuring the connection,</source>
        <translation type="unfinished">Verbindung wird konfiguriert,</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="196"/>
        <source>please wait...</source>
        <translation type="unfinished">bitte warten...</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="203"/>
        <source>Revert</source>
        <comment>Turn back the changes and go back to the previous configuration.</comment>
        <translation type="unfinished">Umkehren</translation>
    </message>
</context>
<context>
    <name>PageException</name>
    <message>
        <location filename="../qml/PageException.qml" line="36"/>
        <source>System Error</source>
        <translation type="unfinished">Systemfehler</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="59"/>
        <source>Error</source>
        <translation type="unfinished">Fehler</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="66"/>
        <source>Error code:</source>
        <translation type="unfinished">Fehler Code:</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="75"/>
        <source>Swipe to proceed</source>
        <translation type="unfinished">Zum Fortfahren wischen</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="100"/>
        <source>Save Logs to USB</source>
        <translation type="unfinished">Sichere Protokolle auf USB</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="113"/>
        <source>Send Logs to Cloud</source>
        <translation type="unfinished">Protokolle an die Cloud senden</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="127"/>
        <source>Update Firmware</source>
        <translation type="unfinished">Firmware Updaten</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="134"/>
        <source>Turn Off</source>
        <translation type="unfinished">Ausschalten</translation>
    </message>
</context>
<context>
    <name>PageFactoryResetWizard</name>
    <message>
        <location filename="../qml/PageFactoryResetWizard.qml" line="31"/>
        <source>Factory Reset</source>
        <translation type="unfinished">Werkseinstellungen</translation>
    </message>
    <message>
        <location filename="../qml/PageFactoryResetWizard.qml" line="43"/>
        <source>Factory Reset done.</source>
        <translation type="unfinished">Werksreset durchgeführt.</translation>
    </message>
</context>
<context>
    <name>PageFeedme</name>
    <message>
        <location filename="../qml/PageFeedme.qml" line="34"/>
        <source>Feed Me</source>
        <translation type="unfinished">Füttere mich</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="42"/>
        <source>Manual resin refill.</source>
        <translation type="unfinished">Manuelles Harzauffüllen.</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="43"/>
        <source>Refill the tank up to the 100% mark and press Done.</source>
        <translation type="unfinished">Bitte füllen Sie den Tank bis zur 100 %-Marke auf und drücken Sie Fertig.</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="44"/>
        <source>If you do not want to refill, press the Back button at top of the screen.</source>
        <translation type="unfinished">Wenn Sie nicht nachfüllen möchten, drücken Sie die Taste Zurück am oberen Bildschirmrand.</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="74"/>
        <source>Done</source>
        <translation type="unfinished">Beendet</translation>
    </message>
</context>
<context>
    <name>PageFileBrowser</name>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="33"/>
        <source>Projects</source>
        <translation type="unfinished">Projekte</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="43"/>
        <source>Selftest</source>
        <translation type="unfinished">Selbsttest</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="44"/>
        <source>Printer Calibration</source>
        <translation type="unfinished">Druckerkalibrierung</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="45"/>
        <source>UV Calibration</source>
        <translation type="unfinished">UV Kalibrierung</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="94"/>
        <source>Local</source>
        <translation type="unfinished">Lokal</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="95"/>
        <source>USB</source>
        <translation type="unfinished">USB</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="96"/>
        <source>Remote</source>
        <translation type="unfinished">Remote</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="97"/>
        <source>Previous Prints</source>
        <comment>a directory with previously printed projects</comment>
        <translation type="unfinished">Vorherige Drucke</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="98"/>
        <source>Update Bundles</source>
        <comment>a directory containing firmware update bundles</comment>
        <translation type="unfinished">Update-Bündel</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="148"/>
        <source>Install?</source>
        <translation type="unfinished">Installieren?</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="150"/>
        <source>Do you really want to install %1?</source>
        <translation type="unfinished">Wollen Sie %1 wirklich installieren?</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="152"/>
        <source>Current system will still be available via Settings -&gt; Firmware -&gt; Downgrade</source>
        <translation type="unfinished">Das aktuelle System ist weiterhin über Einstellungen -&gt; Firmware -&gt; Downgrade verfügbar</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="164"/>
        <source>Calibrate?</source>
        <translation type="unfinished">Kalibrieren?</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="167"/>
        <source>The printer is not fully calibrated.</source>
        <translation type="unfinished">Der Drucker ist nicht vollständig kalibriert.</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="169"/>
        <source>Before printing, the following steps are required to pass:</source>
        <translation type="unfinished">Vor dem Drucken müssen die folgenden Schritte durchlaufen werden:</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="173"/>
        <source>Do you want to start now?</source>
        <translation type="unfinished">Möchten Sie jetzt beginnen?</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/PageFileBrowser.qml" line="229"/>
        <source>%n item(s)</source>
        <comment>number of items in a directory</comment>
        <translation type="unfinished">
            <numerusform>%n Eintrag</numerusform>
            <numerusform>%n Einträge</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="241"/>
        <source>Remote</source>
        <comment>File is stored in remote storage, i.e. a cloud</comment>
        <translation type="unfinished">Remote</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="242"/>
        <location filename="../qml/PageFileBrowser.qml" line="244"/>
        <source>Local</source>
        <comment>File is stored in a local storage</comment>
        <translation type="unfinished">Lokal</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="243"/>
        <source>USB</source>
        <comment>File is stored on USB flash disk</comment>
        <translation type="unfinished">USB</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="245"/>
        <source>Updates</source>
        <comment>File is in a repository of raucb files(update bundles)</comment>
        <translation type="unfinished">Updates</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="246"/>
        <source>Root</source>
        <comment>Directory is a root of the directory tree, its subdirectories are different sources of projects</comment>
        <translation type="unfinished">Root</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="267"/>
        <source>Unknown</source>
        <translation type="unfinished">Unbekannt</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="316"/>
        <source>No usable projects were found,</source>
        <translation type="unfinished">Es wurden keine brauchbaren Projekte gefunden,</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="318"/>
        <source>insert a USB drive or download examples in Settings -&gt; Support.</source>
        <translation type="unfinished">Stecken Sie einen USB-Stick ein oder laden Sie Beispiele unter Einstellungen -&gt; Support herunter.</translation>
    </message>
</context>
<context>
    <name>PageFinished</name>
    <message>
        <location filename="../qml/PageFinished.qml" line="33"/>
        <source>Finished</source>
        <translation type="unfinished">Beended</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="109"/>
        <location filename="../qml/PageFinished.qml" line="112"/>
        <source>FINISHED</source>
        <translation type="unfinished">BEENDED</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="110"/>
        <source>FAILED</source>
        <translation type="unfinished">FEHLGESCHLAGEN</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="111"/>
        <source>CANCELED</source>
        <translation type="unfinished">ABGEBROCHEN</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="136"/>
        <source>Print Time</source>
        <translation type="unfinished">Druckzeit</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="157"/>
        <source>Layers</source>
        <translation type="unfinished">Schichten</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="175"/>
        <source>Consumed Resin</source>
        <translation type="unfinished">Verbrauchtes Harz</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="193"/>
        <source>Layer height</source>
        <translation type="unfinished">Schichthöhe</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="211"/>
        <source>Layer Exposure</source>
        <translation type="unfinished">Schicht Belichtung</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="216"/>
        <location filename="../qml/PageFinished.qml" line="234"/>
        <source>s</source>
        <translation type="unfinished">s</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="229"/>
        <source>First Layer Exposure</source>
        <translation type="unfinished">Erste Schicht Belichtung</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="249"/>
        <source>Swipe to proceed</source>
        <translation type="unfinished">Zum Fortfahren wischen</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="275"/>
        <source>Home</source>
        <translation type="unfinished">Home</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="286"/>
        <source>Reprint</source>
        <translation type="unfinished">Wiederholdruck</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="304"/>
        <source>Turn Off</source>
        <translation type="unfinished">Ausschalten</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="339"/>
        <source>Loading, please wait...</source>
        <translation type="unfinished">Lade, bitte warten...</translation>
    </message>
</context>
<context>
    <name>PageFullscreenImage</name>
    <message>
        <location filename="../qml/PageFullscreenImage.qml" line="27"/>
        <source>Fullscreen Image</source>
        <translation type="unfinished">Vollbild-Bild</translation>
    </message>
</context>
<context>
    <name>PageHome</name>
    <message>
        <location filename="../qml/PageHome.qml" line="29"/>
        <source>Home</source>
        <translation type="unfinished">Home</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="37"/>
        <source>Print</source>
        <translation type="unfinished">Druck</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="50"/>
        <source>Control</source>
        <translation type="unfinished">Kontrolle</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="57"/>
        <source>Settings</source>
        <translation type="unfinished">Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="64"/>
        <source>Turn Off</source>
        <translation type="unfinished">Ausschalten</translation>
    </message>
</context>
<context>
    <name>PageLanguage</name>
    <message>
        <location filename="../qml/PageLanguage.qml" line="31"/>
        <source>Set Language</source>
        <translation type="unfinished">Sprache wählen</translation>
    </message>
    <message>
        <location filename="../qml/PageLanguage.qml" line="79"/>
        <source>Set</source>
        <translation type="unfinished">Einstellen</translation>
    </message>
</context>
<context>
    <name>PageLogs</name>
    <message>
        <location filename="../qml/PageLogs.qml" line="29"/>
        <source>Logs export</source>
        <translation type="unfinished">Protokollexport</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="50"/>
        <source>Log upload finished</source>
        <translation type="unfinished">Protokoll-Upload beendet</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="52"/>
        <source>Logs has been successfully uploaded to the Prusa server.&lt;br /&gt;&lt;br /&gt;Please contact the Prusa support and share the following code with them:</source>
        <translation type="unfinished">Protokolle wurden erfolgreich auf den Prusa-Server hochgeladen.&lt;br /&gt;&lt;br /&gt;Bitte kontaktieren Sie den Prusa-Support und teilen Sie ihm den folgenden Code mit:</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="57"/>
        <source>Logs export finished</source>
        <translation type="unfinished">Protokollexport beendet</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="61"/>
        <source>Logs export canceled</source>
        <translation type="unfinished">Protokollexport abgebrochen</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="64"/>
        <source>Logs export failed.

Please check your flash drive / internet connection.</source>
        <translation type="unfinished">Log-Export fehlgeschlagen. Bitte überprüfen Sie Ihr USB-Laufwerk / Ihre Internetverbindung.</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="88"/>
        <source>Extracting log data</source>
        <translation type="unfinished">Protokolldaten extrahieren</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="91"/>
        <source>Saving data to USB drive</source>
        <translation type="unfinished">Speichere Daten auf USB-Stick</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="92"/>
        <source>Uploading data to server</source>
        <translation type="unfinished">Lade Daten auf den Server hoch</translation>
    </message>
</context>
<context>
    <name>PageManual</name>
    <message>
        <location filename="../qml/PageManual.qml" line="25"/>
        <source>Manual</source>
        <translation type="unfinished">Handbuch</translation>
    </message>
    <message>
        <location filename="../qml/PageManual.qml" line="43"/>
        <source>Scanning the QR code will load the handbook for this device.

Alternatively, use this link:</source>
        <translation type="unfinished">Durch Scannen des QR-Codes wird das Handbuch für dieses Gerät geladen.

Alternativ können Sie auch diesen Link verwenden:</translation>
    </message>
</context>
<context>
    <name>PageMovementControl</name>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="27"/>
        <source>Control</source>
        <translation type="unfinished">Kontrolle</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="38"/>
        <source>Home
Platform</source>
        <translation type="unfinished">Home
Plattform</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="49"/>
        <source>Home
Tank</source>
        <translation type="unfinished">Home
Tank</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="60"/>
        <source>Disable
Steppers</source>
        <translation type="unfinished">Schrittmotoren
abschalten</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="69"/>
        <source>Homing the tank, please wait...</source>
        <translation type="unfinished">Referenzierung des Tanks, bitte warten...</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="69"/>
        <source>Homing the tower, please wait...</source>
        <translation type="unfinished">Referenzierung des Turms, bitte warten...</translation>
    </message>
</context>
<context>
    <name>PageNetworkEthernetList</name>
    <message>
        <location filename="../qml/PageNetworkEthernetList.qml" line="35"/>
        <source>Network</source>
        <translation type="unfinished">Netzwerk</translation>
    </message>
</context>
<context>
    <name>PageNetworkMain</name>
    <message>
        <location filename="../qml/PageNetworkMain.qml" line="28"/>
        <source>Network</source>
        <translation type="unfinished">Netzwerk</translation>
    </message>
    <message>
        <location filename="../qml/PageNetworkMain.qml" line="46"/>
        <source>Wi-Fi</source>
        <translation type="unfinished">Wi-Fi</translation>
    </message>
    <message>
        <location filename="../qml/PageNetworkMain.qml" line="55"/>
        <source>Ethernet</source>
        <translation type="unfinished">Ethernet</translation>
    </message>
    <message>
        <location filename="../qml/PageNetworkMain.qml" line="64"/>
        <source>Hotspot</source>
        <translation type="unfinished">Hotspot</translation>
    </message>
    <message>
        <location filename="../qml/PageNetworkMain.qml" line="85"/>
        <source>IP Address</source>
        <translation type="unfinished">IP Adresse</translation>
    </message>
    <message>
        <location filename="../qml/PageNetworkMain.qml" line="97"/>
        <source>Not connected to network</source>
        <translation type="unfinished">Nicht am Netzwerk angeschlossen</translation>
    </message>
</context>
<context>
    <name>PageNetworkWifiList</name>
    <message>
        <location filename="../qml/PageNetworkWifiList.qml" line="29"/>
        <source>Network</source>
        <translation type="unfinished">Netzwerk</translation>
    </message>
</context>
<context>
    <name>PageNewExpoPanelWizard</name>
    <message>
        <location filename="../qml/PageNewExpoPanelWizard.qml" line="31"/>
        <source>New Exposure Panel</source>
        <translation type="unfinished">Neues Belichtungspanel</translation>
    </message>
    <message>
        <location filename="../qml/PageNewExpoPanelWizard.qml" line="43"/>
        <source>New exposure screen has been detected.

The printer will ask for the inital setup (selftest and calibration) to make sure everything works correctly.</source>
        <translation type="unfinished">Es wurde ein neuer Belichtungsbildschirm erkannt.

Der Drucker fragt nach der Ersteinrichtung (Selbsttest und Kalibrierung), um sicherzustellen, dass alles korrekt funktioniert.</translation>
    </message>
</context>
<context>
    <name>PageNotificationList</name>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="37"/>
        <source>Notifications</source>
        <translation type="unfinished">Benachrichtigungen</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="61"/>
        <source>Available update to %1</source>
        <translation type="unfinished">Verfügbares Update auf %1</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="79"/>
        <source>Print failed: %1</source>
        <translation type="unfinished">Druck fehlgeschlagen: %1</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="80"/>
        <source>Print canceled: %1</source>
        <translation type="unfinished">Druck abgebrochen: %1</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="81"/>
        <source>Print finished: %1</source>
        <translation type="unfinished">Druck beendet: %1</translation>
    </message>
</context>
<context>
    <name>PagePackingWizard</name>
    <message>
        <location filename="../qml/PagePackingWizard.qml" line="31"/>
        <source>Packing Wizard</source>
        <translation type="unfinished">Verpackungsassistent</translation>
    </message>
    <message>
        <location filename="../qml/PagePackingWizard.qml" line="44"/>
        <source>Packing done.</source>
        <translation type="unfinished">Verpacken abgeschlossen.</translation>
    </message>
    <message>
        <location filename="../qml/PagePackingWizard.qml" line="52"/>
        <source>Insert protective foam</source>
        <translation type="unfinished">Legen Sie den Schutzschaum ein</translation>
    </message>
</context>
<context>
    <name>PagePowerOffDialog</name>
    <message>
        <location filename="../qml/PagePowerOffDialog.qml" line="5"/>
        <source>Power Off?</source>
        <translation type="unfinished">Ausschalten?</translation>
    </message>
    <message>
        <location filename="../qml/PagePowerOffDialog.qml" line="6"/>
        <source>Do you really want to turn off the printer?</source>
        <translation type="unfinished">Möchten Sie den Drucker wirklich ausschalten?</translation>
    </message>
    <message>
        <location filename="../qml/PagePowerOffDialog.qml" line="9"/>
        <source>Powering Off...</source>
        <translation type="unfinished">Schalte aus...</translation>
    </message>
</context>
<context>
    <name>PagePrePrintChecks</name>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="32"/>
        <source>Please Wait</source>
        <translation type="unfinished">Bitte warten</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="111"/>
        <source>Temperature</source>
        <translation type="unfinished">Temperatur</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="112"/>
        <source>Project</source>
        <translation type="unfinished">Projekt</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="114"/>
        <source>Fan</source>
        <translation type="unfinished">Lüfter</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="115"/>
        <source>Cover</source>
        <translation type="unfinished">Abdeckung</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="116"/>
        <source>Resin</source>
        <translation type="unfinished">Harz</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="117"/>
        <source>Starting Positions</source>
        <translation type="unfinished">Starte Positionen</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="118"/>
        <source>Stirring</source>
        <translation type="unfinished">Mische</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="133"/>
        <source>Do not touch the printer!</source>
        <translation type="unfinished">Den Drucker nicht berühren!</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="143"/>
        <source>With Warning</source>
        <translation type="unfinished">Mit Warnungen</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="144"/>
        <source>Disabled</source>
        <translation type="unfinished">Abgeschaltet</translation>
    </message>
</context>
<context>
    <name>PagePrint</name>
    <message>
        <location filename="../qml/PagePrint.qml" line="146"/>
        <source>Continue?</source>
        <translation type="unfinished">Weiter?</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="149"/>
        <source>Check Warning</source>
        <translation type="unfinished">Prüfe Warnung</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="159"/>
        <location filename="../qml/PagePrint.qml" line="275"/>
        <source>Stuck Recovery</source>
        <translation type="unfinished">Blockadebehebung</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="160"/>
        <source>The printer got stuck and needs user assistance.</source>
        <translation type="unfinished">Der Drucker ist stecken geblieben und benötigt Unterstützung durch den Benutzer.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="161"/>
        <source>Release the tank mechanism and press Continue.</source>
        <translation type="unfinished">Lösen Sie den Tankmechanismus und drücken Sie Weiter.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="162"/>
        <source>If you do not want to continue, press the Back button on top of the screen and the current job will be canceled.</source>
        <translation type="unfinished">Wenn Sie nicht fortfahren möchten, drücken Sie die Taste &quot;Zurück&quot; am oberen Bildschirmrand und der aktuelle Auftrag wird abgebrochen.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="181"/>
        <source>Close Cover!</source>
        <translation type="unfinished">Schließen Sie die Abdeckung!</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="182"/>
        <source>Please, close the cover! UV radiation is harmful.</source>
        <translation type="unfinished">Bitte schließen Sie die Abdeckung! UV-Strahlung ist schädlich.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="238"/>
        <source>Going up</source>
        <translation type="unfinished">Gehe hoch</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="238"/>
        <source>Moving platform to the top position</source>
        <translation type="unfinished">Plattform in die oberste Position fahren</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="241"/>
        <source>Going down</source>
        <translation type="unfinished">Gehe runter</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="241"/>
        <source>Moving platform to the bottom position</source>
        <translation type="unfinished">Plattform in die untere Position fahren</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="244"/>
        <source>Project</source>
        <translation type="unfinished">Projekt</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="244"/>
        <source>Getting the printer ready to add resin. Please wait.</source>
        <translation type="unfinished">Der Drucker wird für die Harzzugabe vorbereitet. Bitte warten Sie.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="247"/>
        <location filename="../qml/PagePrint.qml" line="301"/>
        <source>Please wait...</source>
        <translation type="unfinished">Bitte warten...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="250"/>
        <source>Cover Open</source>
        <translation type="unfinished">Abdeckung offen</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="250"/>
        <source>Paused.</source>
        <translation type="unfinished">Pausiert.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="250"/>
        <source>Please close the cover to continue</source>
        <translation type="unfinished">Bitte schließen Sie die Abdeckung, um fortzufahren</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="262"/>
        <source>Stirring</source>
        <translation type="unfinished">Mische</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="262"/>
        <source>Stirring resin</source>
        <translation type="unfinished">Mische das Harz</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="265"/>
        <source>Action Pending</source>
        <translation type="unfinished">Anhängige Aktion</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="265"/>
        <source>Requested actions will be executed after layer finish, please wait...</source>
        <translation type="unfinished">Die angeforderten Aktionen werden ausgeführt, sobald diese Schicht beendet ist, bitte warten...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="268"/>
        <source>Reading data...</source>
        <translation type="unfinished">Lese Daten...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="275"/>
        <source>Setting start positions...</source>
        <translation type="unfinished">Einstellen der Startpositionen...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="278"/>
        <source>Tank Moving Down</source>
        <translation type="unfinished">Bewege Tank runter</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="278"/>
        <source>Moving the resin tank down...</source>
        <translation type="unfinished">Bewegen des Harztanks nach unten...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="314"/>
        <source>Loading, please wait...</source>
        <translation type="unfinished">Lade, bitte warten...</translation>
    </message>
</context>
<context>
    <name>PagePrintPreviewSwipe</name>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="33"/>
        <source>Project</source>
        <translation type="unfinished">Projekt</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="165"/>
        <source>Swipe to project</source>
        <translation type="unfinished">Wischen zum Projekt</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="206"/>
        <source>Unknown</source>
        <translation type="unfinished">Unbekannt</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="279"/>
        <source>Layers</source>
        <translation type="unfinished">Schichten</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="279"/>
        <source>Layer Height</source>
        <translation type="unfinished">Schichthöhe</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="298"/>
        <source>Exposure Times</source>
        <translation type="unfinished">Belichtungszeiten</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="323"/>
        <source>Print Time Estimate</source>
        <translation type="unfinished">Druckzeitschätzung</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="340"/>
        <source>Last Modified</source>
        <translation type="unfinished">Zuletzt geändert</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="347"/>
        <source>Unknown</source>
        <comment>Unknow time of last modification of a file</comment>
        <translation type="unfinished">Unbekannt</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="364"/>
        <source>Swipe to continue</source>
        <translation type="unfinished">Wischen zum Fortfahren</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="433"/>
        <source>The project requires %1 % of the resin. It will be necessary to refill the resin during print.</source>
        <translation type="unfinished">Das Projekt benötigt&lt;br/&gt;%1 % des Harzes. Es wird notwendig sein, das Harz während des Drucks nachzufüllen.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="436"/>
        <source>Please fill the resin tank to at least %1 % and close the cover.</source>
        <translation type="unfinished">Bitte füllen Sie den Harztank auf mindestens %1 % und schließen Sie den Deckel.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="471"/>
        <source>Print Settings</source>
        <translation type="unfinished">Druckeinstellungen</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="481"/>
        <source>Print</source>
        <translation type="unfinished">Druck</translation>
    </message>
</context>
<context>
    <name>PagePrintPrinting</name>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="33"/>
        <source>Print</source>
        <translation type="unfinished">Druck</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="104"/>
        <source>Today at</source>
        <translation type="unfinished">Heute um</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="107"/>
        <source>Tomorrow at</source>
        <translation type="unfinished">Morgen um</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="150"/>
        <source>Layer:</source>
        <translation type="unfinished">Schicht:</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="162"/>
        <source>Layer Height:</source>
        <translation type="unfinished">Schichthöhe:</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="162"/>
        <source>N/A</source>
        <translation type="unfinished">N/V</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="251"/>
        <source>Remaining Time</source>
        <translation type="unfinished">Verbleibende Zeit</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="256"/>
        <source>Unknown</source>
        <comment>Remaining time is unknown</comment>
        <translation type="unfinished">Unbekannt</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="266"/>
        <source>Estimated End</source>
        <translation type="unfinished">Erwartetes Ende</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="281"/>
        <source>Printing Time</source>
        <translation type="unfinished">Druckzeit</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="294"/>
        <source>Layer</source>
        <translation type="unfinished">Schicht</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="306"/>
        <source>Remaining Resin</source>
        <translation type="unfinished">Verbleibendes Harz</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="310"/>
        <location filename="../qml/PagePrintPrinting.qml" line="324"/>
        <source>ml</source>
        <translation type="unfinished">ml</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="310"/>
        <location filename="../qml/PagePrintPrinting.qml" line="324"/>
        <source>Unknown</source>
        <translation type="unfinished">Unbekannt</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="320"/>
        <source>Consumed Resin</source>
        <translation type="unfinished">Verbrauchtes Harz</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="359"/>
        <source>Print Settings</source>
        <translation type="unfinished">Druckeinstellungen</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="365"/>
        <source>Refill Resin</source>
        <translation type="unfinished">Manuelles Harzauffüllen</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="378"/>
        <location filename="../qml/PagePrintPrinting.qml" line="384"/>
        <source>Cancel Print</source>
        <translation type="unfinished">Abbrechen</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="385"/>
        <source>To make sure the print is not stopped accidentally,
please swipe the screen to move to the next step,
where you can cancel the print.</source>
        <translation type="unfinished">Um sicherzustellen, dass der Druck nicht versehentlich gestoppt wird, wischen Sie bitte über den Bildschirm, um zum nächsten Schritt zu gelangen, wo Sie den Druck abbrechen können.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="402"/>
        <source>Enter Admin</source>
        <translation type="unfinished">Admin eingeben</translation>
    </message>
</context>
<context>
    <name>PagePrintResinIn</name>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="30"/>
        <source>Pour in resin</source>
        <translation type="unfinished">Harz einfüllen</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="61"/>
        <source>The project requires %1 % of the resin. It will be necessary to refill the resin during print.</source>
        <translation type="unfinished">Das Projekt benötigt&lt;br/&gt;%1 % des Harzes. Es wird notwendig sein, das Harz während des Drucks nachzufüllen.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="64"/>
        <source>Please fill the resin tank to at least %1 % and close the cover.</source>
        <translation type="unfinished">Bitte füllen Sie den Harztank auf mindestens %1 % und schließen Sie den Deckel.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="74"/>
        <source>Continue</source>
        <translation type="unfinished">Fortfahren</translation>
    </message>
</context>
<context>
    <name>PagePrusaConnect</name>
    <message>
        <location filename="../qml/PagePrusaConnect.qml" line="27"/>
        <source>Prusa Connect</source>
        <translation type="unfinished">Prusa Connect</translation>
    </message>
    <message>
        <location filename="../qml/PagePrusaConnect.qml" line="50"/>
        <location filename="../qml/PagePrusaConnect.qml" line="92"/>
        <location filename="../qml/PagePrusaConnect.qml" line="113"/>
        <source>Failed to establish a new connection.
Please try later.</source>
        <translation type="unfinished">Eine neue Verbindung konnte nicht hergestellt werden.
Bitte versuchen Sie es später erneut.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrusaConnect.qml" line="70"/>
        <source>It&apos;s already registered!</source>
        <translation type="unfinished">Ist bereits registriert!</translation>
    </message>
    <message>
        <location filename="../qml/PagePrusaConnect.qml" line="70"/>
        <source>Start Your Registration!</source>
        <translation type="unfinished">Beginnen Sie Ihre Registrierung!</translation>
    </message>
    <message>
        <location filename="../qml/PagePrusaConnect.qml" line="78"/>
        <source>Restore</source>
        <translation type="unfinished">Wiederherstellen</translation>
    </message>
    <message>
        <location filename="../qml/PagePrusaConnect.qml" line="78"/>
        <source>Continue</source>
        <translation type="unfinished">Fortfahren</translation>
    </message>
</context>
<context>
    <name>PageQrCode</name>
    <message>
        <location filename="../qml/PageQrCode.qml" line="30"/>
        <source>Page QR code</source>
        <translation type="unfinished">Seiten QR-Code</translation>
    </message>
</context>
<context>
    <name>PageReleaseNotes</name>
    <message>
        <location filename="../qml/PageReleaseNotes.qml" line="9"/>
        <source>Release Notes</source>
        <translation type="unfinished">Release Notes</translation>
    </message>
    <message>
        <location filename="../qml/PageReleaseNotes.qml" line="111"/>
        <source>Later</source>
        <translation type="unfinished">Später</translation>
    </message>
    <message>
        <location filename="../qml/PageReleaseNotes.qml" line="120"/>
        <source>Install Now</source>
        <translation type="unfinished">Jetzt installieren</translation>
    </message>
</context>
<context>
    <name>PageSelftestWizard</name>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="32"/>
        <source>Selftest</source>
        <translation type="unfinished">Selbsttest</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="48"/>
        <source>Welcome to the selftest wizard.</source>
        <translation type="unfinished">Willkommen beim Selbsttest-Assistenten.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="50"/>
        <source>This procedure is mandatory and it will check all components of the printer.</source>
        <translation type="unfinished">Dieser Vorgang ist obligatorisch und überprüft alle Komponenten des Druckers.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="58"/>
        <source>Please unscrew and remove the resin tank.</source>
        <translation type="unfinished">Bitte lösen und entfernen Sie den Harztank.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="67"/>
        <source>Loosen the black knob and remove the platform.</source>
        <translation type="unfinished">Lösen Sie den schwarzen Dreh-Knopf und entfernen Sie die Plattform.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="76"/>
        <location filename="../qml/PageSelftestWizard.qml" line="137"/>
        <location filename="../qml/PageSelftestWizard.qml" line="169"/>
        <source>Close the cover.</source>
        <translation type="unfinished">Schließen Sie die Abdeckung.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="100"/>
        <source>Can you hear the music?</source>
        <translation type="unfinished">Hören Sie die Musik?</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="110"/>
        <source>Please install the resin tank and tighten the screws evenly.</source>
        <translation type="unfinished">Bitte installieren Sie den Harztank und ziehen Sie die Schrauben gleichmäßig an.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="119"/>
        <source>Please install the platform under 60° angle and tighten it with the black knob.</source>
        <translation type="unfinished">Installieren Sie die Plattform unter einem Winkel von 60° und ziehen Sie sie mit dem schwarzen Drehknopf fest.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="128"/>
        <source>Release the platform from the cantilever and place it onto the center of the resin tank at a 90° angle.</source>
        <translation type="unfinished">Lösen Sie die Plattform vom Kragarm und setzen Sie sie in einem 90°-Winkel auf die Mitte des Harztanks.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="151"/>
        <source>Make sure that the resin tank is installed and the screws are tight.</source>
        <translation type="unfinished">Stellen Sie sicher, dass der Harztank installiert ist und die Schrauben fest angezogen sind.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="160"/>
        <source>Please install the platform under 0° angle and tighten it with the black knob.</source>
        <translation type="unfinished">Installieren Sie die Plattform unter einem Winkel von 0° und ziehen Sie sie mit dem schwarzen Drehknopf fest.</translation>
    </message>
</context>
<context>
    <name>PageSetDate</name>
    <message>
        <location filename="../qml/PageSetDate.qml" line="37"/>
        <source>Set Date</source>
        <translation type="unfinished">Datum setzen</translation>
    </message>
    <message>
        <location filename="../qml/PageSetDate.qml" line="665"/>
        <source>Set</source>
        <translation type="unfinished">Einstellen</translation>
    </message>
</context>
<context>
    <name>PageSetHostname</name>
    <message>
        <location filename="../qml/PageSetHostname.qml" line="30"/>
        <source>Set Hostname</source>
        <translation type="unfinished">Hostnamen einstellen</translation>
    </message>
    <message>
        <location filename="../qml/PageSetHostname.qml" line="47"/>
        <source>Hostname</source>
        <translation type="unfinished">Hostname</translation>
    </message>
    <message>
        <location filename="../qml/PageSetHostname.qml" line="73"/>
        <source>Can contain only characters a-z, A-Z, 0-9 and  &quot;-&quot;.</source>
        <translation type="unfinished">Darf nur die Zeichen a-z, A-Z, 0-9 und &quot;-&quot; enthalten.</translation>
    </message>
    <message>
        <location filename="../qml/PageSetHostname.qml" line="84"/>
        <source>Set</source>
        <translation type="unfinished">Einstellen</translation>
    </message>
</context>
<context>
    <name>PageSetLoginCredentials</name>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="32"/>
        <source>Login Credentials</source>
        <translation type="unfinished">Anmeldedaten</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="42"/>
        <source>Are you sure?</source>
        <translation type="unfinished">Sind Sie sicher?</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="43"/>
        <source>Disable the HTTP Digest?&lt;br/&gt;&lt;br/&gt;CAUTION: This may be insecure!</source>
        <translation type="unfinished">HTTP-Digest deaktivieren?&lt;br/&gt;&lt;br/&gt;ACHTUNG: Dies kann unsicher sein!</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="66"/>
        <source>HTTP Digest</source>
        <translation type="unfinished">HTTP Digest</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="109"/>
        <source>User Name</source>
        <translation type="unfinished">Benutzername</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="146"/>
        <source>Printer Password</source>
        <translation type="unfinished">Drucker Kennwort</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="171"/>
        <location filename="../qml/PageSetLoginCredentials.qml" line="179"/>
        <location filename="../qml/PageSetLoginCredentials.qml" line="229"/>
        <source>Must be at least 8 chars long</source>
        <translation type="unfinished">Muss mindestens 8 Zeichen lang sein</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="185"/>
        <source>Can contain only characters a-z, A-Z, 0-9 and  &quot;-&quot;.</source>
        <translation type="unfinished">Darf nur die Zeichen a-z, A-Z, 0-9 und &quot;-&quot; enthalten.</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="205"/>
        <source>Printer API Key</source>
        <translation type="unfinished">Drucker API Key</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="236"/>
        <source>Can contain only ASCII characters.</source>
        <translation type="unfinished">Darf nur ASCII-Zeichen enthalten.</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="249"/>
        <source>Save</source>
        <translation type="unfinished">Speichern</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="266"/>
        <source>Failed change login settings.</source>
        <translation type="unfinished">Änderung der Anmeldeeinstellungen fehlgeschlagen.</translation>
    </message>
</context>
<context>
    <name>PageSetTime</name>
    <message>
        <location filename="../qml/PageSetTime.qml" line="32"/>
        <source>Set Time</source>
        <translation type="unfinished">Zeit einstellen</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTime.qml" line="98"/>
        <source>Hour</source>
        <translation type="unfinished">Stunde</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTime.qml" line="105"/>
        <source>Minute</source>
        <translation type="unfinished">Minute</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTime.qml" line="112"/>
        <source>Set</source>
        <translation type="unfinished">Einstellen</translation>
    </message>
</context>
<context>
    <name>PageSetTimezone</name>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="29"/>
        <source>Set Timezone</source>
        <translation type="unfinished">Zeitzone setzen</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="129"/>
        <source>Region</source>
        <translation type="unfinished">Region</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="135"/>
        <source>City</source>
        <translation type="unfinished">Ort</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="141"/>
        <source>Set</source>
        <translation type="unfinished">Einstellen</translation>
    </message>
</context>
<context>
    <name>PageSettings</name>
    <message>
        <location filename="../qml/PageSettings.qml" line="28"/>
        <source>Settings</source>
        <translation type="unfinished">Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="58"/>
        <source>Calibration</source>
        <translation type="unfinished">Kalibrierung</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="66"/>
        <source>Network</source>
        <translation type="unfinished">Netzwerk</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="75"/>
        <source>Platform &amp; Resin Tank</source>
        <translation type="unfinished">Plattform &amp; Harztank</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="84"/>
        <source>Firmware</source>
        <translation type="unfinished">Firmware</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="93"/>
        <source>Settings &amp; Sensors</source>
        <translation type="unfinished">Einstellungen &amp; Sensoren</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="102"/>
        <source>Touchscreen</source>
        <translation type="unfinished">Touchscreen</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="111"/>
        <source>Language &amp; Time</source>
        <translation type="unfinished">Sprache &amp; Zeit</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="120"/>
        <source>System Logs</source>
        <translation type="unfinished">System Protokolle</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="129"/>
        <source>Support</source>
        <translation type="unfinished">Support</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="139"/>
        <source>Enter Admin</source>
        <translation type="unfinished">Admin eingeben</translation>
    </message>
</context>
<context>
    <name>PageSettingsCalibration</name>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="31"/>
        <source>Calibration</source>
        <translation type="unfinished">Kalibrierung</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="50"/>
        <source>Selftest</source>
        <translation type="unfinished">Selbsttest</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="57"/>
        <source>Printer Calibration</source>
        <translation type="unfinished">Druckerkalibrierung</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="67"/>
        <source>UV Calibration</source>
        <translation type="unfinished">UV Kalibrierung</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="75"/>
        <source>Display Test</source>
        <translation type="unfinished">Display Test</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="92"/>
        <source>New Display?</source>
        <translation type="unfinished">Neues Display?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="93"/>
        <source>Did you replace the EXPOSITION DISPLAY?</source>
        <translation type="unfinished">Haben Sie das BELICHTUNGSDISPLAY ersetzt?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="105"/>
        <source>New UV LED SET?</source>
        <translation type="unfinished">Neues UV-LED-Set?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="106"/>
        <source>Did you replace the UV LED SET?</source>
        <translation type="unfinished">Haben Sie das UV-LED-SET ersetzt?</translation>
    </message>
</context>
<context>
    <name>PageSettingsFirmware</name>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="35"/>
        <source>Firmware</source>
        <translation type="unfinished">Firmware</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="45"/>
        <source>Unknown</source>
        <comment>Unknown operating system version on alternative slot</comment>
        <translation type="unfinished">Unbekannt</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="74"/>
        <source>Download Now</source>
        <translation type="unfinished">Jetzt herunterladen</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="76"/>
        <source>Install Now</source>
        <translation type="unfinished">Jetzt installieren</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="78"/>
        <source>Check for Update</source>
        <translation type="unfinished">Nach Updates suchen</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="85"/>
        <source>Check for update failed</source>
        <translation type="unfinished">Suche nach Update fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="87"/>
        <source>Checking for updates...</source>
        <translation type="unfinished">Suche nach Updates...</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="89"/>
        <source>System is up-to-date</source>
        <translation type="unfinished">System ist aktuell</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="91"/>
        <source>Update available</source>
        <translation type="unfinished">Update verfügbar</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="93"/>
        <source>Update is downloading</source>
        <translation type="unfinished">Update wird heruntergeladen</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="95"/>
        <source>Update is downloaded</source>
        <translation type="unfinished">Update wird heruntergeladen</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="97"/>
        <source>Update download failed</source>
        <translation type="unfinished">Update Download fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="99"/>
        <source>Updater service idle</source>
        <translation type="unfinished">Updater-Dienst im Leerlauf</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="111"/>
        <source>Installed version</source>
        <translation type="unfinished">Installierte Version</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="137"/>
        <source>Receive Beta Updates</source>
        <translation type="unfinished">Beta-Updates empfangen</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="155"/>
        <source>Switch to beta?</source>
        <translation type="unfinished">Zu Beta wechseln?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="156"/>
        <source>Warning! The beta updates can be unstable.&lt;br/&gt;&lt;br/&gt;Not recommended for production printers.&lt;br/&gt;&lt;br/&gt;Continue?</source>
        <translation type="unfinished">Warnung! Die Beta-Updates können instabil sein.&lt;br/&gt;&lt;br/&gt;Nicht empfohlen für Produktionsdrucker.&lt;br/&gt;&lt;br/&gt;Weiter?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="168"/>
        <source>Switch to version:</source>
        <translation type="unfinished">Wechsel zu Version:</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="180"/>
        <source>Downgrade?</source>
        <translation type="unfinished">Downgraden?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="181"/>
        <source>Do you really want to downgrade to FW</source>
        <translation type="unfinished">Wollen Sie wirklich ein Downgrade auf FW</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="209"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="415"/>
        <source>Incompatible FW!</source>
        <translation type="unfinished">Inkompatible FW!</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="210"/>
        <source>The alternative FW version &lt;b&gt;%1&lt;/b&gt; is not compatible with your printer model - &lt;b&gt;%2&lt;/b&gt;.</source>
        <translation type="unfinished">Die alternative FW-Version &lt;b&gt;%1&lt;/b&gt; ist nicht kompatibel mit Ihrem Druckermodell - &lt;b&gt;%2&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="212"/>
        <source>If you switch, you can update to another FW version afterwards but &lt;b&gt;you will not be able to print.&lt;/b&gt;</source>
        <translation type="unfinished">Wenn Sie wechseln, können Sie anschließend auf eine andere FW-Version aktualisieren, aber &lt;b&gt;Sie werden nicht drucken können.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="214"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="420"/>
        <source>Continue anyway?</source>
        <translation type="unfinished">Trotzdem fortfahren?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="234"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="427"/>
        <source>None</source>
        <comment>Printer model is not known/can&apos;t be determined</comment>
        <translation type="unfinished">Kein</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="238"/>
        <source>Newer than SL1S</source>
        <comment>Printer model is unknown, but better than SL1S</comment>
        <translation type="unfinished">Neuer als SL1S</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="247"/>
        <source>Factory Reset</source>
        <translation type="unfinished">Werkseinstellungen</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="256"/>
        <source>Are you sure?</source>
        <translation type="unfinished">Sind Sie sicher?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="257"/>
        <source>Do you really want to perform the factory reset?

All settings will be erased!
Projects will stay untouched.</source>
        <translation type="unfinished">Wollen Sie den Werksreset wirklich durchführen?

Es werden alle Einstellungen gelöscht!
Projekte bleiben unberührt.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="292"/>
        <source>FW Info</source>
        <comment>page title, information about the selected update bundle</comment>
        <translation type="unfinished">FW Info</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="293"/>
        <source>Loading FW file meta information
(may take up to 20 seconds) ...</source>
        <translation type="unfinished">Lade FW-Datei-Meta-Informationen
(kann bis zu 20 Sekunden dauern) ...</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="335"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="384"/>
        <source>Install Firmware?</source>
        <translation type="unfinished">Firmware installieren?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="336"/>
        <source>FW file meta information not found.&lt;br/&gt;&lt;br/&gt;Do you want to install&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;anyway?</source>
        <translation type="unfinished">FW-Datei-Meta-Informationen nicht gefunden.&lt;br/&gt;&lt;br/&gt;Möchten Sie&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;trotzdem installieren?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="349"/>
        <source>Downgrade Firmware?</source>
        <translation type="unfinished">Firmware downgraden?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="352"/>
        <source>Version of selected FW file&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;,</source>
        <translation type="unfinished">Version der ausgewählten FW-Datei&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;,</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="354"/>
        <source>is lower or equal than current&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;.</source>
        <translation type="unfinished">ist kleiner oder gleich dem aktuellen&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="356"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="389"/>
        <source>Do you want to continue?</source>
        <translation type="unfinished">Wollen Sie fortfahren?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="385"/>
        <source>You have selected update bundle &lt;b&gt;&quot;%1&quot;&lt;/b&gt;,</source>
        <translation type="unfinished">Sie haben das Update-Bundle &lt;b&gt;&quot;%1&quot;&lt;/b&gt; ausgewählt,</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="387"/>
        <source>which has version &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation type="unfinished">mit der Version &lt;b&gt;%1&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="416"/>
        <source>&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;is not compatible with your current hardware model - &lt;b&gt;%2&lt;/b&gt;.</source>
        <translation type="unfinished">&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;ist nicht kompatibel mit Ihrem aktuellen Hardware-Modell - &lt;b&gt;%2&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="418"/>
        <source>After installing this update, the printer can be updated to another FW but &lt;b&gt;printing won&apos;t work.&lt;/b&gt;</source>
        <translation type="unfinished">Nach der Installation dieses Updates kann der Drucker auf eine andere FW aktualisiert werden, aber &lt;b&gt;das Drucken wird nicht funktionieren.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="430"/>
        <source>Unknown</source>
        <comment>Printer model is unknown, but likely better than SL1S</comment>
        <translation type="unfinished">Unbekannt</translation>
    </message>
</context>
<context>
    <name>PageSettingsLanguageTime</name>
    <message>
        <location filename="../qml/PageSettingsLanguageTime.qml" line="28"/>
        <source>Language &amp; Time</source>
        <translation type="unfinished">Sprache &amp; Zeit</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsLanguageTime.qml" line="34"/>
        <source>Set Language</source>
        <translation type="unfinished">Sprache wählen</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsLanguageTime.qml" line="40"/>
        <source>Time Settings</source>
        <translation type="unfinished">Zeiteinstellungen</translation>
    </message>
</context>
<context>
    <name>PageSettingsNetwork</name>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="27"/>
        <source>Network</source>
        <translation type="unfinished">Netzwerk</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="33"/>
        <source>Ethernet</source>
        <translation type="unfinished">Ethernet</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="41"/>
        <source>Wi-Fi</source>
        <translation type="unfinished">Wi-Fi</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="49"/>
        <source>Hot Spot</source>
        <translation type="unfinished">Hot Spot</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="58"/>
        <source>Set Hostname</source>
        <translation type="unfinished">Hostnamen einstellen</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="67"/>
        <source>Login Credentials</source>
        <translation type="unfinished">Anmeldedaten</translation>
    </message>
</context>
<context>
    <name>PageSettingsPlatformResinTank</name>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="27"/>
        <source>Platform &amp; Resin Tank</source>
        <translation type="unfinished">Plattform &amp; Harztank</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="33"/>
        <source>Move Platform</source>
        <translation type="unfinished">Plattform bewegen</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="40"/>
        <source>Move Resin Tank</source>
        <translation type="unfinished">Harztank bewegen</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="47"/>
        <source>Disable Steppers</source>
        <translation type="unfinished">Schrittmotoren abschalten</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="55"/>
        <source>Platform Axis Sensitivity</source>
        <translation type="unfinished">Plattformachsen Empfindlichkeit</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="77"/>
        <source>Tank Axis Sensitivity</source>
        <translation type="unfinished">Tankachsen Empfindlichkeit</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="99"/>
        <source>Limit for Fast Tilt</source>
        <translation type="unfinished">Limit für schnelles Kippen</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="106"/>
        <source>%</source>
        <translation type="unfinished">%</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="125"/>
        <source>Platform Offset</source>
        <translation type="unfinished">Plattform Offset</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="132"/>
        <source>um</source>
        <translation type="unfinished">um</translation>
    </message>
</context>
<context>
    <name>PageSettingsSensors</name>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="27"/>
        <source>Settings &amp; Sensors</source>
        <translation type="unfinished">Einstellungen &amp; Sensoren</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="35"/>
        <source>Device hash in QR</source>
        <translation type="unfinished">Gerätehash im QR</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="60"/>
        <source>Auto Power Off</source>
        <translation type="unfinished">Auto Power Off</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="86"/>
        <source>Cover Check</source>
        <translation type="unfinished">Abdeckungscheck</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="110"/>
        <location filename="../qml/PageSettingsSensors.qml" line="145"/>
        <source>Are you sure?</source>
        <translation type="unfinished">Sind Sie sicher?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="111"/>
        <source>Disable the cover sensor?&lt;br/&gt;&lt;br/&gt;CAUTION: This may lead to unwanted exposure to UV light or personal injury due to moving parts. This action is not recommended!</source>
        <translation type="unfinished">Den Abdeckungssensor deaktivieren?&lt;br/&gt;&lt;br/&gt;ACHTUNG: Dies kann zu ungewollter Einwirkung von UV-Licht oder zu Verletzungen durch bewegliche Teile führen. Diese Aktion wird nicht empfohlen!</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="123"/>
        <source>Resin Sensor</source>
        <translation type="unfinished">Harzsensor</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="146"/>
        <source>Disable the resin sensor?&lt;br/&gt;&lt;br/&gt;CAUTION: This may lead to failed prints or resin tank overflow! This action is not recommended!</source>
        <translation type="unfinished">Den Harzsensor deaktivieren?&lt;br&gt;&lt;br&gt;ACHTUNG: Dies kann zu Fehldrucken oder Überlaufen des Harztanks führen! Diese Aktion wird nicht empfohlen!</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="158"/>
        <source>Rear Fan Speed</source>
        <translation type="unfinished">Drehzahl des hinteren Lüfters</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="165"/>
        <source>RPM</source>
        <translation type="unfinished">UPM</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="177"/>
        <source>Off</source>
        <translation type="unfinished">Aus</translation>
    </message>
</context>
<context>
    <name>PageSettingsSupport</name>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="29"/>
        <source>Support</source>
        <translation type="unfinished">Support</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="35"/>
        <source>Download Examples</source>
        <translation type="unfinished">Download Beispiele</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="49"/>
        <source>Manual</source>
        <translation type="unfinished">Handbuch</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="59"/>
        <source>Videos</source>
        <translation type="unfinished">Videos</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="68"/>
        <source>System Information</source>
        <translation type="unfinished">System Info</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="77"/>
        <source>About Us</source>
        <translation type="unfinished">Über uns</translation>
    </message>
</context>
<context>
    <name>PageSettingsSystemLogs</name>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="28"/>
        <source>System Logs</source>
        <translation type="unfinished">System Protokolle</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="39"/>
        <source>Last Seen Logs:</source>
        <translation type="unfinished">Letztes Protokoll:</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="39"/>
        <source>&lt;b&gt;No logs have been uploaded yet.&lt;/b&gt;</source>
        <translation type="unfinished">&lt;b&gt;Es wurden noch keine Protokolle hochgeladen.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="54"/>
        <source>Save to USB Drive</source>
        <translation type="unfinished">Sichern auf USB</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="68"/>
        <source>Upload to Server</source>
        <translation type="unfinished">Hochladen zum Server</translation>
    </message>
</context>
<context>
    <name>PageSettingsTouchscreen</name>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="27"/>
        <source>Touchscreen</source>
        <translation type="unfinished">Touchscreen</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="35"/>
        <source>Screensaver timer</source>
        <translation type="unfinished">Bildschirmschoner-Timer</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="55"/>
        <source>h</source>
        <comment>hours short</comment>
        <translation type="unfinished">h</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="56"/>
        <source>min</source>
        <comment>minutes short</comment>
        <translation type="unfinished">min</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="57"/>
        <source>s</source>
        <comment>seconds short</comment>
        <translation type="unfinished">s</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="62"/>
        <source>Off</source>
        <translation type="unfinished">Aus</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="79"/>
        <source>Touch Screen Brightness</source>
        <translation type="unfinished">Touch Screen Helligkeit</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="97"/>
        <source>N/A</source>
        <translation type="unfinished">N/V</translation>
    </message>
</context>
<context>
    <name>PageShowToken</name>
    <message>
        <location filename="../qml/PageShowToken.qml" line="27"/>
        <source>Prusa Connect</source>
        <translation type="unfinished">Prusa Connect</translation>
    </message>
    <message>
        <location filename="../qml/PageShowToken.qml" line="31"/>
        <source>Temporary Token</source>
        <translation type="unfinished">Temporärer Token</translation>
    </message>
    <message>
        <location filename="../qml/PageShowToken.qml" line="61"/>
        <source>N/A</source>
        <translation type="unfinished">N/V</translation>
    </message>
    <message>
        <location filename="../qml/PageShowToken.qml" line="80"/>
        <source>Continue</source>
        <translation type="unfinished">Fortfahren</translation>
    </message>
</context>
<context>
    <name>PageSoftwareLicenses</name>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="28"/>
        <source>Software Packages</source>
        <translation type="unfinished">Softwarepakete</translation>
    </message>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="112"/>
        <location filename="../qml/PageSoftwareLicenses.qml" line="233"/>
        <source>Package Name</source>
        <translation type="unfinished">Paketname</translation>
    </message>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="242"/>
        <source>Version</source>
        <translation type="unfinished">Version</translation>
    </message>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="250"/>
        <source>License</source>
        <translation type="unfinished">Lizenz</translation>
    </message>
</context>
<context>
    <name>PageSysinfo</name>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="30"/>
        <source>System Information</source>
        <translation type="unfinished">System Info</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="52"/>
        <source>System</source>
        <translation type="unfinished">System</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="57"/>
        <source>OS Image Version</source>
        <translation type="unfinished">OS Image Version</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="62"/>
        <source>Printer Model</source>
        <translation type="unfinished">Druckermodell</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="65"/>
        <source>None</source>
        <comment>Printer model is not known/can&apos;t be determined</comment>
        <translation type="unfinished">Kein</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="69"/>
        <source>Newer than SL1S</source>
        <comment>Printer model is unknown, but better than SL1S</comment>
        <translation type="unfinished">Neuer als SL1S</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="75"/>
        <source>A64 Controller SN</source>
        <translation type="unfinished">A64 Steuerung SN</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="80"/>
        <source>API Key / Printer Password</source>
        <translation type="unfinished">API Key / Drucker Kennwort</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="92"/>
        <source>Other Components</source>
        <translation type="unfinished">Andere Komponenten</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="97"/>
        <source>Motion Controller SN</source>
        <translation type="unfinished">Bewegungssteuerung SN</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="102"/>
        <source>Motion Controller SW Version</source>
        <translation type="unfinished">Bewegungssteuerung SW Version</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="107"/>
        <source>Motion Controller HW Revision</source>
        <translation type="unfinished">Bewegungssteuerung HW Revision</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="112"/>
        <source>Booster Board SN</source>
        <translation type="unfinished">Booster Board SN</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="118"/>
        <source>Exposure display SN</source>
        <translation type="unfinished">Belichtungsdisplay SN</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="124"/>
        <source>GUI Version</source>
        <translation type="unfinished">GUI Version</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="136"/>
        <source>Hardware State</source>
        <translation type="unfinished">Hardwarestatus</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="141"/>
        <source>Network State</source>
        <translation type="unfinished">Netzwerkstatus</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="142"/>
        <source>Online</source>
        <translation type="unfinished">Online</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="142"/>
        <source>Offline</source>
        <translation type="unfinished">Offline</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="146"/>
        <source>Ethernet IP Address</source>
        <translation type="unfinished">Ethernet IP Adresse</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="147"/>
        <location filename="../qml/PageSysinfo.qml" line="152"/>
        <location filename="../qml/PageSysinfo.qml" line="230"/>
        <location filename="../qml/PageSysinfo.qml" line="263"/>
        <location filename="../qml/PageSysinfo.qml" line="268"/>
        <location filename="../qml/PageSysinfo.qml" line="273"/>
        <location filename="../qml/PageSysinfo.qml" line="278"/>
        <location filename="../qml/PageSysinfo.qml" line="283"/>
        <source>N/A</source>
        <translation type="unfinished">N/V</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="151"/>
        <source>Wifi IP Address</source>
        <translation type="unfinished">Wifi IP Adresse</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="156"/>
        <source>Time of Fast Tilt</source>
        <translation type="unfinished">Zeit des schnellen Kippens</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="157"/>
        <location filename="../qml/PageSysinfo.qml" line="162"/>
        <source>seconds</source>
        <translation type="unfinished">Sekunden</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="161"/>
        <source>Time of Slow Tilt</source>
        <translation type="unfinished">Zeit des langsamen Kippens</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="166"/>
        <source>Resin Sensor State</source>
        <translation type="unfinished">Harzsensor Status</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="167"/>
        <source>Triggered</source>
        <translation type="unfinished">Ausgelöst</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="167"/>
        <source>Not triggered</source>
        <translation type="unfinished">Nicht ausgelöst</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="171"/>
        <source>Cover State</source>
        <translation type="unfinished">Abdeckungsstatus</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="172"/>
        <source>Closed</source>
        <translation type="unfinished">Geschlossen</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="172"/>
        <source>Open</source>
        <translation type="unfinished">Offen</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="176"/>
        <source>CPU Temperature</source>
        <translation type="unfinished">CPU Temperatur</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="177"/>
        <location filename="../qml/PageSysinfo.qml" line="182"/>
        <location filename="../qml/PageSysinfo.qml" line="187"/>
        <source>°C</source>
        <translation type="unfinished">°C</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="181"/>
        <source>UV LED Temperature</source>
        <translation type="unfinished">UV LED Temperatur</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="186"/>
        <source>Ambient Temperature</source>
        <translation type="unfinished">Umgebungstemperatur</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="191"/>
        <source>UV LED Fan</source>
        <translation type="unfinished">UV LED Lüfter</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="194"/>
        <location filename="../qml/PageSysinfo.qml" line="204"/>
        <location filename="../qml/PageSysinfo.qml" line="214"/>
        <source>Fan Error!</source>
        <translation type="unfinished">LÜFTERFEHLER!</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="196"/>
        <location filename="../qml/PageSysinfo.qml" line="206"/>
        <location filename="../qml/PageSysinfo.qml" line="216"/>
        <source>RPM</source>
        <translation type="unfinished">UPM</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="201"/>
        <source>Blower Fan</source>
        <translation type="unfinished">Gebläselüfter</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="211"/>
        <source>Rear Fan</source>
        <translation type="unfinished">Hinterer Lüfter</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="221"/>
        <source>UV LED</source>
        <translation type="unfinished">UV LED</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="235"/>
        <source>Power Supply Voltage</source>
        <translation type="unfinished">Netzspannung</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="236"/>
        <source>V</source>
        <translation type="unfinished">V</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="247"/>
        <source>Statistics</source>
        <translation type="unfinished">Statistik</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="252"/>
        <source>UV LED Time Counter</source>
        <translation type="unfinished">UV LED Zeitzähler</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="253"/>
        <location filename="../qml/PageSysinfo.qml" line="258"/>
        <location filename="../qml/PageSysinfo.qml" line="278"/>
        <source>d</source>
        <translation type="unfinished">d</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="253"/>
        <location filename="../qml/PageSysinfo.qml" line="258"/>
        <location filename="../qml/PageSysinfo.qml" line="278"/>
        <source>h</source>
        <translation type="unfinished">h</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="253"/>
        <location filename="../qml/PageSysinfo.qml" line="258"/>
        <location filename="../qml/PageSysinfo.qml" line="278"/>
        <source>m</source>
        <translation type="unfinished">m</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="257"/>
        <source>Print Display Time Counter</source>
        <translation type="unfinished">Druckdisplay Zeitzähler</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="262"/>
        <source>Started Projects</source>
        <translation type="unfinished">Gestartete Projekte</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="267"/>
        <source>Finished Projects</source>
        <translation type="unfinished">Beendete Projekte</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="272"/>
        <source>Total Layers Printed</source>
        <translation type="unfinished">Schichten gedruckt gesamt</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="277"/>
        <source>Total Print Time</source>
        <translation type="unfinished">Druckzeit gesamt</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="282"/>
        <source>Total Resin Consumed</source>
        <translation type="unfinished">Harzverbrauch gesamt</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="283"/>
        <source>ml</source>
        <translation type="unfinished">ml</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="293"/>
        <source>Show software packages &amp; licenses</source>
        <translation type="unfinished">Softwarepakete und Lizenzen anzeigen</translation>
    </message>
</context>
<context>
    <name>PageTiltmove</name>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="32"/>
        <source>Move Resin Tank</source>
        <translation type="unfinished">Harztank bewegen</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="76"/>
        <source>Fast Up</source>
        <translation type="unfinished">Schnell Hoch</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="84"/>
        <source>Fast Down</source>
        <translation type="unfinished">Schnell Runter</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="92"/>
        <source>Up</source>
        <translation type="unfinished">Hoch</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="100"/>
        <source>Down</source>
        <translation type="unfinished">Runter</translation>
    </message>
</context>
<context>
    <name>PageTimeSettings</name>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="34"/>
        <source>Time Settings</source>
        <translation type="unfinished">Zeiteinstellungen</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="49"/>
        <source>Use NTP</source>
        <translation type="unfinished">Automatische Zeiteinstellung</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="67"/>
        <source>Time</source>
        <translation type="unfinished">Zeit</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="87"/>
        <source>Date</source>
        <translation type="unfinished">Datum</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="107"/>
        <source>Timezone</source>
        <translation type="unfinished">Zeitzone</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="124"/>
        <source>Time Format</source>
        <translation type="unfinished">Zeitformat</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="135"/>
        <source>Native</source>
        <comment>Default time format determined by the locale</comment>
        <translation type="unfinished">Nativ</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="136"/>
        <source>12-hour</source>
        <comment>12h time format</comment>
        <translation type="unfinished">12 Stunden</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="137"/>
        <source>24-hour</source>
        <comment>24h time format</comment>
        <translation type="unfinished">24 Stunden</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="158"/>
        <source>Automatic time settings using NTP ...</source>
        <translation type="unfinished">Automatische Zeiteinstellung über NTP ...</translation>
    </message>
</context>
<context>
    <name>PageTowermove</name>
    <message>
        <location filename="../qml/PageTowermove.qml" line="32"/>
        <source>Move Platform</source>
        <translation type="unfinished">Plattform bewegen</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="77"/>
        <source>Fast Up</source>
        <translation type="unfinished">Schnell Hoch</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="85"/>
        <source>Fast Down</source>
        <translation type="unfinished">Schnell Runter</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="93"/>
        <source>Up</source>
        <translation type="unfinished">Hoch</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="101"/>
        <source>Down</source>
        <translation type="unfinished">Runter</translation>
    </message>
</context>
<context>
    <name>PageUnpackingCompleteWizard</name>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="31"/>
        <source>Unpacking</source>
        <translation type="unfinished">Auspacken</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="47"/>
        <source>Unscrew and remove the resin tank and remove the black foam underneath it.</source>
        <translation type="unfinished">Lösen und entfernen Sie den Harztank und entfernen Sie den darunter liegenden schwarzen Schaumstoff.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="56"/>
        <source>Remove the black foam from both sides of the platform.</source>
        <translation type="unfinished">Entfernen Sie den schwarzen Schaumstoff von beiden Seiten der Plattform.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="65"/>
        <source>Please remove the safety sticker and open the cover.</source>
        <translation type="unfinished">Bitte entfernen Sie den Sicherheitsaufkleber und öffnen Sie die Abdeckung.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="74"/>
        <source>Carefully peel off the protective sticker from the exposition display.</source>
        <translation type="unfinished">Ziehen Sie die Schutz-Aufkleber vorsichtig vom Belichtungsdisplay ab.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="83"/>
        <source>Unpacking done.</source>
        <translation type="unfinished">Auspacken abgeschlossen.</translation>
    </message>
</context>
<context>
    <name>PageUnpackingKitWizard</name>
    <message>
        <location filename="../qml/PageUnpackingKitWizard.qml" line="31"/>
        <source>Unpacking</source>
        <translation type="unfinished">Auspacken</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingKitWizard.qml" line="44"/>
        <source>Carefully peel off the protective sticker from the exposition display.</source>
        <translation type="unfinished">Ziehen Sie die Schutz-Aufkleber vorsichtig vom Belichtungsdisplay ab.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingKitWizard.qml" line="53"/>
        <source>Unpacking done.</source>
        <translation type="unfinished">Auspacken abgeschlossen.</translation>
    </message>
</context>
<context>
    <name>PageUpdatingFirmware</name>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="36"/>
        <source>Printer Update</source>
        <translation type="unfinished">Drucker Update</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="42"/>
        <source>Unknown</source>
        <translation type="unfinished">Unbekannt</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="170"/>
        <source>Downloading</source>
        <translation type="unfinished">Herunterladen</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="211"/>
        <source>Downloading firmware, installation will begin immediately after.</source>
        <translation type="unfinished">Lade Firmware herunter, die Installation beginnt sofort danach.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="228"/>
        <source>Download failed.</source>
        <translation type="unfinished">Download fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="254"/>
        <source>Installing Firmware</source>
        <translation type="unfinished">Installiere Firmware</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="295"/>
        <source>Do not power off the printer while updating!&lt;br/&gt;Printer will be rebooted after a successful update.</source>
        <translation type="unfinished">Schalten Sie den Drucker während der Aktualisierung nicht aus!&lt;br/&gt;Der Drucker wird nach erfolgreicher Aktualisierung neu gestartet.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="345"/>
        <source>Updated to %1 failed.</source>
        <translation type="unfinished">Update auf %1 fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="362"/>
        <source>Printer is being restarted into the new firmware(%1), please wait</source>
        <translation type="unfinished">Drucker wird mit der neuen Firmware(%1) neu gestartet, bitte warten</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="387"/>
        <source>Update to %1 failed.</source>
        <translation type="unfinished">Update auf %1 fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="420"/>
        <source>A problem has occurred while updating, please let us know (send us the logs). Do not panic, your printer is still working the same as it did before. Continue by pressing &quot;back&quot;</source>
        <translation type="unfinished">Beim Aktualisieren ist ein Problem aufgetreten. Bitte lassen Sie uns wissen, was passiert ist (und senden Sie uns die Protokolle). Keine Panik, Ihr Drucker funktioniert immer noch genauso wie zuvor. Drücken Sie &quot;Zurück&quot;, um fortzufahren</translation>
    </message>
</context>
<context>
    <name>PageUpgradeWizard</name>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="31"/>
        <source>Hardware Upgrade</source>
        <translation type="unfinished">Hardware Upgrade</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="45"/>
        <source>SL1S components detected (upgrade from SL1).</source>
        <translation type="unfinished">SL1S-Komponenten erkannt (Upgrade von SL1).</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="46"/>
        <source>To complete the upgrade procedure, printer needs to clear the configuration and reboot.</source>
        <translation type="unfinished">Um den Upgrade-Vorgang abzuschließen, muss der Drucker die Konfiguration löschen und neu starten.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="47"/>
        <source>Proceed?</source>
        <translation type="unfinished">Fortfahren?</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="56"/>
        <source>The printer will power off now.</source>
        <translation type="unfinished">Der Drucker wird nun ausgeschaltet.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="57"/>
        <source>Reassemble SL1 components and power on the printer. This will restore the original state.</source>
        <translation type="unfinished">Bauen Sie die SL1-Komponenten wieder zusammen und schalten Sie den Drucker ein. Dadurch wird der ursprüngliche Zustand wiederhergestellt.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="65"/>
        <source>The configuration is going to be cleared now.</source>
        <translation type="unfinished">Die Konfiguration wird nun gelöscht.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="66"/>
        <source>The printer will ask for the inital setup after reboot.</source>
        <translation type="unfinished">Der Drucker fragt nach dem Neustart nach der Ersteinrichtung.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="75"/>
        <source>Use only the plastic resin tank supplied. Using the old metal resin tank may cause resin to spill and damage your printer!</source>
        <translation type="unfinished">Verwenden Sie nur den mitgelieferten Kunststoff-Harztank. Bei Verwendung des alten Metall-Harzbehälters kann Harz austreten und Ihren Drucker beschädigen!</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="84"/>
        <source>Only use the platform supplied. Using a different platform may cause resin to spill and damage your printer!</source>
        <translation type="unfinished">Verwenden Sie nur die mitgelieferte Plattform. Die Verwendung einer anderen Plattform kann zum Auslaufen des Harzes führen und Ihren Drucker beschädigen!</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="93"/>
        <source>Please note that downgrading is not supported. 

Downgrading your printer will erase your UV calibration and your printer will not work properly. 

You will need to recalibrate it using an external UV calibrator.</source>
        <translation type="unfinished">Bitte beachten Sie, dass ein Downgrade nicht unterstützt wird. 

Durch ein Downgrade wird die UV-Kalibrierung Ihres Druckers gelöscht und Ihr Drucker funktioniert nicht mehr ordnungsgemäß. 

Sie müssen ihn mithilfe eines externen UV-Kalibrators neu kalibrieren.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="101"/>
        <source>Upgrade done. In the next step, the printer will be restarted.</source>
        <translation type="unfinished">Das Upgrade ist abgeschlossen. Im nächsten Schritt wird der Drucker neu gestartet.</translation>
    </message>
</context>
<context>
    <name>PageUvCalibrationWizard</name>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="31"/>
        <source>UV Calibration</source>
        <translation type="unfinished">UV Kalibrierung</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="46"/>
        <source>Welcome to the UV calibration.</source>
        <translation type="unfinished">Willkommen bei der UV-Kalibrierung.</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="48"/>
        <source>1. If the resin tank is in the printer, remove it along with the screws.</source>
        <translation type="unfinished">1. Wenn sich der Harztank im Drucker befindet, entfernen Sie ihn zusammen mit den Schrauben.</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="50"/>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="70"/>
        <source>2. Close the cover, don&apos;t open it! UV radiation is harmful!</source>
        <translation type="unfinished">2. Schließen Sie den Deckel, öffnen Sie ihn nicht! UV-Strahlung kann schädlich sein!</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="53"/>
        <source>Intensity: center %1, edge %2</source>
        <translation type="unfinished">Intensität: Mitte %1, Rand %2</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="55"/>
        <source>Warm-up: %1 s</source>
        <translation type="unfinished">Aufwärmen: %1 s</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="68"/>
        <source>1. Place the UV calibrator on the print display and connect it to the front USB.</source>
        <translation type="unfinished">1. Stellen Sie den UV-Kalibrator auf das Druckdisplay und schließen Sie ihn an den vorderen USB-Anschluss an.</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="80"/>
        <source>Open the cover, &lt;b&gt;remove and disconnect&lt;/b&gt; the UV calibrator.</source>
        <translation type="unfinished">Öffnen Sie die Abdeckung, &lt;b&gt;trennen und entfernen&lt;/b&gt; Sie den UV-Kalibrator.</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="95"/>
        <source>The result of calibration:</source>
        <translation type="unfinished">Das Ergebnis der Kalibrierung:</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="96"/>
        <source>UV PWM: %1</source>
        <translation type="unfinished">UV PWM: %1</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="97"/>
        <source>UV Intensity: %1, σ = %2</source>
        <translation type="unfinished">UV Intensität: %1, σ = %2</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="98"/>
        <source>UV Intensity min: %1, max: %2</source>
        <translation type="unfinished">UV Intensität min: %1, max: %2</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="102"/>
        <source>The printer has been successfully calibrated!
Would you like to apply the calibration results?</source>
        <translation type="unfinished">Der Drucker wurde erfolgreich kalibriert!
Möchten Sie die Kalibrierungsergebnisse anwenden?</translation>
    </message>
</context>
<context>
    <name>PageVerticalList</name>
    <message>
        <location filename="../qml/PageVerticalList.qml" line="57"/>
        <source>Loading, please wait...</source>
        <translation type="unfinished">Lade, bitte warten...</translation>
    </message>
</context>
<context>
    <name>PageVideos</name>
    <message>
        <location filename="../qml/PageVideos.qml" line="23"/>
        <source>Videos</source>
        <translation type="unfinished">Videos</translation>
    </message>
    <message>
        <location filename="../qml/PageVideos.qml" line="26"/>
        <source>Scanning the QR code will take you to our YouTube playlist with videos about this device.

Alternatively, use this link:</source>
        <translation type="unfinished">Wenn Sie den QR-Code scannen, gelangen Sie zu unserer YouTube-Playlist mit Videos zu diesem Gerät.

Alternativ können Sie auch diesen Link verwenden:</translation>
    </message>
</context>
<context>
    <name>PageWait</name>
    <message>
        <location filename="../qml/PageWait.qml" line="27"/>
        <source>Please Wait</source>
        <translation type="unfinished">Bitte warten</translation>
    </message>
</context>
<context>
    <name>PageWifiNetworkSettings</name>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="34"/>
        <source>Wireless Settings</source>
        <translation type="unfinished">Drahtlose Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="90"/>
        <source>Connected</source>
        <translation type="unfinished">Verbunden</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="90"/>
        <source>Disconnected</source>
        <translation type="unfinished">Nicht verbunden</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="102"/>
        <source>Network Info</source>
        <translation type="unfinished">Netzwerk Info</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="109"/>
        <source>DHCP</source>
        <translation type="unfinished">DHCP</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="191"/>
        <source>Apply</source>
        <translation type="unfinished">Anwenden</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="201"/>
        <source>Configuring the connection,
please wait...</source>
        <comment>This is horizontal-center aligned, ideally 2 lines</comment>
        <translation type="unfinished">Verbindung wird konfiguriert, bitte warten...</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="208"/>
        <source>Revert</source>
        <comment>Turn back the changes and go back to the previous configuration.</comment>
        <translation type="unfinished">Umkehren</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="218"/>
        <source>Forget network</source>
        <comment>Removes all information about the network(settings, passwords,...)</comment>
        <translation type="unfinished">Netzwerk vergessen</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="226"/>
        <source>Forget network?</source>
        <translation type="unfinished">Netzwerk vergessen?</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="227"/>
        <source>Do you really want to forget this network&apos;s settings?</source>
        <translation type="unfinished">Möchten Sie die Einstellungen dieses Netzwerks wirklich vergessen?</translation>
    </message>
</context>
<context>
    <name>PageYesNoSimple</name>
    <message>
        <location filename="../qml/PageYesNoSimple.qml" line="28"/>
        <source>Are You Sure?</source>
        <translation type="unfinished">Sind Sie sicher?</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSimple.qml" line="60"/>
        <source>Yes</source>
        <translation type="unfinished">Ja</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSimple.qml" line="76"/>
        <source>No</source>
        <translation type="unfinished">Nein</translation>
    </message>
</context>
<context>
    <name>PageYesNoSwipe</name>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="29"/>
        <source>Are You Sure?</source>
        <translation type="unfinished">Sind Sie sicher?</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="76"/>
        <source>Swipe to proceed</source>
        <translation type="unfinished">Zum Fortfahren wischen</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="97"/>
        <source>Yes</source>
        <translation type="unfinished">Ja</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="110"/>
        <source>No</source>
        <translation type="unfinished">Nein</translation>
    </message>
</context>
<context>
    <name>PageYesNoWithPicture</name>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="28"/>
        <source>Are You Sure?</source>
        <translation type="unfinished">Sind Sie sicher?</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="176"/>
        <source>Swipe for a picture</source>
        <translation type="unfinished">Wischen für ein Bild</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="254"/>
        <source>Yes</source>
        <translation type="unfinished">Ja</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="267"/>
        <source>No</source>
        <translation type="unfinished">Nein</translation>
    </message>
</context>
<context>
    <name>PrusaPicturePictureButtonItem</name>
    <message>
        <location filename="../qml/PrusaPicturePictureButtonItem.qml" line="58"/>
        <source>Plugged in</source>
        <translation type="unfinished">Eingesteckt</translation>
    </message>
    <message>
        <location filename="../qml/PrusaPicturePictureButtonItem.qml" line="58"/>
        <source>Unplugged</source>
        <translation type="unfinished">Nicht angeschlossen</translation>
    </message>
</context>
<context>
    <name>PrusaSwitch</name>
    <message>
        <location filename="../qml/PrusaSwitch.qml" line="112"/>
        <source>Off</source>
        <translation type="unfinished">Aus</translation>
    </message>
    <message>
        <location filename="../qml/PrusaSwitch.qml" line="170"/>
        <source>On</source>
        <translation type="unfinished">An</translation>
    </message>
</context>
<context>
    <name>PrusaWaitOverlay</name>
    <message>
        <location filename="../qml/PrusaWaitOverlay.qml" line="74"/>
        <source>Please wait...</source>
        <comment>can be on multiple lines</comment>
        <translation type="unfinished">Bitte warten...</translation>
    </message>
</context>
<context>
    <name>SwipeSign</name>
    <message>
        <location filename="../qml/SwipeSign.qml" line="98"/>
        <source>Swipe to confirm</source>
        <translation type="unfinished">Zur Bestätigung wischen</translation>
    </message>
</context>
<context>
    <name>WarningText</name>
    <message>
        <location filename="../qml/WarningText.qml" line="45"/>
        <source>Must not be empty, only 0-9, a-z, A-Z, _ and - are allowed here!</source>
        <translation type="unfinished">Darf nicht leer sein, nur 0-9, a-z, A-Z, _ und - sind hier erlaubt!</translation>
    </message>
</context>
<context>
    <name>WindowHeader</name>
    <message>
        <location filename="../PrusaComponents/Delegates/WindowHeader.qml" line="86"/>
        <source>back</source>
        <translation type="unfinished">zurück</translation>
    </message>
    <message>
        <location filename="../PrusaComponents/Delegates/WindowHeader.qml" line="99"/>
        <source>cancel</source>
        <translation type="unfinished">abbrechen</translation>
    </message>
    <message>
        <location filename="../PrusaComponents/Delegates/WindowHeader.qml" line="111"/>
        <source>close</source>
        <translation type="unfinished">geschlossen</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="50"/>
        <source>Prusa SL1 Touchscreen User Interface</source>
        <translation type="unfinished">Prusa SL1 Touchscreen Benutzeroberfläche</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="64"/>
        <location filename="../qml/main.qml" line="69"/>
        <source>Unknown</source>
        <translation type="unfinished">Unbekannt</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="65"/>
        <source>Activating</source>
        <translation type="unfinished">Aktiviere</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="66"/>
        <source>Connected</source>
        <translation type="unfinished">Verbunden</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="67"/>
        <source>Deactivating</source>
        <translation type="unfinished">Am Deaktivieren</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="68"/>
        <source>Deactivated</source>
        <translation type="unfinished">Deaktiviert</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="277"/>
        <source>Notifications</source>
        <translation type="unfinished">Benachrichtigungen</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="320"/>
        <source>Initializing...</source>
        <translation type="unfinished">Initialisierung...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="321"/>
        <source>The printer is initializing, please wait ...</source>
        <translation type="unfinished">Der Drucker wird initialisiert, bitte warten ...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="330"/>
        <source>MC Update</source>
        <translation type="unfinished">MC Update</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="331"/>
        <source>The Motion Controller firmware is being updated.

Please wait...</source>
        <translation type="unfinished">Die Firmware des Motion Controllers wird gerade aktualisiert.

Bitte warten...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="493"/>
        <source>Turn Off?</source>
        <translation type="unfinished">Ausschalten?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="506"/>
        <source>Cancel?</source>
        <translation type="unfinished">Abbrechen</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="521"/>
        <source>Cancel the current print job?</source>
        <translation type="unfinished">Aktuellen Druckjob abbrechen</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="544"/>
        <source>Printer should not be turned off in this state.
Finish or cancel the current action and try again.</source>
        <translation type="unfinished">Der Drucker sollte in diesem Zustand nicht ausgeschaltet werden. Beenden oder brechen Sie die aktuelle Aktion ab und versuchen Sie es erneut.</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="555"/>
        <location filename="../qml/main.qml" line="579"/>
        <source>DEPRECATED PROJECTS</source>
        <translation type="unfinished">VERALTETE PROJEKTE</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="556"/>
        <source>Some incompatible projects were found, you can download them at 

http://%1/old-projects

Do you want to remove them?</source>
        <translation type="unfinished">Es wurden einige inkompatible Projekte gefunden, Sie können sie herunterladen unter 

http://%1/old-projects

Möchten Sie sie entfernen?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="570"/>
        <location filename="../qml/main.qml" line="593"/>
        <source>&lt;printer IP&gt;</source>
        <translation type="unfinished">&lt;printer IP&gt;</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="580"/>
        <source>Some incompatible file were found, you can view them at http://%1/old-projects.

Would you like to remove them?</source>
        <translation type="unfinished">Es wurden einige inkompatible Projekte gefunden, Sie können sie herunterladen unter 

http://%1/old-projects.

Möchten Sie sie entfernen?</translation>
    </message>
</context>
<context>
    <name>utils</name>
    <message>
        <location filename="../qml/utils.js" line="47"/>
        <source>Less than a minute</source>
        <translation type="unfinished">Weniger als eine Minute</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="50"/>
        <source>%n h</source>
        <comment>how many hours</comment>
        <translation type="unfinished">
            <numerusform>%n h</numerusform>
            <numerusform>%n h</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="51"/>
        <source>%n min</source>
        <comment>how many minutes</comment>
        <translation type="unfinished">
            <numerusform>%n Min</numerusform>
            <numerusform>%n Min</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="55"/>
        <source>%n hour(s)</source>
        <comment>how many hours</comment>
        <translation type="unfinished">
            <numerusform>%n Stunde</numerusform>
            <numerusform>%n Stunden</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="56"/>
        <source>%n minute(s)</source>
        <comment>how many minutes</comment>
        <translation type="unfinished">
            <numerusform>%n Minute</numerusform>
            <numerusform>%n Minuten</numerusform>
        </translation>
    </message>
</context>
</TS>
