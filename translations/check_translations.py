#!/usr/bin/python3
import re
import sys
import traceback
import argparse
from enum import Enum
from typing import List, Tuple
from typing import NamedTuple
import xml.etree.ElementTree as ET
from difflib import SequenceMatcher

class ViolationType(Enum):
    NEWLINE_COUNT_DIFFER = 1
    INITIAL_WHITESPACE_DIFFER = 2
    TRAILING_WHITESPACES_DIFFER = 3
    LAST_PUNCTUATION_DIFFER = 4
    TRANSLATION_MISSING = 5


class Violation(NamedTuple):
    file: str
    location: str
    source_text: str
    translation_text: str
    nature: List[ViolationType]
    def __str__(self):
        return "\n".join([
            f"""{self.file}:{self.location} "{self.source_text}" -> "{self.translation_text}", Violations: {[n.name for n in self.nature]}""",
        ])


class CheckFileResult(NamedTuple):
    file: str
    result: bool
    translations_missing_count: int
    violation_count: int
    violations: list
    def __str__(self):
        return "\n".join([
            f"""File: {self.file} {"Succeeded" if self.result else "Failed"}""",
            f"""\t{self.translations_missing_count} translations missing""",
            f"""\t{self.violation_count} violations""",
            "\n\t".join([str(v) for v in self.violations])
        ])


def check_text(source_text : str, translation_text : str) -> List[ViolationType]:
    """ Check that the translated and original text:
        - has the same count of \\n,
        - has the same number of initial whitespaces,
        - has the same number of trailing whitespaces,
        - if the source ends with any of {':', '.', '?', '!', ';', ',', '-'}
          then the translation must also end with it.
    """
    puctuations = {':', '.', '?', '!', ';', ',', '-'}
    result = []
    
    def first_spaces(t: str) -> str:
        return t[0: len(t) - len(t.lstrip())]

    def last_spaces(t : str) -> str:
        return t[len(t.rstrip()): len(t)]
    
    if translation_text.strip() == "":
        result.append(ViolationType.TRANSLATION_MISSING)
        return result

    if (a:=source_text.count("\n")) != (b:=translation_text.count("\n")):
        result.append(ViolationType.NEWLINE_COUNT_DIFFER)

    if first_spaces(source_text) != first_spaces(translation_text):
        result.append(ViolationType.INITIAL_WHITESPACE_DIFFER)
        
    src_last = source_text.strip()[-1] if len(source_text.strip()) else ""
    tr_last = translation_text.strip()[-1] if len(translation_text.strip()) else ""
    if (src_last in puctuations or tr_last in puctuations) and src_last != tr_last:
        result.append(ViolationType.LAST_PUNCTUATION_DIFFER)
        
    if last_spaces(source_text) != last_spaces(translation_text):
        result.append(ViolationType.TRAILING_WHITESPACES_DIFFER)
    return result




def check_translation_file(path: str, strict: bool = False, verbose: bool = False) -> CheckFileResult:
    tree = ET.parse(path)
    root = tree.getroot()
    messages = root.findall("context/message")
    violation_count = 0
    untranslated_count = 0
    violation_list: List[Violation] = []

    for msg in messages:
        location = msg.find("location")
        location_string = location.attrib.get("filename", "unknown") + ":" + location.attrib.get("line", "0")
        source = msg.find("source")
        translation = msg.find("translation")
        is_numerus = True if msg.attrib.get("numerus", "no") == "yes" else False

        # Each plural variant needs to be handled
        if is_numerus:
            for numerusform in translation.findall("numerusform"):
                violations = check_text(str(source.text), str(numerusform.text))
                if numerusform:
                    untranslated_count += 1
                if violations:
                    violation_count += 1
                    violation_list.append(Violation(
                        file=path,
                        location=location_string,
                        source_text=source.text,
                        translation_text=numerusform.text,
                        nature=[ViolationType.TRANSLATION_MISSING] if not numerusform.text else violations
                    ))
        else:
            violations = check_text(str(source.text), str(translation.text))
            if violations:
                violation_count += 1
                violation_list.append(Violation(
                    file=path,
                    location=location_string,
                    source_text=source.text,
                    translation_text=translation.text,
                    nature=[ViolationType.TRANSLATION_MISSING] if not translation.text else violations
                ))
                if ViolationType.TRANSLATION_MISSING in violation_list[-1].nature:
                    untranslated_count += 1
            
    if verbose:
        print(f"""{len(messages) - untranslated_count} / {len(messages)} translated.""")
        print(f"""Found {violation_count} violations.""")

    result = CheckFileResult(
        file=path,
        result=(violation_count == 0) if strict else untranslated_count == 0,
        translations_missing_count=untranslated_count,
        violation_count=len(violation_list),
        violations=violation_list
        )

    if verbose:
        print(result)
    return result


if __name__ == "__main__":
    try:
        parser = argparse.ArgumentParser(description=f"""Checks all of the listed *.ts files for any violation of the translation rules. 
        Returns 0 if all texts are translated, non-zero otherwise.
        {check_text.__doc__}""")
        parser.add_argument("--verbose", "-v", action="store_true", help="Print further information to rule violations")
        parser.add_argument("--strict", action="store_true", help="Normaly, only missing translations are considered critical. With this option, any problem will result in an error code.")
        parser.add_argument("ts_files", nargs='*')
        args = parser.parse_args(sys.argv)
        
        if args.strict:
            pass
        if args.verbose:
            pass
        
        print(f"Processing files: {args.ts_files[1:]}")
        if all([check_translation_file(f, strict=args.strict, verbose=args.verbose).result for f in args.ts_files[1:]]):
            print("[ OK ]")
            sys.exit(0)
        else:
            print("[ FAILED ]")
            sys.exit(1)
        

    except SystemExit as e:
        raise e

    except:
        print(traceback.format_exc())
        exit(1)
