#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 10 16:53:52 2022

@author: martin

Input: <file.ts>
Output: <sorted_file.ts>

The program will pass through all the translation contexts and sort messages within by the source text.
"""

import sys
import argparse
import copy
import unittest
import tempfile
from os.path import exists
import xml.etree.ElementTree as ET

verbose = False

def sort_messages_by_source(tree: ET) -> ET:
    """ Sort messages within every context alphabetically by the source text """
    modified_tree = copy.deepcopy(tree)
    root = modified_tree.getroot()
    for ctx in root:
            ctx_name = ctx.find("name").text
            if verbose:
                print(f"----------< {ctx_name} >------------")
            messages = ctx.findall("message")
            sorted_messages = sorted(messages, key=lambda a: a.find("source").text)
            if verbose:
                print("\n".join([m.find("source").text for m in sorted_messages]))

            # Replace ctx content
            for old_msg in ctx.findall("message"):
                ctx.remove(old_msg)

            for msg in sorted_messages:
                ctx.append(msg)
    return modified_tree

def store_to_file(tree: ET, filename: str):
    with open(filename, "w") as output:
        output.write("""<?xml version="1.0" encoding="utf-8"?>\n""")
        output.write("""<!DOCTYPE TS>\n""")
        tree.write(output, encoding="unicode", xml_declaration=False)
        output.write("\n")


class TestThatSortedTreesAreOtherwiseEquivalent(unittest.TestCase):
    """ Precondition: file en_US.ts is present in the same directory """
    def setUp(self):
        self.translation_file_name = "en_US.ts"
        self.assertTrue(exists(self.translation_file_name))
        self.original_tree = ET.parse(self.translation_file_name)
        self.sorted_tree = sort_messages_by_source(self.original_tree)

    def test_deep_copy(self):
        self.assertNotEqual(self.original_tree, self.sorted_tree)

    def test_store_load(self):
        with tempfile.NamedTemporaryFile("w+b") as tmp_file:
            store_to_file(self.sorted_tree, tmp_file.name)
            reloaded_tree = ET.parse(tmp_file)

            self.test_context_no_duplicities(self.original_tree, reloaded_tree)
            self.test_has_same_contexts(self.original_tree, reloaded_tree)
            self.test_has_same_messages(self.original_tree, reloaded_tree)

    def test_has_same_contexts(self, original_tree=None, sorted_tree=None):
        if not original_tree:
            original_tree = self.original_tree
        if not sorted_tree:
            sorted_tree = self.sorted_tree

        root_o, root_s = original_tree.getroot(), sorted_tree.getroot()

        # Same context names
        self.assertEqual(
            set({ctx.find("name").text for ctx in root_o.findall("context")}),
            set({ctx.find("name").text for ctx in root_o.findall("context")}))

    def test_context_no_duplicities(self, original_tree=None, sorted_tree=None):
        if not original_tree:
            original_tree = self.original_tree
        if not sorted_tree:
            sorted_tree = self.sorted_tree

        root_o, root_s = original_tree.getroot(), sorted_tree.getroot()

        names_o_set = set({ctx.find("name").text for ctx in root_o.findall("context")})
        names_s_set = set({ctx.find("name").text for ctx in root_o.findall("context")})
        self.assertEqual(len(names_o_set), len(root_o.findall("context")))
        self.assertEqual(len(names_s_set), len(root_s.findall("context")))


    def test_has_same_messages(self, original_tree=None, sorted_tree=None):
        if not original_tree:
            original_tree = self.original_tree
        if not sorted_tree:
            sorted_tree = self.sorted_tree

        root_o, root_s = original_tree.getroot(), sorted_tree.getroot()

        for ctx_o, ctx_s in zip(root_o.findall("context"), root_s.findall("context")):
            # Order of contexts should stay the same
            self.assertEqual(ctx_o.find("name").text, ctx_s.find("name").text)

            messages_o, messages_s = sorted(ctx_o.findall("message"),  key=lambda msg: msg.find("source").text), ctx_s.findall("message")
            for msg_o, msg_s in zip(messages_o, messages_s):
                self.assertEqual(msg_o.find("location").attrib["filename"], msg_s.find("location").attrib["filename"])
                self.assertEqual(msg_o.find("location").attrib["line"], msg_s.find("location").attrib["line"])
                self.assertEqual(msg_o.find("source").text, msg_s.find("source").text)
                self.assertEqual(msg_o.find("translation").attrib["type"], msg_s.find("translation").attrib["type"])
                self.assertEqual(msg_o.find("translation").text, msg_s.find("translation").text)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="The program will pass through all the translation contexts and sort messages within by the source text.")
    parser.add_argument("-v", "--verbose", action="store_true", help="Be verbose")
    parser.add_argument("src", help="Input *.ts file to sort")
    parser.add_argument("dst", help="Output *.ts file - sorted")
    args = parser.parse_args(sys.argv[1:])

    verbose = args.verbose

    if verbose:
        print(f"Sorting {args.src} --->  {args.dst}")
    tree = ET.parse(args.src)
    sorted_tree = sort_messages_by_source(tree)
    store_to_file(sorted_tree, args.dst)
    if verbose:
        print("\n---------------------------\n")
    print("\n")
    print(f"Input file: {args.src}")
    print(f"Ouput file: {args.dst}")
    print(f"""Contexts: {len(tree.getroot().findall("context"))}""")
    print(f"""Messages: {len(tree.getroot().findall("context/message"))}""")
    missing_translations = sum([1 if tr.text is None else 0 for tr in tree.getroot().findall("context/message/translation")])
    print(f"Missing Translations: {missing_translations}")
    sys.exit(0)