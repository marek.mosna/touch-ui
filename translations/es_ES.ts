<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es-ES">
<context>
    <name>DelegateAddHiddenNetwork</name>
    <message>
        <location filename="../qml/DelegateAddHiddenNetwork.qml" line="91"/>
        <source>Add Hidden Network</source>
        <translation type="unfinished">Añadir red oculta</translation>
    </message>
</context>
<context>
    <name>DelegateAdminPlusMinus</name>
    <message>
        <location filename="../qml/DelegateAdminPlusMinus.qml" line="41"/>
        <source>N/A</source>
        <translation type="unfinished">N/A</translation>
    </message>
</context>
<context>
    <name>DelegateEthNetwork</name>
    <message>
        <location filename="../qml/DelegateEthNetwork.qml" line="110"/>
        <source>Plugged in</source>
        <translation type="unfinished">Conectado</translation>
    </message>
    <message>
        <location filename="../qml/DelegateEthNetwork.qml" line="110"/>
        <source>Unplugged</source>
        <translation type="unfinished">Desconectado</translation>
    </message>
</context>
<context>
    <name>DelegateRef</name>
    <message>
        <location filename="../qml/DelegateRef.qml" line="28"/>
        <source>N/A</source>
        <translation type="unfinished">N/A</translation>
    </message>
</context>
<context>
    <name>DelegateState</name>
    <message>
        <location filename="../qml/DelegateState.qml" line="38"/>
        <source>Network Info</source>
        <translation type="unfinished">Información de Red</translation>
    </message>
</context>
<context>
    <name>DelegateWifiClientOnOff</name>
    <message>
        <location filename="../qml/DelegateWifiClientOnOff.qml" line="39"/>
        <source>Wi-Fi Client</source>
        <translation type="unfinished">Cliente Wi-Fi</translation>
    </message>
</context>
<context>
    <name>DelegateWifiNetwork</name>
    <message>
        <location filename="../qml/DelegateWifiNetwork.qml" line="106"/>
        <source>Forget network?</source>
        <translation type="unfinished">Red olvidada?</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWifiNetwork.qml" line="107"/>
        <source>Do you really want to forget this network&apos;s settings?</source>
        <translation type="unfinished">¿Realmente quieres olvidar la configuración de esta red?</translation>
    </message>
</context>
<context>
    <name>DelegateWifiOnOff</name>
    <message>
        <location filename="../qml/DelegateWifiOnOff.qml" line="36"/>
        <source>Wi-Fi</source>
        <translation type="unfinished">Wi-Fi</translation>
    </message>
</context>
<context>
    <name>DelegateWizardCheck</name>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="74"/>
        <source>Platform range</source>
        <translation type="unfinished">Recorrido de la plataforma</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="75"/>
        <source>Platform home</source>
        <translation type="unfinished">Plataforma al origen</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="76"/>
        <source>Tank range</source>
        <translation type="unfinished">Rango del tanque</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="77"/>
        <source>Tank home</source>
        <translation type="unfinished">Home tanque</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="78"/>
        <source>Display test</source>
        <translation type="unfinished">Test de pantalla</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="79"/>
        <source>Printer calibration</source>
        <translation type="unfinished">Calibración de la impresora</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="80"/>
        <source>Sound test</source>
        <translation type="unfinished">Prueba de sonido</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="81"/>
        <source>UV LED</source>
        <translation type="unfinished">LED UV</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="82"/>
        <source>UV LED and fans</source>
        <translation type="unfinished">LED UV y ventiladores</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="83"/>
        <source>Release foam</source>
        <translation type="unfinished">Liberar la espuma</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="84"/>
        <source>Make tank accessible</source>
        <translation type="unfinished">Ten el tanque a mano</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="85"/>
        <source>Resin sensor</source>
        <translation type="unfinished">Sensor de resina</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="86"/>
        <source>Serial number</source>
        <translation type="unfinished">Número de serie</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="87"/>
        <source>Temperature</source>
        <translation type="unfinished">Temperatura</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="88"/>
        <source>Tank calib. start</source>
        <translation type="unfinished">Inicio Cal. tanque</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="89"/>
        <location filename="../qml/DelegateWizardCheck.qml" line="120"/>
        <source>Tank level</source>
        <translation type="unfinished">Nivel del tanque</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="90"/>
        <source>Platform calibration</source>
        <translation type="unfinished">Calibración de la plataforma</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="91"/>
        <source>Tilt timming</source>
        <translation type="unfinished">Tiempo de inclinación</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="92"/>
        <source>Obtain system info</source>
        <translation type="unfinished">Obtener información del sistema</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="93"/>
        <source>Obtain calibration info</source>
        <translation type="unfinished">Obtener información de calibración</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="94"/>
        <source>Erase projects</source>
        <translation type="unfinished">Borrar proyectos</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="95"/>
        <source>Reset hostname</source>
        <translation type="unfinished">Restablecer nombre de host</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="96"/>
        <source>Reset API key</source>
        <translation type="unfinished">Restablecer clave de la API</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="97"/>
        <source>Reset remote config</source>
        <translation type="unfinished">Restablecer configuración remota</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="98"/>
        <source>Reset HTTP digest</source>
        <translation type="unfinished">Reiniciar HTTP digest</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="99"/>
        <source>Reset Wi-Fi settings</source>
        <translation type="unfinished">Restablecer la configuración del Wi-Fi</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="100"/>
        <source>Reset timezone</source>
        <translation type="unfinished">Reiniciar zona horaria</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="101"/>
        <source>Reset NTP state</source>
        <translation type="unfinished">Restablecer el estado de NTP</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="102"/>
        <source>Reset system locale</source>
        <translation type="unfinished">Restablecer la configuración regional del sistema</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="103"/>
        <source>Clear UV calibration data</source>
        <translation type="unfinished">Borrar datos de calibración UV</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="104"/>
        <source>Clear downloaded Slicer profiles</source>
        <translation type="unfinished">Borrar perfiles de Slicer descargados</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="105"/>
        <source>Reset print configuration</source>
        <translation type="unfinished">Restablecer la configuración de impresión</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="106"/>
        <source>Erase motion controller EEPROM</source>
        <translation type="unfinished">Borrar la EEPROM del controlador de movimiento</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="107"/>
        <source>Reset homing profiles</source>
        <translation type="unfinished">Restablecer perfiles de posicionamiento inicial</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="108"/>
        <source>Send printer data to MQTT</source>
        <translation type="unfinished">Enviar datos de la impresora a MQTT</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="109"/>
        <source>Disable factory mode</source>
        <translation type="unfinished">Desactivar modo de fábrica</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="110"/>
        <source>Moving printer to accept protective foam</source>
        <translation type="unfinished">Mueve la impresora para colocar la espuma protectora</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="111"/>
        <source>Pressing protective foam</source>
        <translation type="unfinished">Apretando la espuma protectora</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="112"/>
        <source>Disable ssh, serial</source>
        <translation type="unfinished">Deshabilitar ssh, serie</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="113"/>
        <source>Check for UV calibrator</source>
        <translation type="unfinished">Comprueba el calibrador UV</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="114"/>
        <source>UV LED warmup</source>
        <translation type="unfinished">Calentamiento LED UV</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="115"/>
        <source>UV calibrator placed</source>
        <translation type="unfinished">Calibrador UV colocado</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="116"/>
        <source>Calibrate center</source>
        <translation type="unfinished">Calibrar el centro</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="117"/>
        <source>Calibrate edge</source>
        <translation type="unfinished">Calibrar borde</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="118"/>
        <source>Apply calibration results</source>
        <translation type="unfinished">Aplicar los resultados de la calibración</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="119"/>
        <source>Waiting for UV calibrator to be removed</source>
        <translation type="unfinished">Esperando que se retire el calibrador UV</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="121"/>
        <source>Reset UI settings</source>
        <translation type="unfinished">Restablecer la configuración de la interfaz de usuario</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="122"/>
        <source>Erase UV PWM settings</source>
        <translation type="unfinished">Borrar la configuración PWM de los UV</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="123"/>
        <source>Reset selftest status</source>
        <translation type="unfinished">Restablecer el estado del autotest</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="124"/>
        <source>Reset printer calibration status</source>
        <translation type="unfinished">Restablecer el estado de calibración de la impresora</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="125"/>
        <source>Set new printer model</source>
        <translation type="unfinished">Ajustar nuevo modelo de impresora</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="126"/>
        <source>Resetting hardware counters</source>
        <translation type="unfinished">Reseteando contadores de hardware</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="127"/>
        <source>Recording changes</source>
        <translation type="unfinished">Grabando cambios</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="128"/>
        <source>Unknown</source>
        <translation type="unfinished">Desconocido</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="129"/>
        <source>Check ID:</source>
        <translation type="unfinished">Verificar ID:</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="141"/>
        <source>Waiting</source>
        <translation type="unfinished">Esperando</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="142"/>
        <source>Running</source>
        <translation type="unfinished">En ejecución</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="143"/>
        <source>Passed</source>
        <translation type="unfinished">Aprobado</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="144"/>
        <source>Failure</source>
        <translation type="unfinished">Fallo</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="145"/>
        <source>With Warning</source>
        <translation type="unfinished">Con advertencia</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="146"/>
        <source>User action pending</source>
        <translation type="unfinished">Acción del usuario pendiente</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="147"/>
        <source>Canceled</source>
        <translation type="unfinished">Cancelado</translation>
    </message>
</context>
<context>
    <name>ErrorPopup</name>
    <message>
        <location filename="../qml/ErrorPopup.qml" line="40"/>
        <source>Unknown error</source>
        <translation type="unfinished">Error desconocido</translation>
    </message>
    <message>
        <location filename="../qml/ErrorPopup.qml" line="55"/>
        <source>Understood</source>
        <translation type="unfinished">Entendido</translation>
    </message>
</context>
<context>
    <name>ErrorcodesText</name>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="6"/>
        <source>Tilt homing failed, check its surroundings and repeat the action.</source>
        <translation type="unfinished">Homing de la inclinación fallido, verifica sus alrededores y repite la acción.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="7"/>
        <source>Tower homing failed, make sure there is no obstacle in its path and repeat the action.</source>
        <translation type="unfinished">Homing de la torre fallido, asegúrate de que no hay ningún obstáculo en su camino y repite la acción.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="8"/>
        <source>Moving the tower failed. Make sure there is no obstacle in its path and repeat the action.</source>
        <translation type="unfinished">Fallo al mover la torre. Asegúrate de que no hay ningún obstáculo en su camino y repite la acción.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="9"/>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="115"/>
        <source>Incorrect RPM reading of the %(failed_fans_text)s fan. Please check its wiring and connection.</source>
        <translation type="unfinished">Lectura incorrecta de RPM del ventilador %(failed_fans_text)s. Verifica su cableado y conexión.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="10"/>
        <source>Measured resin volume %(volume_ml)d ml is lower than required for this print. Refill the tank and restart the print.</source>
        <translation type="unfinished">El volumen de resina medido %(volume_ml)d ml es menor de lo necesario para esta impresión. Rellena el tanque y reinicia la impresión.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="11"/>
        <source>Measured resin volume %(volume_ml)d ml is higher than required for this print. Make sure that the resin level does not exceed the 100% mark and restart the print.</source>
        <translation type="unfinished">El volumen de resina medido %(volume_ml)d ml es mayor de lo necesario para esta impresión. Asegúrate de que el nivel de resina no supere la marca del 100% y reinicia la impresión.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="12"/>
        <source>The printer is not calibrated. Please run the Wizard first.</source>
        <translation type="unfinished">La impresora no está calibrada. Primero ejecuta el Asistente.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="13"/>
        <source>Failed to reach the tower endstop, check that the tower motor is connected and repeat the action.</source>
        <translation type="unfinished">No se pudo alcanzar el endstop de la torre, verifica que el motor de la torre está conectado y repite la acción.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="14"/>
        <source>Failed to reach the tilt endstop, check that the cable is connected and repeat the action.</source>
        <translation type="unfinished">No se pudo alcanzar el endstop de inclinación, verifica que el cable esté conectado y repite la acción.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="15"/>
        <source>Tower axis check failed!

Current position: %(position_nm)d nm

Check if the ballscrew can move smoothly in its entire range.</source>
        <translation type="unfinished">¡La comprobación del eje de la torre falló!

Posición actual: %(position_nm)d nm

Comprueba si el husillo de bolas puede moverse suavemente en todo su rango.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="16"/>
        <source>Tilt axis check failed!

Current position: %(position)d steps

Check if the tilt can move smoothly in its entire range.</source>
        <translation type="unfinished">¡La comprobación del eje de inclinación falló!

Posición actual: %(position)d pasos

Comprueba si la inclinación se puede mover suavemente en todo su rango.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="17"/>
        <source>Display test failed, check the connection between the display and the A64 board.</source>
        <translation type="unfinished">La prueba de pantalla falló, verifica la conexión entre la pantalla y la placa A64.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="18"/>
        <source>Invalid tilt alignment position. Check the tilt mechanism and repeat the action.</source>
        <translation type="unfinished">Posición de alineación de la inclinación no válida. Comprueba el mecanismo de inclinación y repite la acción.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="19"/>
        <source>RPM of %(fan)s not in range!

Check if the fan is connected correctly.

RPM data: %(rpm)s
Average: %(avg)s</source>
        <translation type="unfinished">¡No están en rango las RPM del %(fan)s!

Comprueba si el ventilador está conectado correctamente.

Datos de RPM: %(rpm)s
Media: %(avg)s</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="20"/>
        <source>Tower not at the expected position.

Are the platform and tank mounted and secured correctly?</source>
        <translation type="unfinished">La torre no está en la posición esperada.

¿Están la plataforma y el tanque montados y asegurados correctamente?</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="21"/>
        <source>Measuring the resin failed. Check the presence of the platform and the amount of resin in the tank.</source>
        <translation type="unfinished">La medición de la resina ha fallado. Verifica la presencia de la plataforma y la cantidad de resina en el tanque.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="22"/>
        <source>The %(sensor)s sensor failed. Check the wiring and connection.</source>
        <translation type="unfinished">El sensor %(sensor)s falló. Comprueba el cableado y la conexión.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="23"/>
        <source>UV LED is overheating! Check whether the heatsink is installed correctly.</source>
        <translation type="unfinished">¡El LED UV se está sobrecalentando! Comprueba si el disipador térmico está instalado correctamente.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="24"/>
        <source>A64 temperature is too high. Measured: %(temperature).1f °C! Shutting down in 10 seconds...</source>
        <translation type="unfinished">Temperatura del A64 es muy alta. Medida: %(temperature).1f °C! Apagando en 10 segundos...</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="25"/>
        <source>%(sensor)s not in range! Measured temperature: %(temperature).1f °C. Keep the printer out of direct sunlight at room temperature (18 - 32 °C).</source>
        <translation type="unfinished">¡%(sensor)s no está en rango! Temperatura medida: %(temperature).1f °C. Mantén la impresora alejada de la luz solar directa a temperatura ambiente. (18 - 32 °C).</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="26"/>
        <source>Reading of UV LED temperature has failed! This value is essential for the UV LED lifespan and printer safety. Please contact tech support! Current print job will be canceled.</source>
        <translation type="unfinished">¡La lectura de la temperatura del LED UV ha fallado! Este valor es esencial para la vida útil de los LED UV y la seguridad de la impresora. Ponte en contacto con el soporte técnico. Se cancelará el trabajo de impresión actual.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="27"/>
        <source>Wrong revision of the Motion Controller (MC). Contact our support.</source>
        <translation type="unfinished">Revisión incorrecta del controlador de movimiento (MC). Ponte en contacto con nuestro soporte.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="28"/>
        <source>The Motion Controller (MC) has encountered an unexpected error. Restart the printer.</source>
        <translation type="unfinished">El Motion Controller (MC) ha encontrado un error inesperado. Reinicia la impresora.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="29"/>
        <source>The resin sensor was not triggered. Check whether the tank and the platform are properly secured. Inspect the wiring of the sensor.</source>
        <translation type="unfinished">El sensor de resina no se activó. Comprueba que el tanque y la plataforma están bien asegurados. Inspecciona el cableado del sensor.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="30"/>
        <source>The printer is not UV calibrated. Connect the UV calibrator and complete the calibration.</source>
        <translation type="unfinished">La impresora no está calibrada con UV. Conecta el calibrador de UV y completa la calibración.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="31"/>
        <source>UV LED voltages differ too much. The LED module might be faulty. Contact our support.</source>
        <translation type="unfinished">Los voltajes de los LED UV difieren demasiado. El módulo LED puede estar defectuoso. Ponte en contacto con nuestro soporte.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="32"/>
        <source>Speaker test failed, check the connection and repeat the action.</source>
        <translation type="unfinished">La prueba del altavoz falló, verifica la conexión y repite la acción.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="33"/>
        <source>The UV LED calibrator is not detected. Check the connection and try again.</source>
        <translation type="unfinished">No se detecta el calibrador de LED UV. Verifica la conexión y vuelve a intentarlo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="34"/>
        <source>Cannot connect to the UV LED calibrator. Check the connection and try again.</source>
        <translation type="unfinished">No se puede conectar al calibrador de LED UV. Verifica la conexión y vuelve a intentarlo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="35"/>
        <source>Communication with the UV LED calibrator has failed. Check the connection and try again.</source>
        <translation type="unfinished">Ha fallado la comunicación con el calibrador de LED UV. Verifica la conexión y vuelve a intentarlo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="36"/>
        <source>The UV LED calibrator detected some light on a dark display. This means there is a light &apos;leak&apos; under the UV calibrator, or your display does not block the UV light enough. Check the UV calibrator placement on the screen or replace the exposure display.</source>
        <translation type="unfinished">El calibrador de LED UV detectó algo de luz en una pantalla oscura. Esto significa que hay una &apos;fuga&apos; de luz debajo del calibrador UV o que tu pantalla no bloquea la luz UV lo suficiente. Verifica la ubicación del calibrador UV en la pantalla o reemplaza la pantalla de exposición.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="37"/>
        <source>The UV LED calibrator failed to read expected UV light intensity. Check the UV calibrator placement on the screen.</source>
        <translation type="unfinished">El calibrador de LED UV no pudo leer la intensidad de luz UV esperada. Verifica la ubicación del calibrador UV en la pantalla.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="38"/>
        <source>Unknown UV LED calibrator error code: %(code)d</source>
        <translation type="unfinished">Código de error desconocido del calibrador de LED UV: %(code)d</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="39"/>
        <source>Requested intensity cannot be reached by min. allowed PWM.</source>
        <translation type="unfinished">La intensidad solicitada no se puede alcanzar con el PWM min. permitido.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="40"/>
        <source>Requested intensity cannot be reached by max. allowed PWM.</source>
        <translation type="unfinished">La intensidad solicitada no se puede alcanzar con el PWM max. permitido.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="41"/>
        <source>Correct settings were found, but the standard deviation
(%(found).1f) is greater than the allowed value (%(allowed).1f).
Verify the UV LED calibrator&apos;s position and calibration, then try again.</source>
        <translation type="unfinished">Se encontraron ajustes correctos, pero la desviación estándar
(%(found).1f) es mayor que el valor permitido (%(allowed).1f).
Verifica la posición y la calibración del calibrador de LED UV y vuelve a intentarlo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="42"/>
        <source>Communication with the Booster board failed. Check the connection and restart the printer.</source>
        <translation type="unfinished">Falló la comunicación con la placa Booster. Comprueba la conexión y reinicia la impresora.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="43"/>
        <source>The UV LED panel is not detected. Check the connection.</source>
        <translation type="unfinished">No se detecta el panel LED UV. Comprueba la conexión.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="44"/>
        <source>A part of the LED panel is disconnected. Check the connection and the LED panel.</source>
        <translation type="unfinished">Una parte del panel LED está desconectada. Comprueba la conexión y el panel LED.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="45"/>
        <source>The printer model was not detected. Check the connection of the exposure display and restart the printer.</source>
        <translation type="unfinished">No se detectó el modelo de impresora. Comprueba la conexión de la pantalla de exposición y reinicia la impresora.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="46"/>
        <source>Cannot send factory config to the database (MQTT)! Check the network connection. Please, contact support.</source>
        <translation type="unfinished">No se puede enviar la configuración de fábrica a la base de datos (MQTT)! Verifica la conexión de red. Por favor, contacta con el soporte.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="47"/>
        <source>The printer is not connected to the internet. Check the connection in the Settings.</source>
        <translation type="unfinished">La impresora no está conectada a Internet. Verifica la conexión en los Ajustes.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="48"/>
        <source>Connection to Prusa servers failed, please try again later.</source>
        <translation type="unfinished">Falló la conexión con los servidores de Prusa. Vuelve a intentarlo más tarde.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="49"/>
        <source>The download failed. Check the connection to the internet and try again.</source>
        <translation type="unfinished">La descarga falló. Verifica la conexión a Internet y vuelve a intentarlo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="50"/>
        <source>Please turn on the HTTP digest (which is the recommended security option) or update the API key. You can find it in Settings &gt; Network &gt; Login credentials.</source>
        <translation type="unfinished">Activa el HTTP Digest (que es la opción de seguridad recomendada) o actualiza la clave API. Puedes encontrarlo en Configuración&gt; Red&gt; Credenciales de inicio de sesión.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="51"/>
        <source>The printer uses HTTP digest security. Please enable it also in your slicer (recommended), or turn off this security option in the printer. You can find it in Settings &gt; Network &gt; Login credentials.</source>
        <translation type="unfinished">La impresora utiliza la seguridad de HTTP Digest. Habilítala también en tu programa de laminado (recomendado) o desactiva esta opción de seguridad en la impresora. Puedes encontrarla en Configuración&gt; Red&gt; Credenciales de inicio de sesión.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="52"/>
        <source>This request is not compatible with the Prusa remote API. See our documentation for more details.</source>
        <translation type="unfinished">Esta solicitud no es compatible con la API remota de Prusa. Consulta nuestra documentación para obtener más detalles.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="53"/>
        <source>No problem detected. You can continue using the printer.</source>
        <translation type="unfinished">No se detectó ningún problema. Puedes seguir usando la impresora.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="54"/>
        <source>An unexpected error has occurred :-(.
If print job is in progress, it should be finished.
You can turn the printer off by pressing the front power button.
See the handbook to learn how to save a log file and send it to us.</source>
        <translation type="unfinished">Ha ocurrido un error inesperado :-(.
Si hay un trabajo de impresión en curso, debería estar terminado.
Puedes apagar la impresora presionando el botón de encendido frontal.
Consulta el manual para saber cómo guardar un archivo de registro y envianoslo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="55"/>
        <source>Image preloader did not finish successfully!</source>
        <translation type="unfinished">¡La precarga de imágenes no se completó correctamente!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="56"/>
        <source>Opening the project failed, the file may be corrupted. Re-slice or re-export the project and try again.</source>
        <translation type="unfinished">No se pudo abrir el proyecto, el archivo puede estar corrupto. Relamina o reexporta el proyecto, vuelve a intentarlo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="57"/>
        <source>Failed to read the configuration file. Try to reset the printer. If the problem persists, contact our support.</source>
        <translation type="unfinished">No se pudo leer el archivo de configuración. Intenta reiniciar la impresora. Si el problema persiste, contacta con nuestro soporte.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="58"/>
        <source>Another action is already running. Finish this action directly using the printer&apos;s touchscreen.</source>
        <translation type="unfinished">Ya se está ejecutando otra acción. Termina esta acción directamente usando la pantalla táctil de la impresora.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="59"/>
        <source>Internal error (DBUS mapping failed), restart the printer. Contact support if the problem persists.</source>
        <translation type="unfinished">Error interno (Error en el mapeo DBUS), reinicia la impresora. Pónte en contacto con nuestro soporte técnico si el problema persiste.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="60"/>
        <source>Error, there is no file to reprint.</source>
        <translation type="unfinished">Error, no hay archivo para reimprimir.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="61"/>
        <source>The wizard did not finish successfully!</source>
        <translation type="unfinished">¡El Asistente no terminó correctamente!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="62"/>
        <source>The calibration did not finish successfully! Run the calibration again.</source>
        <translation type="unfinished">¡La calibración no finalizó correctamente! Ejecuta la calibración nuevamente.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="63"/>
        <source>The automatic UV LED calibration did not finish successfully! Run the calibration again.</source>
        <translation type="unfinished">¡La calibración automática de los LED UV no se completó correctamente! Ejecuta la calibración nuevamente.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="64"/>
        <source>UV intensity not set. Please run the UV calibration before starting a print.</source>
        <translation type="unfinished">Intensidad UV no ajustada. Ejecuta la calibración UV antes de comenzar una impresión.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="65"/>
        <source>Cannot set the update channel. Restart the printer and try again.</source>
        <translation type="unfinished">No se puede establecer el canal de actualización. Reinicia la impresora y vuelve a intentarlo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="66"/>
        <source>Cannot get the update channel. Restart the printer and try again.</source>
        <translation type="unfinished">No se puede obtener el canal de actualización. Reinicia la impresora y vuelve a intentarlo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="67"/>
        <source>The print job cancelled by the user.</source>
        <translation type="unfinished">El trabajo de impresión cancelado por el usuario.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="68"/>
        <source>Internal memory is full. Delete some of your projects first.</source>
        <translation type="unfinished">La memoria interna está llena. Primero elimina algunos de sus proyectos.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="69"/>
        <source>The admin menu is not available.</source>
        <translation type="unfinished">El menú admin no está disponible.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="70"/>
        <source>Cannot find the selected file!</source>
        <translation type="unfinished">¡No se puede encontrar el archivo seleccionado!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="71"/>
        <source>File has an invalid extension! See the article for supported file extensions.</source>
        <translation type="unfinished">¡El archivo tiene una extensión no válida! Consulta el artículo para conocer las extensiones de archivo compatibles.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="72"/>
        <source>File already exists! Delete it in the printer first and try again.</source>
        <translation type="unfinished">¡El archivo ya existe! Bórralo en la impresora primero y vuelve a intentarlo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="73"/>
        <source>The project file is invalid!</source>
        <translation type="unfinished">¡El archivo del proyecto no es válido!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="74"/>
        <source>This Wizard cannot be canceled, finish the steps first.</source>
        <translation type="unfinished">Este Asistente no puede ser cancelado, finaliza los pasos primero.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="75"/>
        <source>Examples (any projects) are missing in the user storage. Redownload them from the &apos;Settings&apos; menu.</source>
        <translation type="unfinished">Faltan ejemplos (cualquier proyecto) en el almacenamiento del usuario. Vuelve a descargarlos desde el menú &apos;Ajustes&apos;.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="76"/>
        <source>Failed to load fans and LEDs factory calibration.</source>
        <translation type="unfinished">Falló la carga de la calibración de fábrica de ventiladores y  de LEDS.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="77"/>
        <source>Failed to serialize Wizard data. Restart the printer and try again.</source>
        <translation type="unfinished">Error al serializar los datos del Asistente. Reinicia la impresora y vuelve a intentarlo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="78"/>
        <source>Failed to save Wizard data. Restart the printer and try again.</source>
        <translation type="unfinished">No se pudieron guardar los datos del Asistente. Reinicia la impresora y vuelve a intentarlo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="79"/>
        <source>Serial numbers in wrong format! A64: %(a64)s MC: %(mc)s Please contact tech support!</source>
        <translation type="unfinished">¡Números de serie en formato incorrecto! A64: %(a64)s MC: %(mc)s ¡Ponte en contacto con el soporte técnico!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="80"/>
        <source>No USB storage present</source>
        <translation type="unfinished">Almac. USB no presente</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="81"/>
        <source>Failed to change the log level (detail). Restart the printer and try again.</source>
        <translation type="unfinished">No se pudo cambiar el nivel de registro (detalle). Reinicia la impresora y vuelve a intentarlo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="82"/>
        <source>Saving the new factory default value failed. Restart the printer and try again.</source>
        <translation type="unfinished">No se pudo guardar el nuevo valor predeterminado de fábrica. Reinicia la impresora y vuelve a intentarlo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="83"/>
        <source>Error displaying test image.</source>
        <translation type="unfinished">Error al mostrar la imagen de prueba.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="84"/>
        <source>No calibration data to show!</source>
        <translation type="unfinished">No hay datos de calibración para mostrar!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="85"/>
        <source>Data is from unknown UV LED sensor!</source>
        <translation type="unfinished">¡Datos de un sensor UV desconocido!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="86"/>
        <source>The update of the firmware failed! Restart the printer and try again.</source>
        <translation type="unfinished">¡Falló la actualización del firmware! Reinicia la impresora y vuelve a intentarlo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="87"/>
        <source>No display usage data to show</source>
        <translation type="unfinished">No hay datos de uso de la pantalla para mostrar</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="88"/>
        <source>Failed to set hostname</source>
        <translation type="unfinished">Fallo al establecer nombre de host</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="89"/>
        <source>Cannot import profile</source>
        <translation type="unfinished">No se puede importar el perfil</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="90"/>
        <source>Cannot export profile</source>
        <translation type="unfinished">No se puede exportar el perfil</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="91"/>
        <source>Opening the project failed. The file is possibly corrupted. Please re-slice or re-export the project and try again.</source>
        <translation type="unfinished">No se pudo abrir el proyecto. Es posible que el archivo esté dañado. Relamina o reexporta el proyecto, vuelve a intentarlo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="92"/>
        <source>The project must have at least one layer</source>
        <translation type="unfinished">El proyecto debe tener al menos una capa</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="93"/>
        <source>Opening the project failed. The file is corrupted. Please re-slice or re-export the project and try again.</source>
        <translation type="unfinished">No se puede abrir el proyecto. El archivo está dañado. Vuelve a laminar o exportar el proyecto y vuelve a intentarlo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="94"/>
        <source>Analysis of the project failed</source>
        <translation type="unfinished">El análisis del proyecto falló</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="95"/>
        <source>Calibration project is invalid</source>
        <translation type="unfinished">El proyecto de calibración no es válido</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="96"/>
        <source>This project was prepared for a different printer</source>
        <translation type="unfinished">Este proyecto se preparó para una impresora diferente</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="97"/>
        <source>Removing this project is not possible. The project is locked by a print job.</source>
        <translation type="unfinished">No es posible eliminar este proyecto. El proyecto está bloqueado por un trabajo de impresión.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="98"/>
        <source>The directory is not empty.</source>
        <translation type="unfinished">El directorio no está vacío.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="99"/>
        <source>The language is not set. Go to Settings -&gt; Language &amp; Time -&gt; Set Language and pick preferred language.</source>
        <translation type="unfinished">No se ha establecido el idioma. Ve a Ajustes -&gt; Idioma &amp; Hora -&gt; Establecer Idioma y escoje tu idioma preferido.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="100"/>
        <source>Exposure screen that is currently connected has already been used on this printer. This screen was last used for approximately %(counter_h)d hours.

If you do not want to use this screen: turn the printer off, replace the screen and turn the printer back on.</source>
        <translation type="unfinished">La pantalla de exposición que está actualmente conectada ya se ha utilizado en esta impresora. Esta pantalla se utilizó por última vez durante aproximadamente %(counter_h)d horas.

Si no desea utilizar esta pantalla: apaga la impresora, cambia la pantalla y vuelve a encender la impresora.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="101"/>
        <source>The printer has booted from an alternative slot due to failed boot attempts using the primary slot.
Update the printer with up-to-date firmware ASAP to recover the primary slot.
This usually happens after a failed update, or due to a hardware failure. Printer settings may have been reset.</source>
        <translation type="unfinished">La impresora se ha iniciado desde una ranura alternativa debido a intentos fallidos de inicio utilizando la ranura principal.
Actualiza la impresora con firmware actualizado lo antes posible para recuperar la ranura principal.
Esto suele ocurrir después de una actualización fallida o debido a un fallo de hardware. Es posible que se hayan restablecido los ajustes de la impresora.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="102"/>
        <source>There is no warning</source>
        <translation type="unfinished">No hay ninguna advertencia</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="103"/>
        <source>An unknown warning has occured. Restart the printer and try again. Contact our tech support if the problem persists.</source>
        <translation type="unfinished">Se ha producido una advertencia desconocida. Reinicia la impresora y vuelve a intentarlo. Pónte en contacto con nuestro soporte técnico si el problema persiste.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="104"/>
        <source>The ambient temperature is too high, the print can continue, but it might fail.</source>
        <translation type="unfinished">La temperatura ambiente es demasiado alta, la impresión puede continuar, pero puede fallar.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="105"/>
        <source>The ambient temperature is too low, the print can continue, but it might fail.</source>
        <translation type="unfinished">La temperatura ambiente es demasiado baja, la impresión puede continuar, pero puede fallar.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="106"/>
        <source>The internal memory is full, project cannot be copied. You can continue printing. However, you must not remove the USB drive during the print, otherwise the process will fail.</source>
        <translation type="unfinished">La memoria interna está llena, el proyecto no se puede copiar. Puedes seguir imprimiendo. Sin embargo, no debes quitar la unidad USB durante la impresión, de lo contrario, el proceso fallará.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="107"/>
        <source>The model was sliced for a different printer model. Reslice the model using the correct settings.</source>
        <translation type="unfinished">El modelo fue laminado para un modelo de impresora diferente. Relamina el modelo usando la configuración correcta.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="108"/>
        <source>The amount of resin in the tank is not enough for the current project. Adding more resin will be required during the print.</source>
        <translation type="unfinished">La cantidad de resina en el tanque no es suficiente para el proyecto actual. Será necesario añadir más resina durante la impresión.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="109"/>
        <source>The print parameters are out of range of the printer, the system can try to fix the project. Proceed?</source>
        <translation type="unfinished">Los parámetros de impresión están fuera del rango de la impresora, el sistema puede intentar arreglar el proyecto. ¿Continuar?</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="110"/>
        <source>Per-partes print not available.</source>
        <translation type="unfinished">Impresión Per Partes no disponible.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="111"/>
        <source>Print mask is missing.</source>
        <translation type="unfinished">Falta la máscara de impresión.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="112"/>
        <source>Object was cropped because it does not fit the print area.</source>
        <translation type="unfinished">La pieza se recortó porque no se ajusta al área de impresión.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="113"/>
        <source>The model was sliced for a different printer variant %(project_variant)s. Your printer variant is %(printer_variant)s.</source>
        <translation type="unfinished">El modelo fue lamiando para una variante de impresora diferente %(project_variant)s. Tu variante de impresora es %(printer_variant)s.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="114"/>
        <source>Measured resin volume is too low. The print can continue, however, a refill might be required.</source>
        <translation type="unfinished">El volumen de resina medido es demasiado bajo. La impresión puede continuar, sin embargo, es posible que se requiera una recarga.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="116"/>
        <source>Incorrect RPM reading of the %(failed_fans_text)s fan. Please check its wiring. The print may continue, however, there&apos;s a risk of overheating.</source>
        <translation type="unfinished">Lectura incorrecta de las RPM del ventilador %(failure_fans_text)s. Comprueba su cableado. La impresión puede continuar, sin embargo, hay riesgo de sobrecalentamiento.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="119"/>
        <source>TILT HOMING FAILED</source>
        <translation type="unfinished">HOMING INCLINACIÓN FALLIDO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="120"/>
        <source>TOWER HOMING FAILED</source>
        <translation type="unfinished">HOMING TORRE FALLIDO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="121"/>
        <source>TOWER MOVING FAILED</source>
        <translation type="unfinished">MOVIMIENTO TORRE FALLIDO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="122"/>
        <source>FAN FAILURE</source>
        <translation type="unfinished">FALLO DEL VENTILADOR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="123"/>
        <source>RESIN TOO LOW</source>
        <translation type="unfinished">NVEL DE RESINA MUY BAJO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="124"/>
        <source>RESIN TOO HIGH</source>
        <translation type="unfinished">NIVEL DE RESINA MUY ALTO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="125"/>
        <source>CALIBRATION ERROR</source>
        <translation type="unfinished">ERROR DE CALIBRACIÓN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="126"/>
        <source>TOWER ENDSTOP NOT REACHED</source>
        <translation type="unfinished">ENDSTOP TORRE NO ALCANZADO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="127"/>
        <source>TILT ENDSTOP NOT REACHED</source>
        <translation type="unfinished">ENDSTOP INCLINACIÓN NO ALCANZADO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="128"/>
        <source>TOWER AXIS CHECK FAILED</source>
        <translation type="unfinished">COMPROBACIÓN EJE TORRE FALLIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="129"/>
        <source>TILT AXIS CHECK FAILED</source>
        <translation type="unfinished">COMPROBACIÓN EJE INCLINACIÓN FALLIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="130"/>
        <source>DISPLAY TEST FAILED</source>
        <translation type="unfinished">TEST DE PANTALLA FALLIDO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="131"/>
        <source>INVALID TILT ALIGN POSITION</source>
        <translation type="unfinished">POSICIÓN DE ALINEACIÓN DE INCLINACIÓN INVÁLIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="132"/>
        <source>FAN RPM OUT OF TEST RANGE</source>
        <translation type="unfinished">RPM DEL VENTILADOR FUERA DEL RANGO DE PRUEBA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="133"/>
        <source>TOWER POSITION ERROR</source>
        <translation type="unfinished">ERROR POSICIÓN TORRE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="134"/>
        <source>RESIN MEASURING FAILED</source>
        <translation type="unfinished">FALLÓ LA MEDICIÓN DE RESINA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="135"/>
        <source>TEMPERATURE SENSOR FAILED</source>
        <translation type="unfinished">SENSOR DE TEMPERATURA FALLIDO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="136"/>
        <source>UVLED HEAT SINK FAILED</source>
        <translation type="unfinished">FALLO DISIPADOR DE LED UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="137"/>
        <source>A64 OVERHEAT</source>
        <translation type="unfinished">SOBRECALENTAMIENTO A64</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="138"/>
        <source>TEMPERATURE OUT OF RANGE</source>
        <translation type="unfinished">TEMPERATURA FUERA DE RANGO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="139"/>
        <source>UV LED TEMP. ERROR</source>
        <translation type="unfinished">ERROR TEMP. LED UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="140"/>
        <source>MC WRONG REVISION</source>
        <translation type="unfinished">REVISIÓN INCORRECTA MC</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="141"/>
        <source>UNEXPECTED MC ERROR</source>
        <translation type="unfinished">ERROR MC INESPERADO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="142"/>
        <source>RESIN SENSOR ERROR</source>
        <translation type="unfinished">ERROR SENSOR DE RESINA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="143"/>
        <source>PRINTER NOT UV CALIBRATED</source>
        <translation type="unfinished">UV DE IMPRESORA NO CALIBRADO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="144"/>
        <source>UVLED VOLTAGE ERROR</source>
        <translation type="unfinished">ERROR DE VOLTAJE DE LED UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="145"/>
        <source>SPEAKER TEST FAILED</source>
        <translation type="unfinished">TEST ALTAVOZ FALLIDO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="146"/>
        <source>UV LED CALIBRATOR NOT DETECTED</source>
        <translation type="unfinished">CALIBRADOR LED UV NO DETECTADO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="147"/>
        <source>UV LED CALIBRATOR CONNECTION ERROR</source>
        <translation type="unfinished">ERROR DE CONEXIÓN CALIBRADOR LED UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="148"/>
        <source>UV LED CALIBRATOR LINK ERROR</source>
        <translation type="unfinished">ERROR DE ENLACE DEL CALIBRADOR LED UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="149"/>
        <source>UV LED CALIBRATOR ERROR</source>
        <translation type="unfinished">ERROR DE CALIBRADOR LED UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="150"/>
        <source>UV LED CALIBRATOR READINGS ERROR</source>
        <translation type="unfinished">ERROR DE LECTURAS DEL CALIBRADOR LED UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="151"/>
        <source>UV LED CALIBRATOR UNKNONW ERROR</source>
        <translation type="unfinished">ERROR DESCONOCIDO DEL CALIBRADOR LED UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="152"/>
        <source>UV INTENSITY TOO HIGH</source>
        <translation type="unfinished">INTENSIDAD UV DEMASIADO ALTA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="153"/>
        <source>UV INTENSITY TOO LOW</source>
        <translation type="unfinished">INTENSIDAD UV DEMASIADO BAJA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="154"/>
        <source>UV CALIBRATION ERROR</source>
        <translation type="unfinished">ERROR DE CALIBRACIÓN UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="155"/>
        <source>BOOSTER BOARD PROBLEM</source>
        <translation type="unfinished">PROBLEMA EN LA PLACA BOOSTER</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="156"/>
        <source>Disconnected UV LED panel</source>
        <translation type="unfinished">Panel LED UV desconectado</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="157"/>
        <source>Broken UV LED panel</source>
        <translation type="unfinished">Panel LED UV roto</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="158"/>
        <source>Unknown printer model</source>
        <translation type="unfinished">Modelo de impresora desconocido</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="159"/>
        <source>MQTT UPLOAD FAILED</source>
        <translation type="unfinished">FALLO EN LA CARGA DE MQTT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="160"/>
        <source>NO INTERNET CONNECTION</source>
        <translation type="unfinished">SIN CONEXIÓN A INTERNET</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="161"/>
        <source>CONNECTION FAILED</source>
        <translation type="unfinished">CONEXIÓN FALLIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="162"/>
        <source>DOWNLOAD FAILED</source>
        <translation type="unfinished">DESCARGA FALLIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="163"/>
        <source>INVALID API KEY</source>
        <translation type="unfinished">CLAVE API INVÁLIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="164"/>
        <source>UNAUTHORIZED</source>
        <translation type="unfinished">NO AUTORIZADO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="165"/>
        <source>REMOTE API ERROR</source>
        <translation type="unfinished">ERROR API REMOTA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="166"/>
        <source>PRINTER IS OK</source>
        <translation type="unfinished">LA IMPRESORA ESTÁ BIEN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="167"/>
        <source>UNEXPECTED ERROR</source>
        <translation type="unfinished">ERROR INESPERADO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="168"/>
        <source>PRELOAD FAILED</source>
        <translation type="unfinished">PRECARGA FALLIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="169"/>
        <source>OPENING PROJECT FAILED</source>
        <translation type="unfinished">APERTURA DEL PROYECTO FALLIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="170"/>
        <source>CONFIG FILE READ ERROR</source>
        <translation type="unfinished">ERROR AL LEER ARCHIVO CONFIG</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="171"/>
        <source>PRINTER IS BUSY</source>
        <translation type="unfinished">LA IMPRESORA ESTÁ OCUPADA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="172"/>
        <source>INTERNAL ERROR</source>
        <translation type="unfinished">ERROR INTERNO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="173"/>
        <source>NO FILE TO REPRINT</source>
        <translation type="unfinished">NO HAY ARCHIVO PARA REIMPRIMIR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="174"/>
        <source>WIZARD FAILED</source>
        <translation type="unfinished">FALLO DEL ASISTENTE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="175"/>
        <source>CALIBRATION FAILED</source>
        <translation type="unfinished">CALIBRACIÓN FALLIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="176"/>
        <source>UV CALIBRATION FAILED</source>
        <translation type="unfinished">CALIBRACIÓN UV FALLIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="177"/>
        <source>UV INTENSITY ERROR</source>
        <translation type="unfinished">ERROR DE INTENSIDAD UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="178"/>
        <source>SETTING UPDATE CHANNEL FAILED</source>
        <translation type="unfinished">AJUSTE DEL CANAL ACTUALIZACIÓN FALLIDO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="179"/>
        <source>UPDATE CHANNEL FAILED</source>
        <translation type="unfinished">ACTUALIZACIÓN DE CANAL FALLIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="180"/>
        <source>PRINT JOB CANCELLED</source>
        <translation type="unfinished">TRABAJO DE IMPRESIÓN CANCELADO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="181"/>
        <source>INTERNAL MEMORY FULL</source>
        <translation type="unfinished">MEMORIA INTERNA LLENA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="182"/>
        <source>ADMIN NOT AVAILABLE</source>
        <translation type="unfinished">ADMIN NO DISPONIBLE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="183"/>
        <source>FILE NOT FOUND</source>
        <translation type="unfinished">ARCHIVO NO ENCOTRADO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="184"/>
        <source>INVALID FILE EXTENSION</source>
        <translation type="unfinished">EXTENSIÓN DE ARCHIVO INVÁLIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="185"/>
        <source>FILE ALREADY EXISTS</source>
        <translation type="unfinished">EL ARCHIVO YA EXISTE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="186"/>
        <source>INVALID PROJECT</source>
        <translation type="unfinished">PROYECTO INVÁLIDO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="187"/>
        <source>YOU SHALL NOT PASS</source>
        <translation type="unfinished">NO PASARÁS</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="188"/>
        <source>PRINT EXAMPLES MISSING</source>
        <translation type="unfinished">FALTAN LOS EJEMPLOS DE IMPRESIÓN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="189"/>
        <source>CALIBRATION LOAD FAILED</source>
        <translation type="unfinished">CARGA DE CALIBRACIÓN FALLIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="190"/>
        <source>DATA PREPARATION FAILURE</source>
        <translation type="unfinished">FALLO EN LA PREPARACIÓN DE DATOS</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="191"/>
        <source>WIZARD DATA FAILURE</source>
        <translation type="unfinished">FALLO DE DATOS DEL ASISTENTE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="192"/>
        <source>SERIAL NUMBER ERROR</source>
        <translation type="unfinished">ERROR NÚMERO DE SERIE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="193"/>
        <source>USB DRIVE NOT DETECTED</source>
        <translation type="unfinished">UNIDAD USB NO DETECTADA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="194"/>
        <source>SETTING LOG DETAIL FAILED</source>
        <translation type="unfinished">AJUSTE DEL REGISTRO DETALLADO FALLIDO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="195"/>
        <source>DATA OVERWRITE FAILED</source>
        <translation type="unfinished">SOBRESCRITURA DE DATOS FALLLIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="196"/>
        <source>DISPLAY TEST ERROR</source>
        <translation type="unfinished">ERROR TEST PANTALLA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="197"/>
        <source>NO UV CALIBRATION DATA</source>
        <translation type="unfinished">SIN DATOS DE CALIBRACIÓN UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="198"/>
        <source>UV DATA EROR</source>
        <translation type="unfinished">ERROR DE DATOS UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="199"/>
        <source>FIRMWARE UPDATE FAILED</source>
        <translation type="unfinished">ACTUALIZACIÓN DE FIRMWARE FALLIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="200"/>
        <source>Display usage error</source>
        <translation type="unfinished">Error uso de pantalla</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="201"/>
        <source>HOSTNAME ERROR</source>
        <translation type="unfinished">ERROR DE NOMBRE DE HOST</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="202"/>
        <source>PROFILE IMPORT ERROR</source>
        <translation type="unfinished">ERROR DE IMPORTACIÓN DE PERFIL</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="203"/>
        <source>PROFILE EXPORT ERROR</source>
        <translation type="unfinished">ERROR DE EXPORTACIÓN DE PERFIL</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="204"/>
        <source>CANNOT READ PROJECT</source>
        <translation type="unfinished">NO PUEDO LEER EL PROYECTO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="205"/>
        <source>NOT ENOUGHT LAYERS</source>
        <translation type="unfinished">NO HAY SUFICIENTES CAPAS</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="206"/>
        <source>PROJECT IS CORRUPTED</source>
        <translation type="unfinished">PROYECTO CORRUPTO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="207"/>
        <source>PROJECT ANALYSIS FAILED</source>
        <translation type="unfinished">ANÁLISIS DE PROYECTO FALLIDO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="208"/>
        <source>CALIBRATION PROJECT IS INVALID</source>
        <translation type="unfinished">EL PROYECTO DE CALIBRACIÓN NO ES VÁLIDO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="209"/>
        <source>WRONG PRINTER MODEL</source>
        <translation type="unfinished">MODELO DE IMPRESORA INCORRECTO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="210"/>
        <source>CANNOT REMOVE PROJECT</source>
        <translation type="unfinished">NO SE PUEDE BORRAR EL PROYECTO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="211"/>
        <source>DIRECTORY NOT EMPTY</source>
        <translation type="unfinished">DIRECTORIO NO VACÍO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="212"/>
        <source>LANGUAGE NOT SET</source>
        <translation type="unfinished">IDIOMA NO ESTABLECIDO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="213"/>
        <source>OLD EXPO PANEL</source>
        <translation type="unfinished">PANEL EXPO ANTIGUO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="214"/>
        <source>BOOTED SLOT CHANGED</source>
        <translation type="unfinished">RANURA DE ARRANQUE CAMBIADA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="215"/>
        <source>NO WARNING</source>
        <translation type="unfinished">SIN ADVERTENCIA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="216"/>
        <source>UNKNOWN WARNING</source>
        <translation type="unfinished">ADVERTENCIA DESCONOCIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="217"/>
        <source>AMBIENT TEMP. TOO HIGH</source>
        <translation type="unfinished">TEMP. AMBIENTE DEMASIADO ALTA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="218"/>
        <source>AMBIENT TEMP. TOO LOW</source>
        <translation type="unfinished">TEMP. AMBIENTE DEMASIADO BAJA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="219"/>
        <source>CAN&apos;T COPY PROJECT</source>
        <translation type="unfinished">NO SE PUEDE COPIAR EL PROYECTO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="220"/>
        <source>INCORRECT PRINTER MODEL</source>
        <translation type="unfinished">MODELO DE IMPRESORA INCORRECTO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="221"/>
        <source>NOT ENOUGH RESIN</source>
        <translation type="unfinished">NO HAY SUFICIENTE RESINA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="222"/>
        <source>PARAMETERS OUT OF RANGE</source>
        <translation type="unfinished">PARÁMETROS FUERA DE RANGO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="223"/>
        <source>PERPARTES NOAVAIL WARNING</source>
        <translation type="unfinished">AVISO NO PERMITIDO PERPARTES</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="224"/>
        <source>MASK NOAVAIL WARNING</source>
        <translation type="unfinished">AVISO MASK NOAVAIL</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="225"/>
        <source>OBJECT CROPPED WARNING</source>
        <translation type="unfinished">ADVERTENCIA DE OBJETO RECORTADO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="226"/>
        <source>PRINTER VARIANT MISMATCH WARNING</source>
        <translation type="unfinished">ADVERTENCIA DE DISCAPACIDAD DE VARIANTE DE IMPRESORA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="227"/>
        <source>RESIN LOW</source>
        <translation type="unfinished">POCA RESINA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="228"/>
        <source>FAN WARNING</source>
        <translation type="unfinished">AVISO DEL VENTILADOR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="229"/>
        <source>EXPECT OVERHEATING</source>
        <translation type="unfinished">SE ESPERA SOBRECALENTAMIENTO</translation>
    </message>
</context>
<context>
    <name>NotificationProgressDelegate</name>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="89"/>
        <source>Error: %1</source>
        <translation type="unfinished">Error: %1</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="99"/>
        <source>Storage full</source>
        <translation type="unfinished">Almacenamiento lleno</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="101"/>
        <source>File not found</source>
        <translation type="unfinished">Archivo no encontrado</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="103"/>
        <source>Already exists</source>
        <translation type="unfinished">Ya existe</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="105"/>
        <source>Invalid extension</source>
        <translation type="unfinished">Extensión inválida</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="107"/>
        <source>Can&apos;t read</source>
        <translation type="unfinished">No puedo leer</translation>
    </message>
</context>
<context>
    <name>PageAPSettings</name>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="34"/>
        <source>AP Settings</source>
        <translation type="unfinished">Ajustes AP</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="51"/>
        <source>(unchanged)</source>
        <translation type="unfinished">(sin cambios)</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="68"/>
        <source>Stop AP</source>
        <translation type="unfinished">Paro AP</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="136"/>
        <source>State:</source>
        <translation type="unfinished">Estado:</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="147"/>
        <source>Inactive</source>
        <translation type="unfinished">Inactivo</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="151"/>
        <source>SSID</source>
        <translation type="unfinished">SSID</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="173"/>
        <source>Password</source>
        <translation type="unfinished">Contraseña</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="214"/>
        <source>Show Password</source>
        <translation type="unfinished">Ver contraseña</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="230"/>
        <source>Security</source>
        <translation type="unfinished">Seguridad</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="245"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation type="unfinished">PSK debe tener al menos 8 caracteres.</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="251"/>
        <source>SSID must not be empty</source>
        <translation type="unfinished">El SSID no debe estar vacío</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="263"/>
        <source>Start AP</source>
        <translation type="unfinished">Comenzar AP</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="294"/>
        <source>Swipe for QRCode</source>
        <translation type="unfinished">Desliza para Código QR</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="372"/>
        <source>Swipe for settings</source>
        <translation type="unfinished">Desliza para ajustes</translation>
    </message>
</context>
<context>
    <name>PageAbout</name>
    <message>
        <location filename="../qml/PageAbout.qml" line="22"/>
        <source>About Us</source>
        <translation type="unfinished">Acerca de Nosotros</translation>
    </message>
    <message>
        <location filename="../qml/PageAbout.qml" line="25"/>
        <source>To find out more about us please scan the QR code or use the link below:</source>
        <translation type="unfinished">Para obtener más información sobre nosotros, escanea el código QR o utiliza el siguiente enlace:</translation>
    </message>
    <message>
        <location filename="../qml/PageAbout.qml" line="29"/>
        <source>Prusa Research a.s.</source>
        <translation type="unfinished">Prusa Research a.s.</translation>
    </message>
</context>
<context>
    <name>PageAddWifiNetwork</name>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="31"/>
        <source>Add Hidden Network</source>
        <translation type="unfinished">Añadir red oculta</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="62"/>
        <source>SSID</source>
        <translation type="unfinished">SSID</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="84"/>
        <source>PASS</source>
        <translation type="unfinished">CORRECTO</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="129"/>
        <source>Show Password</source>
        <translation type="unfinished">Ver contraseña</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="147"/>
        <source>Security</source>
        <translation type="unfinished">Seguridad</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="163"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation type="unfinished">PSK debe tener al menos 8 caracteres.</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="169"/>
        <source>SSID must not be empty.</source>
        <translation type="unfinished">El SSID no debe estar vacío.</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="182"/>
        <source>Connect</source>
        <translation type="unfinished">Conectar</translation>
    </message>
</context>
<context>
    <name>PageAdminAPI</name>
    <message>
        <location filename="../qml/PageAdminAPI.qml" line="36"/>
        <source>Admin API</source>
        <translation type="unfinished">Admin API</translation>
    </message>
</context>
<context>
    <name>PageAskForPassword</name>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="66"/>
        <source>Please, enter the correct password for network &quot;%1&quot;</source>
        <translation type="unfinished">Por favor, introduce la contraseña correcta para la red &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="82"/>
        <source>Password</source>
        <translation type="unfinished">Contraseña</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="92"/>
        <source>(unchanged)</source>
        <translation type="unfinished">(sin cambios)</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="128"/>
        <source>Show Password</source>
        <translation type="unfinished">Ver contraseña</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="154"/>
        <source>Cancel</source>
        <translation type="unfinished">Cancelar</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="166"/>
        <source>Ok</source>
        <translation type="unfinished">Ok</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="193"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation type="unfinished">PSK debe tener al menos 8 caracteres.</translation>
    </message>
</context>
<context>
    <name>PageBasicWizard</name>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="34"/>
        <source>Wizard</source>
        <translation type="unfinished">Asistente</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="55"/>
        <source>Are you sure?</source>
        <translation type="unfinished">¿Estás seguro?</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="56"/>
        <source>Do you really want to cancel the wizard?</source>
        <translation type="unfinished">¿Realmente deseas cancelar el asistente?</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="58"/>
        <source>The machine will not work without a completed wizard procedure.</source>
        <translation type="unfinished">La impresora no funcionará sin completar el procedimiento del Asistente.</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="106"/>
        <source>Can you see the company logo on the exposure display through the cover?</source>
        <translation type="unfinished">¿Puedes ver el logotipo de la empresa en la pantalla de exposición a través de la tapa naranja?</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="108"/>
        <source>Tip: If you can&apos;t see the logo clearly, try placing a sheet of paper onto the screen.</source>
        <translation type="unfinished">Consejo: si no puedes ver el logotipo con claridad, intenta colocar una hoja de papel en la pantalla.</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="110"/>
        <source>Once you place the paper inside the printer, do not forget to CLOSE THE COVER!</source>
        <translation type="unfinished">Una vez que coloques el papel dentro de la impresora, ¡no olvides cerrar la tapa!</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="120"/>
        <source>Carefully peel off the protective sticker from the exposition display.</source>
        <translation type="unfinished">Despega con cuidado el adhesivo de protección de la pantalla de exposición.</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="129"/>
        <source>Wizard finished sucessfuly!</source>
        <translation type="unfinished">El Asistente concluyó con éxito!</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="137"/>
        <source>Wizard failed</source>
        <translation type="unfinished">Fallo del asistente</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="145"/>
        <source>Wizard canceled</source>
        <translation type="unfinished">Asistente cancelado</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="153"/>
        <source>Wizard stopped due to a problem, retry?</source>
        <translation type="unfinished">El asistente se detuvo debido a un problema, ¿repetimos?</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="162"/>
        <source>Cover</source>
        <translation type="unfinished">Tapa</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="163"/>
        <source>Close the cover.</source>
        <translation type="unfinished">Cierra la tapa.</translation>
    </message>
</context>
<context>
    <name>PageCalibrationTilt</name>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="28"/>
        <source>Tank Movement</source>
        <translation type="unfinished">Movimiento del tanque</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="82"/>
        <source>Move Up</source>
        <translation type="unfinished">Mover Arriba</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="91"/>
        <source>Move Down</source>
        <translation type="unfinished">Mover Abajo</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="105"/>
        <source>Tilt position:</source>
        <translation type="unfinished">Posición de inclinación:</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="116"/>
        <source>Done</source>
        <translation type="unfinished">Hecho</translation>
    </message>
</context>
<context>
    <name>PageCalibrationWizard</name>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="31"/>
        <source>Printer Calibration</source>
        <translation type="unfinished">Calibración de la impresora</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="49"/>
        <source>If the platform is not yet inserted, insert it according to the picture at 0° degrees angle and secure it with the black knob.</source>
        <translation type="unfinished">Si la plataforma aún no está colocada, colócala de acuerdo con la imagen en un ángulo de 0° y fíjala con el pomo negro.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="60"/>
        <source>Loosen the small screw on the cantilever with an allen key. Be careful not to unscrew it completely.</source>
        <translation type="unfinished">Afloja el tornillo pequeño del soporte de la plataforma de impresión con una llave Allen. Ten cuidado de no desenroscarlo completamente.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="62"/>
        <source>Some SL1 printers may have two screws - see the handbook for more information.</source>
        <translation type="unfinished">Algunas impresoras SL1 pueden tener dos tornillos - consulta el manual para obtener más información.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="75"/>
        <source>Unscrew the tank, rotate it by 90° and place it flat across the tilt bed. Remove the tank screws completely.</source>
        <translation type="unfinished">Afloja los tornillos del tanque, gíralo 90° y colócalo plano sobre la plataforma basculante. Quita los tornillos del tanque por completo.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="84"/>
        <source>In the next step, move the tilt bed up/down until it is in direct contact with the resin tank. The tilt bed and tank have to be aligned in a perfect line.</source>
        <translation type="unfinished">El el siguiente paso, inclina la base hasta que toque el tanque de resina. La base y el tanque deberían de estar perfectamente alineados.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="96"/>
        <source>Set the tilt bed against the resin tank</source>
        <translation type="unfinished">Ajusta la base basculante con las flechas hasta que toque el tanque</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="105"/>
        <source>Make sure that the platfom, tank and tilt bed are PERFECTLY clean.</source>
        <translation type="unfinished">Comprueba que la plataforma, el tanque y la plataforma basculante están PERFECTAMENTE limpios.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="107"/>
        <source>The image is for illustration purposes only.</source>
        <translation type="unfinished">La imagen es solo para fines ilustrativos.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="116"/>
        <source>Return the tank to the original position and secure it with tank screws. Make sure that you tighten both screws evenly and with the same amount of force.</source>
        <translation type="unfinished">Coloca el tanque en la posición original y sujétalo con los tornillos. Comprueba que aprietas ambos tornillos a la vez y con la misma fuerza.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="125"/>
        <source>Check whether the platform is properly secured with the black knob(hold it in place and tighten the knob if needed).</source>
        <translation type="unfinished">Comprueba si la plataforma está asegurada correctamente con el pomo negro (mantenla en su lugar y aprieta el pomo si es necesario).</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="127"/>
        <source>Do not rotate the platform. It should be positioned according to the picture.</source>
        <translation type="unfinished">No gires la plataforma. Debería estar colocada como en la imagen.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="141"/>
        <source>Close the cover.</source>
        <translation type="unfinished">Cierra la tapa.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="155"/>
        <source>Adjust the platform so it is aligned with the exposition display.</source>
        <translation type="unfinished">Ajuste la plataforma para que esté alineada con la pantalla de exposición.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="157"/>
        <source>Front edges of the platform and exposition display need to be parallel.</source>
        <translation type="unfinished">Los bordes frontales de la plataforma y de la pantalla de exposición deben estar paralelos.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="170"/>
        <source>Hold the platform still with one hand and apply a slight downward force on it, so it maintains good contact with the screen.


Next, use an Allen key to tighten the screw on the cantilever.

Then release the platform.</source>
        <translation type="unfinished">Sostén la plataforma quieta con una mano y aplica una ligera fuerza hacia abajo para que mantenga ejerza un buen contacto con la pantalla.


Luego, usa una llave Allen para apretar el tornillo en el cantilever.

Después suelta la plataforma.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="173"/>
        <source>Tighten the small screw on the cantilever with an allen key.</source>
        <translation type="unfinished">Aprieta el tornillo pequeño del soporte de la plataforma de impresión con una llave Allen.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="175"/>
        <source>Some SL1 printers may have two screws - tighten them evenly, little by little. See the handbook for more information.</source>
        <translation type="unfinished">Algunas impresoras SL1 pueden tener dos tornillos; apriétalos de manera uniforme, poco a poco. Consulta el manual para obtener más información.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="190"/>
        <source>Tilt settings for Prusa Slicer:</source>
        <translation type="unfinished">Configuración de inclinación para PrusaSlicer:</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="192"/>
        <source>Tilt time fast: %1 s</source>
        <translation type="unfinished">Tiempo de inclinación rápido:%1 s</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="194"/>
        <source>Tilt time slow: %1 s</source>
        <translation type="unfinished">Tiempo de inclinación lento: %1 s</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="196"/>
        <source>Area fill: %1 %</source>
        <translation type="unfinished">Relleno de área:%1%</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="198"/>
        <source>All done, happy printing!</source>
        <translation type="unfinished">¡Todo terminado, felices impresiones!</translation>
    </message>
</context>
<context>
    <name>PageChange</name>
    <message>
        <location filename="../qml/PageChange.qml" line="36"/>
        <source>Print Settings</source>
        <translation type="unfinished">Ajustes de Impresión</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="64"/>
        <source>Exposure</source>
        <translation type="unfinished">Exposición</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="81"/>
        <location filename="../qml/PageChange.qml" line="107"/>
        <location filename="../qml/PageChange.qml" line="129"/>
        <source>second</source>
        <translation type="unfinished">segundo</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="87"/>
        <source>Exposure Time Incr.</source>
        <translation type="unfinished">Incremento tiempo exp.</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="112"/>
        <source>First Layer Expo.</source>
        <translation type="unfinished">Expo. Primera Capa</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="134"/>
        <source>Print Profile</source>
        <translation type="unfinished">Perfil de Impresión</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="145"/>
        <source>Faster</source>
        <comment>Default print profile. Printer behaves as before.</comment>
        <extracomment>Default print profile. Printer behaves as before.</extracomment>
        <translation type="unfinished">Rápido</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="147"/>
        <source>Slower</source>
        <comment>Safe print profile with slow tilts and pause before each exposure.</comment>
        <extracomment>Safe print profile with slow tilts and pause before each exposure.</extracomment>
        <translation type="unfinished">Lento</translation>
    </message>
</context>
<context>
    <name>PageConfirm</name>
    <message>
        <location filename="../qml/PageConfirm.qml" line="120"/>
        <source>Swipe for a picture</source>
        <translation type="unfinished">Desliza para ver imagen</translation>
    </message>
    <message>
        <location filename="../qml/PageConfirm.qml" line="187"/>
        <source>Continue</source>
        <translation type="unfinished">Continuar</translation>
    </message>
</context>
<context>
    <name>PageConnectHiddenNetwork</name>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="34"/>
        <source>Hidden Network</source>
        <translation type="unfinished">Red oculta</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="62"/>
        <source>SSID</source>
        <translation type="unfinished">SSID</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="78"/>
        <source>Password</source>
        <translation type="unfinished">Contraseña</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="121"/>
        <source>Show Password</source>
        <translation type="unfinished">Ver contraseña</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="128"/>
        <source>Security</source>
        <translation type="unfinished">Seguridad</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="145"/>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="153"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation type="unfinished">PSK debe tener al menos 8 caracteres.</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="191"/>
        <source>Working...</source>
        <translation type="unfinished">Trabajando...</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="202"/>
        <source>Connection to %1 failed.</source>
        <translation type="unfinished">Conexión a %1 fallida.</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="213"/>
        <source>Connected to %1</source>
        <translation type="unfinished">Conectado a %1</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="241"/>
        <source>Start AP</source>
        <translation type="unfinished">Comenzar AP</translation>
    </message>
</context>
<context>
    <name>PageContinue</name>
    <message>
        <location filename="../qml/PageContinue.qml" line="27"/>
        <location filename="../qml/PageContinue.qml" line="59"/>
        <source>Continue</source>
        <translation type="unfinished">Continuar</translation>
    </message>
</context>
<context>
    <name>PageCoolingDown</name>
    <message>
        <location filename="../qml/PageCoolingDown.qml" line="27"/>
        <location filename="../qml/PageCoolingDown.qml" line="37"/>
        <source>UV LED OVERHEAT!</source>
        <translation type="unfinished">¡SOBRECALENTAMIENTO DE LOS LED UV!</translation>
    </message>
    <message>
        <location filename="../qml/PageCoolingDown.qml" line="40"/>
        <source>Cooling down</source>
        <translation type="unfinished">Enfriando</translation>
    </message>
    <message>
        <location filename="../qml/PageCoolingDown.qml" line="40"/>
        <source>Temperature is %1 C</source>
        <translation type="unfinished">La temperatura es %1 C</translation>
    </message>
</context>
<context>
    <name>PageDisplayTest</name>
    <message>
        <location filename="../qml/PageDisplayTest.qml" line="31"/>
        <source>Display Test</source>
        <translation type="unfinished">Test de Pantalla</translation>
    </message>
</context>
<context>
    <name>PageDisplaytestWizard</name>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="31"/>
        <source>Display Test</source>
        <translation type="unfinished">Test de Pantalla</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="47"/>
        <source>Welcome to the display wizard.</source>
        <translation type="unfinished">Bienvenido al asistente de la pantalla.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="49"/>
        <source>This procedure will help you make sure that your exposure display is working correctly.</source>
        <translation type="unfinished">Este procedimiento te ayudará a asegurarte de que tu pantalla de exposición funciona correctamente.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="57"/>
        <source>Please unscrew and remove the resin tank.</source>
        <translation type="unfinished">Por favor desatornilla y retira el tanque de resina.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="66"/>
        <source>Loosen the black knob and remove the platform.</source>
        <translation type="unfinished">Afloja el pomo negro  y retira la plataforma.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="75"/>
        <source>Close the cover.</source>
        <translation type="unfinished">Cierra la tapa.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="88"/>
        <source>All done, happy printing!</source>
        <translation type="unfinished">¡Todo terminado, felices impresiones!</translation>
    </message>
</context>
<context>
    <name>PageDowngradeWizard</name>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="31"/>
        <source>Hardware Downgrade</source>
        <translation type="unfinished">Degradar hardware</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="45"/>
        <source>SL1 components detected (downgrade from SL1S).</source>
        <translation type="unfinished">Componentes de SL1 detectados (degradación de SL1S).</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="46"/>
        <source>To complete the downgrade procedure, printer needs to clear the configuration and reboot.</source>
        <translation type="unfinished">Para completar el procedimiento de degradación, la impresora debe borrar la configuración y reiniciarse.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="47"/>
        <source>Proceed?</source>
        <translation type="unfinished">¿Continuar?</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="56"/>
        <source>The printer will power off now.</source>
        <translation type="unfinished">Ahora la impresora se apagará.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="57"/>
        <source>Reassemble SL1S components and power on the printer. This will restore the original state.</source>
        <translation type="unfinished">Vuelve a montar las piezas de la SL1S y enciende la impresora. Esto la devolverá al estado original.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="66"/>
        <source>Please note that downgrading is not supported. 

Downgrading your printer will erase your UV calibration and your printer will not work properly. 

You will need to recalibrate it using an external UV calibrator.</source>
        <translation type="unfinished">Tenga en cuenta que no se admite la degradación.

La degradación de su impresora borrará su calibración UV y su impresora no funcionará correctamente.

Deberá volver a calibrarla con un calibrador UV externo.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="74"/>
        <source>Current configuration is going to be cleared now.</source>
        <translation type="unfinished">La configuración actual se borrará ahora.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="75"/>
        <source>The printer will ask for the inital setup after reboot.</source>
        <translation type="unfinished">La impresora solicitará la configuración inicial después de reiniciar.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="84"/>
        <source>Use only the metal resin tank supplied. Using the different resin tank may cause resin to spill and damage your printer!</source>
        <translation type="unfinished">Usa únicamente el tanque de resina metálica suministrado. ¡El uso de otros tanques de resina puede hacer que la resina se derrame y dañe tu impresora!</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="93"/>
        <source>Only use the platform supplied. Using a different platform may cause resin to spill and damage your printer!</source>
        <translation type="unfinished">Emplea solamente la plataforma suministrada. Si usas otra, ¡podrías causar vertidos de resina y daños a tu impresora!</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="101"/>
        <source>Downgrade done. In the next step, the printer will be restarted.</source>
        <translation type="unfinished">Degradado de firmware terminado. En el siguiente paso se reiniciará la impresora.</translation>
    </message>
</context>
<context>
    <name>PageDownloadingExamples</name>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="10"/>
        <source>Examples</source>
        <translation type="unfinished">Ejemplos</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="54"/>
        <source>Downloading examples...</source>
        <translation type="unfinished">Descargando ejemplos ...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="122"/>
        <source>Initializing...</source>
        <translation type="unfinished">Inicializando...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="123"/>
        <source>Downloading ...</source>
        <translation type="unfinished">Descargando ...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="124"/>
        <source>Copying...</source>
        <translation type="unfinished">Copiando...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="125"/>
        <source>Unpacking...</source>
        <translation type="unfinished">Desembalando...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="126"/>
        <source>Done.</source>
        <translation type="unfinished">Hecho.</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="127"/>
        <source>Cleanup...</source>
        <translation type="unfinished">Limpiar...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="128"/>
        <source>Failure.</source>
        <translation type="unfinished">Fallo.</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="131"/>
        <source>Unknown state.</source>
        <translation type="unfinished">Estado desconocido.</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="139"/>
        <source>View Examples</source>
        <translation type="unfinished">Ver ejemplos</translation>
    </message>
</context>
<context>
    <name>PageError</name>
    <message>
        <location filename="../qml/PageError.qml" line="33"/>
        <source>Error</source>
        <translation type="unfinished">Error</translation>
    </message>
    <message>
        <location filename="../qml/PageError.qml" line="42"/>
        <source>Error code:</source>
        <translation type="unfinished">Código de error:</translation>
    </message>
</context>
<context>
    <name>PageEthernetSettings</name>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="34"/>
        <source>Wired Settings</source>
        <translation type="unfinished">Ajustes de red</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="104"/>
        <source>Network Info</source>
        <translation type="unfinished">Información de Red</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="110"/>
        <source>DHCP</source>
        <translation type="unfinished">DHCP</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="120"/>
        <source>IP Address</source>
        <translation type="unfinished">Dirección IP</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="133"/>
        <source>Gateway</source>
        <comment>default gateway address</comment>
        <translation type="unfinished">Gateway</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="186"/>
        <source>Apply</source>
        <translation type="unfinished">Aplicar</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="196"/>
        <source>Configuring the connection,</source>
        <translation type="unfinished">Configurando la conexión,</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="196"/>
        <source>please wait...</source>
        <translation type="unfinished">espera por favor...</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="203"/>
        <source>Revert</source>
        <comment>Turn back the changes and go back to the previous configuration.</comment>
        <translation type="unfinished">Revertir</translation>
    </message>
</context>
<context>
    <name>PageException</name>
    <message>
        <location filename="../qml/PageException.qml" line="36"/>
        <source>System Error</source>
        <translation type="unfinished">Error del Sistema</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="59"/>
        <source>Error</source>
        <translation type="unfinished">Error</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="66"/>
        <source>Error code:</source>
        <translation type="unfinished">Código de error:</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="75"/>
        <source>Swipe to proceed</source>
        <translation type="unfinished">Desliza para continuar</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="100"/>
        <source>Save Logs to USB</source>
        <translation type="unfinished">Guardar Registros al USB</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="113"/>
        <source>Send Logs to Cloud</source>
        <translation type="unfinished">Enviar Registros a la Nube</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="127"/>
        <source>Update Firmware</source>
        <translation type="unfinished">Actualizar Firmware</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="134"/>
        <source>Turn Off</source>
        <translation type="unfinished">Apagar</translation>
    </message>
</context>
<context>
    <name>PageFactoryResetWizard</name>
    <message>
        <location filename="../qml/PageFactoryResetWizard.qml" line="31"/>
        <source>Factory Reset</source>
        <translation type="unfinished">Restablecer configuración de Fábrica</translation>
    </message>
    <message>
        <location filename="../qml/PageFactoryResetWizard.qml" line="43"/>
        <source>Factory Reset done.</source>
        <translation type="unfinished">Restablecimiento de fábrica realizado.</translation>
    </message>
</context>
<context>
    <name>PageFeedme</name>
    <message>
        <location filename="../qml/PageFeedme.qml" line="34"/>
        <source>Feed Me</source>
        <translation type="unfinished">Alimentame</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="42"/>
        <source>Manual resin refill.</source>
        <translation type="unfinished">Recarga de resina manual.</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="43"/>
        <source>Refill the tank up to the 100% mark and press Done.</source>
        <translation type="unfinished">Vuelve a llenar el tanque hasta la marca del 100% y presiona Hecho.</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="44"/>
        <source>If you do not want to refill, press the Back button at top of the screen.</source>
        <translation type="unfinished">Si no deseas volver a llenar, pulsa el botón Atrás en la parte superior de la pantalla.</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="74"/>
        <source>Done</source>
        <translation type="unfinished">Hecho</translation>
    </message>
</context>
<context>
    <name>PageFileBrowser</name>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="33"/>
        <source>Projects</source>
        <translation type="unfinished">Proyectos</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="43"/>
        <source>Selftest</source>
        <translation type="unfinished">Autotest</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="44"/>
        <source>Printer Calibration</source>
        <translation type="unfinished">Calibración de la impresora</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="45"/>
        <source>UV Calibration</source>
        <translation type="unfinished">Calibración UV</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="94"/>
        <source>Local</source>
        <translation type="unfinished">Local</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="95"/>
        <source>USB</source>
        <translation type="unfinished">USB</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="96"/>
        <source>Remote</source>
        <translation type="unfinished">Remoto</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="97"/>
        <source>Previous Prints</source>
        <comment>a directory with previously printed projects</comment>
        <translation type="unfinished">Impresiones anteriores</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="98"/>
        <source>Update Bundles</source>
        <comment>a directory containing firmware update bundles</comment>
        <translation type="unfinished">Paquetes de actualización</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="148"/>
        <source>Install?</source>
        <translation type="unfinished">¿Instalar?</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="150"/>
        <source>Do you really want to install %1?</source>
        <translation type="unfinished">¿Realmente deseas instalar %1?</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="152"/>
        <source>Current system will still be available via Settings -&gt; Firmware -&gt; Downgrade</source>
        <translation type="unfinished">El sistema actual seguirá estando disponible a través de Configuración -&gt; Firmware -&gt; Degradar</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="164"/>
        <source>Calibrate?</source>
        <translation type="unfinished">¿Calibrar?</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="167"/>
        <source>The printer is not fully calibrated.</source>
        <translation type="unfinished">La impresora no está completamente calibrada.</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="169"/>
        <source>Before printing, the following steps are required to pass:</source>
        <translation type="unfinished">Antes de imprimir, se tienen que realizar los siguientes pasos:</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="173"/>
        <source>Do you want to start now?</source>
        <translation type="unfinished">¿Quieres empezar ahora?</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/PageFileBrowser.qml" line="229"/>
        <source>%n item(s)</source>
        <comment>number of items in a directory</comment>
        <translation type="unfinished">
            <numerusform>%n item</numerusform>
            <numerusform>%n items</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="241"/>
        <source>Remote</source>
        <comment>File is stored in remote storage, i.e. a cloud</comment>
        <translation type="unfinished">Remoto</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="242"/>
        <location filename="../qml/PageFileBrowser.qml" line="244"/>
        <source>Local</source>
        <comment>File is stored in a local storage</comment>
        <translation type="unfinished">Local</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="243"/>
        <source>USB</source>
        <comment>File is stored on USB flash disk</comment>
        <translation type="unfinished">USB</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="245"/>
        <source>Updates</source>
        <comment>File is in a repository of raucb files(update bundles)</comment>
        <translation type="unfinished">Actualizaciones</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="246"/>
        <source>Root</source>
        <comment>Directory is a root of the directory tree, its subdirectories are different sources of projects</comment>
        <translation type="unfinished">Raíz</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="267"/>
        <source>Unknown</source>
        <translation type="unfinished">Desconocido</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="316"/>
        <source>No usable projects were found,</source>
        <translation type="unfinished">No se encontraron proyectos utilizables,</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="318"/>
        <source>insert a USB drive or download examples in Settings -&gt; Support.</source>
        <translation type="unfinished">Conecta una unidad USB o descarga ejemplos en Configuración -&gt; Soporte.</translation>
    </message>
</context>
<context>
    <name>PageFinished</name>
    <message>
        <location filename="../qml/PageFinished.qml" line="33"/>
        <source>Finished</source>
        <translation type="unfinished">Terminado</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="109"/>
        <location filename="../qml/PageFinished.qml" line="112"/>
        <source>FINISHED</source>
        <translation type="unfinished">TERMINADO</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="110"/>
        <source>FAILED</source>
        <translation type="unfinished">HA FALLADO</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="111"/>
        <source>CANCELED</source>
        <translation type="unfinished">CANCELADO</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="136"/>
        <source>Print Time</source>
        <translation type="unfinished">Tiempo de impresión</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="157"/>
        <source>Layers</source>
        <translation type="unfinished">Capas</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="175"/>
        <source>Consumed Resin</source>
        <translation type="unfinished">Resina consumida</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="193"/>
        <source>Layer height</source>
        <translation type="unfinished">Altura Capa</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="211"/>
        <source>Layer Exposure</source>
        <translation type="unfinished">Exposición Capa</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="216"/>
        <location filename="../qml/PageFinished.qml" line="234"/>
        <source>s</source>
        <translation type="unfinished">s</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="229"/>
        <source>First Layer Exposure</source>
        <translation type="unfinished">Exposición Primera Capa</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="249"/>
        <source>Swipe to proceed</source>
        <translation type="unfinished">Desliza para continuar</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="275"/>
        <source>Home</source>
        <translation type="unfinished">Home</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="286"/>
        <source>Reprint</source>
        <translation type="unfinished">Reimprimir</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="304"/>
        <source>Turn Off</source>
        <translation type="unfinished">Apagar</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="339"/>
        <source>Loading, please wait...</source>
        <translation type="unfinished">Cargando, por favor espera...</translation>
    </message>
</context>
<context>
    <name>PageFullscreenImage</name>
    <message>
        <location filename="../qml/PageFullscreenImage.qml" line="27"/>
        <source>Fullscreen Image</source>
        <translation type="unfinished">Imagen a pantalla completa</translation>
    </message>
</context>
<context>
    <name>PageHome</name>
    <message>
        <location filename="../qml/PageHome.qml" line="29"/>
        <source>Home</source>
        <translation type="unfinished">Home</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="37"/>
        <source>Print</source>
        <translation type="unfinished">Imprimir</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="50"/>
        <source>Control</source>
        <translation type="unfinished">Control</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="57"/>
        <source>Settings</source>
        <translation type="unfinished">Ajustes</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="64"/>
        <source>Turn Off</source>
        <translation type="unfinished">Apagar</translation>
    </message>
</context>
<context>
    <name>PageLanguage</name>
    <message>
        <location filename="../qml/PageLanguage.qml" line="31"/>
        <source>Set Language</source>
        <translation type="unfinished">Establecer Idioma</translation>
    </message>
    <message>
        <location filename="../qml/PageLanguage.qml" line="79"/>
        <source>Set</source>
        <translation type="unfinished">Ajustar</translation>
    </message>
</context>
<context>
    <name>PageLogs</name>
    <message>
        <location filename="../qml/PageLogs.qml" line="29"/>
        <source>Logs export</source>
        <translation type="unfinished">Exportación de registros</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="50"/>
        <source>Log upload finished</source>
        <translation type="unfinished">Carga de registro finalizada</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="52"/>
        <source>Logs has been successfully uploaded to the Prusa server.&lt;br /&gt;&lt;br /&gt;Please contact the Prusa support and share the following code with them:</source>
        <translation type="unfinished">Los registros se han cargado correctamente en el servidor de Prusa.&lt;br /&gt;&lt;br /&gt;Contacta con el soporte de Prusa y comparte el siguiente código con ellos:</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="57"/>
        <source>Logs export finished</source>
        <translation type="unfinished">Exportación de registros finalizada</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="61"/>
        <source>Logs export canceled</source>
        <translation type="unfinished">Exportación de registros cancelada</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="64"/>
        <source>Logs export failed.

Please check your flash drive / internet connection.</source>
        <translation type="unfinished">La exportación del registro ha fallado. Por favor, comprueba tu unidad flash / conexión a Internet.</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="88"/>
        <source>Extracting log data</source>
        <translation type="unfinished">Extrayendo datos de registro</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="91"/>
        <source>Saving data to USB drive</source>
        <translation type="unfinished">Guardando datos en la memoria USB</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="92"/>
        <source>Uploading data to server</source>
        <translation type="unfinished">Subiendo datos al servidor</translation>
    </message>
</context>
<context>
    <name>PageManual</name>
    <message>
        <location filename="../qml/PageManual.qml" line="25"/>
        <source>Manual</source>
        <translation type="unfinished">Manual</translation>
    </message>
    <message>
        <location filename="../qml/PageManual.qml" line="43"/>
        <source>Scanning the QR code will load the handbook for this device.

Alternatively, use this link:</source>
        <translation type="unfinished">Al escanear el código QR, se cargará el manual de este dispositivo.

Alternativamente, use este enlace:</translation>
    </message>
</context>
<context>
    <name>PageMovementControl</name>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="27"/>
        <source>Control</source>
        <translation type="unfinished">Control</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="38"/>
        <source>Home
Platform</source>
        <translation type="unfinished">Home
Plataforma</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="49"/>
        <source>Home
Tank</source>
        <translation type="unfinished">Home
Tanque</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="60"/>
        <source>Disable
Steppers</source>
        <translation type="unfinished">Desactivar
Motores</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="69"/>
        <source>Homing the tank, please wait...</source>
        <translation type="unfinished">Homing de tanque, por favor espere...</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="69"/>
        <source>Homing the tower, please wait...</source>
        <translation type="unfinished">Homing de torre, por favor espere...</translation>
    </message>
</context>
<context>
    <name>PageNetworkEthernetList</name>
    <message>
        <location filename="../qml/PageNetworkEthernetList.qml" line="35"/>
        <source>Network</source>
        <translation type="unfinished">Redes</translation>
    </message>
</context>
<context>
    <name>PageNetworkMain</name>
    <message>
        <location filename="../qml/PageNetworkMain.qml" line="28"/>
        <source>Network</source>
        <translation type="unfinished">Redes</translation>
    </message>
    <message>
        <location filename="../qml/PageNetworkMain.qml" line="46"/>
        <source>Wi-Fi</source>
        <translation type="unfinished">Wi-Fi</translation>
    </message>
    <message>
        <location filename="../qml/PageNetworkMain.qml" line="55"/>
        <source>Ethernet</source>
        <translation type="unfinished">Ethernet</translation>
    </message>
    <message>
        <location filename="../qml/PageNetworkMain.qml" line="64"/>
        <source>Hotspot</source>
        <translation type="unfinished">Punto de acceso</translation>
    </message>
    <message>
        <location filename="../qml/PageNetworkMain.qml" line="85"/>
        <source>IP Address</source>
        <translation type="unfinished">Dirección IP</translation>
    </message>
    <message>
        <location filename="../qml/PageNetworkMain.qml" line="97"/>
        <source>Not connected to network</source>
        <translation type="unfinished">No conectado a la red</translation>
    </message>
</context>
<context>
    <name>PageNetworkWifiList</name>
    <message>
        <location filename="../qml/PageNetworkWifiList.qml" line="29"/>
        <source>Network</source>
        <translation type="unfinished">Redes</translation>
    </message>
</context>
<context>
    <name>PageNewExpoPanelWizard</name>
    <message>
        <location filename="../qml/PageNewExpoPanelWizard.qml" line="31"/>
        <source>New Exposure Panel</source>
        <translation type="unfinished">Nuevo panel exposición</translation>
    </message>
    <message>
        <location filename="../qml/PageNewExpoPanelWizard.qml" line="43"/>
        <source>New exposure screen has been detected.

The printer will ask for the inital setup (selftest and calibration) to make sure everything works correctly.</source>
        <translation type="unfinished">Se ha detectado una nueva pantalla de exposición.

La impresora solicitará la configuración inicial (selftest y calibración) para asegurarse de que todo funciona correctamente. </translation>
    </message>
</context>
<context>
    <name>PageNotificationList</name>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="37"/>
        <source>Notifications</source>
        <translation type="unfinished">Notificaciones</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="61"/>
        <source>Available update to %1</source>
        <translation type="unfinished">Actualización disponible para %1</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="79"/>
        <source>Print failed: %1</source>
        <translation type="unfinished">Impresión fallida: %1</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="80"/>
        <source>Print canceled: %1</source>
        <translation type="unfinished">Impresión cancelada: %1</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="81"/>
        <source>Print finished: %1</source>
        <translation type="unfinished">Impresión finalizada: %1</translation>
    </message>
</context>
<context>
    <name>PagePackingWizard</name>
    <message>
        <location filename="../qml/PagePackingWizard.qml" line="31"/>
        <source>Packing Wizard</source>
        <translation type="unfinished">Asistente de embalaje</translation>
    </message>
    <message>
        <location filename="../qml/PagePackingWizard.qml" line="44"/>
        <source>Packing done.</source>
        <translation type="unfinished">Embalaje completado.</translation>
    </message>
    <message>
        <location filename="../qml/PagePackingWizard.qml" line="52"/>
        <source>Insert protective foam</source>
        <translation type="unfinished">Introduce la espuma protectora</translation>
    </message>
</context>
<context>
    <name>PagePowerOffDialog</name>
    <message>
        <location filename="../qml/PagePowerOffDialog.qml" line="5"/>
        <source>Power Off?</source>
        <translation type="unfinished">¿Apagar Impresora?</translation>
    </message>
    <message>
        <location filename="../qml/PagePowerOffDialog.qml" line="6"/>
        <source>Do you really want to turn off the printer?</source>
        <translation type="unfinished">¿Realmente quieres apagar la impresora?</translation>
    </message>
    <message>
        <location filename="../qml/PagePowerOffDialog.qml" line="9"/>
        <source>Powering Off...</source>
        <translation type="unfinished">Apagando...</translation>
    </message>
</context>
<context>
    <name>PagePrePrintChecks</name>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="32"/>
        <source>Please Wait</source>
        <translation type="unfinished">Por favor espera</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="111"/>
        <source>Temperature</source>
        <translation type="unfinished">Temperatura</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="112"/>
        <source>Project</source>
        <translation type="unfinished">Proyecto</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="114"/>
        <source>Fan</source>
        <translation type="unfinished">Ventilador</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="115"/>
        <source>Cover</source>
        <translation type="unfinished">Tapa</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="116"/>
        <source>Resin</source>
        <translation type="unfinished">Resina</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="117"/>
        <source>Starting Positions</source>
        <translation type="unfinished">Posiciones de inicio</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="118"/>
        <source>Stirring</source>
        <translation type="unfinished">Removiendo</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="133"/>
        <source>Do not touch the printer!</source>
        <translation type="unfinished">¡No toques la impresora!</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="143"/>
        <source>With Warning</source>
        <translation type="unfinished">Con advertencia</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="144"/>
        <source>Disabled</source>
        <translation type="unfinished">Desactivado</translation>
    </message>
</context>
<context>
    <name>PagePrint</name>
    <message>
        <location filename="../qml/PagePrint.qml" line="146"/>
        <source>Continue?</source>
        <translation type="unfinished">Continuar?</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="149"/>
        <source>Check Warning</source>
        <translation type="unfinished">Comprueba la advertencia</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="159"/>
        <location filename="../qml/PagePrint.qml" line="275"/>
        <source>Stuck Recovery</source>
        <translation type="unfinished">Recuperación atascada</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="160"/>
        <source>The printer got stuck and needs user assistance.</source>
        <translation type="unfinished">La impresora se atascó y necesita ayuda del usuario.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="161"/>
        <source>Release the tank mechanism and press Continue.</source>
        <translation type="unfinished">Suelta el mecanismo del tanque y pulsa Continuar.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="162"/>
        <source>If you do not want to continue, press the Back button on top of the screen and the current job will be canceled.</source>
        <translation type="unfinished">Si no deseas continuar, pulsa el botón Atrás en la parte superior de la pantalla y se cancelará el trabajo actual.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="181"/>
        <source>Close Cover!</source>
        <translation type="unfinished">¡Cierra la tapa!</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="182"/>
        <source>Please, close the cover! UV radiation is harmful.</source>
        <translation type="unfinished">¡Por favor, cierra la tapa! La radiación ultravioleta es perjudicial.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="238"/>
        <source>Going up</source>
        <translation type="unfinished">Subiendo</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="238"/>
        <source>Moving platform to the top position</source>
        <translation type="unfinished">Moviendo la plataforma a la posición superior</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="241"/>
        <source>Going down</source>
        <translation type="unfinished">Bajando</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="241"/>
        <source>Moving platform to the bottom position</source>
        <translation type="unfinished">Moviendo la plataforma a la posición inferior</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="244"/>
        <source>Project</source>
        <translation type="unfinished">Proyecto</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="244"/>
        <source>Getting the printer ready to add resin. Please wait.</source>
        <translation type="unfinished">Preparando la impresora para añadir resina. Por favor, espere.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="247"/>
        <location filename="../qml/PagePrint.qml" line="301"/>
        <source>Please wait...</source>
        <translation type="unfinished">Espera por favor...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="250"/>
        <source>Cover Open</source>
        <translation type="unfinished">Tapa abierta</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="250"/>
        <source>Paused.</source>
        <translation type="unfinished">Pausado.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="250"/>
        <source>Please close the cover to continue</source>
        <translation type="unfinished">Cierra la tapa para continuar</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="262"/>
        <source>Stirring</source>
        <translation type="unfinished">Removiendo</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="262"/>
        <source>Stirring resin</source>
        <translation type="unfinished">Agitando la resina</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="265"/>
        <source>Action Pending</source>
        <translation type="unfinished">Acción pendiente</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="265"/>
        <source>Requested actions will be executed after layer finish, please wait...</source>
        <translation type="unfinished">Las acciones solicitadas se ejecutarán una vez que finalice esta capa, espera ...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="268"/>
        <source>Reading data...</source>
        <translation type="unfinished">Leyendo datos ...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="275"/>
        <source>Setting start positions...</source>
        <translation type="unfinished">Configuración de posiciones de inicio ...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="278"/>
        <source>Tank Moving Down</source>
        <translation type="unfinished">Tanque moviéndose hacia abajo</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="278"/>
        <source>Moving the resin tank down...</source>
        <translation type="unfinished">Moviendo el tanque de resina hacia abajo ...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="314"/>
        <source>Loading, please wait...</source>
        <translation type="unfinished">Cargando, por favor espera...</translation>
    </message>
</context>
<context>
    <name>PagePrintPreviewSwipe</name>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="33"/>
        <source>Project</source>
        <translation type="unfinished">Proyecto</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="165"/>
        <source>Swipe to project</source>
        <translation type="unfinished">Desliza al proyecto</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="206"/>
        <source>Unknown</source>
        <translation type="unfinished">Desconocido</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="279"/>
        <source>Layers</source>
        <translation type="unfinished">Capas</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="279"/>
        <source>Layer Height</source>
        <translation type="unfinished">Altura Capa</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="298"/>
        <source>Exposure Times</source>
        <translation type="unfinished">Tiempos de exposición</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="323"/>
        <source>Print Time Estimate</source>
        <translation type="unfinished">Tiempo Impresión Estimado</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="340"/>
        <source>Last Modified</source>
        <translation type="unfinished">Última Modificación</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="347"/>
        <source>Unknown</source>
        <comment>Unknow time of last modification of a file</comment>
        <translation type="unfinished">Desconocido</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="364"/>
        <source>Swipe to continue</source>
        <translation type="unfinished">Desliza para continuar</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="433"/>
        <source>The project requires %1 % of the resin. It will be necessary to refill the resin during print.</source>
        <translation type="unfinished">El proyecto requiere %1 % de la resina. Será necesario añadir resina durante la impresión.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="436"/>
        <source>Please fill the resin tank to at least %1 % and close the cover.</source>
        <translation type="unfinished">Por favor, llena el tanque de resina al menos al&lt;br/&gt;%1 % y cierra la cubierta.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="471"/>
        <source>Print Settings</source>
        <translation type="unfinished">Ajustes de Impresión</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="481"/>
        <source>Print</source>
        <translation type="unfinished">Imprimir</translation>
    </message>
</context>
<context>
    <name>PagePrintPrinting</name>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="33"/>
        <source>Print</source>
        <translation type="unfinished">Imprimir</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="104"/>
        <source>Today at</source>
        <translation type="unfinished">Hoy a las</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="107"/>
        <source>Tomorrow at</source>
        <translation type="unfinished">Mañana a las</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="150"/>
        <source>Layer:</source>
        <translation type="unfinished">Capa:</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="162"/>
        <source>Layer Height:</source>
        <translation type="unfinished">Altura Capa:</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="162"/>
        <source>N/A</source>
        <translation type="unfinished">N/A</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="251"/>
        <source>Remaining Time</source>
        <translation type="unfinished">Tiempo restante</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="256"/>
        <source>Unknown</source>
        <comment>Remaining time is unknown</comment>
        <translation type="unfinished">Desconocido</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="266"/>
        <source>Estimated End</source>
        <translation type="unfinished">Fin Estimado</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="281"/>
        <source>Printing Time</source>
        <translation type="unfinished">Tiempo de impresión</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="294"/>
        <source>Layer</source>
        <translation type="unfinished">Capa</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="306"/>
        <source>Remaining Resin</source>
        <translation type="unfinished">Resina restante</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="310"/>
        <location filename="../qml/PagePrintPrinting.qml" line="324"/>
        <source>ml</source>
        <translation type="unfinished">ml</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="310"/>
        <location filename="../qml/PagePrintPrinting.qml" line="324"/>
        <source>Unknown</source>
        <translation type="unfinished">Desconocido</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="320"/>
        <source>Consumed Resin</source>
        <translation type="unfinished">Resina consumida</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="359"/>
        <source>Print Settings</source>
        <translation type="unfinished">Ajustes de Impresión</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="365"/>
        <source>Refill Resin</source>
        <translation type="unfinished">Relleno manual de resina</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="378"/>
        <location filename="../qml/PagePrintPrinting.qml" line="384"/>
        <source>Cancel Print</source>
        <translation type="unfinished">Cancelar</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="385"/>
        <source>To make sure the print is not stopped accidentally,
please swipe the screen to move to the next step,
where you can cancel the print.</source>
        <translation type="unfinished">Para estar seguro de que la impresión no se detiene accidentalmente, desliza sobre la pantalla para pasar al siguiente paso, donde podrás cancelar la impresión.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="402"/>
        <source>Enter Admin</source>
        <translation type="unfinished">Iniciar modo Administrador</translation>
    </message>
</context>
<context>
    <name>PagePrintResinIn</name>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="30"/>
        <source>Pour in resin</source>
        <translation type="unfinished">Vierte resina</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="61"/>
        <source>The project requires %1 % of the resin. It will be necessary to refill the resin during print.</source>
        <translation type="unfinished">El proyecto requiere %1 % de la resina. Será necesario añadir resina durante la impresión.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="64"/>
        <source>Please fill the resin tank to at least %1 % and close the cover.</source>
        <translation type="unfinished">Por favor, llena el tanque de resina al menos al&lt;br/&gt;%1 % y cierra la cubierta.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="74"/>
        <source>Continue</source>
        <translation type="unfinished">Continuar</translation>
    </message>
</context>
<context>
    <name>PagePrusaConnect</name>
    <message>
        <location filename="../qml/PagePrusaConnect.qml" line="27"/>
        <source>Prusa Connect</source>
        <translation type="unfinished">Prusa Connect</translation>
    </message>
    <message>
        <location filename="../qml/PagePrusaConnect.qml" line="50"/>
        <location filename="../qml/PagePrusaConnect.qml" line="92"/>
        <location filename="../qml/PagePrusaConnect.qml" line="113"/>
        <source>Failed to establish a new connection.
Please try later.</source>
        <translation type="unfinished">No se pudo establecer una nueva conexión.
Por favor, inténtelo de nuevo más tarde.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrusaConnect.qml" line="70"/>
        <source>It&apos;s already registered!</source>
        <translation type="unfinished">¡Ya está registrada!</translation>
    </message>
    <message>
        <location filename="../qml/PagePrusaConnect.qml" line="70"/>
        <source>Start Your Registration!</source>
        <translation type="unfinished">¡Empieza el Registro!</translation>
    </message>
    <message>
        <location filename="../qml/PagePrusaConnect.qml" line="78"/>
        <source>Restore</source>
        <translation type="unfinished">Restaurar</translation>
    </message>
    <message>
        <location filename="../qml/PagePrusaConnect.qml" line="78"/>
        <source>Continue</source>
        <translation type="unfinished">Continuar</translation>
    </message>
</context>
<context>
    <name>PageQrCode</name>
    <message>
        <location filename="../qml/PageQrCode.qml" line="30"/>
        <source>Page QR code</source>
        <translation type="unfinished">Código QR de la página</translation>
    </message>
</context>
<context>
    <name>PageReleaseNotes</name>
    <message>
        <location filename="../qml/PageReleaseNotes.qml" line="9"/>
        <source>Release Notes</source>
        <translation type="unfinished">Notas de lanzamiento</translation>
    </message>
    <message>
        <location filename="../qml/PageReleaseNotes.qml" line="111"/>
        <source>Later</source>
        <translation type="unfinished">Más Tarde</translation>
    </message>
    <message>
        <location filename="../qml/PageReleaseNotes.qml" line="120"/>
        <source>Install Now</source>
        <translation type="unfinished">Instalar Ahora</translation>
    </message>
</context>
<context>
    <name>PageSelftestWizard</name>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="32"/>
        <source>Selftest</source>
        <translation type="unfinished">Autotest</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="48"/>
        <source>Welcome to the selftest wizard.</source>
        <translation type="unfinished">Bienvenido al asistente del autotest.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="50"/>
        <source>This procedure is mandatory and it will check all components of the printer.</source>
        <translation type="unfinished">Este procedimiento es obligatorio y comprobará todos los componentes de la impresora.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="58"/>
        <source>Please unscrew and remove the resin tank.</source>
        <translation type="unfinished">Por favor desatornilla y retira el tanque de resina.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="67"/>
        <source>Loosen the black knob and remove the platform.</source>
        <translation type="unfinished">Afloja el pomo negro  y retira la plataforma.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="76"/>
        <location filename="../qml/PageSelftestWizard.qml" line="137"/>
        <location filename="../qml/PageSelftestWizard.qml" line="169"/>
        <source>Close the cover.</source>
        <translation type="unfinished">Cierra la tapa.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="100"/>
        <source>Can you hear the music?</source>
        <translation type="unfinished">¿Puedes escuchar la música?</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="110"/>
        <source>Please install the resin tank and tighten the screws evenly.</source>
        <translation type="unfinished">Coloca el tanque de resina y aprieta los tornillos uniformemente.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="119"/>
        <source>Please install the platform under 60° angle and tighten it with the black knob.</source>
        <translation type="unfinished">Instala la plataforma en un ángulo de 60 ° y apriétala con el pomo negro.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="128"/>
        <source>Release the platform from the cantilever and place it onto the center of the resin tank at a 90° angle.</source>
        <translation type="unfinished">Suelta la plataforma del cantilever y colócala en el centro del tanque de resina en un ángulo de 90°.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="151"/>
        <source>Make sure that the resin tank is installed and the screws are tight.</source>
        <translation type="unfinished">Comprueba que el tanque de resina está instalado y que los tornillos están apretados.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="160"/>
        <source>Please install the platform under 0° angle and tighten it with the black knob.</source>
        <translation type="unfinished">Instala la plataforma en un ángulo de 0° y apriétala con el pomo negro.</translation>
    </message>
</context>
<context>
    <name>PageSetDate</name>
    <message>
        <location filename="../qml/PageSetDate.qml" line="37"/>
        <source>Set Date</source>
        <translation type="unfinished">Ajustar Fecha</translation>
    </message>
    <message>
        <location filename="../qml/PageSetDate.qml" line="665"/>
        <source>Set</source>
        <translation type="unfinished">Ajustar</translation>
    </message>
</context>
<context>
    <name>PageSetHostname</name>
    <message>
        <location filename="../qml/PageSetHostname.qml" line="30"/>
        <source>Set Hostname</source>
        <translation type="unfinished">Nombre del host</translation>
    </message>
    <message>
        <location filename="../qml/PageSetHostname.qml" line="47"/>
        <source>Hostname</source>
        <translation type="unfinished">Nombre del Host</translation>
    </message>
    <message>
        <location filename="../qml/PageSetHostname.qml" line="73"/>
        <source>Can contain only characters a-z, A-Z, 0-9 and  &quot;-&quot;.</source>
        <translation type="unfinished">Solo puede contener caracteres a-z, A-Z, 0-9 e &quot;-&quot;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSetHostname.qml" line="84"/>
        <source>Set</source>
        <translation type="unfinished">Ajustar</translation>
    </message>
</context>
<context>
    <name>PageSetLoginCredentials</name>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="32"/>
        <source>Login Credentials</source>
        <translation type="unfinished">Credenciales de Inicio</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="42"/>
        <source>Are you sure?</source>
        <translation type="unfinished">¿Estás seguro?</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="43"/>
        <source>Disable the HTTP Digest?&lt;br/&gt;&lt;br/&gt;CAUTION: This may be insecure!</source>
        <translation type="unfinished">¿Deshabilitar HTTP Digest?&lt;br/&gt;&lt;br/&gt;CUIDADO: ¡Esto puede ser inseguro!</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="66"/>
        <source>HTTP Digest</source>
        <translation type="unfinished">HTTP Digest</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="109"/>
        <source>User Name</source>
        <translation type="unfinished">Nombre de Usuario</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="146"/>
        <source>Printer Password</source>
        <translation type="unfinished">Contraseña de la Impresora</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="171"/>
        <location filename="../qml/PageSetLoginCredentials.qml" line="179"/>
        <location filename="../qml/PageSetLoginCredentials.qml" line="229"/>
        <source>Must be at least 8 chars long</source>
        <translation type="unfinished">Debe tener al menos 8 caracteres</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="185"/>
        <source>Can contain only characters a-z, A-Z, 0-9 and  &quot;-&quot;.</source>
        <translation type="unfinished">Solo puede contener caracteres a-z, A-Z, 0-9 e &quot;-&quot;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="205"/>
        <source>Printer API Key</source>
        <translation type="unfinished">Clave de API de la impresora</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="236"/>
        <source>Can contain only ASCII characters.</source>
        <translation type="unfinished">Solo puede contener caracteres ASCII.</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="249"/>
        <source>Save</source>
        <translation type="unfinished">Guardar</translation>
    </message>
    <message>
        <location filename="../qml/PageSetLoginCredentials.qml" line="266"/>
        <source>Failed change login settings.</source>
        <translation type="unfinished">No se pudo cambiar la configuración de inicio de sesión.</translation>
    </message>
</context>
<context>
    <name>PageSetTime</name>
    <message>
        <location filename="../qml/PageSetTime.qml" line="32"/>
        <source>Set Time</source>
        <translation type="unfinished">Ajustar Hora</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTime.qml" line="98"/>
        <source>Hour</source>
        <translation type="unfinished">Hora</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTime.qml" line="105"/>
        <source>Minute</source>
        <translation type="unfinished">Minutos</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTime.qml" line="112"/>
        <source>Set</source>
        <translation type="unfinished">Ajustar</translation>
    </message>
</context>
<context>
    <name>PageSetTimezone</name>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="29"/>
        <source>Set Timezone</source>
        <translation type="unfinished">Ajustar Huso horario</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="129"/>
        <source>Region</source>
        <translation type="unfinished">Región</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="135"/>
        <source>City</source>
        <translation type="unfinished">Ciudad</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="141"/>
        <source>Set</source>
        <translation type="unfinished">Ajustar</translation>
    </message>
</context>
<context>
    <name>PageSettings</name>
    <message>
        <location filename="../qml/PageSettings.qml" line="28"/>
        <source>Settings</source>
        <translation type="unfinished">Ajustes</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="58"/>
        <source>Calibration</source>
        <translation type="unfinished">Calibración</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="66"/>
        <source>Network</source>
        <translation type="unfinished">Redes</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="75"/>
        <source>Platform &amp; Resin Tank</source>
        <translation type="unfinished">Plataforma &amp; Tanque de Resina</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="84"/>
        <source>Firmware</source>
        <translation type="unfinished">Firmware</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="93"/>
        <source>Settings &amp; Sensors</source>
        <translation type="unfinished">Ajustes &amp; Sensores</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="102"/>
        <source>Touchscreen</source>
        <translation type="unfinished">Pantalla Táctil</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="111"/>
        <source>Language &amp; Time</source>
        <translation type="unfinished">Idioma &amp; Hora</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="120"/>
        <source>System Logs</source>
        <translation type="unfinished">Registros del Sistema</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="129"/>
        <source>Support</source>
        <translation type="unfinished">Soporte</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="139"/>
        <source>Enter Admin</source>
        <translation type="unfinished">Iniciar modo Administrador</translation>
    </message>
</context>
<context>
    <name>PageSettingsCalibration</name>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="31"/>
        <source>Calibration</source>
        <translation type="unfinished">Calibración</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="50"/>
        <source>Selftest</source>
        <translation type="unfinished">Autotest</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="57"/>
        <source>Printer Calibration</source>
        <translation type="unfinished">Calibración de la impresora</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="67"/>
        <source>UV Calibration</source>
        <translation type="unfinished">Calibración UV</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="75"/>
        <source>Display Test</source>
        <translation type="unfinished">Test de Pantalla</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="92"/>
        <source>New Display?</source>
        <translation type="unfinished">¿Pantalla nueva?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="93"/>
        <source>Did you replace the EXPOSITION DISPLAY?</source>
        <translation type="unfinished">¿Has cambiado la PANTALLA DE EXPOSICIÓN?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="105"/>
        <source>New UV LED SET?</source>
        <translation type="unfinished">¿Nuevo conjunto de LED UV?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="106"/>
        <source>Did you replace the UV LED SET?</source>
        <translation type="unfinished">¿Reemplazaste el conjunto de LEDs UV?</translation>
    </message>
</context>
<context>
    <name>PageSettingsFirmware</name>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="35"/>
        <source>Firmware</source>
        <translation type="unfinished">Firmware</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="45"/>
        <source>Unknown</source>
        <comment>Unknown operating system version on alternative slot</comment>
        <translation type="unfinished">Desconocido</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="74"/>
        <source>Download Now</source>
        <translation type="unfinished">Descargar ahora</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="76"/>
        <source>Install Now</source>
        <translation type="unfinished">Instalar Ahora</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="78"/>
        <source>Check for Update</source>
        <translation type="unfinished">Busca actualizaciones</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="85"/>
        <source>Check for update failed</source>
        <translation type="unfinished">La búsqueda de actualizaciones falló</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="87"/>
        <source>Checking for updates...</source>
        <translation type="unfinished">Buscando actualizaciones...</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="89"/>
        <source>System is up-to-date</source>
        <translation type="unfinished">El sistema está actualizado</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="91"/>
        <source>Update available</source>
        <translation type="unfinished">Actualización disponible</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="93"/>
        <source>Update is downloading</source>
        <translation type="unfinished">La actualización se está descargando</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="95"/>
        <source>Update is downloaded</source>
        <translation type="unfinished">Se está descargando la actualización</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="97"/>
        <source>Update download failed</source>
        <translation type="unfinished">La descarga de la actualización ha fallado</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="99"/>
        <source>Updater service idle</source>
        <translation type="unfinished">Servicio de actualización en espera</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="111"/>
        <source>Installed version</source>
        <translation type="unfinished">Versión instalada</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="137"/>
        <source>Receive Beta Updates</source>
        <translation type="unfinished">Recibir Actualizaciones de Betas</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="155"/>
        <source>Switch to beta?</source>
        <translation type="unfinished">¿Cambiar a la beta?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="156"/>
        <source>Warning! The beta updates can be unstable.&lt;br/&gt;&lt;br/&gt;Not recommended for production printers.&lt;br/&gt;&lt;br/&gt;Continue?</source>
        <translation type="unfinished">¡Advertencia! Las actualizaciones beta pueden ser inestables.&lt;br/&gt;&lt;br/&gt; No recomendado para impresoras de producción.&lt;br/&gt;&lt;br/&gt; ¿Continuar?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="168"/>
        <source>Switch to version:</source>
        <translation type="unfinished">Cambiar a versión:</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="180"/>
        <source>Downgrade?</source>
        <translation type="unfinished">¿Degradar?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="181"/>
        <source>Do you really want to downgrade to FW</source>
        <translation type="unfinished">De verdad quieres cambiar a un FW anterior</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="209"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="415"/>
        <source>Incompatible FW!</source>
        <translation type="unfinished">¡FW incompatible!</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="210"/>
        <source>The alternative FW version &lt;b&gt;%1&lt;/b&gt; is not compatible with your printer model - &lt;b&gt;%2&lt;/b&gt;.</source>
        <translation type="unfinished">La versión de FW alternativa &lt;b&gt;%1&lt;/b&gt; no es compatible con tu modelo de impresora - &lt;b&gt;%2&lt;/b&gt; .</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="212"/>
        <source>If you switch, you can update to another FW version afterwards but &lt;b&gt;you will not be able to print.&lt;/b&gt;</source>
        <translation type="unfinished">Si cambias, puedes actualizar a otra versión de FW posteriormente, pero &lt;b&gt;no podrás imprimir.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="214"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="420"/>
        <source>Continue anyway?</source>
        <translation type="unfinished">¿Continuar de todos modos?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="234"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="427"/>
        <source>None</source>
        <comment>Printer model is not known/can&apos;t be determined</comment>
        <translation type="unfinished">Ninguno</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="238"/>
        <source>Newer than SL1S</source>
        <comment>Printer model is unknown, but better than SL1S</comment>
        <translation type="unfinished">Mas nueva que la SL1S</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="247"/>
        <source>Factory Reset</source>
        <translation type="unfinished">Restablecer configuración de Fábrica</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="256"/>
        <source>Are you sure?</source>
        <translation type="unfinished">¿Estás seguro?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="257"/>
        <source>Do you really want to perform the factory reset?

All settings will be erased!
Projects will stay untouched.</source>
        <translation type="unfinished">¿Realmente deseas realizar el restablecimiento de fábrica?

¡Se borrarán todos los ajustes!
Los proyectos permanecerán intactos.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="292"/>
        <source>FW Info</source>
        <comment>page title, information about the selected update bundle</comment>
        <translation type="unfinished">Información de FW</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="293"/>
        <source>Loading FW file meta information
(may take up to 20 seconds) ...</source>
        <translation type="unfinished">Cargando metadatos del archivo de FW
(puede tardar hasta 20 segundos) ...</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="335"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="384"/>
        <source>Install Firmware?</source>
        <translation type="unfinished">¿Instalar firmware?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="336"/>
        <source>FW file meta information not found.&lt;br/&gt;&lt;br/&gt;Do you want to install&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;anyway?</source>
        <translation type="unfinished">No se encontraron los metadatos del archivo FW.&lt;br/&gt;&lt;br/&gt;¿Quieres instalar&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt; de todas formas?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="349"/>
        <source>Downgrade Firmware?</source>
        <translation type="unfinished">¿Degradar el firmware?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="352"/>
        <source>Version of selected FW file&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;,</source>
        <translation type="unfinished">Versión del archivo FW seleccionado&lt;br/&gt; &lt;b&gt;%1&lt;/b&gt; ,</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="354"/>
        <source>is lower or equal than current&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;.</source>
        <translation type="unfinished">es menor o igual que el actual&lt;br/&gt; &lt;b&gt;%1&lt;/b&gt; .</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="356"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="389"/>
        <source>Do you want to continue?</source>
        <translation type="unfinished">¿Quieres continuar?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="385"/>
        <source>You have selected update bundle &lt;b&gt;&quot;%1&quot;&lt;/b&gt;,</source>
        <translation type="unfinished">Has seleccionado el paquete de actualización &lt;b&gt;&quot;%1&quot;&lt;/b&gt;,</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="387"/>
        <source>which has version &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation type="unfinished">que tiene la versión &lt;b&gt;%1&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="416"/>
        <source>&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;is not compatible with your current hardware model - &lt;b&gt;%2&lt;/b&gt;.</source>
        <translation type="unfinished">&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;no es compatible con su modelo de hardware actual - &lt;b&gt;%2&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="418"/>
        <source>After installing this update, the printer can be updated to another FW but &lt;b&gt;printing won&apos;t work.&lt;/b&gt;</source>
        <translation type="unfinished">Después de instalar esta actualización, la impresora se puede actualizar a otro FW pero &lt;b&gt;la impresión no funcionará.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="430"/>
        <source>Unknown</source>
        <comment>Printer model is unknown, but likely better than SL1S</comment>
        <translation type="unfinished">Desconocido</translation>
    </message>
</context>
<context>
    <name>PageSettingsLanguageTime</name>
    <message>
        <location filename="../qml/PageSettingsLanguageTime.qml" line="28"/>
        <source>Language &amp; Time</source>
        <translation type="unfinished">Idioma &amp; Hora</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsLanguageTime.qml" line="34"/>
        <source>Set Language</source>
        <translation type="unfinished">Establecer Idioma</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsLanguageTime.qml" line="40"/>
        <source>Time Settings</source>
        <translation type="unfinished">Ajustes de Hora</translation>
    </message>
</context>
<context>
    <name>PageSettingsNetwork</name>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="27"/>
        <source>Network</source>
        <translation type="unfinished">Redes</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="33"/>
        <source>Ethernet</source>
        <translation type="unfinished">Ethernet</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="41"/>
        <source>Wi-Fi</source>
        <translation type="unfinished">Wi-Fi</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="49"/>
        <source>Hot Spot</source>
        <translation type="unfinished">Hot Spot</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="58"/>
        <source>Set Hostname</source>
        <translation type="unfinished">Nombre del host</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="67"/>
        <source>Login Credentials</source>
        <translation type="unfinished">Credenciales de Inicio</translation>
    </message>
</context>
<context>
    <name>PageSettingsPlatformResinTank</name>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="27"/>
        <source>Platform &amp; Resin Tank</source>
        <translation type="unfinished">Plataforma &amp; Tanque de Resina</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="33"/>
        <source>Move Platform</source>
        <translation type="unfinished">Mover Plataforma</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="40"/>
        <source>Move Resin Tank</source>
        <translation type="unfinished">Mover Tanque Resina</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="47"/>
        <source>Disable Steppers</source>
        <translation type="unfinished">Desactivar Motores</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="55"/>
        <source>Platform Axis Sensitivity</source>
        <translation type="unfinished">Sensibilidad Eje Plataforma</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="77"/>
        <source>Tank Axis Sensitivity</source>
        <translation type="unfinished">Sensibilidad Eje Tanque</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="99"/>
        <source>Limit for Fast Tilt</source>
        <translation type="unfinished">Límite para Inclinación Rápida</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="106"/>
        <source>%</source>
        <translation type="unfinished">%</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="125"/>
        <source>Platform Offset</source>
        <translation type="unfinished">Desplazamiento de la plataforma</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="132"/>
        <source>um</source>
        <translation type="unfinished">um</translation>
    </message>
</context>
<context>
    <name>PageSettingsSensors</name>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="27"/>
        <source>Settings &amp; Sensors</source>
        <translation type="unfinished">Ajustes &amp; Sensores</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="35"/>
        <source>Device hash in QR</source>
        <translation type="unfinished">Hash del dispositivo en QR</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="60"/>
        <source>Auto Power Off</source>
        <translation type="unfinished">Apagado Automático</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="86"/>
        <source>Cover Check</source>
        <translation type="unfinished">Comprobación Cubierta</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="110"/>
        <location filename="../qml/PageSettingsSensors.qml" line="145"/>
        <source>Are you sure?</source>
        <translation type="unfinished">¿Estás seguro?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="111"/>
        <source>Disable the cover sensor?&lt;br/&gt;&lt;br/&gt;CAUTION: This may lead to unwanted exposure to UV light or personal injury due to moving parts. This action is not recommended!</source>
        <translation type="unfinished">¿Deshabilitar el sensor de la tapa?&lt;br/&gt;&lt;br/&gt; PRECAUCIÓN: Esto puede provocar una exposición no deseada a la luz ultravioleta o lesiones personales debido a las piezas móviles. ¡No se recomienda esta acción!</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="123"/>
        <source>Resin Sensor</source>
        <translation type="unfinished">Sensor de Resina</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="146"/>
        <source>Disable the resin sensor?&lt;br/&gt;&lt;br/&gt;CAUTION: This may lead to failed prints or resin tank overflow! This action is not recommended!</source>
        <translation type="unfinished">¿Desactivar el sensor de resina?&lt;br/&gt;&lt;br/&gt;PRECAUCIÓN: ¡Esto puede crear fallos en impresiones o que el tanque se desborde! ¡No se recomienda esta acción!</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="158"/>
        <source>Rear Fan Speed</source>
        <translation type="unfinished">Velocidad Ventilador Trasero</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="165"/>
        <source>RPM</source>
        <translation type="unfinished">RPM</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="177"/>
        <source>Off</source>
        <translation type="unfinished">Ap.</translation>
    </message>
</context>
<context>
    <name>PageSettingsSupport</name>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="29"/>
        <source>Support</source>
        <translation type="unfinished">Soporte</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="35"/>
        <source>Download Examples</source>
        <translation type="unfinished">Descargar Ejemplos</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="49"/>
        <source>Manual</source>
        <translation type="unfinished">Manual</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="59"/>
        <source>Videos</source>
        <translation type="unfinished">Videos</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="68"/>
        <source>System Information</source>
        <translation type="unfinished">Info del Sistema</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="77"/>
        <source>About Us</source>
        <translation type="unfinished">Acerca de Nosotros</translation>
    </message>
</context>
<context>
    <name>PageSettingsSystemLogs</name>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="28"/>
        <source>System Logs</source>
        <translation type="unfinished">Registros del Sistema</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="39"/>
        <source>Last Seen Logs:</source>
        <translation type="unfinished">Últimos Registros Vistos:</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="39"/>
        <source>&lt;b&gt;No logs have been uploaded yet.&lt;/b&gt;</source>
        <translation type="unfinished">&lt;b&gt;Aún no se han subido registros.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="54"/>
        <source>Save to USB Drive</source>
        <translation type="unfinished">Guardar en la Memoria USB</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="68"/>
        <source>Upload to Server</source>
        <translation type="unfinished">Subir al Servidor</translation>
    </message>
</context>
<context>
    <name>PageSettingsTouchscreen</name>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="27"/>
        <source>Touchscreen</source>
        <translation type="unfinished">Pantalla Táctil</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="35"/>
        <source>Screensaver timer</source>
        <translation type="unfinished">Temporizador de salvapantallas</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="55"/>
        <source>h</source>
        <comment>hours short</comment>
        <translation type="unfinished">h</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="56"/>
        <source>min</source>
        <comment>minutes short</comment>
        <translation type="unfinished">min</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="57"/>
        <source>s</source>
        <comment>seconds short</comment>
        <translation type="unfinished">s</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="62"/>
        <source>Off</source>
        <translation type="unfinished">Ap.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="79"/>
        <source>Touch Screen Brightness</source>
        <translation type="unfinished">Brillo Pantalla Táctil</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="97"/>
        <source>N/A</source>
        <translation type="unfinished">N/A</translation>
    </message>
</context>
<context>
    <name>PageShowToken</name>
    <message>
        <location filename="../qml/PageShowToken.qml" line="27"/>
        <source>Prusa Connect</source>
        <translation type="unfinished">Prusa Connect</translation>
    </message>
    <message>
        <location filename="../qml/PageShowToken.qml" line="31"/>
        <source>Temporary Token</source>
        <translation type="unfinished">Token temporal</translation>
    </message>
    <message>
        <location filename="../qml/PageShowToken.qml" line="61"/>
        <source>N/A</source>
        <translation type="unfinished">N/A</translation>
    </message>
    <message>
        <location filename="../qml/PageShowToken.qml" line="80"/>
        <source>Continue</source>
        <translation type="unfinished">Continuar</translation>
    </message>
</context>
<context>
    <name>PageSoftwareLicenses</name>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="28"/>
        <source>Software Packages</source>
        <translation type="unfinished">Paquetes de Software</translation>
    </message>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="112"/>
        <location filename="../qml/PageSoftwareLicenses.qml" line="233"/>
        <source>Package Name</source>
        <translation type="unfinished">Nombre del Paquete</translation>
    </message>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="242"/>
        <source>Version</source>
        <translation type="unfinished">Versión</translation>
    </message>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="250"/>
        <source>License</source>
        <translation type="unfinished">Licencia</translation>
    </message>
</context>
<context>
    <name>PageSysinfo</name>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="30"/>
        <source>System Information</source>
        <translation type="unfinished">Info del Sistema</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="52"/>
        <source>System</source>
        <translation type="unfinished">Sistema</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="57"/>
        <source>OS Image Version</source>
        <translation type="unfinished">Versión de la Imagen del SO</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="62"/>
        <source>Printer Model</source>
        <translation type="unfinished">Modelo de impresora</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="65"/>
        <source>None</source>
        <comment>Printer model is not known/can&apos;t be determined</comment>
        <translation type="unfinished">Ninguno</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="69"/>
        <source>Newer than SL1S</source>
        <comment>Printer model is unknown, but better than SL1S</comment>
        <translation type="unfinished">Mas nueva que la SL1S</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="75"/>
        <source>A64 Controller SN</source>
        <translation type="unfinished">Num.Serie Controlador A64</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="80"/>
        <source>API Key / Printer Password</source>
        <translation type="unfinished">Clave API / Contraseña Impresora</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="92"/>
        <source>Other Components</source>
        <translation type="unfinished">Otros Componentes</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="97"/>
        <source>Motion Controller SN</source>
        <translation type="unfinished">Num.Serie Controlador Movimiento</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="102"/>
        <source>Motion Controller SW Version</source>
        <translation type="unfinished">Versión SW Controlador Movimiento</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="107"/>
        <source>Motion Controller HW Revision</source>
        <translation type="unfinished">Revisión Controlador Movimiento HW</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="112"/>
        <source>Booster Board SN</source>
        <translation type="unfinished">SN Placa Booster</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="118"/>
        <source>Exposure display SN</source>
        <translation type="unfinished">Pantalla de exposición SN</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="124"/>
        <source>GUI Version</source>
        <translation type="unfinished">Versión GUI</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="136"/>
        <source>Hardware State</source>
        <translation type="unfinished">Estado de Hardware</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="141"/>
        <source>Network State</source>
        <translation type="unfinished">Estado de Red</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="142"/>
        <source>Online</source>
        <translation type="unfinished">Conectada</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="142"/>
        <source>Offline</source>
        <translation type="unfinished">Desconectada</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="146"/>
        <source>Ethernet IP Address</source>
        <translation type="unfinished">Dirección IP Ethernet</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="147"/>
        <location filename="../qml/PageSysinfo.qml" line="152"/>
        <location filename="../qml/PageSysinfo.qml" line="230"/>
        <location filename="../qml/PageSysinfo.qml" line="263"/>
        <location filename="../qml/PageSysinfo.qml" line="268"/>
        <location filename="../qml/PageSysinfo.qml" line="273"/>
        <location filename="../qml/PageSysinfo.qml" line="278"/>
        <location filename="../qml/PageSysinfo.qml" line="283"/>
        <source>N/A</source>
        <translation type="unfinished">N/A</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="151"/>
        <source>Wifi IP Address</source>
        <translation type="unfinished">Dirección IP Wifi</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="156"/>
        <source>Time of Fast Tilt</source>
        <translation type="unfinished">Tiempo de Inclinación Rápida</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="157"/>
        <location filename="../qml/PageSysinfo.qml" line="162"/>
        <source>seconds</source>
        <translation type="unfinished">Segundos</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="161"/>
        <source>Time of Slow Tilt</source>
        <translation type="unfinished">Tiempo para Inclinación Lenta</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="166"/>
        <source>Resin Sensor State</source>
        <translation type="unfinished">Estado Sensor Resina</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="167"/>
        <source>Triggered</source>
        <translation type="unfinished">Activado</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="167"/>
        <source>Not triggered</source>
        <translation type="unfinished">Sin activar</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="171"/>
        <source>Cover State</source>
        <translation type="unfinished">Estado Cubierta</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="172"/>
        <source>Closed</source>
        <translation type="unfinished">Cerrado</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="172"/>
        <source>Open</source>
        <translation type="unfinished">Abrir</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="176"/>
        <source>CPU Temperature</source>
        <translation type="unfinished">Temperatura CPU</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="177"/>
        <location filename="../qml/PageSysinfo.qml" line="182"/>
        <location filename="../qml/PageSysinfo.qml" line="187"/>
        <source>°C</source>
        <translation type="unfinished">°C</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="181"/>
        <source>UV LED Temperature</source>
        <translation type="unfinished">Temperatura LED UV</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="186"/>
        <source>Ambient Temperature</source>
        <translation type="unfinished">Temperatura ambiente</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="191"/>
        <source>UV LED Fan</source>
        <translation type="unfinished">Ventilador LED UV</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="194"/>
        <location filename="../qml/PageSysinfo.qml" line="204"/>
        <location filename="../qml/PageSysinfo.qml" line="214"/>
        <source>Fan Error!</source>
        <translation type="unfinished">¡Error del ventilador!</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="196"/>
        <location filename="../qml/PageSysinfo.qml" line="206"/>
        <location filename="../qml/PageSysinfo.qml" line="216"/>
        <source>RPM</source>
        <translation type="unfinished">RPM</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="201"/>
        <source>Blower Fan</source>
        <translation type="unfinished">Ventilador Radial</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="211"/>
        <source>Rear Fan</source>
        <translation type="unfinished">Ventilador Trasero</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="221"/>
        <source>UV LED</source>
        <translation type="unfinished">LED UV</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="235"/>
        <source>Power Supply Voltage</source>
        <translation type="unfinished">Voltaje Fuente Alimentación</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="236"/>
        <source>V</source>
        <translation type="unfinished">V</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="247"/>
        <source>Statistics</source>
        <translation type="unfinished">Estadísticas</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="252"/>
        <source>UV LED Time Counter</source>
        <translation type="unfinished">Contador Tiempo LED UV</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="253"/>
        <location filename="../qml/PageSysinfo.qml" line="258"/>
        <location filename="../qml/PageSysinfo.qml" line="278"/>
        <source>d</source>
        <translation type="unfinished">d</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="253"/>
        <location filename="../qml/PageSysinfo.qml" line="258"/>
        <location filename="../qml/PageSysinfo.qml" line="278"/>
        <source>h</source>
        <translation type="unfinished">h</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="253"/>
        <location filename="../qml/PageSysinfo.qml" line="258"/>
        <location filename="../qml/PageSysinfo.qml" line="278"/>
        <source>m</source>
        <translation type="unfinished">m</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="257"/>
        <source>Print Display Time Counter</source>
        <translation type="unfinished">Contador de Tiempo de la Pantalla de Impresión</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="262"/>
        <source>Started Projects</source>
        <translation type="unfinished">Proyectos Comenzados</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="267"/>
        <source>Finished Projects</source>
        <translation type="unfinished">Proyectos Acabados</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="272"/>
        <source>Total Layers Printed</source>
        <translation type="unfinished">Capas Totales Impresas</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="277"/>
        <source>Total Print Time</source>
        <translation type="unfinished">Tiempo Total de Impresión</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="282"/>
        <source>Total Resin Consumed</source>
        <translation type="unfinished">Resina Total Consumida</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="283"/>
        <source>ml</source>
        <translation type="unfinished">ml</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="293"/>
        <source>Show software packages &amp; licenses</source>
        <translation type="unfinished">Mostrar paquetes de software &amp; licencias</translation>
    </message>
</context>
<context>
    <name>PageTiltmove</name>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="32"/>
        <source>Move Resin Tank</source>
        <translation type="unfinished">Mover Tanque Resina</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="76"/>
        <source>Fast Up</source>
        <translation type="unfinished">Subir Rápido</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="84"/>
        <source>Fast Down</source>
        <translation type="unfinished">Bajar Rápido</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="92"/>
        <source>Up</source>
        <translation type="unfinished">Arriba</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="100"/>
        <source>Down</source>
        <translation type="unfinished">Abajo</translation>
    </message>
</context>
<context>
    <name>PageTimeSettings</name>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="34"/>
        <source>Time Settings</source>
        <translation type="unfinished">Ajustes de Hora</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="49"/>
        <source>Use NTP</source>
        <translation type="unfinished">Ajuste automático de hora</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="67"/>
        <source>Time</source>
        <translation type="unfinished">Tiempo</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="87"/>
        <source>Date</source>
        <translation type="unfinished">Fecha</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="107"/>
        <source>Timezone</source>
        <translation type="unfinished">Huso horario</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="124"/>
        <source>Time Format</source>
        <translation type="unfinished">Formato de tiempo</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="135"/>
        <source>Native</source>
        <comment>Default time format determined by the locale</comment>
        <translation type="unfinished">Nativo</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="136"/>
        <source>12-hour</source>
        <comment>12h time format</comment>
        <translation type="unfinished">12 horas</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="137"/>
        <source>24-hour</source>
        <comment>24h time format</comment>
        <translation type="unfinished">24 horas</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="158"/>
        <source>Automatic time settings using NTP ...</source>
        <translation type="unfinished">Ajuste de hora automático usando NTP ...</translation>
    </message>
</context>
<context>
    <name>PageTowermove</name>
    <message>
        <location filename="../qml/PageTowermove.qml" line="32"/>
        <source>Move Platform</source>
        <translation type="unfinished">Mover Plataforma</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="77"/>
        <source>Fast Up</source>
        <translation type="unfinished">Subir Rápido</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="85"/>
        <source>Fast Down</source>
        <translation type="unfinished">Bajar Rápido</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="93"/>
        <source>Up</source>
        <translation type="unfinished">Arriba</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="101"/>
        <source>Down</source>
        <translation type="unfinished">Abajo</translation>
    </message>
</context>
<context>
    <name>PageUnpackingCompleteWizard</name>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="31"/>
        <source>Unpacking</source>
        <translation type="unfinished">Desembalaje</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="47"/>
        <source>Unscrew and remove the resin tank and remove the black foam underneath it.</source>
        <translation type="unfinished">Desatornilla y retira el tanque de resina y retira la espuma negra que se encuentra debajo.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="56"/>
        <source>Remove the black foam from both sides of the platform.</source>
        <translation type="unfinished">Retira la espuma negra de ambos lados de la plataforma.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="65"/>
        <source>Please remove the safety sticker and open the cover.</source>
        <translation type="unfinished">Retira el adhesivo de seguridad y abre la tapa</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="74"/>
        <source>Carefully peel off the protective sticker from the exposition display.</source>
        <translation type="unfinished">Despega con cuidado el adhesivo de protección de la pantalla de exposición.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="83"/>
        <source>Unpacking done.</source>
        <translation type="unfinished">Desembalaje terminado.</translation>
    </message>
</context>
<context>
    <name>PageUnpackingKitWizard</name>
    <message>
        <location filename="../qml/PageUnpackingKitWizard.qml" line="31"/>
        <source>Unpacking</source>
        <translation type="unfinished">Desembalaje</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingKitWizard.qml" line="44"/>
        <source>Carefully peel off the protective sticker from the exposition display.</source>
        <translation type="unfinished">Despega con cuidado el adhesivo de protección de la pantalla de exposición.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingKitWizard.qml" line="53"/>
        <source>Unpacking done.</source>
        <translation type="unfinished">Desembalaje terminado.</translation>
    </message>
</context>
<context>
    <name>PageUpdatingFirmware</name>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="36"/>
        <source>Printer Update</source>
        <translation type="unfinished">Actualización de la Impresora</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="42"/>
        <source>Unknown</source>
        <translation type="unfinished">Desconocido</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="170"/>
        <source>Downloading</source>
        <translation type="unfinished">Descargando</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="211"/>
        <source>Downloading firmware, installation will begin immediately after.</source>
        <translation type="unfinished">Descargando firmware, la instalación comenzará inmediatamente después.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="228"/>
        <source>Download failed.</source>
        <translation type="unfinished">Descarga fallida.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="254"/>
        <source>Installing Firmware</source>
        <translation type="unfinished">Instalando Firmware</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="295"/>
        <source>Do not power off the printer while updating!&lt;br/&gt;Printer will be rebooted after a successful update.</source>
        <translation type="unfinished">¡No apagues la impresora durante la actualización!&lt;br/&gt;La impresora se reiniciará después de actualizarse con éxito.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="345"/>
        <source>Updated to %1 failed.</source>
        <translation type="unfinished">Actualizado a %1 fallido.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="362"/>
        <source>Printer is being restarted into the new firmware(%1), please wait</source>
        <translation type="unfinished">La impresora se reinicia con el nuevo firmware(%1), por favor espere</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="387"/>
        <source>Update to %1 failed.</source>
        <translation type="unfinished">Actualización a %1 fallida.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="420"/>
        <source>A problem has occurred while updating, please let us know (send us the logs). Do not panic, your printer is still working the same as it did before. Continue by pressing &quot;back&quot;</source>
        <translation type="unfinished">Ha ocurrido un problema durante la actualización, háznoslo saber (envíanos los registros). Que no cunda el pánico, tu impresora sigue funcionando igual que antes. Continuar presionando &quot;atrás&quot;</translation>
    </message>
</context>
<context>
    <name>PageUpgradeWizard</name>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="31"/>
        <source>Hardware Upgrade</source>
        <translation type="unfinished">Actualización de Hardware</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="45"/>
        <source>SL1S components detected (upgrade from SL1).</source>
        <translation type="unfinished">Componentes SL1S detectados (actualización para SL1).</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="46"/>
        <source>To complete the upgrade procedure, printer needs to clear the configuration and reboot.</source>
        <translation type="unfinished">Para completar el procedimiento de actualización, la impresora debe borrar la configuración y reiniciarse.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="47"/>
        <source>Proceed?</source>
        <translation type="unfinished">¿Continuar?</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="56"/>
        <source>The printer will power off now.</source>
        <translation type="unfinished">Ahora la impresora se apagará.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="57"/>
        <source>Reassemble SL1 components and power on the printer. This will restore the original state.</source>
        <translation type="unfinished">Vuelve a montar las piezas de la SL1 y enciende la impresora. Esto la devolverá al estado original.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="65"/>
        <source>The configuration is going to be cleared now.</source>
        <translation type="unfinished">La configuración se borrará ahora.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="66"/>
        <source>The printer will ask for the inital setup after reboot.</source>
        <translation type="unfinished">La impresora solicitará la configuración inicial después de reiniciar.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="75"/>
        <source>Use only the plastic resin tank supplied. Using the old metal resin tank may cause resin to spill and damage your printer!</source>
        <translation type="unfinished">Utiliza únicamente el tanque de resina plástica suministrado. El uso del anterior tanque de resina de metal puede hacer que la resina se derrame y dañe la impresora!</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="84"/>
        <source>Only use the platform supplied. Using a different platform may cause resin to spill and damage your printer!</source>
        <translation type="unfinished">Emplea solamente la plataforma suministrada. Si usas otra, ¡podrías causar vertidos de resina y daños a tu impresora!</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="93"/>
        <source>Please note that downgrading is not supported. 

Downgrading your printer will erase your UV calibration and your printer will not work properly. 

You will need to recalibrate it using an external UV calibrator.</source>
        <translation type="unfinished">Tenga en cuenta que no se admite la degradación.

La degradación de su impresora borrará su calibración UV y su impresora no funcionará correctamente.

Deberá volver a calibrarla con un calibrador UV externo.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="101"/>
        <source>Upgrade done. In the next step, the printer will be restarted.</source>
        <translation type="unfinished">Actualización realizada. En el siguiente paso, se reiniciará la impresora.</translation>
    </message>
</context>
<context>
    <name>PageUvCalibrationWizard</name>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="31"/>
        <source>UV Calibration</source>
        <translation type="unfinished">Calibración UV</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="46"/>
        <source>Welcome to the UV calibration.</source>
        <translation type="unfinished">Bienvenido a la calibración UV.</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="48"/>
        <source>1. If the resin tank is in the printer, remove it along with the screws.</source>
        <translation type="unfinished">1. Si el tanque de resina está en la impresora, retíralo junto con los tornillos.</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="50"/>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="70"/>
        <source>2. Close the cover, don&apos;t open it! UV radiation is harmful!</source>
        <translation type="unfinished">2. Cierra la tapa, no la abras. ¡La radiación UV puede ser perjudicial!</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="53"/>
        <source>Intensity: center %1, edge %2</source>
        <translation type="unfinished">Intensidad: centro %1, borde %2</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="55"/>
        <source>Warm-up: %1 s</source>
        <translation type="unfinished">Calentamiento:%1 s</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="68"/>
        <source>1. Place the UV calibrator on the print display and connect it to the front USB.</source>
        <translation type="unfinished">1. Coloca el calibrador UV sobre la pantalla de impresión y conéctalo al USB frontal.</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="80"/>
        <source>Open the cover, &lt;b&gt;remove and disconnect&lt;/b&gt; the UV calibrator.</source>
        <translation type="unfinished">Abre la tapa, &lt;br&gt;retira y desconecta&lt;/b&gt; el calibrador UV.</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="95"/>
        <source>The result of calibration:</source>
        <translation type="unfinished">El resultado de la calibración:</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="96"/>
        <source>UV PWM: %1</source>
        <translation type="unfinished">PWM UV: %1</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="97"/>
        <source>UV Intensity: %1, σ = %2</source>
        <translation type="unfinished">Intensidad UV:%1, σ =%2</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="98"/>
        <source>UV Intensity min: %1, max: %2</source>
        <translation type="unfinished">Intensidad UV min: %1, max: %2</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="102"/>
        <source>The printer has been successfully calibrated!
Would you like to apply the calibration results?</source>
        <translation type="unfinished">¡La impresora se ha calibrado correctamente!
¿Te gustaría aplicar el resultado de la calibración?</translation>
    </message>
</context>
<context>
    <name>PageVerticalList</name>
    <message>
        <location filename="../qml/PageVerticalList.qml" line="57"/>
        <source>Loading, please wait...</source>
        <translation type="unfinished">Cargando, por favor espera...</translation>
    </message>
</context>
<context>
    <name>PageVideos</name>
    <message>
        <location filename="../qml/PageVideos.qml" line="23"/>
        <source>Videos</source>
        <translation type="unfinished">Videos</translation>
    </message>
    <message>
        <location filename="../qml/PageVideos.qml" line="26"/>
        <source>Scanning the QR code will take you to our YouTube playlist with videos about this device.

Alternatively, use this link:</source>
        <translation type="unfinished">Al escanear el código QR, te llevará a nuestra lista de reproducción de YouTube con videos sobre este dispositivo.

Alternativamente, usa este enlace:</translation>
    </message>
</context>
<context>
    <name>PageWait</name>
    <message>
        <location filename="../qml/PageWait.qml" line="27"/>
        <source>Please Wait</source>
        <translation type="unfinished">Por favor espera</translation>
    </message>
</context>
<context>
    <name>PageWifiNetworkSettings</name>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="34"/>
        <source>Wireless Settings</source>
        <translation type="unfinished">Ajustes inalámbricos</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="90"/>
        <source>Connected</source>
        <translation type="unfinished">Conectado</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="90"/>
        <source>Disconnected</source>
        <translation type="unfinished">Desconectado</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="102"/>
        <source>Network Info</source>
        <translation type="unfinished">Información de Red</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="109"/>
        <source>DHCP</source>
        <translation type="unfinished">DHCP</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="191"/>
        <source>Apply</source>
        <translation type="unfinished">Aplicar</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="201"/>
        <source>Configuring the connection,
please wait...</source>
        <comment>This is horizontal-center aligned, ideally 2 lines</comment>
        <translation type="unfinished">Configurando la conexión, espera...</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="208"/>
        <source>Revert</source>
        <comment>Turn back the changes and go back to the previous configuration.</comment>
        <translation type="unfinished">Revertir</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="218"/>
        <source>Forget network</source>
        <comment>Removes all information about the network(settings, passwords,...)</comment>
        <translation type="unfinished">Olvidar red</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="226"/>
        <source>Forget network?</source>
        <translation type="unfinished">Red olvidada?</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="227"/>
        <source>Do you really want to forget this network&apos;s settings?</source>
        <translation type="unfinished">¿Realmente quieres olvidar la configuración de esta red?</translation>
    </message>
</context>
<context>
    <name>PageYesNoSimple</name>
    <message>
        <location filename="../qml/PageYesNoSimple.qml" line="28"/>
        <source>Are You Sure?</source>
        <translation type="unfinished">¿Estás seguro?</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSimple.qml" line="60"/>
        <source>Yes</source>
        <translation type="unfinished">Si</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSimple.qml" line="76"/>
        <source>No</source>
        <translation type="unfinished">No</translation>
    </message>
</context>
<context>
    <name>PageYesNoSwipe</name>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="29"/>
        <source>Are You Sure?</source>
        <translation type="unfinished">¿Estás seguro?</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="76"/>
        <source>Swipe to proceed</source>
        <translation type="unfinished">Desliza para continuar</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="97"/>
        <source>Yes</source>
        <translation type="unfinished">Si</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="110"/>
        <source>No</source>
        <translation type="unfinished">No</translation>
    </message>
</context>
<context>
    <name>PageYesNoWithPicture</name>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="28"/>
        <source>Are You Sure?</source>
        <translation type="unfinished">¿Estás seguro?</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="176"/>
        <source>Swipe for a picture</source>
        <translation type="unfinished">Desliza para ver imagen</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="254"/>
        <source>Yes</source>
        <translation type="unfinished">Si</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="267"/>
        <source>No</source>
        <translation type="unfinished">No</translation>
    </message>
</context>
<context>
    <name>PrusaPicturePictureButtonItem</name>
    <message>
        <location filename="../qml/PrusaPicturePictureButtonItem.qml" line="58"/>
        <source>Plugged in</source>
        <translation type="unfinished">Conectado</translation>
    </message>
    <message>
        <location filename="../qml/PrusaPicturePictureButtonItem.qml" line="58"/>
        <source>Unplugged</source>
        <translation type="unfinished">Desconectado</translation>
    </message>
</context>
<context>
    <name>PrusaSwitch</name>
    <message>
        <location filename="../qml/PrusaSwitch.qml" line="112"/>
        <source>Off</source>
        <translation type="unfinished">Ap.</translation>
    </message>
    <message>
        <location filename="../qml/PrusaSwitch.qml" line="170"/>
        <source>On</source>
        <translation type="unfinished">En.</translation>
    </message>
</context>
<context>
    <name>PrusaWaitOverlay</name>
    <message>
        <location filename="../qml/PrusaWaitOverlay.qml" line="74"/>
        <source>Please wait...</source>
        <comment>can be on multiple lines</comment>
        <translation type="unfinished">Espera por favor...</translation>
    </message>
</context>
<context>
    <name>SwipeSign</name>
    <message>
        <location filename="../qml/SwipeSign.qml" line="98"/>
        <source>Swipe to confirm</source>
        <translation type="unfinished">Desliza para confirmar</translation>
    </message>
</context>
<context>
    <name>WarningText</name>
    <message>
        <location filename="../qml/WarningText.qml" line="45"/>
        <source>Must not be empty, only 0-9, a-z, A-Z, _ and - are allowed here!</source>
        <translation type="unfinished">No debe estar vacío, ¡solo 0-9, a-z, A-Z, _ y - se permite aquí!</translation>
    </message>
</context>
<context>
    <name>WindowHeader</name>
    <message>
        <location filename="../PrusaComponents/Delegates/WindowHeader.qml" line="86"/>
        <source>back</source>
        <translation type="unfinished">Atrás</translation>
    </message>
    <message>
        <location filename="../PrusaComponents/Delegates/WindowHeader.qml" line="99"/>
        <source>cancel</source>
        <translation type="unfinished">cancelar</translation>
    </message>
    <message>
        <location filename="../PrusaComponents/Delegates/WindowHeader.qml" line="111"/>
        <source>close</source>
        <translation type="unfinished">cerrar</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="50"/>
        <source>Prusa SL1 Touchscreen User Interface</source>
        <translation type="unfinished">Interfaz de Usuario de la Pantalla Táctil Prusa SL1</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="64"/>
        <location filename="../qml/main.qml" line="69"/>
        <source>Unknown</source>
        <translation type="unfinished">Desconocido</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="65"/>
        <source>Activating</source>
        <translation type="unfinished">Activando</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="66"/>
        <source>Connected</source>
        <translation type="unfinished">Conectado</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="67"/>
        <source>Deactivating</source>
        <translation type="unfinished">Desactivando</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="68"/>
        <source>Deactivated</source>
        <translation type="unfinished">Desactivado</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="277"/>
        <source>Notifications</source>
        <translation type="unfinished">Notificaciones</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="320"/>
        <source>Initializing...</source>
        <translation type="unfinished">Inicializando...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="321"/>
        <source>The printer is initializing, please wait ...</source>
        <translation type="unfinished">La impresora se está inicializando, espere por favor ...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="330"/>
        <source>MC Update</source>
        <translation type="unfinished">Actualización MC</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="331"/>
        <source>The Motion Controller firmware is being updated.

Please wait...</source>
        <translation type="unfinished">Se está actualizando el firmware del controlador de movimiento.

Espera por favor...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="493"/>
        <source>Turn Off?</source>
        <translation type="unfinished">¿Apagar?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="506"/>
        <source>Cancel?</source>
        <translation type="unfinished">¿Cancelar?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="521"/>
        <source>Cancel the current print job?</source>
        <translation type="unfinished">¿Cancelar el trabajo de impresión actual?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="544"/>
        <source>Printer should not be turned off in this state.
Finish or cancel the current action and try again.</source>
        <translation type="unfinished">La impresora no se puede apagar en este estado. Finaliza o cancela la acción actual e inténtalo de nuevo.</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="555"/>
        <location filename="../qml/main.qml" line="579"/>
        <source>DEPRECATED PROJECTS</source>
        <translation type="unfinished">PROYECTOS OBSOLETOS</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="556"/>
        <source>Some incompatible projects were found, you can download them at 

http://%1/old-projects

Do you want to remove them?</source>
        <translation type="unfinished">Se encontraron algunos proyectos incompatibles, puede descargarlos en

http://%1/old-projects

¿Quieres eliminarlos?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="570"/>
        <location filename="../qml/main.qml" line="593"/>
        <source>&lt;printer IP&gt;</source>
        <translation type="unfinished">&lt;printer IP&gt;</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="580"/>
        <source>Some incompatible file were found, you can view them at http://%1/old-projects.

Would you like to remove them?</source>
        <translation type="unfinished">Se encontraron algunos proyectos incompatibles, puedes descargarlos en

http://%1/old-projects.

¿Quieres eliminarlos?</translation>
    </message>
</context>
<context>
    <name>utils</name>
    <message>
        <location filename="../qml/utils.js" line="47"/>
        <source>Less than a minute</source>
        <translation type="unfinished">Menos de un minuto</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="50"/>
        <source>%n h</source>
        <comment>how many hours</comment>
        <translation type="unfinished">
            <numerusform>%n h</numerusform>
            <numerusform>%n h</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="51"/>
        <source>%n min</source>
        <comment>how many minutes</comment>
        <translation type="unfinished">
            <numerusform>%n min</numerusform>
            <numerusform>%n min</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="55"/>
        <source>%n hour(s)</source>
        <comment>how many hours</comment>
        <translation type="unfinished">
            <numerusform>%n hora</numerusform>
            <numerusform>%n horas</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="56"/>
        <source>%n minute(s)</source>
        <comment>how many minutes</comment>
        <translation type="unfinished">
            <numerusform>%n minuto</numerusform>
            <numerusform>%n minutos</numerusform>
        </translation>
    </message>
</context>
</TS>
