# How to create translations files

1. Build the touch-ui with `<touch-ui-repo>/build/build.sh` script to obtain strings from Prusa-Error-Codes repo.
2. Refresh translations with `<touch-ui-repo>/translations/refresh_translations.sh`. This should append all strings into the .ts files.
3. Send `en_US_sorted_for_translators.ts` to the translators team.
4. After receiving *.ts files:
   1. Move the old files to separate folder. For example `<touch-ui-repo>/translations/old/`.
   2. Move the new file to folder `<touch-ui-repo>/translations/`.
5. Run `<touch-ui-repo>/translations/refresh_translations.sh` again. It should correct the `<context>` the xml. The `<context>` needs to be created for each .qml file.
6. Test if transtalions work for string in `<touch-ui-repo>/*.qml` and also for strings in `<touch-ui-repo>/build/*.qml` eg.: ErrorcodesText.qml.
7. Check, that every source strings is represented in the translated file(./check_translations.py old/old_file.ts new_translated_file.ts -v). Should be enough for one language. The script should output warnings for any missing/untranslated strings(empty tranlation is considered valid in this step, it means that the original will be used) and will try to find similar texts for those not found(in case of misspelling). Empty output means everyting is fine.
8. Check warnings for translation consistency(leading/trailing whitespaces, newlines, punctuation): ./check_translations.py file.ts -v
9. Delete folder with old .ts files.
