project(pictures)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
include_directories("${CMAKE_CURRENT_SOURCE_DIR}")
include_directories("${CMAKE_CURRENT_BINARY_DIR}")

find_package(QT NAMES Qt6 Qt5 COMPONENTS Core Quick REQUIRED)

add_library(
    ${PROJECT_NAME} STATIC
    pictures.qrc
    )


add_library(myProject::${PROJECT_NAME} ALIAS ${PROJECT_NAME})
target_include_directories( ${PROJECT_NAME} PUBLIC ${PROJECT_SOURCE_DIR})
