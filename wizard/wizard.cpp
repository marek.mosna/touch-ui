/*
    Copyright 2020-2022, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "wizard.h"


Wizard::Wizard(QJSEngine & engine, QObject *parent):
    BaseDBusObject(engine, service, interface, path, QDBusConnection::systemBus(), parent),
    wizardProxy(service, path, QDBusConnection::systemBus(), this),
    checksModel(this), sortedChecksModel(this) {
    qRegisterMetaType<CheckStatesType>("CheckStatesType");
    qRegisterMetaType<CheckDataType>("CheckDataType");
    qDBusRegisterMetaType<CheckStatesType>();
    qDBusRegisterMetaType<CheckDataType>();

    connect(this, &Wizard::checksChanged, &checksModel, &WizardChecksModel::change);

    sortedChecksModel.setSourceModel(&checksModel);
    sortedChecksModel.setSortRole(WizardChecksModel::Roles::RoleCheckState);
    sortedChecksModel.sort(0, Qt::SortOrder::DescendingOrder);
    sortedChecksModel.setFilterRole(WizardChecksModel::Roles::RoleHidden);
    sortedChecksModel.setFilterRegExp(QRegExp("false", Qt::CaseInsensitive));
}

int Wizard::identifier() {
    return wizardProxy.identifier();
}

int Wizard::state() {
    return m_state;
}

CheckStatesType Wizard::checkStates() const {
    return m_checkStates;
}

CheckDataType Wizard::checkData() const {
    return m_checkData;
}

QAbstractItemModel* Wizard::getChecksModel() {
    return &sortedChecksModel;
}

QVariantMap Wizard::data() const {
    return wizardProxy.data();
}

bool Wizard::set(const QString &name, const QVariant &value)
{
    if(name == QStringLiteral("identifier")) {
        int identifier = value.toInt();
        emit identifierChanged(identifier);
    }
    else if(name == QStringLiteral("state")) {
        int state = value.toInt();
        m_state = state;
        emit stateChanged(state);
    }
    else if(name == QStringLiteral("check_states")) {
        CheckStatesType states;
        qvariant_cast< QDBusArgument >(value) >> states;
        m_checkStates = states;
        emit checkStatesChanged(states);
        emit checksChanged(states, m_checkData);
        emit totalChecksChanged(countTotalChecks(states));
        emit finishedChecksChanged(countFinishedChecks(states));
        emit runningChecksChanged(countRunningChecks(states));
    }
    else if(name == QStringLiteral("check_data")) {
        CheckDataType data;
        qvariant_cast<QDBusArgument>(value) >> data;
        m_checkData = data;
        emit checkDataChanged(data);
        emit checksChanged(m_checkStates, data);
    }
    else if(name == QStringLiteral("cancelable")) {
        bool cancelable = value.toBool();
        emit cancelableChanged(cancelable);
    }
    else if(name == QStringLiteral("check_exception")) {
        QVariantMap exception;
        qvariant_cast< QDBusArgument >(value) >> exception;
        emit checkExceptionChanged(exception);
    }
    else if(name == QStringLiteral("data")) {
        QVariantMap data;
        qvariant_cast<QDBusArgument>(value) >> data;

        emit dataChanged(data);
    }
    else if(name == QStringLiteral("check_warnings")) {
        QList<QVariantMap> data;
        qvariant_cast<QDBusArgument>(value) >> data;
        m_check_warnings = data;
        emit check_warningsChanged();
    }
    else {
        return BaseDBusObject::set(name, value);
    }
    return true;
}

int Wizard::totalChecks() const {
    return countTotalChecks(checkStates());
}

int Wizard::finishedChecks() const {
    return countFinishedChecks(checkStates());
}

int Wizard::runningChecks() const {
    return countRunningChecks(checkStates());
}

int Wizard::countTotalChecks(const CheckStatesType &checkStates) const {
    return checkStates.size();
}

int Wizard::countFinishedChecks(const CheckStatesType &checkStates) const {
    int finished = 0;
    for(int const& state : checkStates) {
        if(state == WizardChecksModel::CheckState::Success) {
            finished++;
        }
    }
    return finished;
}

int Wizard::countRunningChecks(const CheckStatesType &checkStates) const {
    int running = 0;
    for(int const& state : checkStates) {
        if(state == WizardChecksModel::CheckState::Running) {
            running++;
        }
    }
    return running;
}

void Wizard::cancel(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(wizardProxy.cancel(), callback_ok, callback_fail);
}

void Wizard::abort(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(wizardProxy.abort(), callback_ok, callback_fail);
}

void Wizard::retry(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(wizardProxy.retry(), callback_ok, callback_fail);
}

bool Wizard::cancelable() {
    return wizardProxy.cancelable();
}

QVariantMap Wizard::checkException() {
    return wizardProxy.check_exception();
}

void Wizard::prepareWizardPart1Done(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(wizardProxy.prepare_wizard_part_1_done(), callback_ok, callback_fail);
}

void Wizard::prepareWizardPart2Done(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(wizardProxy.prepare_wizard_part_2_done(), callback_ok, callback_fail);
}

void Wizard::prepareWizardPart3Done(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(wizardProxy.prepare_wizard_part_3_done(), callback_ok, callback_fail);
}

void Wizard::prepareDisplaytestDone(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(wizardProxy.prepare_displaytest_done(), callback_ok, callback_fail);
}

void Wizard::reportDisplay(bool ok, QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(wizardProxy.report_display(ok), callback_ok, callback_fail);
}

void Wizard::reportAudio(bool ok, QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(wizardProxy.report_audio(ok), callback_ok, callback_fail);
}

void Wizard::safetyStickerRemoved(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(wizardProxy.safety_sticker_removed(), callback_ok, callback_fail);
}

void Wizard::sideFoamRemoved(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(wizardProxy.side_foam_removed(), callback_ok, callback_fail);
}

void Wizard::tankFoamRemoved(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(wizardProxy.tank_foam_removed(), callback_ok, callback_fail);
}

void Wizard::displayFoilRemoved(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(wizardProxy.display_foil_removed(), callback_ok, callback_fail);
}

void Wizard::prepareCalibrationFinishDone(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(wizardProxy.prepare_calibration_finish_done(), callback_ok, callback_fail);
}

void Wizard::showResultsDone(QJSValue callback_ok, QJSValue callback_fail)
{
    handleCallbackWithError(wizardProxy.show_results_done(), callback_ok, callback_fail);
}

void Wizard::prepareCalibrationTiltAlignDone(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(wizardProxy.prepare_calibration_tilt_align_done(), callback_ok, callback_fail);
}

void Wizard::prepareCalibrationPlatformAlignDone(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(wizardProxy.prepare_calibration_platform_align_done(), callback_ok, callback_fail);
}

void Wizard::prepareCalibrationPlatformTankDone(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(wizardProxy.prepare_calibration_platform_tank_done(), callback_ok, callback_fail);
}

void Wizard::tiltCalibrationDone(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(wizardProxy.tilt_calibration_done(), callback_ok, callback_fail);
}

void Wizard::tiltMove(int speed, QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(wizardProxy.tilt_move(speed), callback_ok, callback_fail);
}

void Wizard::foamInserted(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(wizardProxy.foam_inserted(), callback_ok, callback_fail);
}

void Wizard::uvCalibrationPrepared(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(wizardProxy.uv_calibration_prepared(), callback_ok, callback_fail);
}

void Wizard::uvMeterPlaced(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(wizardProxy.uv_meter_placed(), callback_ok, callback_fail);
}

void Wizard::uvCalibrationApplyResults(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(wizardProxy.uv_apply_result(), callback_ok, callback_fail);
}

void Wizard::uvCalibrationDiscardResults(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(wizardProxy.uv_discard_results(), callback_ok, callback_fail);
}

void Wizard::sl1sConfirmUpgrade(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(wizardProxy.sl1s_confirm_upgrade(), callback_ok, callback_fail);
}

void Wizard::sl1sRejectUpgrade(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(wizardProxy.sl1s_reject_upgrade(), callback_ok, callback_fail);
}

void Wizard::newExpoPanelDone(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(wizardProxy.new_expo_panel_done(), callback_ok, callback_fail);
}

void Wizard::tank_surface_cleaner_init_done()
{
    wizardProxy.tank_surface_cleaner_init_done();
}

void Wizard::insert_cleaning_adaptor_done()
{
    wizardProxy.insert_cleaning_adaptor_done();
}

void Wizard::remove_cleaning_adaptor_done()
{
    wizardProxy.remove_cleaning_adaptor_done();
}

const QList<QVariantMap> &Wizard::check_warnings() const
{
    return m_check_warnings;

}

void Wizard::resetWizardChecksModel()
{
    this->checksModel.reset();
}
