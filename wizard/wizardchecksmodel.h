/*
    Copyright 2020, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef WIZARDCHECKSMODEL_H
#define WIZARDCHECKSMODEL_H

#include <QAbstractListModel>


#include "wizard/types.h"

/** @brief C++ model for the wizard checks */
class WizardChecksModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum Check {
        Unknown = 0,
        TowerRange,
        TowerHome,
        TiltRange,
        TiltHome,
        Display,
        Calibration,
        Music,
        UVLEDs,
        UVFans,
        MoveToFoam,
        MoveToTank,
        ResinSensor,
        SerialNumber = 20,
        Temperature,
        TiltCalibrationStart,
        TiltCalibration,
        TowerCalibration,
        TiltTiming,
        SysInfo,
        CalibrationInfo,
        EraseProjects,
        ResetHostname,
        ResetAPIKey,
        ResetRemoteConfig,
        ResetHttpDigest,
        ResetWifi,
        ResetTimezone,
        ResetNTP,
        ResetLocale,
        ResetUVCalibrationData,
        RemoveSlicerProfiles,
        ResetHWConfig,
        EraseMCEeprom,
        ResetHomingProfiles,
        SendPrinterData,
        DisableFactory,
        InitiatePackingMoves,
        FinishPackingMoves,
        DisableAccess,
        UVMeterPresent = 60,
        UVWarmup,
        UVMeterPlacement,
        UVCalibrateCenter,
        UVCalibrateEdge,
        UVCalibrationApplyResults,
        UVMetereRemoved,
        TiltLevel,
        ResetTouchUI,
        EraseUVPWM,
        ResetSelfTest,
        ResetMechanicalCalibration,
        MarkPrinterModel,
        ResetHwCounters,
        RecordExpoLog,

        TowerSafeDistance = 76,
        TowerTouchdown,
        ExposingDebris,
        TowerGentlyUp,
        WaitingForUser = 80,
        TowerHomeFinish = 81
    };
    Q_ENUM(Check)

    enum CheckState {
        Waiting = 0,
        Running,
        Success,
        Failure,
        Warning,
        User,
        Canceled,
    };
    Q_ENUM(CheckState)

    enum Roles {
        RoleCheckId = Qt::UserRole + 1,
        RoleCheckState,
        RoleCheckData,
        RoleHidden,
    };

protected:
    struct CheckRecord {
        Check checkId;
        CheckState checkState;
        QVariantMap checkData;
        bool hidden;
        CheckRecord(Check _checkId = Temperature, CheckState _checkState = Waiting, QVariantMap _checkData = QVariantMap(), bool _hidden = false):
            checkId(_checkId), checkState(_checkState), checkData(_checkData), hidden(_hidden) {}
    };

public:
    explicit WizardChecksModel(QObject *parent = nullptr);
    QHash<int, QByteArray> roleNames() const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;
    Qt::ItemFlags flags(const QModelIndex& index) const override;
    /** Reset the model content / empty it */
    void reset();
public slots:
    void change(const CheckStatesType &states, const CheckDataType &data);

private:
    QList<CheckRecord> listData;
};


#endif // WIZARDCHECKSMODEL_H
