/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QDateTime>
#include "fmcontext.h"
#include "fmdata.h"
#include "filemanager.h"
#include "fmdirectory.h"

FMContext::FMContext(QObject *parent) : QObject(parent), frozen(false), m_data(FMData::getInstance()), m_currentDirectory(FMData::getInstance()->getRoot())
{
    m_busy = false;
    reset();

    // Freeze all operation on the underlaying data structure
    connect(FMData::getInstance(), &FMData::dataChangeBegin, this, [&](){
        frozen = true;
        m_currentDirectory = nullptr;
    });

    // If the underlying data structure changes, seek out the new equivalent of the current directory
    // Otherwise reset to root
    connect(FMData::getInstance(), &FMData::dataChangeEnd, this, &FMContext::update);

    connect(this, &FMContext::currentChanged, this, [&](){
        Q_ASSERT(m_currentDirectory);
        setCurrentPath(m_currentDirectory->path);
        setCurrentOrigin(m_currentDirectory->origin);
        setBusy(false);
    });


}

void FMContext::setCurrentOrigin(FMFile::Origin _origin) {
    if(_origin != m_currentOrigin) {
        m_currentOrigin = _origin;
        emit currentOriginChanged(m_currentOrigin);
    }
}

QJSValue FMContext::current() const {
    if(frozen) {
        qDebug() << "FMContext::current - attempt while frozen";
        return QJSValue();
    }

    Q_ASSERT(m_currentDirectory);
    qDebug() << "FMContext::current()...path:" << m_currentPath <<"current:" << (void*)m_currentDirectory;
    QQmlEngine *ownerEngine = getOwnerEngine();
    if(! ownerEngine) return QJSValue();
    QJSValue tmp = ownerEngine->newObject();
    tmp.setProperty("name", m_currentDirectory->path.split("/").last());
    tmp.setProperty("type", m_currentDirectory->type());
    tmp.setProperty("path", m_currentDirectory->path);
    tmp.setProperty("origin", m_currentDirectory->origin);
    tmp.setProperty("childrenCount", m_currentDirectory->children.length());
    tmp.setProperty("children", list());
    return tmp;
}

bool FMContext::isAtRoot() const {
    if(frozen) {
        qDebug() << "FMContext::isAtRoot - attempt while frozen";
        return false;
    }

    Q_ASSERT(m_currentDirectory);
    return m_currentDirectory->getParent() == m_currentDirectory;
}

void FMContext::cd(int index) {
    if(frozen) {
        qDebug() << "FMContext::cd - attempt while frozen";
        return;
    }

    qDebug() << "FMContext::cd(child" << index <<")";
    bool origIsRoot = isAtRoot();

    if(index >= 0 && index < m_currentDirectory->children.length()
            && m_currentDirectory->children[index]->type() == FMFile::Directory) {

        m_currentDirectory = static_cast<FMDirectory*>(m_currentDirectory->children[index]);
        Q_ASSERT(m_currentDirectory);
        Q_ASSERT(m_currentDirectory->type() == FMFile::Directory);

        qDebug() << "FSContext" << this->objectName() <<"cd " <<  m_currentDirectory->path;

        setCurrentPath(m_currentDirectory->path);
        emit currentChanged();
    }

    if(isAtRoot() != origIsRoot) emit isAtRootChanged(isAtRoot());
}

void FMContext::remove(int index)
{
    if(frozen) {
        qDebug() << "FMContext::remove - attempt while frozen";
        return;
    }

    setBusy(true);

    qDebug() << "FMContext::remove(child" << index <<")";
    auto & child = m_currentDirectory->children[index];
    if(child->type() == FMFile::File) {
        FileManager::getInstance()->remove(child->path,
                                           [](QVariantList vargs){
            Q_UNUSED(vargs)
            qDebug() << "File removed, refreshing...";
            FMData::getInstance()->handleUpdate(QVariantMap());
        });
    }
    else if(child->type() == FMFile::Directory && ! isAtRoot()) {
        FileManager::getInstance()->remove_dir(child->path, true,
                                           [](QVariantList vargs){
            Q_UNUSED(vargs)
            qDebug() << "File removed, refreshing...";
            FMData::getInstance()->handleUpdate(QVariantMap());
        });
    }

}

void FMContext::reset() {
    qDebug() << "Reseting context" << this->objectName();
    m_currentDirectory = static_cast<FMDirectory*>(m_data->getRoot());
    Q_ASSERT(m_currentDirectory->type() == FMFile::Directory);
    emit currentChanged();
    emit isAtRootChanged(isAtRoot());
    setCurrentPath(m_currentDirectory->path);

}

QJSValue FMContext::list() const {
    if(frozen) {
        qDebug() << "FMContext::list - attempt while frozen";
        return QJSValue();
    }

    qDebug() << "FMContext::list() current:" << (void*)m_currentDirectory;
    QQmlEngine *ownerEngine = getOwnerEngine();
    if(! ownerEngine) return QJSValue();
    QJSValue ret = ownerEngine->newArray(m_currentDirectory ? m_currentDirectory->children.length() : 0 );
    for(int i = 0; i < m_currentDirectory->children.length(); ++i) {
        FMFile * child = m_currentDirectory->children.at(i);
        Q_ASSERT(child);
        QJSValue tmp = qmlFileStructure(child);
        ret.setProperty(i, tmp);
    }
    return ret;
}

void FMContext::collectFilesWithOrigin(FMFile * root, FMFile::Origin origin, QVector<FMFile *> & out) const
{
    if(root->type() == FMFile::File) {
        if(root->origin == origin) {
            out.append(root);
        }
    }
    else if(root->type() == FMFile::Directory && (root->origin == origin || root->origin == FMFile::InvalidOrigin)) {
        FMDirectory * dir = static_cast<FMDirectory*>(root);
        for(auto it = std::next(dir->children.begin()); it != dir->children.end(); ++it) {
            FMFile * child = *it;
            collectFilesWithOrigin(child, origin, out);
        }
    }
}

QJSValue FMContext::qmlFileStructure(FMFile *file) const
{
    Q_ASSERT(file);

    QQmlEngine *ownerEngine = getOwnerEngine();
    if(! ownerEngine) return QJSValue();
    QJSValue tmp = ownerEngine->newObject();

    tmp.setProperty("name", file->path.split("/").last());
    tmp.setProperty("type", file->type());
    tmp.setProperty("path", file->path);
    tmp.setProperty("origin", file->origin);
    tmp.setProperty("mtime", ownerEngine->toScriptValue(QDateTime::fromSecsSinceEpoch(file->mtime)));
    if(file->type() == FMFile::Directory) {
        FMDirectory * dir = static_cast<FMDirectory*>(file);
        tmp.setProperty("childrenCount", dir->children.length() - 1); // Without ".."
        tmp.setProperty("isRoot", dir->children.first() == dir);
        tmp.setProperty("isUnderRoot", m_currentDirectory->children.first() == m_currentDirectory); // Is its parent root?
    }
    else if(file->type() == FMFile::File) {
        tmp.setProperty("size", static_cast<qreal>(file->size));
    }
    return tmp;
}

QQmlEngine *FMContext::getOwnerEngine() const {
    QQmlContext *ctx =  QQmlEngine::contextForObject(this);
    if(! ctx) return nullptr;
    QQmlEngine *ownerEngine = ctx->engine();
    return ownerEngine;
}

bool FMContext::busy() const
{
    return m_busy;
}

void FMContext::setBusy(bool newBusy)
{
    if (m_busy == newBusy)
        return;
    m_busy = newBusy;
    emit busyChanged();
}

QJSValue FMContext::flatList(FMFile::Origin origin) const
{
    if(frozen) {
        qDebug() << "FMContext::list - attempt while frozen";
        return QJSValue();
    }

    FMDirectory*  root =  m_data->getRoot();
    QVector<FMFile*> list;
    collectFilesWithOrigin(root, origin, list);
    FMFile::sortByTime(list.begin(), list.end());

    // Make it qml-friendly
    QQmlEngine *ownerEngine = getOwnerEngine();
    if(! ownerEngine) return QJSValue();
    QJSValue jsList = ownerEngine->newArray(list.length());
    for(int i = 0; i < list.length(); ++i) {
        jsList.setProperty(i, qmlFileStructure(list[i]));
    }
    return jsList;
}

void FMContext::get_metadata(QString path, bool get_thumbnails, QJSValue callback_ok, QJSValue callback_fail) {

    // Supplies the callback with a QQmlEngine from the current snapshot of QML context
    // in case it will be deleted later.
    //
    // This requires that QQmlEngine has longer lifetime than both FMContext and FMData,
    // which is and should be the case. Otherwise it will crash
    QQmlContext * ctx =  QQmlEngine::contextForObject(this);
    if(!ctx) {
        return;
    }
    QQmlEngine *ownerEngine = ctx->engine();
    if(!ownerEngine) {
        return;
    }

    FMData::getInstance()->get_metadata(path, get_thumbnails, [=](FMData::metadata_t metadata) mutable{
        QJSValue ret = ownerEngine->newObject();
        ret.setProperty("path", metadata.path);
        ret.setProperty("size", (qreal)metadata.size);
        ret.setProperty("mtime", (qreal)metadata.mtime.toSecsSinceEpoch());
        ret.setProperty("expTime", metadata.expTime);
        ret.setProperty("expTimeFirst", metadata.expTimeFirst);
        ret.setProperty("fileCreationTimestamp", metadata.fileCreationTimestamp);
        ret.setProperty("layerHeight", metadata.layerHeight);
        ret.setProperty("materialName", metadata.materialName);
        ret.setProperty("numFade", metadata.numFade);
        ret.setProperty("numFast", metadata.numFast);
        ret.setProperty("numSlow", metadata.numSlow);
        ret.setProperty("printProfile", metadata.printProfile);
        ret.setProperty("printTime", metadata.printTime);
        ret.setProperty("printerModel", metadata.printerModel);
        ret.setProperty("printerProfile", metadata.printerProfile);
        ret.setProperty("printerVariant", metadata.printerVariant);
        ret.setProperty("prusaSlicerVersion", metadata.prusaSlicerVersion);
        ret.setProperty("usedMaterial", metadata.usedMaterial);
        QJSValue thumbnailList = ownerEngine->newArray();
        int thumbIndex = 0;
        for(const QString & thumb : qAsConst(metadata.thumbnails)) {
            thumbnailList.setProperty(thumbIndex++, thumb);
        }
        ret.setProperty("thumbnails", thumbnailList);
        if(callback_ok.isCallable()) {
            callback_ok.call({ret});
        }

    }, [=](QVariantList vl) mutable{
        if(callback_fail.isCallable()) {
            QJSValueList params;
            foreach(const QVariant & varg, vl) {
                params.append( ownerEngine->toScriptValue<>(varg));
            }
            callback_fail.call(params);
        }
    });
}


void FMContext::update() {
    qDebug() << "FMContext::update(" << objectName() << ") update:" << currentPath();
    FMDirectory *newDir = static_cast<FMDirectory*>(FMData::getInstance()->getFile(currentPath(), currentOrigin()));
    if(newDir) {
        Q_ASSERT(newDir->type() == FMFile::Directory);
        m_currentDirectory = newDir;
    }
    else reset();
    frozen = false;
    emit currentChanged();
    setBusy(false);
}

void FMContext::forceCurrentPath(QString newPath) {
    if(frozen) {
        qDebug() << "FMContext::setCurrentPath - attempt while frozen";
        return;
    }

    FMDirectory * node = m_data->getRoot();
    FMDirectory * prev = node;
    for(QString & dirname : newPath.split(QLatin1Char('/'), Qt::SkipEmptyParts)) {
        prev = node;
        node = static_cast<FMDirectory*>(node->findChildByName(dirname, FMFile::Directory));
        if(nullptr == node) {
            qDebug() << "FMContext: Trying to force an invalid path:" << newPath;
            qDebug() << "Directory" << dirname << "was not found in directory " << prev->path;
            emit currentPathChanged(m_currentPath); // Keep the current path
            return;
        }
    }

    m_currentDirectory = node;
    Q_ASSERT(m_currentDirectory);
    Q_ASSERT(m_currentDirectory->type() == FMFile::Directory);
    qDebug() << "FSContext" << this->objectName() <<"forceCurrentPath " <<  m_currentDirectory->path;

    setCurrentPath(node->path);
    emit currentChanged();
}

void FMContext::setCurrentPath(QString newPath)
{
    if(newPath != m_currentPath) {
        m_currentPath = newPath;
        emit currentPathChanged(m_currentPath);
    }
}
