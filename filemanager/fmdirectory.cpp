/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "fmdirectory.h"

FMDirectory::FMDirectory(QString _path, FMFile::Origin _origin, qreal _mtime, const QVector<FMFile *> &_children, FMFile *_parentFile) : FMFile(_path, _origin, 0, _mtime, _parentFile), children(_children)
{
    children.push_front(_parentFile); // First child is a reference to the parent
}

FMDirectory::~FMDirectory() {
    this->clear();
}

void FMDirectory::clear() {
    FMDirectory * tmp = static_cast<FMDirectory*>(getParent());
    children.pop_front(); // Remove the reference to parent
    qDeleteAll(children); // Remove all the children
    children.clear();     // Remove pointers
    children.push_back(tmp); // Return the reference to parent
}

FMFile::Type FMDirectory::type() const{
    return FMFile::Directory;
}

FMFile *FMDirectory::findChildByName(QString _name, FMFile::Type _type) {
    for(FMFile * child : qAsConst(children)) {
        if(child->type() == _type || _type == FMFile::InvalidType) {
            if(child->path != "" && child->path.split("/").last() == _name) {
                return child;
            }
        }
    }
    return nullptr;
}
