/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FMCONTEXT_H
#define FMCONTEXT_H

#include <QObject>
#include <QJSValue>
#include <QJSEngine>
#include <QQmlContext>

#include "fmfile.h"

class FMData;
class FMDirectory;

/** FSContext offers simplified access to the tree structure from QML
 *  Depends on FSData instance.
 */
class FMContext : public QObject
{
    Q_OBJECT

    QString m_currentPath;

    /// If true, any access to the underlying data structure is forbiden
    bool frozen;

    /** @brief Find all the files with the set origin and return them in a flat list
     *  Relies on the fact that children can only have the same origin as their
     *  parent(with the exception of root)
     *  @param root node to be processed(recursively)
     *  @param origin origin of the files we are searching for
     *  @param out output, all the found files will be stored here
     */
    void collectFilesWithOrigin(FMFile*root, FMFile::Origin origin, QVector<FMFile*>&out) const;

    /** @brief Create the qml-friendly representation for the file */
    QJSValue qmlFileStructure(FMFile * file) const;

    /** @return ownerEngine or nullptr if there is none */
    QQmlEngine * getOwnerEngine() const;

    /** Recorded origin of the current directory, this is useful because the directory object
     *  is deleted on refresh and its properties are no longer available
     */
    FMFile::Origin m_currentOrigin;
    bool m_busy;
public:
    explicit FMContext(QObject *parent = nullptr);
    FMData * m_data;
    FMDirectory * m_currentDirectory;

    /// Two setters: forceCurrent path to set the path from QML
    /// and setCurrentPath - primitive setter used in c++
    Q_PROPERTY(QString currentPath READ currentPath WRITE forceCurrentPath NOTIFY currentPathChanged)
    Q_PROPERTY(QJSValue current READ current NOTIFY currentChanged)
    Q_PROPERTY(bool isAtRoot READ isAtRoot NOTIFY isAtRootChanged)
    Q_PROPERTY(FMFile::Origin currentOrigin READ currentOrigin WRITE setCurrentOrigin NOTIFY currentOriginChanged)
    Q_PROPERTY(bool busy READ busy WRITE setBusy NOTIFY busyChanged)


    QString currentPath() const {
        return m_currentPath;
    }

    FMFile::Origin currentOrigin() const {
        return m_currentOrigin;
    }

    void setCurrentOrigin(FMFile::Origin _origin);
    bool busy() const;


    /** @returns The current directory in the form of JSValue
     *           along with the list of its children.
    **/
    QJSValue current() const;

    /** Is the current directory root? */
    bool isAtRoot() const;


    /** Change the working directory to a child(at index) of the current one */
    Q_INVOKABLE void cd(int index);

    /** Remove child at index */
    Q_INVOKABLE void remove(int index);

    /** Reset context - current directory to the root of the tree */
    Q_INVOKABLE void reset();

    /** List children of the current directory */
    Q_INVOKABLE QJSValue list() const;

    /** Flat list(made by recursive pass) of all files starting at directory directoryPath */
    Q_INVOKABLE QJSValue flatList(int origin) const {return flatList(FMFile::Origin(origin)); }
    QJSValue flatList(FMFile::Origin origin) const;

    /** Get metadata for file */
    Q_INVOKABLE void get_metadata(QString path, bool get_thumbnails, QJSValue callback_ok, QJSValue callback_fail);

public slots:
    /// If the underlying data structure changes, seek out the new equivalent of the current directory
    /// Otherwise reset to root
    void update();

    /** Setter used to set the object path from QML, uses the path of directories
     * as they are presented by the filemanager:  /Local/examples/
     * The path must always be absolute.
     */
    void forceCurrentPath(QString currentPath);

    void setBusy(bool newBusy);

signals:

private:
    void setCurrentPath(QString newPath);
signals:
    void currentChanged();
    void isAtRootChanged(bool);
    void currentPathChanged(QString);
    void currentOriginChanged(FMFile::Origin);
    void busyChanged();
};

#endif // FMCONTEXT_H
