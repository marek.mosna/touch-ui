/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FMDIRECTORY_H
#define FMDIRECTORY_H

#include "fmfile.h"
#include <QObject>

class FMDirectory : public FMFile
{
public:
    FMDirectory(QString _path = QStringLiteral(""),
            Origin _origin = Origin::InvalidOrigin,
            qreal _mtime = 0,
            const QVector<FMFile*> & _children = {},
            FMFile * _parentFile = nullptr);

    virtual ~FMDirectory();

    /** Remove and delete all children */
    void clear();

    virtual FMFile::Type type() const override;

    QVector<FMFile*> children;

    Q_INVOKABLE FMFile* getParent() const {
        return children.at(0);
    }

    Q_INVOKABLE void setParent(FMDirectory * _parentDirectory) {
        Q_ASSERT(_parentDirectory != nullptr);
        children[0] = _parentDirectory;
    }

    inline FMFile & addFile(QString _path, Origin _origin, size_t _size, qreal mtime) {
        children.push_back(new FMFile(_path, _origin, _size, mtime, this));
        return *children.last();
    }

    inline FMDirectory & addDirectory(QString _path, Origin _origin, qreal _mtime, QVector<FMFile*> _children = {}) {
        children.push_back(new FMDirectory(_path, _origin, _mtime, _children, this));
        return *(FMDirectory*)children.last();
    }

    inline FMDirectory & addDirectory(FMDirectory * _directory) {
        _directory->setParent(this);
        children.push_back(_directory);
        return *_directory;
    }

    FMFile *findChildByName(QString _name, FMFile::Type _type);


    inline friend QDebug operator<<(QDebug debug, const FMDirectory &dir) {
        debug << "FMDirectory[" << dir.path << dir.origin << dir.children <<"]";
        return debug;
    }
};


#endif // FMDIRECTORY_H
