/*
    Copyright 2022, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef BASEDBUSOBJECT_H
#define BASEDBUSOBJECT_H

#include <QObject>
#include <QJSEngine>
#include <QDBusPendingReply>
#include <QDBusServiceWatcher>
#include <properties_proxy.h>
#include "dbusutils.h"

class BaseDBusObject : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool isServiceRegistered READ isServiceRegistered /*WRITE setIsServiceRegistered*/ NOTIFY isServiceRegisteredChanged)
    Q_PROPERTY(bool propertiesValid READ propertiesValid WRITE setPropertiesValid NOTIFY propertiesValidChanged)
    Q_PROPERTY(bool serviceIsOnDemand READ serviceIsOnDemand WRITE setServiceIsOnDemand NOTIFY serviceIsOnDemandChanged)
    Q_PROPERTY(bool emitExceptionOnUnregistration READ emitExceptionOnUnregistration WRITE setEmitExceptionOnUnregistration NOTIFY emitExceptionOnUnregistrationChanged)


protected:
    QSharedPointer<QDBusServiceWatcher> m_watcher;
    QDBusConnection m_connection;
    PropertiesProxy m_properties;
    QJSEngine & m_engine;
    QTimer m_loadPropertiesRepeatedAttempt;

    QString m_service;
    QString m_interface;
    QString m_path;

    bool m_emitExceptionOnUnregistration{false};
    bool m_isServiceRegistered{false};
    // Needed to facilitate javascript callbacks for async methods
    bool m_propertiesLoaded{false};
    bool m_serviceIsOnDemand{false};
    /**
     * @brief watcherInstance returns QDBusServiceWatcher instance
     * This QDBusServiceWatcher is shared between all instances of BaseDBusObject class to conserve resources
     * @param service A DBus service, i.e. "cz.prusa3d.sl1.printer0"
     * @param connection A DBus connection, usually QDBusConnection::systemBus()
     * @return QDBusServiceWatcher instance, common for all derived classes
     */
    QSharedPointer<QDBusServiceWatcher> watcherInstance(const QString & service, const QDBusConnection &connection);

    /**
     * @brief setServiceRegistered This should not be modified from outside this class
     * @param newServiceReady
     */
    void setIsServiceRegistered(bool newServiceReady);

public:
    explicit BaseDBusObject(QJSEngine & engine, const QString & service, const QString & interface, const QString &path, const QDBusConnection &connection, QObject *parent = nullptr);

    /**
     * @property isServiceRegistered
     * @default false
     * @brief serviceReady The service (represented by this object) is registered on DBus
     * @return registered / not registered
     */
    bool isServiceRegistered() const;

    /**
     * @property propertiesValid
     * @default false
     * @brief propertiesLoaded Indicates whether the contained properties reflect those of the DBus service
     * In time between creation of the object and loading of property values, this will return false.
     * If DBus service is unregistered, this will return false.
     *
     * @return Properties of this object are loaded - true or false
     */
    bool propertiesValid() const;
    void setPropertiesValid(bool newPropertiesValid);

    /**
     * @property serviceIsOnDemand
     * @default false
     * @brief  ServiceIsOnDemand disables automatic reload_properties when the service registers on DBus.
     * Update of properties will depend only on the derived class, which should call reload_properties()
     *  when appropriate.
     * This is useful for services that are activated on-demand, like org.freedesktop.timedate1
     * @return On-demand or not
     */
    bool serviceIsOnDemand() const;
    void setServiceIsOnDemand(bool newFakeServiceRegistered);

    bool emitExceptionOnUnregistration() const;
    void setEmitExceptionOnUnregistration(bool newEmitExceptionOnUnregistration);

signals:
    /**
     * @brief dbusError Triggered when any dbus method call returns a dbus error
     * Error will then be mapped into the SLA error codes if possible, or set to unknown if not
     * @param errorData
     */
    void dbusError(QVariantMap errorData);

    /**
     * @brief exception Is normally propagated from DBus, but here it is used to propagate "service disconnect"
     * @param exception
     */
    void exception(const QMap<QString, QVariant> &exception);


    /**
     * @brief isServiceRegisteredChanged Is this service registered on DBus?
     */
    void isServiceRegisteredChanged();

    void propertiesValidChanged();

    void serviceIsOnDemandChanged();

    void emitExceptionOnUnregistrationChanged();

protected:

    template<typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8>
    void handleCallbackWithError(
            QDBusPendingReply<T1, T2, T3, T4, T5, T6, T7, T8> pending,
            QJSValue callback_ok = QJSValue(),
            QJSValue callback_fail = QJSValue()) {
        DBusUtils::handle_callbacks(m_engine, pending, callback_ok, callback_fail, [this](QString err_name, QString err_message){
            emit dbusError(DBusUtils::parseErrorData(err_name, err_message));
        });
    }

    virtual void reload_properties();


    /**
     * @brief set Allows intercepting calls to setProperty
     * It should be intercepted for any complex & user types to ensure correct typing.
     * @param name Property name
     * @param value Property value
     * @return If the property is defined in the class using Q_PROPERTY
     * then true is returned on success and false otherwise. If the property
     * is not defined using Q_PROPERTY, and therefore not listed in
     * the meta-object, it is added as a dynamic property and false is returned.
     */
    virtual bool set(const QString & name, const QVariant & value);
protected slots:
    /**
     * @brief handleServiceRegistrationChanged Handle setting the state and reloading properties
     *  when the service is registered / unregistered.
     */
    virtual void handleServiceRegistrationChanged();

    /**
     * @brief handlePropertiesChanged Handles the normal DBus PropertiesChanged signal
     *  from org.freedesktop.DBus.Properties proxy object
     * @param interface
     * @param changed_properties
     * @param invalidates_properties
     */
    virtual void handlePropertiesChanged(const QString &interface, const QMap<QString, QVariant> &changed_properties, const QStringList &invalidates_properties);

};

#endif // BASEDBUSOBJECT_H
