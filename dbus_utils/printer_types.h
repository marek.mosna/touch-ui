#ifndef __TYPES_H__
#define __TYPES_H__
#include <QMap>
#include <QString>
#include <QtCore>
#include <QDBusMetaType>
#ifdef BUILD_NETWORKING
// Need QMap<QString, QString> is already declared by NetworkManagerQt and declaring it
// again under different name is not possible -> load the type from NetworkManagerQt
#include <networkmanagerqt/generictypes.h>
#else
// Without networking, those types need to be declared here
typedef QMap<QString, QString> NMStringMap;
typedef QList<QVariantMap> NMVariantMapList;
typedef QMap<QString, QVariantMap> NMVariantMapMap;
#endif // BUILD_NETWORKING
typedef QMap<QString, QMap<QString, int> > MapStringMapStringInt;
typedef QMap<QString,double> MapStringDouble;
typedef QMap<QString,bool> MapStringBool;
typedef QMap<QString, int> MapStringInt;
typedef QMap<int, int> MapIntInt;
typedef QVector<QVector<int> > Int2D; // 2-D array of integers
typedef QMap<QString, QString> MapStringString;


Q_DECLARE_METATYPE(MapStringMapStringInt)
Q_DECLARE_METATYPE(MapStringDouble)
Q_DECLARE_METATYPE(MapStringBool)
Q_DECLARE_METATYPE(MapStringInt)
Q_DECLARE_METATYPE(MapIntInt)
Q_DECLARE_METATYPE(Int2D)




inline void registerPrinterTypes() {
    static bool registeredAlready = false;
    if(!registeredAlready) {
        registeredAlready = true;
        qDBusRegisterMetaType<MapStringMapStringInt>();
        qDBusRegisterMetaType<MapStringInt>();
        qDBusRegisterMetaType<MapStringDouble>();
        qDBusRegisterMetaType<NMStringMap>();
        qDBusRegisterMetaType<NMVariantMapList>();
        qDBusRegisterMetaType<MapIntInt>();
        qDBusRegisterMetaType<NMVariantMapMap>();
        qDBusRegisterMetaType<Int2D>();
        qDBusRegisterMetaType<MapStringString>();
        qDBusRegisterMetaType<MapStringBool>();
    }

}

#endif // __TYPES_H__

