#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include "dbusutils.h"
#include "errors_sl1.h"

DBusUtils::DBusUtils()
{

}

QStringList DBusUtils::listObjects(const QString &service, const QString &path)
{
    QStringList ret;
    auto connection = QDBusConnection::systemBus();

    // make a low-level call, to avoid introspecting the Introspectable interface
    QDBusMessage call = QDBusMessage::createMethodCall(service, path.isEmpty() ? QLatin1String("/") : path,
                                                       QLatin1String("org.freedesktop.DBus.Introspectable"),
                                                       QLatin1String("Introspect"));
    QDBusReply<QString> xml = connection.call(call);

    if (path.isEmpty()) {
        // top-level
        if (! xml.isValid()) {
            QDBusError err = xml.error();
            if (err.type() == QDBusError::ServiceUnknown) {
                qDebug() << "Service" << qPrintable(service) << " does not exist";
            }
            else {
                qDebug() << "Error: " <<  qPrintable(err.name())<< qPrintable(err.message());
            }
            return {};
        }
    }
    if(xml.isValid()) {
    }
    else {
        // this is not the first object, just fail silently
        return {};
    }

    QDomDocument doc;
    doc.setContent(xml);
    QDomElement node = doc.documentElement();
    QDomElement child = node.firstChildElement();
    while (!child.isNull()) {
        if (child.tagName() == QLatin1String("node")) {
            QString sub = path + QLatin1Char('/') + child.attribute(QLatin1String("name"));
            ret.append(sub);
            ret.append( listObjects(service, sub));
        }
        child = child.nextSiblingElement();
    }
    return ret;
}

void DBusUtils::Introspect(const QString &service, const QString &path, std::function<void(QString)> callback_ok, std::function<void()> callback_fail)
{
    auto connection = QDBusConnection::systemBus();

    // make a low-level call, to avoid introspecting the Introspectable interface
    QDBusMessage call = QDBusMessage::createMethodCall(service, path.isEmpty() ? QLatin1String("/") : path,
                                                       QLatin1String("org.freedesktop.DBus.Introspectable"),
                                                       QLatin1String("Introspect"));
    QDBusPendingReply<> pending = connection.asyncCall(call, 3000);
    handle_callbacks(pending,
        [&](QVariantList vl){
            if(vl.length() > 0) {
                callback_ok(vl.at(0).toString());
            }
            else callback_fail();
        },
        [&](QVariantList){
            callback_fail();
        }
    );
}

QVariantMap DBusUtils::parseErrorData(const QString name, const QString message)
{
    // Try parse exception data from JSON encoded in message string
    QJsonDocument document = QJsonDocument::fromJson(message.toUtf8());
    if(!document.isNull() && document.isObject()) {
        // Document is parsed correctly (!isNull) and represents an object
        QJsonObject object = document.object();
        if(object.contains("code")) {
            // Data contains at last an error code
            return object.toVariantMap();
        }
    }

    // Fallback - parse code from error name
    ErrorsSL1::Errors error = ErrorsSL1::static_parseFromDBus(name);
    return QVariantMap({{"code", error}, {"name", name}, {"text", message}});
}


