/*
    Copyright 2022, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QDBusPendingReply>
#include <QDebug>
#include <errors_sl1.h>
#include <dbusutils.h>
#include "basedbusobject.h"

QSharedPointer<QDBusServiceWatcher> BaseDBusObject::watcherInstance(const QString &service, const QDBusConnection &connection) {
    static QSharedPointer<QDBusServiceWatcher> instance;
    if(instance) {
        instance->addWatchedService(service);
    }
    else {
        instance.reset(new QDBusServiceWatcher(service, connection));
        instance->setObjectName("shared_BaseDBusObject_watcher_instance");
    }
    return instance;
}

BaseDBusObject::BaseDBusObject(QJSEngine & engine, const QString & service, const QString & interface, const QString & path,  const QDBusConnection & connection, QObject *parent)
    : QObject(parent),
      m_watcher(watcherInstance(service, connection)),
      m_connection(connection),
      m_properties(service, path, connection),
      m_engine(engine),
      m_service(service),
      m_interface(interface),
      m_path(path),
      m_isServiceRegistered(connection.interface()->isServiceRegistered(service))
{
    m_properties.setTimeout(5000);

    // Prepare the retry timer
    m_loadPropertiesRepeatedAttempt.setInterval(500);
    m_loadPropertiesRepeatedAttempt.setSingleShot(true);
    connect(&m_loadPropertiesRepeatedAttempt, &QTimer::timeout, this, [this](){
        this->reload_properties();
    });

    connect(m_watcher.data(), &QDBusServiceWatcher::serviceOwnerChanged, this, [this, service](const QString & service_name, const QString & oldOwner, const QString newOwner){
        Q_UNUSED(oldOwner)
        if(service_name == this->m_service) {
            // In general, we don't care about actual owner, only that the service is registered
            this->setIsServiceRegistered(newOwner != "");
        }
    });

    connect(this, &BaseDBusObject::isServiceRegisteredChanged, this, [this](){
        if(! this->serviceIsOnDemand()) {
            if(this->isServiceRegistered()) {
                qInfo() << "DBus Service" << this->m_service << "registered!";
            }
            else {
                // For normal services, unregistering usually means a problem - mark properties invalid
                qWarning() << "DBus Service" << this->m_service << "just went dead!";
            }
        }
    });

    connect(this, &BaseDBusObject::isServiceRegisteredChanged, this, &BaseDBusObject::handleServiceRegistrationChanged);

    connect(&m_properties, &PropertiesProxy::PropertiesChanged, this, &BaseDBusObject::handlePropertiesChanged);


    // Changed signal won't be emitted if service is registered already.
    if(m_isServiceRegistered) {
        handleServiceRegistrationChanged();
    }
    // On-demand services are always ready, try even if it's not registered.
    else if(serviceIsOnDemand()) {
        reload_properties();
    }
}

bool BaseDBusObject::isServiceRegistered() const
{
    return m_isServiceRegistered;
}

void BaseDBusObject::reload_properties()
{
    if(propertiesValid() && ! serviceIsOnDemand())  return;
    if( ! serviceIsOnDemand()) qDebug() << m_service << "Reloading properties";
    QDBusPendingReply<QMap<QString, QVariant> > pendingReply = m_properties.GetAll(this->m_interface);
    QDBusPendingCallWatcher * watcher = new QDBusPendingCallWatcher(pendingReply, this);

    connect(watcher, &QDBusPendingCallWatcher::finished, this, [&](QDBusPendingCallWatcher * replyWatcher) mutable {
        QDBusPendingReply<QMap<QString, QVariant> > reply = *replyWatcher;
        if(reply.isError()) {
            qDebug() << m_service << ".GetAll error -> retry in " << m_loadPropertiesRepeatedAttempt.interval() << "ms";
            m_loadPropertiesRepeatedAttempt.start();
        }
        else {
            const auto & theMap = reply.value();
            for(auto [key, value] : asKeyValueRange(theMap)) {
                bool r = this->set(key, value);
                if( ! r) {
                    qDebug() << m_service << "Unknown property" << key << "or invalid value" <<value;
                }
            }

            this->setPropertiesValid(true);
        }
        replyWatcher->deleteLater();
    });
}

bool BaseDBusObject::set(const QString &name, const QVariant &value) {
    return setProperty(name.toStdString().c_str(), value);
}

void BaseDBusObject::handleServiceRegistrationChanged()
{
    // Ingnore DBus registration events if service is set as on-demand.
    // reload_properties must be called by the derived class when appropriate.
    if(serviceIsOnDemand()) {
        return;
    }
    else if(this->isServiceRegistered()) {
        this->reload_properties();
    }
    else { // Unregistered
        setPropertiesValid(false);
        if(emitExceptionOnUnregistration()) {
            emit exception(QVariantMap{
                               {"code",QVariant(ErrorsSL1::static_toStringCode(ErrorsSL1::SYSTEM_SERVICE_CRASHED))},
                               {"name", QVariant(ErrorsSL1::static_toString(ErrorsSL1::SYSTEM_SERVICE_CRASHED))},
                               {"service", QVariant(m_service)}
                           });
        }
    }
}

void BaseDBusObject::handlePropertiesChanged(const QString &interface, const QMap<QString, QVariant> &changed_properties, const QStringList &invalidates_properties)
{
    for(auto [key, value] : asKeyValueRange(changed_properties)) {
        bool r = this->set(key, value);
        if( ! r) {
            qDebug() << interface << "Unknown property" << key << "or invalid value" <<value;
        }
    }

    if( ! invalidates_properties.isEmpty()) {
        qWarning() << interface << "Is trying to invalidate properties. That is not supported: " << invalidates_properties;
    }
}

void BaseDBusObject::setIsServiceRegistered(bool newServiceReady)
{
    if (m_isServiceRegistered == newServiceReady)
        return;
    m_isServiceRegistered = newServiceReady;
    emit isServiceRegisteredChanged();
}

bool BaseDBusObject::propertiesValid() const
{
    return m_propertiesLoaded;
}

void BaseDBusObject::setPropertiesValid(bool newPropertiesValid)
{
    if (m_propertiesLoaded == newPropertiesValid)
        return;
    m_propertiesLoaded = newPropertiesValid;
    emit propertiesValidChanged();
}

bool BaseDBusObject::serviceIsOnDemand() const
{
    return m_serviceIsOnDemand;
}

void BaseDBusObject::setServiceIsOnDemand(bool newFakeServiceRegistered)
{
    if (m_serviceIsOnDemand == newFakeServiceRegistered)
        return;
    m_serviceIsOnDemand = newFakeServiceRegistered;
    emit serviceIsOnDemandChanged();
}

bool BaseDBusObject::emitExceptionOnUnregistration() const
{
    return m_emitExceptionOnUnregistration;
}

void BaseDBusObject::setEmitExceptionOnUnregistration(bool newEmitExceptionOnUnregistration)
{
    if (m_emitExceptionOnUnregistration == newEmitExceptionOnUnregistration)
        return;
    m_emitExceptionOnUnregistration = newEmitExceptionOnUnregistration;
    emit emitExceptionOnUnregistrationChanged();
}
