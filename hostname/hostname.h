/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef HOSTNAME_H
#define HOSTNAME_H

#include <QObject>
#include <QTimer>
#include <QJSEngine>
#include <QJSValue>
#include <hostname_proxy.h>
#include <basedbusobject.h>

/** QML-facing object implementing org.freedesktop.hostname1 interface
Implements a useful subset of methods and properties.
*/
class Hostname : public BaseDBusObject
{
    Q_OBJECT

    HostnameProxy m_hostnameProxy;
//    bool m_propertiesLoaded;
//    QJSEngine & m_engine;
//    QTimer m_reloadTimer;
    QString m_location;
    QString m_kernelRelease;
    QString m_kernelName;
    QString m_hostname;
    QString m_deployment;
    QString m_kernelVersion;
    QString m_staticHostname;
    QString m_operatingSystemPrettyName;
    QString m_homeURL;

    /// Protected setter
    void _setLocation(QString location);
    void _setKernelRelease(QString kernelRelease);
    void _setKernelName(QString kernelName);
    void _setHostname(QString hostname);
    void _setDeployment(QString deployment);
    void _setKernelVersion(QString kernelVersion);
    void _setStaticHostname(QString staticHostname);
    void _setOperatingSystemPrettyName(QString operatingSystemPrettyName);
    void _setHomeURL(QString homeURL);
    void _setDefaultHostname(QString defaultHostname);
    void _setHardwareVendor(QString hardwareVendor);
    void _setHardwareModel(QString hardwareModel);
    void _setHostnameSource(QString hostnameSource);

    virtual bool set(const QString & key, const QVariant & value) override;
public:
    inline static const QString service{"org.freedesktop.hostname1"};
    inline static const QString interface{"org.freedesktop.hostname1"};
    inline static const QString path{"/org/freedesktop/hostname1"};

    explicit Hostname(QJSEngine & _engine, QObject *parent = nullptr);
    Q_PROPERTY(QString deployment READ deployment NOTIFY deploymentChanged)
    Q_PROPERTY(QString hostname READ hostname NOTIFY hostnameChanged)
    Q_PROPERTY(QString kernelName READ kernelName  NOTIFY kernelNameChanged)
    Q_PROPERTY(QString kernelRelease READ kernelRelease  NOTIFY kernelReleaseChanged)
    Q_PROPERTY(QString kernelVersion READ kernelVersion  NOTIFY kernelVersionChanged)
    Q_PROPERTY(QString location READ location NOTIFY locationChanged)
    Q_PROPERTY(QString staticHostname READ staticHostname NOTIFY staticHostnameChanged)
    Q_PROPERTY(QString operatingSystemPrettyName READ operatingSystemPrettyName NOTIFY operatingSystemPrettyNameChanged)
    Q_PROPERTY(QString homeURL READ homeURL NOTIFY homeURLChanged)
    Q_PROPERTY(QString defaultHostname READ defaultHostname NOTIFY defaultHostnameChanged)
    Q_PROPERTY(QString hardwareVendor READ hardwareVendor WRITE _setHardwareVendor NOTIFY hardwareVendorChanged)
    Q_PROPERTY(QString hardwareModel READ hardwareModel WRITE _setHardwareModel NOTIFY hardwareModelChanged)
    Q_PROPERTY(QString hostnameSource READ hostnameSource WRITE _setHostnameSource NOTIFY hostnameSourceChanged)

    QString location() const
    {
        return m_location;
    }

    QString kernelRelease() const
    {
        return m_kernelRelease;
    }

    QString kernelName() const
    {
        return m_kernelName;
    }

    QString hostname() const
    {
        return m_hostname;
    }

    QString deployment() const
    {
        return m_deployment;
    }

    QString kernelVersion() const
    {
        return m_kernelVersion;
    }


    QString staticHostname() const
    {
        return m_staticHostname;
    }

    QString operatingSystemPrettyName() const
    {
        return m_operatingSystemPrettyName;
    }

    QString homeURL() const
    {
        return m_homeURL;
    }


    /** Set the staticHostname
     *
     * Call over DBus will run asynchronously and call the appropriate callback when done.
     * @param staticHostname
     * @param callback_ok JS function()
     * @param callback_fail JS function(string name_of_error)
    **/
    Q_INVOKABLE void setStaticHostname(QString staticHostname, QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    /** Set the hostname
     *
     * Call over DBus will run asynchronously and call the appropriate callback when done.
     * @param staticHostname
     * @param callback_ok JS function()
     * @param callback_fail JS function(string name_of_error)
    **/
    Q_INVOKABLE void setHostname(QString hostname, QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    /** Set the location of the printer
     *
     * Call over DBus will run asynchronously and call the appropriate callback when done.
     * @param staticHostname
     * @param callback_ok JS function()
     * @param callback_fail JS function(string name_of_error)
    **/
    Q_INVOKABLE void setLocation(QString location, QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    /** Set the deployment
     *
     * Call over DBus will run asynchronously and call the appropriate callback when done.
     * @param staticHostname
     * @param callback_ok JS function()
     * @param callback_fail JS function(string name_of_error)
    **/
    Q_INVOKABLE void setDeployment(QString deployment, QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    /** Returns a JSON representation of all properties in one.
     * @returns JSON object describing the properties
     */
    Q_INVOKABLE void describe(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());


    QString defaultHostname() const
    {
        return m_defaultHostname;
    }

    QString hardwareVendor() const
    {
        return m_hardwareVendor;
    }

    QString hardwareModel() const
    {
        return m_hardwareModel;
    }

    QString hostnameSource() const
    {
        return m_hostnameSource;
    }

signals:

    void locationChanged(QString location);

    void kernelReleaseChanged(QString kernelRelease);

    void kernelNameChanged(QString kernelName);

    void hostnameChanged(QString hostname);

    void deploymentChanged(QString deployment);

    void kernelVersionChanged(QString kernelVersion);

    void staticHostnameChanged(QString staticHostname);

    void operatingSystemPrettyNameChanged(QString operatingSystemPrettyName);

    void homeURLChanged(QString homeURL);

    void defaultHostnameChanged(QString defaultHostname);

    void hardwareVendorChanged(QString hardwareVendor);

    void hardwareModelChanged(QString hardwareModel);

    void hostnameSourceChanged(QString hostnameSource);

public slots:


private:
    QString m_defaultHostname;
    QString m_hardwareVendor;
    QString m_hardwareModel;
    QString m_hostnameSource;
};

#endif // HOSTNAME_H
