
/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtQml.Models 2.2
import Native 1.0
import PrusaComponents 1.0
/*
 Depends on:
    StackView view

*/
PrusaPage {
    implicitWidth: 800
    implicitHeight: 480
    id: root
    title: qsTr("Ethernet")
    name: "EthernetSettings"
    pictogram: Theme.pictogram.ethernet
    property bool wait: false
    property int rightColWidth: 400

    StackView.onActivated: () => {
        ethernet.refreshScratchpad()
        localScratchpad.reload()
    }
    StackView.onDeactivated: () => {
        // Remove text focus on leaving
        inpIP.txtValue.focus = false
        inpDns.txtValue.focus = false
        inpGateway.txtValue.focus = false
        inpMask.txtValue.focus = false
    }

    // Reload text fields if carrier changes
    property bool connection: ethernet.activeConnectionState
    onConnectionChanged: () => {
        if(ethernet.activeConnectionState === Ethernet.Activated && view.currentItem === root)
            localScratchpad.reload()
    }

    Item {
        id: localScratchpad
        property bool dhcp
        property string ip
        property string gateway
        property string dns
        property string mask
        function reload() {
            dhcp = ethernet.scratchpadDhcp
            ip = ethernet.scratchpadIp
            gateway = ethernet.scratchpadGateway
            dns = ethernet.scratchpadDns
            mask = ethernet.scratchpadMask
            console.debug("Reloaded scratchpad, dhcp: " + dhcp + " ip: " + ip + " gateway: " + gateway + " dns: " + dns + " mask: " + mask)
        }
        function apply() {
            console.debug("Applying scratchpad, dhcp: " + dhcp + " ip: " + ip + " gateway: " + gateway + " dns: " + dns + " mask: " + mask)
            ethernet.scratchpadDhcp = dhcp
            ethernet.scratchpadIp = ip == "" ? ethernet.defaultStaticIpAddress : ip
            ethernet.scratchpadGateway = gateway
            ethernet.scratchpadDns = dns
            ethernet.scratchpadMask = mask === "" ? ethernet.defaultStaticNetmask : mask
            ethernet.applyScratchpad(
                        function(){console.log("Connection re-configuration succeeded"); root.wait = false},
                        function(e){console.log("Connection re-configuration failed: " + e); root.wait = false}
                        )
        }

        Connections {
            target: ethernet
            function onScratchPadChanged(){
                localScratchpad.reload()
            }
        }
    }

    ObjectModel {
        id: ethernetSettingControls
        PrusaPicturePictureButtonItem {
            label: "Ethernet 0" //ethernet.activeConnectionName
            pictogram: Theme.pictogram.ethernet
            connected: ethernet.carrier
        }

        Item {
            height: 20
            width: 760
        }
        Column {
            visible: {
                if (ethernet.carrier)
                    return true
                return false
            }
            Text {
                font.pixelSize: 28
                font.weight: Font.Medium
                text: qsTr("Network Info")
            }

            PrusaSwitchItem {
                id: dhcpSwitch
                name: "EthernetDHCP"
                label: qsTr("DHCP")
                checked: localScratchpad.dhcp
                clickSwitch: false
                onClicked:  () => {
                    localScratchpad.dhcp = ! localScratchpad.dhcp
                    waitOverlay.text = qsTr("Configuring the connection,") + "\n" + qsTr("please wait...")
                    root.wait = true
                    localScratchpad.apply()
                }
            }

            PrusaTextInput {
                id: inpIP
                label: qsTr("IP Address")
                value: localScratchpad.ip
                enabled: ! dhcpSwitch.checked
                regexp: "^(([01]?[0-9]?[0-9]|2([0-4][0-9]|5[0-5]))\.){3}([01]?[0-9]?[0-9]|2([0-4][0-9]|5[0-5]))$"
                txtValue.inputMethodHints: Qt.ImhPreferNumbers
                onAccepted: () => {
                    localScratchpad.ip = value
                }
            }

            PrusaTextInput {
                id: inpGateway
                label: qsTr("Gateway", "default gateway address")
                value: localScratchpad.gateway
                enabled: ! dhcpSwitch.checked
                regexp: "^(([01]?[0-9]?[0-9]|2([0-4][0-9]|5[0-5]))\.){3}([01]?[0-9]?[0-9]|2([0-4][0-9]|5[0-5]))$|$"
                txtValue.inputMethodHints: Qt.ImhPreferNumbers
                onAccepted: () => {
                    localScratchpad.gateway = value
                }
            }

            PrusaTextInput {
                id: inpMask
                label: "Mask"
                value: localScratchpad.mask
                enabled: ! dhcpSwitch.checked
                regexp: "^(([01]?[0-9]?[0-9]|2([0-4][0-9]|5[0-5]))\.){3}([01]?[0-9]?[0-9]|2([0-4][0-9]|5[0-5]))$"
                txtValue.inputMethodHints: Qt.ImhPreferNumbers
                onAccepted: () => {
                    localScratchpad.mask = value
                }
            }

            PrusaTextInput {
                id: inpDns
                label: "DNS"
                value: localScratchpad.dns
                enabled: ! dhcpSwitch.checked
                regexp: "^(([01]?[0-9]?[0-9]|2([0-4][0-9]|5[0-5]))\.){3}([01]?[0-9]?[0-9]|2([0-4][0-9]|5[0-5]))$|$"
                txtValue.inputMethodHints: Qt.ImhPreferNumbers
                onAccepted: () => {
                    localScratchpad.dns = value
                }
            }
            PrusaTextInput {
                id: inpMac
                label: "MAC"
                value: ethernet.hwAddress
                enabled: false
            }

            RowLayout {
                height: 260
                anchors {
                    rightMargin: 20
                    leftMargin: 20
                }
                spacing: 30

                Item {
                    height: 10
                    Layout.fillWidth: true
                }
                PictureButton {
                    id: btnApply
                    text: qsTr("Apply")
                    backgroundColor: "green"
                    pictogram: Theme.pictogram.yes
                    enabled: inpDns.acceptableInput &&
                             inpGateway.acceptableInput &&
                             inpMask.acceptableInput &&
                             inpIP.acceptableInput || localScratchpad.dhcp


                    onClicked: () => {
                        waitOverlay.text = qsTr("Configuring the connection,") + "\n" + qsTr("please wait...")
                        root.wait = true
                        localScratchpad.apply()
                    }
                }
                PictureButton {
                    id: btnRevert
                    text: qsTr("Revert", "Turn back the changes and go back to the previous configuration.")
                    pictogram: Theme.pictogram.no
                    onClicked: () => {
                        localScratchpad.reload()
                    }
                }
            }
        }
    }

    ListView {
        height: 420
        interactive: true
        anchors {
          bottom: parent.bottom
          left: parent.left
          right: parent.right
          rightMargin: 10
          leftMargin: 10
        }
        clip: true
        model: ethernetSettingControls

        ScrollBar.vertical: ScrollBar {
            active: true
            policy: ScrollBar.AlwaysOn
        }
    }
    PrusaWaitOverlay {
        id: waitOverlay
        doWait: root.wait
    }
}
