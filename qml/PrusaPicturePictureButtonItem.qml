import QtQuick 2.12
import QtQuick.Controls 2.5

import PrusaComponents 1.0

/* Simple item for puting directly into columns */
Item {
    id: root
    property string label
    property bool enabled: false
    property string pictogram

    property bool connected: false
    property alias connectedText: txtConnected.text
    signal clicked()

    width: 760
    height: 100
    Item {
        id: img
        width: 70
        height: 70
        anchors {
            left: parent.left
            verticalCenter:  parent.verticalCenter
            leftMargin: 10
        }
        Image {
            anchors {
                centerIn: parent
                margins: 15
            }
            height: parent.height - 30


            sourceSize.width: parent.width
            sourceSize.height: parent.height
            fillMode: Image.PreserveAspectFit
            source: Theme.pictogram.path + "/" + root.pictogram
        }
    }

    Column {
        anchors {
            left: img.right
            leftMargin: 20
            verticalCenter: parent.verticalCenter
        }
        spacing: 5
        Text {
            id: txtLabel

            font.pixelSize: 28
            text:  label === undefined ? "<No label>" : label
        }
        Text {
            id: txtConnected
            text: root.connected ? qsTr("Plugged in") : qsTr("Unplugged")
            color: "grey"
        }
    }

    HorizontalSeparator {
        width: root.width
        anchors.bottom: root.bottom
    }
}
