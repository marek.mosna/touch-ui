
/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import Native 1.0
import ErrorsSL1 1.0
import Notification 1.0
import Qt.labs.qmlmodels 1.0
import cz.prusa3d.sl1.printer0 1.0
import PrusaComponents 1.0
import "utils.js" as Utils
/*
 Depends on:
    StackView view
*/

Page {
    id: root
    title: qsTr("Notifications")
    height: notificationList.height
    background: null
    signal notificationClicked()
    Connections {
        target: notificationModel
        function onNewNotification() {
            if(notificationModel.count > 4) notificationModel.removeRows(4, notificationModel.count - 4)
        }
    }


    ListView {
        id: notificationList
        interactive: false
        width: parent.width
        height: (notificationModel.count + 1)*80
        model: notificationModel
        delegate: DelegateChooser {
            role: "type"
            DelegateChoice {
                roleValue: Notification.UpdateNotification
                delegate: NotificationNestedDelegate {
                    pictogram: Theme.pictogram.updateInverse
                    text: qsTr("Available update to %1").arg(model.object.nextVersion)
                    onClicked: () => {
                        printer0.beep_button()
                        if(view.currentItem.Name !== "releasenotes") view.push("PageReleaseNotes.qml")
                        root.notificationClicked()

                    }
                }
            }
            DelegateChoice {
                roleValue: Notification.FinishedPrintJob
                delegate: NotificationNestedDelegate {
                    pictogram: statusIcon()
                    text: statusText()

                    function statusText() {
                        var exposure = printer0.getExposureObject(model.object.exposurePath)
                        switch(exposure.state) {
                            case Exposure.FAILURE: return qsTr("Print failed: %1").arg(model.object.projectName);
                            case Exposure.CANCELED: return qsTr("Print canceled: %1").arg(model.object.projectName);
                            default: return qsTr("Print finished: %1").arg(model.object.projectName);
                        }
                    }

                    function statusIcon() {
                        var exposure = printer0.getExposureObject(model.object.exposurePath)
                        switch(exposure.state) {
                            case Exposure.FAILURE: return Theme.pictogram.errorInverse
                            case Exposure.CANCELED: return Theme.pictogram.cancelInverse
                            default: return Theme.pictogram.finishInverse
                        }
                    }

                    // For notification with high enough severity, show them immediately.
                    // This should mainly apply to prints that finished before the printer was automatically turned-off.
                    // Those pages will have the "back" button disabled.
                    Component.onCompleted: {
                        if(model.severity >= 100) {
                            push_notification_page(printer0.getCurrentExposureObject().state === Exposure.CANCELED)
                        }
                    }

                    onClicked: () => {
                        console.log(LoggingCategories.trajectory, "Notification \"" + statusText() + "\" clicked")
                        printer0.beep_button()
                        if(false === (view.currentItem && view.currentItem.name === "PrintFinished"
                                && view.currentItem.exposure_path === model.object.exposurePath))
                        {
                            push_notification_page(true)
                        }
                        root.notificationClicked()
                    }

                    function push_notification_page(back_enabled = false) {
                        var parameter_list = {
                            exposure: printer0.getExposureObject(model.object.exposurePath),
                            back_button_enabled: back_enabled
                        }
                        if(statePages.idle.view.currentItem instanceof PageFinished ) {
                            statePages.idle.view.replace("PageFinished.qml", parameter_list)
                        }
                        else statePages.idle.view.push("PageFinished.qml", parameter_list)
                    }
                }
            }
            DelegateChoice {
                roleValue: Notification.FileUpload;
                delegate: NotificationProgressDelegate {
                    pictogram: Theme.pictogram.upInverse
                    text: model.object.filename
                    progress: model.object.progress
                    done: model.object.done
                    errorCode: model.object.errorCode
                    onActivated: () => {
                                     console.log(LoggingCategories.trajectory, "Notification \"" + text + "\" clicked")
                                     printer0.print(
                                         model.object.path,
                                         false,
                                         ()=>{root.notificationClicked()},
                                         (e)=>{
                                             console.log("print error: ", e)
                                             push_error({code: ErrorsSL1.toStringCode(ErrorsSL1.parseFromDBus(e))})
                                         }
                                         )
                                 }
                }
            }

            DelegateChoice {
                roleValue: Notification.FileDownload;
                delegate: NotificationProgressDelegate {
                    pictogram: Theme.pictogram.downInverse
                    text: model.object.filename
                    progress: model.object.progress
                    done: model.object.done
                    errorCode: model.object.errorCode
                    onActivated: () => {
                                     console.log(LoggingCategories.trajectory, "Notification \"" + text + "\" clicked")
                                     printer0.print(
                                         model.object.path,
                                         false,
                                         ()=>{root.notificationClicked()},
                                         (e)=>{
                                             console.log("print error: ", e)
                                             push_error({code: ErrorsSL1.toStringCode(ErrorsSL1.parseFromDBus(e))})
                                         }
                                         )
                                 }
                }
            }
        }


        ScrollBar.vertical: ScrollBar {
            active: true
            policy: ScrollBar.AsNeeded
        }

        remove: Transition {
            NumberAnimation { property: "opacity"; from: 1.0; to: 0; duration: 400 }
            NumberAnimation { property: "x";  to: -600; duration: 400 }
        }

        displaced: Transition {
            NumberAnimation { properties: "x,y"; duration: 400; easing.type: Easing.InSine }
            NumberAnimation { property: "opacity"; to: 1.0 }
        }

        add: Transition {
            NumberAnimation { property: "opacity"; from: 0.0; to: 1.0; duration: 400 }
            NumberAnimation { properties: "x"; duration: 400; easing.type: Easing.InSine }
        }

    }
}
