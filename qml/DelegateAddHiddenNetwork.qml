/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5

import PrusaComponents 1.0

Item {
    id: root
    height: visible ? 80 : 0
    visible: (! wifiNetworkModel.isHotspotRunning) && wifiNetworkModel.wifiEnabled
    width: 760

    Rectangle { color: "black"; anchors.fill: parent }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        z: -1

        onClicked: ()=>{
            printer0.beep_button()
            view.push("PageAddWifiNetwork.qml")
        }
    }

    Column {
        id: mainColumn
        spacing: 5
        RowLayout {
            Rectangle {
                id: mainArea
                color: "black"
                height: 70
                width: 680
                Rectangle {
                    id: img
                    color: "black"
                    anchors {
                        left: parent.left
                        verticalCenter: parent.verticalCenter
                        leftMargin: 20
                        margins: 10
                    }
                    width: parent.height - 10
                    height: parent.height - 10
                    Image {
                        anchors {
                            fill: parent
                            margins: 2
                        }
                        sourceSize.width: parent.height
                        sourceSize.height: parent.height
                        fillMode: Image.PreserveAspectFit
                        source: Theme.pictogram.path + "/" + Theme.pictogram.justPlus
                    }
                }
                Column {
                    spacing: 5
                    anchors {
                        verticalCenter: parent.verticalCenter
                        left: img.right
                        leftMargin: 50
                        right: parent.right
                        rightMargin: 10
                    }

                    RowLayout {
                        height: 26
                        width: parent.width
                        Text {
                            text: qsTr("Add Hidden Network")
                        }
                        Text {
                            text: " "
                            color: "grey"
                        }
                        Item {
                            Layout.fillWidth: true
                        }
                    }
                    RowLayout {
                        height: 26
                    }
                }

            }
        }

        HorizontalSeparator {
            width: root.width
        }
    }
}
