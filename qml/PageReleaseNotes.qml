import QtQuick 2.15
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import "3rdparty/qrcode-generator"
import cz.prusa3d.sl1.printer0 1.0
import PrusaComponents 1.0

PrusaPage {
    title: qsTr("Release Notes")
    name: "releasenotes"

    StackView.onActivating: ()=>{
       updater.getReleaseNotes()
    }

    ScrollView {
        anchors.fill: parent
        contentWidth: width
        contentHeight: updateNotificationColumn.height
        anchors.margins: 10
        clip: true
        ScrollBar.vertical.policy: ScrollBar.AlwaysOn
        ScrollBar.vertical.interactive: true

        Column {
            id: updateNotificationColumn
            width: 740
            x: 30
            anchors.margins: 10
            Rectangle {
                id: textContainer
                width: parent.width
                height: notificationText.height
                color: "black"
                Text {
                    id: notificationText
                    width: parent.width
                    // Only one available SW update at a time is expected,
                    // thus the release notes are pulled from the updater
                    // directly so as to fix the actualization anomaly
                    // after system startup(no release notes for and update).
                    text: updater.releaseNotes
                    textFormat: Text.MarkdownText

                    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                }
            }

            Item {
                id: updateNotificationQrcode
                width: 740
                height: 400
                property string url: "github.com/prusa3d/Prusa-Firmware-SL1/releases"
                Rectangle {
                    y: 30
                    anchors.horizontalCenter: parent.horizontalCenter
                    id: qrcodeBackground
                    color: "white"
                    width: 320
                    height: 320
                    QRCode {
                        anchors {
                            verticalCenterOffset: 2
                            horizontalCenterOffset: 2
                            centerIn: parent
                        }
                        id: qrcode
                        value : updateNotificationQrcode.url
                        width: 300
                        height: 300
                        level: "Q"
                    }
                }
                Item {
                    anchors {
                        horizontalCenter: qrcodeBackground.horizontalCenter
                        top: qrcodeBackground.bottom
                        margins: 20
                    }
                    height: 30
                    width: 740
                    Text {
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        font.pixelSize: 24
                        text: updateNotificationQrcode.url
                        anchors.centerIn: parent
                    }
                }
            }

            Item {
                height: 10
                width: 1
            }
            RowLayout {
                id: updateButtonsLayout
                width: parent.width
                spacing: 20

                Item {
                    height: btnInstall.height
                    Layout.fillWidth: true
                }
                PictureButton {
                    id:btnInstallLater
                    name: "InstallLater"
                    onClicked: ()=>{
                        view.pop()
                    }
                    pictogram: Theme.pictogram.clock
                    text: qsTr("Later")
                }

                PictureButton {
                    id:btnInstall
                    name: "installUpdate"
                    enabled: printer0.state === Printer0.IDLE || printer0.state === Printer0.ADMIN ||  printer0.state === Printer0.INITIALIZING
                    onClicked: () =>  run_update({initiateDownload: true})
                    pictogram: Theme.pictogram.yes
                    text: qsTr("Install Now")
                    backgroundColor: "green"
                }
            }
            Item {
                // Keep some additional space
                height: 80
                width: 1
            }
        }
    }
}
