/*
    Copyright 2019-2020, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtQml.Models 2.11
import PrusaComponents 1.0
import PrusaComponents.Delegates 1.0

PageVerticalList {
    id: root
    title: qsTr("Language & Time")
    name: "settingsLanguageTime"
    pictogram: Theme.pictogram.language
    model: ObjectModel {
        DelegateRef {
            name:"setlanguage"
            text: qsTr("Set Language")
            pictogram: Theme.pictogram.language
            onClicked: ()=>{ root.StackView.view.push("PageLanguage.qml") }
        }
        DelegateRef {
            name:"timesettings"
            text: qsTr("Time Settings")
            pictogram: Theme.pictogram.clock
            onClicked: ()=>{
                root.StackView.view.push("PageTimeSettings.qml")
            }
        }
    }
}
