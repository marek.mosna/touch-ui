
/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import PrusaComponents 1.0

PrusaPage { 
    id: root
    title: qsTr("Prusa Connect")
    name: "SetPrusaConnect"
    pictogram: Theme.pictogram.settingsSimple
    property bool is_registered: connect0.is_registered;
    property bool do_register: false;
    property string temp_token: "";

    Timer {
        interval: 1000
        running: root.do_register
        repeat: true
        onTriggered: () => {
            connect0.confirm_registration(root.temp_token,
                function (is_registered){
                    if (is_registered){
                        root.do_register = false;
                        root.is_registered = connect0.is_registered;
                        root.temp_token = "";
                        view.pop();
                    }
                },
                function (error){
                    console.log("ERROR: Prusa connect " + JSON.stringify(error));
                    Qt.createComponent("qrc:/ErrorPopup.qml").createObject(window, {text: qsTr("Failed to establish a new connection.\nPlease try later.")}).open();
                    root.do_register = false;
                    root.is_registered = connect0.is_registered;
                    root.temp_token = "";
                    view.pop();
                }
            )
        }
    }
        


    RowLayout {
        id: layout
        anchors.fill: parent
        spacing: 6

        Text {
            Layout.alignment: Qt.AlignVCenter
            Layout.leftMargin: 50
            text: root.is_registered ? qsTr("It's already registered!") : qsTr("Start Your Registration!")
            font.pixelSize: 44
            wrapMode: Text.Wrap
        }


        PictureButton {
            id: btnconfirmScreenContinue
            name: "RestoreOrContinue"
            text: root.is_registered ? qsTr("Restore") : qsTr("Continue")
            pictogram: root.is_registered ? Theme.pictogram.cancel : Theme.pictogram.yes
            backgroundColor: root.is_registered ? "#5B5B5B" : "green"
            Layout.alignment: Qt.AlignVCenter
            onClicked: () => {
                if (root.is_registered){
                    connect0.reset_connect(
                        function (){
                            root.do_register = false;
                            root.is_registere = connect0.is_registered;
                            root.temp_token = "";
                        },
                        function (error){
                            console.log("ERROR: Prusa connect " + JSON.stringify(error));
                            Qt.createComponent("qrc:/ErrorPopup.qml").createObject(window, {text: qsTr("Failed to establish a new connection.\nPlease try later.")}).open();
                        }
                    )
                }else{
                    connect0.register_printer(
                        function (temp_token){
                            root.do_register = true;
                            root.temp_token = temp_token;
                            view.push("PageShowToken.qml",
                                {
                                    token: temp_token,
                                    onClick: function() {
                                        root.do_register = false;
                                        root.is_registere = connect0.is_registered;
                                        root.temp_token = "";
                                    }
                                }
                            );
                        },
                        function (error){
                            console.log("ERROR: Prusa connect " + JSON.stringify(error));
                            Qt.createComponent("qrc:/ErrorPopup.qml").createObject(window, {text: qsTr("Failed to establish a new connection.\nPlease try later.")}).open();
                            root.do_register = false;
                            root.is_registere = connect0.is_registered;
                            root.temp_token = "";
                        }
                    )
                }

            }
        }
    }

    ImageVersionText {}
}
