import QtQuick 2.12
import QtQuick.Controls 2.5
import PrusaComponents 1.0

Item {
    id: root
    width: PathView.view.width
    height: 50
    opacity: PathView.iconOpacity !== undefined ? PathView.iconOpacity : 0
    scale: PathView.iconScale !== undefined ? PathView.iconScale : 1
    property var view: PathView.view
    property alias color: txt.color
    property alias text: txt.text
    property alias font: txt.font
    Text {
        id: txt
        color: "white"
        anchors.centerIn: parent
        text: parent.text
        font.pixelSize: Theme.font.big
        font.family: "DejaVu Sans Mono"
    }

    MouseArea {
        anchors.fill: parent
        onClicked: ()=>{
            root.view.currentIndex = index
        }
    }
}
