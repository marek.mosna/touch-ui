/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import PrusaComponents 1.0
import "3rdparty/qrcode-generator"

Item {
    id: root
    width: 760
    height: visible ? 480 : 0
    property string hotspot_psk: ""
    property string hotspot_ssid: ""
    Component.onCompleted: ()=>{
        wifiNetworkModel.getPSKbyName("hotspot", function(password){hotspot_psk=password}, function(e){});
        hotspot_ssid = wifiNetworkModel.getLocalHotspotSSID()
    }
    property  string text: {
        if(model.network === "ethernet") {
            return "http://maker:" + printer0.api_key + "@" + ethernet.ipAddress
        }
        else {
            if(wifiNetworkModel.isHotspotRunning) {
                return "WIFI:T:WPA;S:" + hotspot_ssid + ";P:"+ hotspot_psk + ";;"
            }else {
                return "http://maker:" + printer0.api_key + "@" + wifiNetworkModel.ipAddress
            }
        }
    }
    visible: {
        if(model.network === "ethernet") {
            return  ethernet.carrier && ethernet.ipAddress !== ""
        }
        else {
            if(wifiNetworkModel.isHotspotRunning) {
                return true

            }else {
                return  wifiNetworkModel.ipAddress !== ""
            }
        }
    }


    Rectangle {
        y: 30
        anchors.horizontalCenter: parent.horizontalCenter
        id: r1
        color: "white"
        width: 320
        height: 320
        visible: root.text != ""
        QRCode {
            anchors {
                verticalCenterOffset: 2
                horizontalCenterOffset: 2
                centerIn: parent
            }
            id: netinfoQRCode
            value : root.text
            width: 300
            height: 300
            level: "Q"
        }
    }
    Item {
        anchors {
            horizontalCenter: r1.horizontalCenter
            top: r1.bottom
            margins: 20
        }
        height: 30
        width: 760
        Text {
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            font.pixelSize: 24
            text: root.text
            anchors.centerIn: parent
        }
    }
}
