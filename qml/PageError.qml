/*
    Copyright 2019-2020, Prusa Research s.r.o.
              2021, Prusa Research a.s.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import PrusaComponents 1.0
import ErrorsSL1 1.0
import cz.prusa3d.sl1.errorcodestext 1.0
import cz.prusa3d.sl1.printer0 1.0
import Native 1.0
import "3rdparty/qrcode-generator"

PageQrCode {
    id: root
    title: code in ErrorcodesText.titles ? ErrorcodesText.titles[code] : qsTr("Error")
    name: "error"
    screensaver_enabled: false
    header_can_expand: false
    showQrDescription: true
    qrContent: root.url
    qrDescription: simpleUrl
    pictogram: Theme.pictogram.error

    text: qsTr("Error code:") + " #" + code.toString() + "\n\n" +
          (ErrorcodesText.messages[root.code] ? global.stringFormat(ErrorcodesText.messages[root.code], root.params) : "") +
          (root.code === ErrorsSL1.UNKNOWN ? ("\n\n" + root.params.name) : "")
    bgColor: Theme.color.errorBackground
    property int code: 10500
    property var params: ({})
    property string simpleUrl: (printer0.printer_model === Printer0.M1 ? "medicalhelp.prusa3d.com/" : "help.prusa3d.com/") + LocaleSetting.locale.substring(0,2) + "/" + code
    property string url: simpleUrl + printer0.help_page_url

    Component.onCompleted: () => { global.soundError(); printer0.power_led_set_error() }
    Component.onDestruction: () => { printer0.power_led_remove_error() }
}
