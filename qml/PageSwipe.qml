/*
    Copyright 2022, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Controls 2.12
import PrusaComponents 1.0

PrusaPage {
    id: root

    default property alias content: swipeView.contentData
    property alias indicator: indicator
    property alias interactive: swipeView.interactive
    property alias currentIndex: swipeView.currentIndex
    property alias transitions: swipeView.transitions
    function incrementCurrentIndex() { swipeView.incrementCurrentIndex() }
    function decrementCurrentIndex() { swipeView.decrementCurrentIndex() }

    header_rollup: swipeView.currentItem instanceof PrusaPage ? swipeView.currentItem.header_rollup : false


    SwipeView {
        id: swipeView
        anchors.fill: parent

    }

    PrusaPageIndicator {
        id: indicator
        count: swipeView.count
        currentIndex: swipeView.currentIndex
    }
}
