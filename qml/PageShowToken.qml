
/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import PrusaComponents 1.0

PrusaPage { 
    id: root
    title: qsTr("Prusa Connect")
    name: "ShowToken"
    pictogram: Theme.pictogram.settingsSimple
    property string token: ""
    property string description: qsTr("Temporary Token")
    property int description_size: 34
    property var onClick: function() { view.pop(); }

    Column {
        anchors.left: root.left
        anchors.leftMargin: 80
        anchors.topMargin: 100
        anchors.fill: parent
        spacing: 20

        Text {
            width: 450
            height: 100
            text: description
            font.pixelSize: description_size
            wrapMode: Text.Wrap
            verticalAlignment: Text.AlignBottom
        }

        Rectangle {
            color: "white"
            width: 400
            height: 50
            Text {
                color: "black"
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    verticalCenter: parent.verticalCenter
                }
                text: token ? token : qsTr("N/A")
                font.pixelSize: 44
                wrapMode: Text.Wrap
            }
        }
    }

    Row {
        id: buttonCancel
        anchors {
            right: parent.right
            verticalCenter: parent.bottom
            verticalCenterOffset: -480/2 - 10
            rightMargin: 55
        }
        spacing: 30

        PictureButton {
            id: btnconfirmScreenContinue
            name: "Register"
            text: qsTr("Continue")
            pictogram: Theme.pictogram.yes
            backgroundColor: "green"
            onClicked: () => {
                root.onClick();
            }
        }
    }
    
    ImageVersionText {}
}
