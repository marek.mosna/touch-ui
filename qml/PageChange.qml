/*
    Copyright 2019-2020, Prusa Research s.r.o.
    Copyright 2021, Prusa Research a.s.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5


import Qt.labs.qmlmodels 1.0
import QtQml.Models 2.12

import cz.prusa3d.sl1.filemanager 1.0
import cz.prusa3d.sl1.printer0 1.0
import ErrorsSL1 1.0
import Notification 1.0
import PrusaComponents 1.0
import PrusaComponents.Delegates 1.0

PageVerticalList {
    id: root
    name: "PrintSettings"
    title: qsTr("Print Settings")
    pictogram: Theme.pictogram.settingsSimple

    property QtObject exposure: ({})

    property real exposure_time_ms: 0
    property real exposure_time_calibrate_ms: 0
    property real exposure_time_first_ms: 0
    property int calibration_regions: 0

    // Detect calibration object and enable setting of exposure_time_calibration_ms
    // Calibration objects have the number calibration regions set to a non-zero value, normal ones don't.
    property bool show_time_calibrate: calibration_regions > 0

    property int spinbox_width: 280

    // Conditionaly bind the local properties to the values of the exposure object
    Binding on exposure_time_ms { when: exposure;  value: exposure.exposure_time_ms }
    Binding on exposure_time_calibrate_ms { when: exposure; value: exposure.exposure_time_calibrate_ms}
    Binding on exposure_time_first_ms { when: exposure; value: exposure.exposure_time_first_ms}
    Binding on calibration_regions { when: exposure; value: exposure.calibration_regions}

    Component.onCompleted: () => {
        if(!root.exposure) root.exposure = printer0.getCurrentExposureObject()
    }

    ObjectModel {
        id: listItemModel
        DelegatePlusMinus {
            id: exposure_time_delegate
            name: "ExposureTime"
            text: qsTr("Exposure")
            pictogram: Theme.pictogram.exposureTime

            spinBox.width: root.spinbox_width
            initValue:  exposure.exposure_time_ms
            step: 100
            from: 1000
            to: 60000

            Component.onDestruction: () => exposure.exposure_time_ms = value

            Connections {
                target: exposure
                function onExposure_time_msChanged() {
                    exposure_time_delegate.value = exposure.exposure_time_ms
                }
            }
            valuePresentation: function(v) { return (v/1000).toFixed(1) }
            units: qsTr("second")
        }


        DelegatePlusMinus {
            id: exposure_time_calibrate_delegate
            name: "ExposureTimeCalibrate"
            text: qsTr("Exposure Time Incr.")
            pictogram: Theme.pictogram.exposureTime

            visible: root.show_time_calibrate
            height: visible ? 70 : 0

            spinBox.width: root.spinbox_width
            initValue: exposure.exposure_time_calibrate_ms
            step: 100
            from: 100
            to: 5000

            Component.onDestruction: () => exposure.exposure_time_calibrate_ms = value

            Connections {
                target: exposure
                function onExposure_time_calibrate_msChanged() {
                    exposure_time_calibrate_delegate.value = exposure.exposure_time_calibrate_ms
                }
            }
            valuePresentation: function(v) { return (v/1000).toFixed(1) }
            units: qsTr("second")
        }

        DelegatePlusMinus {
            id: exposure_time_first_ms_delegate
            name: "FirstLayerExposure"
            text: qsTr("First Layer Expo.")
            pictogram: Theme.pictogram.exposureTime

            spinBox.width: root.spinbox_width
            initValue: exposure.exposure_time_first_ms
            step: 1000
            from: 10000
            to: 120000

            Component.onDestruction: () => exposure.exposure_time_first_ms = value

            Connections {
                target: exposure
                function onExposure_time_first_msChanged() {
                    exposure_time_first_ms_delegate.value = exposure.exposure_time_first_ms
                }
            }
            valuePresentation: function(v) { return (v/1000).toFixed(1) }
            units: qsTr("second")
        }

        DelegateSelect {
            id: tilt_profile_delegate
            name: "UserProfile"
            text: qsTr("Print Profile")
            selectionBox.width: root.spinbox_width
            pictogram: Theme.pictogram.limit
            index: exposure.user_profile
            items: [
                //: Default print profile. Printer behaves as before.
                qsTr("Faster", "Default print profile. Printer behaves as before."),
                //: Safe print profile with slow tilts and pause before each exposure.
                qsTr("Slower", "Safe print profile with slow tilts and pause before each exposure."),
                //: SuperSlow print profile for extra viscous resin
                qsTr("High Viscosity", "SuperSlow print profile for extra viscous resin")
            ]
            wrap: true

            Component.onDestruction: () => {
                                         console.log("Setting the user profile to ", index)
                                         exposure.user_profile = index
                                     }

            Connections {
                target: exposure
                function onUser_profileChanged() {
                    tilt_profile_delegate.index = exposure.user_profile
                }
            }

        }
    }
    model: listItemModel
}
