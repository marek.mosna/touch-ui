/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import cz.prusa3d.updater 1.0
import cz.prusa3d.sl1.printer0 1.0
import PrusaComponents 1.0

Column {
    anchors {
        left: parent.left
        leftMargin: 55
        bottom: parent.bottom
        bottomMargin: 5
    }

    Text {
        visible: printer0.factory_mode || updater.updateChannel === Updater.Development
        color: "lightgrey"
        text: {
            var channelText
            switch(updater.updateChannel) {
            case Updater.Stable:
                channelText = "stable update channel"
                break
            case Updater.Medic:
                channelText = "stable medical channel"
                break
            case Updater.Beta:
                channelText = "beta update channel"
                break
            case Updater.Development:
                channelText = "dev update channel"
                break
            }
            return channelText
        }
    }


    Text {
        text: printer0.system_version + (printer0.factory_mode ? " (factory mode)" : "")
        color: "lightgrey"
    }
}


