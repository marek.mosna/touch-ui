/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import cz.prusa3d.sl1.printer0 1.0
import PrusaComponents 1.0

PrusaPage {
    id: root
    title: qsTr("Fullscreen Image")
    name: "fullscreenimage"
    header_rollup: true

    SwipeView {
        // This makes it overlap the area normally ocupied by the page title
        height: 480
        width: parent.width
        anchors.bottom: parent.bottom
        Image {
            source: "file:///run/slafw/fsimage.png"
            cache: false
            fillMode: Image.Pad
        }
    }

    // MouseArea intentionally overlaps the ListView
    MouseArea {
        height: 480
        width: parent.width
        anchors.bottom: parent.bottom
        onClicked: {
            printer0.beep_button()
            view.pop()
        }
    }
}
