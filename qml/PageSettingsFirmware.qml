/*
    Copyright 2019, Prusa Research s.r.o.
    Copyright 2020-2021, Prusa Research a.s.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import Qt.labs.qmlmodels 1.0
import QtQml.Models 2.12
import cz.prusa3d.updater 1.0
import cz.prusa3d.sl1.filemanager 1.0
import cz.prusa3d.sl1.printer0 1.0
import ErrorsSL1 1.0
import Notification 1.0
import PrusaComponents 1.0
import PrusaComponents.Delegates 1.0

PageVerticalList {
    id: root
    title: qsTr("Firmware")
    name: "firmwareupdate"
    pictogram: Theme.pictogram.firmware

    property var firmwares: fmcontext.flatList(FMFile.Raucb)

    function reload_model() { firmwares = fmcontext.flatList(FMFile.Raucb) }

    // AlternativeSystemVersion has no Changed(..) signal, but it should be fine to reload it on creation
    property string loadedAlternativeSystemVersion: qsTr("Unknown", "Unknown operating system version on alternative slot")
    Component.onCompleted: ()=>{
        if(rauc.alternativeSystemVersion()) {
            loadedAlternativeSystemVersion = rauc.alternativeSystemVersion()
        }
        console.log("rauc loadedAlternativeSystemVersion = ", loadedAlternativeSystemVersion)
        printer0.add_oneclick_inhibitor("touch-ui-firmware-update")
    }

    Component.onDestruction: ()=>{
        printer0.remove_oneclick_inhibitor("touch-ui-firmware-update")
    }

    FMContext {
        id: fmcontext
        onCurrentChanged: () => reload_model()
    }

    Connections {
        target: updater
        function onOperationChanged() {
            reload_model()
        }
    }

    property var updater_action_text : {
        switch(updater.operation) {
        case Updater.UpdateStatusUpdateAvailable:
        case Updater.UpdateStatusDownloadFailed:
            return qsTr("Download Now")
        case Updater.UpdateStatusDownloaded:
            return qsTr("Install Now")
        default:
            return qsTr("Check for Update")
        }
    }

    property var status_text : {
        switch(updater.operation) {
        case Updater.UpdateStatusCheckFailed:
            return qsTr("Check for update failed");
        case Updater.UpdateStatusCheckingForUpdate:
            return qsTr("Checking for updates...");
        case Updater.UpdateStatusUpToDate:
            return qsTr("System is up-to-date");
        case Updater.UpdateStatusUpdateAvailable:
            return qsTr("Update available");
        case Updater.UpdateStatusDownloading:
            return qsTr("Update is downloading")
        case Updater.UpdateStatusDownloaded:
            return qsTr("Update is downloaded")
        case Updater.UpdateStatusDownloadFailed:
            return qsTr("Update download failed")
        default:
            return qsTr("Updater service idle")
        }
    }




    ObjectModel {
        id: listItemModel

        DelegateText {
            pictogram: Theme.pictogram.systemInfo
            text: qsTr("Installed version") + ": " + printer0.system_version
        }

        DelegateRefWithStatus {
            name: "check"
            pictogram: Theme.pictogram.update
            text: updater_action_text
            status:  status_text
            onClicked: () => {
               switch(updater.operation) {
               case Updater.UpdateStatusUpdateAvailable:
               case Updater.UpdateStatusDownloadFailed:
               case Updater.UpdateStatusDownloaded:
                   console.log("Initiating update install")
                   root.StackView.view.push("PageReleaseNotes.qml")
                   break;
               default:
                   console.log("Initating manual check for update")
                   updater.check()
               }
           }
        }

        DelegateSwitch {
            name: "beta"
            visible: printer0.printer_model !== Printer0.M1
            checked: updater.updateChannel === Updater.Beta
            text: qsTr("Receive Beta Updates") + ( updater.updateChannel === Updater.Development ?  "<br/><font color=\"#888\">" + "Dev update channel is active!" + "</font>" : "")
            pictogram: Theme.pictogram.testtube
            onClicked: ()=>{
                notificationModel.removeAll(Notification.UpdateNotification)
                if (updater.updateChannel === Updater.Beta) {
                    console.log("Switching update channel to stable")
                    updater.updateChannel = Updater.Stable
                } else {
                    console.log("Switching update channel to beta")
                    root.StackView.view.push(switchToBetaDialog)
                }
            }

            Component {
                id: switchToBetaDialog
                PageYesNoSimple {
                    id: switchToBetaRoot
                    name: "SwitchChannelToBetaConfirm"
                    title: qsTr("Switch to beta?")
                    text: qsTr("Warning! The beta updates can be unstable.<br/><br/>Not recommended for production printers.<br/><br/>Continue?")
                    onYes: () => {
                               updater.updateChannel = Updater.Beta
                               switchToBetaRoot.StackView.view.pop()
                           }
                    onNo: () => switchToBetaRoot.StackView.view.pop()
                }
            }
        }

        DelegateRef {
            name: "switchsystem"
            text: qsTr("Switch to version:") + " " + root.loadedAlternativeSystemVersion
            pictogram:  Theme.pictogram.adminSimple

            visible: root.loadedAlternativeSystemVersion !== ""
            onClicked: ()=>root.StackView.view.push(switchAlternativeDialog)


            Component {
                id: switchAlternativeDialog
                PageYesNoSimple {
                    id: switchAlternativeDialogRoot
                    name: "SwitchToAlternativeSlot"
                    title: qsTr("Downgrade?")
                    text: qsTr("Do you really want to downgrade to FW") + " <b>" + root.loadedAlternativeSystemVersion + "</b> ?"
                    onYes: () => {
                               console.log("Switching to alternative slot");
                               if(printer0.isVersionCompatibleWithHw(root.loadedAlternativeSystemVersion, printer0.printer_model)) {
                                   rauc.switchToAlternativeSystem(
                                       function(){
                                           printer0.poweroff(true, true)
                                           //switchAlternativeDialogRoot.StackView.view.pop()
                                       },
                                       function(){
                                           console.log("error while switching slots")
                                           push_error({code: ErrorsSL1.toStringCode(ErrorsSL1.UNKNOWN)})
                                           switchAlternativeDialogRoot.StackView.view.pop()
                                       }
                                       );
                               }
                               else switchAlternativeDialogRoot.StackView.view.replace(incompatibleFwOnAlternativeDialog)
                           }
                    onNo: () => switchAlternativeDialogRoot.StackView.view.pop()
                }
            }

            Component {
                id: incompatibleFwOnAlternativeDialog
                PageYesNoSimple {
                    id: switchAlternativeDialogRoot
                    name: "IncompatibleFirmwareConfirm"
                    pictogram: Theme.pictogram.warning
                    title: qsTr("Incompatible FW!")
                    text: qsTr("The alternative FW version <b>%1</b> is not compatible with your printer model - <b>%2</b>.").arg(root.loadedAlternativeSystemVersion).arg(printerModelName(printer0.printer_model))
                          + "<br/><br/>"
                          + qsTr("If you switch, you can update to another FW version afterwards but <b>you will not be able to print.</b>")
                          + "<br/><br/>"
                          + qsTr("Continue anyway?")

                    onYes: () => {
                               console.log("Switching to incompatible alternative slot");
                               rauc.switchToAlternativeSystem(
                                   function(){
                                       printer0.poweroff(true, true)
                                       //switchAlternativeDialogRoot.StackView.view.pop()
                                   },
                                   function(){
                                       console.log("error while switching to incompatible slot")
                                       push_error({code: ErrorsSL1.toStringCode(ErrorsSL1.UNKNOWN)})
                                       switchAlternativeDialogRoot.StackView.view.pop()
                                   }
                                   );
                           }
                    onNo: () => switchAlternativeDialogRoot.StackView.view.pop()

                    function printerModelName(model) {
                        switch(model) {
                        case Printer0.NONE: return qsTr("None", "Printer model is not known/can't be determined")
                        case Printer0.SL1: return "SL1"
                        case Printer0.SL1S: return "SL1S"
                        case Printer0.M1: return "M1"
                        default: return qsTr("Newer than SL1S", "Printer model is unknown, but better than SL1S")
                        }
                    }
                }
            }
        }

        DelegateRef {
            name: "factoryreset"
            text:qsTr("Factory Reset")
            pictogram: Theme.pictogram.factory
            onClicked: root.StackView.view.push(factoryResetDialog)
            Component {
                id: factoryResetDialog
                PageYesNoSimple {
                    id: factoryResetDialogRoot
                    name: root.Name
                    title: qsTr("Are you sure?")
                    text: qsTr("Do you really want to perform the factory reset?\n\nAll settings will be erased!\nProjects will stay untouched.")
                    onYes: () => {
                               printer0.run_factory_reset_wizard()
                               factoryResetDialogRoot.StackView.view.pop()
                           }
                    onNo: () => factoryResetDialogRoot.StackView.view.pop()
                }
            }
        }

        ListView {
            height: 70 * count
            model: root.firmwares
            interactive: false
            delegate: DelegateRef {
                //
                // click --> wait --> downgrade? --> incompatible downgrade?
                //
                //
                id: bundle_delegate
                name: modelData.name
                text: modelData.name
                pictogram: Theme.pictogram.usb
                onClicked: () => {
                    root.StackView.view.push(waitForBundleVersionDialog, {bundle_path: modelData.path, bundle_name: modelData.name})
                }
            }

            Component {
                id: waitForBundleVersionDialog
                PageWait {
                    id: waitForBundleVersionDialogRoot
                    name: "BundleVersionWait"
                    title: qsTr("FW Info", "page title, information about the selected update bundle")
                    text:  qsTr("Loading FW file meta information\n(may take up to 20 seconds) ...")
                    Component.onCompleted: () => {
                        rauc.info(
                           bundle_path,
                           function(compatible, version){
                               console.log("bundle:", version, " current:", updater.currentVersion)
                               if(printer0.versionLessEqual(version, updater.currentVersion)) {
                                   waitForBundleVersionDialogRoot.StackView.view.replace(downgradeDialog, {
                                                                                             currentVersion: updater.currentVersion,
                                                                                             bundleVersion: version,
                                                                                             bundle_path: bundle_path,
                                                                                             bundle_name: bundle_name
                                                                                         })
                               }
                               else {
                                   waitForBundleVersionDialogRoot.StackView.view.replace(doYouWantToInstallBundleDialog, {
                                                                                             bundleVersion: version,
                                                                                             bundle_path: bundle_path,
                                                                                             bundle_name: bundle_name
                                                                                         })
                               }
                           },
                           function(e){
                               console.log("Error getting info for bundle",bundle_path," - ", JSON.stringify(e))
                               console.log("Bundle version could not be determined, but allowing installation anyway.")
                               waitForBundleVersionDialogRoot.StackView.view.replace(installBundleDialog,
                                                                                     {
                                                                                         bundle_path: bundle_path,
                                                                                         bundle_name: bundle_name
                                                                                     })
                           })
                    }
                    property string bundle_path
                    property string bundle_name
                }
            }

            Component {
                id: installBundleDialog
                PageYesNoSimple {
                    id: installBundleDialogRoot
                    name: "InstallBundleConfirm"
                    title: qsTr("Install Firmware?")
                    text: qsTr("FW file meta information not found.<br/><br/>Do you want to install<br/><b>%1</b><br/>anyway?").arg(bundle_name)
                    onYes: () =>  run_update({bundle_path: bundle_path, bundle_name: bundle_name, replace: true})
                    onNo: () => popDialog()
                    property string bundle_path
                    property string bundle_name
                }
            }

            Component {
                id: downgradeDialog
                PageYesNoSimple {
                    id: downgradeDialogRoot
                    name: "DowngradeFirmwareConfirm"
                    title: qsTr("Downgrade Firmware?")
                    text: {
                        var help = "Version of selected FW file 1.5.3.raucb, is lower or equal than current 1.6.0."
                        return qsTr("Version of selected FW file<br/><b>%1</b>,", help).arg(bundleVersion) +
                            "<br/>" +
                            qsTr("is lower or equal than current<br/><b>%1</b>.", help).arg(currentVersion) +
                            "<br/><br/>" +
                            qsTr("Do you want to continue?")
                    }
                    onYes: () => {
                               if(printer0.isVersionCompatibleWithHw(bundleVersion, printer0.printer_model)) {
                                   run_update({bundle_path: bundle_path, bundle_name: bundle_name, replace: true})
                               }
                               else {
                                   downgradeDialogRoot.StackView.view.replace(incompatibleBundleDialog, {
                                                                                  currentVersion: currentVersion,
                                                                                  bundleVersion: bundleVersion,
                                                                                  bundle_path: bundle_path,
                                                                                  bundle_name: bundle_name
                                                                              })
                               }
                           }
                    onNo: () => popDialog()
                    property string currentVersion
                    property string bundleVersion
                    property string bundle_path
                    property string bundle_name
                }
            }

            Component {
                id: doYouWantToInstallBundleDialog
                PageYesNoSimple {
                    id: doYouWantToInstallBundleDialogRoot
                    name: "InstallBundleConfirm"
                    title: qsTr("Install Firmware?")
                    text: qsTr("You have selected update bundle <b>\"%1\"</b>,").arg(bundle_name)
                          + "<br/>"
                          + qsTr("which has version <b>%1</b>.").arg(bundleVersion)
                          + "<br/><br/>"
                          + qsTr("Do you want to continue?")
                    onYes: () => {
                               if(printer0.isVersionCompatibleWithHw(bundleVersion, printer0.printer_model)) {
                                   run_update({bundle_path: bundle_path, bundle_name: bundle_name, replace: true})
                               }
                               else {
                                   doYouWantToInstallBundleDialogRoot.StackView.view.replace(incompatibleBundleDialog, {
                                                                                  bundleVersion: bundleVersion,
                                                                                  bundle_path: bundle_path,
                                                                                  bundle_name: bundle_name
                                                                              })
                               }
                           }
                    onNo: () => popDialog()
                    property string bundleVersion
                    property string bundle_path
                    property string bundle_name
                }
            }

            Component {
                id: incompatibleBundleDialog
                PageYesNoSimple {
                    id: incompatibleBundleDialogRoot
                    name: "IncompatibleBundleConfirm"
                    pictogram: Theme.pictogram.warning
                    title: qsTr("Incompatible FW!")
                    text: qsTr("<b>%1</b><br/>is not compatible with your current hardware model - <b>%2</b>.").arg(bundleVersion).arg(printerModelName(printer0.printer_model))
                        + "<br/><br/>"
                        + qsTr("After installing this update, the printer can be updated to another FW but <b>printing won't work.</b>")
                        + "<br/><br/>"
                        + qsTr("Continue anyway?")

                    onYes: () => run_update({bundle_path: bundle_path, bundle_name: bundle_name, replace: true})
                    onNo: () => popDialog()

                    function printerModelName(model) {
                        switch(model) {
                        case Printer0.NONE: return qsTr("None", "Printer model is not known/can't be determined")
                        case Printer0.SL1: return "SL1"
                        case Printer0.SL1S: return "SL1S"
                        default: return qsTr("Unknown", "Printer model is unknown, but likely better than SL1S")
                        }
                    }
                    property string bundleVersion
                    property string bundle_path
                    property string bundle_name
                }
            }

        }
    }

    model: listItemModel
}
