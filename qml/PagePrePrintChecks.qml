
/*
    Copyright 2019-2020, Prusa Research s.r.o.
    Copyright 2021, Prusa Research a.s.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import Native 1.0

import PrusaComponents 1.0


PrusaPage {
    id: root
    title: qsTr("Please Wait")
    name: "PrePrintChecks"
    implicitWidth: 800
    implicitHeight: 480
    back_button_enabled: true
    cancel_instead_of_back: true
    pictogram: Theme.pictogram.sandclock
    screensaver_enabled: false
    property QtObject exposure
    back_handler: function(){
        if(exposure) {
            exposure.cancel()
        }
    }

    Component.onCompleted: ()=>{
        if(! root.exposure) {
            root.exposure = printer0.getCurrentExposureObject()
        }
    }



    Component {
        id: checkDelegate
        ColumnLayout {
            width: checkList.width
            spacing: 0
            RowLayout {
                height: 80
                Timer {
                    interval: 1000
                    running: model.checkState === ChecksModel.Success
                    onTriggered: ()=>{
                        checkList.model.removeRow(index)
                    }
                }
                Item {
                    width: 40
                    height: 40
                    Layout.alignment: Qt.AlignVCenter
                    Image {
                        id: checkPictogram
                        anchors.fill: parent
                        sourceSize.width: parent.width
                        sourceSize.height: parent.height
                        fillMode: Image.PreserveAspectFit
                        rotation: 0

                        source: {
                            switch(model.checkState) {
                            case ChecksModel.Scheduled: return Theme.pictogram.path + "/" + Theme.pictogram.waiting
                            case ChecksModel.Running: return Theme.pictogram.path + "/" + Theme.pictogram.busy
                            case ChecksModel.Success: return Theme.pictogram.path + "/" + Theme.pictogram.ok
                            case ChecksModel.Failure: return Theme.pictogram.path + "/" + Theme.pictogram.nok
                            case ChecksModel.Warning: return  Theme.pictogram.path + "/" + Theme.pictogram.ok
                            case ChecksModel.Disabled: return Theme.pictogram.path + "/" + Theme.pictogram.disabled
                            default: JSON.stringify(model.checksState)
                            }
                        }
                        NumberAnimation on rotation {
                            running: {
                                if(model.checkState === ChecksModel.Running) {
                                    return true
                                }
                                else {
                                    checkPictogram.rotation = 0
                                    return false
                                }
                            }

                            from:0
                            to: 360
                            duration: 800
                            loops: Animation.Infinite
                        }
                    }
                }
                Text {
                    id: txtLabel
                    Layout.fillWidth: true
                    font.pixelSize: Theme.font.big
                    color: model.checkState === ChecksModel.Disabled ? Theme.color.grey : Theme.color.text
                    text: {
                        switch(model.checkId) {
                        case ChecksModel.Temperature: return qsTr("Temperature")
                        case ChecksModel.Project: return qsTr("Project")
                        // case ChecksModel.Hardware: return qsTr("Hardware") axis are homed separately
                        case ChecksModel.Fan: return qsTr("Fan")
                        case ChecksModel.Cover: return qsTr("Cover")
                        case ChecksModel.Resin: return qsTr("Resin")
                        case ChecksModel.StartPositions: return qsTr("Starting Positions")
                        case ChecksModel.Stirring: return qsTr("Stirring")
                        default: return JSON.stringify(model.checkId)
                        }
                    }
                }
                Text {
                    id: txtDescription
                    width: 400-10
                    font.pixelSize: Theme.font.big
                    color: Theme.color.grey //model.checkState === ChecksModel.Disabled ? Theme.color.grey : "white"
                    text: {
                        switch(model.checkState) {
                        case ChecksModel.Scheduled: return ""
                        case ChecksModel.Running:
                            switch(model.checkId) {
                                case ChecksModel.Resin: return qsTr("Do not touch the printer!")
                                default: return ""
                            }
                        case ChecksModel.Success:
                            switch(model.checkId) {
                            // FIXME: Seems that the resin volume is not available yet
                            //case ChecksModel.Resin: return root.exposure.resin_remaining_ml.toFixed(1) + " " + qsTr("ml")
                            default: return ""
                            }
                        case ChecksModel.Failure: return ""
                        case ChecksModel.Warning: return  qsTr("With Warning")
                        case ChecksModel.Disabled: return qsTr("Disabled")
                        default: JSON.stringify(model.checksState)
                        }
                    }
                }
                // Spacer
                Item {
                    height: parent.height
                    width: 20
                }
            }
            Rectangle {
                height: 2
                Layout.fillWidth: true
                color: Theme.color.grey
            }
        }
    }
    ListView {
        id: checkList
        height: 420
        interactive: true
        anchors {
          bottom: parent.bottom
          left: parent.left
          right: parent.right
          rightMargin: 10
          leftMargin: 10
        }

        clip: true

        ScrollBar.vertical: ScrollBar {
            active: true
            policy: ScrollBar.AsNeeded
        }
        delegate: checkDelegate

        remove: Transition {
            NumberAnimation { property: "opacity"; from: 1.0; to: 0; duration: 400 }
            NumberAnimation { property: "x";  to: -600; duration: 400 }
        }

        displaced: Transition {
            NumberAnimation { properties: "x,y"; duration: 400; easing.type: Easing.InSine }
        }

        Binding on model {
            when: root.exposure !== null && root.exposure !== undefined
            value: root.exposure.checksModel
        }
    }
}
