
/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12

FontLoader {
    id: prusaFont;
    source: "fonts/black_grotesk/AtlasGrotesk-Light.otf"
    onStatusChanged: () => {
        if(prusaFont.status == FontLoader.Ready) console.log("Prusa Font status changed: Ready");
        else if(prusaFont.status == FontLoader.Error) console.log("Prusa Font status changed: Error");
        else if(prusaFont.status == FontLoader.Loading) console.log("Prusa Font status changed: Loading");
        else if(prusaFont.status == FontLoader.Null) console.log("Prusa Font status changed: Null");
        else console.log("FontLoader - control should never reach this place")
    }
}
