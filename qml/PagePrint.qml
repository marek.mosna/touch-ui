
/*
    Copyright 2019-2020, Prusa Research s.r.o.
    Copyright 2021, Prusa Research a.s.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import Native 1.0
import cz.prusa3d.sl1.errorcodestext 1.0
import PrusaComponents 1.0

/*
This page handles the UI related to the current print job represented
by the current Exposure object.

The contained StackView is used to switch pages internaly
based only on state of the exposure object.

Title is extracted from the subordinate pages and propagated up.
BackButton press is handled via back_handler properties.
*/

PrusaPage {
    id: root
    name: "PrintTopPage"
    title: printContainer.currentItem.title
    back_button_enabled: printContainer.currentItem.back_button_enabled
    screensaver_enabled:  printContainer.currentItem.screensaver_enabled
    pictogram:  printContainer.currentItem.pictogram
    cancel_instead_of_back:  printContainer.currentItem.cancel_instead_of_back
    back_handler: function(){
        if(printContainer.currentItem.name === "PrintPreviewSwipe") {
            printContainer.currentItem.back_handler()
            root.StackView.view.pop()
        }
        else printContainer.currentItem.back_handler()
    }

    property QtObject exposure: printer0.current_exposure_object
    property int exposure_state: 0
    property var exposure_exception: ({})

    // "loading" should be turned-off on the second exposureChanged signal,
    // that normally indicates that the exposure has been switched to the current one.
    // --[exposureChanged]---> first_exposure --[exposureChanged]---> loading
    property bool first_exposure: true
    property bool loading: true

    Binding on exposure_state {
        when: exposure !== undefined && exposure !== null
        value: exposure.state
    }

    Binding on exposure_exception {
        when: exposure !== undefined && exposure !== null
        value: exposure.exposure_exception
    }

    /* Note: if Printer0.state is updated before new exposure object is created
             and the frontend is notified of the change, that would lead to an
             intermediate state where exposure is an old one or null. This makes
             sure the state is switched correctly.
    */
    onExposureChanged: () => {
        console.log("exposure changed:", printer0.current_exposure, ", ", exposure ? exposure.project_file : "NULL")
        if(root.exposure) {
            root.handleStateChange(root.exposure.state)

            // FIXME: handles situation where the printer0.state is switched to PRINTING
            // when the current exposure is still the previous one(non-atomic update)
            if(root.first_exposure === true) {
                root.first_exposure = false
                if(printer0.current_exposure === "/") {
                    root.loading = false
                }
            }
            else {
                root.loading = false
            }
        }
    }

    onExposure_exceptionChanged: () => {
        // This should handle the situation where the exposure
        // is updated after the printer0.state changes to PRINTING
        // so as to avoid pushing errors for the previous exposure object.
        if( !root.loading) {
            push_error(exposure.exposure_exception)
        }
    }

    Component.onCompleted: () => {
        cancelLoadingTimer.start()
    }

    // "emergency" timer to switch-off the loading state
    // if waiting for exposureChanged takes too long
    Timer {
        id: cancelLoadingTimer
        interval: 5000
        onTriggered: () => { root.loading = false}
        triggeredOnStart: false
        repeat: false
    }

    Component {
        id: checkWarningPage
        PageYesNoSimple {
            property QtObject exposure: root.exposure
            property var exposure_warning
            property var error_code: 10501

            Binding on exposure_warning {
                when: exposure !== undefined
                value: exposure.exposure_warning
            }

            Binding on error_code {
                when: exposure_warning !== undefined
                value: exposure_warning["code"] ? exposure_warning["code"].substring(1) : 10501
            }

            name: "CheckWarning"


            /* Binding is intentionally avoided, because the exposure_warning will be changed before
               leaving this state, leading to visible switch from the original message to a blank
               one shortly before removing this page(code 10700).
             */
            Component.onCompleted: () => {
                text = ErrorcodesText.messages[error_code] + "\n\n" + qsTr("Continue?")
                global.soundError()
            }

            title: qsTr("Check Warning")
            onYes: () => root.exposure.confirm_print_warning()
            onNo: () => root.exposure.reject_print_warning()
        }
    }

    Component {
        id: stuckPage
        PageConfirm {
            name: "StuckRecovery"
            title: qsTr("Stuck Recovery")
            text: qsTr("The printer got stuck and needs user assistance.") + "\n\n" +
                  qsTr("Release the tank mechanism and press Continue.") + "\n\n" +
                  qsTr("If you do not want to continue, press the Back button on top " +
                       "of the screen and the current job will be canceled.")
            onConfirmed: () => {
                console.log("Continuing after printer was stuck")
                exposure.cont()
            }
            back_handler: function(){
                exposure.back()
            }
            property QtObject exposure
            Component.onCompleted: () => global.soundError()
        }
    }


    Component {
        id: closeCoverWarning
        PageWait {
            id: closeCoverWarningRoot
            property QtObject exposure
            name: "CloseCoverWarning"
            title: qsTr("Close Cover!")
            text: qsTr("Please, close the cover! UV radiation is harmful.")
            Component.onCompleted: () => global.soundError()
            Connections {
                target: exposure
                function onClose_cover_warningChanged() {
                      if(! closeCoverWarningRoot.exposure.close_cover_warning) {
                          if(closeCoverWarningRoot.StackView.view.currentItem === closeCoverWarningRoot) {
                              closeCoverWarningRoot.StackView.view.pop()
                          }
                      }
                  }
                function onStateChanged() {
                    let removeInStates = [
                        Exposure.FEED_ME,
                        Exposure.FAILURE,
                        Exposure.FINISHED,
                        Exposure.STUCK,
                        Exposure.STUCK_RECOVERY,
                        Exposure.READING_DATA,
                        Exposure.CONFIRM,
                        Exposure.CHECKS,
                        Exposure.CANCELED,
                        Exposure.CHECK_WARNING,
                        Exposure.DONE,
                        Exposure.OVERHEATING,
                    ]
                    if(removeInStates.indexOf(exposure.state) != -1) closeCoverWarningRoot.StackView.view.pop()
                }
            }
        }
    }

    Connections {
        target: root.exposure
        function onClose_cover_warningChanged() {
            let activeInStates = [
                Exposure.PRINTING,
                Exposure.STIRRING,
                Exposure.PENDING_ACTION
            ]
            if( root.exposure.close_cover_warning && activeInStates.indexOf(root.exposure.state) != -1) {
                 // It will remove itself from the stack when no longer needed
                root.StackView.view.push(closeCoverWarning, {exposure: root.exposure})
            }
        }
        function onDbusError(errdata){
            push_error(errdata)
        }
    }

    onExposure_stateChanged: () => handleStateChange(exposure_state)

    function handleStateChange(state) {
        console.log("Exposure state change: ", state)
        switch(state) {
        case Exposure.PRINTING:
            printContainer.replace(null, "PagePrintPrinting.qml", {exposure: root.exposure})
            break
        case Exposure.GOING_UP:
            printContainer.replace(null, "PageWait.qml", {title: qsTr("Going up"), text: qsTr("Moving platform to the top position")})
            break
        case Exposure.GOING_DOWN:
            printContainer.replace(null, "PageWait.qml", {title: qsTr("Going down"), text: qsTr("Moving platform to the bottom position")})
            break
        case Exposure.HOMING_AXIS:
            printContainer.push(null, "PageWait.qml", {title: qsTr("Project"), text: qsTr("Getting the printer ready to add resin. Please wait.")})
            break
        case Exposure.WAITING:
            printContainer.replace(null, "PageWait.qml", {text: qsTr("Please wait...")})
            break
        case Exposure.COVER_OPEN:
            printContainer.replace(null, "PageWait.qml", {title: qsTr("Cover Open"), text: qsTr("Paused.") + "\n" + qsTr("Please close the cover to continue")})
            global.soundError()
            break
        case Exposure.FEED_ME:
            printContainer.replace(null, "PageFeedme.qml", {exposure: root.exposure})
            global.soundError()
            break
        case Exposure.CONFIRM:
            printContainer.replace(null, "PagePrintPreviewSwipe.qml", {exposure: root.exposure})
            break
        case Exposure.POUR_IN_RESIN:
            printContainer.replace(null, "PagePrintPreviewSwipe.qml", {exposure: root.exposure, viewIndex: 2})
            break
        case Exposure.STIRRING:
            printContainer.replace(null, "PageWait.qml", {title: qsTr("Stirring"), text: qsTr("Stirring resin")})
            break
        case Exposure.PENDING_ACTION:
            printContainer.replace(null, "PageWait.qml", {title: qsTr("Action Pending"), text: qsTr("Requested actions will be executed after layer finish, please wait...")})
            break
        case Exposure.READING_DATA:
            printContainer.replace(null, "PageWait.qml", {text: qsTr("Reading data...")})
            break

        case Exposure.STUCK:
            printContainer.replace(null, stuckPage, {exposure: root.exposure})
            break
        case Exposure.STUCK_RECOVERY:
            printContainer.replace(null, "PageWait.qml", {title: qsTr("Stuck Recovery"), text: qsTr("Setting start positions...")})
            break
        case Exposure.TILTING_DOWN:
            printContainer.replace(null, "PageWait.qml", {title: qsTr("Tank Moving Down"), text: qsTr("Moving the resin tank down...")})
            break
        case Exposure.CHECK_WARNING:
            // Note: Exposure.exposure_warning is sometimes set only after Exposure.state is modified.
            //       Because of that, Exception.exposure_warning does not corespond to the correct warning(at the time of state change).
            //       Using a binding can work better in that case(as it will be updated later).

            if( printContainer.currentItem.name !== checkWarningPage.name ) printContainer.replace(null, checkWarningPage)
            break

        case Exposure.CHECKS:
            printContainer.replace(null, "PagePrePrintChecks.qml", {exposure: root.exposure})
            break
        case Exposure.OVERHEATING:
            printContainer.replace(null, "PageCoolingDown.qml")
            global.soundError()
            break
        case Exposure.FINISHED:
        case Exposure.FAILURE:
        case Exposure.CANCELED:
        //case Exposure.DONE:
            printContainer.replace(null, "PageFinished.qml", {exposure: root.exposure})
            break
        default:
            printContainer.replace(null, "PageWait.qml", {text: qsTr("Please wait...")})
        }
    }

    StackView {
        id: printContainer
        name: "Print"
        anchors.fill: parent
        initialItem: PagePrintPreviewSwipe {exposure: root.exposure}
    }

    PrusaWaitOverlay {
        id: waitOverlay
        text: qsTr("Loading, please wait...")
        doWait: root.loading
        anchors.fill: parent
    }

}
