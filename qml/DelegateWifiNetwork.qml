/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import Native 1.0

import PrusaComponents 1.0

Item {
    id: root
    height: wifiNetworkModel.isHotspotRunning ? 0 : 80
    visible: ! wifiNetworkModel.isHotspotRunning
    width: 760

    property var saved_psk: model.psk
    property var connection: model.connection
    onConnectionChanged: ()=>{
        if(connection) {
            wifiNetworkModel.refreshPsk(model.name)
        }
    }

    MouseArea {
        anchors.fill: parent
        z: -1
        onClicked: ()=>{
            printer0.beep_button()
            if( ! model.active) {
                if(model.connection && saved_psk) {
                    Wifi.addConnectionAuto(model.name, saved_psk, false, true,
                        function(){
                            console.log("Connecting to " + model.name + " succeeded.")
                        },
                        function(e) {
                            console.log("Connecting to " + model.name + " failed: " + e)
                        }
                    )
                }
                else {
                    view.push("PageAskForPassword.qml",
                              {
                                  network_name: model.name,
                                  okAction: function(network_name, password, saved_password) {
                                      var psk = password
                                      if(psk === "") psk = saved_password

                                      Wifi.addConnectionAuto(network_name, psk, false, true,
                                                             function(){
                                                                 console.log("Connecting to " + network_name + " succeeded.")
                                                                 view.pop()
                                                             },
                                                             function(e) {
                                                                 console.log("Connecting to " + network_name + " failed: " + e)
                                                                 view.pop()
                                                             }
                                                             )
                                  },
                                  cancelAction: function(network_name, password, saved_password) {
                                      view.pop()
                                  }

                              })
                }
            }
            else {
                if(wifiNetworkModel.activeConnectionState === WifiNetworkModel.Activated) {
                    view.push("PageWifiNetworkSettings.qml")
                }
            }
        }

        // Press & Hold action = delete the underlaying connection, if any.
        // Won't remove the item if the network is still visible.
        onPressAndHold: ()=>{
            console.log("Attempting to delete connection \"" + model.name + "\"")
            console.log("model.connection=" + model.connection)
            printer0.beep_button()
            if(model.connection !== "") {
                view.push(forgetNetDialog)
            }
        }

        Component {
            id: forgetNetDialog
            PageYesNoSimple {
                id: forgetNetDialogRoot
                title: qsTr("Forget network?")
                text: qsTr("Do you really want to forget this network's settings?")
                onYes: ()=>{
                           wifiNetworkModel.removeConnection(
                               model.name,
                               function() {
                                   saved_psk = ""
                                   forgetNetDialogRoot.StackView.view.pop()
                               },
                               function(err){
                                   saved_psk = ""
                                   forgetNetDialogRoot.StackView.view.pop()
                               }
                               )
                       }
                onNo: ()=>{ forgetNetDialogRoot.StackView.view.pop() }
            }
        }
    }

    Column {
        spacing: 5

        RowLayout {

            Rectangle {
                id: mainArea
                color: "black"
                height: 70
                width: 680

                Rectangle {
                    id: img
                    color: "black"
                    anchors {
                        left: parent.left
                        verticalCenter: parent.verticalCenter
                        leftMargin: 20
                        margins: 10
                    }
                    width: parent.height - 10
                    height: parent.height - 10

                    Image {
                        anchors {
                            fill: parent
                            margins: 2
                        }
                        sourceSize.width: parent.height
                        sourceSize.height: parent.height
                        fillMode: Image.PreserveAspectFit
                        source: {
                            if(! model.network && model.hidden === true ) {return Theme.pictogram.path + "/" + Theme.pictogram.wifiQuestion }
                            else if(! model.network) {return Theme.pictogram.path + "/" + Theme.pictogram.wifiOff}
                            return Theme.pictogram.path + "/" + global.pictureOfWifiSignalStrength(model.strength, Theme.pictogram.wifiPictogramsColor)
                        }
                    }
                }

                ColumnLayout {
                    spacing: 5
                    anchors {
                        verticalCenter: parent.verticalCenter
                        left: img.right
                        leftMargin: 50
                        right: parent.right
                        rightMargin: 10
                    }

                    RowLayout {
                        height: 26
                        width: parent.width

                        Text {
                            text: model.name
                        }

                        Text {
                            text: " "
                            color: "grey"
                        }

                        Item {
                            Layout.fillWidth: true
                        }
                    }

                    RowLayout {
                        visible: (model.connection !== "") || (model.active) || (_debug)
                        enabled: visible
                        height: visible ? 26 : 0

                        Rectangle {
                            id: savedPictogram
                            color: "black"
                            visible: model.connection !== ""
                            width: 24
                            height: 24

                            Image {
                                anchors {
                                    fill: parent
                                    margins: 2
                                }
                                sourceSize.width: parent.height
                                sourceSize.height: parent.height
                                fillMode: Image.PreserveAspectFit
                                source: Theme.pictogram.path + "/" +Theme.pictogram.save
                            }
                        }

                        Text {
                            id: txtActive
                            text:  model.active ? global.connectionStateText(wifiNetworkModel.activeConnectionState) : ""
                            color: "grey"
                            visible: text !== "" && text !== undefined

                            Connections {
                                target: wifiNetworkModel
                                function onActiveConnectionStateChanged(){
                                    console.log("wifiNetworkModel.activeConnectionState: " + wifiNetworkModel.activeConnectionState)
                                }
                            }
                        }

                        Text {
                            visible: _debug
                            text: model.connection
                            font.pixelSize: 10
                            color: "grey"
                        }

                        Text {
                            visible: _debug
                            text: "Strength: " + model.strength
                            font.pixelSize: 10
                            color: "grey"
                        }

                        Text {
                            function maskPassword(psk) {
                                if(psk === undefined) return "PSK_UNDEFINED"
                                else if(psk === null) return "PSK_NULL"
                                else if(psk === "") return "PSK==\"\""
                                else if(psk.length < 2) return "PSK(**)"
                                else return psk[0] + "***" + psk[psk.length -1]
                            }
                            visible: _debug
                            text: "psk: " + maskPassword(model.psk)
                            font.pixelSize: 10
                            color: "grey"
                        }
                    }
                }

            }

            Rectangle {
                Layout.alignment: Qt.AlignRight

                color: Theme.color.background
                width: 60
                height: 60
                Image {
                    anchors {
                        fill: parent
                        margins: 2
                    }
                    sourceSize.width: parent.height
                    sourceSize.height: parent.height
                    fillMode: Image.PreserveAspectFit
                    source: Theme.pictogram.path + "/" +Theme.pictogram.prevGrey
                    rotation: 180
                }
            }
        }

        HorizontalSeparator {
            width: parent.width
        }
    }
}
