/*
    Copyright 2021, Prusa Research a.s.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.0
import PrusaComponents 1.0

Item {
    id: root

    property string pictogram
    property string text
    property real value: 0.0
    property int delay: 500
    property int interval: 2000
    signal triggered()
    signal aborted()

    parent: Overlay.overlay
    anchors.fill: parent

    Component.onCompleted: () => printer0.beep_button()

    SequentialAnimation {
        running: true

        PauseAnimation {
            duration: root.delay
        }
        NumberAnimation {
            target: root
            property: "value"
            from: 0.0
            to: 1.0
            duration: root.interval
        }
        onFinished: () => {
                        printer0.beep_button()
                        root.triggered()
                    }
        Component.onDestruction: () => {
                                     printer0.beep_button()
                                     if(running) root.aborted();
                                 }
    }

    Rectangle {
        anchors.fill: parent
        color: "black"
    }

    Item {
        width: 800
        height: 420


        Rectangle {
            id: ring
            width: 260
            height: 260
            anchors.centerIn: parent
            color: "transparent"
            radius: 130
            border.width: 8
            border.color: Theme.color.controlDarkerBackground
            ConicalGradient
            {
                visible: root.interval != 0
                source: ring
                anchors.fill: parent
                gradient: Gradient
                {
                    GradientStop { position: 0.00; color: Theme.color.highlight }
                    GradientStop { position: root.value; color: Theme.color.highlight }
                    GradientStop { position: root.value + 0.01; color: "transparent" }
                    GradientStop { position: 1.00; color: "transparent" }
                }
            }
        }

        Item {
            id: pictogram
            width: 160
            height: 160
            anchors.centerIn: parent

            Image {
                anchors.fill: parent
                sourceSize.width: parent.width
                sourceSize.height: parent.height
                fillMode: Image.PreserveAspectFit
                rotation: 0
                source: Theme.pictogram.path + "/" + root.pictogram
            }
        }

        Text {
            id: description
            anchors {
                horizontalCenter: parent.horizontalCenter
                top: pictogram.bottom
                topMargin: 80
            }

            text: root.text
        }
    }

    ImageVersionText {}
}
