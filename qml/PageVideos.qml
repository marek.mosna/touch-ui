/*
    Copyright 2020, Prusa Research a.s.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import PrusaComponents 1.0

PageQrCode {
    title: qsTr("Videos")
    pictogram: Theme.pictogram.videoSimple
    qrContent: "prusa3d.com/SL1guide/"
    text: qsTr("Scanning the QR code will take you to our YouTube playlist with videos about this device.\n\nAlternatively, use this link:") + "\n\n" + qrContent.replace("https://www.","")
}
