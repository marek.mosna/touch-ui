/*
Copyright 2021, Prusa Research a.s.

This file is part of touch-ui

touch-ui is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Controls 2.5
import PrusaComponents 1.0
Page {
    id: root
    // Page identifier, should be unique, but that is not enforced.
    property string name: "Page"

    property string pictogram: Theme.pictogram.cancelSimple

    // If disabled, the back button will be hidden.
    property bool back_button_enabled: true

    // Show "Cancel" instead of normal back button
    property bool cancel_instead_of_back: false

    property bool screensaver_enabled: true

    // Function to be called instead of view.pop() when the user presses the "back" button
    property var back_handler: root.StackView.view ? function(){ root.StackView.view.pop() } : undefined

    // Can the notifications expand over this page?
    property bool header_can_expand: true

    // Should the header be "rolled_up", meaning invisible on this page?
    property bool header_rollup: false

    // Color of background, this also applies to the background of the titlebar
    property var bgColor: Theme.color.background

    background: Rectangle {color: bgColor}

    implicitWidth: 800
    implicitHeight: 480
}
