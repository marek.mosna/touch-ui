/*
    Copyright 2019-2020, Prusa Research s.r.o.
    Copyright 2021, Prusa Research a.s.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtMultimedia 5.12
import Qt.labs.settings 1.0

import Native 1.0
import cz.prusa3d.updater 1.0
import cz.prusa3d.logs0 1.0
import cz.prusa3d.sl1.admin 1.0
import cz.prusa3d.sl1.printer0 1.0
import cz.prusa3d.wizard0 1.0
import cz.prusa3d.sl1.filemanager 1.0
import ErrorsSL1 1.0
import "3rdparty/qrcode-generator"
import QtQuick.VirtualKeyboard 2.3
import QtQuick.VirtualKeyboard.Settings 2.2
import PrusaComponents 1.0
import PrusaComponents.Delegates 1.0



ApplicationWindow {
    id: window
    visible: true

    width:  800
    height: 480

    title: qsTr("Prusa SL1 Touchscreen User Interface")
    objectName: "mainWindow"

    background: Rectangle {color: activePage.background.color }
    PrusaFont { id: prusaFont }

    Item {
        id: global
        property alias initialSettings: initialSettings
        property alias userSettings: userSettings
        property alias timeFormat: timeFormat

        function connectionStateText(networkState) {
            switch(networkState) {
            case WifiNetworkModel.Unknown: return qsTr("Unknown")
            case WifiNetworkModel.Activating:  return qsTr("Activating")
            case WifiNetworkModel.Activated:  return qsTr("Connected")
            case WifiNetworkModel.Deactivating: return qsTr("Deactivating")
            case WifiNetworkModel.Deactivated: return qsTr("Deactivated")
            default: return  qsTr("Unknown")
            }
        }

        property var pictureOfWifiSignalStrength: function(strength, pictogramSet) {
            var pictures = pictogramSet === undefined ? Theme.pictogram.wifiPictogramsLight : pictogramSet
            if(strength === 999) { return pictures[0]; }
            else if(strength >= 0 && strength < 20) {
                return pictures[0]
            }
            else if(strength >= 20 && strength < 50 ) {
                return pictures[1]
            }
            else if(strength >= 50 && strength < 80) {
                return pictures[2]
            }
            else return pictures[3]
        }

        function stringFormat(text, params) {
            Object.keys(params).forEach(function(key) {
                // TODO: This just supports a very limited set of formatting
                // If should be enough to cover what is required as of now.
                // Future extensions should use a library or at last a bit
                // smarter code.
                if(text.includes("%(" + key + ")s")) {
                    // This provides limited localization of values
                    // Device names names are looked up an translated
                    var value = params[key]
                    if(key.endsWith("__map_HardwareDeviceId")) {
                        switch(value) {
                        case 1000:
                            value = qsTr("UV LED temperature")
                            break
                        case 1001:
                            value = qsTr("ambient temperature")
                            break
                        case 1002:
                            value = qsTr("CPU temperature")
                            break
                        case 2000:
                            value = qsTr("UV LED fan")
                            break
                        case 2001:
                            value = qsTr("blower fan")
                            break
                        case 2002:
                            value = qsTr("rear fan")
                            break
                        default:
                            value = qsTr("unknown device")
                        }
                    }
                    text = text.replace("%(" + key + ")s", value)
                } else if(text.includes("%(" + key + ")d")) {
                    text = text.replace("%(" + key + ")d", params[key])
                } else if(text.includes("%(" + key + ").1f")) {
                    text = text.replace("%(" + key + ").1f", params[key].toFixed(1))
                } else if(text.includes("%(" + key + ").2f")) {
                    text = text.replace("%(" + key + ").2f", params[key].toFixed(2))
                }
            });
            return text
        }

        // Having this globally accessible enables having the same "view" from
        // multiple pages and avoids synchronization problems
        Settings {
            id: initialSettings
            fileName: configPath
            category: "initial"
            property bool language: false
            property bool timezone: false
        }

        // Contains general user settings
        Settings {
            id: userSettings
            fileName: configPath
            category: "user"
            property int time_format: 0
        }

        QtObject {
            id: timeFormat
            readonly property int format_native: 0
            readonly property int format_12: 1
            readonly property int format_24: 2
        }

        function localizeTime(t) {
            let format = Locale.ShortFormat
            switch(userSettings.time_format) {
            case timeFormat.format_12:
                if(Translator.language === Translator.Czech) { // lowercase abreviation in czech language
                    format = "h:mm ap"
                }
                else format = "h:mm AP"
                break;
            case timeFormat.format_24:
                format = "hh:mm"
                break;
            case timeFormat.format_native:
            default:
            }
            return t.toLocaleTimeString(Qt.locale(LocaleSetting.locale), format)
        }

        function localizeDate(t) {
            return t.toLocaleDateString(Qt.locale(LocaleSetting.locale), Locale.ShortFormat)
        }

        function localizeDateTime(t) {
            return localizeDate(t) + " " + localizeTime(t)
        }

        function soundError() {
            printer0.beep(
                        1800,
                        800,
                        ()=>{},
                        ()=>{}
                        )
        }
    }

    Loader {
        id: themeLoader

        Component {
            id: prusaTheme
            PrusaTheme { }
        }

        Component {
            id: medicalTheme
            MedicalTheme { }
        }

        sourceComponent: printer0.printer_model == Printer0.M1 ? medicalTheme : prusaTheme
        Component.onCompleted: () => Theme.current = Qt.binding(() => {return themeLoader.item})
    }

    Connections {
        target: SecretPrusa
        function onRequest_pendingChanged() {
            if(SecretPrusa.request_pending) {
                view.push("PageAskForPassword.qml",
                          {
                              "network_name": SecretPrusa.getConnectionName(),
                              okAction: function(network_name, password, saved_password) {
                                  var psk = password
                                  if(psk === "") psk = saved_password

                                  SecretPrusa.respond(psk)
                                  view.pop()
                              },
                              cancelAction: function(network_name, password, saved_password) {
                                  console.debug("Cancel action")
                                  SecretPrusa.cancel()
                                  Wifi.deactivateCurrentConnection()
                                  view.pop()
                              }
                          }
                          )
                console.log("SecretPrusa.connection_name: " + SecretPrusa.getConnectionName())
            }
        }
    }


    property int screenshot_counter: 0
    function screenshot() {
        var destination_directory = Filemanager.current_media_path
        if(destination_directory === "")  return;
        var escaped_pagename = activePage.title.replace(/[ :./!@#$%^&*()?+\[\]{}\,;<>]/g,"_");
        var screenshot_path = destination_directory + "/screenshot_" + escaped_pagename + "_" + Qt.formatDateTime(new Date(), "yyyy-MM-dd_hh-mm") + "_" + String(screenshot_counter) + ".png"
        nativeFunctions.screenshotWindow(
            screenshot_path,
            () => {
                returnToOriginalColor.start()
                console.log("Screenshot saved: " + screenshot_path)
            },
            (e) => {
                console.log("No path to save screenshot")
            }
            )
    }

    Timer {
        id: returnToOriginalColor
        running: false
        repeat: false
        interval: 200
    }

    Binding {
        when: returnToOriginalColor.running
        target: window
        property: "background.color"
        value: "grey"
    }

    header: WindowHeader {
        visible: ! activePage.header_rollup
        height: 60
        leftPadding: 30
        rightPadding: 30
        title: activePage.title
        pictogram: activePage.pictogram
        clock:  global.localizeTime(timedate.time)
        ethernetConnected: ethernet.activeConnectionState === Ethernet.Activated
        wifiConnected: wifiNetworkModel.activeConnectionState == WifiNetworkModel.Activated
        wifiStrength: (wifiNetworkModel.activeConnectionSignalStrength * 3) / 100
        networkWorking: ethernet.activeConnectionState === Ethernet.Activating ||
                        ethernet.activeConnectionState === Ethernet.Deactivating ||
                        wifiNetworkModel.activeConnectionState === WifiNetworkModel.Activating ||
                        wifiNetworkModel.activeConnectionState === WifiNetworkModel.Deactivating
        backButtonVisible: activePage.back_button_enabled && ! activePage.cancel_instead_of_back
        cancelButtonVisible: activePage.back_button_enabled &&  activePage.cancel_instead_of_back
        closeButtonVisible: false
        messageCount: notificationModel.count
        bgColor: window.background.color
        onBackClicked: () => activePage.back_handler()
        onCancelClicked: () => activePage.back_handler()
        onMessageClicked: () => { if(activePage.header_can_expand) notificationArea.toggle() }
        onNetworkClicked: () => {if( ! (view.currentItem instanceof PageSettingsNetwork)) view.push("PageSettingsNetwork.qml")}
        onClockClicked: () => {if( ! (view.currentItem instanceof PageTimeSettings)) view.push("PageTimeSettings.qml")}
        onTitleLongPressed: () => screenshot()
        onTitleDoubleClicked: () => { if(_debug) debugSection.toggle() }
        onCloseClicked: () => notificationArea.close()
        onSwipeDown: () => { if(activePage.header_can_expand) notificationArea.open() }
        onMessageCountChanged: () => notificationArea.peek()

        states: [
            State {
                when: notificationArea.isOpen
                name: "notification"
                PropertyChanges {
                    target: window.header
                    title: qsTr("Notifications")
                    pictogram: Theme.pictogram.message
                    backButtonVisible: false
                    cancelButtonVisible: false
                    closeButtonVisible: true
                }
            }
        ]

        ExpandingSection {
            id: notificationArea
            width: parent.width
            anchors.top: parent.bottom
            PageNotificationList {
                width: parent.width
                onNotificationClicked: () => parent.close()
            }
        }

        DebugSection {
            id: debugSection
            width: parent.width
            anchors.top: parent.bottom
        }
    }

    /** Active page can be on the errorStack - those are on top, or if it's empty, on the view */
    readonly property var activePage: languageSelectorLoader.item ? languageSelectorLoader.item.currentItem : (errorStack.depth > 1 ? errorStack.currentItem :  view.currentItem)

    /** Some pages need to keep their state even if they are "out of focus" */
    Item {
        id: statePages
        property alias idle: idle
        visible: false

        PageStateIdle {
            id: idle
        }
    }

    Component {
        id: pageInitializing
        PageWait {
            title: qsTr("Initializing...")
            text: qsTr("The printer is initializing, please wait ...")
            screensaver_enabled: false
            header_can_expand: false
        }
    }

    Component {
        id: pageMcUpdate
        PageWait {
            title: qsTr("MC Update")
            text: qsTr("The Motion Controller firmware is being updated.\n\nPlease wait...")
            screensaver_enabled: false
            header_can_expand: false
        }
    }

    /** It's theoretically possible to overlap the state pages with anything(like dialogs), because of that
         the whole stack should be checked if the page is not already there before replacing the whole
        stack content (if the state has changed, whatever else is there is no longer relevant). */
    property var stateSwitch: function(state) {
        switch(state) {
        case Printer0.INITIALIZING:
            view.replace(null, pageInitializing)
            break;
        case Printer0.IDLE:
            if(view.find(function(item, index){return view.currentItem instanceof PageStateIdle}) === null) view.replace(null, statePages.idle)
            showInitialUserDialogs()
            break;
        case Printer0.WIZARD:   // Do not cache wizards !
            if(view.find(function(item, index){return view.currentItem instanceof PageStateWizard}) === null) view.replace(null, "PageStateWizard.qml")
            showInitialUserDialogs()
            break;
        case Printer0.PRINTING:// Do not cache Print !
            if(view.find(function(item, index){return view.currentItem instanceof PagePrint}) === null) view.replace(null, "PagePrint.qml")
            break;
        case Printer0.UPDATE:
            if(view.find(function(item, index){return view.currentItem instanceof PageUpdatingFirmware}) === null) view.replace(null, "PageUpdatingFirmware.qml")
            break;
        case Printer0.ADMIN:
            //if(view.find(function(item, index){return view.currentItem instanceof PageAdminAPI}) === null) view.replace(null, "PageAdminAPI.qml")
            view.replace(null, "PageAdminAPI.qml")
            break;
        case Printer0.EXCEPTION:
            var exception = printer0.printer_exception
            var _code = decode_error_code(exception)
            view.replace(null, "PageException.qml", {code: _code, params: exception, bgColor: Theme.color.errorBackground})
            break;
        case Printer0.UPDATE_MC:
            view.replace(null, pageMcUpdate)
            break;
        case Printer0.OVERHEATING:
            view.replace(null, "PageCoolingDown.qml")
            break
        default:
            console.warn("No page to use for unknown Printer0.state:", state)
        }

        // Clean the potential leftover inhibitor on state change
        // Note: will probably fail when called for the first time because the backend
        //          has not yet started(INITIALIZING state is the default), that's ok.
        if(state !== Printer0.UPDATE)   {
            printer0.remove_oneclick_inhibitor("touch-ui-firmware-update")
        }
    }

    function decode_error_code(exception) {
        var _code
        if(typeof exception["code"] === "number") {
            _code = exception["code"]
        }
        else if(typeof exception["code"] === "string"){
            _code = ErrorsSL1.fromStringCode(exception["code"])
        }
        else {
            console.warn("push_error: error code is some nonsense: ", JSON.stringify(exception))
        }
        return _code
    }

    /** Push an error to the errorStack
      @param exception dictionary {code:"#10500", params:{} }
     */
    function push_error(exception, backgroundColor = Theme.color.errorBackground) {
        var _code = decode_error_code(exception)

        // De-duplicate errors
        if(errorStack.currentItem.code === _code) {
            console.debug("Deduplicated error: ", JSON.stringify(exception))
            return;
        }

        console.log("Trying to push an error: ", JSON.stringify(exception))
        if(exception["params"] === undefined) {
            exception["params"] = {}
        }
        if( _code !== ErrorsSL1.NONE ) {
            errorStack.push("PageError.qml", {code: _code, params: exception, bgColor: backgroundColor})
        }
    }

    // Warning page should look the same as an exception, but with different color
    function push_warning(exception, backgroundColor = "orange") { return push_error(exception, backgroundColor) }

    /* Global function, needed to keep consistency with Printer0.UPDATE state
        - the PageUpdatingFirmware needs to end up on the main stack.

        Params will be passed to the PageUpdatingFirmware.
    */
    function run_update(params) {
        if(view.find(function(item, index){return view.currentItem instanceof PageUpdatingFirmware}) === null) {
            if(params !== undefined && params["replace"] === true) {
                view.replace("PageUpdatingFirmware.qml", params)
            }
            else {
                view.push("PageUpdatingFirmware.qml", params)
            }
        }
    }

    Connections {
        target: printer0

        function onStateChanged(state) {
            console.log("Printer state changed to: ", state)
            stateSwitch(state)
        }

        function onException(exception) {
            push_error(exception)
        }

        function onPower_switch_stateChanged(power_switch_state) {
            function getOverlay() {
                if(printer0.power_switch_state) {
                    switch(printer0.state) {
                    case Printer0.IDLE: // fall
                    case Printer0.EXCEPTION: // fall
                    case Printer0.ADMIN:
                        return powerOffOverlay
                    case Printer0.WIZARD:
                        return cancelOverlay
                    case Printer0.PRINTING:
                        return printCancelOverlay
                    default:
                        printer0.beep_button()
                        return cannotPowerOffOverlay
                    }
                }
                else return null
            }
            if(printer0.power_switch_state) {
                powerButtonOverlayLoader.sourceComponent = getOverlay()
            }
            else powerButtonOverlayLoader.sourceComponent = null
        }

        function onDbusError(errdata) {
            if("name" in errdata && errdata["name"] === "org.freedesktop.DBus.Error.ServiceUnknown") {
                return; // Do not report an error, backend may start later
            }
            push_error(errdata)
        }
    }


    Component.onCompleted: () => stateSwitch(printer0.state)


    Loader {
        id: powerButtonOverlayLoader
        anchors.fill: parent
        z: 100
    }

    Component {
        id: powerOffOverlay
        PowerButtonOverlay {
            pictogram: Theme.pictogram.powerOff
            text: qsTr("Turn Off?")
            onTriggered: () => {
                             console.log("powerOff triggered!")
                             printer0.poweroff(true, false, () => console.log("powering off."), (e) => console.warn("power off error: ", e))
                         }
            onAborted: () => console.log("powerOff aborted!")
        }
    }

    Component {
        id: cancelOverlay
        PowerButtonOverlay {
            pictogram: Theme.pictogram.cancel
            text: qsTr("Cancel?")
            onTriggered: () => {
                             if(printer0.state === Printer0.WIZARD && wizard0.cancelable) {
                                 wizard0.cancel()
                                 console.log("Canceled by power button.")
                             }
                         }
            onAborted: () => console.log("Cancel aborted!")
        }
    }

    Component {
        id: printCancelOverlay
        PowerButtonOverlay {
            pictogram: Theme.pictogram.cancel
            text: qsTr("Cancel the current print job?")
            onTriggered: () => {
                             if(printer0.state === Printer0.PRINTING && printer0.current_exposure !== "") {
                                 try {
                                     printer0.current_exposure_object.cancel(
                                         () => {console.log("Print job canceled by power button.")},
                                         (e) => {console.log("Could not cancel the current print job.")}
                                         )
                                 }
                                 catch(e) {
                                     console.log("Print job cancel failed. current exposure:", printer0.current_exposure)
                                 }
                             }
                         }
            onAborted: () => console.log("Print job cancel aborted!")
        }
    }

    Component {
        id: cannotPowerOffOverlay
        PowerButtonOverlay {
            interval: 0
            pictogram: Theme.pictogram.nok
            text: qsTr("Printer should not be turned off in this state.\nFinish or cancel the current action and try again.")
            onTriggered: () => printer0.beep_button()
            onAborted: () => printer0.beep_button()

        }
    }

    Component {
        id: deleteAllFilesDialog
        PageYesNoSimple {
            id: deleteAllFilesDialogRoot
            title: qsTr("DEPRECATED PROJECTS")
            text: qsTr("Some incompatible projects were found, you can download them at \n\nhttp://%1/old-projects\n\nDo you want to remove them?").arg(getIP())
            onYes: () => {
                       Filemanager.remove_all_deprecated_files(()=>{}, (err) => {push_error(Filemanager.sync_last_exception())});
                       Filemanager.remove_downloaded_deprecated_files(()=>{},(err) => {push_error(Filemanager.sync_last_exception())});
                       popDialog()
                   }
            onNo: () => popDialog()
            function getIP() {
                if(ethernet.ipAddress !== "") {
                    return ethernet.ipAddress
                }
                else if(wifiNetworkModel.ipAddress !== "") {
                    return wifiNetworkModel.ipAddress
                }
                else return qsTr("<printer IP>")
            }
        }
    }

    Component {
        id: deleteOldFilesDialog
        PageYesNoSimple {
            id: deleteOldFilesDialogRoot
            title: qsTr("DEPRECATED PROJECTS")
            text: qsTr("Some incompatible file were found, you can view them at http://%1/old-projects.\n\nWould you like to remove them?").arg(getIP())
            onYes: () => {
                       Filemanager.remove_downloaded_deprecated_files(()=>{}, (err) => {push_error(Filemanager.sync_last_exception())});
                       popDialog();
                   }
            onNo: () => popDialog()
            function getIP() {
                if(ethernet.ipAddress !== "") {
                    return ethernet.ipAddress
                }
                else if(wifiNetworkModel.ipAddress !== "") {
                    return wifiNetworkModel.ipAddress
                }
                else return qsTr("<printer IP>")
            }
        }
    }

    property bool initalDialogsShown: false
    function showInitialUserDialogs() {
        if(initalDialogsShown) return; // only once
        initalDialogsShown = true
        // check if http://<ip>/old-projects
        if (Filemanager.sync_len_download_deprecated_files() > 0){
            errorStack.push(deleteOldFilesDialog);
        }

        // check if not usb source has deprecated projects
        var deprecated_files = Filemanager.sync_has_deprecated();
        if(deprecated_files.length > 0){
            console.log("deprecated files in " + JSON.stringify(deprecated_files))
            Filemanager.move_deprecated_files(()=>{}, (err) => {push_error(Filemanager.sync_last_exception())})
            errorStack.push(deleteAllFilesDialog);
        }
    }

    property var activeFocusItemPos: ({x:0, y:0})
    onActiveFocusItemChanged: () => {
        try {
            activeFocusItemPos = activeFocusItem.mapToItem(basicBottomPage, 0,0)
        }
        catch(e) {

        }
    }

    Page {
        id: basicBottomPage
        objectName: "basicBottomPage"
        anchors.fill: parent
        background: Rectangle { color: "transparent"} // Background


        // Keyboard
        InputPanel {
            id: inputPanel
            z: 99
            x: 0
            y: parent.height
            width: parent.width
            visible: false

            states: State {
                name: "visible"
                when: inputPanel.active
                PropertyChanges {
                    target: inputPanel
                    y: parent.height - inputPanel.height
                }

                PropertyChanges {
                    target: flickable
                    contentY: {
                        try {
                            var itemY = basicBottomPage.mapFromItem(activeFocusItem, 0,0).y
                            var targetY = inputPanel.height
                            return contentY + itemY - targetY + activeFocusItem.height + 80
                        }
                        catch(e) {
                            return contentY
                        }
                    }
                }
            }
            transitions: [
                Transition {
                    from: ""
                    to: "visible"
                    reversible: true
                    ParallelAnimation {
                        NumberAnimation {
                            properties: "y"
                            duration: 250
                            easing.type: Easing.InOutQuad
                        }
                        NumberAnimation {
                            properties: "contentY"
                            duration: 250
                            easing.type: Easing.InOutQuad
                        }
                    }
                    onRunningChanged: () => {
                        if(running) {
                            inputPanel.visible = true
                        }
                        else {
                            if(inputPanel.state !== "visible") {
                                inputPanel.visible = false
                            }
                        }
                    }
                }
            ]
        }

        Component.onCompleted: () => {
            VirtualKeyboardSettings.styleName = "prusa"
        }

        Flickable {
            id: flickable
            height: 420
            anchors {
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }


            contentHeight: height + inputPanel.height
            contentWidth: width
            interactive: false

            StackView {
                id: view
                name: "TopLevelStackView"
                initialItem:  statePages.idle
                height: 420
                width: basicBottomPage.width
                anchors {
                    top: parent.top
                    left: parent.left
                }
            }

            /* Special stack for error pages and similar, should always be on top */
            StackView {
                id: errorStack
                name: "ErrorStackView"
                height: 420
                width: basicBottomPage.width
                anchors {
                    top: parent.top
                    left: parent.left
                }
                visible:  errorStack.depth > 1
                initialItem: PrusaPage {}
            }

            // Will load the stackview only if language change is requested(above
            // everything else, even errors), otherwise it will not be allocated.
            Loader {
                id: languageSelectorLoader
                sourceComponent: ((Translator.language === Translator.Default && ! global.initialSettings.language) || ! global.initialSettings.timezone) && printer0.printerReady ?  languageStackComponent : null
                Component {
                    id: languageStackComponent
                    StackView {
                        id: languageStack
                        name: "LanguageStackView"
                        height: 420
                        width: basicBottomPage.width
                        anchors {
                            top: parent.top
                            left: parent.left
                        }

                        Component.onCompleted: () => {
                                                   if( ! global.initialSettings.timezone) {
                                                       languageStack.push("PageSetTimezone.qml", {back_button_enabled: false})
                                                   }

                                                   if( Translator.language === Translator.Default ) {
                                                       languageStack.push("PageLanguage.qml", {back_button_enabled: false})
                                                   }
                                               }
                    }
                }
            }
        }
    }

    ScreenSaver {
        id: screenSaver
        anchors.fill: parent
        enabled: activePage !== undefined ? activePage.screensaver_enabled : false
        parent: window.overlay
    }
}
