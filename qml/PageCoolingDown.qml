/*
    Copyright 2019-2020, Prusa Research s.r.o.
    Copyright 2021, Prusa Research a.s.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import PrusaComponents 1.0

PrusaPage {
    id: root
    title: qsTr("UV LED OVERHEAT!")
    name: "overheat"
    pictogram: Theme.pictogram.sandclock
    back_button_enabled: false
    screensaver_enabled: false
    property var uv_led_temp

    Binding on uv_led_temp { when: printer0; value: printer0.uv_led_temp}

    PageWait {
            title: qsTr("UV LED OVERHEAT!")
            text: {
                var uvTemp = root.uv_led_temp.toFixed(1);
                return qsTr("Cooling down") + "\n\n" + qsTr("Temperature is %1 C").arg(uvTemp);
            }
    }

}
