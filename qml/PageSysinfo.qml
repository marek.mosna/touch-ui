/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtQml.Models 2.12
import Native 1.0

import PrusaComponents 1.0
import cz.prusa3d.sl1.printer0 1.0

PrusaPage {
    id: root
    title: qsTr("System Information")
    name: "sysinfo"
    pictogram: Theme.pictogram.systemInfo

    property int textSize: 24

    StackView.onActivated: ()=>{
        printer0.enable_resin_sensor(true, false, false);
    }

    StackView.onDeactivated: ()=>{
        printer0.enable_resin_sensor(false, false, false);
    }

    ObjectModel {
        id: systemInfoModel
        RowLayout {
            height: 30
            Item {width: 30; height: 1}
            Text {
                Layout.alignment: Qt.AlignBottom
                color: Theme.color.highlight
                text: qsTr("System") + ":"
            }
        }

        DelegateSysInfoItem {
            text: qsTr("OS Image Version")
            value: printer0.system_version
        }

        DelegateSysInfoItem {
            text: qsTr("Printer Model")
            value: {
                       switch(printer0.printer_model) {
                           case Printer0.NONE: return qsTr("None", "Printer model is not known/can't be determined")
                           case Printer0.SL1: return "SL1"
                           case Printer0.SL1S: return "SL1S"
                           case Printer0.M1: return "M1"
                           default: return qsTr("Newer than SL1S", "Printer model is unknown, but better than SL1S")
                       }
                   }
        }

        DelegateSysInfoItem {
            text: qsTr("A64 Controller SN")
            value: printer0.serial_number
        }

        DelegateSysInfoItem {
            text: qsTr("API Key / Printer Password")
            value: printer0.api_key
        }

        RowLayout {
            height: 30

            Item {width: 30; height: 1}

            Text {
                Layout.alignment: Qt.AlignBottom
                color: Theme.color.highlight
                text: qsTr("Other Components") + ":"
            }
        }

        DelegateSysInfoItem {
            text: qsTr("Motion Controller SN")
            value: printer0.controller_serial
        }

        DelegateSysInfoItem {
            text: qsTr("Motion Controller SW Version")
            value: printer0.controller_sw_version
        }

        DelegateSysInfoItem {
            text: qsTr("Motion Controller HW Revision")
            value: printer0.controller_revision
        }

        DelegateSysInfoItem {
            text: qsTr("Booster Board SN")
            value: printer0.booster_serial
            visible: printer0.printer_model == Printer0.SL1S || printer0.printer_model == Printer0.M1
        }

        DelegateSysInfoItem {
            text: qsTr("Exposure display SN")
            value: printer0.expo_panel_serial
            visible: printer0.printer_model == Printer0.SL1S || printer0.printer_model == Printer0.M1
        }

        DelegateSysInfoItem {
            text: qsTr("GUI Version")
            value: AppInfo.version
        }

        RowLayout {
            height: 30

            Item {width: 30; height: 1}

            Text {
                Layout.alignment: Qt.AlignBottom
                color: Theme.color.highlight
                text: qsTr("Hardware State") + ":"
            }
        }

        DelegateSysInfoItem {
            text: qsTr("Network State")
            value: (ethernet.ipAddress !== "" || wifiNetworkModel.ipAddress !== "") ? qsTr("Online") : qsTr("Offline")
        }

        DelegateSysInfoItem {
            text: qsTr("Ethernet IP Address")
            value :ethernet.ipAddress !== "" ? ethernet.ipAddress : qsTr("N/A")
        }

        DelegateSysInfoItem {
            text: qsTr("Wifi IP Address")
            value: wifiNetworkModel.ipAddress !== "" ? wifiNetworkModel.ipAddress : qsTr("N/A")
        }

        DelegateSysInfoItem {
            text: qsTr("Time of Fast Tilt")
            value: printerConfig0.tiltFastTime.toFixed(1) + " " + qsTr("seconds")
        }

        DelegateSysInfoItem {
            text: qsTr("Time of Slow Tilt")
            value: printerConfig0.tiltSlowTime.toFixed(1) + " " + qsTr("seconds")
        }

        DelegateSysInfoItem {
            text: qsTr("Time of High Viscosity Tilt:")
            value: printerConfig0.tiltHighViscosityTime.toFixed(1) + " " + qsTr("seconds")
        }

        DelegateSysInfoItem {
            text: qsTr("Resin Sensor State")
            value: printer0.resin_sensor_state ? qsTr("Triggered") : qsTr("Not triggered")
        }

        DelegateSysInfoItem {
            text: qsTr("Cover State")
            value: printer0.cover_state ? qsTr("Closed") : qsTr("Open")
        }

        DelegateSysInfoItem {
            text: qsTr("CPU Temperature")
            value: printer0.cpu_temp.toFixed(1) + " " + qsTr("°C")
        }

        DelegateSysInfoItem {
            text: qsTr("UV LED Temperature")
            value: printer0.uv_led_temp.toFixed(1) + " " + qsTr("°C")
        }

        DelegateSysInfoItem {
            text: qsTr("Ambient Temperature")
            value: printer0.ambient_temp.toFixed(1) + " " + qsTr("°C")
        }

        DelegateSysInfoItem {
            text: qsTr("UV LED Fan")
            value: {
                if(printer0.fanUvLedError) {
                   return qsTr("Fan Error!")
                }
                else return printer0.fanUvLedRpm + " " + qsTr("RPM")
            }
        }

        DelegateSysInfoItem {
            text: qsTr("Blower Fan")
            value: {
                if(printer0.fanBlowerError) {
                    return qsTr("Fan Error!")
                }
                else return printer0.fanBlowerRpm + " " + qsTr("RPM")
            }
        }

        DelegateSysInfoItem {
            text: qsTr("Rear Fan")
            value: {
                if(printer0.fanRearError) {
                    return qsTr("Fan Error!")
                }
                else return printer0.fanRearRpm + " " + qsTr("RPM")
            }
        }

         DelegateSysInfoItem {
            text: qsTr("UV LED")
            value: {
                var leds = printer0.leds;
                if(leds){
                    return qsTr(
                        leds["led0_voltage_volt"].toFixed(1) + " , " +
                        leds["led1_voltage_volt"].toFixed(1) + " , " +
                        leds["led2_voltage_volt"].toFixed(1) + " V"
                    )
                }else return qsTr("N/A");
            }
        }

        DelegateSysInfoItem {
            text: qsTr("Power Supply Voltage")
            value: printer0.leds["led3_voltage_volt"].toFixed(1) + " " + qsTr("V")
        }

        RowLayout {
            height: 30

            Item {width: 30; height: 1}

            Text {
                Layout.alignment: Qt.AlignBottom
                color: Theme.color.highlight
                text: qsTr("Statistics") + ":"
            }
        }

        DelegateSysInfoItem {
            text: qsTr("UV LED Time Counter")
            value: Math.floor(printer0.uv_statistics["uv_stat0"]/86400) + qsTr("d") + " " + Math.floor(printer0.uv_statistics["uv_stat0"] % 86400 / 3600) + qsTr("h") + " " + Math.round(printer0.uv_statistics["uv_stat0"] % 86400 % 3600 / 60) + qsTr("m")
        }

        DelegateSysInfoItem {
            text: qsTr("Print Display Time Counter")
            value: Math.floor(printer0.uv_statistics["uv_stat1"]/86400) + qsTr("d") + " " + Math.floor(printer0.uv_statistics["uv_stat1"] % 86400 / 3600) + qsTr("h") + " " + Math.round(printer0.uv_statistics["uv_stat1"] % 86400 % 3600 / 60) + qsTr("m")
        }

        DelegateSysInfoItem {
            text: qsTr("Started Projects")
            value: printer0.statistics["started_projects"] || qsTr("N/A")
        }

        DelegateSysInfoItem {
            text: qsTr("Finished Projects")
            value: printer0.statistics["finished_projects"] || qsTr("N/A")
        }

        DelegateSysInfoItem {
            text: qsTr("Total Layers Printed")
            value: printer0.statistics["layers"] || qsTr("N/A")
        }

        DelegateSysInfoItem {
            text: qsTr("Total Print Time")
            value: printer0.statistics["total_seconds"] !== undefined ? Math.floor(printer0.statistics["total_seconds"].toFixed(1)/86400) + qsTr("d") + " " + Math.floor(printer0.statistics["total_seconds"].toFixed(1) % 86400 / 3600) + qsTr("h") + " " + Math.round(printer0.statistics["total_seconds"].toFixed(1) % 86400 % 3600 / 60) + qsTr("m") : qsTr("N/A")
        }

        DelegateSysInfoItem {
            text: qsTr("Total Resin Consumed")
            value: printer0.statistics["total_resin"] !== undefined ? printer0.statistics["total_resin"].toFixed(2) + " " + qsTr("ml") : qsTr("N/A")
        }

        RowLayout {
            width: 760

            Item {width: 25; height: 1}

            TextButton {
                name: "Licenses"
                text: qsTr("Show software packages & licenses")
                Layout.fillWidth: true
                onClicked: ()=>{
                    view.push("PageSoftwareLicenses.qml")
                }
             }
        }
    }


    ListView {
        id: sysInfoView
        height: 420
        interactive: true
        clip: true
        anchors {
          bottom: parent.bottom
          left: parent.left
          right: parent.right
        }
        ScrollBar.vertical: ScrollBar {
            active: true
            policy: ScrollBar.AlwaysOn
        }
        spacing: 5
        model: systemInfoModel
    }
}
