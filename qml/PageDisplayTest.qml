/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import cz.prusa3d.sl1.printer0 1.0
import PrusaComponents 1.0

/*
 Depends on:
    StackView view
*/
PrusaPage {
    id: root
    title: qsTr("Display Test")
    name: "displaytest"
    pictogram: Theme.pictogram.yes
    screensaver_enabled: false
    header_rollup: true

    StackView.onActivated: () => {
        console.log("DisplayTest: showing color " + colorList.currentItem.color)
    }

    SwipeView {
        id: colorList
        // This makes it overlap the area normally ocupied by the page title
        height: 480
        width: parent.width
        anchors.bottom: parent.bottom

        orientation: ListView.Horizontal
        interactive: false
        Rectangle { width: colorList.width; height: colorList.height; color: "red"}
        Rectangle { width: colorList.width; height: colorList.height; color: "green"}
        Rectangle { width: colorList.width; height: colorList.height; color: "blue"}
        Rectangle { width: colorList.width; height: colorList.height; color: "black"}
        Rectangle { width: colorList.width; height: colorList.height; color: "white"}
        onCurrentItemChanged: () => {
            console.log("DisplayTest: showing color " + currentItem.color)
            background.color = currentItem.color
        }
    }

    // MouseArea intentionally overlaps the ListView
    MouseArea {
        height: 480
        width: parent.width
        anchors.bottom: parent.bottom
        onClicked: () => {
            printer0.beep_button()

            if(colorList.currentIndex == colorList.count - 1) {
                view.pop()
            }
            colorList.incrementCurrentIndex()
        }
    }
}
