/*
    Copyright 2021, Prusa Development a.s.

    This file is part of touch-ui

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5

import cz.prusa3d.sl1.printer0 1.0
import cz.prusa3d.wizard0 1.0
import cz.prusa3d.wizardcheckmodel 1.0
import PrusaComponents 1.0

PageBasicWizard {
    id: root
    title: qsTr("Printer Calibration")
    essentialCalibration: true
    overloadStateMapping: (wizardState) => {
        switch(wizardState) {
        case Wizard0.PREPARE_CALIBRATION_INSERT_PLATFORM_TANK: return prepare_insert_platform
        case Wizard0.PREPARE_CALIBRATION_TILT_ALIGN: return pre_tank_alignment
        case Wizard0.LEVEL_TILT: return tank_alignment
        case Wizard0.PREPARE_CALIBRATION_PLATFORM_ALIGN: return clean_bed
        case Wizard0.PREPARE_CALIBRATION_FINISH: return prepare_calibration_finish
        case Wizard0.SHOW_RESULTS: // fall-through
        case Wizard0.DONE: return wizard_done
        default: return null// Otherwise use BasicWizard default
        }
    }

    Component { // 1/11
        id: prepare_insert_platform
        PageConfirm {
            name: "CalibrationInsertPlatform"
            text: qsTr("If the platform is not yet inserted, insert it according to the picture at 0° degrees angle and secure it with the black knob.")
            onConfirmed: ()=>{ wizard0.prepareCalibrationPlatformTankDone() }
            image_source: "calibration-tighten_knob.jpg"
        }
    }

    Component { // 2,3,4 / 11
        id: pre_tank_alignment
        PageConfirm {
            id: pre_tank_alignment_root
            name: "CalibrationLoosenPlatform"
            text: {
                text = qsTr("Loosen the small screw on the cantilever with an allen key. Be careful not to unscrew it completely.")
                switch(printer0.printer_model) {
                    case Printer0.SL1: text += "\n\n" + qsTr("Some SL1 printers may have two screws - see the handbook for more information.")
                }
                return text
            }
            onConfirmed: ()=>{ state = "second_screen" }
            image_source: "calibration-loosen_cantilever.jpg"

            // This is a way to show multiple screens(one after the other) for one state.
            states: [
                State {
                    name: "second_screen"
                    PropertyChanges {
                        target: pre_tank_alignment_root
                        text: qsTr("Unscrew the tank, rotate it by 90° and place it flat across the tilt bed. Remove the tank screws completely.")
                        image_source: "calibration-place_bed.jpg"
                        onConfirmed: ()=>{ state = "third_screen" }
                    }
                },
                State {
                    name: "third_screen"
                    PropertyChanges {
                        target: pre_tank_alignment_root
                        text: qsTr("In the next step, move the tilt bed up/down until it is in direct contact with the resin tank. The tilt bed and tank have to be aligned in a perfect line.")
                        image_source: "calibration-proper_aligment.jpg"
                        onConfirmed: ()=>{ wizard0.prepareCalibrationTiltAlignDone() }
                    }
                }
            ]
        }
    }

    Component { // 5/11
        id: tank_alignment
        PageCalibrationTilt {
            name: "AlignTilt"
            top_text: qsTr("Set the tilt bed against the resin tank")
            onConfirmed: ()=>{ wizard0.tiltCalibrationDone() }
        }
    }

    Component {// 6,7,8,9/11
        id: clean_bed
        PageConfirm {
            id: clean_bed_root
            name: "CleanBed"
            text: qsTr("Make sure that the platfom, tank and tilt bed are PERFECTLY clean.")
                  +"\n\n"
                  + qsTr("The image is for illustration purposes only.")
            image_source: "calibration-clean.jpg"
            onConfirmed: ()=>{ state = "7_11" }

            states: [
                State {
                    name: "7_11"
                    PropertyChanges {
                        target: clean_bed_root
                        text: qsTr("Return the tank to the original position and secure it with tank screws. Make sure that you tighten both screws evenly and with the same amount of force.")
                        image_source: "tighten_screws.jpg"
                        onConfirmed: ()=>{ state = "8_11" }
                    }
                },
                State {
                    name: "8_11"
                    PropertyChanges {
                        target: clean_bed_root
                        text: qsTr("Check whether the platform is properly secured with the black knob(hold it in place and tighten the knob if needed).")
                              + "\n\n"
                              + qsTr("Do not rotate the platform. It should be positioned according to the picture.")
                        image_source: "calibration-tighten_knob.jpg"
                        onConfirmed: ()=>{
                            if(printerConfig0.coverCheck ? printer0.cover_state === true : true) {
                                wizard0.prepareCalibrationPlatformAlignDone()
                            } else { state = "close_cover"}
                        }

                    }
                },
                State {
                    name: "close_cover" // 9_11
                    PropertyChanges {
                        target: clean_bed_root
                        text: qsTr("Close the cover.")
                        image_source: "close_cover.jpg"
                        enabled: printerConfig0.coverCheck ? printer0.cover_state == true : true
                        onConfirmed: ()=>{ wizard0.prepareCalibrationPlatformAlignDone() }
                    }
                }
            ]
        }
    }

    Component { // 10/11
        id: prepare_calibration_finish
        PageConfirm {
            id: prepare_calibration_finish_root
            name: "CalibrationAdjustPlatform"
            text: qsTr("Adjust the platform so it is aligned with the exposition display.")
                  + "\n\n"
                  + qsTr("Front edges of the platform and exposition display need to be parallel.")
            image_source: "calibration-align_platform.jpg"
            onConfirmed: ()=>{ state = "11_11" }

            states: [
                State {
                    name: "11_11"
                    PropertyChanges {
                        target: prepare_calibration_finish_root
                        text: {
                            switch(printer0.printer_model) {
                            case Printer0.M1: // fall-through
                            case Printer0.SL1S:
                                return qsTr("Hold the platform still with one hand and apply a slight downward force on it, so it maintains good contact with the screen.\n\n
Next, use an Allen key to tighten the screw on the cantilever.\n\nThen release the platform.")
                            default:
                                return qsTr("Tighten the small screw on the cantilever with an allen key.")
                                        + "\n\n"
                                        + qsTr("Some SL1 printers may have two screws - tighten them evenly, little by little. See the handbook for more information.")
                            }
                        }
                        image_source: "calibration-tighten_cantilever.jpg"
                        onConfirmed: ()=>{ wizard0.prepareCalibrationFinishDone() }
                    }
                }
            ]
        }
    }

    Component {
        id: wizard_done
        PageConfirm {
            name: "CalibrationDone"
            back_button_enabled: false
            text: qsTr("Tilt settings for Prusa Slicer:")
                  + "\n\n"
                  + qsTr("Tilt time fast: %1 s").arg((wizard0.data["tilt_fast_time_ms"]/1000).toFixed(1))
                  + "\n"
                  + qsTr("Tilt time slow: %1 s").arg((wizard0.data["tilt_slow_time_ms"]/1000).toFixed(1))
                  + "\n"
                  + qsTr("Tilt time high viscosity: %1 s").arg((wizard0.data["tilt_high_viscosity_time_ms"]/1000).toFixed(1))
                  + "\n"
                  + qsTr("Area fill: %1 %").arg(printerConfig0.limit4fast)
                  + "\n\n"
                  + qsTr("All done, happy printing!")
            onConfirmed: ()=>{ wizard0.showResultsDone() }
        }
    }
}
