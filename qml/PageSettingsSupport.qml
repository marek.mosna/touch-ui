
/*
    Copyright 2019-2020, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtQml.Models 2.11
import PrusaComponents 1.0
import PrusaComponents.Delegates 1.0
import cz.prusa3d.sl1.printer0 1.0

PageVerticalList {
    id: root
    title: qsTr("Support")
    name: "settingsSupport"
    pictogram: Theme.pictogram.supportSimple
    model: ObjectModel {
        DelegateRef {
            name:"DownloadExamples"
            text: qsTr("Download Examples")
            pictogram: Theme.pictogram.download
            onClicked: ()=>{
                printer0.download_examples(
                            function(obj_path){
                                root.StackView.view.push("PageDownloadingExamples.qml")
                            },
                            function(err){console.log("ERROR: download_examples error:", err)})
            }
        }

        DelegateRef {
            name: "Manual"
            pictogram: Theme.pictogram.manual
            text: qsTr("Manual")
            onClicked: ()=>{
                root.StackView.view.push("PageManual.qml")
            }
        }

        DelegateRef {
            name: "Videos"
            visible: printer0.printer_model !== Printer0.M1
            pictogram: Theme.pictogram.video
            text: qsTr("Videos")
            onClicked: ()=>{
                root.StackView.view.push("PageVideos.qml")
            }
        }

        DelegateRef {
            name: "Sysinfo"
            pictogram: Theme.pictogram.systemInfo
            text: qsTr("System Information")
            onClicked: ()=>{
                root.StackView.view.push("PageSysinfo.qml")
            }
        }

        DelegateRef {
            name: "About"
            pictogram: Theme.pictogram.about_us
            text: qsTr("About Us")
            onClicked: ()=>{
                root.StackView.view.push("PageAbout.qml")
            }
        }
    }
}
