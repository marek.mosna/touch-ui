
/*
    Copyright 2019-2020, Prusa Research s.r.o.
    Copyright 2021, Prusa Research a.s.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import Native 1.0

import cz.prusa3d.sl1.printer0 1.0
import PrusaComponents 1.0
import "utils.js" as Utils

PrusaPage {
    id: root
    title: qsTr("Finished")
    name: "PrintFinished"
    pictogram: Theme.pictogram.finish
    back_button_enabled: false

    property string project_name: exposure.project_name

    property string project_file: exposure.project_file

    /// Print time in seconds
    property int print_time: exposure.print_end_timestamp - exposure.print_start_timestamp

    /// Layers total
    property int layers: exposure.total_layers

    /// Layer, where the print finished(it could be incomplete)
    property int current_layer: exposure.current_layer
    /// Consumed resin in ml
    property real consumed_resin: exposure.resin_used_ml
    property real layer_height: exposure.layer_height_nm * 1e-6
    property real layer_exposure_time: exposure.exposure_time_ms * 1e-3

    property real first_layer_exposure_time: exposure.exposure_time_first_ms * 1e-3

    property QtObject exposure: printer0.current_exposure_object // Default

    property bool canPrint: nativeFunctions.syncFileExists(root.project_file) && printer0.state === Printer0.IDLE
    property bool canReprint: printer0.state === Printer0.IDLE && exposure.objectPath === printer0.current_exposure
    property bool busy: false
    readonly property int columnWidth: 240
    SwipeView {
        id: swipePrint
        anchors.fill: parent
        currentIndex: 0

        Item {
            clip: true

            ScrollableText {
                id: txtProjectName
                y: 40
                x: 60
                swing: true
                height: 33
                width: parent.width - 2*60
                font.pixelSize: Theme.font.big
                font.bold: true
                text: root.project_name
            }

            Column {
                id: debugInfo
                anchors {
                    left: txtProjectName.left
                    top: txtProjectName.bottom
                }
                visible: _debug

                Text {
                    font.pixelSize: 10
                    text: "file: " + root.project_file
                }

                Text {
                    font.pixelSize: 10
                    text: "exposure state: " + exposure.state
                }
            }

            Text {
                id: finishedBanner
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    verticalCenter: parent.verticalCenter
                    verticalCenterOffset: -60
                }
                font.pixelSize: Theme.font.bannerSize
                text: {
                    switch(exposure.state) {
                    case Exposure.FINISHED: return  qsTr("FINISHED")
                    case Exposure.FAILURE: return qsTr("FAILED")
                    case Exposure.CANCELED: return qsTr("CANCELED")
                    default: return qsTr("FINISHED")
                    }
                }
                color: Theme.color.highlight
            }

            Rectangle {
                color: "black"
                height: 100
                anchors {
                    top: finishedBanner.bottom
                    topMargin: 20
                    left: parent.left
                    right:parent.right
                    rightMargin: 60
                    leftMargin: 60
                }

                // Top Left
                ColumnLayout {
                    id: printTimeColumn
                    anchors {
                        top: parent.top
                    }
                    width: root.columnWidth
                    clip: true

                    ScrollableText {
                        text: qsTr("Print Time")
                        color: "grey"
                        font.pixelSize: 25
                        width: root.columnWidth
                        swing: true
                    }

                    ScrollableText {
                        text:  Utils.durationToString(root.print_time, true)
                        font.pixelSize: 25
                        font.bold: true
                        width: root.columnWidth
                        swing: true
                    }
                }

                // Top Middle
                ColumnLayout {
                    id: layersColumn
                    anchors {
                        top: parent.top
                        horizontalCenter: parent.horizontalCenter
                        horizontalCenterOffset: 25
                    }
                    width: root.columnWidth

                    ScrollableText {
                        text: qsTr("Layers")
                        color: "grey"
                        font.pixelSize: 25
                        width: root.columnWidth
                        swing: true
                    }

                    Text {
                        text: (exposure.state !== Exposure.FINISHED && exposure.state !== Exposure.DONE ? root.current_layer + " / " : "") + root.layers
                        font.bold: true
                        font.pixelSize: 25
                    }
                }

                // Top Right
                ColumnLayout {
                    id: resinColumn
                    anchors {
                        top: parent.top
                        right: parent.right
                    }
                    width: root.columnWidth

                    Text {
                        text: qsTr("Consumed Resin")
                        color: "grey"
                        font.pixelSize: 25
                    }

                    Text {
                        text: root.consumed_resin.toFixed(1) + " ml"
                        font.bold: true
                        font.pixelSize: 25
                    }
                }

                // Bottom Left
                ColumnLayout {
                    anchors {
                        top: printTimeColumn.bottom
                        topMargin: 20
                        left: printTimeColumn.left
                    }
                    width: root.columnWidth

                    ScrollableText {
                        text: qsTr("Layer height")
                        color: "grey"
                        font.pixelSize: 25
                        width: root.columnWidth
                        swing: true
                    }

                    Text {
                        text: root.layer_height.toFixed(3) + " mm"
                        font.bold: true
                        font.pixelSize: 25
                    }
                }

                // Bottom Middle
                ColumnLayout {
                    anchors {
                        top: layersColumn.bottom
                        topMargin: 20
                        left: layersColumn.left
                    }
                    width: root.columnWidth

                    ScrollableText {
                        text: qsTr("Layer Exposure")
                        color: "grey"
                        font.pixelSize: 25
                        width: root.columnWidth - 50
                        swing: true
                    }

                    Text {
                        text: root.layer_exposure_time.toFixed(1) + " " + qsTr("s")
                        font.bold: true
                        font.pixelSize: 25
                    }
                }

                // Bottom Right
                ColumnLayout {
                    anchors {
                        top: resinColumn.bottom
                        topMargin: 20
                        left: resinColumn.left
                    }
                    width: root.columnWidth

                    ScrollableText {
                        text: qsTr("First Layer Exposure")
                        color: "grey"
                        font.pixelSize: 25
                        width: root.columnWidth
                        swing: true
                    }

                    Text {
                        text: root.first_layer_exposure_time.toFixed(1) + " " + qsTr("s")
                        font.bold: true
                        font.pixelSize: 25
                    }
                }

            }

            SwipeSign {
                anchors {
                    right: parent.right
                    margins: 10
                    bottom: parent.bottom
                    bottomMargin: 5
                }
                text: qsTr("Swipe to continue")
                direction: "right"
                onClicked: ()=>{
                    swipePrint.incrementCurrentIndex()
                }
            }
        }

        Item {
            clip: true

            GridLayout {
                anchors {
                    leftMargin: 55
                    rightMargin: 55
                    verticalCenter: parent.verticalCenter
                    verticalCenterOffset: -40
                    left: parent.left
                }

                columns: 4
                rows: 1
                columnSpacing: 30
                rowSpacing: 5

                PictureButton {
                    id: btnHome
                    pictogram: Theme.pictogram.home
                    text: qsTr("Home")
                    name: "Home"
                    onClicked: ()=>{
                         root.StackView.view.replace(null, "PageHome.qml")
                    }
                }

                PictureButton {
                    name: "CleanTank"
                    text: qsTr("Resin Tank Cleaning")
                    pictogram: Theme.pictogram.wizardTankSurfaceCleaner
                    onClicked: ()=>printer0.run_tank_surface_cleaner_wizard()
                }

                PictureButton {
                    pictogram:  Theme.pictogram.reprint
                    text: qsTr("Reprint")
                    name: "Reprint"
                    enabled: root.canReprint
                    onClicked: ()=>{
                        console.log("Trying to start reprint of: " + root.project_file)
                        root.busy = true
                        printer0.reprint(false,
                                         ()=>{root.busy = false},
                                         (err)=>{
                                             root.busy = true;
                                             canReprint = false;
                                         }
                                         )
                    }
                }

                PictureButton {
                    name: "TurnOff"
                    text: qsTr("Turn Off")
                    pictogram: Theme.pictogram.powerOff
                    enabled: printer0.state === Printer0.IDLE
                    onClicked: () => {
                        root.StackView.view.push("PagePowerOffDialog.qml")
                    }
                }
            }
        }
    }


    PageIndicator {
        id: indicator
        count: swipePrint.count
        currentIndex: swipePrint.currentIndex
        delegate: Item {
            height: 15
            width: 30

            Rectangle {
                width: 15
                height: 15
                color: index === indicator.currentIndex ? Theme.color.highlight : "grey"
                radius: 7
                anchors.margins: 10
            }
        }
        anchors.bottom: swipePrint.bottom
        anchors.margins: 5
        anchors.horizontalCenter: parent.horizontalCenter
    }

    ImageVersionText {}

    PrusaWaitOverlay {
        id: waitOverlay
        text: qsTr("Loading, please wait...")
        doWait: root.busy
        anchors.fill: parent
    }
}
