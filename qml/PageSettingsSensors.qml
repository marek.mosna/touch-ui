
/*
    Copyright 2019-2020, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtQml.Models 2.11
import PrusaComponents 1.0
import PrusaComponents.Delegates 1.0

PageVerticalList {
    title: qsTr("Settings & Sensors")
    name: "SettingsSensors"
    pictogram: Theme.pictogram.sensor
    model: ObjectModel {

        DelegateSwitch {
            id: anonymizedqr
            name: "AnonymizedQR"
            text: qsTr("Device hash in QR")
            pictogram: Theme.pictogram.hostname
            Component.onCompleted: ()=>{
                anonymizedqr.checked = printer0.data_privacy
            }
            onClicked: ()=>{
                if(anonymizedqr.checked) {
                    printer0.set_data_privacy(false);
                }
                else {
                    printer0.set_data_privacy(true);
                }
                anonymizedqr.checked = printer0.data_privacy
            }

            Connections {
                target: printer0
                function onData_privacyChanged() {
                    anonymizedqr.checked = printer0.data_privacy
                }
            }
        }
        DelegateSwitch {
            id: autopoweroff
            name: "AutoPowerOff"
            text: qsTr("Auto Power Off")
            pictogram: Theme.pictogram.powerOff
            Component.onCompleted: ()=>{
                autopoweroff.checked = printerConfig0.autoOff
            }
            onClicked: ()=>{
                if(autopoweroff.checked) {
                    printerConfig0.autoOff = false
                }
                else {
                    printerConfig0.autoOff = true
                }
                autopoweroff.checked = printerConfig0.autoOff
            }

            Connections {
                target: printerConfig0
                function onAutoOffChanged() {
                    autopoweroff.checked = printerConfig0.autoOff
                }
            }
        }

        DelegateSwitch {
            id: covercheck
            name: "CoverCheck"
            text: qsTr("Cover Check")
            pictogram: Theme.pictogram.cover
            Component.onCompleted: ()=>{
                covercheck.checked = printerConfig0.coverCheck
            }
            onClicked: ()=>{
                printerConfig0.coverCheck = (! covercheck.checked)
                covercheck.checked = printerConfig0.coverCheck
                if(covercheck.checked == false) {
                    view.push(coverSensorDisableDialog)
                }
            }

            Connections {
                target: printerConfig0
                function onCoverCheckChanged() {
                    covercheck.checked = printerConfig0.coverCheck
                }
            }

            Component {
                id: coverSensorDisableDialog
                PageYesNoSimple {
                    id: coverSensorDisableDialogRoot
                    name: "CoverCheckDisableconfirm"
                    title: qsTr("Are you sure?")
                    text: qsTr("Disable the cover sensor?<br/><br/>CAUTION: This may lead to unwanted exposure to UV light or personal injury due to moving parts. This action is not recommended!")
                    onResult: (result) => {
                                  printerConfig0.coverCheck = result ? false : true
                                  coverSensorDisableDialogRoot.StackView.view.pop()
                              }
                }
            }
        }

        DelegateSwitch {
            id: resinsensor
            name: "ResinSensor"
            text: qsTr("Resin Sensor")
            pictogram:  Theme.pictogram.refill
            checked: printerConfig0.resinSensor
            onClicked: ()=>{
                printerConfig0.resinSensor = ! resinsensor.checked
                resinsensor.checked = printerConfig0.resinSensor
                if(resinsensor.checked == false) {
                    view.push(resinSensorDisableDialog)
                }
            }

            Connections {
                target: printerConfig0
                function onResinSensorChanged(){
                    resinsensor.checked = printerConfig0.resinSensor
                }
            }

            Component {
                id: resinSensorDisableDialog
                PageYesNoSimple {
                    id: resinSensorDisableDialogRoot
                    name: "ResinSensorDisableConfirm"
                    title: qsTr("Are you sure?")
                    text: qsTr("Disable the resin sensor?<br/><br/>CAUTION: This may lead to failed prints or resin tank overflow! This action is not recommended!")
                    onResult: (result) => {
                                  printerConfig0.resinSensor = result ? false : true
                                  resinSensorDisableDialogRoot.StackView.view.pop()
                              }
                }
            }
        }

        DelegatePlusMinus {
            id: rearfanspeed
            name: "RearFanSpeed"
            text: qsTr("Rear Fan Speed")
            pictogram: Theme.pictogram.fan

            initValue: printerConfig0.fanRearRpm
            step: 100
            from: 800
            to: 5000
            units: qsTr("RPM")

            onValueChanged: ()=>{
                printerConfig0.fanRearRpm = value
            }

            Connections {
                target: printerConfig0
                function onFanRearRpmChanged() {
                    rearfanspeed.value = printerConfig0.fanRearRpm
                }
            }
            valuePresentation: function(v) { return (v <= from) ? qsTr("Off") : v }
        }
    }
}
