/*
    Copyright 2021, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5

import cz.prusa3d.sl1.printer0 1.0
import cz.prusa3d.wizard0 1.0
import cz.prusa3d.wizardcheckmodel 1.0
import PrusaComponents 1.0

ColumnLayout {
    id: root
    width: parent ? parent.width : 0
    spacing: 0

    Timer {
        interval: 1500
        running: model.checkState === WizardChecksModel.Success
        onTriggered: () => {
            model.hidden = true
        }
    }

    RowLayout {
        Item {
            width: 40
            height: 80
            Layout.leftMargin: 10
            Layout.alignment: Qt.AlignVCenter
            Image {
                id: checkPictogram
                anchors.fill: parent
                sourceSize.width: parent.width
                sourceSize.height: parent.height
                fillMode: Image.PreserveAspectFit
                rotation: 0

                source: {
                    switch(model.checkState) {
                    case WizardChecksModel.Waiting: return Theme.pictogram.path + "/" + Theme.pictogram.waiting
                    case WizardChecksModel.Running: return Theme.pictogram.path + "/" + Theme.pictogram.busy
                    case WizardChecksModel.Success: return Theme.pictogram.path + "/" + Theme.pictogram.ok
                    case WizardChecksModel.Failure: return Theme.pictogram.path + "/" + Theme.pictogram.nok
                    case WizardChecksModel.Warning: return  Theme.pictogram.path + "/" + Theme.pictogram.ok
                    case WizardChecksModel.User: return Theme.pictogram.path + "/" + Theme.pictogram.adminSimple
                    case WizardChecksModel.Canceled: return Theme.pictogram.path + "/" + Theme.pictogram.cancel
                    default: return Theme.pictogram.path + "/" + Theme.pictogram.testtube
                    }
                }
                NumberAnimation on rotation {
                    running: {
                        if(model.checkState === WizardChecksModel.Running) {
                            return true
                        } else {
                            checkPictogram.rotation = 0
                            return false
                        }
                    }

                    from:0
                    to: 360
                    duration: 800
                    loops: Animation.Infinite
                }
            }
        }

        Text {
            id: txtLabel
            Layout.fillWidth: true
            font.pixelSize: Theme.font.big
            color: "white"
            text: {
                switch(model.checkId) {
                case WizardChecksModel.TowerRange: return qsTr("Platform range")
                case WizardChecksModel.TowerHome: return qsTr("Platform home")
                case WizardChecksModel.TiltRange: return qsTr("Tank range")
                case WizardChecksModel.TiltHome: return qsTr("Tank home")
                case WizardChecksModel.Display: return qsTr("Display test")
                case WizardChecksModel.Calibration: return qsTr("Printer calibration")
                case WizardChecksModel.Music: return qsTr("Sound test")
                case WizardChecksModel.UVLEDs: return qsTr("UV LED")
                case WizardChecksModel.UVFans: return qsTr("UV LED and fans")
                case WizardChecksModel.MoveToFoam: return qsTr("Release foam")
                case WizardChecksModel.MoveToTank: return qsTr("Make tank accessible")
                case WizardChecksModel.ResinSensor: return qsTr("Resin sensor")
                case WizardChecksModel.SerialNumber: return qsTr("Serial number")
                case WizardChecksModel.Temperature: return qsTr("Temperature")
                case WizardChecksModel.TiltCalibrationStart: return qsTr("Tank calib. start")
                case WizardChecksModel.TiltCalibration: return qsTr("Tank level")
                case WizardChecksModel.TowerCalibration: return qsTr("Platform calibration")
                case WizardChecksModel.TiltTiming: return qsTr("Tilt timming")
                case WizardChecksModel.SysInfo: return qsTr("Obtain system info")
                case WizardChecksModel.CalibrationInfo: return qsTr("Obtain calibration info")
                case WizardChecksModel.EraseProjects: return qsTr("Erase projects")
                case WizardChecksModel.ResetHostname: return qsTr("Reset hostname")
                case WizardChecksModel.ResetAPIKey: return qsTr("Reset API key")
                case WizardChecksModel.ResetRemoteConfig: return qsTr("Reset remote config")
                case WizardChecksModel.ResetHttpDigest: return qsTr("Reset HTTP digest")
                case WizardChecksModel.ResetWifi: return qsTr("Reset Wi-Fi settings")
                case WizardChecksModel.ResetTimezone: return qsTr("Reset timezone")
                case WizardChecksModel.ResetNTP: return qsTr("Reset NTP state")
                case WizardChecksModel.ResetLocale: return qsTr("Reset system locale")
                case WizardChecksModel.ResetUVCalibrationData: return qsTr("Clear UV calibration data")
                case WizardChecksModel.RemoveSlicerProfiles: return qsTr("Clear downloaded Slicer profiles")
                case WizardChecksModel.ResetHWConfig: return qsTr("Reset print configuration")
                case WizardChecksModel.EraseMCEeprom: return qsTr("Erase motion controller EEPROM")
                case WizardChecksModel.ResetHomingProfiles: return qsTr("Reset homing profiles")
                case WizardChecksModel.SendPrinterData: return qsTr("Send printer data to MQTT")
                case WizardChecksModel.DisableFactory: return qsTr("Disable factory mode")
                case WizardChecksModel.InitiatePackingMoves: return qsTr("Moving printer to accept protective foam")
                case WizardChecksModel.FinishPackingMoves: return qsTr("Pressing protective foam")
                case WizardChecksModel.DisableAccess: return qsTr("Disable ssh, serial")
                case WizardChecksModel.UVMeterPresent: return qsTr("Check for UV calibrator")
                case WizardChecksModel.UVWarmup: return qsTr("UV LED warmup")
                case WizardChecksModel.UVMeterPlacement: return qsTr("UV calibrator placed")
                case WizardChecksModel.UVCalibrateCenter: return qsTr("Calibrate center")
                case WizardChecksModel.UVCalibrateEdge: return qsTr("Calibrate edge")
                case WizardChecksModel.UVCalibrationApplyResults: return qsTr("Apply calibration results")
                case WizardChecksModel.UVMetereRemoved: return qsTr("Waiting for UV calibrator to be removed")
                case WizardChecksModel.TiltLevel: return qsTr("Tank level")
                case WizardChecksModel.ResetTouchUI: return qsTr("Reset UI settings")
                case WizardChecksModel.EraseUVPWM: return qsTr("Erase UV PWM settings")
                case WizardChecksModel.ResetSelfTest: return qsTr("Reset selftest status")
                case WizardChecksModel.ResetMechanicalCalibration: return qsTr("Reset printer calibration status")
                case WizardChecksModel.MarkPrinterModel: return qsTr("Set new printer model")
                case WizardChecksModel.ResetHwCounters: return qsTr("Resetting hardware counters")
                case WizardChecksModel.RecordExpoLog: return qsTr("Recording changes")
                case WizardChecksModel.Unknown: return qsTr("Unknown")

                case WizardChecksModel.TowerSafeDistance: return qsTr("Moving platform down to safe distance")
                case WizardChecksModel.TowerTouchdown: return qsTr("Touching platform down")
                case WizardChecksModel.ExposingDebris: return qsTr("Exposing debris")
                case WizardChecksModel.TowerGentlyUp: return qsTr("Moving platform gently up")
                case WizardChecksModel.WaitingForUser: return qsTr("Waiting for user")
                case WizardChecksModel.TowerHomeFinish: return qsTr("Platform home")

                default: return qsTr("Check ID:") + " " + model.checkId
                }
            }
        }

        Text {
            id: txtDescription
            font.pixelSize: Theme.font.big
            Layout.rightMargin: 20
            color: Theme.color.grey
            text: {
                switch(model.checkState) {
                case WizardChecksModel.Waiting: return qsTr("Waiting")
                case WizardChecksModel.Running: return qsTr("Running")
                case WizardChecksModel.Success: return qsTr("Passed")
                case WizardChecksModel.Failure: return qsTr("Failure")
                case WizardChecksModel.Warning: return  qsTr("With Warning")
                case WizardChecksModel.User: return qsTr("User action pending")
                case WizardChecksModel.Canceled: return qsTr("Canceled")
                default: JSON.stringify(model.checksState)
                }
            }
        }
    }

    Rectangle {
        visible: model.checkState === WizardChecksModel.Running
        height: 5
        color: "green"
        width: parent.width * (model.checkData ? model.checkData.progress : 1.0)
    }

    Rectangle {
        height: 2
        Layout.fillWidth: true
        color: Theme.color.grey
    }
}
