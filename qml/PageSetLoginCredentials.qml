
/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import PrusaComponents 1.0

/*
 Depends on:
    StackView view

*/
PrusaPage {
    id: root
    title: qsTr("Login Credentials")
    name: "SetLoginCredentials"
    pictogram: Theme.pictogram.key
    property bool http_digest: printer0.http_digest
    
    Component {
        id: digestDisableDialog

        PageYesNoSimple {
            id: digestDisableDialogRoot
            name: "DisableDigestDialog"
            title: qsTr("Are you sure?")
            text: qsTr("Disable the HTTP Digest?<br/><br/>CAUTION: This may be insecure!")
            onYes: (result) => {
                          root.http_digest = false
                          localView.currentIndex = 1
                          digestDisableDialogRoot.StackView.view.pop()
                      }
            onNo: () => digestDisableDialogRoot.StackView.view.pop()
        }
    }

    Item {
        id: swRoot
        width: 500
        height: 70
        anchors.horizontalCenter: parent.horizontalCenter

        Text {
            id: txtLabel
            anchors {
                left: parent.left
                verticalCenter: parent.verticalCenter
            }
            font.pixelSize: 28
            text: qsTr("HTTP Digest")
        }

        PrusaSwitch {
            id: sw
            name: "HTTPDigest"
            checked: root.http_digest
            clickSwitch: false
            anchors {
                right: parent.right
                verticalCenter: parent.verticalCenter
            }
            onClicked:  () => {
                if(! root.http_digest) {
                    root.http_digest = true
                    localView.currentIndex = 0
                }
               else  root.StackView.view.push(digestDisableDialog)
            }
        }
    }
    
    StackLayout {
        id: localView
        anchors.fill: parent
        currentIndex: root.http_digest ? 0 : 1
        
        Item { 
            
            RowLayout {
                id: nnn
                spacing: 30
                width: 500
                anchors {
                    //centerIn: parent
                    left: rrr.left
                    bottom: rrr.top
                    bottomMargin: 10
                }

                Text {
                    id: t1
                    Layout.alignment: Qt.AlignVCenter
                    text: qsTr("User Name") + ":"
                    font.pixelSize: 24
                }

                Item {
                    width: t2.width - t1.width - 30
                }
                
                TextField {
                    id: txtUserName
                    Layout.alignment: Qt.AlignVCenter
                    Layout.fillWidth: true
                    text: "maker"
                    width: 350
                    font.family: prusaFont.name
                    font.pixelSize: 24
                    enabled: false
                    property var valid: text.match(/^[0-9a-zA-Z][0-9a-zA-Z,\-,\.]*$/)
                    inputMethodHints: Qt.ImhNoAutoUppercase
                }                
            }

            RowLayout {
                id: rrr
                spacing: 30
                width: 500
                anchors {
                    leftMargin: 55
                    rightMargin: 55
                    verticalCenter: parent.verticalCenter
                    verticalCenterOffset: -20
                    left: parent.left
                }

                Text {
                    id: t2
                    Layout.alignment: Qt.AlignVCenter
                    text: qsTr("Printer Password") + ":"
                    font.pixelSize: 24
                }
                
                TextField {
                    id: txtPassword
                    Layout.alignment: Qt.AlignVCenter
                    Layout.fillWidth: true
                    text: printer0.api_key
                    width: 350
                    font.family: prusaFont.name
                    font.pixelSize: 24
                    inputMethodHints: Qt.ImhNoAutoUppercase
                    property var valid: text.length >= 8
                }
            }

            Text {
                anchors {
                    top: rrr.bottom
                    topMargin: 20
                    left: rrr.left
                }
                
                font.pixelSize: 24
                text: qsTr("Must be at least 8 chars long")
                color: "grey"
                
            }
            
            WarningText {
                item: txtPassword
                anchors.bottom: rrr.top
                text: qsTr("Must be at least 8 chars long")
            }

            WarningText {
                item: txtUserName
                anchors.bottom: nnn.top
                text: qsTr("Can contain only characters a-z, A-Z, 0-9 and  \"-\".")
            }
        }
        
        Item { 
            RowLayout {
                id: ooo
                spacing: 30
                width: 500
                anchors {
                    leftMargin: 55
                    rightMargin: 55
                    verticalCenter: parent.verticalCenter
                    verticalCenterOffset: -40
                    left: parent.left
                }

                Text {
                    id: t3
                    Layout.alignment: Qt.AlignVCenter
                    text: qsTr("Printer API Key") + ":"
                    font.pixelSize: 24
                }
                
                TextField {
                    id: txtApiKey
                    Layout.alignment: Qt.AlignVCenter
                    Layout.fillWidth: true
                    text: printer0.api_key
                    width: 350
                    font.family: prusaFont.name
                    font.pixelSize: 24
                    inputMethodHints: Qt.ImhNoAutoUppercase
                    property var valid: text.match(/^[0-9A-Za-z\_\-\`\!\@\#\$\%\^\&\*\(\)_\+\=\{\}\]\[\;\:\\\'\"\<\>\?\,\.\/\~\ ]*$/) && text.length >= 8 // ASCII characters
                }
            }

            Text {
                anchors {
                    top: ooo.bottom
                    topMargin: 20
                    left: ooo.left
                }
                font.pixelSize: 24
                text: qsTr("Must be at least 8 chars long")
                color: "grey"
            }
            
            WarningText {
                item: txtApiKey
                anchors.bottom: ooo.top
                text: qsTr("Can contain only ASCII characters.")
            }
        }
    }
    
    PictureButton {
        anchors {
            right: parent.right
            verticalCenter: parent.verticalCenter
            verticalCenterOffset:  -40
            rightMargin: 55
        }
        
        text: qsTr("Save")
        name: "Save"
        backgroundColor:"green"
        pictogram: Theme.pictogram.yes
        z: -1
        enabled: root.http_digest ? (txtUserName.valid && txtPassword.valid) : txtApiKey.valid
        onClicked: () => {
            var api_key;
            if(root.http_digest){
                api_key = txtPassword.text;
            }else{
                api_key = txtApiKey.text;
            }

            if (!(printer0.set_http_digest(root.http_digest) && printer0.set_api_key(api_key))){
                Qt.createComponent("qrc:/ErrorPopup.qml")
                    .createObject(window, {
                        text: qsTr("Failed change login settings.")
                        }
                    ).open();
            }else{
                view.pop();
            }
        }
    }
    
    ImageVersionText {}
}
