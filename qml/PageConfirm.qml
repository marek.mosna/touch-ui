
/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import PrusaComponents 1.0
import cz.prusa3d.sl1.printer0 1.0

PrusaPage {
    id: root
    title: ""
    name: "confirm"
    pictogram: Theme.pictogram.yes
    screensaver_enabled: false
    header_rollup: flick.contentX == 0
    property string image_source
    property string text: ""
    property bool enabled: true
    signal confirmed()


    StackView.onActivated: ()=>{
        movementCanceled = true // Set the flag
        flick.cancelFlick()
        flick.contentX = img_edge
    }

    property double initContentX;
    property int img_edge: 765
    property bool movementCanceled: false
    function popDialog() { if(StackView.status === StackView.Active) root.StackView.view.pop(); }

    Flickable {
        id: flick
        anchors {
            bottom: parent.bottom
            fill: parent
        }
        contentWidth: 800 + img_edge
        contentX: img_edge
        interactive: root.image_source ? true : false // Flick only if there is an image
        boundsBehavior: Flickable.StopAtBounds

        onMovementEnded: ()=>{
           //
           console.log("Confirm flick: movement stopped at " + contentX)

           if(contentX < img_edge/2 && contentX != 0) {
               if(movementCanceled) contentX = 0
               else flick.flick(5000, 0)
               console.log("Flicking to 0")
           }
           else if(contentX >= img_edge/2 && contentX != img_edge) {
               console.log("Flicking to " + img_edge)
               if(movementCanceled) contentX = img_edge
               else flick.flick(-5000,0)
           }
           else {
               // Revert the movementCanceled flag
               movementCanceled = false;
           }
        }

        Image {
            id: theImg
            source: root.image_source ? printer0.multimediaImages + "/" + root.image_source : ""
            visible: root.image_source ? true : false
            height: 480
            width: 800
            fillMode: Image.Stretch
            property int dummy: 0
            anchors {
                left: parent.left
                bottom: parent.bottom
                margins: 0
            }


            onVisibleChanged: ()=>{
                swipeSign.shake()
            }
            onSourceChanged: ()=>{
                onVisibleChanged()
            }

            // Flick to the right if the image is clicked
            MouseArea {
                anchors.fill: parent
                onClicked: flick.flick(-5000,0)
            }
        }


        SwipeSign {
            id: swipeSign
            anchors {
                left: theImg.right
                margins: 10
                bottom: theImg.bottom
                bottomMargin: 5
            }
            visible: root.image_source ? true : false
            text: qsTr("Swipe for a picture")
            direction: "left"
            onClicked: ()=>{ flick.flick(5000,0) }
        }

        Rectangle {
            color: "grey"
            visible: root.image_source ? true : false
            width: 34
            height: 80
            anchors {
                horizontalCenter: theImg.right
                verticalCenter: theImg.bottom
                verticalCenterOffset:  -480/2
            }
            radius:10

            Grid {
                anchors {
                    centerIn: parent
                }
                rows: 4
                columns: 2
                rowSpacing: 4
                columnSpacing: 8

                Repeater {
                    model: 6

                    Rectangle {
                        width: 6
                        height: 6
                        radius: 3
                        color: "black"
                    }
                }
            }

            MouseArea {
                anchors.fill: parent
                onClicked: ()=>{
                    if(flick.contentX == 0) flick.contentX = img_edge
                    if(flick.contentX == img_edge) flick.contentX = 0
                }
            }
        }


        Column {
            x: 55 + img_edge + (root.image_source ? 30 : 0)
            anchors.verticalCenter: parent.bottom
            anchors.verticalCenterOffset: -480/2
            width: 490

            Text {
                width: parent.width
                visible: root.text ? true : false
                text: root.text
                wrapMode: Text.Wrap
            }
        }

        Row {
            id: buttonRow
            anchors {
                right: parent.right
                verticalCenter: parent.bottom
                verticalCenterOffset: -480/2 - 10
                rightMargin: 55
            }
            spacing: 30

            PictureButton {
                name: "Continue"
                text: qsTr("Continue")
                pictogram: Theme.pictogram.yes
                backgroundColor: "green"
                enabled: root.enabled
                onClicked:()=>{ root.confirmed() }
            }
        }
    }

    Row {
        visible: theImg.visible

        Item {
           height: 15
           width: 30

           Rectangle {
               width: 15
               height: 15
               color: flick.contentX == 0 ? Theme.color.highlight : "grey"
               radius: 7
               anchors.margins: 10
           }
       }

        Item {
           height: 15
           width: 30

           Rectangle {
               width: 15
               height: 15
               color: flick.contentX == img_edge ? Theme.color.highlight : "grey"
               radius: 7
               anchors.margins: 10
           }
       }
       anchors {
            horizontalCenter: parent.horizontalCenter
            bottom: parent.bottom
            bottomMargin: 10
       }
    }

    ImageVersionText {
        visible: root.image_source ? false : true
    }
}
