/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtQml.Models 2.3
import Qt.labs.qmlmodels 1.0

import PrusaComponents 1.0
import PrusaComponents.Delegates 1.0

import cz.prusa3d.sl1.admin 1.0
import cz.prusa3d.sl1.printer0 1.0
import cz.prusa3d.wizard0.item.action 1.0
import cz.prusa3d.wizard0.item.value.int 1.0
import cz.prusa3d.wizard0.item.value.fixed 1.0
import cz.prusa3d.wizard0.item.value.float 1.0
import cz.prusa3d.wizard0.item.value.bool 1.0
import cz.prusa3d.wizard0.item.value.text 1.0

PrusaPage {
    id: root
    title: qsTr("Admin API")
    name: "Admin"
    pictogram: Theme.pictogram.settingsSimple
    header_rollup: true
    header_can_expand: false

    Component.onCompleted: ()=>{
        adminApi.enter()
    }

    Connections {
        target: adminApi

        function onEnterSysinfo() {
            console.log("Entering sysinfo from admin")
            view.push("PageSysinfo.qml")
        }
        function onEnterTouchscreenTest() {
            console.log("Entering touchscreen test from admin")
            view.push("PageDisplayTest.qml")
        }
        function onEnterFullscreenImage() {
            console.log("Entering fullscreen image from admin")
            view.push("PageFullscreenImage.qml")
        }
        function onEnterTowerMoves() {
            console.log("Entering tower moves from admin")
            view.push("PageTowermove.qml")
        }
        function onEnterTiltMoves() {
            console.log("Entering tilt moves from admin")
            view.push("PageTiltmove.qml")
        }
    }

    ListView {
        id: adminList
        height: 480
        interactive: true
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            rightMargin: 10
            leftMargin: 10
        }
        //        cacheBuffer: height * 5
        model: adminApi.model

        delegate: DelegateChooser {
            id: chooser
            role: "itemType"

            DelegateChoice {
                roleValue: AdminModel.XTextValueItem
                DelegateText {
                    name: model.name
                    text: model.value
                    pictogram: model.icon === "" ? Theme.pictogram.adminSimple : model.icon
                    enabled: model.enabled
                }
            }

            DelegateChoice {
                roleValue: AdminModel.XIntValueItem
                DelegatePlusMinus {
                    property int modelValue: model.value
                    name: model.name
                    text: model.name
                    pictogram: model.icon === "" ? Theme.pictogram.adminSimple : model.icon
                    enabled: model.enabled
                    step: model.step
                    from: model.minimum
                    to: model.maximum
                    onValueModified: () => {model.value = value; value = model.value}
                    Component.onCompleted: () => value = modelValue
                    onModelValueChanged: () => { value = modelValue }

                }
            }

            DelegateChoice {
                roleValue: AdminModel.XFloatValueItem
                DelegatePlusMinusDouble {
                    property real modelValue: model.value
                    name: model.name
                    text: model.name
                    pictogram: model.icon === "" ? Theme.pictogram.adminSimple : model.icon
                    enabled: model.enabled
                    step: model.step
                    from: model.minimum
                    to: model.maximum
                    onValueModified: () => {model.value = value; setValue(model.value)}
                    initValue: modelValue
                    onModelValueChanged: () => { setValue(modelValue) }
                }
            }

            DelegateChoice {
                roleValue: AdminModel.XFixedValueItem
                DelegatePlusMinus {
                    property real modelValue: model.value
                    name: model.name
                    text: model.name
                    pictogram: model.icon === "" ? Theme.pictogram.adminSimple : model.icon
                    enabled: model.enabled
                    step: model.step
                    from: model.minimum
                    to: model.maximum
                    Component.onCompleted: () => value = model.value
                    onModelValueChanged: () => { value = model.value }
                    onValueModified: () => {model.value = value; value = model.value}
                    valuePresentation: function(value) {
                        var factor = Math.pow(10, fractions)
                        var absolute = Math.abs(value)
                        var intPart = Math.trunc(absolute / factor)
                        var fracPart = absolute % factor
                        return (value < 0 ? "-": "") + intPart + "." + fracPart.toString().padStart(fractions, "0")
                    }
                }
            }

            DelegateChoice {
                roleValue: AdminModel.XBoolValueItem
                DelegateSwitch {
                    name: model.name
                    text: model.name
                    checked: model.value
                    pictogram: model.icon === "" ? Theme.pictogram.adminSimple : model.icon
                    enabled: model.enabled
                    onClicked: () => {model.value = ! checked}
                }
            }

            DelegateChoice {
                roleValue: AdminModel.XActionItem
                DelegateRef {
                    name: model.name
                    text: model.name
                    pictogram: model.icon === "" ? Theme.pictogram.adminSimple : model.icon
                    enabled: model.enabled
                    onClicked: () => {adminApi.model.execute(model.index)}
                }
            }

            DelegateChoice {
                roleValue: AdminModel.XSelectionValueItem
                DelegateSelect {
                    property real modelValue: model.value
                    name: model.name
                    text: model.name
                    pictogram: model.icon === "" ? Theme.pictogram.adminSimple : model.icon
                    enabled: model.enabled
                    items: model.selection
                    onModelValueChanged: () => { index = modelValue}
                    onIndexChanged: () => {
                        if(index != model.value) {
                            model.value = index
                            index = model.value
                        }
                    }
                    wrap: model.wrap_around
                }
            }

        }

        clip: true
        ScrollBar.vertical: ScrollBar {
            id: adminScroll
            active: true
            policy: ScrollBar.AlwaysOn
        }
    }
}
