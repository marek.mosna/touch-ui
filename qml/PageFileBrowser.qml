
/*
    Copyright 2019-2020, Prusa Research s.r.o.
              2021, Prusa Research a.s.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import Qt.labs.qmlmodels 1.0
import cz.prusa3d.sl1.filemanager 1.0
import cz.prusa3d.sl1.printer0 1.0
import Native 1.0
import ErrorsSL1 1.0
import PrusaComponents 1.0

PageVerticalList {
    id: root
    title: qsTr("Projects")
    name: "ProjectSelect"
    pictogram: Theme.pictogram.folderSimple

    property var show_origin: [FMFile.Local, FMFile.Usb, FMFile.PreviousPrints, FMFile.Raucb]

    property string initial_path

    property string needed_steps: "" +
                           //(printer0.unboxed ? "" : ("\n\u2022 " + qsTr("Unboxing") )) +
                           (printer0.self_tested ? "" : ("\n\u2022 " + qsTr("Selftest"))) +
                           (printer0.mechanically_calibrated ? "" : ("\n\u2022 " + qsTr("Printer Calibration"))) +
                           (printer0.uv_calibrated ? "" : ("\n\u2022 " + qsTr("UV Calibration")))

    FMContext {
        id: projSelectionContext
        objectName: "projSelectionContext"
    }
    busy: projSelectionContext.busy

    model: projSelectionContext.current.children

    delegate: SwipeDelegate {
        id: projectDelegate


        property var type: modelData.type
        function setPictogram(metadata) {
            var thumbnail = metadata["thumbnails"].length ? "file://" + metadata["thumbnails"][0] : Theme.pictogram.path + "/" + Theme.pictogram.thumbnailPlaceholder
            _preview = thumbnail
        }

        property string _preview: type === FMFile.File ? Theme.pictogram.path + "/" + Theme.pictogram.thumbnailPlaceholder : ""

        Component.onCompleted: function(){
            if(type === FMFile.File && modelData.origin !== FMFile.Raucb) {
                projSelectionContext.get_metadata(projectDelegate.path, true, projectDelegate.setPictogram, function(e1, e2, e3) {
                    console.log("Preview Error: ", e1, e2,e3)
                })
            }
        }

        property string pictogram: {
            if(modelData.type === FMFile.File) {
                if(modelData.origin === FMFile.Raucb) {
                    return Theme.pictogram.firmware
                }
                else return  Theme.pictogram.thumbnailPlaceholder
            }
            else if(modelData.type === FMFile.Directory) {
                return Theme.pictogram.folderSimple
            }
            else return Theme.pictogram.testtube
        }
        property string path: modelData.path
        property string filename: {
            if(index === 0) {
                return ".."
            }
            else if(modelData.isUnderRoot) {
                switch(modelData.origin) {
                case FMFile.Local: return qsTr("Local")
                case FMFile.Usb: return qsTr("USB")
                case FMFile.Remote: return qsTr("Remote")
                case FMFile.PreviousPrints: return qsTr("Previous Prints", "a directory with previously printed projects")
                case FMFile.Raucb: return qsTr("Update Bundles", "a directory containing firmware update bundles")
                default:
                    var spath = projectDelegate.path.split("/")
                    return spath[spath.length -1]
                }
            }
            else {
                var sp = projectDelegate.path.split("/")
                return sp[sp.length -1]
            }
        }

        visible: (modelData.isUnderRoot !== true || index !== 0)
                 &&  (root.show_origin.indexOf(modelData.origin) !== -1 || modelData.origin === FMFile.InvalidOrigin)
        height: visible ? 90 : 0
        width: parent.width - 5
        onClicked: function() {
            printer0.beep_button()
            if(modelData.type === FMFile.Directory) {
                projSelectionContext.cd(index)
            }
            else if(modelData.type === FMFile.File && modelData.origin !== FMFile.Raucb) {
                console.log(
                            "Trying to print ", path, "\n",
                            "\nunboxed? ", printer0.unboxed,
                            "\nself_tested? ", printer0.self_tested,
                            "\nmechanically_calibrated? ", printer0.mechanically_calibrated,
                            "\nuv_calibrated? ", printer0.uv_calibrated)
                // Questions will be asked in this order so that
                // mechanical calibration will end-up on top(to be shown first)
                if(printer0.self_tested && printer0.mechanically_calibrated && printer0.uv_calibrated ) {
                    root.busy = true
                    printer0.print(
                                path,
                                false,
                                ()=>{root.busy = false},
                                (e)=>{root.busy = false}
                                )
                }
                else root.StackView.view.push(dialogDoYouWantToRunWizards)
            }
            else { // Raucb file
                root.StackView.view.push(installUpdateDialog)
            }
        }

        Component {
            id: installUpdateDialog

            PageYesNoSimple {
                title: qsTr("Install?")
                text:
                    qsTr("Do you really want to install %1?").arg(filename)
                    + "\n\n"
                    + qsTr("Current system will still be available via Settings -> Firmware -> Downgrade")
                onYes: () => {
                           run_update({bundle_path: path, bundle_name: filename})
                           popDialog()
                       }
                onNo: () => {console.log("User does not want to install ", path); popDialog()}
            }
        }

        Component {
            id: dialogDoYouWantToRunWizards
            PageYesNoSimple {
                title: qsTr("Calibrate?")
                text: {
                    var help = "The printer is not fully calibrated. Before printing, the following steps are required to pass: <list of wizards to pass before print>. Do you want to start now?"
                    return qsTr("The printer is not fully calibrated.", help) +
                        "\n\n" +
                        qsTr("Before printing, the following steps are required to pass:", help) +
                        "\n" +
                        root.needed_steps +
                        "\n\n" +
                        qsTr("Do you want to start now?", help)
                }
                onYes: () => {printer0.make_ready_to_print(); popDialog()}
                onNo: () => {console.log("User does not want to run calibration."); popDialog()}

            }
        }

        background: Rectangle {color: "black"}


        contentItem: GridLayout {
            width: parent.width
            height: parent.height
            columns: 3
            rows: 2
            flow: GridLayout.TopToBottom

            Image {
                Layout.maximumHeight: parent.height - Layout.topMargin - Layout.bottomMargin
                Layout.preferredWidth: 90

                Layout.maximumWidth: 90
                Layout.margins: 5
                Layout.topMargin: {
                    if(_preview === "") {
                        return 10
                    }
                    else return 0
                }
                Layout.bottomMargin: 10
                Layout.rowSpan: 2
                source: _preview ? _preview : Theme.pictogram.path + "/" + root.pictogram
                sourceSize.width: width
                sourceSize.height: height
                fillMode: {
                    if(_preview === "") {
                        return Image.PreserveAspectFit
                    }
                    else return Image.PreserveAspectCrop
                }
            }

            Text {
                Layout.topMargin: 10
                Layout.fillWidth: true
                text: filename
                font.pixelSize: 24
                font.bold: true
                clip: true
            }

            Text {
                Layout.bottomMargin: 10
                text: {
                    if(modelData.type === FMFile.File) return global.localizeDateTime(modelData.mtime)
                    else if(modelData.type === FMFile.Directory) return qsTr("%n item(s)", "number of items in a directory", modelData.childrenCount)
                }
                clip: true
            }

            Text {
                Layout.topMargin: 10
                Layout.maximumWidth: 90
                Layout.alignment: Qt.AlignRight
                clip: true
                function origin(){
                    switch(modelData.origin) {
                    case FMFile.Remote: return qsTr("Remote", "File is stored in remote storage, i.e. a cloud")
                    case FMFile.Local: return qsTr("Local", "File is stored in a local storage")
                    case FMFile.Usb: return qsTr("USB", "File is stored on USB flash disk")
                    case FMFile.PreviousPrints: return qsTr("Local", "File is stored in a local storage")
                    case FMFile.Raucb: return qsTr("Updates", "File is in a repository of raucb files(update bundles)")
                    default: return qsTr("Root", "Directory is a root of the directory tree, its subdirectories are different sources of projects")
                    }
                }
                text: origin()
            }

            Text {
                Layout.bottomMargin: 10
                Layout.maximumWidth: 130
                Layout.alignment: Qt.AlignRight
                clip: true
                text: {
                    if(modelData.type === FMFile.Directory) {
                        return ""
                    }
                    // Assuming it is a file
                    else if(modelData.size >= 0) {
                        var sz_mb = (modelData.size / 1024) / 1024
                        return sz_mb.toFixed(2) + " MB"
                    }
                    else {
                        return qsTr("Unknown")
                    }
                }
            }
        }

        swipe.right: Loader {
            property int modelIndex: index
            property var grandparent: parent
            anchors {
                top: parent.top
                right: parent.right
            }
            // Directories under root(project sources) and ".." items are protected
            active: !projSelectionContext.isAtRoot && index !== 0
            sourceComponent: Rectangle {
                width:  grandparent.height
                height: grandparent.height
                    color: Theme.color.textGrey
                    PictureButton {
                        anchors {
                            centerIn: parent
                        }
                        height: 60
                        width: height
                        pictogram: Theme.pictogram.deleteSimple
                        name: "delete"
                        onClicked: ()=>{ projSelectionContext.remove(modelIndex) }
                        backgroundColor: Theme.color.buttonBackground
                    }
                }
            }



        Rectangle {
            anchors.bottom: parent.bottom
            color: Theme.color.grey
            width: parent.width
            height: 1
        }
    }

    Text {
        id: txtEmptyListIndicator
        text: {
            var help = "No usable projects were found, insert a USB drive or download examples in Settings -> Support."
            return qsTr("No usable projects were found,", help) +
                "\n" +
                qsTr("insert a USB drive or download examples in Settings -> Support.", help)
        }
        color: "grey"
        font.pixelSize: Theme.font.big
        anchors.centerIn: parent
    }

    Component.onCompleted: ()=>{
        if(root.initial_path) {
            projSelectionContext.currentPath = root.initial_path
            console.log("Initial path set to ", root.initial_path)
        }
    }
}

