/*
    Copyright 2021, Prusa Development a.s.

    This file is part of touch-ui

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5

import cz.prusa3d.wizard0 1.0
import cz.prusa3d.wizardcheckmodel 1.0
import PrusaComponents 1.0


PrusaPage {
    screensaver_enabled: false
    ColumnLayout {
        anchors.fill: parent
        visible: wizard0.state == Wizard0.RUNNING

        ListView {
            id: checkList
            Layout.fillHeight: true
            Layout.fillWidth: true
            interactive: true
            clip: true
            model: wizard0.checksModel
            delegate: DelegateWizardCheck {}

            ScrollBar.vertical: ScrollBar {
                active: true
                policy: ScrollBar.AsNeeded
            }

            remove: Transition {
                NumberAnimation { property: "opacity"; from: 1.0; to: 0; duration: 400 }
                NumberAnimation { property: "x";  to: -600; duration: 400 }
            }

            displaced: Transition {
                NumberAnimation { properties: "x,y"; duration: 400; easing.type: Easing.InSine }
            }
        }

        Item {
            id: wizardChecksProgressBar
            Layout.fillWidth: true
            height: 5

            Rectangle {
                height: parent.height
                width: parent.width
                color: "lightgrey"
            }

            Rectangle {
                height: parent.height
                width: parent.width * (wizard0.finishedChecks + wizard0.runningChecks) / wizard0.totalChecks
                color: Theme.color.highlight
            }

            Rectangle {
                height: parent.height
                width: parent.width * wizard0.finishedChecks / wizard0.totalChecks
                color: "green"
            }
        }
    }
}
