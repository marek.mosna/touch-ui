
/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import PrusaComponents 1.0

PrusaPage {
    id: root
    title: qsTr("Are You Sure?")
    name: "YesNoWithPicture"
    pictogram: Theme.pictogram.yes
    back_button_enabled: false
    screensaver_enabled: false
    header_rollup: flick.contentX == 0

    property string image_source: ""
    property string text: ""

    signal yes()
    signal no()
    signal result(bool result)

    function popDialog() { if(StackView.view.currentItem === root) root.StackView.view.pop(); }

    property double _initContentX;
    property int _img_edge: 765
    property bool _movementCanceled: false

    StackView.onActivated: () => {
        _movementCanceled = true // Set the flag
        flick.cancelFlick()
        flick.contentX = _img_edge
        console.log("Showing dialog: ", title)
    }

    Flickable {
        id: flick
        anchors {
            bottom: parent.bottom
            fill: parent
        }
        contentWidth: 800 + _img_edge
        contentX: _img_edge
        interactive: root.image_source ? true : false // Flick only if there is an image
        boundsBehavior: Flickable.StopAtBounds

        onMovementEnded: () => {
           //
           console.log("Confirm flick: movement stopped at " + contentX)
           if(contentX < _img_edge/2 && contentX != 0) {
               if(_movementCanceled) contentX = 0
               else flick.flick(5000, 0)
               console.log("Flicking to 0")
           }
           else if(contentX >= _img_edge/2 && contentX != _img_edge) {
               console.log("Flicking to " + _img_edge)
               if(_movementCanceled) contentX = _img_edge
               else flick.flick(-5000,0)
           }
           else {
               // Revert the movementCanceled flag
               _movementCanceled = false;
           }
        }

        Image {
            id: theImg
            source: root.image_source ? printer0.multimediaImages + "/" + root.image_source : ""
            visible: root.image_source ? true : false
            height: 480
            width: 800
            fillMode: Image.Stretch
            anchors {
                left: parent.left
                bottom: parent.bottom
                margins: 0
            }

            onVisibleChanged: () => {
                shakingAnim.start()
            }
            onSourceChanged: () => {
                onVisibleChanged()
            }


            // On changed source shortly show the new picture, then return back to right part of the screen
            SequentialAnimation {
                id: flickAnim

                NumberAnimation {
                    id: x1
                    target: flick
                    property: "contentX"
                    duration: 700
                    easing.type: Easing.InOutQuad
                    to: 0
                }

                NumberAnimation {
                    id: x2
                    target: theImg
                    property: "dummy"
                    duration: 1000
                    easing.type: Easing.InOutQuad
                    to: 10
                }

                NumberAnimation {
                    id: x3
                    target: flick
                    property: "contentX"
                    duration: 700
                    easing.type: Easing.InOutSine//Easing.InOutQuad
                    to: _img_edge
                }
            }
        }

        RowLayout {
            anchors {
                left: theImg.right
                margins: 10
                bottom: theImg.bottom
                bottomMargin: 5
            }
            visible: root.image_source ? true : false

            Image {
                height: 38
                width:  38
                source: Theme.pictogram.path + "/" + Theme.pictogram.switeRight
                sourceSize.width: width
                sourceSize.height: height
                fillMode: Image.PreserveAspectFit

                SequentialAnimation on y{
                    id: shakingAnim
                    running: flick.contentX == _img_edge
                    loops: 6 //Animation.Infinite

                    PropertyAnimation {
                        easing.type: Easing.InOutCubic
                        to: -5
                    }

                    PropertyAnimation {
                        easing.type: Easing.InOutCubic
                        to: 5
                    }
                }
            }

            Text {
                text: qsTr("Swipe for a picture")
                color: Theme.color.disabledText
                font.pixelSize: 24

                MouseArea {
                    anchors.fill: parent
                    onClicked: () => {
                        flick.contentX = 0
                    }
                }
            }
        }

        Rectangle {
            color: "grey"
            visible: root.image_source ? true : false
            width: 34
            height: 80
            anchors {
                horizontalCenter: theImg.right
                verticalCenter: theImg.bottom
                verticalCenterOffset: -480/2
            }
            radius:10

            Grid {
                anchors.centerIn: parent
                rows: 4
                columns: 2
                rowSpacing: 4
                columnSpacing: 8

                Repeater {
                    model: 6

                    Rectangle {
                        width: 6
                        height: 6
                        radius: 3
                        color: "black"
                    }
                }
            }

            MouseArea {
                anchors.fill: parent
                onClicked: () => {
                    if(flick.contentX == 0) flick.contentX = _img_edge
                    if(flick.contentX == _img_edge) flick.contentX = 0
                }
            }
        }

        Column {
            x: 55 + _img_edge + (root.image_source ? 30 : 0)
            anchors.verticalCenter: parent.bottom
            anchors.verticalCenterOffset: -480/2
            width: 330

            Text {
                width: parent.width
                visible: root.text ? true : false
                text: root.text
                wrapMode: Text.Wrap
            }
        }

        Row {
            anchors {
                right: parent.right
                verticalCenter: parent.bottom
                verticalCenterOffset: -480/2 - 10
                rightMargin: 55
            }
            spacing: 30

            PictureButton {
                id: btnYes
                name: "Yes"
                text: qsTr("Yes")
                pictogram: Theme.pictogram.yes
                backgroundColor: "green"
                onClicked: () => {
                    root.yes()
                    root.result(true)
                }
            }

            PictureButton {
                id: btnNo
                name: "No"
                text: qsTr("No")
                pictogram: Theme.pictogram.no
                backgroundColor: "#ffb8381e"
                onClicked: () => {
                    root.no()
                    root.result(false)
                }
            }
        }
    }

    Row {
        visible: theImg.visible

        Item {
           height: 15
           width: 30

           Rectangle {
               width: 15
               height: 15
               color: flick.contentX == 0 ? Theme.color.highlight : "grey"
               radius: 7
               anchors.margins: 10
           }
       }

       Item {
           height: 15
           width: 30

           Rectangle {
               width: 15
               height: 15
               color: flick.contentX == _img_edge ? Theme.color.highlight : "grey"
               radius: 7
               anchors.margins: 10
           }
       }
       anchors {
            horizontalCenter: parent.horizontalCenter
            bottom: parent.bottom
            bottomMargin: 10
       }
    }

    ImageVersionText {}
}
