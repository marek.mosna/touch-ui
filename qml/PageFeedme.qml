
/*
    Copyright 2019-2020, Prusa Research s.r.o.
    Copyright 2021, Prusa Research a.s.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import PrusaComponents 1.0
//import "3rdparty/qrcode-generator"
/*
 Depends on:
    StackView view

*/

PrusaPage {
    id: root
    title: qsTr("Feed Me")
    name: "feedme"
    pictogram: Theme.pictogram.refill
    screensaver_enabled: false
    back_handler: function(){
        root.exposure.back()
    }

    property string text: qsTr("Manual resin refill.") + "\n\n" +
                          qsTr("Refill the tank up to the 100% mark and press Done.") + "\n\n" +
                          qsTr("If you do not want to refill, press the Back button at top of the screen.")
    property QtObject exposure


    Component.onCompleted: () => {
        if(! root.exposure) root.exposure = printer0.getCurrentExposureObject()
    }

    Column {
        x: 60
        y: 80
        width: 500

        Text {
            text: root.text
            width: parent.width
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        }
    }

    Row {
        id: buttons
        anchors {
            verticalCenter: parent.verticalCenter
            verticalCenterOffset: -40
            right: parent.right
            rightMargin: 55
        }

        PictureButton {
            text: qsTr("Done")
            pictogram: Theme.pictogram.refill
            name: "Done"
            onClicked: () => {
                root.exposure.cont()
            }
        }
    }

    ImageVersionText {}
}
