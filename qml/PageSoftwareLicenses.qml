
/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtQml.Models 2.2
import PrusaComponents 1.0

PrusaPage {
    id: root
    title: qsTr("Software Packages")
    name: "licenses"
    pictogram:  Theme.pictogram.systemInfo

    Column {
        width: parent.width
        anchors.top: parent.top

        RowLayout {
            id: licenseListHeader
            height: 40
            width: parent.width

            // MouseArea makes the "Package name" item clickable(to open search dialog)
            MouseArea {
                Layout.fillWidth: true
                height: parent.height

                PrusaDialog {
                    id: searchDialog
                    height: 100
                    width: 600
                    // List of indexes containing string
                    property var searchResults: []
                    // Index of the current index in the searchResults list
                    property int searchIndex: -1

                    // Highlight item with index idx
                    function highlight(idx) {
                        if(idx >= 0 && idx < licenseModel.count()) {
                            licenseView.positionViewAtIndex(idx, ListView.Contain)
                            licenseView.currentIndex = idx
                        }
                    }

                    // Select the next search result
                    function selectNext() {
                        var newIdx = searchIndex + 1
                        // Check bounds
                        if(newIdx >= 0 && searchResults.length > newIdx) {
                            var idx = searchResults[newIdx]
                            highlight(idx)
                            searchIndex = newIdx
                        }
                    }

                    // Select the previous search result
                    function selectPrev() {
                        var newIdx = searchIndex - 1
                        // Check bounds
                        if(newIdx >= 0 && searchResults.length > newIdx) {
                            var idx = searchResults[newIdx]
                            highlight(idx)
                            searchIndex = newIdx
                        }
                    }

                    function newSearch() {
                        var idxs = licenseView.model.findPackageName(txtPackageName.text)
                        console.log("%1 results for search \"%2\"".arg(idxs.length).arg(txtPackageName.text))
                        searchDialog.searchResults = idxs
                        if(searchDialog.searchResults.length == 0) {
                            searchDialog.searchIndex = -1
                        }
                        else {
                            searchDialog.searchIndex = 0
                            searchDialog.highlight(searchDialog.searchResults[searchDialog.searchIndex])
                        }
                    }


                    RowLayout {
                        anchors.verticalCenter: parent.verticalCenter
                        anchors {
                            fill: parent
                            margins: 10
                            leftMargin: 20
                            rightMargin: 10
                        }
                        spacing: 10

                        TextField {
                            id: txtPackageName
                            Layout.fillWidth: true
                            placeholderText: qsTr("Package Name")
                            placeholderTextColor: Theme.color.placeholderText
                            inputMethodHints: Qt.ImhNoAutoUppercase

                            onAccepted: ()=>{
                                searchDialog.accept()
                            }
                            onEditingFinished: ()=>{
                                if(searchDialog.searchResults.length != 0) {
                                    searchDialog.newSearch()
                                }
                                else {
                                    searchDialog.selectNext()
                                }
                            }
                        }

                        PictureButton {
                            id: btnReject
                            name: "rejectSearch"
                            width: 60
                            height: 60
                            innerMargin: 2
                            pictogram:  Theme.pictogram.cancel
                            onClicked: ()=>{
                                searchDialog.reject()
                            }
                        }

                        PictureButton {
                            id: btnAccept
                            name: "acceptSearch"
                            width: 60
                            height: 60
                            innerMargin: 10
                            pictogram:  Theme.pictogram.search
                            backgroundColor: "green"
                            onClicked: ()=>{
                                searchDialog.accept()
                            }
                        }

                        Item {
                            id: spacer
                            width: 10
                            height: 60
                        }

                        PictureButton {
                            id: btnUp
                            name: "searchUp"
                            width: 60
                            height: 60
                            innerMargin: 10
                            pictogram: Theme.pictogram.moveUp
                            backgroundColor: "green"
                            onClicked: ()=>{
                                txtPackageName.focus = false
                                if(searchDialog.searchResults.length == 0) {
                                    searchDialog.newSearch()
                                }
                                else {
                                    searchDialog.selectPrev()
                                }
                            }
                        }

                        PictureButton {
                            id: btnDown
                            name: "searchDown"
                            width: 60
                            height: 60
                            innerMargin: 10
                            pictogram: Theme.pictogram.moveDown
                            backgroundColor: "green"
                            onClicked: ()=>{
                                txtPackageName.focus = false
                                if(searchDialog.searchResults.length == 0) {
                                    searchDialog.newSearch()
                                }
                                else {
                                    searchDialog.selectNext()
                                }
                            }
                        }
                    }

                    onOpened: ()=>{
                        txtPackageName.text = ""
                        txtPackageName.forceActiveFocus()
                        txtPackageName.focus = true
                    }

                    onAccepted: ()=>{
                        searchDialog.newSearch()
                        searchResults = []
                        searchIndex = -1
                        txtPackageName.focus = false
                    }

                    onRejected: ()=>{
                        searchResults = []
                        searchIndex = -1
                        txtPackageName.focus = false
                    }
                }
                onClicked: ()=>{
                    searchDialog.open()
                }

                Row {
                    height: parent.height

                    Image {
                        source: Theme.pictogram.path + "/" + Theme.pictogram.search
                        sourceSize.width: 24
                        sourceSize.height: 24
                        fillMode: Image.PreserveAspectFit
                    }

                    Text {
                        text: qsTr("Package Name")
                        Layout.fillWidth: true
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        color: Theme.color.highlight
                    }
                }
            }

            Text {
                text: qsTr("Version")
                width: 150
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                Layout.preferredWidth: width
                color: Theme.color.highlight
            }

            Text {
                text: qsTr("License")
                width: 250
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                Layout.preferredWidth: width
                color: Theme.color.highlight

            }
        }

        HorizontalSeparator {
            width: parent.width
        }
    }

    ListView {
        id: licenseView
        height: 378
        width: parent.width
        anchors.bottom: parent.bottom
        clip: true
        model: licenseModel
        spacing: 5
        currentIndex: -1
        delegate: Rectangle {
            color: "black"
            height: 70
            radius: 10
            width: parent.width
            property string textColor: ListView.view.currentIndex === model.index ? Theme.color.highlight : Theme.color.text

            RowLayout {
                anchors {
                    fill: parent
                    leftMargin: 5
                    rightMargin: 5
                }

                ScrollableText {
                    text: model.packageName
                    Layout.fillWidth: true
                    color: textColor
                }

                ScrollableText {
                    text: model.packageVersion
                    width: 150
                    color: textColor
                    Layout.preferredWidth: width
                }

                ScrollableText {
                    text: model.license
                    width: 250
                    color: textColor
                    Layout.preferredWidth: width
                }
            }

            HorizontalSeparator {
                anchors.bottom: parent.bottom
                width: parent.width
            }
        }

        ScrollBar.vertical: ScrollBar {
            active: true
            policy: ScrollBar.AlwaysOn
            minimumSize: 0.3
        }
    }
}
