
/*
    Copyright 2019-2020, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtQml.Models 2.11
import Native 1.0
import PrusaComponents 1.0
import PrusaComponents.Delegates 1.0

PageVerticalList {
    title: qsTr("Network")
    name: "SettingsNetwork"
    pictogram: Theme.pictogram.network
    model: ObjectModel {

        DelegateRefWithStatus {
            name: "Ethernet"
            pictogram: Theme.pictogram.ethernet
            text: qsTr("Ethernet")
            status: {
                if(ethernet.activeConnectionState == WifiNetworkModel.Activated)
                    return "IP: " + ethernet.ipAddress
                else if(ethernet.activeConnectionState == WifiNetworkModel.Unknown)
                    return "IP: " +  global.connectionStateText(ethernet.activeConnectionState)
                else  return global.connectionStateText(ethernet.activeConnectionState)
            }

            onClicked: () => view.push("PageEthernetSettings.qml")
        }


        DelegateRefWithStatus {
            name: "WiFi"
            pictogram: Theme.pictogram.wifi
            text: qsTr("Wi-Fi")
            status: {
                if(wifiNetworkModel.activeConnectionState == WifiNetworkModel.Activated)
                    return "IP: " + wifiNetworkModel.ipAddress
                else if(wifiNetworkModel.activeConnectionState == WifiNetworkModel.Unknown)
                    return "IP: " +  global.connectionStateText(wifiNetworkModel.activeConnectionState)
                else return global.connectionStateText(wifiNetworkModel.activeConnectionState)
            }

            onClicked: () => view.push("PageNetworkWifiList.qml")
        }

        DelegateRef {
            name: "Hotspot"
            text: qsTr("Hotspot")
            pictogram: Theme.pictogram.accessPoint
            onClicked: () => view.push("PageAPSettings.qml")
        }

        DelegateRef {
            name:"SetHostname"
            text: qsTr("Hostname")
            pictogram: Theme.pictogram.hostname
            onClicked: () => view.push("PageSetHostname.qml")
        }

        DelegateRef {
            name:"SetRemoteAccess"
            text: qsTr("Login Credentials")
            pictogram: Theme.pictogram.key
            onClicked: () => view.push("PageSetLoginCredentials.qml")
        }
    }
}
