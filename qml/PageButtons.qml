/*
    Copyright 2021, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5

/** Page template for 4 action PictureButtons in a row */
PrusaPage {
    id: root

    /// Stores the list of buttons
    default property alias content: grid.data

    GridLayout {
        id: grid
        anchors {
            leftMargin: 55
            verticalCenter: parent.verticalCenter
            verticalCenterOffset: -40
            left: parent.left
        }

        columns: 4
        rows: 1
        columnSpacing: 30
        rowSpacing: 5
    }

    ImageVersionText {}
}
