/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12

import PrusaComponents 1.0

Rectangle {
    id: root
    width: 760
    height: 70
    color: "black"
    signal changedOn()
    signal changedOff()


    Text {
        id: label
        text: model.text ? model.text : qsTr("Wi-Fi")
        anchors {
            left: parent.left
            verticalCenter: parent.verticalCenter
            margins: 10
        }
    }

    PrusaSwitch {
        id: sw
        name: model.name;
        anchors {
            right: parent.right
            margins: 10
            verticalCenter: parent.verticalCenter
        }
        fontPixelSize: label.font.pixelSize
        checked: wifiNetworkModel.wifiEnabled

        clickSwitch: false
        onClicked: ()=>{
            if(wifiNetworkModel.wifiEnabled) {
                console.log("Disabling wifi")
                wifiNetworkModel.wifiEnabled = false
            }
            else {
                console.log("Enabling wifi")
                wifiNetworkModel.wifiEnabled = true

            }
        }
    }

    Rectangle {
        height: 1
        width: parent.width
        color: Theme.color.grey
        anchors {
            bottom: parent.bottom
        }
    }
}
