/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import PrusaComponents 1.0

/*
 Depends on:
    StackView view

*/
PrusaPage {
    id: root
    title:  network_name // Calling page will fill-in
    name: "wifipassword"
    back_button_enabled: false
    pictogram: Theme.pictogram.key
    property string network_name // Is supplied by the caller
    property var saved_psk: ""

    property var cancelAction: function(network_name, password, saved_password) {if(view.currentItem === root) {view.pop()}}
    property var okAction: function(network_name, password, saved_password) {if(view.currentItem === root) {view.pop()}}

    StackView.onActivated: ()=>{
        chShowPassword.checked = false
        saved_psk = wifiNetworkModel.getPskForNetwork(network_name)
        txtPassword.text = ""
    }

    StackView.onDeactivated: ()=>{
        saved_psk = ""
        txtPassword.text = ""
    }

    ColumnLayout {
        anchors {
            leftMargin: 55
            rightMargin: 55
            verticalCenter: parent.verticalCenter
            verticalCenterOffset: -70
            left: parent.left
            right: parent.right
        }

        Text {
            Layout.maximumWidth: parent.width
            text: qsTr("Please, enter the correct password for network \"%1\"").arg(network_name)
            wrapMode: Text.WordWrap
            height: 28
            font.pixelSize: 28

        }

        Item {
            height: 20
        }

        RowLayout {
            id: passwordRow
            spacing: 30
            width: 400

            Text {
                Layout.alignment: Qt.AlignVCenter
                text: qsTr("Password") + ":"
                font.pixelSize: 28
            }

            TextField {
                id: txtPassword
                property bool valid:  text.length >= 8 || (text.length == 0 && saved_psk.length >= 8)
                Layout.alignment: Qt.AlignVCenter
                Layout.fillWidth: true
                placeholderTextColor: Theme.color.placeholderText
                placeholderText: (saved_psk !== "") ? qsTr("(unchanged)") : ""


                width: 350
                font.pixelSize: 28
                inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhSensitiveData
                echoMode: {
                    if(chShowPassword.checked) {
                        return TextInput.Normal
                    }
                    else return TextInput.Password
                }
                passwordCharacter: "*"
                passwordMaskDelay: 20 * 1000

                onAccepted: () => {
                    root.okAction(network_name, txtPassword.text, saved_psk)
                }
            }
        }

        RowLayout {
            Layout.alignment: Qt.AlignLeft
            spacing: 10
            CheckBox {
                id: chShowPassword
                focusPolicy: Qt.NoFocus
                font.pixelSize: 28
                font.family: prusaFont.name
                checked: false
                Layout.alignment: Qt.AlignVCenter
            }
            Item {
                width: childrenRect.width
                height: childrenRect.height
                Text {
                    text: qsTr("Show Password")
                    font.pixelSize: 28
                    Layout.alignment: Qt.AlignVCenter
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        chShowPassword.toggle()
                    }
                }
            }
        }
    }


    RowLayout {
        id: buttonRow
        anchors {
            right: parent.right
            rightMargin: 55
            bottom: parent.bottom
            bottomMargin: 60
        }
        spacing: 30

        PictureButton {
            id: btnCancel
            name: "Cancel"
            text: qsTr("Cancel")
            pictogram: Theme.pictogram.no
            backgroundColor: "#ffb8381e"
            onClicked: () => {
                forceActiveFocus()
                root.cancelAction(network_name, txtPassword.text, saved_psk)
            }
        }

        PictureButton {
            id: btnOk
            name: "Ok"
            text: qsTr("Ok")
            pictogram: Theme.pictogram.yes
            backgroundColor: "green"
            onClicked: () => {
                // Only allow a valid password
                if( ! txtPassword.valid) {
                    txtPassword.forceActiveFocus()
                    return
                }
                root.okAction(network_name, txtPassword.text, saved_psk)
            }
        }
    }

    Connections {
        target: SecretPrusa
        function onCanceled_by_networkManager() {
            if(view.currentItem === root) {
                view.pop()
            }
        }
    }

    WarningText {
        item: txtPassword
        y: txtPassword.y + txtPassword.height*1.6
        text: qsTr("PSK must be at least 8 characters long.")
    }

    ImageVersionText {}
}

