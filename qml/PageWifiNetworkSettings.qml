
/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtQml.Models 2.2
import Native 1.0
import PrusaComponents 1.0

/*
 Depends on:
    StackView view

*/
PrusaPage {
    id: root
    title: qsTr("Wireless Settings")
    name: "wifinetworksettings"
    pictogram: Theme.pictogram.wifiSimple
    property bool wait: false

    StackView.onActivated: () => {
        wait = false
        localScratchpad.reload()
    }

    StackView.onDeactivated: () => {
        // Remove text focus on leaving
        inpIP.txtValue.focus = false
        inpDns.txtValue.focus = false
        inpGateway.txtValue.focus = false
        inpMask.txtValue.focus = false
    }

    property int rightColWidth: 400
    property int wifiSignalStrength: wifiNetworkModel.activefignalStrength

    Item {
        id: localScratchpad
        property bool dhcp
        property string ip
        property string gateway
        property string dns
        property string mask
        function reload() {
            wifiNetworkModel.refreshScratchpad()
            dhcp = wifiNetworkModel.scratchpadDhcp
            ip = wifiNetworkModel.scratchpadIp
            gateway = wifiNetworkModel.scratchpadGateway
            dns = wifiNetworkModel.scratchpadDns
            mask = wifiNetworkModel.scratchpadMask
            console.debug("Reloaded scratchpad, dhcp: " + dhcp + " ip: " + ip + " gateway: " + gateway + " dns: " + dns + " mask: " + mask)
        }
        function apply() {
            console.debug("Applying scratchpad, dhcp: " + dhcp + " ip: " + ip + " gateway: " + gateway + " dns: " + dns + " mask: " + mask)
            wifiNetworkModel.scratchpadDhcp = dhcp
            wifiNetworkModel.scratchpadIp = ip
            wifiNetworkModel.scratchpadGateway = gateway
            wifiNetworkModel.scratchpadMask = mask
            wifiNetworkModel.applyScratchpad(
                        function(){console.log("Connection re-configuration succeeded"); root.wait = false},
                        function(e){console.log("Connection re-configuration failed: " + e); root.wait = false}
                        )
        }
    }
    Rectangle { anchors.fill: parent; color: "black"; width: 800} // Background

    ObjectModel {
        id: things
        PrusaPicturePictureButtonItem {
            label: wifiNetworkModel.activeConnectionName
            pictogram: global.pictureOfWifiSignalStrength(wifiSignalStrength)
            connectedText: wifiNetworkModel.activeConnectionState === WifiNetworkModel.Activated ? qsTr("Connected") : qsTr("Disconnected")
        }


        Item {
            height: 20
            width: 760
        }

        Text {
            font.pixelSize: 28
            font.weight: Font.Medium
            text: qsTr("Network Info")
        }


        PrusaSwitchItem {
            id: dhcpSwitch
            name: "WirelessDHCP"
            label: qsTr("DHCP")
            checked: localScratchpad.dhcp
            clickSwitch: false
            onClicked:  () => {
                localScratchpad.dhcp = ! localScratchpad.dhcp
            }
        }

        PrusaTextInput {
            id: inpIP
            label: "IP Address"
            value: localScratchpad.ip
            enabled: ! dhcpSwitch.checked
            regexp: "^(([01]?[0-9]?[0-9]|2([0-4][0-9]|5[0-5]))\.){3}([01]?[0-9]?[0-9]|2([0-4][0-9]|5[0-5]))$"
            txtValue.inputMethodHints: Qt.ImhPreferNumbers
            onAccepted: () => {
               localScratchpad.ip = value
            }
        }

        PrusaTextInput {
            id: inpGateway
            label: "Gateway"
            value: localScratchpad.gateway
            enabled: ! dhcpSwitch.checked
            regexp: "^(([01]?[0-9]?[0-9]|2([0-4][0-9]|5[0-5]))\.){3}([01]?[0-9]?[0-9]|2([0-4][0-9]|5[0-5]))$|$"
            txtValue.inputMethodHints: Qt.ImhPreferNumbers
            onAccepted: () => {
                localScratchpad.gateway = value
            }
        }

        PrusaTextInput {
            id: inpMask
            label: "Mask"
            value: localScratchpad.mask
            enabled: ! dhcpSwitch.checked
            regexp: "^(([01]?[0-9]?[0-9]|2([0-4][0-9]|5[0-5]))\.){3}([01]?[0-9]?[0-9]|2([0-4][0-9]|5[0-5]))$"
            txtValue.inputMethodHints: Qt.ImhPreferNumbers
            onAccepted: () => {
                localScratchpad.mask = value
            }
        }

        PrusaTextInput {
            id: inpDns
            label: "DNS"
            value: localScratchpad.dns
            enabled: ! dhcpSwitch.checked
            regexp: "^(([01]?[0-9]?[0-9]|2([0-4][0-9]|5[0-5]))\.){3}([01]?[0-9]?[0-9]|2([0-4][0-9]|5[0-5]))$|$"
            txtValue.inputMethodHints: Qt.ImhPreferNumbers
            onAccepted: () => {
                localScratchpad.dns = value
            }
        }

        PrusaTextInput {
            id: inpMac
            label: "MAC"
            value: Wifi.hardwareAddress
            enabled: false
            txtValue.inputMethodHints: Qt.ImhPreferNumbers
        }
        Item {
            height: 20
            width: 1
        }

        RowLayout {
            layer.enabled: true
            height: 300
            anchors {
                rightMargin: 20
                leftMargin: 20
            }
            spacing: 30
            Item {
                height: 10
                Layout.fillWidth: true
            }

            PictureButton {
                id: btnApply
                name: "Apply"
                text: qsTr("Apply")
                backgroundColor: "green"
                pictogram: Theme.pictogram.yes
                enabled: inpDns.acceptableInput &&
                         inpGateway.acceptableInput &&
                         inpMask.acceptableInput &&
                         inpIP.acceptableInput || dhcpSwitch.checked


                onClicked: () => {
                    waitOverlay.text = qsTr("Configuring the connection,\nplease wait...", "This is horizontal-center aligned, ideally 2 lines")
                    root.wait = true
                    localScratchpad.apply()
                }
            }

            PictureButton {
                id: btnRevert
                name: "Revert"
                text: qsTr("Revert", "Turn back the changes and go back to the previous configuration.")
                pictogram: Theme.pictogram.no
                onClicked: () => {
                    localScratchpad.reload()
                }
            }

            PictureButton {
                id: btnDeleteNetwork
                name: "DeleteNetwork"
                backgroundColor: "#ffb8381e"
                text: qsTr("Forget network", "Removes all information about the network(settings, passwords,...)")
                pictogram: Theme.pictogram.no
                onClicked: () => root.StackView.view.push(forgetNetDialog)

                Component {
                    id: forgetNetDialog
                    PageYesNoSimple {
                        id: forgetNetDialogRoot
                        name: "ForgetNetworkConfirm"
                        title: qsTr("Forget network?")
                        text: qsTr("Do you really want to forget this network's settings?")
                        onYes: () => {
                                   wifiNetworkModel.removeConnection(wifiNetworkModel.activeConnectionName,
                                                                     function() {
                                                                         var tmpView = forgetNetDialogRoot.StackView.view
                                                                         tmpView.pop()
                                                                         tmpView.pop()
                                                                     },
                                                                     function(err){forgetNetDialogRoot.StackView.view.pop()}
                                                                     )
                               }
                        onNo: () => forgetNetDialogRoot.StackView.view.pop()
                    }
                }
            }
        }
    }

    ListView {
        height: 420
        interactive: true
        anchors {
          bottom: parent.bottom
          left: parent.left
          right: parent.right
          rightMargin: 10
          leftMargin: 10
        }
        clip: true
        model: things// settingsListModel

        ScrollBar.vertical: ScrollBar {
            active: true
            policy: ScrollBar.AlwaysOn
        }
    }

    PrusaWaitOverlay {
        id: waitOverlay
        doWait: root.wait
    }
}
