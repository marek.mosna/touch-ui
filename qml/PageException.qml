/*
    Copyright 2019, Prusa Research s.r.o.
    Copyright 2020, Prusa Research a.s.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5

import "3rdparty/qrcode-generator"

import cz.prusa3d.logs0 1.0
import PrusaComponents 1.0
import cz.prusa3d.sl1.printer0 1.0
import cz.prusa3d.sl1.errorcodestext 1.0
import ErrorsSL1 1.0
import Native 1.0

PrusaPage {
    id: root
    title: code in ErrorcodesText.titles ? ErrorcodesText.titles[code] : qsTr("System Error")
    name: "exception"
    back_button_enabled: false
    screensaver_enabled: false
    pictogram: Theme.pictogram.errorSimple
    header_can_expand: false
    bgColor: Theme.color.errorBackground

    property int code: 10500
    property var params: ({})
    property string simpleUrl:(printer0.printer_model === Printer0.M1 ? "medicalhelp.prusa3d.com" : "help.prusa3d.com/") + LocaleSetting.locale.substring(0,2) + "/" + code
    property string url: "https://" + simpleUrl + printer0.help_page_url

    StackView.onActivated: () => swipeSignException.shake()

    Component.onCompleted: () => global.soundError()

    SwipeView {
        id: swipeException
        anchors.fill: parent
        currentIndex: 0

        PageQrCode {
            id: errorPage
            title: code in ErrorcodesText.titles ? ErrorcodesText.titles[code] : qsTr("Error")
            name: "error"
            screensaver_enabled: false
            header_can_expand: false
            showQrDescription: true
            qrContent: root.url
            qrDescription: simpleUrl
            text: qsTr("Error code:") + " #" + code.toString() + "\n\n" +
                  (ErrorcodesText.messages[root.code] ? global.stringFormat(ErrorcodesText.messages[root.code], root.params) : "") +
                  (root.code === ErrorsSL1.UNKNOWN ? ("\n\n" + root.params.name) : "")
            bgColor: Theme.color.errorBackground
            clip: true

            SwipeSign {
                id: swipeSignException
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                text: qsTr("Swipe to proceed")
                onClicked: {
                    swipeException.incrementCurrentIndex()
                }
                // Lighter grey to make it visible on red background
                textColor: Theme.color.disabledText
            }
        }

        Item {
            clip: true
            GridLayout {
                anchors {
                    leftMargin: 55
                    verticalCenter: parent.verticalCenter
                    verticalCenterOffset: -40
                    left: parent.left
                }

                columns: 4
                rows: 1
                columnSpacing: 30
                rowSpacing: 5

                PictureButton {
                    backgroundColor: Theme.color.backgroundInverse
                    text: qsTr("Save Logs to USB")
                    pictogram: Theme.pictogram.saveInverse
                    onClicked: {
                        logs0.usbSave(
                                    function(){},
                                    function(err){push_error(printer0.last_exception)}
                                    )
                        root.StackView.view.push("PageLogs.qml")
                    }
                }

                PictureButton {
                    backgroundColor: Theme.color.backgroundInverse
                    text: qsTr("Send Logs to Cloud")
                    pictogram: Theme.pictogram.uploadToCloudInverse
                    onClicked: {
                        logs0.serverUpload(
                                    function(){},
                                    function(err){push_error(printer0.last_exception)}
                                    )
                        root.StackView.view.push("PageLogs.qml")
                    }
                }

                PictureButton {
                    backgroundColor: Theme.color.backgroundInverse
                    pictogram: Theme.pictogram.firmwareInverse
                    text: qsTr("Update Firmware")
                    onClicked: root.StackView.view.push("PageSettingsFirmware.qml")
                }

                PictureButton {
                    backgroundColor: Theme.color.backgroundInverse
                    pictogram: Theme.pictogram.powerOffInverse
                    text: qsTr("Turn Off")
                    onClicked: root.StackView.view.push("PagePowerOffDialog.qml")
                }
            }
        }
    }

    PageIndicator {
        id: indicator
        count: swipeException.count
        currentIndex: swipeException.currentIndex

        delegate: Item {
            height: 15
            width: 30
            Rectangle {
                width: 15
                height: 15
                color: index === indicator.currentIndex ? Theme.color.backgroundInverse : Theme.color.background
                radius: 7
                anchors.margins: 10
            }
        }
        anchors.bottom: swipeException.bottom
        anchors.margins: 5
        anchors.horizontalCenter: parent.horizontalCenter
    }
}
