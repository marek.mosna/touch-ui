
/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Controls.Private 1.0
import Native 1.0

import PrusaComponents 1.0
/*
 Depends on:
    StackView view

*/

PrusaPage {
    id: root
    title: qsTr("Set Date")
    name: "setdate"
    pictogram: Theme.pictogram.clock

    property var theDay: new Date()

    Calendar {
        id: cal
        minimumDate: {
            var d = new Date()
            d.setFullYear(d.getFullYear() - 30)
            return d
        }
        maximumDate: {
            var d = new Date()
            d.setFullYear(d.getFullYear() + 30)
            return d
        }
        anchors {
            left: parent.left
            leftMargin: 30
            bottom: parent.bottom
            top: parent.top
            right: btnSetDate.left
            rightMargin: 30
            margins: 30
        }

        style: CalendarStyle {
            /*!
                The Calendar this style is attached to.
            */
            readonly property Calendar control: __control

            /*!
                The color of the grid lines.
            */
            property color gridColor: Theme.color.grey

            /*!
                This property determines the visibility of the grid.

                The default value is \c true.
            */
            property bool gridVisible: true

            /*!
                \internal

                The width of each grid line.
            */
            property real __gridLineWidth: 1

            /*! \internal */
            property color __horizontalSeparatorColor: gridColor

            /*! \internal */
            property color __verticalSeparatorColor: gridColor

            function __cellRectAt(index) {
                return CalendarUtils.cellRectAt(index, control.__panel.columns, control.__panel.rows,
                                                control.__panel.availableWidth, control.__panel.availableHeight, gridVisible ? __gridLineWidth : 0);
            }

            function __isValidDate(date) {
                return date !== undefined
                        && date.getTime() >= control.minimumDate.getTime()
                        && date.getTime() <= control.maximumDate.getTime();
            }

            /*!
                The background of the calendar.

                The implicit size of the calendar is calculated based on the implicit size of the background delegate.
            */
            property Component background: Rectangle {
                color: Theme.color.background
                implicitWidth: Math.max(250, Math.round(TextSingleton.implicitHeight * 14))
                implicitHeight: Math.max(250, Math.round(TextSingleton.implicitHeight * 14))
            }

            /*!
                The navigation bar of the calendar.

                Styles the bar at the top of the calendar that contains the
                next month/previous month buttons and the selected date label.

                The properties provided to the delegate are:
                \table
                    \row \li readonly property string \b styleData.title
                         \li The title of the calendar.
                \endtable
            */
            property Component navigationBar: Rectangle {
                height: Math.round(TextSingleton.implicitHeight * 4.0)
                color: Theme.color.background

                Rectangle {
                    color: Theme.color.grey
                    height: 1
                    width: parent.width
                }

                Rectangle {
                    anchors.bottom: parent.bottom
                    height: 1
                    width: parent.width
                    color: Theme.color.grey
                }
                PictureButton {
                    id: previousMonth
                    name: "PreviousMonth"
                    width: parent.height
                    height: width
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    pictogram: Theme.pictogram.fastBackward
                    innerMargin: 3
                    onClicked: ()=>{ control.showPreviousMonth() }
                    backgroundColor: Theme.color.background
                }
                Label {
                    id: dateText
                    text: styleData.title
                    elide: Text.ElideRight
                    horizontalAlignment: Text.AlignHCenter
                    font.pixelSize: Math.min(parent.height/2, parent.width/2)
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: previousMonth.right
                    anchors.leftMargin: 2
                    anchors.right: nextMonth.left
                    anchors.rightMargin: 2
                    color: Theme.color.text
                    font.family: prusaFont.name
                }
                PictureButton {
                    id: nextMonth
                    name: "NextMonth"
                    width: parent.height
                    height: width
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right: parent.right
                    pictogram: Theme.pictogram.fastForward
                    innerMargin: 3
                    onClicked: ()=>{ control.showNextMonth() }
                    backgroundColor: Theme.color.background
                }
            }

            /*!
                The delegate that styles each date in the calendar.

                The properties provided to each delegate are:
                \table
                    \row \li readonly property date \b styleData.date
                        \li The date this delegate represents.
                    \row \li readonly property bool \b styleData.selected
                        \li \c true if this is the selected date.
                    \row \li readonly property int \b styleData.index
                        \li The index of this delegate.
                    \row \li readonly property bool \b styleData.valid
                        \li \c true if this date is greater than or equal to than \l {Calendar::minimumDate}{minimumDate} and
                            less than or equal to \l {Calendar::maximumDate}{maximumDate}.
                    \row \li readonly property bool \b styleData.today
                        \li \c true if this date is equal to today's date.
                    \row \li readonly property bool \b styleData.visibleMonth
                        \li \c true if the month in this date is the visible month.
                    \row \li readonly property bool \b styleData.hovered
                        \li \c true if the mouse is over this cell.
                            \note This property is \c true even when the mouse is hovered over an invalid date.
                    \row \li readonly property bool \b styleData.pressed
                        \li \c true if the mouse is pressed on this cell.
                            \note This property is \c true even when the mouse is pressed on an invalid date.
                \endtable
            */
            property Component dayDelegate: Rectangle {
                anchors.fill: parent
                anchors.leftMargin: (!addExtraMargin || control.weekNumbersVisible) && styleData.index % CalendarUtils.daysInAWeek === 0 ? 0 : -1
                anchors.rightMargin: !addExtraMargin && styleData.index % CalendarUtils.daysInAWeek === CalendarUtils.daysInAWeek - 1 ? 0 : -1
                anchors.bottomMargin: !addExtraMargin && styleData.index >= CalendarUtils.daysInAWeek * (CalendarUtils.weeksOnACalendarMonth - 1) ? 0 : -1
                anchors.topMargin: styleData.selected ? -1 : 0
                color: {
                    if(styleData.date !== undefined) {

                        if( styleData.selected) {
                            return selectedDateBgColor
                        }
                        else if(theDay.getDate() === styleData.date.getDate() // Not selected, but current
                                && theDay.getMonth() === styleData.date.getMonth()) {
                            return currentDateBgColor
                        }
                    }
                    return "transparent"
                }

                readonly property bool addExtraMargin: control.frameVisible && styleData.selected
                readonly property color sameMonthDateTextColor: "white"
                readonly property color selectedDateBgColor: "white"
                readonly property color selectedDateTextColor: Theme.color.highlight
                readonly property color differentMonthDateTextColor: "grey"
                readonly property color invalidDateColor: Theme.color.disabledText
                readonly property color currentDateTextColor: "white"
                readonly property color currentDateBgColor: Theme.color.highlight


                Text {
                    id: dayDelegateText
                    font.family: prusaFont.name
                    text: styleData.date.getDate()
                    anchors.centerIn: parent
                    font.pixelSize: Math.min(parent.height/2, parent.width/2)
                    // This won't be dynamically updated on change of the current date,
                    // but that should be acceptable.
                    color: {
                        var theColor = "white"
                        if(! styleData.valid) theColor = invalidDateColor
                        if(theDay.getDate() === styleData.date.getDate()
                                && theDay.getMonth() === styleData.date.getMonth())
                        {
                            theColor = currentDateTextColor
                        }
                        if(styleData.selected) {
                            theColor = selectedDateTextColor
                        }

                        return theColor
                    }
                }
            }

            /*!
                The delegate that styles each weekday.

                The height of the weekday row is calculated based on the maximum implicit height of the delegates.

                The properties provided to each delegate are:
                \table
                    \row \li readonly property int \b styleData.index
                         \li The index (0-6) of the delegate.
                    \row \li readonly property int \b styleData.dayOfWeek
                         \li The day of the week this delegate represents. Possible values:
                             \list
                             \li \c Locale.Sunday
                             \li \c Locale.Monday
                             \li \c Locale.Tuesday
                             \li \c Locale.Wednesday
                             \li \c Locale.Thursday
                             \li \c Locale.Friday
                             \li \c Locale.Saturday
                             \endlist
                \endtable
            */
            property Component dayOfWeekDelegate: Item {}

            /*!
                The delegate that styles each week number.

                The width of the week number column is calculated based on the maximum implicit width of the delegates.

                The properties provided to each delegate are:
                \table
                    \row \li readonly property int \b styleData.index
                         \li The index (0-5) of the delegate.
                    \row \li readonly property int \b styleData.weekNumber
                         \li The number of the week this delegate represents.
                \endtable
            */
            property Component weekNumberDelegate: Rectangle {
                implicitWidth: Math.round(TextSingleton.implicitHeight * 2)
                Label {
                    text: styleData.weekNumber
                    anchors.centerIn: parent
                    color: Theme.color.grey
                    font.family: prusaFont.name
                }
            }

            /*! \internal */
            property Component panel: Item {
                id: panelItem

                implicitWidth: backgroundLoader.implicitWidth
                implicitHeight: backgroundLoader.implicitHeight

                property alias navigationBarItem: navigationBarLoader.item

                property alias dayOfWeekHeaderRow: dayOfWeekHeaderRow

                readonly property int weeksToShow: 6
                readonly property int rows: weeksToShow
                readonly property int columns: CalendarUtils.daysInAWeek

                // The combined available width and height to be shared amongst each cell.
                readonly property real availableWidth: viewContainer.width
                readonly property real availableHeight: viewContainer.height

                property int hoveredCellIndex: -1
                property int pressedCellIndex: -1
                property int pressCellIndex: -1
                property var pressDate: null

                Rectangle {
                    anchors.fill: parent
                    color: "transparent"
                    border.color: gridColor
                    visible: control.frameVisible
                }

                Item {
                    id: container
                    anchors.fill: parent
                    anchors.margins: control.frameVisible ? 1 : 0

                    Loader {
                        id: backgroundLoader
                        anchors.fill: parent
                        sourceComponent: background
                    }

                    Loader {
                        id: navigationBarLoader
                        anchors.left: parent.left
                        anchors.right: parent.right
                        anchors.top: parent.top
                        sourceComponent: navigationBar
                        active: control.navigationBarVisible

                        property QtObject styleData: QtObject {

                            readonly property string title: Qt.locale(LocaleSetting.locale).standaloneMonthName(control.visibleMonth)
                                                            + new Date(control.visibleYear, control.visibleMonth, 1).toLocaleDateString(control.locale, " yyyy")
                        }
                    }

                    Row {
                        id: dayOfWeekHeaderRow
                        anchors.top: navigationBarLoader.bottom
                        anchors.left: parent.left
                        anchors.leftMargin: (control.weekNumbersVisible ? weekNumbersItem.width : 0)
                        anchors.right: parent.right
                        spacing: gridVisible ? __gridLineWidth : 0

                        Repeater {
                            id: repeater
                            model: CalendarHeaderModel {
                                locale: Qt.locale(LocaleSetting.locale)
                            }
                            Loader {
                                id: dayOfWeekDelegateLoader
                                sourceComponent: dayOfWeekDelegate
                                width: __cellRectAt(index).width

                                readonly property int __index: index
                                readonly property var __dayOfWeek: dayOfWeek

                                property QtObject styleData: QtObject {
                                    readonly property alias index: dayOfWeekDelegateLoader.__index
                                    readonly property alias dayOfWeek: dayOfWeekDelegateLoader.__dayOfWeek
                                }
                            }
                        }
                    }

                    Rectangle {
                        id: topGridLine
                        color: __horizontalSeparatorColor
                        width: parent.width
                        height: __gridLineWidth
                        visible: gridVisible
                        anchors.top: dayOfWeekHeaderRow.bottom
                    }

                    Row {
                        id: gridRow
                        width: weekNumbersItem.width + viewContainer.width
                        height: viewContainer.height
                        anchors.top: topGridLine.bottom

                        Column {
                            id: weekNumbersItem
                            visible: control.weekNumbersVisible
                            height: viewContainer.height
                            spacing: gridVisible ? __gridLineWidth : 0
                            Repeater {
                                id: weekNumberRepeater
                                model: panelItem.weeksToShow

                                Loader {
                                    id: weekNumberDelegateLoader
                                    height: __cellRectAt(index * panelItem.columns).height
                                    sourceComponent: weekNumberDelegate

                                    readonly property int __index: index
                                    property int __weekNumber: control.__model.weekNumberAt(index)

                                    Connections {
                                        target: control
                                        function onVisibleMonthChanged() {
                                            __weekNumber = control.__model.weekNumberAt(index)
                                        }
                                        function onVisibleYearChanged() {
                                            __weekNumber = control.__model.weekNumberAt(index)
                                        }
                                    }

                                    Connections {
                                        target: control.__model
                                        function onCountChanged(){
                                            __weekNumber = control.__model.weekNumberAt(index)
                                        }
                                    }

                                    property QtObject styleData: QtObject {
                                        readonly property alias index: weekNumberDelegateLoader.__index
                                        readonly property int weekNumber: weekNumberDelegateLoader.__weekNumber
                                    }
                                }
                            }
                        }

                        Rectangle {
                            id: separator
                            anchors.topMargin: - dayOfWeekHeaderRow.height - 1
                            anchors.top: weekNumbersItem.top
                            anchors.bottom: weekNumbersItem.bottom

                            width: __gridLineWidth
                            color: __verticalSeparatorColor
                            visible: control.weekNumbersVisible
                        }

                        // Contains the grid lines and the grid itself.
                        Item {
                            id: viewContainer
                            width: container.width - (control.weekNumbersVisible ? weekNumbersItem.width + separator.width : 0)
                            height: container.height - navigationBarLoader.height - dayOfWeekHeaderRow.height - topGridLine.height

                            Repeater {
                                id: verticalGridLineRepeater
                                model: panelItem.columns - 1
                                delegate: Rectangle {
                                    x: __cellRectAt(index + 1).x - __gridLineWidth
                                    y: 0
                                    width: __gridLineWidth
                                    height: viewContainer.height
                                    color: gridColor
                                    visible: gridVisible
                                }
                            }

                            Repeater {
                                id: horizontalGridLineRepeater
                                model: panelItem.rows - 1
                                delegate: Rectangle {
                                    x: 0
                                    y: __cellRectAt((index + 1) * panelItem.columns).y - __gridLineWidth
                                    width: viewContainer.width
                                    height: __gridLineWidth
                                    color: gridColor
                                    visible: gridVisible
                                }
                            }

                            MouseArea {
                                id: mouseArea
                                anchors.fill: parent

                                hoverEnabled: Settings.hoverEnabled

                                function cellIndexAt(mouseX, mouseY) {
                                    var viewContainerPos = viewContainer.mapFromItem(mouseArea, mouseX, mouseY);
                                    var child = viewContainer.childAt(viewContainerPos.x, viewContainerPos.y);
                                    // In the tests, the mouseArea sometimes gets picked instead of the cells,
                                    // probably because stuff is still loading. To be safe, we check for that here.
                                    return child && child !== mouseArea ? child.__index : -1;
                                }

                                onEntered: ()=>{
                                    hoveredCellIndex = cellIndexAt(mouseX, mouseY);
                                    if (hoveredCellIndex === undefined) {
                                        hoveredCellIndex = cellIndexAt(mouseX, mouseY);
                                    }

                                    var date = view.model.dateAt(hoveredCellIndex);
                                    if (__isValidDate(date)) {
                                        control.hovered(date);
                                    }
                                }

                                onExited: ()=>{
                                    hoveredCellIndex = -1;
                                }

                                onPositionChanged: (mouse)=>{
                                    var indexOfCell = cellIndexAt(mouse.x, mouse.y);
                                    var previousHoveredCellIndex = hoveredCellIndex;
                                    hoveredCellIndex = indexOfCell;
                                    if (indexOfCell !== -1) {
                                        var date = view.model.dateAt(indexOfCell);
                                        if (__isValidDate(date)) {
                                            if (hoveredCellIndex !== previousHoveredCellIndex)
                                                control.hovered(date);

                                            // The date must be different for the pressed signal to be emitted.
                                            if (pressed && date.getTime() !== control.selectedDate.getTime()) {
                                                control.pressed(date);

                                                // You can't select dates in a different month while dragging.
                                                if (date.getMonth() === control.selectedDate.getMonth()) {
                                                    control.selectedDate = date;
                                                    pressedCellIndex = indexOfCell;
                                                }
                                            }
                                        }
                                    }
                                }

                                onPressed: (mouse)=>{
                                    pressCellIndex = cellIndexAt(mouse.x, mouse.y);
                                    pressDate = null;
                                    if (pressCellIndex !== -1) {
                                        var date = view.model.dateAt(pressCellIndex);
                                        pressedCellIndex = pressCellIndex;
                                        pressDate = date;
                                        if (__isValidDate(date)) {
                                            control.selectedDate = date;
                                            control.pressed(date);
                                        }
                                    }
                                }

                                onReleased: (mouse)=>{
                                    var indexOfCell = cellIndexAt(mouse.x, mouse.y);
                                    if (indexOfCell !== -1) {
                                        // The cell index might be valid, but the date has to be too. We could let the
                                        // selected date validation take care of this, but then the selected date would
                                        // change to the earliest day if a day before the minimum date is clicked, for example.
                                        var date = view.model.dateAt(indexOfCell);
                                        if (__isValidDate(date)) {
                                            control.released(date);
                                        }
                                    }
                                    pressedCellIndex = -1;
                                }

                                onClicked: (mouse)=>{
                                    var indexOfCell = cellIndexAt(mouse.x, mouse.y);
                                    if (indexOfCell !== -1 && indexOfCell === pressCellIndex) {
                                        if (__isValidDate(pressDate))
                                            control.clicked(pressDate);
                                    }
                                }

                                onDoubleClicked: (mouse)=>{
                                    var indexOfCell = cellIndexAt(mouse.x, mouse.y);
                                    if (indexOfCell !== -1) {
                                        var date = view.model.dateAt(indexOfCell);
                                        if (__isValidDate(date))
                                            control.doubleClicked(date);
                                    }
                                }

                                onPressAndHold: (mouse)=>{
                                    var indexOfCell = cellIndexAt(mouse.x, mouse.y);
                                    if (indexOfCell !== -1 && indexOfCell === pressCellIndex) {
                                        var date = view.model.dateAt(indexOfCell);
                                        if (__isValidDate(date))
                                            control.pressAndHold(date);
                                    }
                                }
                            }

                            Connections {
                                target: control
                                function onSelectedDateChanged() {
                                    view.selectedDateChanged()
                                }
                            }

                            Repeater {
                                id: view

                                property int currentIndex: -1

                                model: control.__model

                                Component.onCompleted: ()=> selectedDateChanged()

                                function selectedDateChanged() {
                                    if (model !== undefined && model.locale !== undefined) {
                                        currentIndex = model.indexAt(control.selectedDate);
                                    }
                                }

                                delegate: Loader {
                                    id: delegateLoader

                                    x: __cellRectAt(index).x
                                    y: __cellRectAt(index).y
                                    width: __cellRectAt(index).width
                                    height: __cellRectAt(index).height
                                    sourceComponent: dayDelegate

                                    readonly property int __index: index
                                    readonly property date __date: date
                                    // We rely on the fact that an invalid QDate will be converted to a Date
                                    // whose year is -4713, which is always an invalid date since our
                                    // earliest minimum date is the year 1.
                                    readonly property bool valid: __isValidDate(date)

                                    property QtObject styleData: QtObject {
                                        readonly property alias index: delegateLoader.__index
                                        readonly property bool selected: control.selectedDate.getFullYear() === date.getFullYear() &&
                                                                         control.selectedDate.getMonth() === date.getMonth() &&
                                                                         control.selectedDate.getDate() === date.getDate()
                                        readonly property alias date: delegateLoader.__date
                                        readonly property bool valid: delegateLoader.valid
                                        // TODO: this will not be correct if the app is running when a new day begins.
                                        readonly property bool today: date.getTime() === new Date().setHours(0, 0, 0, 0)
                                        readonly property bool visibleMonth: date.getMonth() === control.visibleMonth
                                        readonly property bool hovered: panelItem.hoveredCellIndex == index
                                        readonly property bool pressed: panelItem.pressedCellIndex == index
                                        // todo: pressed property here, clicked and doubleClicked in the control itself
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }



    PictureButton {
        id:btnSetDate
        pictogram: Theme.pictogram.yes
        name: "settime"
        text: qsTr("Set")
        backgroundColor: "green"
        anchors {
            rightMargin: 55
            right: parent.right
            verticalCenter: parent.verticalCenter
            verticalCenterOffset: -40
        }
        onClicked: ()=>{
            var newTime = new Date(timedate.time)
            newTime.setDate(cal.selectedDate.getDate())
            newTime.setMonth(cal.selectedDate.getMonth())
            newTime.setFullYear(cal.selectedDate.getFullYear())
            timedate.setTime(newTime,
                             function(){
                                 console.log("Set time to " + newTime)
                                 theDay = newTime
                             },
                             function(){console.log("Could not set time to " + newTime)}
                             )
        }
    }
    Rectangle {
        id: resetInfo
        opacity: 0
        width: 400
        height: 100
        color: "black"
        border.color: "grey"
        radius: 10
        border.width: 2
        anchors {
            bottom: parent.bottom
            horizontalCenter: parent.horizontalCenter
        }
        Text {
            anchors.centerIn: parent
            width: parent.width - 40
            text: qsTr("Only tap 15 times to reset date to %1-01-01".arg(AppInfo.buildYear))
            color: Theme.color.highlight
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        }
        SequentialAnimation {
            id: animResetInfo

            NumberAnimation {
                target: resetInfo
                property: "opacity"
                from: 0
                to: 1
                duration: 300
            }
            NumberAnimation {
                target: resetInfo
                property: "opacity"
                to: 1
                duration: 5000
            }
            NumberAnimation {
                target: resetInfo
                property: "opacity"
                from: 1
                to: 0
                duration: 300
            }

        }
    }

    MouseArea {

        property bool tapping: false
        property int tapCount: 0
        anchors {
            right: parent.right
            bottom: parent.bottom
        }
        width: 150
        height: 150

        Timer {
            id: tappingTimeOut
            interval: 5000
            running: false
            repeat: false
            onTriggered:  {
                parent.tapping = false
                parent.tapCount = 0

            }
        }

        onClicked: ()=>{
            if(tapping == false) {
                tapping = true
                tapCount = 1
                tappingTimeOut.stop()
                tappingTimeOut.start()
            }
            else {
                tapCount += 1
                console.log("Only tap " + (15 - tapCount) +  " more times to reset date to %1-01-01".arg(AppInfo.buildYear))
                if( tapCount === 3) {
                    animResetInfo.start()
                }

                if(tapCount >= 15) {
                    tapping = false
                    tapCount = 0
                    tappingTimeOut.stop()
                    var newTime = new Date(timedate.time)
                    newTime.setFullYear(AppInfo.buildYear)
                    newTime.setMonth(0)
                    newTime.setDate(1)
                    printer0.beep_button()
                    timedate.setTime(newTime,
                                     function(){
                                         console.log("Set time to " + newTime)
                                         theDay = newTime
                                     },
                                     function(){console.log("Could not set time to " + newTime)}
                                     )

                }
            }
        }
    }
}
