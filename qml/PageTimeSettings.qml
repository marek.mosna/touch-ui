
/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtQml.Models 2.11
import PrusaComponents 1.0
import PrusaComponents.Delegates 1.0
import Native 1.0
/*
 Depends on:
    StackView view
*/

PageVerticalList {
    id: root
    title: qsTr("Time Settings")
    name: "timesettings"
    pictogram: Theme.pictogram.clock
    interactive: false

    property int textPixelSize: 24

    ObjectModel{
        DelegateSwitch {
            id: ntpSwitch
            name: "ntpEnableDisable"
            pictogram: Theme.pictogram.settings
            text: qsTr("Use NTP")

            onClicked: ()=>{
                if(ntpSwitch.checked) {
                    console.log("Disabling NTP")
                }
                else {
                    console.log("Enabling NTP")
                }
                timedate.setNtp(! ntpSwitch.checked)
            }
            Component.onCompleted: () => { ntpSwitch.checked = timedate.ntp }
            Connections {
                target: timedate
                function onNtpChanged() {
                    ntpSwitch.checked = timedate.ntp
                }
            }
        }

        DelegateRef {
            name: "Time"
            pictogram: Theme.pictogram.clock
            enabled: !ntpSwitch.checked
            textComponent: RowLayout{
                Text {
                    text: qsTr("Time")
                }

                Item {
                    height: 1
                    Layout.fillWidth: true
                }

                Text {
                    text: global.localizeTime(timedate.time)
                }
            }
            onClicked: () => view.push("PageSetTime.qml")
        }

        DelegateRef {
            name: "Date"
            pictogram: Theme.pictogram.clock
            enabled: !ntpSwitch.checked
            textComponent: RowLayout{
                Text {
                    text: qsTr("Date")
                }

                Item {
                    height: 1
                    Layout.fillWidth: true
                }

                Text {
                    text: global.localizeDate(timedate.time)
                }
            }
            onClicked: () => view.push("PageSetDate.qml")
        }

        DelegateRef {
            name: "Timezone"
            pictogram: Theme.pictogram.timezone
            enabled: true
            textComponent: RowLayout{
                Text {
                    text: qsTr("Timezone")
                }

                Item {
                    height: 1
                    Layout.fillWidth: true
                }

                Text {
                    text: timedate.timezone
                }
            }
            onClicked: () => view.push("PageSetTimezone.qml")
        }

        DelegateSelect {
            id: timeFormatSelector
            name: "TimeFormat"
            text: qsTr("Time Format")
            pictogram: Theme.pictogram.settings

            // Order is important, must be the same as global.timeFormat
            items: [
                qsTr("Native", "Default time format determined by the locale"),
                qsTr("12-hour", "12h time format"),
                qsTr("24-hour", "24h time format")
            ]


            onIndexChanged: () => {
                                if(global.userSettings.time_format !== timeFormatSelector.index) global.userSettings.time_format = timeFormatSelector.index
                            }

            Connections {
                target: global.userSettings
                function onTime_formatChanged() {
                    timeFormatSelector.index = global.userSettings.time_format
                }
            }

            Component.onCompleted: () => timeFormatSelector.index = global.userSettings.time_format
        }

        DelegateRef {
            pictogram: Theme.pictogram.timezone
            enabled: false
            visible: ntpSwitch.checked
            textComponent: Text {
                text: qsTr("Automatic time settings using NTP ...")
                font.italic: true
                color: "grey"
            }
            arrowVisible: false
        }
    }
}



























