/*
    Copyright 2020, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import ErrorsSL1 1.0
import PrusaComponents 1.0

SwipeDelegate {
    id: root
    property string pictogram: ""
    property real progress: 0.0
    property bool done: false
    property int errorCode: ErrorsSL1.NONE
    signal activated()

    width: parent.width
    height: 80
    onClicked: () => {
        printer0.beep_button()
        root.activated()
    }

    background: Rectangle {
        color: root.swipe.position === -1.0 ? Qt.darker(Theme.color.backgroundInverse, 1.5) : Theme.color.backgroundInverse
    }
    contentItem: ColumnLayout {
        spacing: 15

        RowLayout {
            id: itemRow
            height: 80
            width: parent.width

            Item {
                width: 60
                height: 50
                Layout.alignment: Qt.AlignVCenter | Qt.AlignLeft

                Image {
                    anchors.centerIn: parent
                    height: 50
                    sourceSize.width: width
                    sourceSize.height: height
                    fillMode: Image.PreserveAspectFit
                    source: Theme.pictogram.path + "/" + root.pictogram
                }
            }

            ColumnLayout {
                height: parent.height
                Layout.fillWidth: true
                spacing: 4

                RowLayout {
                    width: parent.width
                    clip: true

                    Text {
                        id: notificationTitleText
                        text: root.text
                        clip: true
                        Layout.fillWidth: true
                        color: Theme.color.textInverse
                    }

                    Text {
                        visible: errorCode === ErrorsSL1.NONE
                        text: (progress*100).toFixed(0) + " %"
                        color: Theme.color.textInverse
                    }

                    Text {
                        id: errorNotice
                        Layout.alignment: Qt.AlignRight
                        color: Theme.color.red
                        text: {
                            if(errorCode !== ErrorsSL1.NONE) {
                                return qsTr("Error: %1").arg(describeError(errorCode))
                            }
                            else {
                               return ""
                            }
                        }
                        function describeError(errorCode) {
                            var ret = "#" + errorCode
                            switch(errorCode) {
                            case ErrorsSL1.NOT_ENOUGH_INTERNAL_SPACE:
                                return ret + " " + qsTr("Storage full")
                            case ErrorsSL1.FILE_NOT_FOUND:
                                return ret + " " + qsTr("File not found")
                            case ErrorsSL1.FILE_ALREADY_EXISTS:
                                return ret + " " + qsTr("Already exists")
                            case ErrorsSL1.INVALID_EXTENSION:
                                return ret + " " + qsTr("Invalid extension")
                            case ErrorsSL1.PROJECT_ERROR_CANT_READ:
                                return ret + " " + qsTr("Can't read")
                            default:
                                return ret
                            }
                        }
                    }
                }

                ProgressBar {
                    id: upDownProgress
                    Layout.fillWidth: true
                    height: 6
                    maximum: 1.0
                    value: progress
                    visible:  errorCode === ErrorsSL1.NONE ? true : progress !== 0
                    SequentialAnimation {
                        id: progressAnimation
                        running:  false
                        //alwaysRunToEnd: true
                        loops: Animation.Infinite
                        PropertyAnimation {
                            easing.type: Easing.InOutQuart
                            target: upDownProgress
                            properties: "value"
                            from: 0.0
                            to: 0.1
                            duration: 800
                        }
                        PropertyAnimation {
                            easing.type: Easing.InOutQuad
                            target: upDownProgress
                            properties: "value"
                            from: 0.1
                            to: 0.0
                            duration: 800
                        }
                    }
                    states: [
                        State {
                            when: progress === 0.0 && ! done
                            name: "animating"
                            PropertyChanges {
                                target: progressAnimation
                                running: true
                            }
                            PropertyChanges {
                                target: notificationTitleText
                                text: root.text
                            }
                        },
                        State {
                            when: done
                            name: "done"
                            PropertyChanges {
                                target: upDownProgress
                                value: 1.0
                            }
                            PropertyChanges {
                                target: notificationTitleText
                                text: root.text
                            }
                        }
                    ]
                }
            }

            Image {
                Layout.alignment: Qt.AlignRight
                visible: false
                height: 50
                width: height
                sourceSize.width: width
                sourceSize.height: height
                fillMode: Image.PreserveAspectFit
                source: Theme.pictogram.path + "/" + Theme.pictogram.rightGrey
            }
        }
    }

    Rectangle {
        width: parent.width
        height: 1
        color: Theme.color.grey
        anchors {
            bottom: parent.bottom
        }
    }

    swipe.right: Rectangle {
        width: root.height
        height: root.height
        color: Qt.darker(Theme.color.backgroundInverse, 1.8)
        anchors {
            right: parent.right
        }

        PictureButton {
            anchors {
                centerIn: parent
            }
            height: 60
            width: height
            pictogram: Theme.pictogram.cancelInverse
            name: "Delete"
            background: null
            innerMargin: 0
            onClicked: () => {
                notificationModel.removeRows(model.index, 1)
            }
        }
    }
    swipe.onCompleted: () => {
        if(swipe.position === -1.0) {
            notificationModel.removeRows(model.index, 1)
        }
    }
}
