/*
    Copyright 2020, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import PrusaComponents 1.0

Item {
    id: root
    property string direction: "right"
    property alias text: txtSign.text
    property bool exception: false
    property alias textColor: txtSign.color

    function shake() {shakingAnim.start(); shakingAnim2.start()}
    signal clicked()
    width: swipeRow.width
    height: swipeRow.height
    anchors {
        leftMargin: 20
        rightMargin: 20
    }

    RowLayout {
        id: swipeRow
        anchors {
            right: parent.right
            margins: 10
            bottom: parent.bottom
            bottomMargin: 5
        }

        Image {
            height: 38
            width:  38
            visible: root.direction === "left"
            source: root.exception ? Theme.pictogram.path + "/" + Theme.pictogram.swipeRightSimple : Theme.pictogram.path + "/" + Theme.pictogram.swipeRight
            sourceSize.width: width
            sourceSize.height: height
            fillMode: Image.PreserveAspectFit
            SequentialAnimation on y {
                id: shakingAnim2
                loops: 1

                PropertyAnimation {
                    easing.type: Easing.InOutQuart
                    to: -5
                }
                PropertyAnimation {
                    easing.type: Easing.InOutQuart
                    to: 5
                }
                PropertyAnimation {
                    easing.type: Easing.InOutQuart
                    to: -5
                }
                PropertyAnimation {
                    easing.type: Easing.InOutQuart
                    to: 5
                }
                PropertyAnimation {
                    easing.type: Easing.InOutQuart
                    to: -5
                }
                PropertyAnimation {
                    easing.type: Easing.InOutQuart
                    to: 5
                }


                // Return to the neutral position
                PropertyAnimation {
                    easing.type: Easing.InOutQuart
                    to: 0
                }
            }
        }

        Text {
            id: txtSign
            text: qsTr("Swipe to confirm")
            color: root.exception ? "white" : "grey"
            font.pixelSize: 24

            width: 150
            clip: true
        }
        Image {
            height: 38
            width:  38
            visible: root.direction === "right"
            source: root.exception ? Theme.pictogram.path + "/" + Theme.pictogram.swipeRightSimple : Theme.pictogram.path + "/" + Theme.pictogram.swipeRight
            rotation: 180
            sourceSize.width: width
            sourceSize.height: height
            fillMode: Image.PreserveAspectFit
            SequentialAnimation on y {
                id: shakingAnim
                loops: 1

                PropertyAnimation {
                    easing.type: Easing.InOutQuart
                    to: -5
                }
                PropertyAnimation {
                    easing.type: Easing.InOutQuart
                    to: 5
                }
                PropertyAnimation {
                    easing.type: Easing.InOutQuart
                    to: -5
                }
                PropertyAnimation {
                    easing.type: Easing.InOutQuart
                    to: 5
                }
                PropertyAnimation {
                    easing.type: Easing.InOutQuart
                    to: -5
                }
                PropertyAnimation {
                    easing.type: Easing.InOutQuart
                    to: 5
                }

                // Return to the neutral position
                PropertyAnimation {
                    easing.type: Easing.InOutQuart
                    to: 0
                }
            }
        }
    }
    MouseArea {
        anchors.fill: parent
        onClicked:()=>{
            root.clicked()
        }
    }
}
