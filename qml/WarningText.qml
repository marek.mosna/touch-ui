
/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import PrusaComponents 1.0
Rectangle {
    property var item: undefined
    property alias text: txt.text
    property bool activeWhen: item.activeFocus
    visible: item == undefined ? false : ((!item.valid) && activeWhen)
    width: 450
    height: 70
    border.width: 1
    border.color: "grey"
    z:99
    color: "#D7000000"
    radius: 10
    anchors {
        bottomMargin: 4
        topMargin: 20
        horizontalCenter: parent.horizontalCenter
    }

    Text {
        id: txt
        width: 400
        color: Theme.color.red
        wrapMode: Text.WordWrap
        text: qsTr("Must not be empty, only 0-9, a-z, A-Z, _ and - are allowed here!")
        anchors {
            centerIn: parent
        }
    }
}
