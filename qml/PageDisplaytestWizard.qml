/*
    Copyright 2021, Prusa Development a.s.

    This file is part of touch-ui

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5

import cz.prusa3d.sl1.printer0 1.0
import cz.prusa3d.wizard0 1.0
import cz.prusa3d.wizardcheckmodel 1.0
import PrusaComponents 1.0

PageBasicWizard {
    id: root
    name: "DisplayTestWizard"
    title: qsTr("Display Test")

    overloadStateMapping: (wizardState) => {
        switch(wizardState) {
        case Wizard0.PREPARE_DISPLAYTEST: return prepare_displaytest
        case Wizard0.SHOW_RESULTS: // fall-through
        case Wizard0.DONE: return wizard_done
        default: return null // Otherwise use BasicWizard default
        }
    }

    Component {
        id: prepare_displaytest

        PageConfirm {
            id: prepare_displaytest_root
            name: "DisplayTestWizardPrepare"
            text: qsTr("Welcome to the display wizard.")
                  + "\n\n"
                  + qsTr("This procedure will help you make sure that your exposure display is working correctly.")
            onConfirmed: ()=>{ state = "remove_tank" }

            states: [
                State {
                    name: "remove_tank"
                    PropertyChanges {
                        target: prepare_displaytest_root
                        text: qsTr("Please unscrew and remove the resin tank.")
                        image_source: "selftest-remove_tank.jpg"
                        onConfirmed: ()=>{ state = "remove_platform" }
                    }
                },
                State {
                    name: "remove_platform"
                    PropertyChanges {
                        target: prepare_displaytest_root
                        text: qsTr("Loosen the black knob and remove the platform.")
                        image_source: "selftest-remove_platform.jpg"
                        onConfirmed: ()=>{ state = "close_cover" }
                    }
                },
                State {
                    name: "close_cover"
                    PropertyChanges {
                        target: prepare_displaytest_root
                        text: qsTr("Close the cover.")
                        image_source: "close_cover_no_tank.jpg"
                        enabled: printerConfig0.coverCheck ? printer0.cover_state === true : true
                        onConfirmed: ()=>{ wizard0.prepareDisplaytestDone() }
                    }
                }
            ]
        }
    }

    Component {
        id: wizard_done
        PageConfirm {
            name: "DisplayTestWizardDone"
            text: qsTr("All done, happy printing!")
            onConfirmed: ()=>{ wizard0.showResultsDone() }
        }
    }
}
