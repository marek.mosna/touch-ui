
/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5

import PrusaComponents 1.0

PrusaPage {
    id: root
    title: qsTr("Are You Sure?")
    name: "YesNoSimple"
    pictogram: Theme.pictogram.yes
    back_button_enabled: false
    screensaver_enabled: false
    property string text: ""
    property alias secondText: content.secondText

    signal yes()
    signal no()
    signal result(bool result)

    function popDialog() { if(StackView.view.currentItem === root) root.StackView.view.pop(); }

    StackView.onActivated: () => console.log("Showing dialog: ", title)

    Item {
        anchors.fill: parent

        Row {
            anchors {
                right: parent.right
                verticalCenter: parent.verticalCenter
                verticalCenterOffset:  -40
                rightMargin: 55
            }
            spacing: 30

            PictureButton {
                id: btnYes
                name: "Yes"
                text: qsTr("Yes")
                pictogram: Theme.pictogram.yes
                backgroundColor: "green"
                onClicked: () => {
                    root.yes()
                    root.result(true)
                }
            }

            PictureButton {
                id: btnNo
                name: "No"
                text: qsTr("No")
                pictogram: Theme.pictogram.no
                backgroundColor: Theme.color.red
                onClicked: () => {
                    root.no()
                    root.result(false)
                }
            }
        }
    }

    Column {
        id: content
        z: 10
        x: 55
        anchors.verticalCenter: parent.verticalCenter
        anchors.verticalCenterOffset: -40
        width: 330
        property alias secondText: secondText

        Text {
            width: parent.width
            text: root.text
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        }

        Text {
            id: secondText
        }
    }

    ImageVersionText {}
}
