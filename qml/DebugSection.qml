/*
    Copyright 2021, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import cz.prusa3d.sl1.printer0 1.0
//import cz.prusa3d.sl1.config0 1.0
import Native 1.0
import ErrorsSL1 1.0
import cz.prusa3d.sl1.errorcodestext 1.0

import PrusaComponents 1.0

ExpandingSection {
    GridLayout {
        width: parent.width/2 - 5
        anchors {
            top: parent.top
            left: parent.left
        }
        columns: 2
        columnSpacing: 5
        Text {
            Layout.alignment: Qt.AlignVCenter
            text: "Mute"
        }
        Row {
            Button {width: 100; name: "MuteOn"; text: "On"; onClicked: () => printerConfig0.mute = true }
            Button {width: 100; name: "MuteOff";  text: "Off"; onClicked: () => printerConfig0.mute = false }
        }


        Text {
            text: "Model"
        }

        SelectionBox {
            name: "PrinterModel"
            implicitWidth: 300
            implicitHeight: 60
            Layout.alignment: Qt.AlignVCenter
            wrap: true
            items: ["None", "SL1", "SL1S", "Medical"]
            index: printer0.printer_model
            onIndexChanged: () => {
                                if(index == 1) printer0.setPrinter_model(Printer0.SL1)
                                else if(index == 2) printer0.setPrinter_model(Printer0.SL1S)
                                else if(index == 3) printer0.setPrinter_model(Printer0.M1)
                                else if(index == 0) printer0.setPrinter_model(Printer0.NONE)
                            }
        }

        Text {text: "Lang"}
        SelectionBox {
            name: "Language"
            implicitWidth: 300
            implicitHeight: 60
            items: Translator.languages
            index:  Translator.languageKeys.indexOf(Translator.language)
            onIndexChanged: Translator.setLanguage(Translator.languageKeys[index])
        }
    }

    GridLayout {
        width: parent.width/2 - 5
        anchors {
            top: parent.top
            right: parent.right
        }
        columns: 2
        columnSpacing: 5
        Text { text: "printer0.state" }
        Text { text: printer0.state }

        Text { text: "p0 calibrated" }
        RollingText { Layout.fillWidth: true;  text: "mech: " + printer0.mechanically_calibrated + " | " + "unbox:" + printer0.unboxed + "|" + "selftest:" + printer0.self_tested + "|" + "uv:" + printer0.uv_calibrated}

        Text { text: "exposure:" }
        RollingText { Layout.fillWidth: true;  text: printer0.current_exposure }

        Text { text: "exposure.state" }
        Text { text: printer0.current_exposure_object == null ? "N/A" : printer0.current_exposure_object.state }

        Button { text: "Synthetic fail"; onClicked: printer0.fail_action("#10319")}
        FpsItem {}

    }
}
