
/*
    Copyright 2019-2020, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtQml.Models 2.11
import PrusaComponents 1.0

PrusaPage {
    id: root
    pictogram: Theme.pictogram.settingsSimple

    default property alias model: listView.model
    property alias delegate: listView.delegate
    property alias interactive: listView.interactive
    property bool busy: false

    ListView {
        id: listView
        height: 420
        interactive: true

        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            rightMargin: 10
            leftMargin: 10
        }

        clip: true
        ScrollBar.vertical: ScrollBar {
            active: true
            policy: ScrollBar.AlwaysOn
        }
    }

    PrusaWaitOverlay {
        id: waitOverlay
        text: qsTr("Loading, please wait...")
        doWait: root.busy
        anchors.fill: parent
    }
}
