/*
    Copyright 2021, Prusa Development a.s.

    This file is part of touch-ui

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5

import cz.prusa3d.sl1.printer0 1.0
import cz.prusa3d.wizard0 1.0
import cz.prusa3d.wizardcheckmodel 1.0
import PrusaComponents 1.0

PageBasicWizard {
    id: root
    name: "NewExposurePanelWizard"
    title: qsTr("New Exposure Panel")

    overloadStateMapping: (wizardState) => {
        switch(wizardState) {
        case Wizard0.PREPARE_NEW_EXPO_PANEL: return prepare_new_expo_panel
        default: return null // Otherwise use BasicWizard default
        }
    }

    Component {
        id: prepare_new_expo_panel
        PageConfirm {
            name: "ConfirmNewDisplay"
            text: qsTr("New exposure screen has been detected.\n\nThe printer will ask for the inital setup (selftest and calibration) to make sure everything works correctly.")
            onConfirmed: ()=>{ wizard0.newExpoPanelDone() }
        }
    }
}
