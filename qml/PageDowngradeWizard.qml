/*
    Copyright 2021, Prusa Development a.s.

    This file is part of touch-ui

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5

import cz.prusa3d.sl1.printer0 1.0
import cz.prusa3d.wizard0 1.0
import cz.prusa3d.wizardcheckmodel 1.0
import PrusaComponents 1.0

PageBasicWizard {
    id: root
    title: qsTr("Hardware Downgrade")
    overloadStateMapping: (wizardState) => {
        switch(wizardState) {
        case Wizard0.SL1S_CONFIRM_UPGRADE: return confirm_downgrade
        case Wizard0.SHOW_RESULTS: // fall-through
        case Wizard0.DONE: return resinTankWarning
        default: return null// Otherwise use BasicWizard default
        }
    }

    Component {
        id: confirm_downgrade
        PageYesNoSimple {
            id: dialog
            name: "ConfirmDowngrade"
            text: qsTr("SL1 components detected (downgrade from SL1S).") + "\n\n" +
                qsTr("To complete the downgrade procedure, printer needs to clear the configuration and reboot.") + "\n\n" +
                qsTr("Proceed?")
            onYes: () => dialog.StackView.view.replace(downgradeNotSupportedDialog)
            onNo: () => dialog.StackView.view.replace(rejectionDialog)
        }
    }

    Component {
        id: rejectionDialog
        PageConfirm {
            name: "DowngradeCancelConfirm"
            text: qsTr("The printer will power off now.") + "\n\n" +
                qsTr("Reassemble SL1S components and power on the printer. This will restore the original state.")
            onConfirmed: () => wizard0.sl1sRejectUpgrade()
        }
    }

    Component {
        id: downgradeNotSupportedDialog
        PageConfirm {
            name: "DowngradeUnsupportedWarning"
            image_source: "display_uv_calibrator_set.jpg"
            text: qsTr("Please note that downgrading is not supported. \n\nDowngrading your printer will erase your UV calibration and your printer will not work properly. \n\nYou will need to recalibrate it using an external UV calibrator.")
            onConfirmed: () => StackView.view.replace(confirmationDialog)
        }
    }

    Component {
        id: confirmationDialog
        PageConfirm {
            name: "DowngradeConfirm"
            text: qsTr("Current configuration is going to be cleared now.") + "\n\n" +
                  qsTr("The printer will ask for the inital setup after reboot.")
            onConfirmed: () => wizard0.sl1sConfirmUpgrade()
        }
    }

    Component {
        id: resinTankWarning
        PageConfirm {
            name: "DowngradeResinTankWarning"
            image_source: "sl1_downgrade_tank_image.jpg"
            text: qsTr("Use only the metal resin tank supplied. Using the different resin tank may cause resin to spill and damage your printer!")
            onConfirmed: () => StackView.view.replace(platformWarning)
        }
    }

    Component {
        id: platformWarning
        PageConfirm {
            name: "DowngradePlatformWarning"
            image_source: "sl1_downgrade_cantilever_image.jpg"
            text: qsTr("Only use the platform supplied. Using a different platform may cause resin to spill and damage your printer!")
            onConfirmed: () => StackView.view.replace(wizard_done)
        }
    }

    Component {
        id: wizard_done
        PageConfirm {
            name: "DowngradeDone"
            text: qsTr("Downgrade done. In the next step, the printer will be restarted.")
            onConfirmed: () => wizard0.showResultsDone()
        }
    }
}
