
/*
    Copyright 2019-2020, Prusa Research s.r.o.
    Copyright 2021, Prusa Research a.s.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5

import PrusaComponents 1.0
import cz.prusa3d.sl1.filemanager 1.0
import Native 1.0
import "utils.js" as Utils

/** Needs to be inserted in a StackView */
PrusaPage {
    id: root
    title: qsTr("Project")
    name: "PrintPreviewSwipe"
    pictogram: Theme.pictogram.printSimple

    property QtObject exposure
    property var metadata
    property var preview_files: []
    property int viewIndex: 1
    property string file_path: ""
    function back_handler() {
        if(exposure) {
            exposure.cancel(function(){}, function(){})
            root.StackView.view.pop()
        }
    }

    // Needs FileManager context to access the thumbnails(and other meta-information)
    FMContext {
        id: fm
        objectName: "PreviewFileManagerContext"
    }

    Binding on file_path {
        when: exposure !== undefined && exposure !== null
        value: exposure.project_file
    }

    onMetadataChanged: ()=>{
        root.preview_files = metadata.thumbnails
    }

    function load_metadata() {
        fm.get_metadata(root.exposure.project_file, true,
                        function(_metadata){
                            root.metadata = _metadata
                            console.log("PagePrintPreviewSwipe.qml: metadata: ", JSON.stringify(_metadata))
                        },
                        function(err){
                            console.log("Error getting metadata for file ", metadata.path, " err:", err)
                        })

    }

    StackView.onActivated: ()=>{
        projectStatisticsItemSwipeSign.shake()
        if(exposure.state === Exposure.POUR_IN_RESIN && swipePrint.currentIndex != 2)
            swipePrint.setCurrentIndex(2)
    }

    Component.onCompleted: ()=>{
        console.log("Created PagePrintPreviewSwipe")
        if(root.exposure === undefined) {
            root.exposure = printer0.getCurrentExposureObject()
            if(!root.exposure) {
                console.log("WARN: PagePrintPreviewSwipe: could not get exposure!")
            }

            load_metadata()
        }
        else load_metadata()
    }

    SwipeView {
        id: swipePrint
        anchors.fill: parent
        currentIndex: root.viewIndex

        Item {
            id: previewItem
            clip: true
            states: [
                State {
                    name: "preview"
                    when: root.preview_files !== undefined && root.preview_files[1] !== undefined
                    PropertyChanges {
                        target: placeholderImage
                        opacity: 0.0

                    }
                    PropertyChanges {
                        target: img
                        opacity: 1.0
                    }
                }]
            transitions: [
                Transition {
                    from: ""
                    to: "preview"
                    reversible: false
                    ParallelAnimation {
                        NumberAnimation { target: placeholderImage; property: "opacity"; duration: 400 }
                        NumberAnimation { target: img; property: "opacity";  duration: 300 }
                    }
                }]

            MouseArea {
                anchors.fill: parent
                onClicked: ()=>{
                    swipePrint.setCurrentIndex(1)
                }
            }

            Image {
                opacity: 1.0
                id: placeholderImage
                anchors.fill: parent
                sourceSize.width: height
                sourceSize.height: width
                fillMode: Image.PreserveAspectCrop
                source: Theme.pictogram.path + "/" + Theme.pictogram.thumbnailPlaceholder
            }

            Image {
                opacity: 0.0
                id: img
                anchors.fill: parent
                width:  parent.width
                sourceSize.width: 800
                sourceSize.height: 480
                fillMode: Image.PreserveAspectCrop
                asynchronous: true
                source: root.preview_files[1] ? ("file://" + root.preview_files[1]) : Theme.pictogram.path + "/" + Theme.pictogram.thumbnailPlaceholder

            }

            SwipeSign {
                id: previewItemSwipeSign
                anchors {
                    right: parent.right
                    margins: 10
                    bottom: parent.bottom
                    bottomMargin: 5
                }
                text: qsTr("Swipe to project")
                direction: "right"
                onClicked: ()=>{
                    swipePrint.incrementCurrentIndex()
                }
            }

            SwipeView.onIsCurrentItemChanged: ()=>{
                if(SwipeView.isCurrentItem) previewItemSwipeSign.shake()
            }
        }

        Item {
            id: projectStatisticsItem
            clip: true

            GridLayout {
                height: 340
                anchors {
                    verticalCenter: parent.verticalCenter
                    verticalCenterOffset:  -20
                    leftMargin: 55
                    rightMargin: 55
                    bottomMargin: 30
                    left: parent.left
                    right: parent.right
                }
                columns: 2
                rows: 2

                ScrollableText {
                    id: projectName
                    Layout.columnSpan: 2
                    swing: true
                    height: 33
                    width: 690
                    font.pixelSize: Theme.font.big
                    text: {
                        if(file_path) {
                            var sp = file_path.split("/")
                            return sp[sp.length-1]
                        }
                        else return qsTr("Unknown")
                    }
                }

                Item {
                    width: 400
                    height: 300
                    states: [
                        State {
                            name: "preview"
                            when: root.preview_files !== undefined && root.preview_files[0] !== undefined
                            PropertyChanges {
                                target: placeholderImage2
                                opacity: 0.0

                            }
                            PropertyChanges {
                                target: img2
                                opacity: 1.0
                            }
                        }]
                    transitions: [
                        Transition {
                            from: ""
                            to: "preview"
                            reversible: false
                            ParallelAnimation {
                                NumberAnimation { target: placeholderImage2; property: "opacity"; duration: 400 }
                                NumberAnimation { target: img2; property: "opacity";  duration: 300 }
                            }
                        }]

                    Image {
                        opacity: 1.0
                        id: placeholderImage2
                        anchors.fill: parent
                        sourceSize.width: height
                        sourceSize.height: width
                        fillMode: Image.PreserveAspectCrop
                        source: Theme.pictogram.path + "/" + Theme.pictogram.thumbnailPlaceholder
                    }

                    Image {
                        opacity: 0.0
                        id: img2
                        anchors {
                            fill: parent
                        }

                        sourceSize.width: 800
                        sourceSize.height: 480
                        fillMode: Image.PreserveAspectCrop
                        source: root.preview_files[0] ? ("file://" + root.preview_files[0]) : Theme.pictogram.path + "/" + Theme.pictogram.thumbnailPlaceholder
                        asynchronous: true
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: ()=>{
                            if(root.preview_files[0]) {
                                swipePrint.setCurrentIndex(0)
                            }
                        }
                    }
                }
                GridLayout {
                    rowSpacing: 15
                    columns: 1
                    clip: true
                    width: 400

                    ColumnLayout {
                        ScrollableText {
                            text: qsTr("Layers") + " / " + qsTr("Layer Height")
                            color: "grey"
                            font.pixelSize: 25
                            swing: true
                            width: 330
                        }
                        Text {
                            text: {
                                if(exposure !== undefined && exposure !== null) {
                                    return exposure.total_layers + " / " + (exposure.layer_height_nm / 1e6) + "\u202Fmm"
                                }
                                else return "0 / 0\u202Fmm"
                            }

                            font.bold: true
                            font.pixelSize: 25
                        }
                    }

                    ColumnLayout {
                        ScrollableText {
                            text: qsTr("Exposure Times")
                            color: "grey"
                            font.pixelSize: 25
                            swing: true
                            width: 330
                        }
                        Text {
                            text: {
                                if(exposure !== undefined && exposure !== null) {
                                    var first =  Math.round(exposure.exposure_time_first_ms/100) / 10
                                    var second = Math.round(exposure.exposure_time_ms/100) / 10
                                    var third = Math.round(exposure.exposure_time_calibrate_ms/100) / 10
                                    if(exposure.calibration_regions) {
                                        return first + " / " + second + " / " + third + "\u202Fs"
                                    }
                                    else return first + " / " + second + "\u202Fs"
                                }
                                else return "0/0/0"
                            }
                            font.bold: true
                            font.pixelSize: 25
                        }
                    }

                    ColumnLayout {

                        ScrollableText {
                            text: qsTr("Print Time Estimate")
                            color: "grey"
                            font.pixelSize: 25
                            swing: true
                            width: 330
                        }

                        ScrollableText {
                            width: 400
                            text: Utils.durationToString(exposure.time_remain_ms / 1000)
                            font.bold: true
                            font.pixelSize: 25
                            swing: true
                        }
                    }

                    ColumnLayout {

                        ScrollableText {
                            text: qsTr("Last Modified")
                            color: "grey"
                            font.pixelSize: 25
                            swing: true
                            width: 330
                        }

                        ScrollableText {
                            width: 400
                            text: root.metadata ? global.localizeDateTime(new Date(metadata.mtime * 1000)): qsTr("Unknown", "Unknow time of last modification of a file")
                            swing: true
                            font.bold: true
                            font.pixelSize: 25
                        }
                    }
                }
            }

            SwipeSign {
                id: projectStatisticsItemSwipeSign
                anchors {
                    right: parent.right
                    margins: 10
                    bottom: parent.bottom
                    bottomMargin: 5
                }
                text: qsTr("Swipe to continue")
                direction: "right"
                onClicked: ()=>{
                    swipePrint.incrementCurrentIndex()
                }
            }
            SwipeView.onIsCurrentItemChanged: ()=>{
                if(SwipeView.isCurrentItem) projectStatisticsItemSwipeSign.shake()
            }
        }

        Item {
            id: printStartItem
            clip: true
            SwipeView.onIsCurrentItemChanged: () => {
                                                  if(SwipeView.isCurrentItem) {
                                                      if(exposure.state === Exposure.CONFIRM) {
                                                          printScreenAnimation.start()
                                                          root.exposure.confirm_start(
                                                                      function(){console.log("Start confirmed")},
                                                                      function(err){console.warn("Confirm start error: ", err)}
                                                                      )
                                                      }
                                                  }
                                              }
            opacity: exposure.state === Exposure.CONFIRM ? 0.0 : 1.0

            Connections {
                target: exposure
                function onStateChanged() {
                                    if(exposure.state === Exposure.POUR_IN_RESIN) {
                                        printScreenAnimation.stop()
                                        printStartItem.opacity = 1.0
                                    }
                                }
            }

            SequentialAnimation {
                id: printScreenAnimation
                running: false

                PauseAnimation {
                    duration: 500
                }

                OpacityAnimator {
                    target: printStartItem
                    duration: 0
                    from: 0
                    to: 1.0
                    easing.type: Easing.InOutQuad
                }
                onRunningChanged: ()=>{ if(!running) { root.cancel_instead_of_back = true} }
            }

            Column {
                width: 300
                anchors.leftMargin: 55
                anchors {
                    left: parent.left
                    verticalCenter: parent.verticalCenter
                    verticalCenterOffset: -40
                }

                Text {
                    width: parent.width
                    text: {
                        var requiredResin = Math.round(exposure.total_resin_required_percent)
                        var text = ""
                        if (requiredResin > 100) {
                            text += "<b>" + qsTr("The project requires %1 % of the resin. It will be necessary to refill the resin during print.").arg(requiredResin) + "</b><br/><br/>"
                            requiredResin = 100
                        }
                        text += qsTr("Please fill the resin tank to at least %1 % and close the cover.").arg(requiredResin)
                        return text
                    }
                    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                }
            }

            GridLayout {
                id: buttons
                anchors {
                    leftMargin: 55
                    verticalCenter: parent.verticalCenter
                    verticalCenterOffset: -40
                    left: parent.left
                }

                columns: 4
                rows: 1
                columnSpacing: 30
                rowSpacing: 5

                Item {
                    width: btnChange.width
                    height: btnChange.height
                }

                Item {
                    width: btnChange.width
                    height: btnChange.height
                }

                PictureButton {
                    id: btnChange
                    name: "PrintSettings"
                    pictogram: Theme.pictogram.exposureTime
                    text: qsTr("Print Settings")
                    onClicked: ()=>{
                        root.StackView.view.push("PageChange.qml", {exposure: root.exposure})
                    }
                }

                PictureButton {
                    name: "Print"
                    pictogram: Theme.pictogram.yes
                    backgroundColor: "green"
                    text: qsTr("Print")
                    onClicked: ()=>{
                        root.exposure.confirm_resin_in(
                                    function(){console.log("Resin in confirmed, starting...")},
                                    function(err){console.warn("Confirm resin in error: ", err)}
                                    )
                    }
                }
            }
        }
    }

    PageIndicator {
        id: indicator
        count: swipePrint.count
        currentIndex: swipePrint.currentIndex

        delegate: Item {
            height: 15
            width: 30
            Rectangle {
                width: 15
                height: 15
                color: index === indicator.currentIndex ? Theme.color.highlight : "grey"
                radius: 7
                anchors.margins: 10
            }
        }
        anchors.bottom: swipePrint.bottom
        anchors.margins: 5
        anchors.horizontalCenter: parent.horizontalCenter
    }

    ImageVersionText {}
}
