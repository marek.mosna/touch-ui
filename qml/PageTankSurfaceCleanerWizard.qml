/*
    Copyright 2021, Prusa Development a.s.

    This file is part of touch-ui

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5

import cz.prusa3d.sl1.printer0 1.0
import cz.prusa3d.wizard0 1.0
import cz.prusa3d.wizardcheckmodel 1.0
import PrusaComponents 1.0
import PrusaComponents.Delegates 1.0

PageBasicWizard {
    id: root
    name: "TankCleaningWizard"
    title: qsTr("Resin Tank Cleaning")
    overloadStateMapping: (wizardState) => {
                              switch(wizardState) {
                                  case Wizard0.TANK_SURFACE_CLEANER_INIT:
                                  return tankSurfaceCleanerPageInit

                                  case Wizard0.TANK_SURFACE_CLEANER_INSERT_CLEANING_ADAPTOR:
                                  return tankSurfaceCleanerPageInsertCleaningAdaptor

                                  case Wizard0.TANK_SURFACE_CLEANER_REMOVE_CLEANING_ADAPTOR:
                                  return tankSurfaceCleanerPageRemoveCleaningAdaptor

                                  default:
                                  return null// Otherwise use BasicWizard default
                              }
                          }

    Component {
        id: tankSurfaceCleanerPageInit
        PageSwipe {
            id: swipe
            name: "TankCleaningSwipe"
            interactive: false
            indicator.visible: false

            PageConfirm {
                transitions: [Transition {}]
                name: "TankCleaningIntro"
                title: qsTr("Tank Cleaning")
                text:  qsTr("This wizard will help you clean your resin tank and remove resin debris.\n\nYou will need a special tool - a cleaning adaptor. You can print it yourself - it is included as an example print in this machine's internal storage. Or you can download it at Prusaprinters.org")
                image_source: "tank_cleaning-cleaning_adaptor"
                onConfirmed: () => { swipe.incrementCurrentIndex() }
            }

            PageButtons {
                name: "TankCleaningExposureSettings"
                title: qsTr("Tank Cleaning")
                transitions: [Transition{}]
                children: [
                    Text {
                        id: txtSubtitle
                        width: 600
                        anchors {
                            topMargin: 10
                            bottomMargin: 10
                            horizontalCenter: parent.horizontalCenter
                        }

                        text:  qsTr("Here you can optionally adjust the exposure settings before cleaning starts.")
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                    }
                ]

                PictureButton {
                    name: "LessExposure"
                    pictogram: Theme.pictogram.minus
                    text: qsTr("Less")
                    onClicked: () => printerConfig0.tankCleaningExposureTime -= 1

                    property bool longPressed: false
                    onPressAndHold: () => { longPressed = true;  printer0.beep_button() }
                    onReleased: () => { longPressed = false; printer0.beep_button() }

                    Timer {
                        running: parent.longPressed
                        interval: 100
                        repeat: true
                        onTriggered: () => printerConfig0.tankCleaningExposureTime -= 1
                    }
                }

                PictureButton {
                    id: btnMoreExposure
                    name: "MoreExposure"
                    pictogram: Theme.pictogram.plus
                    text: qsTr("More")
                    onClicked: () => printerConfig0.tankCleaningExposureTime += 1

                    property bool longPressed: false
                    onPressAndHold: () => { longPressed = true;  printer0.beep_button() }
                    onReleased: () => { longPressed = false; printer0.beep_button() }

                    Timer {
                        running: parent.longPressed
                        interval: 100
                        repeat: true
                        onTriggered: () => printerConfig0.tankCleaningExposureTime += 1
                    }
                }

                ColumnLayout {
                    width: btnMoreExposure.width
                    height: btnMoreExposure.height
                    spacing: 5
                    Layout.alignment: Qt.AlignCenter
                    Layout.maximumWidth: btnMoreExposure.width
                    clip: true

                    Text {
                        Layout.alignment: Qt.AlignCenter
                        Layout.maximumWidth: parent.width + 10
                        text: qsTr("Exposure[s]:")
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                    }

                    Text {
                        Layout.alignment: Qt.AlignCenter
                        text: JSON.stringify(printerConfig0.tankCleaningExposureTime)
                    }
                }

                PictureButton {
                    Layout.alignment: Qt.AlignRight
                    name: "Continue"
                    pictogram: Theme.pictogram.yes
                    onClicked: () => { printerConfig0.save(); wizard0.tank_surface_cleaner_init_done() }
                    text: qsTr("Continue")
                    backgroundColor: "green"
                }
            }
        }
    }

    Component {
        id: tankSurfaceCleanerPageInsertCleaningAdaptor
        PageConfirm {
            name: "TankCleaningInsertAdaptor"
            title: qsTr("Cleaning Adaptor")
            text: qsTr("Please insert the cleaning adaptor as is ilustrated in the picture on the left.")
            image_source: "tank_cleaning-place_cleaning_adaptor"
            onConfirmed: () => wizard0.insert_cleaning_adaptor_done()
        }
    }

    Component {
        id: tankSurfaceCleanerPageRemoveCleaningAdaptor
        PageConfirm {
            name: "TankCleaningRemoveAdaptor"
            title: qsTr("Remove Adaptor")
            text: qsTr("Now remove the cleaning adaptor along with the exposed film, careful not to tear it.")
            image_source: "tank_cleaning-remove_cleaning_adaptor"
            onConfirmed: () => wizard0.remove_cleaning_adaptor_done()
        }
    }
}
