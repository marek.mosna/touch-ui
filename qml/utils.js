/*
    Copyright 2019-2020, Prusa Research s.r.o.
    2021, Prusa Research a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


.pragma library

function get_with_default(array, index, _default) {
    if(index >= 0 && index < array.length) {
        return array[index]
    }
    else return _default
}

function isEmpty(obj) {
  for(var prop in obj) {
    if(obj.hasOwnProperty(prop)) {
      return false;
    }
  }

  return JSON.stringify(obj) === JSON.stringify({});
}

/** Stringify a time duration in seconds */
function durationToString(seconds, shortFormat = false) {
    var m = Math.floor((seconds/60)  % 60)
    var h = Math.floor((seconds/3600))
   // var d  = Math.floor((seconds/(3600*24)))
    var str = ""
    if( seconds < 60 ) str =  qsTr("Less than a minute")
    else if(shortFormat) {
        //if(d) str += qsTr("%n d", "day(s)", d)
        if(h) str += /*(d ? " " : "") +*/ qsTr("%n h", "how many hours", h)
        if(m) str +=(h ? " " : "") + qsTr("%n min", "how many minutes", m)
    }
    else {
        //if(d) str += qsTr("%n day(s)", "how many days", d)
        if(h) str += /*(d ? " " : "") + */ qsTr("%n hour(s)", "how many hours", h)
        if(m) str += (h ? " " : "") +qsTr("%n minute(s)", "how many minutes",  m)
    }
    return str;
}
