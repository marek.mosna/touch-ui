
/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import cz.prusa3d.sl1.printer0 1.0
import PrusaComponents 1.0
/*
 Depends on:
    StackView view
*/

PageButtons {
    id: root
    title: qsTr("Move Resin Tank")
    name: "TiltMove"
    pictogram: Theme.pictogram.moveResinTank

    function tilt_move(next_movement) {
        printer0.tilt_move(next_movement,
                           function(success) {
                               if(success){
                                   var movement;
                                   switch(next_movement) {
                                   case Printer0.MovementSpeed.FAST_UP:
                                       movement = "FAST_UP";
                                       break;
                                   case Printer0.MovementSpeed.FAST_DOWN:
                                       movement = "FAST_DOWN";
                                       break;
                                   case Printer0.MovementSpeed.SLOW_DOWN:
                                       movement = "SLOW_DOWN";
                                       break;
                                   case Printer0.MovementSpeed.SLOW_UP:
                                       movement = "SLOW_UP";
                                       break;
                                   case Printer0.MovementSpeed.STOP:
                                       movement = "STOP";
                                       break;
                                   default:
                                       console.exception("printer0.tilt_move called with unknown speed")
                                       break;
                                   }
                                   console.log("PageTiltmove: MovementSpeed." + movement)
                               }
                           },
                           function(err) {
                               console.log("ERROR: PageTiltmove " + JSON.stringify(error));
                           }
                           );
    }

    function tilt_stop() {
        tilt_move(Printer0.MovementSpeed.STOP)
    }

    PictureButton {
        name: "FastUp"
        text: qsTr("Fast Up")
        pictogram: Theme.pictogram.fastUp
        onButtonPressed: ()=>{ tilt_move(Printer0.MovementSpeed.FAST_UP) }
        onButtonReleased: ()=>{ tilt_stop() }
    }

    PictureButton {
        name: "FastDown"
        text: qsTr("Fast Down")
        pictogram: Theme.pictogram.fastDown
        onButtonPressed: ()=>{ tilt_move(Printer0.MovementSpeed.FAST_DOWN) }
        onButtonReleased: ()=>{ tilt_stop() }
    }

    PictureButton {
        name: "SlowUp"
        text: qsTr("Up")
        pictogram: Theme.pictogram.moveUp
        onButtonPressed: ()=>{ tilt_move(Printer0.MovementSpeed.SLOW_UP) }
        onButtonReleased: ()=>{ tilt_stop() }
    }

    PictureButton {
        name: "SlowDown"
        text: qsTr("Down")
        pictogram: Theme.pictogram.moveDown
        onButtonPressed: ()=>{ tilt_move(Printer0.MovementSpeed.SLOW_DOWN) }
        onButtonReleased: ()=>{ tilt_stop() }
    }
}
