/*
    Copyright 2020, Prusa Research a.s.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import cz.prusa3d.sl1.printer0 1.0
import Native 1.0
import PrusaComponents 1.0

PageQrCode {
    title: qsTr("Manual")
    pictogram: Theme.pictogram.manualSimple
    qrContent: {
        var lang = LocaleSetting.locale.substring(0,2)
        if (lang === "cs") {
            lang = "cz"
        }
        console.log(LocaleSetting)
        console.log(LocaleSetting.locale)
        console.log(JSON.stringify(LocaleSetting))
        var model = ""
        switch(printer0.printer_model) {
            case Printer0.SL1: model = "sl1"; break;
            case Printer0.SL1S: model = "sl1s"; break;
            case Printer0.M1: model = "m1"; break;
        }
        return "prusa3d.com/manual-" + model + "/" + lang
    }
    text: qsTr("Scanning the QR code will load the handbook for this device.\n\nAlternatively, use this link:") + "\n\n" + qrContent
    qrCodeOffset: -2
}
