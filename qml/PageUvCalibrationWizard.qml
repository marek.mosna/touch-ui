/*
    Copyright 2021, Prusa Development a.s.

    This file is part of touch-ui

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5

import cz.prusa3d.sl1.printer0 1.0
import cz.prusa3d.wizard0 1.0
import cz.prusa3d.wizardcheckmodel 1.0
import PrusaComponents 1.0

PageBasicWizard {
    id: root
    name: "UVCalibrationWizard"
    title: qsTr("UV Calibration")
    essentialCalibration: true
    overloadStateMapping: (wizardState) => {
        switch(wizardState) {
        case Wizard0.UV_CALIBRATION_PREPARE: return uv_prepare
        case Wizard0.UV_CALIBRATION_PLACE_UV_METER: return uv_place_uv_meter
        case Wizard0.UV_CALIBRATION_REMOVE_UV_METER: return uv_remove_calibrator
        case Wizard0.UV_CALIBRATION_APPLY_RESULTS: return uv_apply_results
        default: return null// Otherwise use BasicWizard default
        }
    }

    Component {
        id: uv_prepare
        PageConfirm {
            name: "UVCalibrationPrepare"
            text:  qsTr("Welcome to the UV calibration.")
                   + "\n\n"
                   + qsTr("1. If the resin tank is in the printer, remove it along with the screws.")
                   + "\n"
                   + qsTr("2. Close the cover, don't open it! UV radiation is harmful!")
                   + (printer0.factory_mode ? (
                                                 "\n\n"
                                                 + qsTr("Intensity: center %1, edge %2").arg(printerConfig0.uvCalibIntensity).arg(printerConfig0.uvCalibMinIntEdge)
                                                 + "\n\n"
                                                 + qsTr("Warm-up: %1 s").arg(printerConfig0.uvWarmUpTime)
                                                 ) : "")
            // TODO: in the old wizard, it showed some values:
            // Intensity: center 140, edge 90, Warm-up: 120s, PWM: <150, 250>
            onConfirmed: () => wizard0.uvCalibrationPrepared()
            enabled: printerConfig0.coverCheck ? printer0.cover_state === true : true
        }
    }

    Component {
        id: uv_place_uv_meter
        PageConfirm {
            name: "UVCalibrationInsertMeter"
            image_source: "uvcalibration_insert_meter.jpg"
            text:  qsTr("1. Place the UV calibrator on the print display and connect it to the front USB.")
                   + "\n"
                   + qsTr("2. Close the cover, don't open it! UV radiation is harmful!")
            onConfirmed: () => wizard0.uvMeterPlaced()
            // Note: cover_state check is not enforced here, because the UV Calibrator cable
            //       sticks from under the cover and may stop it from completely closing.
        }
    }

    Component {
        id: uv_remove_calibrator
        PageWait {
            name: "UVCalibrationCoverWait"
            text:  qsTr("Open the cover, <b>remove and disconnect</b> the UV calibrator.")
        }
    }

    Component {
        id: uv_apply_results

        PageYesNoSimple {
            name: "UVCalibrationShowResults"
            states: [
                State {
                    name: "admin_enabled"
                    when: printer0.admin_enabled

                    PropertyChanges {
                       target: secondText
                       text: "\n\n" + qsTr("The result of calibration:") + "\n" +
                               qsTr("UV PWM: %1").arg(wizard0.data["uvFoundPwm"]) + "\n" +
                               qsTr("UV Intensity: %1, \u03C3 = %2").arg(wizard0.data["uvMean"].toFixed(1)).arg(wizard0.data["uvStdDev"].toFixed(1)) + "\n" +
                               qsTr("UV Intensity min: %1, max: %2").arg(wizard0.data["uvMinValue"].toFixed(1)).arg(wizard0.data["uvMaxValue"].toFixed(1))
                    }
                }
            ]
            text:   qsTr("The printer has been successfully calibrated!\nWould you like to apply the calibration results?")
            onYes: () => wizard0.uvCalibrationApplyResults()
            onNo: () => wizard0.uvCalibrationDiscardResults()
        }
    }
}
