
/*
    Copyright 2019-2020, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtQml.Models 2.11
import PrusaComponents 1.0
import PrusaComponents.Delegates 1.0

PageVerticalList {
    title: qsTr("Platform & Resin Tank")
    name: "SettingsPlatformResinTank"
    pictogram: Theme.pictogram.platformTank
    model: ObjectModel {
        DelegateRef {
            name:"TowerMove"
            text: qsTr("Move Platform")
            pictogram: Theme.pictogram.movePlatform
            onClicked: ()=>{ view.push("PageTowermove.qml") }
        }

        DelegateRef {
            name:"TiltMove"
            text: qsTr("Move Resin Tank")
            pictogram: Theme.pictogram.moveResinTank
            onClicked: ()=>{ view.push("PageTiltmove.qml") }
        }

        DelegateRef {
            name:"DisableSteppers"
            text: qsTr("Disable Steppers")
            pictogram: Theme.pictogram.disableSteppers
            onClicked: ()=> { printer0.disable_motors() }
        }

        DelegatePlusMinus {
            id: towersensitivity
            name: "TowerSensitivity"
            text: qsTr("Platform Axis Sensitivity")
            pictogram: Theme.pictogram.towerSensitivity

            initValue: printerConfig0.towerSensitivity
            step: 1
            from: -2
            to: 2
            valuePresentation: (value) => { return (value > 0 ? "+" : "") + value.toString() }


            Component.onDestruction: ()=>{ printerConfig0.towerSensitivity = value }
            Connections {
                target: printerConfig0
                function onTowerSensitivityChanged() {
                    towersensitivity.value = printerConfig0.towerSensitivity
                    printerConfig0.update_motor_sensitivity()
                }
            }
        }

        DelegatePlusMinus {
            id: tiltsensitivity
            objectName: "TiltSensitivity"
            text: qsTr("Tank Axis Sensitivity")
            pictogram: Theme.pictogram.tiltSensitivity

            initValue: printerConfig0.tiltSensitivity
            step: 1
            from: -2
            to: 2

            valuePresentation: function(value) {return (value > 0 ? "+" : "") + value.toString()}
            Component.onDestruction: ()=>{ printerConfig0.tiltSensitivity = value }

            Connections {
                target: printerConfig0
                function onTiltSensitivityChanged() {
                    tiltsensitivity.value = printerConfig0.tiltSensitivity
                    printerConfig0.update_motor_sensitivity()
                }
            }
        }

        DelegatePlusMinus {
            id: fasttiltlimit
            name: "FastTiltLimit"
            text: qsTr("Limit for Fast Tilt")
            pictogram: Theme.pictogram.limit

            initValue: printerConfig0.limit4fast
            step: 1.0
            from: 0
            to: 100
            units: qsTr("%")

            valuePresentation: function(v) {
                return Math.round(v)
            }

            Component.onDestruction: ()=>{ printerConfig0.limit4fast = value }

            Connections {
                target: printerConfig0
                function onLimit4fastChanged() {
                    fasttiltlimit.value = printerConfig0.limit4fast
                }
            }
        }

        DelegatePlusMinus {
            id: toweroffset
            name: "TowerOffset"
            text: qsTr("Platform Offset")
            pictogram: Theme.pictogram.towerOffset

            initValue: Math.round((printerConfig0.calibTowerOffset / printerConfig0.microStepsMM) * 1000)
            step: 1
            from: -500
            to: 500
            units: qsTr("um")

            Component.onDestruction: ()=>{ printerConfig0.calibTowerOffset = Math.round(value * printerConfig0.microStepsMM /1000.0) }

            Connections {
                target: printerConfig0
                function onCalibTowerOffsetChanged() {
                    if(Math.round(toweroffset.value * printerConfig0.microStepsMM /1000.0) !== printerConfig0.calibTowerOffset) {
                        toweroffset.value = Math.round((printerConfig0.calibTowerOffset / printerConfig0.microStepsMM) * 1000)
                    }
                }
            }
        }

        DelegatePlusMinus {
            id: tankSurfaceCleaningExposureSettings
            name: "TankSurfaceCleaningExposure"
            text: qsTr("Tank Surface Cleaning Exposure")
            pictogram: Theme.pictogram.exposureTime

            initValue: printerConfig0.tankCleaningExposureTime
            step: 1
            from:  5
            to: 120
            units: qsTr("s")

            Component.onDestruction: ()=>{ printerConfig0.tankCleaningExposureTime = value }

            Connections {
                target: printerConfig0
                function onTankCleaningExposureTimeChanged() {
                    if(tankSurfaceCleaningExposureSettings.value !== printerConfig0.tankCleaningExposureTime) {
                        tankSurfaceCleaningExposureSettings.value = printerConfig0.tankCleaningExposureTime
                    }
                }
            }
        }
    }
}
