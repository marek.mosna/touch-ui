/*
    Copyright 2019-2020, Prusa Research s.r.o.
              2021 Prusa Research a.s.
    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import PrusaComponents 1.0
import cz.prusa3d.wizard0 1.0

PrusaPage {
    id: root
    title: qsTr("Tank Movement")
    name: "tiltmovecalibration"
    pictogram: Theme.pictogram.calibrationSimple
    property string top_text: ""
    property string bottom_text: ""
    signal confirmed()

    QtObject {
        id: direction
        readonly property int up: 1
        readonly property int down: -1
        readonly property int stop: 0
    }

    RollingText {
        anchors {
            horizontalCenter: parent.horizontalCenter
            bottom: buttonGrid.top
            bottomMargin: 40
        }

        width: parent.width - 2*55
        text: root.top_text
        font.pixelSize: Theme.font.big
    }

    Text {
        anchors {
            horizontalCenter: parent.horizontalCenter
            top: buttonGrid.bottom
            topMargin: 70
        }

        text: root.bottom_text
        font.pixelSize:  Theme.font.big
    }

    GridLayout {
        id: buttonGrid
        anchors {
            leftMargin: 55
            verticalCenter: parent.verticalCenter
            verticalCenterOffset: -40
            left: parent.left
        }

        columns: 4
        rows: 1
        columnSpacing: 30
        rowSpacing: 5

        PictureButton {
            id: btnUp
            name: "upslow"
            pictogram: Theme.pictogram.moveUp
            text: qsTr("Move Up")
            onButtonPressed: () => wizard0.tiltMove(direction.up)
            onButtonReleased: () => wizard0.tiltMove(direction.stop)
        }

        PictureButton {
            id: btnDown
            name: "downslow"
            pictogram: Theme.pictogram.moveDown
            text: qsTr("Move Down")

            onButtonPressed: () => wizard0.tiltMove(direction.down)
            onButtonReleased: () => wizard0.tiltMove(direction.stop)
        }

        ColumnLayout {
            width: btnUp.width
            height: btnUp.height
            spacing: 5
            Layout.alignment: Qt.AlignCenter
            Layout.maximumWidth: btnDown.width
            clip: true

            Text {
                Layout.alignment: Qt.AlignCenter
                Layout.maximumWidth: parent.width + 10
                text: qsTr("Tilt position:")
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            }

            Text {
                Layout.alignment: Qt.AlignCenter
                text: JSON.stringify(printer0.tilt_position)
            }
        }

        PictureButton {
            pictogram: Theme.pictogram.yes
            text: qsTr("Done")
            name: "ok"
            backgroundColor: "green"
            onClicked: () => root.confirmed()
        }
    }

    ImageVersionText {}
}
