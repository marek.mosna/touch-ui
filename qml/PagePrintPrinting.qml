
/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import Native 1.0

import ErrorsSL1 1.0
import PrusaComponents 1.0
import "utils.js" as Utils

PrusaPage {
    id: root
    name: "Printing"
    title: qsTr("Print")
    pictogram: Theme.pictogram.printSimple
    back_button_enabled: false

    property QtObject exposure

    // Properties will be conditionally bound to their counterparts in the
    // exposure object when it becomes available.
    property string project_name: exposure.project_name
    property real print_start_timestamp: exposure.print_start_timestamp
    property real expected_finish_timestamp: exposure.expected_finish_timestamp
    property int current_layer: exposure.current_layer + 1 // Users count layers starting from 1
    property int total_layers: exposure.total_layers
    property real resin_remaining_ml: exposure.resin_remaining_ml
    property real resin_used_ml: exposure.resin_used_ml
    property real progress: exposure.progress
    property real layer_height_first_mm: exposure.layer_height_first_nm * 1e-6
    property real layer_height_mm: exposure.layer_height_nm * 1e-6
    property bool resin_low: exposure.resin_low
    property bool resin_warn: exposure.resin_warn

    property real current_height: Math.max(0.0, (current_layer - 1) * layer_height_mm + layer_height_first_mm)
    property real height_total_mm: Math.max(0.0, layer_height_first_mm + (total_layers - 1) * layer_height_mm)
    property real exposure_end: exposure.exposure_end

    property real printing_time: root.current_time.getTime()/1000 - root.print_start_timestamp
    property string presentable_printing_time: Utils.durationToString(printing_time, true)

    // Periodically sample time to update bindings
    property var current_time: new Date()

    readonly property int columnWidth: 240

    Timer {
        interval: 30000
        onTriggered: ()=>{
            root.current_time = new Date()
        }
        running: true
        repeat: true
    }

    Component.onCompleted: ()=>{
        if(! root.exposure) root.exposure = printer0.getCurrentExposureObject()
        swipePrint.setCurrentIndex(1)
        root.exposure.warning.connect(showWarning)
        root.exposure.error.connect(showError)
    }

    onExposure_endChanged: ()=>{
        var duration = exposure_end*1000 - (new Date).getTime() // milliseconds
        console.log("Exposure duration: ", duration)
        exposure_started(duration)
        imgLayer.source = ""
        imgLayer.source = "file:///run/slafw/live.png"
    }

    function showWarning(data) {
        push_warning(data)
    }

    function showError(data) {
        push_error(data)
    }

    function exposure_started(exposure_time) {
        timedProgress.start(exposure_time)
    }

    function formatEstimatedTime(milliseconds) {
        let t_now = new Date()
        let t_end = new Date(t_now.getTime() + milliseconds)
        if(t_now.getDate() === t_end.getDate()) {
            return qsTr("Today at") + " " + global.localizeTime(t_end)
        }
        else if((new Date(t_now.getTime() + 24*60*60*1000)).getDate() === t_end.getDate()) {
            return qsTr("Tomorrow at") + " " + global.localizeTime(t_end)
        }
        else return global.localizeDate(t_end) + " " + global.localizeTime(t_end)
    }

    SwipeView {
        id: swipePrint
        anchors.fill: parent
        currentIndex: 1

        Item {
            clip: true

            Rectangle {
                id: imgRect
                anchors{
                    verticalCenterOffset: -30
                    centerIn: parent
                }
                color: "black"
                width: imgLayer.height + 2
                height: imgLayer.width + 2
                border.width: 1
                border.color: Theme.color.grey

                Image {
                    id: imgLayer
                    cache: false
                    smooth: false
                    asynchronous: false
                    anchors.centerIn: parent
                    height: 520
                    width: 292
                    rotation: 90
                    fillMode: Image.PreserveAspectFit
                    sourceSize.width: width
                    sourceSize.height: height

                }
            }

            Text {
                id: txtCurrentLayer
                text: {
                    var cur = root.current_layer
                    var tot = root.total_layers
                    return qsTr("Layer:") + " " + cur.toString() + "/" + tot.toString()
                }

                font.pixelSize: 24
                anchors {
                    left: imgRect.left
                    bottom: imgRect.top
                    bottomMargin: 5
                }
            }
            Text {
                id: txtLayerHeight
                text: qsTr("Layer Height:") + " " + root.layer_height_mm ? root.layer_height_mm.toFixed(3) : qsTr("N/A") + " mm"
                font.pixelSize: 24
                anchors {
                    top: imgRect.bottom
                    left: imgRect.left
                    topMargin: 14
                }
            }
            Text {
                text: {
                    var pos = root.current_height.toFixed(2)
                    var total = root.height_total_mm.toFixed(2)
                    return pos + "/" + total + " mm"
                }

                font.pixelSize: 24
                anchors {
                    right: imgRect.right
                    bottom: txtLayerHeight.bottom
                }
            }
        }

        Item {
            clip: true

            ScrollableText {
                id: txtProjectName
                y: 89 - 60
                anchors {
                    left: progress.left
                    right: progress.right
                }
                swing: true
                height: 33
                width: parent.width - 2*60
                font.pixelSize: Theme.font.big
                font.bold: true
                text: root.project_name
            }

            ProgressBar {
                id: progress
                y: 157 - 60
                anchors {
                    horizontalCenter: parent.horizontalCenter
                }
                height: 22
                width: 800 - 2*49
                value: root.progress
            }

            TimedProgress {
                visible: true
                id: timedProgress
                anchors {
                    horizontalCenter: progress.horizontalCenter
                    top: progress.bottom
                }
                width: progress.width
                height: 1
            }

            Text {
                id: txtPercent
                font.pixelSize: 40
                font.bold: true
                text: progress.value.toFixed(0) + "\u202F%"
                anchors {
                    horizontalCenter: progress.horizontalCenter
                    top: progress.bottom
                    topMargin: 21
                }
            }

            GridLayout {
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    top: txtPercent.bottom
                    topMargin: 23
                    left: progress.left
                    bottomMargin: 75
                }
                rows: 2
                columns: 3
                rowSpacing: 22

                ColumnLayout {
                    Text {
                        color: "grey"
                        text: qsTr("Remaining Time")
                        font.pixelSize: 25
                    }
                    ScrollableText {
                        width: root.columnWidth
                        text: root.current_time ? Utils.durationToString(exposure.time_remain_ms / 1000, true) : qsTr("Unknown","Remaining time is unknown")
                        font.pixelSize: 25
                        font.bold: true
                        swing: true
                    }
                }

                ColumnLayout {
                    Text {
                        color: "grey"
                        text: qsTr("Estimated End")
                        font.pixelSize: 25
                    }
                    ScrollableText {
                        width: root.columnWidth
                        text: formatEstimatedTime(exposure.time_remain_ms)
                        font.pixelSize: 25
                        font.bold: true
                        swing: true
                    }
                }
                ColumnLayout {
                    id: remainResinColumn
                    Text {
                        color: "grey"
                        text: qsTr("Printing Time")
                        font.pixelSize: 25
                    }
                    ScrollableText {
                        id: txtPrintingTime
                        text:  root.presentable_printing_time
                        font.pixelSize: 25
                        font.bold: true
                        width: root.columnWidth
                        swing: true
                    }
                }
                ColumnLayout {
                    Text {
                        color: "grey"
                        text: qsTr("Layer")
                        font.pixelSize: 25
                    }
                    ScrollableText {
                        text: root.current_layer.toString() + "/" + root.total_layers.toString()
                        font.pixelSize: 25
                        font.bold: true
                        width: root.columnWidth
                        swing: true
                    }
                }
                ColumnLayout {
                    Text {
                        color: "grey"
                        text: qsTr("Remaining Resin")
                        font.pixelSize: 25
                    }
                    Text {
                        text: root.resin_remaining_ml >= 0.0  ? root.resin_remaining_ml.toFixed(1) + " " + qsTr("ml") : qsTr("Unknown")
                        font.pixelSize: 25
                        font.bold: true
                        color: root.resin_warn ? Theme.color.highlight : "white"
                    }
                }

                ColumnLayout {
                    Text {
                        color: "grey"
                        text: qsTr("Consumed Resin")
                        font.pixelSize: 25
                    }
                    Text {
                        text: root.resin_used_ml >= 0.0 ? root.resin_used_ml.toFixed(1) + " " + qsTr("ml") : qsTr("Unknown")
                        font.pixelSize: 25
                        font.bold: true
                    }
                }
            }
            Text {
                x: 247
                y: 290
                text: "!"
                font.bold: true
                font.pixelSize: remainResinColumn.height + 10
                visible: root.resin_warn
                color: Theme.color.highlight
            }

        }
        Item {
            clip: true

            GridLayout {
                anchors {
                    leftMargin: 55
                    verticalCenter: parent.verticalCenter
                    verticalCenterOffset: -40
                    left: parent.left
                }

                columns: 4
                rows: 1
                columnSpacing: 30
                rowSpacing: 5

                PictureButton {
                    id: btnETimes
                    name: "PrintSettings"
                    pictogram: Theme.pictogram.exposureTime
                    text: qsTr("Print Settings")
                    onClicked: ()=>{ root.StackView.view.push("PageChange.qml", {exposure: root.exposure}) }
                }

                PictureButton {
                    pictogram: Theme.pictogram.refill
                    text: qsTr("Refill Resin")
                    name: "feedme"
                    onClicked: ()=>{
                        console.log("Telling backend to start feedme state")
                        root.exposure.feed_me(
                                    function(){console.log("Feedme command succeeded.")},
                                    function(err){console.log("Feedme failed: ", err)})
                    }
                }

                PictureButton {
                    name: "CancelPrint"
                    pictogram: Theme.pictogram.cancel
                    text: qsTr("Cancel Print")
                    onClicked: ()=>{ root.StackView.view.push(cancelDialog) }
                    Component {
                        id: cancelDialog
                        PageYesNoSwipe {
                            id: cancelDialogRoot
                            title: qsTr("Cancel Print")
                            text: qsTr("To make sure the print is not stopped accidentally,\n"
                                       + "please swipe the screen to move to the next step,\n"
                                       + "where you can cancel the print."
                                   )
                            onYes: () => {
                                       root.exposure.cancel(
                                                   function(){console.log("Print of ", root.project_name, " canceled.")},
                                                   function(err){console.warn("Err: could not cancel print of ", root.project_name)})
                                   }

                            onNo: ()=>{ cancelDialogRoot.StackView.view.pop() }
                        }
                    }
                }

                PictureButton {
                    visible: printer0.admin_enabled
                    text: qsTr("Enter Admin")
                    pictogram: Theme.pictogram.settings
                    name: "admin"
                    onClicked: ()=>{
                        adminApi.enter()
                    }
                }
            }
        }
    }


    PrusaPageIndicator {
        count: swipePrint.count
        currentIndex: swipePrint.currentIndex
        anchors {
            bottom: parent.bottom
        }
    }

    ImageVersionText {}
}
