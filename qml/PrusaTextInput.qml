import QtQuick 2.12
import QtQuick.Controls 2.5
import QtGraphicalEffects 1.0

import PrusaComponents 1.0

/* Simple item for puting directly into columns */
Item {
    id: root
    property alias value: txtValue.text
    property string label: "<no label>"
    property string regexp: ".*"
    property bool enabled: false
    signal accepted(string value)

    property alias txtValue: txtValue
    property int rightColWidth: 200
    property alias acceptableInput: txtValue.acceptableInput

    width: 760
    height: 70
    Text {
        id: txtLabel
        anchors {
            left: parent.left
            verticalCenter: parent.verticalCenter
        }
        font.pixelSize: 28
        text:  label === undefined ? "<No label>" : label
    }
    TextInput {
        id: txtValue
        anchors {
            right: imgEditable.left
            rightMargin: 20
            verticalCenter: parent.verticalCenter
        }


        width: rightColWidth
        font.pixelSize: 28
        text:   "<No value>"
        color: "white"
        //selectedTextColor: Theme.color.highlight
        validator: RegExpValidator { regExp: new RegExp(regexp === undefined ? ".*" : regexp) }
        font.family: prusaFont.name
        enabled: root.enabled

        onAccepted: {
            if(acceptableInput) {
                root.accepted(value)
                console.log("Accepted " + value)
            }
            else {
                console.log("enter pressed on unacceptable input: " + value)
            }
        }
        onEditingFinished: {
            if(acceptableInput) {
                root.accepted(value)
                console.log("EditingEnd Accepted " + value)
            }
            else {
                console.log("EditingEnd on unacceptable input: " + value)
            }
        }

        onAcceptableInputChanged: {
            if(acceptableInput) {
                root.accepted(value)
            }
        }

        horizontalAlignment:  TextInput.AlignRight
        onActiveFocusChanged: {
            if(activeFocus) {
                //txtValue.selectAll()
            }
        }
    }

    Item {
        id: imgEditable
        height: txtValue.height
        width: visible ? height : 0
        visible: root.enabled
        anchors {
            right: parent.right
            verticalCenter: parent.verticalCenter
        }

        Image {
            id: pencil
            anchors {
                fill: parent
            }
            sourceSize.width: parent.height
            sourceSize.height: parent.height
            fillMode: Image.PreserveAspectFit
            source: Theme.pictogram.path + "/" + Theme.pictogram.edit
        }

        ColorOverlay {
            visible: ! txtValue.acceptableInput
            anchors.fill: pencil
            source: pencil
            color: "red"  // make image like it lays under red glass
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                txtValue.forceActiveFocus()
                txtValue.cursorPosition = txtValue.text.length
            }
        }
    }

    HorizontalSeparator {
        width: parent.width
        anchors.bottom: parent.bottom
    }
}
