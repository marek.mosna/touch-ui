
/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12

Item {
    id: root
    implicitHeight: 1
    implicitWidth: 200
    property int pos: 0
    property string finishedColor: "lightgrey"
    property string unfinishedColor: "#676767"
    property int duration: 0
    property var tStart: new Date().getTime()
    property var tExpectedEnd: new Date().getTime()
    property var update_period: 30

    property var start: function(_duration) {
        pos = 0
        duration = _duration
        tStart = (new Date()).getTime()
        tExpectedEnd += _duration
        animTimer.start()
    }

    Rectangle {
        id: background
        anchors.fill: parent
        color: unfinishedColor
    }

    Rectangle {
        id: finished
        height: parent.height
        width: pos
        anchors {
            left: parent.left
            top: parent.top
        }
        color: pos != root.width ? finishedColor : unfinishedColor
    }
    Timer {
        id: animTimer
        repeat: true
        interval: update_period
        onTriggered: () => {
            var sinceStart = (new Date()).getTime() - tStart
            pos = sinceStart/duration * width
            if(pos >= width) {
                running = false
                pos = width
            }
        }
    }
}
