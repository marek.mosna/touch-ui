
/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import Native 1.0
import PrusaComponents 1.0

//import "3rdparty/qrcode-generator"
/*
 Depends on:
    StackView view

*/

PrusaPage {
    id: root
    title: qsTr("Network")
    name: "NetworkEthernetList"
    pictogram: Theme.pictogram.ethernetSimple
    property int textSize: 16
    property var connectedAP: null

    ListModel {
        id: theModel
        /*
        ListElement {
            type:"onoff_switch";
            text: "Ethernet";
            name: "wifioff";
            checked: true;
            device: "ethernet"
        }
        */
        ListElement {
            type:"eth_network"
            name: "Ethernet 0"// activeConnection.networkId
            device: "/org/freedesktop/NetworkManager/Devices/2"
        }
        ListElement {
            type: "qrcode"
            network: "ethernet"
        }
    }

    Component {
        id: baseDelegate
        Loader {
            source: {
                switch(type) {
                case "onoff_switch": return "DelegateWifiOnOff.qml"
                case "eth_network": return "DelegateEthNetwork.qml"
                case "qrcode": return "DelegateQRCode.qml"
                //case "add_network": return "DelegateAddNetwork.qml"
                default:
                    console.error("ERROR: No delegate for object " + type)
                }
            }
        }
    }

    ListView {
        id: networkListView
        height: 420
        interactive: true
        anchors {
          bottom: parent.bottom
          left: parent.left
          right: parent.right
          rightMargin: 10
          leftMargin: 10
        }
        clip: true
        model: theModel              // concrete model
        delegate: baseDelegate   // provide delegate component.
        spacing: 0
        onCurrentItemChanged: ()=>{
            for(var i=0; i<contentData.length; i++) {
                if(contentData[i].hasOwnProperty("expanded")) {
                    contentData[i].expanded = false
                }
            }

            if(currentItem && currentItem.hasOwnProperty("expanded")) {
                currentItem.expanded = true
            }
        }
        ScrollBar.vertical: ScrollBar {
            active: true
            policy: ScrollBar.AlwaysOn
        }
    }
}
