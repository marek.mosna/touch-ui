#!/bin/bash

# TODO: SSH configuration if not in factory mode.
# TODO: More unified use of SDKs variables made by enviroment* source file.
# TODO: Debuging configuration. Mainly GDB port and stuff.
# TODO: More polite termination of previous instances of tasks.

shopt -s extglob 

# Initial variables for the script: 
# (Do not edit. Only if you now what you are doing and/or don't like to set parameters repeatedly.)
SDK=""
PRINTER_ADDR=""
J="4"
H="no"
TOOLCHAIN_FILE=""
OE_QMAKE_PATH_EXTERNAL_HOST_BINS=""
BUILD_TYPE="Debug"
IN_GAMMARAY="no"
IN_GDB="no"

BINARY="bin/touch-ui"
print_help() {
	echo "
Build script to make Touch-UI.
Uses Yocto SDK if provided otherwise build natively. 
Deploy on networked printer if IP address provided 
and SSH on the printer is enabled.

build.sh [-h|--help]          Print this message.
         [-s|--sdk]           Path to SDK direcotry. 
                                If not specified it will try to use global variables
                                from SDK enviroment setup file.
         [-a|--addr|          IP address of the printer. 
         --printer_address]     Will not deploy if not specified or as
                                global variable PRUSA_PRINTER_IP
         [-t|--build_type]    Build type. (Debug or Release) (default=$BUILD_TYPE)
         [-j]                 How many parallel jobs. (default=$J)
         [--in-gammaray]      Use Gammaray as debugger. (default=$IN_GAMMARAY)
         [--in-gdb]           Use GDB service as debugger. (default=$IN_GDB)
         [--clean]            Only clean build directory.
"
	exit 2
}

TEMP=`getopt -o h,s:,a:,j:,t:: --long sdk:,printer_address:,help,addr:,clean,in-gammaray,in-gdb,build_type: \
     -n 'example.bash' -- "$@"`

if [ $? != 0 ] ; then print_help; exit 1 ; fi
eval set -- "$TEMP"
while true; do
	case "$1" in 
		-h|--help) print_help; exit 0;;
		-s|--sdk) SDK="$2"; shift 2;;
		-a|--addr|--printer_address) PRINTER_ADDR="$2"; shift 2;;
		-j) J="$2"; shift 2;;
		--clean) [ $(echo "$PWD" | rev | cut -d"/" -f1 | rev) == "build" -a -f "${PWD}/build.sh" -a -f "${PWD}/CMakeCache.txt" ] && rm !(build.sh) -rf; exit 0;;
		-t|--build_type) [ "$2" == "Release" -o "$2" == "Debug" ] && BUILD_TYPE="$2" || print_help; shift 2;;
		--in-gammaray) IN_GAMMARAY="yes"; shift 1;;
		--in-gdb) IN_GDB="yes"; shift 1;;
		--) shift; break;;
		\?|*)  print_help; exit 1;;
	esac
done

# Set environment for build
if [ "$SDK" != "" ]; then
	source `find -L $SDK -maxdepth 1 -type f -name environment-setup-*-prusa-linux | head -n 1`
	TOOLCHAIN_FILE="${SDK}/sysroots/x86_64-prusasdk-linux/usr/share/cmake/OEToolchainConfig.cmake"
	OE_QMAKE_PATH_EXTERNAL_HOST_BINS="${SDK}/sysroots/x86_64-prusasdk-linux/usr/bin"
else
	if [ -z ${OECORE_NATIVE_SYSROOT+x} ]; then echo "SDK not inputed or in global variables."; exit 1; fi
	TOOLCHAIN_FILE="${OECORE_NATIVE_SYSROOT}/usr/share/cmake/OEToolchainConfig.cmake"
	OE_QMAKE_PATH_EXTERNAL_HOST_BINS="${OECORE_NATIVE_SYSROOT}/usr/bin"
fi
if [ -z ${PRINTER_ADDR}]; then
	PRINTER_ADDR="${PRUSA_PRINTER_IP}"
fi

echo "
binary:       $BINARY
SDK version:  $OECORE_SDK_VERSION
IP address:   $PRINTER_ADDR
Build type:   $BUILD_TYPE
Jobs:         $J
Debugers:     Gammaray: $IN_GAMMARAY GDB: $IN_GDB
"


# (* OE_QMAKE_PATH_EXTERNAL_HOST_BINS should probably be defined by the toolchain file, something to fix in the future )
if cmake .. -DCMAKE_TOOLCHAIN_FILE="${TOOLCHAIN_FILE}" -DOE_QMAKE_PATH_EXTERNAL_HOST_BINS="$OE_QMAKE_PATH_EXTERNAL_HOST_BINS" -DCMAKE_BUILD_TYPE="$BUILD_TYPE"; then
	echo "[ OK ] CMake succeeded."
else
	echo "[ ERROR ] CMake failed."; exit 1
fi

if make -j "$J"; then
	echo "[ OK ] Make succeeded."
else
	echo "[ ERROR ] Make failed."; exit 1
fi

# Copy the binary into the printer, if the arguments allow for that
if [ -z ${PRINTER_ADDR} ]; then
	echo "PRINTER_ADDR is empty, will not deploy to the printer."
	exit 0
fi

if ! ssh -o StrictHostKeyChecking=no root@$PRINTER_ADDR "systemctl stop touch-ui; killall gdbserver; killall touch-ui; echo;"; then
	echo "[ ERROR ] Could not stop the service"
	exit 1
fi

if ! scp -o StrictHostKeyChecking=no "$BINARY" root@$PRINTER_ADDR:/usr/bin/; then
	echo "[ ERROR ] Could not deploy to the printer."
	exit 1
fi
echo "[ OK ] Copied the binary to the printer."


if [ $IN_GAMMARAY == "yes" ]; then
	if ! ssh -o StrictHostKeyChecking=no root@$PRINTER_ADDR "killall touch-ui; killall gammaray; killall gdbserver; gammaray --inject-only /usr/bin/touch-ui -platform wayland &"; then
        echo "[ ERROR ] Could not start the service"
        exit 1
    fi
elif [ $IN_GDB == "yes" ]; then
    # Make the GDB script
    F=$(mktemp)
    GDB="${OECORE_NATIVE_SYSROOT}/usr/bin/${OECORE_TARGET_ARCH}-prusa-linux/${OECORE_TARGET_ARCH}-prusa-linux-gdb"
    echo "set index-cache on" >> "${F}"
    echo "target extended-remote ${PRINTER_ADDR}:5000" >> "${F}"
    echo "set arg -platform wayland" >> "${F}"
    echo "break main" >> "${F}"

    START_GDB_SCRIPT=$(mktemp)
    echo "#!/bin/bash" > "${START_GDB_SCRIPT}"
    echo "sleep 3" >> "${START_GDB_SCRIPT}"
    echo "$GDB -x $F" >> "${START_GDB_SCRIPT}"

    START_GDB_SERVER=$(mktemp)
    echo "#!/bin/bash" > "${START_GDB_SERVER}"
    echo "ssh -o StrictHostKeyChecking=no root@$PRINTER_ADDR \"killall touch-ui; killall gammaray; killall gdbserver; nohup gdbserver localhost:5000 /usr/bin/touch-ui&\"" >> "${START_GDB_SERVER}"

    # Make screen config file
    KCFG=$(mktemp)
    echo screen $START_GDB_SERVER > $KCFG
    echo "split -v" >> $KCFG
    echo "focus" >> $KCFG
    echo "screen $START_GDB_SCRIPT" >> $KCFG

    chmod a+x "${START_GDB_SERVER}"
    chmod a+x "${START_GDB_SCRIPT}"

    screen -c $KCFG
    echo screen -c $KCFG
else
	if ! ssh -o StrictHostKeyChecking=no root@$PRINTER_ADDR "killall touch-ui; killall gammaray; systemctl restart touch-ui"; then
        echo "[ ERROR ] Could not start the service"
        exit 1
    fi
fi

echo [ DONE ]


