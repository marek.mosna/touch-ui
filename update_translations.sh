#!/bin/bash
echo "This script will update translations/*.ts"
echo "according to the current source files." 
echo "(* lrelease is taken care of by during build)"

lupdate *.qml *.cpp -ts translations/*.ts

