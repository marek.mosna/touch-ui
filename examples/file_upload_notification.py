#!/usr/bin/python3
"""
Notification system usage example, creates 3 Notification and fakes progress on 
two of them using Notify1.modify(..)
After that, notifications are removed, only "download.txt" remains.

Note that upload/download notifications should set the "done" property
when they are finished and should not be removed from code, leave that to the user.

"""
import pydbus
import sys, time
from  gi.repository.GLib import Variant as Variant
bus = pydbus.SystemBus()
notify = bus.get("cz.prusa3d.sl1.Notify1")
progress = 0.0
#                                         filename, progress, total size

handle_down = notify.addFileDownloadNotification("download.txt", 0, 1024**2, "/var/projects/download.txt")
handle_down_progressing = notify.addFileDownloadNotification("progressing_download.txt", 0.1, 1024**2, "/var/projects/download.txt")
handle = notify.addFileUploadNotification("test.txt", 0.1, 1024**2, "/var/projects/test.txt")

print(f"FileDownload download.txt handle: {handle_down}")
print(f"FileDownload progressing_download.txt handle: {handle_down_progressing}")
print(f"FileUpload test.txt handle: {handle}")

if handle > 0:
    for i in range(100):
        progress = (1.0 / 100) * i
        r = notify.modify(handle, "progress", Variant("d", progress))
        if r == False:
            print("Could not modify the notification.")
            break
        r = notify.modify(handle_down_progressing, "progress", Variant("d", progress/2))
        time.sleep(0.2)
        print(f"Handle: {handle} progress: {progress}")
        print(f"Handle: {handle_down_progressing} progress: {progress/2}")
    # upload/download notifications have "done"  property, that should
    # be set when finished.
    r = notify.modify(handle, "done", Variant("b", True))
    if not r:
        print("Could not set \"done\" property.")
    else:
        print("Done.")
r = notify.removeNotification(handle)
print("Removed the upload notification for {}? {}".format("test.txt", r))
r = notify.removeNotification(handle_down_progressing)
print("Removed the download notification for {}? {}".format("progressing_download.txt", r))

r = notify.modify(handle_down, "errorCode", Variant("d", 501))
print(f"Setting an error for download.txt? {r}")

print("The only remaining notification should be DownloadNotification for download.txt")
        
            


