#!/usr/bin/python3
""" Creates an example SwUpdateAvailable notification """
import pydbus
import sys, time
from  gi.repository.GLib import Variant as Variant
bus = pydbus.SystemBus()
notify = bus.get("cz.prusa3d.sl1.Notify1")
progress = 0.0

handle = notify.addSwUpdateAvailableNotification(
        "/var/xxx/bundle.raucb", 
        "1.4.5-fake",
        "1.5.6-fake",
        "http://prusa3d.cz",
        [12,54,54,32,43,34,34,34],
        1592056205,
         """
* Feature
* Feature
* Bug
* Answer to the ultimate question
        """)

print(handle)

