#ifndef ERRORSSL1_H
#define ERRORSSL1_H

#include <QObject>

/** Class exposing SL1 Error enums and methods to manipulate them
 *
**/
class ErrorsSL1 : public QObject
{
    Q_OBJECT
public:
    explicit ErrorsSL1(QObject *parent = nullptr);
    // TODO: autogenerate this enum from Prusa-Error-Codes
    enum Errors {
        TILT_HOME_FAILED = 10101,
        TOWER_HOME_FAILED = 10102,
        TOWER_MOVE_FAILED = 10103,
        FAN_FAILED = 10106,
        RESIN_TOO_LOW = 10108,
        RESIN_TOO_HIGH = 10109,
        NOT_MECHANICALLY_CALIBRATED = 10113,
        TOWER_ENDSTOP_NOT_REACHED = 10114,
        TILT_ENDSTOP_NOT_REACHED = 10115,
        TOWER_AXIS_CHECK_FAILED = 10118,
        TILT_AXIS_CHECK_FAILED = 10119,
        DISPLAY_TEST_FAILED = 10120,
        INVALID_TILT_ALIGN_POSITION = 10121,
        FAN_RPM_OUT_OF_TEST_RANGE = 10122,
        TOWER_BELOW_SURFACE = 10123,
        RESIN_MEASURE_FAILED = 10124,
        CLEANING_ADAPTOR_MISSING = 10125,
        FAN_FAILED_ID = 10126,
        FAN_RPM_OUT_OF_TEST_RANGE_ID = 10127,
        TEMP_SENSOR_FAILED = 10205,
        UVLED_HEAT_SINK_FAILED = 10206,
        A64_OVERHEAT = 10207,
        TEMPERATURE_OUT_OF_RANGE = 10208,
        UV_TEMP_SENSOR_FAILED = 10209,
        TEMP_SENSOR_FAILED_ID = 10210,
        TEMPERATURE_OUT_OF_RANGE_ID = 10211,
        MOTION_CONTROLLER_WRONG_REVISION = 10301,
        MOTION_CONTROLLER_EXCEPTION = 10306,
        RESIN_SENSOR_FAILED = 10307,
        NOT_UV_CALIBRATED = 10308,
        UVLED_VOLTAGE_DIFFER_TOO_MUCH = 10309,
        SOUND_TEST_FAILED = 10310,
        UV_LED_METER_NOT_DETECTED = 10311,
        UV_LED_METER_NOT_RESPONDING = 10312,
        UV_LED_METER_COMMUNICATION_ERROR = 10313,
        DISPLAY_TRANSLUCENT = 10314,
        UNEXPECTED_UV_INTENSITY = 10315,
        UNKNOWN_UV_MEASUREMENT_ERROR = 10316,
        UV_TOO_BRIGHT = 10317,
        UV_TOO_DIMM = 10318,
        UV_INTENSITY_DEVIATION_TOO_HIGH = 10319,
        BOOSTER_ERROR = 10320,
        UV_LEDS_DISCONNECTED = 10321,
        UV_LEDS_ROW_FAILED = 10322,
        UNKNOWN_PRINTER_MODEL = 10323,
        MQTT_SEND_FAILED = 10401,
        NOT_CONNECTED_TO_NETWORK = 10402,
        CONNECTION_FAILED = 10403,
        DOWNLOAD_FAILED = 10404,
        INVALID_API_KEY = 10405,
        UNAUTHORIZED = 10406,
        REMOTE_API_ERROR = 10407,
        NONE = 10500,
        UNKNOWN = 10501,
        PRELOAD_FAILED = 10503,
        PROJECT_FAILED = 10504,
        CONFIG_EXCEPTION = 10505,
        NOT_AVAILABLE_IN_STATE = 10506,
        DBUS_MAPPING_ERROR = 10507,
        REPRINT_WITHOUT_HISTORY = 10508,
        MISSING_WIZARD_DATA = 10509,
        MISSING_CALIBRATION_DATA = 10510,
        MISSING_UV_CALIBRATION_DATA = 10511,
        MISSING_UVPWM_SETTINGS = 10512,
        FAILED_UPDATE_CHANNEL_SET = 10513,
        FAILED_UPDATE_CHANNEL_GET = 10514,
        WARNING_ESCALATION = 10515,
        NOT_ENOUGH_INTERNAL_SPACE = 10516,
        ADMIN_NOT_AVAILABLE = 10517,
        FILE_NOT_FOUND = 10518,
        INVALID_EXTENSION = 10519,
        FILE_ALREADY_EXISTS = 10520,
        INVALID_PROJECT = 10521,
        WIZARD_NOT_CANCELABLE = 10522,
        MISSING_EXAMPLES = 10523,
        FAILED_TO_LOAD_FACTORY_LEDS_CALIBRATION = 10524,
        FAILED_TO_SERIALIZE_WIZARD_DATA = 10525,
        FAILED_TO_SAVE_WIZARD_DATA = 10526,
        SERIAL_NUMBER_IN_WRONG_FORMAT = 10527,
        NO_EXTERNAL_STORAGE = 10528,
        FAILED_TO_SET_LOGLEVEL = 10529,
        FAILED_TO_SAVE_FACTORY_DEFAULTS = 10530,
        FAILED_TO_DISPLAY_IMAGE = 10531,
        NO_UV_CALIBRATION_DATA = 10532,
        DATA_FROM_UNKNOWN_UV_SENSOR = 10533,
        UPDATE_FAILED = 10534,
        NO_DISPLAY_USAGE_DATA = 10535,
        FAILED_TO_SET_HOSTNAME = 10536,
        FAILED_PROFILE_IMPORT = 10537,
        FAILED_PROFILE_EXPORT = 10538,
        PROJECT_ERROR_CANT_READ = 10539,
        PROJECT_ERROR_NOT_ENOUGH_LAYERS = 10540,
        PROJECT_ERROR_CORRUPTED = 10541,
        PROJECT_ERROR_ANALYSIS_FAILED = 10542,
        PROJECT_ERROR_CALIBRATION_INVALID = 10543,
        PROJECT_ERROR_WRONG_PRINTER_MODEL = 10544,
        PROJECT_ERROR_CANT_REMOVE = 10545,
        DIRECTORY_NOT_EMPTY = 10546,
        LANGUAGE_ERROR = 10547,
        OLD_EXPO_PANEL = 10548,
        SYSTEM_SERVICE_CRASHED = 10549,

        ALTERNATIVE_SLOT_BOOT = 10601,
        NONE_WARNING = 10700,
        UNKNOWN_WARNING = 10701,
        AMBIENT_TOO_HOT_WARNING = 10702,
        AMBIENT_TOO_COLD_WARNING = 10703,
        PRINTING_DIRECTLY_WARNING = 10704,
        PRINTER_MODEL_MISMATCH_WARNING = 10705,
        RESIN_NOT_ENOUGH_WARNING = 10706,
        PROJECT_SETTINGS_MODIFIED_WARNING = 10707,
        PERPARTES_NOAVAIL_WARNING = 10708,
        MASK_NOAVAIL_WARNING = 10709,
        OBJECT_CROPPED_WARNING = 10710,
        PRINTER_VARIANT_MISMATCH_WARNING = 10711,
        RESIN_LOW = 10712,
        FAN_WARNING = 10713,
        EXPECT_OVERHEATING = 10714,
    };
    Q_ENUM(Errors)

    static void registerTypes();

    /** Convert a number into a string representation used by the backend
     *  Wrapper for use in QML
     * @param err One of the Errors enum
     * @return String representation of the error code, i.e. "#10501"
    */
    Q_INVOKABLE QString toStringCode(ErrorsSL1::Errors err);
    static QString static_toStringCode(ErrorsSL1::Errors err);

    /** Convert the backend string representation into an enum
     *  Wrapper for use in QML.
     * @param errStr string error code i.e. "#10501"
     * @return converted error or UNKNOWN if unable
    */
    Q_INVOKABLE ErrorsSL1::Errors fromStringCode(QString errStr);
    static ErrorsSL1::Errors static_fromStringCode(QString errStr);

    /** Get a name of an error(by the enum)
     *  Wrapper for use in QML
     * @param err One of the Errors enum
     * @return Name of the error, i.e. "UNKNOWN" or "GENERAL_NOT_CONNECTED_TO_NETWORK"
     */
    Q_INVOKABLE QString toString(ErrorsSL1::Errors err);
    static QString static_toString(ErrorsSL1::Errors err);

    /** Convert the error name into its enum
     *  Wrapper for use in QML
     * @param str name of the errro
     * @return converted error or UNKNOWN if unable
     */
    Q_INVOKABLE ErrorsSL1::Errors fromString(QString str);
    static ErrorsSL1::Errors static_fromString(QString str);

    /**
     * @brief parseFromDBus
     * @param str A string to be parsed, example: "e10544.ProjectErrorWrongPrinterModel"
     * @return processed error code or Errors::UNKNOWN
     */
    Q_INVOKABLE ErrorsSL1::Errors parseFromDBus(QString str);
    static ErrorsSL1::Errors static_parseFromDBus(QString str);

    static void tests();


signals:

public slots:
};

#endif // ERRORSSL1_H
