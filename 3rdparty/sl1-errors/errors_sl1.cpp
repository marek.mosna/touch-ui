#include "errors_sl1.h"
#include <QMetaEnum>
#include <QMetaObject>
#include <QRegExp>

ErrorsSL1::ErrorsSL1(QObject *parent) : QObject(parent)
{
    registerTypes();
}

void ErrorsSL1::registerTypes()
{
    static bool registered = false;
    if( ! registered) {
        qRegisterMetaType<Errors>("Errors");
        registered = true;
    }
}

QString ErrorsSL1::toStringCode(Errors err)
{
    return static_toStringCode(err);
}

QString ErrorsSL1::static_toStringCode(Errors err)
{
    // String is simply # + error_number, this may change in the future
    return "#" + QString::number(err);
}

ErrorsSL1::Errors ErrorsSL1::fromStringCode(QString errStr)
{
    return static_fromStringCode(errStr);
}

ErrorsSL1::Errors ErrorsSL1::static_fromStringCode(QString errStr)
{
    Errors ret = Errors::UNKNOWN;
    QRegExp re("#([0-9]+)");
    re.indexIn(errStr);
    QStringList captured = re.capturedTexts();
    if(captured.length() > 1) {
        QString match = captured.at(1);
        QMetaEnum metaEnum = QMetaEnum::fromType<Errors>();
        Q_ASSERT(metaEnum.isValid());
        bool ok = false;
        const char * key = metaEnum.valueToKey(match.toInt(&ok));
        if(ok && key) {// Valid enum value
            ret = Errors(metaEnum.keyToValue(key));
        }

    }
    return ret;
}

QString ErrorsSL1::toString(Errors err)
{
    return static_toString(err);
}

QString ErrorsSL1::static_toString(Errors err)
{
    QMetaEnum metaEnum = QMetaEnum::fromType<Errors>();
    Q_ASSERT(metaEnum.isValid());
    const char * key = metaEnum.valueToKey(err);
    if(key) return QString(key);
    else return "";
}

ErrorsSL1::Errors ErrorsSL1::fromString(QString errStr)
{
    return static_fromString(errStr);
}

ErrorsSL1::Errors ErrorsSL1::static_fromString(QString str)
{
    Errors ret = Errors::UNKNOWN;
    QMetaEnum metaEnum = QMetaEnum::fromType<Errors>();
    Q_ASSERT(metaEnum.isValid());
    bool ok = false;
    int errorCode = metaEnum.keyToValue(str.toStdString().c_str(), &ok);
    if(ok) {// Valid enum value
        ret = Errors(errorCode);
    }
    return ret;
}

ErrorsSL1::Errors ErrorsSL1::parseFromDBus(QString str)
{
    return static_parseFromDBus(str);
}

ErrorsSL1::Errors ErrorsSL1::static_parseFromDBus(QString str)
{
    Errors ret = Errors::UNKNOWN;
    QRegExp re("e([^\\.]+)\\..*");
    re.indexIn(str);
    QStringList captured = re.capturedTexts();
    if(captured.length() > 1) {
        bool ok = false;
        ret =  Errors(captured[1].toInt(&ok));
        ret = ok ? ret : Errors::UNKNOWN;
    }
    return ret;
}

