/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NATIVEFUNCTIONS_H
#define NATIVEFUNCTIONS_H

#include <QObject>
#include <QJSValue>
#include <QJSEngine>
#include <QQmlApplicationEngine>
#include <QImage>
class NativeFunctions : public QObject
{
    Q_OBJECT
    QQmlApplicationEngine& m_appEngine;

    /** Helper function, recursively search children */
    static QObject* FindByName(QList<QObject*> nodes, const QString& name);

    /** Process-handliong method */
    void runProcess(QString program, QStringList args, std::function<void(int)> ok_callback, std::function<void(int)> error_callback);

public:
    explicit NativeFunctions(QQmlApplicationEngine & _engine, QObject *parent = nullptr);

    /** Reboot the printer
    @param error_callback Called on error
    */
    Q_INVOKABLE void reboot(QJSValue error_callback = QJSValue());

    /** Make a screenshot
    @param path Path to the output file
    @param ok_callback Called on success
    @param error_callback Called on error
    @param itemName objectName of the object to be screenshot(will be searched preorder from root object)
    */
    Q_INVOKABLE void screenshot(QString path, QJSValue ok_callback = QJSValue(), QJSValue error_callback = QJSValue(), QString itemName = QStringLiteral("basicBottomPage"));

    Q_INVOKABLE void screenshotWindow(QString path, QJSValue ok_callback = QJSValue(), QJSValue error_callback = QJSValue());

    /** Check if a file exists, should be fast enought to do syncronously */
    Q_INVOKABLE bool syncFileExists(QString path);

signals:

public slots:
};

#endif // NATIVEFUNCTIONS_H
