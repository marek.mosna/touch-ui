﻿/*
    Copyright 2019-2020, Prusa Research s.r.o.
    Copyright 2021, Prusa Research a.s.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PRINTER_H
#define PRINTER_H

#include <QObject>
#include <QString>
#include <QMap>
#include <QJSValue>
#include <QJSEngine>
#include <semver.hpp>
#include "printer_proxy.h"
#include "exposure.h"
#include "examples.h"
#include "properties_proxy.h"
#include "notificationmodel.h"
#include "dbus_utils/basedbusobject.h"

/** @brief QML accessible wrapper around the printer0 proxy class
 *  Allows access to the printer properties.
 * BTW: QML does not support dynamic properties(on c++ side), so all of them have to be specified here.
 **/
class Printer : public BaseDBusObject
{
    Q_OBJECT
    PrinterProxy m_printerProxy;

    NotificationModel & m_notificationModel;

    /** Storage for already existing exposure objects */
    QMap<QString, Exposure*> m_exposureObjects;
    bool m_propertiesLoaded;

    /** Should the reloading if properties fail, the attempt will be repeated periodically until it succeeds */
    QTimer m_reloadTimer;


public:

    enum PrinterState {
        INITIALIZING = 0,
        IDLE = 1,
        //UNBOXING = 2, <-- obsolete
        WIZARD = 3,
        //CALIBRATION = 4, <-- obsolete
        //DISPLAY_TEST = 5, <-- obsolete
        PRINTING = 6,
        UPDATE = 7,
        ADMIN = 8,
        EXCEPTION = 9,
        UPDATE_MC = 10,
        OVERHEATING = 11,
    };
    Q_ENUM(PrinterState)

    enum class MovementSpeed {
        FAST_DOWN = -2,
        SLOW_DOWN = -1,
        STOP = 0,
        SLOW_UP = 1,
        FAST_UP = 2,
    };
    Q_ENUM(MovementSpeed)

    enum PrinterModel {
        NONE = 0,
        SL1 = 1,
        SL1S = 2,
        M1 = 3, // Prusa Medical
    };
    Q_ENUM(PrinterModel)


    inline static const QString service{"cz.prusa3d.sl1.printer0"};
    inline static const QString interface{"cz.prusa3d.sl1.printer0"};
    inline static const QString path{"/cz/prusa3d/sl1/printer0"};

    /// Wrapper around semver::version
    struct version_t {
        semver::version m_version;

        version_t(QString str = "0.0.0") {
            fromString(str);
        }

        friend bool operator==(const version_t & a, const version_t & b) {
            return a.m_version == b.m_version;
        }

        friend bool operator!=(const version_t & a, const version_t & b) {
            return a.m_version != b.m_version;
        }

        friend bool operator<=(const version_t & a, const version_t & b) {
           return a.m_version <= b.m_version;
        }

        void fromString(QString versionString) {
            versionString = versionString.split("+")[0]; // Remove metadata
            m_version = semver::from_string_noexcept(versionString.toStdString()).value_or(semver::version(0, 0, 0));
            if(m_version == semver::version(0, 0, 0)) {
                m_version =  semver::from_string_noexcept(versionString.split("-")[0].toStdString()).value_or(semver::version(0, 0, 0));
            }
            if(m_version == semver::version(0, 0, 0)) {
                qDebug() << "invalid semantic version: " << versionString;
            }
        }

        QString toString() const {
            try {
                return QString::fromStdString(m_version.to_string());
            }  catch (...) {
                qDebug() << "invalid semantic version toString()";
                return "0.0.0";
            }
        }
    };


    Q_PROPERTY(QString api_key READ api_key NOTIFY api_keyChanged)

    Q_PROPERTY(QString controller_revision READ controller_revision NOTIFY controller_revisionChanged)

    Q_PROPERTY(QString controller_serial READ controller_serial NOTIFY controller_serialChanged)

    Q_PROPERTY(QString controller_sw_version READ controller_sw_version NOTIFY controller_sw_versionChanged)

    Q_PROPERTY(QString booster_serial READ booster_serial NOTIFY booster_serialChanged)

    Q_PROPERTY(QString expo_panel_serial READ expo_panel_serial NOTIFY expo_panel_serialChanged)

    Q_PROPERTY(double expo_panel_transmittance READ expo_panel_transmittance NOTIFY expo_panel_transmittanceChanged)

    Q_PROPERTY(bool cover_state READ cover_state NOTIFY cover_stateChanged)

    Q_PROPERTY(double cpu_temp READ cpu_temp NOTIFY cpu_tempChanged)

    Q_PROPERTY(QString current_page READ current_page NOTIFY current_pageChanged)

    Q_PROPERTY(QString help_page_url READ help_page_url NOTIFY help_page_urlChanged)

    Q_PROPERTY(bool factory_mode READ factory_mode NOTIFY factory_modeChanged)

    Q_PROPERTY(QString internal_project_path READ internal_project_path NOTIFY internal_project_pathChanged)

    Q_PROPERTY(QVariantMap leds READ leds NOTIFY ledsChanged)

    Q_PROPERTY(bool power_switch_state READ power_switch_state NOTIFY power_switch_stateChanged)

    Q_PROPERTY(QVariantMap printer_exception READ printer_exception NOTIFY printer_exceptionChanged)

    Q_PROPERTY(QVariantMap statistics READ statistics NOTIFY statisticsChanged)

    Q_PROPERTY(QStringList project_extensions READ project_extensions NOTIFY project_extensionsChanged)

    Q_PROPERTY(bool resin_sensor_state READ resin_sensor_state NOTIFY resin_sensor_stateChanged)

    Q_PROPERTY(QString serial_number READ serial_number NOTIFY serial_numberChanged)

    Q_PROPERTY(PrinterState state READ state NOTIFY stateChanged)

    Q_PROPERTY(PrinterModel printer_model READ printer_model NOTIFY printer_modelChanged)

    Q_PROPERTY(QString system_name READ system_name NOTIFY system_nameChanged)

    Q_PROPERTY(QString system_version READ system_version NOTIFY system_versionChanged)

    Q_PROPERTY(double uv_led_temp READ uv_led_temp NOTIFY uv_led_tempChanged)

    Q_PROPERTY(double ambient_temp READ ambient_temp NOTIFY ambient_tempChanged)

    Q_PROPERTY(int tilt_position READ tilt_position NOTIFY tilt_positionChanged)
    Q_PROPERTY(int tower_position_nm READ tower_position_nm NOTIFY tower_position_nmChanged)

    Q_PROPERTY(QVariantMap uv_statistics READ uv_statistics NOTIFY uv_statisticsChanged)

    Q_PROPERTY(bool admin_enabled READ admin_enabled NOTIFY admin_enabledChanged)
    Q_PROPERTY(QString current_exposure READ current_exposure NOTIFY current_exposureChanged)
    Q_PROPERTY(bool http_digest READ http_digest NOTIFY http_digestChanged)
    Q_PROPERTY(bool data_privacy READ data_privacy NOTIFY data_privacyChanged)
    Q_PROPERTY(double resin_tank_capacity_ml READ resin_tank_capacity_ml NOTIFY resin_tank_capacity_mlChanged)

    Q_PROPERTY(bool unboxed READ unboxed NOTIFY unboxedChanged)
    Q_PROPERTY(bool mechanically_calibrated READ mechanically_calibrated NOTIFY mechanically_calibratedChanged)
    Q_PROPERTY(bool uv_calibrated READ uv_calibrated NOTIFY uv_calibratedChanged)
    Q_PROPERTY(bool self_tested READ self_tested NOTIFY self_testedChanged)

    Q_PROPERTY(Exposure * current_exposure_object READ getCurrentExposureObject NOTIFY current_exposure_objectChanged)
    Q_PROPERTY(QStringList exposureList READ exposureList  NOTIFY exposureListChanged MEMBER m_exposureList)
    Q_PROPERTY(Examples* examplesObject READ examplesObject NOTIFY examplesObjectChanged)
    Q_PROPERTY(QString multimediaRoot READ multimediaRoot NOTIFY multimediaRootChanged)
    Q_PROPERTY(QString multimediaImages READ multimediaImages NOTIFY multimediaImagesChanged)
    Q_PROPERTY(QString multimediaSounds READ multimediaSounds NOTIFY multimediaSoundsChanged)
    Q_PROPERTY(bool printerReady READ printerReady NOTIFY printerReadyChanged)


    Q_PROPERTY(int fanUvLedRpm READ fanUvLedRpm NOTIFY fanUvLedRpmChanged)
    Q_PROPERTY(bool fanUvLedError READ fanUvLedError NOTIFY fanUvLedErrorChanged)

    Q_PROPERTY(int fanBlowerRpm READ fanBlowerRpm NOTIFY fanBlowerRpmChanged)
    Q_PROPERTY(bool fanBlowerError READ fanBlowerError NOTIFY fanBlowerErrorChanged)

    Q_PROPERTY(int fanRearRpm READ fanRearRpm NOTIFY fanRearRpmChanged)
    Q_PROPERTY(bool fanRearError READ fanRearError NOTIFY fanRearErrorChanged)



    explicit Printer(QJSEngine & engine, NotificationModel & notificationModel, QObject *parent = nullptr);

    Notification * makeLastPrintNotification(Exposure * exposure, QString objPath);

    /** Most of the properties are cached from the underlying m_printerProxy object
     * and are tracking changes. They still need to be synced in the first place.
     **/
    virtual void reload_properties() override;

    /** Check if the provided version is compatible with the provided printer model */
    Q_INVOKABLE bool isVersionCompatibleWithHw(QString versionString, PrinterModel model) const {
        version_t sl1sMinimalVersion("1.6.0-alpha.1"); // Earliest version compatible with SL1S
        version_t version(versionString);
        bool ret = false;
        switch(model) {
        case PrinterModel::NONE: // Assumed SL1m fall-through
        case PrinterModel::SL1:
            ret = true; // All versions(so far) are compatible with sl1 model
            break;
        case PrinterModel::M1: // Same as SL1S, fall-through
        case PrinterModel::SL1S:
            ret = (sl1sMinimalVersion <= version);
            break;
        default: // Some future model
            // Any future models will require at least the same version of firmware
            // as that required by SL1S.
            ret = (sl1sMinimalVersion <= version);
        }
        return ret;
    }

    /** Enable version comparison to QML */
    Q_INVOKABLE bool versionLessEqual(QString versionString1, QString versionString2) const {
        return version_t(versionString1) <= version_t(versionString2);
    }

    /** @property Printer::controller_serial
     *  @brief Serial number of the motion controller
     *
     * Default value: -
    **/
    QString controller_serial() const;


    /** @property Printer::uv_statistics
     *  @brief
     *
     * Default value: -
    **/
    QVariantMap uv_statistics() const;

    /** @property Printer::controller_sw_version
     *  @brief Version of the motion controller firmware
     *
     * Default value: -
    **/
    QString controller_sw_version() const;

    /** @property Printer::booster_serial
     *  @brief Booster board serial number
     *
     * Default value: -
    **/
    QString booster_serial() const;

    /** @property Printer::expo_panel_serial
     *  @brief Exposure display serial number
     *
     * Default value: -
    **/
    QString expo_panel_serial() const;

    /** @property Printer::expo_panel_transmittance
     *  @brief Exposure display transmittance [%]
     *
     * Default value: -
    **/
    double expo_panel_transmittance() const;

    /** @property Printer::internal_project_path
     *  @brief Filesystem path to the projects root on the internal storage
     *
     * Default value:
    **/
    QString internal_project_path() const;

    /** @property Printer::tower_position_nm
         *  @brief Position of the printer's vertical tower in nanometers
         *
         * Default value: 0
        **/
    int tower_position_nm() const;

    /** @property Printer::controller_revision
     *  @brief ?
     *
     * Default value:
    **/
    QString controller_revision() const;

    /** @property Printer::project_extensions
     *  @brief Set of supported project extensions
     *
     * Default value:
    **/
    QStringList project_extensions() const;

    /** @property Printer::resin_sensor_state
     *  @brief Resin sensor enabled state
     * True if enabled, False otherwise
     * Default value: True
    **/
    bool resin_sensor_state() const;

    /** @property Printer::state
     *  @brief State of the printer FSM
     * Possible values: PrinterState enum
     * Default value:
     * @sa Printer::PrinterState
    **/
    PrinterState state() const;

    /** @property Printer::cpu_temp
     *  @brief Temperature of the printer's CPU
     *  In celsius degrees.
     * Default value:
    **/
    double cpu_temp() const;

    /** @property Printer::serial_number
     *  @brief Serial number of the printer
     *
     * Default value:
    **/
    QString serial_number() const;

    /** @property Printer::api_key
     *  @brief API key
     *  An ASCII string serving as a simple autentication measure,
     *  mainly to keep compatibility with octoprint(slicer upload).
     * OBSOLETE: It will be replaced with something else in the future
     * because it is unsafe.
     * Default value:
    **/
    QString api_key() const;

    /** @property Printer::cover_state
     *  @brief The state of the cover
     * Values: true - closed, false - open
     * Default value:
    **/
    bool cover_state() const;

    /** @property Printer::tilt_position
     *  @brief Read or set tilt position in micro-steps
     *
     * Default value:
    **/
    int tilt_position() const;

    /** @property Printer::factory_mode
     *  @brief Factory mode?
     *  Some additional settings are available in factory mode.
     * Value: true, false
     * Default value:
    **/
    bool factory_mode() const;

    /** @property Printer::system_version
     *  @brief Version of the printer's operating system
     * Value: example: "1.3.3"
     * Default value:
    **/
    QString system_version() const;

    /** @property Printer::power_switch_state
     *  @brief State of the power switch
     * on the front of the printer.
     * Value: true - pressed, false otherwise
     * Default value:
    **/
    bool power_switch_state() const;

    /** @property Printer::current_page
     *  @brief Current page name(of the GUI)
     *  OBSOLETE: to be replaced by D-BUS API
     * Default value:
    **/
    QString current_page() const;


    /** @property Printer::help_page_url
     *  @brief identification of the printer for url
     * Default value: /{printer_identifier}/{fw_version}
    **/
    QString help_page_url() const;

    /** @property Printer::leds
     *  @brief UV LED voltages
     *  There are multiple independently powered LED chips.
     * 	Mapping from LED channel name to voltage value
     * Default value:
    **/
    QVariantMap leds() const;

    /** @property Printer::system_name
     *  @brief The full name of the printer's operating system
    **/
    QString system_name() const;

    /** @property Printer::printer_exception
    *  @brief
    *
    * Default value:
    **/
    QVariantMap printer_exception() const;

    /** @property Printer::statistics
         *  @brief Get statistics
         *
        **/
    QVariantMap statistics() const;

    /** Available setters */
    void setTower_position_nm(int new_val);
    void setTilt_position(int new_val);
    void setAdmin_enabled(bool new_val);
    void setCurrent_exposure(QString new_val);
    void setHttp_digest(bool new_val);
    void setData_privacy(bool new_val);
    void setResin_tank_capacity_ml(double new_val);

signals:
    void propertiesChanged(const QMap<QString, QVariant> &changed_properties);
    void exception(const QMap<QString, QVariant> &exception);
    void controller_serialChanged(QString controller_serial);
    void uv_statisticsChanged(QVariantMap uv_statistics);
    void controller_sw_versionChanged(QString controller_sw_version);
    void booster_serialChanged(QString booster_serial);
    void expo_panel_serialChanged(QString expo_panel_serial);
    void expo_panel_transmittanceChanged(double expo_panel_transmittance);
    void internal_project_pathChanged(QString internal_project_path);
    void tower_position_nmChanged(int tower_position_nm);
    void controller_revisionChanged(QString controller_revision);
    void project_extensionsChanged(QStringList project_extensions);
    void resin_sensor_stateChanged(bool resin_sensor_state);
    void stateChanged(PrinterState state);
    void cpu_tempChanged(double cpu_temp);
    void serial_numberChanged(QString serial_number);
    void api_keyChanged(QString api_key);
    void cover_stateChanged(bool cover_state);
    void tilt_positionChanged(int tilt_position);
    void factory_modeChanged(bool factory_mode);
    void system_versionChanged(QString system_version);
    void power_switch_stateChanged(bool power_switch_state);
    void current_pageChanged(QString current_page);
    void help_page_urlChanged(QString help_page_url);
    void ledsChanged(QVariantMap leds);
    void system_nameChanged(QString system_name);
    void statisticsChanged(QVariantMap statistics);
    void printer_exceptionChanged(QVariantMap printer_exception);
    void admin_enabledChanged(bool admin_enabled);

    void current_exposureChanged(QString current_exposure);

    void http_digestChanged(bool http_digest);
    void data_privacyChanged(bool data_privacy);

    void resin_tank_capacity_mlChanged(double resin_tank_capacity_ml);

    void exposureListChanged(QStringList exposureList);


    void examplesObjectChanged(Examples* examplesObject);

    void current_exposure_objectChanged(Exposure * current_exposure_object);

    void unboxedChanged(bool unboxed);

    void mechanically_calibratedChanged(bool mechanically_calibrated);

    void uv_calibratedChanged(bool uv_calibrated);

    void self_testedChanged(bool self_tested);

    void uv_led_tempChanged(double uv_led_temp);

    void ambient_tempChanged(double ambient_temp);

    void printer_modelChanged(PrinterModel printer_model);

    void multimediaRootChanged();

    void multimediaImagesChanged();

    void multimediaSoundsChanged();

    void printerReadyChanged(bool _printerReady);

    void fanUvLedRpmChanged();

    void fanUvLedErrorChanged();

    void fanBlowerRpmChanged();

    void fanBlowerErrorChanged();

    void fanRearRpmChanged();

    void fanRearErrorChanged();

public slots:
    /** @brief Handle changes to printer0 properties
     * Is connected to the PropertiesChanged signal of Updater1
    **/
    void slotPropertiesChanged(const QString &interface, const QMap<QString, QVariant> &changed_properties, const QStringList &invalidates_properties);

    void setExposureList(QStringList exposureList);

    /** If the old examplesObject pointer is not null,
     * setExamplesObject will delete before replacing it to avoid leaks.
    **/
    void setExamplesObject(Examples* examplesObject)
    {
        if (m_examplesObject == examplesObject)
            return;

        if(m_examplesObject) delete m_examplesObject;

        m_examplesObject = examplesObject;
        emit examplesObjectChanged(m_examplesObject);
    }

    void setUnboxed(bool unboxed)
    {
        if (m_unboxed == unboxed)
            return;

        m_unboxed = unboxed;
        emit unboxedChanged(m_unboxed);
    }

    void setMechanically_calibrated(bool mechanically_calibrated)
    {
        if (m_mechanically_calibrated == mechanically_calibrated)
            return;

        m_mechanically_calibrated = mechanically_calibrated;
        emit mechanically_calibratedChanged(m_mechanically_calibrated);
    }

    void setUv_calibrated(bool uv_calibrated)
    {
        if (m_uv_calibrated == uv_calibrated)
            return;

        m_uv_calibrated = uv_calibrated;
        emit uv_calibratedChanged(m_uv_calibrated);
    }

    void setSelf_tested(bool self_tested)
    {
        if (m_self_tested == self_tested)
            return;

        m_self_tested = self_tested;
        emit self_testedChanged(m_self_tested);
    }

    void setUv_led_temp(double uv_led_temp)
    {
        if (qFuzzyCompare(m_uv_led_temp, uv_led_temp))
            return;

        m_uv_led_temp = uv_led_temp;
        emit uv_led_tempChanged(m_uv_led_temp);
    }

    void setAmbient_temp(double ambient_temp)
    {
        if (qFuzzyCompare(m_ambient_temp, ambient_temp))
            return;

        m_ambient_temp = ambient_temp;
        emit ambient_tempChanged(m_ambient_temp);
    }

#ifdef QT_DEBUG
    Q_INVOKABLE
#endif // QT_DEBUG
    void setPrinter_model(Printer::PrinterModel printer_model)
    {
        if (m_printer_model == printer_model)
            return;

        m_printer_model = printer_model;
        emit printer_modelChanged(m_printer_model);
    }

    void setPrinterReady(bool newVal) {
        if(m_printerReady != newVal) {
            m_printerReady = newVal;
            emit printerReadyChanged(m_printerReady);
        }
    }

    /*  Some properties can be be safely read for the first time only
     *   after the state leaves the set of states {UPDATE_MC, INITIALIZING }.
     *   However, the printer0.state can be read at any time.  So we will wait
     *   until the service is registered on DBus & after that, we will wait further
     *   for the printer0.state to leave the forbidden states.
     */
    void handleServiceRegistrationChanged() override;

public:


    /** @brief Get an exposure object for path
     * Stores already existing exposure objects in m_exposureObjects.
     * This method should have a monopol on creating Exposure objects.
     * NOTE: Exposure objects last forever, shotuld that change, destruction will need to be handled!
     * @return return existing object, if it exist, otherwise create a new one and store it in m_exposureObjects
     */
    Q_INVOKABLE Exposure * getExposureObject(const QString & path);

    /** @brief Get the current exposure object
     * This is the most common usecase.
     * @return nullptr if object is not valid
     */
    Q_INVOKABLE Exposure * getCurrentExposureObject() {
        return getExposureObject(m_current_exposure);
    }

    /** @fn Printer::listExposurePaths
     * @brief List paths to exposure path objects, asynchronous
     * @param callback_ok Javascript function(lst : [path1, path2, ...])
     * @param callback_fail Javascript function(error_string){}
     **/
    Q_INVOKABLE QStringList syncListExposurePaths();

    /** @brief Set property
     * Most of the properties are read-only(setter is not specified), but still need to be updated
     * from time to time. Because the setters are not mentioned in the Q_PROPERTY macro,
     * the setProperty method cannot be used. This is a replacement for that.
     *
     * Will call the appropriate setter for the property.
     * @return true - success, false - property does not exist
     **/
    virtual bool set(const QString & name, const QVariant & value) override;


    /** @brief the only correct way to set API key */
    Q_INVOKABLE bool set_api_key(QString api_key);
    /** @brief the only correct way to set http digest */
    Q_INVOKABLE bool set_http_digest(bool enabled);
    /** @brief the only correct way to set data_privacy */
    Q_INVOKABLE bool set_data_privacy(bool enabled);

    /** @fn Printer::beep
     * @brief Request beep on the motion controller, asynchronous
     * @param callback_ok Javascript function(no parameters)
     * @param callback_fail Javascript function(error_string){}
     **/
    Q_INVOKABLE void beep(int frequency_hz, int length_ms, QJSValue callback_ok, QJSValue callback_fail);

    /** @brief Specialization of beep with default parameters: 1800hz, 50ms
     * Just for convenience.
     */
    Q_INVOKABLE void beep_button();

    /** @fn Printer::disable_motors
     * @brief Request for the motors(all of them) to be disabled, asynchronous
     * @param callback_ok Javascript function(no parameters)
     * @param callback_fail Javascript function(error_string){}
     **/
    Q_INVOKABLE void disable_motors(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());


    /** @fn Printer::enable_resin_sensor
     * @brief Enable/disable resin sensor
     * @param callback_ok Javascript function(no parameters)
     * @param callback_fail Javascript function(error_string){}
     **/
    Q_INVOKABLE void enable_resin_sensor(bool value, QJSValue callback_ok, QJSValue callback_fail);

    /**
     * @brief run_unboxing_wizard Enter the WIZARD state
     **/
    Q_INVOKABLE void run_unboxing_wizard(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    /**
     * @brief run_factory_reset_wizard Enter the WIZARD state
     **/
    Q_INVOKABLE void run_factory_reset_wizard(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    /**
     * @brief run_calibration_wizard Enter the WIZARD state and run the calibration wizard
     */
    Q_INVOKABLE void run_calibration_wizard(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    /**
     * @brief run_uv_calibration_wizard Enter the WIZARD state
     **/
    Q_INVOKABLE void run_uv_calibration_wizard(bool display_replaced, bool led_module_replaced, QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    /**
     * @brief cmd_try_cancel_by_path
     * @param path
     * @param callback_ok  Javascript function(no parameters)
     * @param callback_fail Javascript function(error_string){}
     */
    Q_INVOKABLE void cmd_try_cancel_by_path(QString path,QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    /**
     * @brief run_self_test_wizard Enter the WIZARD state
     */
    Q_INVOKABLE void run_self_test_wizard(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    /**
     * @brief run_kit_unboxing_wizard Enter the WIZARD state
     */
    Q_INVOKABLE void run_kit_unboxing_wizard(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());


    /**
     * @brief run_displaytest_wizard Start the display test wizard
     * @param callback_ok
     * @param callback_fail
     */
    Q_INVOKABLE void run_displaytest_wizard(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    /**
         * @brief run_tank_surface_cleaner_wizard Start the wizard for cleaning the resin tank from debris of exposed resin
         * @param callback_ok
         * @param callback_fail
         */
        Q_INVOKABLE void run_tank_surface_cleaner_wizard(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    /**
     * @brief download_examples
     * @param callback_ok Javascript function(1 parameter = DBus path to the examples0 object)
     * @param callback_fail Javascript function(error_string){}
     */
    Q_INVOKABLE void download_examples(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    /** @brief FIXME!
     * @param callback_ok Javascript function(no parameters)
     * @param callback_fail Javascript function(error_string){}
     **/
    Q_INVOKABLE void list_projects_raw(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    /** @brief Power off the printer
     * @param do_shutdown
     * @param reboot just reboot
     * @param callback_ok Javascript function(no parameters)
     * @param callback_fail Javascript function(error_string){}
     **/
    Q_INVOKABLE void poweroff(bool do_shutdown, bool reboot, QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    /** @brief Start print
     * @param project_path absolute filesystem path to the project
     * @param auto_advance start the print immediately
     * @param callback_ok Javascript function(string exposure_object_dbus_path)
     * @param callback_fail Javascript function(error_string){}
     **/
    Q_INVOKABLE void print(const QString & project_path, bool auto_advance, QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    /** @brief Start the last print again
     * @param auto_advance start the print immediately
     * @param callback_ok Javascript function(string exposure_object_dbus_path)
     * @param callback_fail Javascript function(error_string){}
     **/
    Q_INVOKABLE void reprint(bool auto_advance, QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    /** @brief Home tilt
     * @param callback_ok Javascript function(no parameters)
     * @param callback_fail Javascript function(error_string){}
     **/
    Q_INVOKABLE void tilt_home(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    /** @brief Move tilt
     * @param speed accepted values: {-2: fast back, -1 slow back, 0: stop, 1: slow forward, 2: fast forward}
     * @param callback_ok Javascript function(no parameters)
     * @param callback_fail Javascript function(error_string){}
     **/
    Q_INVOKABLE void tilt_move(int speed, QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    /** @brief Home tower
     * @param callback_ok Javascript function(bool valid_move)
     * @param callback_fail Javascript function(error_string){}
     **/
    Q_INVOKABLE void tower_home(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    /** @brief Move tower
     * @param speed accepted values: {-2: fast up, -1: slow up, 0: stop, 1: slow down, 2: fast down}
     * @param callback_ok Javascript function(no parameters)
     * @param callback_fail Javascript function(error_string){}
     **/
    Q_INVOKABLE void tower_move(int speed, QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    /** @brief Update firmware
     * @param callback_ok Javascript function(no parameters)
     * @param callback_fail Javascript function(error_string){}
     **/
    Q_INVOKABLE void update_firmware(QString fw_file, QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    /** @brief Ask the backend to run all the wizards it needs to make the printer ready to print
     * @param callback_ok Javascript function(no parameters)
     * @param callback_fail Javascript function(error_string){}
    **/
    Q_INVOKABLE void make_ready_to_print(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    /**
     * @brief Add oneclick print inhibitor. Oneclick is disabled until the inhibitor is removed.
     * @param name Name of the new inhibitor
     * @param callback_ok Javascript function(no parameters)
     * @param callback_fail Javascript function(error_string){}
     */
    Q_INVOKABLE void add_oneclick_inhibitor(QString name, QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    /**
     * @brief Remove oneclick print inhibitor.
     * @param name Name of the inhibitor to remove
     * @param callback_ok Javascript function(no parameters)
     * @param callback_fail Javascript function(error_string){}
     */
    Q_INVOKABLE void remove_oneclick_inhibitor(QString name, QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    /**
     * @brief Inject exception into backend
     * @param code Exception code i.e. "#10515"
     * @param callback_ok Javascript function(no parameters)
     * @param callback_fail Javascript function(error_string){}
     */
    Q_INVOKABLE void inject_exception(QString code, QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    /**
     * @brief Run an action that results in a failure
     * @param code Exception code i.e. "#10515"
     * @param callback_ok Javascript function(no parameters)
     * @param callback_fail Javascript function(error_string){}
     */
    Q_INVOKABLE void fail_action(QString code, QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    /**
     * @brief Remote error flag on the beackend side
     * @param callback_ok Javascript function(no parameters)
     * @param callback_fail Javascript function(error_string){}
     */
    Q_INVOKABLE void power_led_remove_error(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    /**
     * @brief Set error flag on the beackend side
     * @param callback_ok Javascript function(no parameters)
     * @param callback_fail Javascript function(error_string){}
     */
    Q_INVOKABLE void power_led_set_error(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    bool admin_enabled() const;

    QString current_exposure() const
    {
        return m_current_exposure;
    }

    bool http_digest() const
    {
        return m_http_digest;
    }

    bool data_privacy() const
    {
        return m_data_privacy;
    }

    double resin_tank_capacity_ml() const
    {
        return m_resin_tank_capacity_ml;
    }

    QStringList exposureList() const
    {
        return m_exposureList;
    }

    Examples* examplesObject() const
    {
        return m_examplesObject;
    }

    bool unboxed() const
    {
        return m_unboxed;
    }

    bool mechanically_calibrated() const
    {
        return m_mechanically_calibrated;
    }

   bool uv_calibrated() const
    {
        return m_uv_calibrated;
    }

    bool self_tested() const
    {
        return m_self_tested;
    }

    double uv_led_temp() const
    {
        return m_uv_led_temp;
    }

    double ambient_temp() const
    {
        return m_ambient_temp;
    }

    PrinterModel printer_model() const
    {
        return m_printer_model;
    }

    /** Root of the directory tree containing multimedia(images, music) */
    const QString &multimediaRoot() const;
    void setMultimediaRoot(const QString &newMultimediaRoot);

    /** Directory with model-specific images */
    const QString multimediaImages() const;

    const QString &multimediaSounds() const;

    bool printerReady() const;

    int fanUvLedRpm() const;
    void setFanUvLedRpm(int newFanUvLedRpm);

    bool fanUvLedError() const;
    void setFanUvLedError(bool newFanUvLedError);

    int fanBlowerRpm() const;
    void setFanBlowerRpm(int newFanBlowerRpm);

    bool fanBlowerError() const;
    void setFanBlowerError(bool newFanBlowerError);

    int fanRearRpm() const;
    void setFanRearRpm(int newFanRearRpm);

    bool fanRearError() const;
    void setFanRearError(bool newFanRearError);

private:
    QString m_usb_path;
    QString m_controller_serial;
    QVariantMap m_uv_statistics;
    QString m_controller_sw_version;
    QString m_booster_serial;
    QString m_expo_panel_serial;
    double m_expo_panel_transmittance{0.0};
    QString m_internal_project_path;
    int m_tower_position_nm{0};
    QString m_controller_revision;
    QStringList m_project_extensions;
    bool m_resin_sensor_state{false};
    PrinterState m_state{PrinterState::INITIALIZING};
    double m_cpu_temp{0.0};
    QString m_serial_number;
    QString m_api_key;
    bool m_cover_state{false};
    int m_tilt_position{0};
    bool m_factory_mode{false};
    QString m_system_version;
    bool m_power_switch_state{false};
    QString m_current_page;
    QString m_help_page_url;
    QVariantMap m_leds;
    QString m_system_name;
    QVariantMap m_printer_exception;
    QVariantMap m_statistics;
    /** Determines if printer0 was initialized and refresh_properties can be called safely **/
    bool m_printerReady{false};
    bool m_admin_enabled{false};
    QString m_current_exposure;
    bool m_http_digest{false};
    bool m_data_privacy{false};
    double m_resin_tank_capacity_ml{0.0};
    Examples* m_examplesObject{nullptr};
    Exposure * m_current_exposure_object{nullptr};
    bool m_unboxed{false};
    bool m_mechanically_calibrated{false};
    bool m_unboxedChanged{false};
    bool m_self_tested{false};
    bool m_uv_calibrated{false};
    double m_uv_led_temp{0.0};
    double m_ambient_temp{0.0};
    PrinterModel m_printer_model{PrinterModel::NONE};
    QString m_multimediaRoot{"file:///usr/share/sla-multimedia/"};
    int m_fanUvLedRpm{0};
    bool m_fanUvLedError{false};
    int m_fanBlowerRpm{0};
    bool m_fanBlowerError{false};
    int m_fanRearRpm{0};
    bool m_fanRearError{false};

    QStringList m_exposureList;

    // Setters that are not acknoledged in the properties, for internal use only
    void setUsb_path(QString new_val);
    void setController_serial(QString new_val);
    void setBooster_serial(QString new_val);
    void setExpo_panel_serial(QString new_val);
    void setExpo_panel_transmittance(double new_val);
    void setUv_statistics(QVariantMap new_val);
    void setController_sw_version(QString new_val);
    void setUvLedFan(MapStringInt new_val);
    void setBlowerFan(MapStringInt new_val);
    void setRearFan(MapStringInt new_val);
    void setInternal_project_path(QString new_val);
    void setController_revision(QString new_val);
    void setProject_extensions(QStringList new_val);
    void setResin_sensor_state(bool new_val);
    void setState(PrinterState new_val);
    void setCpu_temp(double new_val);
    void setSerial_number(QString new_val);
    void setApi_key(QString new_val);
    void setCover_state(bool new_val);
    void setFactory_mode(bool new_val);
    void setSystem_version(QString new_val);
    void setPower_switch_state(bool new_val);
    void setCurrent_page(QString new_val);
    void setHelp_page_url(QString new_val);
    void setLeds(QVariantMap new_val);
    void setSystem_name(QString new_val);
    void setPrinter_exception(QVariantMap new_val);
    void setStatistics(QVariantMap new_val);

};

#endif // PRINTER_H
