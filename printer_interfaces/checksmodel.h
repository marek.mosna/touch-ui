/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CHECKSMODEL_H
#define CHECKSMODEL_H

#include <QAbstractListModel>

/** @brief C++ model for the pre-print checks */
class ChecksModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum Check {
        // Hardware = 1, axis are homed separately
        Temperature = 2,
        Project,
        Fan,
        Cover,
        Resin,
        StartPositions,
        Stirring
    };
    Q_ENUM(Check)

    enum CheckState {
        Scheduled = -1,
        Running,
        Success,
        Failure,
        Warning,
        Disabled
    };
    Q_ENUM(CheckState)
protected:
    struct CheckRecord {
        Check checkId;
        CheckState checkState;
        bool hidden;
        CheckRecord(Check _checkId = Temperature, CheckState _checkState = Scheduled, bool _hidden = false):
            checkId(_checkId), checkState(_checkState), hidden(_hidden)
        {
        }
    };

    enum Roles {
        RoleCheckId = Qt::UserRole + 1,
        RoleCheckState,
        RoleHidden,
    };

    QList<CheckRecord> m_data;


public:
    explicit ChecksModel(QObject *parent = nullptr);
    void resetModel();

    QHash<int, QByteArray> roleNames() const override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    // Add data:
    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

    // Remove data:
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

    Q_INVOKABLE void removeRow(int row);

public slots:
    /** @brief Convenience method to accept changes of checks state */
    void change(const QVariantMap & newValues);

private:
};

#endif // CHECKSMODEL_H
