#include <QMetaObject>
#include <QMetaProperty>
#include <QHash>
#include "connect.h"
#include "errors_sl1.h"
#include "dbusutils.h"

PrusaConnect::PrusaConnect(QJSEngine & _engine, QObject *parent) : QObject(parent),
    m_connectProxy("cz.prusa3d.connect0", "/cz/prusa3d/connect0", QDBusConnection::systemBus(), this),
    m_properties("cz.prusa3d.connect0", "/cz/prusa3d/connect0", QDBusConnection::systemBus(), this),
    m_engine(_engine),
    m_propertiesLoaded(false),
    m_reloadTimer(this)
{
    m_connectProxy.setTimeout(210000);
    m_properties.setTimeout(2000);
    registerPrinterTypes();
    m_reloadTimer.setInterval(3000);
    m_reloadTimer.setSingleShot(true);
    connect(&m_reloadTimer, &QTimer::timeout, this, [this](){this->reload_properties();});

    connect(&m_properties, &PropertiesProxy::PropertiesChanged, this, &PrusaConnect::slotPropertiesChanged);
    reload_properties();
}

void PrusaConnect::reload_properties()
{
    if(m_reloadTimer.isActive()) return; // Only once !

    QDBusPendingReply<QMap<QString, QVariant> > pendingReply = m_properties.GetAll("cz.prusa3d.connect0");

    QDBusPendingCallWatcher * watcher = new QDBusPendingCallWatcher(pendingReply, this);
    connect(watcher, &QDBusPendingCallWatcher::finished, this, [&](QDBusPendingCallWatcher * replyWatcher) mutable {
        QDBusPendingReply<QMap<QString, QVariant> > reply = *replyWatcher;
        if(reply.isError()) {
            qDebug() << "Could not load properties of " << m_connectProxy.interface() << ", will try again in " << this->m_reloadTimer.interval() <<"ms";
            this->m_reloadTimer.start(); // Try again later
        }
        else {
            foreach(QString key, reply.value().keys()) {
                this->set(key, reply.value()[key]);
            }
            if(this->m_propertiesLoaded == false) {
                this->m_propertiesLoaded = true; // Succeeded, no need to repeat
                qDebug() << "Properties of " << m_connectProxy.interface() << "loaded.";
            }
        }
        replyWatcher->deleteLater();
    });
}

void PrusaConnect::slotPropertiesChanged(const QString &interface, const QMap<QString, QVariant> &changed_properties, const QStringList &invalidates_properties)
{
    Q_UNUSED(invalidates_properties)
    Q_UNUSED(interface)
    foreach(QString key, changed_properties.keys()) {
        QVariant v = changed_properties[key];
        if(v.isValid()) {
            set(key, changed_properties.value(key));
        }
        else {
            qDebug() << "Printer::slotPropertiesChanged attempt to set nonexistent property " << key;
        }

    }
    emit propertiesChanged(changed_properties);
}


bool PrusaConnect::is_registered() const{
    return m_is_registered;
}

void PrusaConnect::setIs_registered(bool new_val){
    if(new_val != m_is_registered) {
        m_is_registered = new_val;
        emit is_registeredChanged(new_val);
    }
}

QVariantMap PrusaConnect::last_exception() const {
    return m_last_exception;
}

void PrusaConnect::setLast_exception(QVariantMap new_val)  {
    if(new_val != m_last_exception) {
        m_last_exception = new_val;
        emit last_exceptionChanged(new_val);
    }
}

void PrusaConnect::register_printer(QJSValue callback_ok, QJSValue callback_fail) {
    auto pending = m_connectProxy.register_printer();
    DBusUtils::handle_callbacks(m_engine, pending, callback_ok, callback_fail,  [this](QString err_name){emit dbusError(ErrorsSL1::static_parseFromDBus(err_name));});
}

void PrusaConnect::reset_connect(QJSValue callback_ok, QJSValue callback_fail) {
    auto pending = m_connectProxy.reset_connect();
    DBusUtils::handle_callbacks(m_engine, pending, callback_ok, callback_fail,  [this](QString err_name){emit dbusError(ErrorsSL1::static_parseFromDBus(err_name));});
}

void PrusaConnect::confirm_registration(const QString & tmp_token, QJSValue callback_ok, QJSValue callback_fail) {
    auto pending = m_connectProxy.confirm_registration(tmp_token);
    DBusUtils::handle_callbacks(m_engine, pending, callback_ok, callback_fail,  [this](QString err_name){emit dbusError(ErrorsSL1::static_parseFromDBus(err_name));});
}

bool PrusaConnect::set(QString name, QVariant value)
{
    if(name == QStringLiteral("is_registered")) {
        setIs_registered(value.toBool());
    } else if(name == QStringLiteral("last_exception")) {
        setLast_exception(value.toMap());
    } else {
        qDebug() << "An attempt to set unknown property "<< name;
        return false; // Unknown property
    }
    return true;

}
