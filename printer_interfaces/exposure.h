/*
    Copyright 2019-2020, Prusa Research s.r.o.
    Copyright 2021-2022, Prusa Research a.s.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef EXPOSURE_H
#define EXPOSURE_H

#include <QObject>
#include <QDBusPendingReply>
#include <QJSEngine>
#include <QVariantMap>
#include "exposure_proxy.h"
#include "properties_proxy.h"
#include "printer_types.h"
#include "checksmodel.h"
#include "errors_sl1.h"
#include "basedbusobject.h"

class Exposure : public BaseDBusObject
{
    Q_OBJECT

    inline static const QString service{"cz.prusa3d.sl1.exposure0"};
    inline static const QString interface{"cz.prusa3d.sl1.exposure0"};
    // path is variable in this case (m_objectPath)

    ExposureProxy m_exposureProxy;
    ChecksModel m_checksModel;

public:
    explicit Exposure(QString path, QJSEngine & _engine, QObject *parent = nullptr);

    virtual bool set(const QString & name, const QVariant  &value) override;

    enum ExposureState {
        PRINTING = 1,
        GOING_UP = 2,
        GOING_DOWN = 3,
        WAITING = 4,
        COVER_OPEN = 5,
        FEED_ME = 6,
        FAILURE = 7,
        STIRRING = 9,
        PENDING_ACTION = 10,
        FINISHED = 11,
        STUCK = 12,
        STUCK_RECOVERY = 13,
        READING_DATA = 14,
        CONFIRM = 15,
        CHECKS = 16,

        TILTING_DOWN = 19,
        CANCELED = 20,

        CHECK_WARNING = 23,
        DONE = 24,
        OVERHEATING = 25,
        POUR_IN_RESIN = 26,
        HOMING_AXIS = 27,
    };
    Q_ENUM(ExposureState)


    enum ProjectErrors {
        NONE = 0,
        PROJECT_NOT_FOUND = 1,
        PROJECT_CANT_READ = 2,
        PROJECT_NOT_ENOUGH_LAYERS = 3,
        PROJECT_CORRUPTED = 4,
        PROJECT_ANALYSIS_FAILED = 5,
        PROJECT_CALIBRATION_INVALID = 6
    };
    Q_ENUM(ProjectErrors)

    enum ExposureUserProfile {
        DEFAULT = 0,
        SAFE,
        SUPERSLOW
    };
    Q_ENUM(ExposureUserProfile)

    Q_PROPERTY(QString objectPath READ objectPath CONSTANT)

    Q_PROPERTY(QVariantMap checks_state READ checks_state NOTIFY checks_stateChanged)
    Q_PROPERTY(ExposureState state READ state NOTIFY stateChanged)
    Q_PROPERTY(ChecksModel* checksModel READ checksModel CONSTANT)


    Q_PROPERTY(bool resin_low READ resin_low  NOTIFY resin_lowChanged)
    Q_PROPERTY(double progress READ progress  NOTIFY progressChanged)
    Q_PROPERTY(int total_nm READ total_nm  NOTIFY total_nmChanged)
    Q_PROPERTY(int time_remain_ms READ time_remain_ms  NOTIFY time_remain_msChanged)
    Q_PROPERTY(int total_time_ms READ total_time_ms NOTIFY total_time_msChanged)
    Q_PROPERTY(QString project_name READ project_name  NOTIFY project_nameChanged)
    Q_PROPERTY(double expected_finish_timestamp READ expected_finish_timestamp  NOTIFY expected_finish_timestampChanged)
    Q_PROPERTY(double resin_remaining_ml READ resin_remaining_ml  NOTIFY resin_remaining_mlChanged)
    Q_PROPERTY(double resin_used_ml READ resin_used_ml  NOTIFY resin_used_mlChanged)
    Q_PROPERTY(double print_start_timestamp READ print_start_timestamp  NOTIFY print_start_timestampChanged)
    Q_PROPERTY(double print_end_timestamp READ print_end_timestamp  NOTIFY print_end_timestampChanged)
    Q_PROPERTY(int position_nm READ position_nm  NOTIFY position_nmChanged)
    Q_PROPERTY(int exposure_time_ms READ exposure_time_ms WRITE softSetExposure_time_ms NOTIFY exposure_time_msChanged)
    Q_PROPERTY(int exposure_time_first_ms READ exposure_time_first_ms WRITE softSetExposure_time_first_ms NOTIFY exposure_time_first_msChanged)
    Q_PROPERTY(bool resin_warn READ resin_warn  NOTIFY resin_warnChanged)
    Q_PROPERTY(int current_layer READ current_layer  NOTIFY current_layerChanged)
    Q_PROPERTY(int exposure_time_calibrate_ms READ exposure_time_calibrate_ms WRITE softSetExposure_time_calibrate_ms NOTIFY exposure_time_calibrate_msChanged)
    Q_PROPERTY(int layer_height_first_nm READ layer_height_first_nm  NOTIFY layer_height_first_nmChanged)
    Q_PROPERTY(int layer_height_nm READ layer_height_nm  NOTIFY layer_height_nmChanged)
    Q_PROPERTY(int total_layers READ total_layers  NOTIFY total_layersChanged)
    Q_PROPERTY(double wait_until_timestamp READ wait_until_timestamp  NOTIFY wait_until_timestampChanged)
    Q_PROPERTY(double exposure_end READ exposure_end  NOTIFY exposure_endChanged)
    Q_PROPERTY(int remaining_wait_sec READ remaining_wait_sec  NOTIFY remaining_wait_secChanged)
    Q_PROPERTY(QVariantMap exposure_exception READ exposure_exception  NOTIFY exposure_exceptionChanged)
    Q_PROPERTY(QString project_file READ project_file  NOTIFY project_fileChanged)

    Q_PROPERTY(double total_resin_required_ml READ total_resin_required_ml NOTIFY total_resin_required_mlChanged)
    Q_PROPERTY(double total_resin_required_percent READ total_resin_required_percent NOTIFY total_resin_required_percentChanged)
    Q_PROPERTY(double resin_measured_ml READ resin_measured_ml NOTIFY resin_measured_mlChanged)
    Q_PROPERTY(QVariantMap exposure_warning READ exposure_warning NOTIFY exposure_warningChanged)
    Q_PROPERTY(bool close_cover_warning READ close_cover_warning NOTIFY close_cover_warningChanged)
    Q_PROPERTY(int calibration_regions READ calibration_regions NOTIFY calibration_regionsChanged)
    Q_PROPERTY(ExposureUserProfile user_profile READ user_profile WRITE softSetUser_profile NOTIFY user_profileChanged)

    /** @property Exposure::checks_state
     * @brief State of the pre-print checks
     * Mapping ChecksModel::Check -> CheckModel::CheckState
     * NOTE: Transformed to QVariantMap to make it accessible to QML
     */
    QVariantMap checks_state();

    /** @property Exposure::state
     * @brief State of the exposure object
     */
    inline ExposureState state() const { return m_state; }

    /** @property Exposure::checksModel
     * @brief Model of the checks, that are running before every print
     * The model is kept actual by refreshing state on every checks_stateChanged signal.
     */
    inline ChecksModel * checksModel() {return &m_checksModel; }

    inline bool resin_low() const {
        return m_resin_low;
    }


    inline double progress() const {
        return m_progress;
    }


    inline int total_nm() const {
        return m_total_nm;
    }


    inline int time_remain_ms() const {
        return m_time_remain_ms;
    }


    inline QString project_name() const {
        return m_project_name;
    }


    inline double expected_finish_timestamp() const {
        return m_expected_finish_timestamp;
    }


    inline double resin_remaining_ml() const {
        return m_resin_remaining_ml;
    }


    inline double resin_used_ml() const {
        return m_resin_used_ml;
    }


    inline double print_start_timestamp() const {
        return m_print_start_timestamp;
    }


    inline int position_nm() const {
        return m_position_nm;
    }


    inline int exposure_time_ms() const {
        return m_exposure_time_ms;
    }

    inline int exposure_time_first_ms() const {
        return m_exposure_time_first_ms;
    }


    inline bool resin_warn() const {
        return m_resin_warn;
    }


    inline int current_layer() const {
        return m_current_layer;
    }


    inline int exposure_time_calibrate_ms() const {
        return m_exposure_time_calibrate_ms;
    }


    inline int layer_height_first_nm() const {
        return m_layer_height_first_nm;
    }


    inline int layer_height_nm() const {
        return m_layer_height_nm;
    }


    inline int total_layers() const {
        return m_total_layers;
    }

    inline double wait_until_timestamp() const {
        return m_wait_until_timestamp;
    }


    inline double exposure_end() const {
        return m_exposure_end;
    }


    inline int remaining_wait_sec() const {
        return m_remaining_wait_sec;
    }


    inline QVariantMap exposure_exception() const {
        return m_exposure_exception;
    }


    inline QString project_file() const {
        return m_project_file;
    }

    inline double print_end_timestamp() const
    {
        return m_print_end_timestamp;
    }

    inline QString objectPath() const
    {
        return m_objectPath;
    }

    /// only tell the backend to set the value
    void softSetExposure_time_ms(int new_val);

    /// only tell the backend to set the value
    void softSetExposure_time_first_ms(int new_val);

    /// only tell the backend to set the value
    void softSetExposure_time_calibrate_ms(int new_val);

    Q_INVOKABLE void back(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());
    Q_INVOKABLE void cancel(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());
    Q_INVOKABLE void confirm_print_warning(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());
    Q_INVOKABLE void confirm_start(QJSValue callback_ok, QJSValue callback_fail);
    Q_INVOKABLE void confirm_resin_in(QJSValue callback_ok, QJSValue callback_fail);
    Q_INVOKABLE void cont(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());
    Q_INVOKABLE void feed_me(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());
    Q_INVOKABLE void reject_print_warning(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());
    Q_INVOKABLE void up_and_down(QJSValue callback_ok, QJSValue callback_fail);
    Q_INVOKABLE void stats_seen(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());
    Q_INVOKABLE void inject_fatal_error(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());
    Q_INVOKABLE void inject_exception(QString code, QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());



    double total_resin_required_ml() const
    {
        return m_total_resin_required_ml;
    }

    double total_resin_required_percent() const
    {
        return m_total_resin_required_percent;
    }

    double resin_measured_ml() const
    {
        return m_resin_measured_ml;
    }

    QVariantMap exposure_warning() const
    {
        return m_exposure_warning;
    }

    bool close_cover_warning() const
    {
        return m_close_cover_warning;
    }

    int calibration_regions() const
    {
        return m_calibration_regions;
    }

    ExposureUserProfile user_profile() const;
    void setUser_profile(ExposureUserProfile newUser_profile);
    void softSetUser_profile(ExposureUserProfile newUser_profile);

    int total_time_ms() const;
    void setTotal_time_ms(int newTotal_time_ms);

private:
    QVariantMap m_checks_state;
    ExposureState m_state;

    bool m_resin_low;
    double m_progress;
    int m_total_nm;
    int m_time_remain_ms;
    QString m_project_name;
    double m_expected_finish_timestamp;
    double m_resin_remaining_ml;
    double m_resin_used_ml;
    double m_print_start_timestamp;
    int m_position_nm;
    int m_exposure_time_ms;
    int m_exposure_time_first_ms;
    bool m_resin_warn;
    int m_current_layer;
    int m_exposure_time_calibrate_ms;
    int m_layer_height_first_nm;
    int m_layer_height_nm;
    int m_total_layers;
    double m_wait_until_timestamp;
    double m_exposure_end;
    int m_remaining_wait_sec;
    QVariantMap m_exposure_exception;
    QString m_project_file;
    double m_print_end_timestamp;
    QString m_objectPath;

    void setChecks_state(QVariantMap state);
    void setState(ExposureState state);

    void setResin_low(bool new_val);
    void setProgress(double new_val);
    void setTotal_nm(int new_val);
    void setTime_remain_ms(int new_val);
    void setProject_name(QString new_val);
    void setExpected_finish_timestamp(double new_val);
    void setResin_remaining_ml(double new_val);
    void setResin_used_ml(double new_val);
    void setPrint_start_timestamp(double new_val);
    void setPrint_end_timestamp(double print_end_timestamp);
    void setPosition_nm(int new_val);
    void setResin_warn(bool new_val);
    void setCurrent_layer(int new_val);
    void setLayer_height_first_nm(int new_val);
    void setLayer_height_nm(int new_val);
    void setTotal_layers(int new_val);
    void setWait_until_timestamp(double new_val);
    void setExposure_end(double new_val);
    void setRemaining_wait_sec(int new_val);
    void setExposure_exception(QVariantMap new_val);
    void setProject_file(QString new_val);

    void setTotal_resin_required_ml(double total_resin_required_ml);
    void setTotal_resin_required_percent(double total_resin_required_percent);
    void setResin_measured_ml(double resin_measured_ml);
    void setExposure_warning(QVariantMap exposure_warning);
    void setClose_cover_warning(bool close_cover_warning);
    void setCalibration_regions(int calibration_regions);

    void setExposure_time_ms(int new_val);
    void setExposure_time_first_ms(int new_val);
    void setExposure_time_calibrate_ms(int new_val);

    double m_total_resin_required_ml;

    double m_total_resin_required_percent;

    double m_resin_measured_ml;

    QVariantMap m_exposure_warning;

    bool m_close_cover_warning;

    int m_calibration_regions;

    ExposureUserProfile m_user_profile;

    int m_total_time_ms;

signals:
    void checks_stateChanged(QVariantMap state);
    void stateChanged(ExposureState state);
    void resin_lowChanged(bool resin_low);
    void progressChanged(double progress);
    void total_nmChanged(int total_nm);
    void time_remain_msChanged(int time_remain_ms);
    void project_nameChanged(QString project_name);
    void expected_finish_timestampChanged(double expected_finish_timestamp);
    void resin_remaining_mlChanged(double resin_remaining_ml);
    void resin_used_mlChanged(double resin_used_ml);
    void print_start_timestampChanged(double print_start_timestamp);
    void position_nmChanged(int position_nm);
    void exposure_time_msChanged(int exposure_time_ms);
    void exposure_time_first_msChanged(int exposure_time_first_ms);
    void resin_warnChanged(bool resin_warn);
    void current_layerChanged(int current_layer);
    void exposure_time_calibrate_msChanged(int exposure_time_calibrate_ms);
    void layer_height_first_nmChanged(int layer_height_first_nm);
    void layer_height_nmChanged(int layer_height_nm);
    void total_layersChanged(int total_layers);
    void wait_until_timestampChanged(double wait_until_timestamp);
    void exposure_endChanged(double exposure_end);
    void remaining_wait_secChanged(int remaining_wait_sec);
    void exposure_exceptionChanged(QVariantMap exposure_exception);
    void project_fileChanged(QString project_file);

    void print_end_timestampChanged(double print_end_timestamp);

    void total_resin_required_mlChanged(double total_resin_required_ml);

    void total_resin_required_percentChanged(double total_resin_required_percent);

    void resin_measured_mlChanged(double resin_measured_ml);

    void exposure_warningChanged(QVariantMap exposure_warning);

    void close_cover_warningChanged(bool close_cover_warning);

    void calibration_regionsChanged(int calibration_regions);

    void warning(const QVariantMap &value);

    void error(const QVariantMap &value);

    void user_profileChanged();

    void total_time_msChanged();

};

#endif // EXPOSURE_H
