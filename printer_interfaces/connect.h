#ifndef PRUSA_CONNECT_H
#define PRUSA_CONNECT_H

#include <QtCore/QObject>
#include <QtCore/QByteArray>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QVariant>
#include <QtDBus/QtDBus>
#include <QJSValue>
#include <QJSEngine>
#include "printer_types.h"
#include "connect_proxy.h"
#include "properties_proxy.h"
#include "errors_sl1.h"

/*
 * Proxy class for interface cz.prusa3d.connect0
 */
class PrusaConnect : public QObject
{
    Q_OBJECT
    ConnectProxy m_connectProxy;
    PropertiesProxy m_properties;
    QJSEngine &m_engine;

    bool m_propertiesLoaded;
    QTimer m_reloadTimer;

    QVariantMap m_last_exception;
    void setLast_exception(QVariantMap new_val);

    bool m_is_registered;
    void setIs_registered(bool new_val);

public:
    explicit PrusaConnect(QJSEngine &_engine, QObject *parent = nullptr);

    Q_INVOKABLE void reload_properties();

    bool set(QString name, QVariant value);

    Q_PROPERTY(bool is_registered READ is_registered NOTIFY is_registeredChanged)
    bool is_registered() const;

    Q_PROPERTY(QVariantMap last_exception READ last_exception NOTIFY last_exceptionChanged)
    QVariantMap last_exception() const;

    Q_INVOKABLE void register_printer(QJSValue callback_ok, QJSValue callback_fail);
    Q_INVOKABLE void confirm_registration(const QString &tmp_token, QJSValue callback_ok, QJSValue callback_fail);
    Q_INVOKABLE void reset_connect(QJSValue callback_ok, QJSValue callback_fail);

signals:

    void propertiesChanged(const QMap<QString, QVariant> &changed_properties);
    void last_exceptionChanged(QVariantMap last_exception);
    void is_registeredChanged(bool is_registered);
    void dbusError(ErrorsSL1::Errors);
public slots:

    void slotPropertiesChanged(const QString &interface, const QMap<QString, QVariant> &changed_properties, const QStringList &invalidates_properties);

};

#endif // PRUSA_CONNECT_H
