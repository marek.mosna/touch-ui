/*
    Copyright 2019-2020, Prusa Research s.r.o.
    Copyright 2021-2022, Prusa Research a.s.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "dbusutils.h"
#include "exposure.h"


Exposure::Exposure(QString path, QJSEngine &_engine, QObject *parent) :
    BaseDBusObject(_engine,  service, interface, path, QDBusConnection::systemBus(),  parent),
    m_exposureProxy(service, path, QDBusConnection::systemBus(), this),
    m_objectPath(path)
{
    registerPrinterTypes();
    m_exposureProxy.setTimeout(2000);

    // Make sure the model items change in accordance with the states
    connect(this, &Exposure::checks_stateChanged, &m_checksModel, &ChecksModel::change);
    connect(&m_exposureProxy, &ExposureProxy::exception, this, &Exposure::warning);
}

bool Exposure::set(const QString &name, const QVariant & value)
{
    if(name == "checks_state") {
        if(value.canConvert<MapIntInt>()) {
            MapIntInt orig = value.value<MapIntInt>();
            QVariantMap tmp;
            foreach(const int & key, orig.keys()) {
                tmp[QString::number(key)] = QVariant::fromValue(orig[key]);
            }
            setChecks_state(tmp);
        }
        else if(value.canConvert<QDBusArgument>()) {
            QDBusArgument dbusargument = value.value<QDBusArgument>();
            MapIntInt orig;
            dbusargument >> orig;
            QVariantMap tmp;
            foreach(const int & key, orig.keys()) {
                tmp[QString::number(key)] = QVariant::fromValue(orig[key]);
            }
            setChecks_state(tmp);
        }
        else {
            qWarning() << "WARN: Could not convert exposure0.checks_state: " << value;
        }
    }
    else if(name == "state") {
        setState(ExposureState(DBusUtils::convert<int>(value)));
    }

    else if(name == QStringLiteral("resin_low")) {
        setResin_low(DBusUtils::convert<bool>(value));
    }

    else if(name == QStringLiteral("progress")) {
        setProgress(DBusUtils::convert<double>(value));
    }

    else if(name == QStringLiteral("total_nm")) {
        setTotal_nm(DBusUtils::convert<int>(value));
    }

    else if(name == QStringLiteral("time_remain_ms")) {
        setTime_remain_ms(DBusUtils::convert<int>(value));
    }

    else if(name == QStringLiteral("project_name")) {
        setProject_name(DBusUtils::convert<QString>(value));
    }

    else if(name == QStringLiteral("expected_finish_timestamp")) {
        setExpected_finish_timestamp(DBusUtils::convert<double>(value));
    }

    else if(name == QStringLiteral("resin_remaining_ml")) {
        setResin_remaining_ml(DBusUtils::convert<double>(value));
    }

    else if(name == QStringLiteral("resin_used_ml")) {
        setResin_used_ml(DBusUtils::convert<double>(value));
    }

    else if(name == QStringLiteral("print_start_timestamp")) {
        setPrint_start_timestamp(DBusUtils::convert<double>(value));
    }

    else if(name == QStringLiteral("print_end_timestamp")) {
        setPrint_end_timestamp(DBusUtils::convert<double>(value));
    }

    else if(name == QStringLiteral("position_nm")) {
        setPosition_nm(DBusUtils::convert<int>(value));
    }

    else if(name == QStringLiteral("failure_reason")) {
        setExposure_exception(DBusUtils::convert<QVariantMap>(value));
    }

    else if(name == QStringLiteral("exposure_time_ms")) {
        setExposure_time_ms(DBusUtils::convert<int>(value));
    }

    else if(name == QStringLiteral("exposure_time_first_ms")) {
        setExposure_time_first_ms(DBusUtils::convert<int>(value));
    }

    else if(name == QStringLiteral("resin_warn")) {
        setResin_warn(DBusUtils::convert<bool>(value));
    }

    else if(name == QStringLiteral("current_layer")) {
        setCurrent_layer(DBusUtils::convert<int>(value));
    }

    else if(name == QStringLiteral("exposure_time_calibrate_ms")) {
        setExposure_time_calibrate_ms(DBusUtils::convert<int>(value));
    }

    else if(name == QStringLiteral("layer_height_first_nm")) {
        setLayer_height_first_nm(DBusUtils::convert<int>(value));
    }

    else if(name == QStringLiteral("layer_height_nm")) {
        setLayer_height_nm(DBusUtils::convert<int>(value));
    }

    else if(name == QStringLiteral("total_layers")) {
        setTotal_layers(DBusUtils::convert<int>(value));
    }

    else if(name == QStringLiteral("wait_until_timestamp")) {
        setWait_until_timestamp(DBusUtils::convert<double>(value));
    }

    else if(name == QStringLiteral("exposure_end")) {
        setExposure_end(DBusUtils::convert<double>(value));
    }

    else if(name == QStringLiteral("remaining_wait_sec")) {
        setRemaining_wait_sec(DBusUtils::convert<int>(value));
    }

    else if(name == QStringLiteral("exposure_exception")) {
        setExposure_exception(DBusUtils::convert<QVariantMap>(value));
    }

    else if(name == QStringLiteral("project_file")) {
        setProject_file(DBusUtils::convert<QString>(value));
    }

    else if(name == QStringLiteral("total_resin_required_ml")) {
        setTotal_resin_required_ml(DBusUtils::convert<double>(value));
    }

    else if(name == QStringLiteral("total_resin_required_percent")) {
        setTotal_resin_required_percent(DBusUtils::convert<double>(value));
    }

    else if(name == QStringLiteral("resin_measured_ml")) {
        setResin_measured_ml(DBusUtils::convert<double>(value));
    }

    else if(name == QStringLiteral("exposure_warning")) {
        QVariantMap warnings = DBusUtils::convert<QVariantMap>(value);
        setExposure_warning(warnings);
    }

    else if(name == QStringLiteral("close_cover_warning")) {
        setClose_cover_warning(DBusUtils::convert<bool>(value));
    }

    else if(name == QStringLiteral("calibration_regions")) {
        setCalibration_regions(DBusUtils::convert<int>(value));
    }

    else if(name == QStringLiteral("user_profile")) {
        setUser_profile(ExposureUserProfile(DBusUtils::convert<int>(value)));
    }

    else if(name == QStringLiteral("total_time_ms")) {
        setTotal_time_ms(DBusUtils::convert<int>(value));
    }

    else {
        qWarning() << "Exposure: attempt to set unknown property " << name << ":"  << value;
        return BaseDBusObject::set(name, value);
    }
    return true;
}

void Exposure::setChecks_state(QVariantMap state) {
    // don't compare map
    m_checks_state = state;
    emit checks_stateChanged(m_checks_state);
}

void Exposure::setCalibration_regions(int calibration_regions)
{
    if (m_calibration_regions == calibration_regions)
        return;

    m_calibration_regions = calibration_regions;
    emit calibration_regionsChanged(m_calibration_regions);
}

void Exposure::setClose_cover_warning(bool close_cover_warning)
{
    if (m_close_cover_warning == close_cover_warning)
        return;

    m_close_cover_warning = close_cover_warning;
    emit close_cover_warningChanged(m_close_cover_warning);
}

void Exposure::setTotal_resin_required_ml(double total_resin_required_ml)
{
    if (qFuzzyCompare(m_total_resin_required_ml, total_resin_required_ml))
        return;

    m_total_resin_required_ml = total_resin_required_ml;
    emit total_resin_required_mlChanged(m_total_resin_required_ml);
}

void Exposure::setTotal_resin_required_percent(double total_resin_required_percent)
{
    if (qFuzzyCompare(m_total_resin_required_percent, total_resin_required_percent))
        return;

    m_total_resin_required_percent = total_resin_required_percent;
    emit total_resin_required_percentChanged(m_total_resin_required_percent);
}

void Exposure::setResin_measured_ml(double resin_measured_ml)
{
    if (qFuzzyCompare(m_resin_measured_ml, resin_measured_ml))
        return;

    m_resin_measured_ml = resin_measured_ml;
    emit resin_measured_mlChanged(m_resin_measured_ml);
}

void Exposure::setExposure_warning(QVariantMap exposure_warning)
{
    if (m_exposure_warning == exposure_warning)
        return;

    m_exposure_warning = exposure_warning;
    emit exposure_warningChanged(m_exposure_warning);
}

void Exposure::setPrint_end_timestamp(double print_end_timestamp)
{
    if (qFuzzyCompare(m_print_end_timestamp, print_end_timestamp))
        return;

    m_print_end_timestamp = print_end_timestamp;
    emit print_end_timestampChanged(m_print_end_timestamp);
}

QVariantMap Exposure::checks_state() {
    return m_checks_state;
}

void Exposure::setExposure_time_ms(int new_val)
{
    if(m_exposure_time_ms != new_val) {
        m_exposure_time_ms = new_val;
        emit exposure_time_msChanged(new_val);
    }
}

void Exposure::setExposure_time_first_ms(int new_val)
{
    if(m_exposure_time_first_ms != new_val) {
        m_exposure_time_first_ms = new_val;
        emit exposure_time_first_msChanged(new_val);
    }
}

void Exposure::setExposure_time_calibrate_ms(int new_val)
{
    if(m_exposure_time_calibrate_ms != new_val) {
        m_exposure_time_calibrate_ms = new_val;
        emit exposure_time_calibrate_msChanged(new_val);
    }
}

void Exposure::softSetExposure_time_ms(int new_val)
{
    m_properties.Set(m_exposureProxy.interface(), "exposure_time_ms", QDBusVariant(new_val));
    //m_exposureProxy.setExposure_time_ms(new_val);
}

void Exposure::softSetExposure_time_first_ms(int new_val)
{
    m_properties.Set(m_exposureProxy.interface(), "exposure_time_first_ms", QDBusVariant(new_val));
    //m_exposureProxy.setExposure_time_first_ms(new_val);
}

void Exposure::softSetExposure_time_calibrate_ms(int new_val)
{
    m_properties.Set(m_exposureProxy.interface(), "exposure_time_calibrate_ms", QDBusVariant(new_val));
    //m_exposureProxy.setExposure_time_calibrate_ms(new_val);
}

void Exposure::back(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(m_exposureProxy.back(), callback_ok, callback_fail);
}

void Exposure::cancel(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(m_exposureProxy.cancel(), callback_ok, callback_fail);
}

void Exposure::confirm_print_warning(QJSValue callback_ok, QJSValue callback_fail)
{
    handleCallbackWithError(m_exposureProxy.confirm_print_warning(), callback_ok, callback_fail);
}

void Exposure::confirm_start(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(m_exposureProxy.confirm_start(), callback_ok, callback_fail);
}

void Exposure::confirm_resin_in(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(m_exposureProxy.confirm_resin_in(), callback_ok, callback_fail);
}

void Exposure::cont(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(m_exposureProxy.cont(), callback_ok, callback_fail);
}

void Exposure::feed_me(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(m_exposureProxy.feed_me(), callback_ok, callback_fail);
}

void Exposure::reject_print_warning(QJSValue callback_ok, QJSValue callback_fail)
{
    handleCallbackWithError(m_exposureProxy.reject_print_warning(), callback_ok, callback_fail);
}

void Exposure::up_and_down(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(m_exposureProxy.up_and_down(), callback_ok, callback_fail);
}

void Exposure::stats_seen(QJSValue callback_ok, QJSValue callback_fail)
{
    handleCallbackWithError(m_exposureProxy.stats_seen(), callback_ok, callback_fail);
}

void Exposure::inject_fatal_error(QJSValue callback_ok, QJSValue callback_fail)
{
    handleCallbackWithError(m_exposureProxy.inject_fatal_error(), callback_ok, callback_fail);
}

void Exposure::inject_exception(QString code, QJSValue callback_ok, QJSValue callback_fail)
{
    handleCallbackWithError(m_exposureProxy.inject_exception(code), callback_ok, callback_fail);
}

void Exposure::setState(Exposure::ExposureState state) {
    if(m_state != state) {
        m_state = state;
        emit stateChanged(m_state);
    }
}



void Exposure::setResin_low(bool new_val)  {
    if(new_val != m_resin_low) {
        m_resin_low = new_val;
            emit resin_lowChanged(new_val);
    }
}

void Exposure::setProgress(double new_val)  {
    if(! qFuzzyCompare(new_val, m_progress)) {
            m_progress = new_val;
            emit progressChanged(new_val);
    }
}

void Exposure::setTotal_nm(int new_val)  {
    if(new_val != m_total_nm) {
            m_total_nm = new_val;
            emit total_nmChanged(new_val);
    }
}

void Exposure::setTime_remain_ms(int new_val)  {
    if(new_val != m_time_remain_ms) {
            m_time_remain_ms = new_val;
            emit time_remain_msChanged(new_val);
    }
}

void Exposure::setProject_name(QString new_val)  {
    if(new_val != m_project_name) {
            m_project_name = new_val;
            emit project_nameChanged(new_val);
    }
}

void Exposure::setExpected_finish_timestamp(double new_val)  {
    if(! qFuzzyCompare(new_val, m_expected_finish_timestamp)) {
            m_expected_finish_timestamp = new_val;
            emit expected_finish_timestampChanged(new_val);
    }
}

void Exposure::setResin_remaining_ml(double new_val)  {
    if(! qFuzzyCompare(new_val, m_resin_remaining_ml)) {
            m_resin_remaining_ml = new_val;
            emit resin_remaining_mlChanged(new_val);
    }
}

void Exposure::setResin_used_ml(double new_val)  {
    if(! qFuzzyCompare(new_val, m_resin_used_ml)) {
            m_resin_used_ml = new_val;
            emit resin_used_mlChanged(new_val);
    }
}

void Exposure::setPrint_start_timestamp(double new_val)  {
    if(! qFuzzyCompare(new_val, m_print_start_timestamp)) {
            m_print_start_timestamp = new_val;
            emit print_start_timestampChanged(new_val);
    }
}

void Exposure::setPosition_nm(int new_val)  {
    if(new_val != m_position_nm) {
            m_position_nm = new_val;
            emit position_nmChanged(new_val);
    }
}

void Exposure::setResin_warn(bool new_val)  {
    if(new_val != m_resin_warn) {
            m_resin_warn = new_val;
            emit resin_warnChanged(new_val);
    }
}

void Exposure::setCurrent_layer(int new_val)  {
    if(new_val != m_current_layer) {
            m_current_layer = new_val;
            emit current_layerChanged(new_val);
    }
}

void Exposure::setLayer_height_first_nm(int new_val)  {
    if(new_val != m_layer_height_first_nm) {
            m_layer_height_first_nm = new_val;
            emit layer_height_first_nmChanged(new_val);
    }
}

void Exposure::setLayer_height_nm(int new_val)  {
    if(new_val != m_layer_height_nm) {
            m_layer_height_nm = new_val;
            emit layer_height_nmChanged(new_val);
    }
}

void Exposure::setTotal_layers(int new_val)  {
    if(new_val != m_total_layers) {
            m_total_layers = new_val;
            emit total_layersChanged(new_val);
    }
}

void Exposure::setWait_until_timestamp(double new_val)  {
    if(! qFuzzyCompare(new_val, m_wait_until_timestamp)) {
            m_wait_until_timestamp = new_val;
            emit wait_until_timestampChanged(new_val);
    }
}

void Exposure::setExposure_end(double new_val)  {
    if(! qFuzzyCompare(new_val, m_exposure_end)) {
            m_exposure_end = new_val;
            emit exposure_endChanged(new_val);
    }
}

void Exposure::setRemaining_wait_sec(int new_val)  {
    if(new_val != m_remaining_wait_sec) {
            m_remaining_wait_sec = new_val;
            emit remaining_wait_secChanged(new_val);
    }
}

void Exposure::setExposure_exception(QVariantMap new_val)  {
    if(new_val != m_exposure_exception) {
            m_exposure_exception = new_val;
            emit exposure_exceptionChanged(new_val);
    }
}

void Exposure::setProject_file(QString new_val)  {
    if(new_val != m_project_file) {
            m_project_file = new_val;
            emit project_fileChanged(new_val);
    }
}


Exposure::ExposureUserProfile Exposure::user_profile() const
{
    return m_user_profile;
}

void Exposure::setUser_profile(Exposure::ExposureUserProfile newUser_profile)
{
    if (m_user_profile == newUser_profile)
        return;
    m_user_profile = newUser_profile;
    emit user_profileChanged();
}

void Exposure::softSetUser_profile(ExposureUserProfile newUser_profile)
{
    m_properties.Set(m_exposureProxy.interface(), "user_profile", QDBusVariant((int)newUser_profile));
}

int Exposure::total_time_ms() const
{
    return m_total_time_ms;
}

void Exposure::setTotal_time_ms(int newTotal_time_ms)
{
    if (m_total_time_ms == newTotal_time_ms)
        return;
    m_total_time_ms = newTotal_time_ms;
    emit total_time_msChanged();
}
