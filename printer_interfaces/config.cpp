/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QMetaObject>
#include <QMetaProperty>
#include <QHash>
#include "config.h"


int Config::forceSlowTiltHeight() const
{
    return m_forceSlowTiltHeight;
}

void Config::setForceSlowTiltHeight(int newForceSlowTiltHeight)
{
    if (m_forceSlowTiltHeight == newForceSlowTiltHeight)
        return;
    m_forceSlowTiltHeight = newForceSlowTiltHeight;
    emit forceSlowTiltHeightChanged();
}

int Config::uvCalibBoostTolerance() const
{
    return m_uvCalibBoostTolerance;
}

void Config::setUvCalibBoostTolerance(int newUvCalibBoostTolerance)
{
    if (m_uvCalibBoostTolerance == newUvCalibBoostTolerance)
        return;
    m_uvCalibBoostTolerance = newUvCalibBoostTolerance;
    emit uvCalibBoostToleranceChanged();
}

int Config::rpmControlUvFanMinRpm() const
{
    return m_rpmControlUvFanMinRpm;
}

void Config::setRpmControlUvFanMinRpm(int newRpmControlUvFanMinRpm)
{
    if (m_rpmControlUvFanMinRpm == newRpmControlUvFanMinRpm)
        return;
    m_rpmControlUvFanMinRpm = newRpmControlUvFanMinRpm;
    emit rpmControlUvFanMinRpmChanged();
}

int Config::vatRevision() const
{
    return m_vatRevision;
}

void Config::setVatRevision(int newVatRevision)
{
    if (m_vatRevision == newVatRevision)
        return;
    m_vatRevision = newVatRevision;
    emit vatRevisionChanged();
}

bool Config::rpmControlOverride() const
{
    return m_rpmControlOverride;
}

void Config::setRpmControlOverride(bool newRpmControlOverride)
{
    if (m_rpmControlOverride == newRpmControlOverride)
        return;
    m_rpmControlOverride = newRpmControlOverride;
    emit rpmControlOverrideChanged();
}

int Config::uvPwmPrint() const
{
    return m_uvPwmPrint;
}

void Config::setUvPwmPrint(int newUvPwmPrint)
{
    if (m_uvPwmPrint == newUvPwmPrint)
        return;
    m_uvPwmPrint = newUvPwmPrint;
    emit uvPwmPrintChanged();
}

int Config::rpmControlUvFanMaxRpm() const
{
    return m_rpmControlUvFanMaxRpm;
}

void Config::setRpmControlUvFanMaxRpm(int newRpmControlUvFanMaxRpm)
{
    if (m_rpmControlUvFanMaxRpm == newRpmControlUvFanMaxRpm)
        return;
    m_rpmControlUvFanMaxRpm = newRpmControlUvFanMaxRpm;
    emit rpmControlUvFanMaxRpmChanged();
}

int Config::uvPwmTune() const
{
    return m_uvPwmTune;
}

void Config::setUvPwmTune(int newUvPwmTune)
{
    if (m_uvPwmTune == newUvPwmTune)
        return;
    m_uvPwmTune = newUvPwmTune;
    emit uvPwmTuneChanged();
}

int Config::tiltMin() const
{
    return m_tiltMin;
}

void Config::setTiltMin(int newTiltMin)
{
    if (m_tiltMin == newTiltMin)
        return;
    m_tiltMin = newTiltMin;
    emit tiltMinChanged();
}

int Config::tiltMax() const
{
    return m_tiltMax;
}

void Config::setTiltMax(int newTiltMax)
{
    if (m_tiltMax == newTiltMax)
        return;
    m_tiltMax = newTiltMax;
    emit tiltMaxChanged();
}

const QString &Config::currentProfilesSet() const
{
    return m_currentProfilesSet;
}

void Config::setCurrentProfilesSet(const QString &newCurrentProfilesSet)
{
    if (m_currentProfilesSet == newCurrentProfilesSet)
        return;
    m_currentProfilesSet = newCurrentProfilesSet;
    emit currentProfilesSetChanged();
}

bool Config::lockProfiles() const
{
    return m_lockProfiles;
}

void Config::setLockProfiles(bool newLockProfiles)
{
    if (m_lockProfiles == newLockProfiles)
        return;
    m_lockProfiles = newLockProfiles;
    emit lockProfilesChanged();
}

int Config::rpmControlUvLedMaxTemp() const
{
    return m_rpmControlUvLedMaxTemp;
}

void Config::setRpmControlUvLedMaxTemp(int newRpmControlUvLedMaxTemp)
{
    if (m_rpmControlUvLedMaxTemp == newRpmControlUvLedMaxTemp)
        return;
    m_rpmControlUvLedMaxTemp = newRpmControlUvLedMaxTemp;
    emit rpmControlUvLedMaxTempChanged();
}

int Config::rpmControlUvLedMinTemp() const
{
    return m_rpmControlUvLedMinTemp;
}

void Config::setRpmControlUvLedMinTemp(int newRpmControlUvLedMinTemp)
{
    if (m_rpmControlUvLedMinTemp == newRpmControlUvLedMinTemp)
        return;
    m_rpmControlUvLedMinTemp = newRpmControlUvLedMinTemp;
    emit rpmControlUvLedMinTempChanged();
}


Config::Config(QJSEngine &engine, QObject *parent) :
    BaseDBusObject(engine, service, interface, path, QDBusConnection::systemBus(),  parent),
    m_configProxy(QStringLiteral("cz.prusa3d.sl1.config0"), QStringLiteral("/cz/prusa3d/sl1/config0"), QDBusConnection::systemBus(), this)
{
    m_configProxy.setTimeout(2000);
}

void Config::debugPrint() const
{
    qDebug() << "PrinterConfig {";
    for(int i = this->metaObject()->propertyOffset(); i < this->metaObject()->propertyCount(); ++i)
    {
        auto prop = this->metaObject()->property(i);
        qDebug() << prop.name() << ":" << prop.read(this);
    }
    qDebug() << "}";
}

bool Config::set(const QString &name, const QVariant &value) {
    if(name == QStringLiteral("fanCheck")) {
        m_fanCheck = value.value<bool>();
        emit fanCheckChanged(m_fanCheck);
    }
    else if(name == QStringLiteral("coverCheck")) {
        m_coverCheck = value.value<bool>();
        emit coverCheckChanged(m_coverCheck);
    }
    else if(name == QStringLiteral("MCversionCheck")) {
        m_mCversionCheck = value.value<bool>();
        emit mCversionCheckChanged(m_mCversionCheck);
    }
    else if(name == QStringLiteral("resinSensor")) {
        m_resinSensor = value.value<bool>();
        emit resinSensorChanged(m_resinSensor);
    }
    else if(name == QStringLiteral("autoOff")) {
        m_autoOff = value.value<bool>();
        emit autoOffChanged(m_autoOff);
    }
    else if(name == QStringLiteral("mute")) {
        m_mute = value.value<bool>();
        emit muteChanged(m_mute);
    }
    else if(name == QStringLiteral("screwMm")) {
        m_screwMm = value.value<qint32>();
        emit screwMmChanged(m_screwMm);
    }
    else if(name == QStringLiteral("microStepsMM")) {
        m_microStepsMM = value.value<double>();
        emit microStepsMMChanged(m_microStepsMM);
    }
    else if(name == QStringLiteral("tower_microstep_size_nm")) {
        m_tower_microstep_size_nm = value.value<qint32>();
        emit tower_microstep_size_nmChanged(m_tower_microstep_size_nm);
    }
    else if(name == QStringLiteral("tiltHeight")) {
        m_tiltHeight = value.value<qint32>();
        emit tiltHeightChanged(m_tiltHeight);
    }
    else if(name == QStringLiteral("stirringMoves")) {
        m_stirringMoves = value.value<qint32>();
        emit stirringMovesChanged(m_stirringMoves);
    }
    else if(name == QStringLiteral("stirringDelay")) {
        m_stirringDelay = value.value<qint32>();
        emit stirringDelayChanged(m_stirringDelay);
    }
    else if(name == QStringLiteral("measuringMoves")) {
        m_measuringMoves = value.value<qint32>();
        emit measuringMovesChanged(m_measuringMoves);
    }
    else if(name == QStringLiteral("pwrLedPwm")) {
        m_pwrLedPwm = value.value<qint32>();
        emit pwrLedPwmChanged(m_pwrLedPwm);
    }
    else if(name == QStringLiteral("MCBoardVersion")) {
        m_MCBoardVersion = value.value<qint32>();
        emit mCBoardVersionChanged(m_MCBoardVersion);
    }
    else if(name == QStringLiteral("tiltSensitivity")) {
        m_tiltSensitivity = value.value<qint32>();
        emit tiltSensitivityChanged(m_tiltSensitivity);
    }
    else if(name == QStringLiteral("towerSensitivity")) {
        m_towerSensitivity = value.value<qint32>();
        emit towerSensitivityChanged(m_towerSensitivity);
    }
    else if(name == QStringLiteral("limit4fast")) {
        m_limit4fast = value.value<qint32>();
        emit limit4fastChanged(m_limit4fast);
    }
    else if(name == QStringLiteral("calibTowerOffset")) {
        m_calibTowerOffset = value.value<qint32>();
        emit calibTowerOffsetChanged(m_calibTowerOffset);
    }
    else if(name == QStringLiteral("perPartes")) {
        m_perPartes = value.value<bool>();
        emit perPartesChanged(m_perPartes);
    }
    else if(name == QStringLiteral("tilt")) {
        m_tilt = value.value<bool>();
        emit tiltChanged(m_tilt);
    }
    else if(name == QStringLiteral("upAndDownUvOn")) {
        m_upAndDownUvOn = value.value<bool>();
        emit upAndDownUvOnChanged(m_upAndDownUvOn);
    }
    else if(name == QStringLiteral("trigger")) {
        m_trigger = value.value<qint32>();
        emit triggerChanged(m_trigger);
    }
    else if(name == QStringLiteral("layerTowerHop")) {
        m_layerTowerHop = value.value<qint32>();
        emit layerTowerHopChanged(m_layerTowerHop);
    }
    else if(name == QStringLiteral("delayBeforeExposure")) {
        m_delayBeforeExposure = value.value<qint32>();
        emit delayBeforeExposureChanged(m_delayBeforeExposure);
    }
    else if(name == QStringLiteral("delayAfterExposure")) {
        m_delayAfterExposure = value.value<qint32>();
        emit delayAfterExposureChanged(m_delayAfterExposure);
    }
    else if(name == QStringLiteral("upAndDownWait")) {
        m_upAndDownWait = value.value<qint32>();
        emit upAndDownWaitChanged(m_upAndDownWait);
    }
    else if(name == QStringLiteral("upAndDownEveryLayer")) {
        m_upAndDownEveryLayer = value.value<qint32>();
        emit upAndDownEveryLayerChanged(m_upAndDownEveryLayer);
    }
    else if(name == QStringLiteral("upAndDownZoffset")) {
        m_upAndDownZoffset = value.value<qint32>();
        emit upAndDownZoffsetChanged(m_upAndDownZoffset);
    }
    else if(name == QStringLiteral("upAndDownExpoComp")) {
        m_upAndDownExpoComp = value.value<qint32>();
        emit upAndDownExpoCompChanged(m_upAndDownExpoComp);
    }
    else if(name == QStringLiteral("fan1Rpm")) {
        m_fanUvLedRpm = value.value<qint32>();
        emit fanUvLedRpmChanged(m_fanUvLedRpm);
    }
    else if(name == QStringLiteral("fan2Rpm")) {
        m_fan2Rpm = value.value<qint32>();
        emit fanBlowerRpmChanged(m_fan2Rpm);
    }
    else if(name == QStringLiteral("fan3Rpm")) {
        m_fan3Rpm = value.value<qint32>();
        emit fanRearRpmChanged(m_fan3Rpm);
    }
    else if(name == QStringLiteral("fan1Enabled")) {
        m_fanUvLedEnabled = value.value<bool>();
        emit fanUvLedEnabledChanged(m_fanUvLedEnabled);
    }
    else if(name == QStringLiteral("fan2Enabled")) {
        m_fan2Enabled = value.value<bool>();
        emit fanBlowerEnabledChanged(m_fan2Enabled);
    }
    else if(name == QStringLiteral("fan3Enabled")) {
        m_fan3Enabled = value.value<bool>();
        emit fanRearEnabledChanged(m_fan3Enabled);
    }
    else if(name == QStringLiteral("uvCurrent")) {
        m_uvCurrent = value.value<double>();
        emit uvCurrentChanged(m_uvCurrent);
    }
    else if(name == QStringLiteral("uvPwm")) {
        m_uvPwm = value.value<qint32>();
        emit uvPwmChanged(m_uvPwm);
    }
    else if(name == QStringLiteral("uvWarmUpTime")) {
        m_uvWarmUpTime = value.value<qint32>();
        emit uvWarmUpTimeChanged(m_uvWarmUpTime);
    }
    else if(name == QStringLiteral("uvCalibIntensity")) {
        m_uvCalibIntensity = value.value<qint32>();
        emit uvCalibIntensityChanged(m_uvCalibIntensity);
    }
    else if(name == QStringLiteral("uvCalibMinIntEdge")) {
        m_uvCalibMinIntEdge = value.value<qint32>();
        emit uvCalibMinIntEdgeChanged(m_uvCalibMinIntEdge);
    }
    else if(name == QStringLiteral("tuneTilt")) {
        m_tuneTilt = value.value<Int2D>();
        emit tuneTiltChanged(m_tuneTilt);
    }
    else if(name == QStringLiteral("calibrated")) {
        m_calibrated = value.value<bool>();
        emit calibratedChanged(m_calibrated);
    }
    else if(name == QStringLiteral("towerHeight")) {
        m_towerHeight = value.value<qint32>();
        emit towerHeightChanged(m_towerHeight);
    }
    else if(name == QStringLiteral("tiltFastTime")) {
        m_tiltFastTime = value.value<double>();
        emit tiltFastTimeChanged(m_tiltFastTime);
    }
    else if(name == QStringLiteral("tiltSlowTime")) {
        m_tiltSlowTime = value.value<double>();
        emit tiltSlowTimeChanged(m_tiltSlowTime);
    }
    else if(name == QStringLiteral("showWizard")) {
        m_showWizard = value.value<bool>();
        emit showWizardChanged(m_showWizard);
    }
    else if(name == QStringLiteral("showUnboxing")) {
        m_showUnboxing = value.value<bool>();
        emit showUnboxingChanged(m_showUnboxing);
    }
    else if(name == QStringLiteral("showI18nSelect")) {
        m_showI18nSelect = value.value<bool>();
        emit showI18nSelectChanged(m_showI18nSelect);
    }
    else if(name == QStringLiteral("tankCleaningExposureTime")) {
        m_tankCleaningExposureTime = value.value<int>();
        emit tankCleaningExposureTimeChanged();
    }
    else if(name == QStringLiteral("tiltHighViscosityTime")) {
        m_tiltHighViscosityTime = value.value<double>();
        emit tiltHighViscosityTimeChanged();
    }
    else if(name == QStringLiteral("constraints")) {
        m_constraints = DBusUtils::convert<QVariantMap>(value);
        emit constraintsChanged();
    }
    else if(name == QStringLiteral("layer_tower_hop_nm")) {
        m_layer_tower_hop_nm = value.value<int>();
        emit layer_tower_hop_nmChanged();
    }
    else if(name == QStringLiteral("tower_height_nm")) {
        m_tower_height_nm = value.value<int>();
        emit tower_height_nmChanged();
    }
    else if(name == QStringLiteral("up_and_down_z_offset_nm")) {
        m_up_and_down_z_offset_nm = value.value<int>();
        emit up_and_down_z_offset_nmChanged();
    }
    else if(name == QStringLiteral("tankCleaningGentlyUpProfile")) {
        m_tankCleaningGentlyUpProfile = value.value<int>();
        emit tankCleaningGentlyUpProfileChanged();
    }
    else if(name == QStringLiteral("tankCleaningMinDistance_nm")) {
        m_tankCleaningMinDistance_nm = value.value<int>();
        emit tankCleaningMinDistance_nmChanged();
    }
    else if(name == QStringLiteral("max_tower_height_mm")) {
        m_max_tower_height_mm = value.value<int>();
        emit max_tower_height_mmChanged();
    }
    else if(name == QStringLiteral("calib_tower_offset_nm")) {
        m_calib_tower_offset_nm = value.value<int>();
        emit calib_tower_offset_nmChanged();
    }

    else return BaseDBusObject::set(name, value);
    return true;
}

void Config::setMCBoardVersion(int new_val)  {
    if(new_val != m_MCBoardVersion) {
        m_configProxy.setMCBoardVersion(new_val);
        m_MCBoardVersion = m_configProxy.mCBoardVersion();
        emit mCBoardVersionChanged(m_MCBoardVersion);

    }
}

void Config::setMCversionCheck(bool new_val)  {
    if(new_val != m_mCversionCheck) {
        m_configProxy.setMCversionCheck(new_val);
        m_mCversionCheck = m_configProxy.mCversionCheck();
        emit mCversionCheckChanged(m_mCversionCheck);
    }
}

void Config::setAutoOff(bool new_val)  {
    if(new_val != m_autoOff) {
        m_configProxy.setAutoOff(new_val);
        m_autoOff = m_configProxy.autoOff();
        emit autoOffChanged(m_autoOff);
    }
}

void Config::setCalibTowerOffset(int new_val)  {
    if(new_val != m_calibTowerOffset) {
        m_configProxy.setCalibTowerOffset(new_val);
        m_calibTowerOffset = m_configProxy.calibTowerOffset();
        emit calibTowerOffsetChanged(m_calibTowerOffset);

    }
}

void Config::setCalibrated(bool new_val)  {
    if(new_val != m_calibrated) {
        m_configProxy.setCalibrated(new_val);
        m_calibrated = m_configProxy.calibrated();
        emit calibratedChanged(m_calibrated);
    }
}

void Config::setCoverCheck(bool new_val)  {
    if(new_val != m_coverCheck) {
        m_coverCheck = new_val;
        m_configProxy.setCoverCheck(new_val);
        m_coverCheck = m_configProxy.coverCheck();
        emit coverCheckChanged(m_coverCheck);
    }
}

void Config::setDelayAfterExposure(int new_val)  {
    if(new_val != m_delayAfterExposure) {
        m_configProxy.setDelayAfterExposure(new_val);
        m_delayAfterExposure = m_configProxy.delayAfterExposure();
        emit delayAfterExposureChanged(m_delayAfterExposure);
    }
}

void Config::setDelayBeforeExposure(int new_val)  {
    if(new_val != m_delayBeforeExposure) {
        m_configProxy.setDelayBeforeExposure(new_val);
        m_delayBeforeExposure = m_configProxy.delayBeforeExposure();
        emit delayBeforeExposureChanged(m_delayBeforeExposure);

    }
}

void Config::setFanUvLedEnabled(bool new_val)  {
    if(new_val != m_fanUvLedEnabled) {
        m_configProxy.setFan1Enabled(new_val);
        m_fanUvLedEnabled = m_configProxy.fan1Enabled();
        emit fanUvLedEnabledChanged(m_fanUvLedEnabled);
    }
}

void Config::setFanUvLedRpm(int new_val)  {
    if(new_val != m_fanUvLedRpm) {
        m_configProxy.setFan1Rpm(new_val);
        m_fanUvLedRpm = m_configProxy.fan1Rpm();
        emit fanUvLedRpmChanged(m_fanUvLedRpm);
    }
}

void Config::setFanBlowerEnabled(bool new_val)  {
    if(new_val != m_fan2Enabled) {
        m_configProxy.setFan2Enabled(new_val);
        m_fan2Enabled = m_configProxy.fan2Enabled();
        emit fanBlowerEnabledChanged(m_fan2Enabled);
    }
}

void Config::setFanBlowerRpm(int new_val)  {
    if(new_val != m_fan2Rpm) {
        m_configProxy.setFan2Rpm(new_val);
        m_fan2Rpm = m_configProxy.fan2Rpm();
        emit fanBlowerRpmChanged(m_fan2Rpm);
    }
}

void Config::setFanRearEnabled(bool new_val)  {
    if(new_val != m_fan3Enabled) {
        m_configProxy.setFan3Enabled(new_val);
        m_fan3Enabled = m_configProxy.fan3Enabled();
        emit fanRearEnabledChanged(m_fan3Enabled);
    }
}

void Config::setFanRearRpm(int new_val)  {
    if(new_val != m_fan3Rpm) {
        m_configProxy.setFan3Rpm(new_val);
        m_fan3Rpm = m_configProxy.fan3Rpm();
        emit fanRearRpmChanged(m_fan3Rpm);
    }
}

void Config::setFanCheck(bool new_val)  {
    if(new_val != m_fanCheck) {
        m_configProxy.setFanCheck(new_val);
        m_fanCheck = m_configProxy.fanCheck();
        emit fanCheckChanged(m_fanCheck);
    }
}

void Config::setLayerTowerHop(int new_val)  {
    if(new_val != m_layerTowerHop) {
        m_configProxy.setLayerTowerHop(new_val);
        m_layerTowerHop = m_configProxy.layerTowerHop();
        emit layerTowerHopChanged(m_layerTowerHop);

    }
}

void Config::setLimit4fast(int new_val)  {
    if(new_val != m_limit4fast) {
        m_limit4fast = new_val;
        m_configProxy.setLimit4fast(new_val);
        emit limit4fastChanged(m_limit4fast);
    }
}

void Config::setMeasuringMoves(int new_val)  {
    if(new_val != m_measuringMoves) {
        m_configProxy.setMeasuringMoves(new_val);
        m_measuringMoves = m_configProxy.measuringMoves();
        emit measuringMovesChanged(m_measuringMoves);
    }
}

void Config::setMicroStepsMM(double new_val)  {
    if(! qFuzzyCompare(new_val, m_microStepsMM)) {
        m_microStepsMM = m_configProxy.microStepsMM();
        emit microStepsMMChanged(m_microStepsMM);

    }
}

void Config::setMute(bool new_val)  {
    if(new_val != m_mute) {
        m_configProxy.setMute(new_val);
        m_mute = m_configProxy.mute();
        emit muteChanged(m_mute);
    }
}

void Config::setPerPartes(bool new_val)  {
    if(new_val != m_perPartes) {
        m_configProxy.setPerPartes(new_val);
        m_perPartes = m_configProxy.perPartes();
        emit perPartesChanged(m_perPartes);
    }
}

void Config::setPwrLedPwm(int new_val)  {
    if(new_val != m_pwrLedPwm) {
        m_configProxy.setPwrLedPwm(new_val);
        m_pwrLedPwm = m_configProxy.pwrLedPwm();
        emit pwrLedPwmChanged(m_pwrLedPwm);
    }
}

void Config::setResinSensor(bool new_val)  {
    if(new_val != m_resinSensor) {
        m_configProxy.setResinSensor(new_val);
        m_resinSensor = m_configProxy.resinSensor();
        emit resinSensorChanged(m_resinSensor);
    }
}

void Config::setScrewMm(int new_val)  {
    if(new_val != m_screwMm) {
        m_configProxy.setScrewMm(new_val);
        m_screwMm = m_configProxy.screwMm();
        emit screwMmChanged(m_screwMm);
    }
}

void Config::setShowI18nSelect(bool new_val)  {
    if(new_val != m_showI18nSelect) {
        m_configProxy.setShowI18nSelect(new_val);
        m_showI18nSelect = m_configProxy.showI18nSelect();
        emit showI18nSelectChanged(m_showI18nSelect);
    }
}

void Config::setShowUnboxing(bool new_val)  {
    if(new_val != m_showUnboxing) {
        m_configProxy.setShowUnboxing(new_val);
        m_showUnboxing = m_configProxy.showUnboxing();
        emit showUnboxingChanged(m_showUnboxing);
    }
}

void Config::setShowWizard(bool new_val)  {
    if(new_val != m_showWizard) {
        m_configProxy.setShowWizard(new_val);
        m_showWizard = m_configProxy.showWizard();
        emit showWizardChanged(m_showWizard);
    }
}

void Config::setStirringDelay(int new_val)  {
    if(new_val != m_stirringDelay) {
        m_configProxy.setStirringDelay(new_val);
        m_stirringDelay = m_configProxy.stirringDelay();
        emit stirringDelayChanged(m_stirringDelay);
    }
}

void Config::setStirringMoves(int new_val)  {
    if(new_val != m_stirringMoves) {
        m_configProxy.setStirringMoves(new_val);
        m_stirringMoves = m_configProxy.stirringMoves();
        emit stirringMovesChanged(m_stirringMoves);
    }
}

void Config::setTilt(bool new_val)  {
    if(new_val != m_tilt) {
        m_configProxy.setTilt(new_val);
        m_tilt = m_configProxy.tilt();
        emit tiltChanged(m_tilt);
    }
}

void Config::setTiltHeight(int new_val)  {
    if(new_val != m_tiltHeight) {
        m_configProxy.setTiltHeight(new_val);
        m_tiltHeight = m_configProxy.tiltHeight();
        emit tiltHeightChanged(m_tiltHeight);
    }
}

void Config::setTiltSensitivity(int new_val)  {
    if(new_val != m_tiltSensitivity) {
        m_configProxy.setTiltSensitivity(new_val);
        m_tiltSensitivity = m_configProxy.tiltSensitivity();
        emit tiltSensitivityChanged(m_tiltSensitivity);
    }
}

void Config::setTiltFastTime(double new_val)  {
    if( ! qFuzzyCompare(new_val, m_tiltFastTime)) {
        m_configProxy.setTiltFastTime(new_val);
        m_tiltFastTime = m_configProxy.tiltFastTime();
        emit tiltFastTimeChanged(m_tiltFastTime);

    }
}

void Config::setTiltSlowTime(double new_val)  {
    if( ! qFuzzyCompare(new_val, m_tiltSlowTime)) {
        m_configProxy.setTiltSlowTime(new_val);
        m_tiltSlowTime = m_configProxy.tiltSlowTime();
        emit tiltSlowTimeChanged(m_tiltSlowTime);
    }
}

void Config::setTiltHighViscosityTime(double new_val)  {
    if( ! qFuzzyCompare(new_val, m_tiltHighViscosityTime)) {
        m_configProxy.setTiltHighViscosityTime(new_val);
        m_tiltHighViscosityTime = m_configProxy.tiltHighViscosityTime();
        emit tiltHighViscosityTimeChanged();
    }
}

void Config::setTowerHeight(int new_val)  {
    if(new_val != m_towerHeight) {
        m_configProxy.setTowerHeight(new_val);
        m_towerHeight = m_configProxy.towerHeight();
        emit towerHeightChanged(m_towerHeight);
    }
}

void Config::setTowerSensitivity(int new_val)  {
    if(new_val != m_towerSensitivity) {
        m_configProxy.setTowerSensitivity(new_val);
        m_towerSensitivity = m_configProxy.towerSensitivity();
        emit towerSensitivityChanged(m_towerSensitivity);
    }
}

void Config::setTower_microstep_size_nm(int new_val)  {
    if(new_val != m_tower_microstep_size_nm) {
        m_tower_microstep_size_nm = m_configProxy.tower_microstep_size_nm();
        emit tower_microstep_size_nmChanged(m_tower_microstep_size_nm);
    }
}

void Config::setTrigger(int new_val)  {
    if(new_val != m_trigger) {
        m_configProxy.setTrigger(new_val);
        m_trigger = m_configProxy.trigger();
        emit triggerChanged(m_trigger);
    }
}

void Config::setTuneTilt(Int2D new_val)  {
    if(new_val != m_tuneTilt) {
        m_tuneTilt = m_configProxy.tuneTilt();
        emit tuneTiltChanged(m_tuneTilt);
    }
}

void Config::setUpAndDownEveryLayer(int new_val)  {
    if(new_val != m_upAndDownEveryLayer) {
        m_configProxy.setUpAndDownEveryLayer(new_val);
        m_upAndDownEveryLayer = m_configProxy.upAndDownEveryLayer();
        emit upAndDownEveryLayerChanged(m_upAndDownEveryLayer);
    }
}

void Config::setUpAndDownExpoComp(int new_val)  {
    if(new_val != m_upAndDownExpoComp) {
        m_configProxy.setUpAndDownExpoComp(new_val);
        m_upAndDownExpoComp = m_configProxy.upAndDownExpoComp();
        emit upAndDownExpoCompChanged(m_upAndDownExpoComp);
    }
}

void Config::setUpAndDownUvOn(bool new_val)  {
    if(new_val != m_upAndDownUvOn) {
        m_configProxy.setUpAndDownUvOn(new_val);
        m_upAndDownUvOn = m_configProxy.upAndDownUvOn();
        emit upAndDownUvOnChanged(m_upAndDownUvOn);
    }
}

void Config::setUpAndDownWait(int new_val)  {
    if(new_val != m_upAndDownWait) {
        m_configProxy.setUpAndDownWait(new_val);
        m_upAndDownWait = m_configProxy.upAndDownWait();
        emit upAndDownWaitChanged(m_upAndDownWait);
    }
}

void Config::setUpAndDownZoffset(int new_val)  {
    if(new_val != m_upAndDownZoffset) {
        m_configProxy.setUpAndDownZoffset(new_val);
        m_upAndDownZoffset = m_configProxy.upAndDownZoffset();
        emit upAndDownZoffsetChanged(m_upAndDownZoffset);
    }
}

void Config::setUvCalibIntensity(int new_val)  {
    if(new_val != m_uvCalibIntensity) {
        m_configProxy.setUvCalibIntensity(new_val);
        m_uvCalibIntensity = m_configProxy.uvCalibIntensity();
        emit uvCalibIntensityChanged(m_uvCalibIntensity);
    }
}

void Config::setUvCalibMinIntEdge(int new_val)  {
    if(new_val != m_uvCalibMinIntEdge) {
        m_configProxy.setUvCalibMinIntEdge(new_val);
        m_uvCalibMinIntEdge = m_configProxy.uvCalibMinIntEdge();
        emit uvCalibMinIntEdgeChanged(m_uvCalibMinIntEdge);
    }
}

void Config::setUvCurrent(double new_val)  {
    if( ! qFuzzyCompare(new_val, m_uvCurrent)) {
        m_configProxy.setUvCurrent(new_val);
        m_uvCurrent = m_configProxy.uvCurrent();
        emit uvCurrentChanged(m_uvCurrent);
    }
}

void Config::setUvPwm(int new_val)  {
    if(new_val != m_uvPwm) {
        m_configProxy.setUvPwm(new_val);
        m_uvPwm = m_configProxy.uvPwm();
        emit uvPwmChanged(m_uvPwm);
    }
}

void Config::setUvWarmUpTime(int new_val)  {
    if(new_val != m_uvWarmUpTime) {
        m_configProxy.setUvWarmUpTime(new_val);
        m_uvWarmUpTime = m_configProxy.uvWarmUpTime();
        emit uvWarmUpTimeChanged(m_uvWarmUpTime);
    }
}

void Config::setMax_tower_height_mm(int value)
{
    if(value != m_max_tower_height_mm) {
        m_configProxy.setMax_tower_height_mm(value);
        m_max_tower_height_mm = m_configProxy.max_tower_height_mm();
        emit max_tower_height_mmChanged();
    }
}


int Config::mCBoardVersion() {
    return m_MCBoardVersion;
}

bool Config::mCversionCheck() {
    return m_mCversionCheck;
}

bool Config::autoOff() {
    return m_autoOff;
}

int Config::calibTowerOffset() {
    return m_calibTowerOffset;
}

bool Config::calibrated() {
    return m_calibrated;
}

bool Config::coverCheck() {
    return m_coverCheck;
}

int Config::delayAfterExposure() {
    return m_delayAfterExposure;
}

int Config::delayBeforeExposure() {
    return m_delayBeforeExposure;
}

bool Config::fanUvLedEnabled() {
    return m_fanUvLedEnabled;
}

int Config::fanUvLedRpm() {
    return m_fanUvLedRpm;
}

bool Config::fanBlowerEnabled() {
    return m_fan2Enabled;
}

int Config::fanBlowerRpm() {
    return m_fan2Rpm;
}

bool Config::fanRearEnabled() {
    return m_fan3Enabled;
}

int Config::fanRearRpm() {
    return m_fan3Rpm;
}

bool Config::fanCheck() {
    return m_fanCheck;
}

int Config::layerTowerHop() {
    return m_layerTowerHop;
}

int Config::limit4fast() {
    return m_limit4fast;
}

int Config::measuringMoves() {
    return m_measuringMoves;
}

double Config::microStepsMM() {
    return m_microStepsMM;
}

bool Config::mute() {
    return m_mute;
}

bool Config::perPartes() {
    return m_perPartes;
}

int Config::pwrLedPwm() {
    return m_pwrLedPwm;
}

bool Config::resinSensor() {
    return m_resinSensor;
}

int Config::screwMm() {
    return m_screwMm;
}

bool Config::showI18nSelect() {
    return m_showI18nSelect;
}

bool Config::showUnboxing() {
    return m_showUnboxing;
}

bool Config::showWizard() {
    return m_showWizard;
}

int Config::stirringDelay() {
    return m_stirringDelay;
}

int Config::stirringMoves() {
    return m_stirringMoves;
}

bool Config::tilt() {
    return m_tilt;
}

int Config::tiltHeight() {
    return m_tiltHeight;
}

int Config::tiltSensitivity() {
    return m_tiltSensitivity;
}

double Config::tiltFastTime() {
    return m_tiltFastTime;
}

double Config::tiltSlowTime() {
    return m_tiltSlowTime;
}

double Config::tiltHighViscosityTime() {
    return m_tiltHighViscosityTime;
}

int Config::towerHeight() {
    return m_towerHeight;
}

int Config::towerSensitivity() {
    return m_towerSensitivity;
}

int Config::tower_microstep_size_nm() {
    return m_tower_microstep_size_nm;
}

int Config::trigger() {
    return m_trigger;
}

Int2D Config::tuneTilt() {
    return m_tuneTilt;
}

int Config::upAndDownEveryLayer() {
    return m_upAndDownEveryLayer;
}

int Config::upAndDownExpoComp() {
    return m_upAndDownExpoComp;
}

bool Config::upAndDownUvOn() {
    return m_upAndDownUvOn;
}

int Config::upAndDownWait() {
    return m_upAndDownWait;
}

int Config::upAndDownZoffset() {
    return m_upAndDownZoffset;
}

int Config::uvCalibIntensity() {
    return m_uvCalibIntensity;
}

int Config::uvCalibMinIntEdge() {
    return m_uvCalibMinIntEdge;
}

double Config::uvCurrent() {
    return m_uvCurrent;
}

int Config::uvPwm() {
    return m_uvPwm;
}

int Config::uvWarmUpTime() {
    return m_uvWarmUpTime;
}

int Config::max_tower_height_mm() const
{
    return m_max_tower_height_mm;
}

void Config::save() {
    m_configProxy.save();
}

void Config::update_motor_sensitivity() {
    m_configProxy.update_motor_sensitivity();
}

int Config::tankCleaningExposureTime() const
{
    return m_tankCleaningExposureTime;
}

void Config::setTankCleaningExposureTime(int newTankCleaningExposureTime)
{
    if (m_tankCleaningExposureTime == newTankCleaningExposureTime)
        return;
    m_configProxy.setTankCleaningExposureTime(newTankCleaningExposureTime);
    m_tankCleaningExposureTime = m_configProxy.tankCleaningExposureTime();

    emit tankCleaningExposureTimeChanged();
}

int Config::calib_tower_offset_nm() const
{
    return m_calib_tower_offset_nm;
}

void Config::setCalib_tower_offset_nm(int newCalib_tower_offset_nm)
{
    if (m_calib_tower_offset_nm == newCalib_tower_offset_nm)
        return;
    m_configProxy.setCalib_tower_offset_nm(newCalib_tower_offset_nm);
    m_calib_tower_offset_nm = m_configProxy.calib_tower_offset_nm();
    emit calib_tower_offset_nmChanged();
}

const QVariantMap &Config::constraints() const
{
    return m_constraints;
}


int Config::layer_tower_hop_nm() const
{
    return m_layer_tower_hop_nm;
}

void Config::setLayer_tower_hop_nm(int newLayer_tower_hop_nm)
{
    if (m_layer_tower_hop_nm == newLayer_tower_hop_nm)
        return;
    m_configProxy.setLayer_tower_hop_nm(newLayer_tower_hop_nm);
    m_layer_tower_hop_nm = m_configProxy.layer_tower_hop_nm();
    emit layer_tower_hop_nmChanged();
}

int Config::tower_height_nm() const
{
    return m_tower_height_nm;
}

void Config::setTower_height_nm(int newTower_height_nm)
{
    if (m_tower_height_nm == newTower_height_nm)
        return;
    m_configProxy.setTower_height_nm(newTower_height_nm);
    m_tower_height_nm = m_configProxy.tower_height_nm();
    emit tower_height_nmChanged();
}

int Config::up_and_down_z_offset_nm() const
{
    return m_up_and_down_z_offset_nm;
}

void Config::setUp_and_down_z_offset_nm(int newUp_and_down_z_offset_nm)
{
    if (m_up_and_down_z_offset_nm == newUp_and_down_z_offset_nm)
        return;
    m_configProxy.setUp_and_down_z_offset_nm(newUp_and_down_z_offset_nm);
    m_up_and_down_z_offset_nm = m_configProxy.up_and_down_z_offset_nm();
    emit up_and_down_z_offset_nmChanged();
}

int Config::tankCleaningMinDistance_nm() const
{
    return m_tankCleaningMinDistance_nm;
}

void Config::setTankCleaningMinDistance_nm(int newTankCleaningMinDistance_nm)
{
    if (m_tankCleaningMinDistance_nm == newTankCleaningMinDistance_nm)
        return;
    m_configProxy.setTankCleaningMinDistance_nm(newTankCleaningMinDistance_nm);
    m_tankCleaningMinDistance_nm = m_configProxy.tankCleaningMinDistance_nm();
    emit tankCleaningMinDistance_nmChanged();
}

int Config::tankCleaningGentlyUpProfile() const
{
    return m_tankCleaningGentlyUpProfile;
}

void Config::setTankCleaningGentlyUpProfile(int newTankCleaningGentlyUpProfile)
{
    if (m_tankCleaningGentlyUpProfile == newTankCleaningGentlyUpProfile)
        return;
    m_configProxy.setTankCleaningGentlyUpProfile(newTankCleaningGentlyUpProfile);
    m_tankCleaningGentlyUpProfile = m_configProxy.tankCleaningGentlyUpProfile();
    emit tankCleaningGentlyUpProfileChanged();
}

int Config::tankCleaningAdaptorHeight_nm() const
{
    return m_tankCleaningAdaptorHeight_nm;
}

void Config::setTankCleaningAdaptorHeight_nm(int newTankCleaningAdaptorHeight_nm)
{
    if (m_tankCleaningAdaptorHeight_nm == newTankCleaningAdaptorHeight_nm)
        return;
    m_configProxy.setTankCleaningAdaptorHeight_nm(newTankCleaningAdaptorHeight_nm);
    m_tankCleaningAdaptorHeight_nm = m_configProxy.tankCleaningAdaptorHeight_nm();
    emit tankCleaningAdaptorHeight_nmChanged();
}
