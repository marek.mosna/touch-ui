/*
    Copyright 2019-2020, Prusa Research s.r.o.
    Copyright 2021, Prusa Research a.s.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QMetaClassInfo>
#include <QMetaObject>
#include <QDBusPendingCallWatcher>
#include <QQmlEngine>
#include <QDBusError>
#include <QSet>
#include "printer.h"
#include "printer_types.h"
#include "dbusutils.h"
#include "exposure.h"
#include "finishedprintjob_notification.h"


Exposure *Printer::getExposureObject(const QString &path) {
    if(m_exposureObjects.contains(path)) {
        return m_exposureObjects[path];
    }
    else {
        if(path != "/" && path != "") {
            auto tmp = new Exposure(path, m_engine, this);

            // Make sure that QML does not try to delete it
            QQmlEngine::setObjectOwnership(tmp, QQmlEngine::CppOwnership);
            m_exposureObjects[path] = tmp;

            // After the print has finished, generate notification
            connect(tmp, &Exposure::stateChanged, this, [=](Exposure::ExposureState state){
                Exposure * exposure = qobject_cast<Exposure*>(QObject::sender());
                if(exposure) {
                    switch(state) {
                    case Exposure::ExposureState::FINISHED: Q_FALLTHROUGH();
                    case Exposure::ExposureState::FAILURE:  Q_FALLTHROUGH();
                    case Exposure::ExposureState::CANCELED:
                        {
                        Notification * notification = makeLastPrintNotification(exposure, path);
                        notification->setSeverity(100); // Make it self-expanding
                        m_notificationModel.notificationStore()->addNotification(notification);
                        }
                        break;
                    default:
                        ; // do nothing
                    }
                }
            });
            return tmp;
        }
        else {
            return nullptr;
        }
    }
}

QStringList Printer::syncListExposurePaths()
{
    return DBusUtils::listObjects("cz.prusa3d.sl1.exposure0", "/cz/prusa3d/sl1/exposures0");
}

Printer::Printer(QJSEngine &engine, NotificationModel & notificationModel, QObject *parent) :
    BaseDBusObject(engine,  service, interface, path, QDBusConnection::systemBus(),  parent),
    m_printerProxy(service, path, QDBusConnection::systemBus(), this),
    m_notificationModel(notificationModel),
    m_propertiesLoaded(false),
    m_reloadTimer(this)
{

    m_printerProxy.setTimeout(210000);
    registerPrinterTypes();
    setEmitExceptionOnUnregistration(true);

    connect(&m_printerProxy, &PrinterProxy::exception, this, &Printer::exception);
    connect(&m_properties, &PropertiesProxy::PropertiesChanged, this, &Printer::slotPropertiesChanged);

    Printer::handleServiceRegistrationChanged(); // Check the state at creation
    connect(this, &BaseDBusObject::isServiceRegisteredChanged, this, &Printer::handleServiceRegistrationChanged);
}

Notification * Printer::makeLastPrintNotification(Exposure * exposure, QString objPath) {
    FinishedPrintJobNotification * n = new FinishedPrintJobNotification(this);
    n->setProjectName(exposure->project_name());
    n->setProjectFile(exposure->project_file());
    n->setExposurePath(objPath);
    n->setSeverity(0);
    return n;
}


void Printer::reload_properties()
{
    if(m_reloadTimer.isActive() || ! m_printerReady) return; // Only once! Only when ready!

    QDBusPendingReply<QMap<QString, QVariant> > pendingReply = m_properties.GetAll(m_printerProxy.interface());

    QDBusPendingCallWatcher * watcher = new QDBusPendingCallWatcher(pendingReply, this);
    connect(watcher, &QDBusPendingCallWatcher::finished, this, [&](QDBusPendingCallWatcher * replyWatcher) mutable {
        QDBusPendingReply<QMap<QString, QVariant> > reply = *replyWatcher;
        if(reply.isError()) {
            qDebug() << "Could not load properties of " << m_printerProxy.interface() << ", will try again in " << this->m_reloadTimer.interval() <<"ms";
            this->m_reloadTimer.start(); // Try again later
        }
        else {
            const auto & values = reply.value();
            for(const auto & [key, value] : asKeyValueRange(values)) {
                this->set(key, value);
            }

            if(this->m_propertiesLoaded == false) {
                this->m_propertiesLoaded = true; // Succeeded, no need to repeat
                qDebug() << "Properties of " << m_printerProxy.interface() << "loaded.";
                QStringList exposurePaths = this->syncListExposurePaths();
                qDebug() << "Exposures found: " << exposurePaths;

                // Sort the exposure0 DBus paths by the object number
                std::sort(exposurePaths.begin(), exposurePaths.end(), [](const QString & first, const QString & second) {
                    Q_ASSERT(first.length() > 1 && first.contains("/"));
                    Q_ASSERT(second.length() > 1 && second.contains("/"));
                    bool ok = false;
                    int a = first.split("/").last().toInt(&ok);
                    Q_ASSERT(ok);
                    int b = first.split("/").last().toInt(&ok);
                    Q_ASSERT(ok);
                    return a <= b;
                });

                setExposureList(exposurePaths);
                if( ! exposurePaths.isEmpty()) {
                    qDebug() << "Creating notification for " << exposurePaths.last();
                    Exposure * lastPrintExposure = getExposureObject(exposurePaths.last()); // Pre-fetch the last exposure(instantiate its object)
                    if(lastPrintExposure) {
                        // TODO: Conditional on "already seen?"
                        if(true) {
                            Notification * lastPrintNotification = makeLastPrintNotification(lastPrintExposure, exposurePaths.last());
                            m_notificationModel.notificationStore()->addNotification(lastPrintNotification);
                        }
                    }
                }
            }
            setPropertiesValid(true);
        }
        replyWatcher->deleteLater();
    });
}

void Printer::slotPropertiesChanged(const QString &interface, const QMap<QString, QVariant> &changed_properties, const QStringList &invalidates_properties)
{
    Q_UNUSED(invalidates_properties)
    Q_UNUSED(interface)

    //if(! this->m_propertiesLoaded) return; // Ignore all changes before the printer is considered ready

    foreach(QString key, changed_properties.keys()) {
        QVariant v = changed_properties[key];
        if(v.isValid()) {
            set(key, changed_properties.value(key));
        }
        else {
            qDebug() << "Printer::slotPropertiesChanged attempt to set nonexistent property " << key;
        }
    }
    emit propertiesChanged(changed_properties);
}

void Printer::setExposureList(QStringList exposureList)
{
    m_exposureList = exposureList;
    emit exposureListChanged(m_exposureList);
}

void Printer::handleServiceRegistrationChanged() {
    if(this->isServiceRegistered()) {
        setPrinterReady(true);
        this->reload_properties();
    }
    else {
        setPrinterReady(false); // Service died.
        setState(Printer::INITIALIZING);
        setPropertiesValid(false);
        emit exception(QVariantMap{
                            {"code",QVariant(ErrorsSL1::static_toStringCode(ErrorsSL1::SYSTEM_SERVICE_CRASHED))},
                            {"name", QVariant(ErrorsSL1::static_toString(ErrorsSL1::SYSTEM_SERVICE_CRASHED))},
                            {"service", QVariant(m_printerProxy.interface())}
                       });
    }
}

bool Printer::set(const QString &name, const QVariant &value)
{
    if(name == QStringLiteral("api_key")) {
        setApi_key(value.toString());
    }

    else if(name == QStringLiteral("controller_serial")) {
        setController_serial(value.toString());
    }

    else if(name == QStringLiteral("booster_serial")) {
        setBooster_serial(value.toString());
    }

    else if(name == QStringLiteral("expo_panel_serial")) {
        setExpo_panel_serial(value.toString());
    }

    else if(name == QStringLiteral("expo_panel_transmittance")) {
        setExpo_panel_transmittance(value.toDouble());
    }

    else if(name == QStringLiteral("uv_statistics")) {
        MapStringInt orig = value.value<MapStringInt>();
        QVariantMap tmp;
        if(value.canConvert<QDBusArgument>()) {
            QDBusArgument arg = value.value<QDBusArgument>();
            arg >> orig;
        }
        foreach(const QString & key, orig.keys()) {
            tmp[key] = QVariant::fromValue(orig[key]);
        }
        setUv_statistics(tmp);
    }

    else if(name == QStringLiteral("controller_sw_version")) {
        setController_sw_version(value.toString());
    }

    else if(name == QStringLiteral("uv_led_fan")) {
        MapStringInt map = value.value<MapStringInt>();
        if(value.canConvert<QDBusArgument>()) {
            QDBusArgument arg = value.value<QDBusArgument>();
            arg >> map;
        }
        setUvLedFan(map);
    }

    else if(name == QStringLiteral("blower_fan")) {
        MapStringInt map = value.value<MapStringInt>();
        if(value.canConvert<QDBusArgument>()) {
            QDBusArgument arg = value.value<QDBusArgument>();
            arg >> map;
        }
        setBlowerFan(map);
    }

    else if(name == QStringLiteral("rear_fan")) {
        MapStringInt map = value.value<MapStringInt>();
        if(value.canConvert<QDBusArgument>()) {
            QDBusArgument arg = value.value<QDBusArgument>();
            arg >> map;
        }
        setRearFan(map);
    }

    else if(name == QStringLiteral("internal_project_path")) {
        setInternal_project_path(value.toString());
    }

    else if(name == QStringLiteral("tower_position_nm")) {
        setTower_position_nm(value.toInt());
    }

    else if(name == QStringLiteral("controller_revision")) {
        setController_revision(value.toString());
    }

    else if(name == QStringLiteral("project_extensions")) {
        setProject_extensions(value.toStringList());
    }

    else if(name == QStringLiteral("resin_sensor_state")) {
        setResin_sensor_state(value.toBool());
    }

    else if(name == QStringLiteral("state")) {
        setState(PrinterState(value.toInt()));
    }

    else if(name == QStringLiteral("printer_model")) {
        setPrinter_model(PrinterModel(value.toInt()));
    }

    else if(name == QStringLiteral("cpu_temp")) {
        setCpu_temp(value.toDouble());
    }

    else if(name == QStringLiteral("serial_number")) {
        setSerial_number(value.toString());
    }

    else if(name == QStringLiteral("cover_state")) {
        setCover_state(value.toBool());
    }

    else if(name == QStringLiteral("tilt_position")) {
        setTilt_position(value.toInt());
    }

    else if(name == QStringLiteral("factory_mode")) {
        setFactory_mode(value.toBool());
    }

    else if(name == QStringLiteral("system_version")) {
        setSystem_version(value.toString());
    }

    else if(name == QStringLiteral("power_switch_state")) {
        setPower_switch_state(value.toBool());
    }

    else if(name == QStringLiteral("current_page")) {
        setCurrent_page(value.toString());
    }

    else if(name == QStringLiteral("help_page_url")) {
        setHelp_page_url(value.toString());
    }

    else if(name == QStringLiteral("uv_led_temp")) {
        setUv_led_temp(value.toFloat());
    }

    else if(name == QStringLiteral("ambient_temp")) {
        setAmbient_temp(value.toFloat());
    }

    else if(name == QStringLiteral("leds")) {
        MapStringDouble orig = value.value<MapStringDouble>();
        QVariantMap tmp;
        if(value.canConvert<QDBusArgument>()) {
            QDBusArgument arg = value.value<QDBusArgument>();
            arg >> orig;
        }
        foreach(const QString & key, orig.keys()) {
            tmp[key] = QVariant::fromValue(orig[key]);
        }

        setLeds(tmp);
    }

    else if(name == QStringLiteral("system_name")) {
        setSystem_name(value.toString());
    }
    else if(name == QStringLiteral("statistics")) {
        QVariantMap tmp = value.value<QVariantMap>();
        if(value.canConvert<QDBusArgument>()) {
            QDBusArgument arg = value.value<QDBusArgument>();
            arg >> tmp;
        }
        setStatistics(tmp);
    }
    else if(name == QStringLiteral("admin_enabled")) {
        setAdmin_enabled(value.toBool());
    }
    else if(name == QStringLiteral("current_exposure")) {
        setCurrent_exposure(value.value<QDBusObjectPath>().path());
    }
    else if(name == QStringLiteral("http_digest")) {
        setHttp_digest(value.toBool());
    }
    else if(name == QStringLiteral("data_privacy")) {
        setData_privacy(value.toBool());
    }
    else if(name == QStringLiteral("resin_tank_capacity_ml")) {
        setResin_tank_capacity_ml(value.toDouble());
    }
    else if(name == QStringLiteral("unboxed")) {
        setUnboxed(value.toBool());
    }
    else if(name == QStringLiteral("mechanically_calibrated")) {
        setMechanically_calibrated(value.toBool());
    }
    else if(name == QStringLiteral("uv_calibrated")) {
        setUv_calibrated(value.toBool());
    }
    else if(name == QStringLiteral("self_tested")) {
        setSelf_tested(value.toBool());
    }
    else if(name == QStringLiteral("usb_path")) {
        // suppress warning, ignored on purpose
    }
    else if(name == QStringLiteral("failure_reason")) {
        setPrinter_exception(DBusUtils::convert<QVariantMap>(value));
    }
    else {
        qDebug() << "An attempt to set unknown property "<< name;
        return BaseDBusObject::set(name, value);
    }
    return true;
}

bool Printer::set_api_key(QString value){
    return m_printerProxy.setProperty("api_key", QVariant::fromValue(value));
 }

bool Printer::set_http_digest(bool value){
    return m_printerProxy.setProperty("http_digest", QVariant::fromValue(value));
}

bool Printer::set_data_privacy(bool value){
    return m_printerProxy.setProperty("data_privacy", QVariant::fromValue(value));
}

void Printer::beep(int frequency_hz, int length_ms, QJSValue callback_ok, QJSValue callback_fail) {
    auto pending = m_printerProxy.beep(frequency_hz, length_ms);
    DBusUtils::handle_callbacks(m_engine,pending, callback_ok, callback_fail);
    // Allowed to fail silently
}

void Printer::beep_button()
{
    this->beep(1800, 50, QJSValue(), QJSValue());
}

void Printer::disable_motors(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(m_printerProxy.disable_motors(), callback_ok, callback_fail);
}

void Printer::enable_resin_sensor(bool value, QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(m_printerProxy.enable_resin_sensor(value), callback_ok, callback_fail);
}

void Printer::run_unboxing_wizard(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(m_printerProxy.run_unboxing_wizard(), callback_ok, callback_fail);
}

void Printer::run_factory_reset_wizard(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(m_printerProxy.run_factory_reset_wizard(), callback_ok, callback_fail);
}

void Printer::run_calibration_wizard(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(m_printerProxy.run_calibration_wizard(), callback_ok, callback_fail);
}

void Printer::run_uv_calibration_wizard(bool display_replaced, bool led_module_replaced, QJSValue callback_ok, QJSValue callback_fail)
{
    handleCallbackWithError(m_printerProxy.run_uv_calibration_wizard(display_replaced, led_module_replaced), callback_ok, callback_fail);
}

void Printer::cmd_try_cancel_by_path(QString path, QJSValue callback_ok, QJSValue callback_fail)
{
    handleCallbackWithError(m_printerProxy.cmd_try_cancel_by_path(path), callback_ok, callback_fail);
}

void Printer::run_self_test_wizard(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(m_printerProxy.run_self_test_wizard(), callback_ok, callback_fail);
}

void Printer::run_kit_unboxing_wizard(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(m_printerProxy.run_kit_unboxing_wizard(), callback_ok, callback_fail);
}

void Printer::run_displaytest_wizard(QJSValue callback_ok, QJSValue callback_fail)
{
    handleCallbackWithError(m_printerProxy.run_displaytest_wizard(), callback_ok, callback_fail);
}

void Printer::run_tank_surface_cleaner_wizard(QJSValue callback_ok, QJSValue callback_fail)
{
    handleCallbackWithError(m_printerProxy.run_tank_surface_cleaner_wizard(), callback_ok, callback_fail);
}

void Printer::power_led_remove_error(QJSValue callback_ok, QJSValue callback_fail)
{
    handleCallbackWithError(m_printerProxy.power_led_remove_error(), callback_ok, callback_fail);
}

void Printer::power_led_set_error(QJSValue callback_ok, QJSValue callback_fail)
{
    handleCallbackWithError(m_printerProxy.power_led_set_error(), callback_ok, callback_fail);
}

void Printer::download_examples(QJSValue callback_ok, QJSValue callback_fail)
{
    auto pending = m_printerProxy.download_examples();
    DBusUtils::handle_callbacks(pending,
        [=](QVariantList vl) mutable {
            if(callback_ok.isCallable()) {
                QJSValueList params;
                foreach(const QVariant & varg, vl) {
                    params.append( m_engine.toScriptValue<>(varg));
                }
                // Note: deleting the old object is handled by the setter
                // Should be created before calling the callback
                this->setExamplesObject(new Examples(m_engine));
                callback_ok.call(params);
            }
        },
        [=](QVariantList vl) mutable {
            if(callback_fail.isCallable()) {
                QJSValueList params;
                foreach(const QVariant & varg, vl) {
                    params.append( m_engine.toScriptValue<>(varg));
                }
                callback_fail.call(params);
            }
        },
        [this](QString err_name, QString err_message){
            emit dbusError(DBusUtils::parseErrorData(err_name, err_message));
        }
    );

}

void Printer::list_projects_raw(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(m_printerProxy.list_projects_raw(), callback_ok, callback_fail);
}

void Printer::poweroff(bool do_shutdown, bool reboot, QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(m_printerProxy.poweroff(do_shutdown, reboot), callback_ok, callback_fail);
}

void Printer::print(const QString &project_path, bool auto_advance, QJSValue callback_ok, QJSValue callback_fail) {
    auto pending = m_printerProxy.print(project_path, auto_advance);
    QDBusPendingCallWatcher*  watcher = new QDBusPendingCallWatcher(pending, this);
    connect(watcher, &QDBusPendingCallWatcher::finished, this, [=](QDBusPendingCallWatcher * replyWatcher) mutable {
        QDBusPendingReply<QDBusObjectPath> reply = *replyWatcher;
        if(reply.isError()) {
            if(callback_fail.isCallable()) {
                qDebug() << "Printer::print DBusError: "<< reply.error().name();
                callback_fail.call({reply.error().name()});
                emit dbusError(DBusUtils::parseErrorData(reply.error().name(), reply.error().message()));
            }
        }
        else {
            if(callback_ok.isCallable()) {
                QJSValueList params;
                // Pre-fetch the object for later uses
                getExposureObject(reply.argumentAt<0>().path());
                params << reply.argumentAt<0>().path();
                callback_ok.call(params);
            }
        }
        replyWatcher->deleteLater();
    });

}

void Printer::reprint(bool auto_advance, QJSValue callback_ok, QJSValue callback_fail) {
    auto pending = m_printerProxy.reprint(auto_advance);
    QDBusPendingCallWatcher*  watcher = new QDBusPendingCallWatcher(pending, this);
    connect(watcher, &QDBusPendingCallWatcher::finished, this, [=](QDBusPendingCallWatcher * replyWatcher) mutable {
        QDBusPendingReply<QDBusObjectPath> reply = *replyWatcher;
        if(reply.isError()) {
            if(callback_fail.isCallable()) {
                qDebug() << "Printer::reprint DBusError: "<< reply.error().name();
                callback_fail.call({reply.error().name()});
                emit dbusError(DBusUtils::parseErrorData(reply.error().name(), reply.error().message()));
            }
        }
        else {
            if(callback_ok.isCallable()) {
                QJSValueList params;
                // Pre-fetch the object for later uses
                getExposureObject(reply.argumentAt<0>().path());
                params << reply.argumentAt<0>().path();
                callback_ok.call(params);
            }
        }
        replyWatcher->deleteLater();
    });
}

void Printer::tilt_home(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(m_printerProxy.tilt_home(), callback_ok, callback_fail);
}

void Printer::tilt_move(int speed, QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(m_printerProxy.tilt_move(speed), callback_ok, callback_fail);
}

void Printer::tower_home(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(m_printerProxy.tower_home(), callback_ok, callback_fail);
}

void Printer::tower_move(int speed, QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(m_printerProxy.tower_move(speed), callback_ok, callback_fail);
}

void Printer::update_firmware(QString fw_file, QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(m_printerProxy.update_firmware(fw_file), callback_ok, callback_fail);
}

void Printer::make_ready_to_print(QJSValue callback_ok, QJSValue callback_fail)
{
    handleCallbackWithError(m_printerProxy.make_ready_to_print(), callback_ok, callback_fail);
}

void Printer::add_oneclick_inhibitor(QString name, QJSValue callback_ok, QJSValue callback_fail)
{
    handleCallbackWithError(m_printerProxy.add_oneclick_inhibitor(name), callback_ok, callback_fail);
}

void Printer::remove_oneclick_inhibitor(QString name, QJSValue callback_ok, QJSValue callback_fail)
{
    handleCallbackWithError(m_printerProxy.remove_oneclick_inhibitor(name), callback_ok, callback_fail);
}

void Printer::inject_exception(QString code, QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(m_printerProxy.inject_exception(code), callback_ok, callback_fail);
}

void Printer::fail_action(QString code, QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(m_printerProxy.fail_action(code), callback_ok, callback_fail);
}

bool Printer::admin_enabled() const
{
    return m_admin_enabled;
}

// getters:

QString Printer::controller_serial() const {
    return m_controller_serial;
}


QString Printer::booster_serial() const {
    return m_booster_serial;
}

QString Printer::expo_panel_serial() const {
    return m_expo_panel_serial;
}

double Printer::expo_panel_transmittance() const {
    return m_expo_panel_transmittance;
}

QVariantMap Printer::uv_statistics() const {
    return m_uv_statistics;
}


QString Printer::controller_sw_version() const {
    return m_controller_sw_version;
}

QString Printer::internal_project_path() const {
    return m_internal_project_path;
}


int Printer::tower_position_nm() const {
    return m_tower_position_nm;
}


QString Printer::controller_revision() const {
    return m_controller_revision;
}


QStringList Printer::project_extensions() const {
    return m_project_extensions;
}


bool Printer::resin_sensor_state() const {
    return m_resin_sensor_state;
}


Printer::PrinterState Printer::state() const {
    return m_state;
}


double Printer::cpu_temp() const {
    return m_cpu_temp;
}


QString Printer::serial_number() const {
    return m_serial_number;
}


QString Printer::api_key() const {
    return m_api_key;
}


bool Printer::cover_state() const {
    return m_cover_state;
}


int Printer::tilt_position() const {
    return m_tilt_position;
}


bool Printer::factory_mode() const {
    return m_factory_mode;
}


QString Printer::system_version() const {
    return m_system_version;
}


bool Printer::power_switch_state() const {
    return m_power_switch_state;
}


QString Printer::current_page() const {
    return m_current_page;
}

QString Printer::help_page_url() const {
    return m_help_page_url;
}

QVariantMap Printer::leds() const {
    return m_leds;
}


QString Printer::system_name() const {
    return m_system_name;
}

QVariantMap Printer::printer_exception() const {
    return m_printer_exception;
}

QVariantMap Printer::statistics() const {
    return m_statistics;
}

// setters:

void Printer::setTower_position_nm(int new_val)  {
    if(new_val != m_tower_position_nm) {
        m_tower_position_nm = new_val;
        // Do not propagate changes to the backend
        emit tower_position_nmChanged(m_tower_position_nm);
    }
}

void Printer::setTilt_position(int new_val)  {
    if(new_val != m_tilt_position) {
        m_tilt_position = new_val;
        // Do not propagate changes to the backend

        emit tilt_positionChanged(m_tilt_position);
    }
}

void Printer::setAdmin_enabled(bool new_val) {
    if(m_admin_enabled == new_val) {
        return;
    }
    m_admin_enabled = new_val;
    emit admin_enabledChanged(new_val);
}

void Printer::setCurrent_exposure(QString new_val) {
    if(m_current_exposure == new_val) {
        return;
    }
    m_current_exposure = new_val;
    emit current_exposure_objectChanged(getExposureObject(m_current_exposure));
    emit current_exposureChanged(new_val);
}

void Printer::setHttp_digest(bool new_val) {
    if(m_http_digest == new_val) {
        return;
    }
    m_http_digest = new_val;
    emit http_digestChanged(new_val);
}

void Printer::setData_privacy(bool new_val) {
    if(m_data_privacy == new_val) {
        return;
    }
    m_data_privacy = new_val;
    emit data_privacyChanged(new_val);
}


void Printer::setResin_tank_capacity_ml(double new_val) {
    if(qFuzzyCompare(m_resin_tank_capacity_ml, new_val)) {
        return;
    }
    m_resin_tank_capacity_ml = new_val;
    emit resin_tank_capacity_mlChanged(new_val);
}

void Printer::setController_serial(QString new_val)  {
    if(new_val != m_controller_serial) {
        m_controller_serial = new_val;
        emit controller_serialChanged(new_val);
    }
}

void Printer::setBooster_serial(QString new_val)  {
    if(new_val != m_booster_serial) {
        m_booster_serial = new_val;
        emit booster_serialChanged(new_val);
    }
}

void Printer::setExpo_panel_serial(QString new_val)  {
    if(new_val != m_expo_panel_serial) {
        m_expo_panel_serial = new_val;
        emit expo_panel_serialChanged(new_val);
    }
}

void Printer::setExpo_panel_transmittance(double new_val)  {
    if(new_val != m_expo_panel_transmittance) {
        m_expo_panel_transmittance = new_val;
        emit expo_panel_transmittanceChanged(new_val);
    }
}

void Printer::setUv_statistics(QVariantMap new_val)  {
    if(new_val != m_uv_statistics) {
        m_uv_statistics = new_val;
        emit uv_statisticsChanged(new_val);
    }
}

void Printer::setController_sw_version(QString new_val)  {
    if(new_val != m_controller_sw_version) {
        m_controller_sw_version = new_val;
        emit controller_sw_versionChanged(new_val);
    }
}

void Printer::setUvLedFan(QMap<QString, int> new_val) {
    setFanUvLedError(new_val.value("error") == 1);
    setFanUvLedRpm(new_val.value("rpm"));
}

void Printer::setBlowerFan(QMap<QString, int> new_val) {
    setFanBlowerError(new_val.value("error") == 1);
    setFanBlowerRpm(new_val.value("rpm"));
}

void Printer::setRearFan(QMap<QString, int> new_val) {
    setFanRearError(new_val.value("error") == 1);
    setFanRearRpm(new_val.value("rpm"));
}

void Printer::setInternal_project_path(QString new_val)  {
    if(new_val != m_internal_project_path) {
        m_internal_project_path = new_val;
        emit internal_project_pathChanged(new_val);
    }
}

void Printer::setController_revision(QString new_val)  {
    if(new_val != m_controller_revision) {
        m_controller_revision = new_val;
        emit controller_revisionChanged(new_val);
    }
}

void Printer::setProject_extensions(QStringList new_val)  {
    if(new_val != m_project_extensions) {
        m_project_extensions = new_val;
        emit project_extensionsChanged(new_val);
    }
}

void Printer::setResin_sensor_state(bool new_val)  {
    if(new_val != m_resin_sensor_state) {
        m_resin_sensor_state = new_val;
        emit resin_sensor_stateChanged(new_val);
    }
}

void Printer::setState(Printer::PrinterState new_val)  {
    qDebug() << "Printer0.state -> " << new_val;
    if(new_val != m_state) {
        m_state = new_val;
        emit stateChanged(new_val);
    }
}

void Printer::setCpu_temp(double new_val)  {
    if(! qFuzzyCompare(new_val, m_cpu_temp)) {
        m_cpu_temp = new_val;
        emit cpu_tempChanged(new_val);
    }
}

void Printer::setSerial_number(QString new_val)  {
    if(new_val != m_serial_number) {
        m_serial_number = new_val;
        emit serial_numberChanged(new_val);
    }
}

void Printer::setApi_key(QString new_val)  {
    if(new_val != m_api_key) {
        m_api_key = new_val;
        emit api_keyChanged(new_val);
    }
}

void Printer::setCover_state(bool new_val)  {
    if(new_val != m_cover_state) {
        m_cover_state = new_val;
        emit cover_stateChanged(new_val);
    }
}

void Printer::setFactory_mode(bool new_val)  {
    if(new_val != m_factory_mode) {
        m_factory_mode = new_val;
        emit factory_modeChanged(new_val);
    }
}

void Printer::setSystem_version(QString new_val)  {
    if(new_val != m_system_version) {
        m_system_version = new_val;
        emit system_versionChanged(new_val);
    }
}

void Printer::setPower_switch_state(bool new_val)  {
    if(new_val != m_power_switch_state) {
        m_power_switch_state = new_val;
        emit power_switch_stateChanged(new_val);
    }
}

void Printer::setCurrent_page(QString new_val)  {
    if(new_val != m_current_page) {
        m_current_page = new_val;
        emit current_pageChanged(new_val);
    }
}

void Printer::setHelp_page_url(QString new_val)  {
    if(new_val != m_help_page_url) {
        m_help_page_url = new_val;
        emit help_page_urlChanged(new_val);
    }
}

void Printer::setLeds(QVariantMap new_val)  {
    if(new_val != m_leds) {
        m_leds = new_val;
        emit ledsChanged(new_val);
    }
}

void Printer::setSystem_name(QString new_val)  {
    if(new_val != m_system_name) {
        m_system_name = new_val;
        emit system_nameChanged(new_val);
    }
}

void Printer::setStatistics(QVariantMap new_val)  {
    if(new_val != m_statistics) {
        m_statistics = new_val;
        emit statisticsChanged(new_val);
    }
}

const QString &Printer::multimediaRoot() const
{
    return m_multimediaRoot;
}

void Printer::setPrinter_exception(QVariantMap new_val)  {
    if(new_val != m_printer_exception) {
            m_printer_exception = new_val;
            emit printer_exceptionChanged(new_val);
    }
}

void Printer::setMultimediaRoot(const QString &newMultimediaRoot)
{
    if (m_multimediaRoot == newMultimediaRoot)
        return;
    m_multimediaRoot = newMultimediaRoot;
    emit multimediaRootChanged();
    emit multimediaImagesChanged();
}

const QString Printer::multimediaImages() const
{
    QString model = "SL1";
    switch(printer_model()) {
        case NONE:
    case SL1:
        model = "SL1";
        break;
    case SL1S:
        model = "SL1S";
        break;
    case M1:
        model = "M1";
        break;
    default:
        model = "SL1";
    }
    return multimediaRoot() + model;
}

const QString &Printer::multimediaSounds() const
{
    return multimediaRoot();
}



bool Printer::printerReady() const
{
    return m_printerReady;
}

int Printer::fanUvLedRpm() const
{
    return m_fanUvLedRpm;
}

void Printer::setFanUvLedRpm(int newFanUvLedRpm)
{
    if (m_fanUvLedRpm == newFanUvLedRpm)
        return;
    m_fanUvLedRpm = newFanUvLedRpm;
    emit fanUvLedRpmChanged();
}

bool Printer::fanUvLedError() const
{
    return m_fanUvLedError;
}

void Printer::setFanUvLedError(bool newFanUvLedError)
{
    if (m_fanUvLedError == newFanUvLedError)
        return;
    m_fanUvLedError = newFanUvLedError;
    emit fanUvLedErrorChanged();
}

int Printer::fanBlowerRpm() const
{
    return m_fanBlowerRpm;
}

void Printer::setFanBlowerRpm(int newFanBlowerRpm)
{
    if (m_fanBlowerRpm == newFanBlowerRpm)
        return;
    m_fanBlowerRpm = newFanBlowerRpm;
    emit fanBlowerRpmChanged();
}

bool Printer::fanBlowerError() const
{
    return m_fanBlowerError;
}

void Printer::setFanBlowerError(bool newFanBlowerError)
{
    if (m_fanBlowerError == newFanBlowerError)
        return;
    m_fanBlowerError = newFanBlowerError;
    emit fanBlowerErrorChanged();
}

int Printer::fanRearRpm() const
{
    return m_fanRearRpm;
}

void Printer::setFanRearRpm(int newFanRearRpm)
{
    if (m_fanRearRpm == newFanRearRpm)
        return;
    m_fanRearRpm = newFanRearRpm;
    emit fanRearRpmChanged();
}

bool Printer::fanRearError() const
{
    return m_fanRearError;
}

void Printer::setFanRearError(bool newFanRearError)
{
    if (m_fanRearError == newFanRearError)
        return;
    m_fanRearError = newFanRearError;
    emit fanRearErrorChanged();
}
