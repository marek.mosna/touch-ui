/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef LOGS_H
#define LOGS_H

#include <QObject>

#include "logs_proxy.h"
#include "basedbusobject.h"

class Logs : public BaseDBusObject
{
    Q_OBJECT
public:
    enum class LogsState {
        IDLE = 0,
        EXPORTING,
        SAVING,
        FINISHED,
        FAILED,
        CANCELED,
    };
    Q_ENUM(LogsState)

    enum class StoreType {
        IDLE = 0,
        USB,
        UPLOAD,
    };
    Q_ENUM(StoreType)

    inline static const QString service{"cz.prusa3d.sl1.logs0"};
    inline static const QString interface{"cz.prusa3d.sl1.logs0"};
    inline static const QString path{"/cz/prusa3d/sl1/logs0"};

    explicit Logs(QJSEngine & engine, QObject *parent = nullptr);

    Q_INVOKABLE QString lastLogUploadIdentifier();
    Q_INVOKABLE void usbSave(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());
    Q_INVOKABLE void serverUpload(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());
    Q_INVOKABLE void cancel(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    Q_PROPERTY(QVariantMap failureReason READ failureReason /*WRITE setFailureReason*/ NOTIFY failureReasonChanged)
    Q_PROPERTY(LogsState state READ state /*WRITE setState*/ NOTIFY stateChanged)
    Q_PROPERTY(StoreType type READ type /*WRITE setType*/ NOTIFY typeChanged)
    Q_PROPERTY(double exportProgress READ exportProgress /*WRITE setExportProgress*/ NOTIFY exportProgressChanged)
    Q_PROPERTY(double storeProgress READ storeProgress /*WRITE setStoreProgress*/ NOTIFY storeProgressChanged)
    Q_PROPERTY(QString logUploadIdentifier READ logUploadIdentifier /*WRITE setLogUploadIdentifier*/ NOTIFY logUploadIdentifierChanged)
    Q_PROPERTY(QString logUploadUrl READ logUploadUrl /*WRITE setLogUploadUrl*/ NOTIFY logUploadUrlChanged)

    void setFailureReason(const QVariantMap &newFailureReason);

    void setState(LogsState newState);

    void setType(StoreType newType);

    void setExportProgress(double newExportProgress);

    void setStoreProgress(double newStoreProgress);

    void setLogUploadIdentifier(const QString &newLogUploadIdentifier);

    void setLogUploadUrl(const QString &newLogUploadUrl);

signals:
    void failureReasonChanged();
    void stateChanged();
    void typeChanged();
    void exportProgressChanged();
    void storeProgressChanged();
    void logUploadIdentifierChanged();
    void logUploadUrlChanged();


public slots:

private:
    LogsProxy logsProxy;

    QVariantMap failureReason() const;
    LogsState state() const;
    StoreType type() const;
    double exportProgress() const;
    double storeProgress() const;
    QString logUploadIdentifier() const;
    QString logUploadUrl() const;

    // BaseDBusObject interface
    QVariantMap m_failureReason;

    LogsState m_state{LogsState::IDLE};

    StoreType m_type{StoreType::IDLE};

    double m_exportProgress{0.0};

    double m_storeProgress{0.0};

    QString m_logUploadIdentifier;

    QString m_logUploadUrl;

protected:
    virtual bool set(const QString &name, const QVariant &value) override;

};

#endif // LOGS_H
