/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "logs.h"


Logs::Logs(QJSEngine & engine, QObject *parent):
    BaseDBusObject(engine, service, interface, path, QDBusConnection::systemBus(), parent),
    logsProxy(service, path, QDBusConnection::systemBus(), this)
{
}


bool Logs::set(const QString &name, const QVariant &value)
{
    if(name == QStringLiteral("failure_reason")) {
        QVariantMap failureReason = DBusUtils::convert<QVariantMap>(value);
        setFailureReason(failureReason);
    }
    else if(name == QStringLiteral("state")) {
        setState(LogsState(DBusUtils::convert<int>(value)));
    }
    else if(name == QStringLiteral("export_progress")) {
        setExportProgress(DBusUtils::convert<double>(value));
    }
    else if(name == QStringLiteral("store_progress")) {
        setStoreProgress(DBusUtils::convert<double>(value));
    }
    else if(name == QStringLiteral("log_upload_identifier")) {
        setLogUploadIdentifier(DBusUtils::convert<QString>(value));
    }
    else if(name == QStringLiteral("log_upload_url")) {
        setLogUploadUrl(DBusUtils::convert<QString>(value));
    }
    else if(name == QStringLiteral("type")) {
        setType(StoreType(DBusUtils::convert<int>(value)));
    }
    else return BaseDBusObject::set(name, value);

    return true;
}

void Logs::usbSave(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(logsProxy.usb_save(), callback_ok, callback_fail);
}

void Logs::serverUpload(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(logsProxy.server_upload(), callback_ok, callback_fail);
}

void Logs::cancel(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(logsProxy.cancel(), callback_ok, callback_fail);
}

QString Logs::lastLogUploadIdentifier() {
    return logsProxy.last_log_upload_identifier();
}

QVariantMap Logs::failureReason() const {
    return logsProxy.failure_reason();
}

Logs::LogsState Logs::state() const {
    return Logs::LogsState(logsProxy.state());
}

Logs::StoreType Logs::type() const {
    return Logs::StoreType(logsProxy.type());
}

double Logs::exportProgress() const {
    return logsProxy.export_progress();
}

double Logs::storeProgress() const {
    return logsProxy.store_progress();
}

QString Logs::logUploadIdentifier() const {
    return logsProxy.log_upload_identifier();
}

QString Logs::logUploadUrl() const
{
    return logsProxy.log_upload_url();
}



void Logs::setFailureReason(const QVariantMap &newFailureReason)
{
    if (m_failureReason == newFailureReason)
        return;
    m_failureReason = newFailureReason;
    emit failureReasonChanged();
}

void Logs::setState(LogsState newState)
{
    if (m_state == newState)
        return;
    m_state = newState;
    emit stateChanged();
}

void Logs::setType(StoreType newType)
{
    if (m_type == newType)
        return;
    m_type = newType;
    emit typeChanged();
}

void Logs::setExportProgress(double newExportProgress)
{
    if (qFuzzyCompare(m_exportProgress, newExportProgress))
        return;
    m_exportProgress = newExportProgress;
    emit exportProgressChanged();
}

void Logs::setStoreProgress(double newStoreProgress)
{
    if (qFuzzyCompare(m_storeProgress, newStoreProgress))
        return;
    m_storeProgress = newStoreProgress;
    emit storeProgressChanged();
}

void Logs::setLogUploadIdentifier(const QString &newLogUploadIdentifier)
{
    if (m_logUploadIdentifier == newLogUploadIdentifier)
        return;
    m_logUploadIdentifier = newLogUploadIdentifier;
    emit logUploadIdentifierChanged();
}

void Logs::setLogUploadUrl(const QString &newLogUploadUrl)
{
    if (m_logUploadUrl == newLogUploadUrl)
        return;
    m_logUploadUrl = newLogUploadUrl;
    emit logUploadUrlChanged();
}
