/*
    Copyright 2020, Prusa Research a.s.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef RAUC_H
#define RAUC_H

#include <QObject>
#include <QJSValue>
#include <QQmlEngine>

#include "dbusutils.h"
#include "basedbusobject.h"

#include "rauc_proxy.h"


class Rauc : public BaseDBusObject
{
    Q_OBJECT

public:
    /// Rauc install operation result
    enum Result {
        RaucResultOk = 0,
        RaucresultError,
    };
    Q_ENUM(Result)

    inline static const QString service{"de.pengutronix.rauc"};
    inline static const QString interface{"de.pengutronix.rauc.Installer"};
    inline static const QString path{"/"};

    explicit Rauc(QQmlEngine & _engine, QObject *parent = nullptr);


    Q_PROPERTY(QString operation READ operation /*WRITE setOperation*/ NOTIFY operationChanged)
    Q_PROPERTY(QString lastError READ lastError /*WRITE setLastError*/ NOTIFY lastErrorChanged)
    Q_PROPERTY(QString compatible READ compatible /*WRITE setCompatible*/ NOTIFY compatibleChanged)
    Q_PROPERTY(QString bootSlot READ bootSlot /*WRITE setBootSlot*/ NOTIFY bootSlotChanged)
    Q_PROPERTY(int progressPercent READ progressPercent /*WRITE setProgressPercent*/ NOTIFY progressPercentChanged)
    Q_PROPERTY(QString progressMessage READ progressMessage /*WRITE setProgressMessage*/ NOTIFY progressMessageChanged)
    Q_PROPERTY(int progressDepth READ progressDepth /*WRITE setProgressDepth*/ NOTIFY progressDepthChanged)
    Q_PROPERTY(QString variant READ variant /*WRITE setVariant*/ NOTIFY variantChanged)

    Q_INVOKABLE void switchToAlternativeSystem(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());
    Q_INVOKABLE void install(QString bundlePath);
    Q_INVOKABLE void info(QString bundlePath, QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    Q_INVOKABLE QString alternativeSystemVersion();


    const QString &operation() const;
    void setOperation(const QString &newOperation);

    const QString &lastError() const;
    void setLastError(const QString &newLastError);

    const QString &compatible() const;
    void setCompatible(const QString &newCompatible);

    const QString &bootSlot() const;
    void setBootSlot(const QString &newBootSlot);

    int progressPercent() const;
    void setProgressPercent(int newProgressPercent);

    const QString &progressMessage() const;
    void setProgressMessage(const QString &newProgressMessage);

    int progressDepth() const;
    void setProgressDepth(int newProgressDepth);

    const QString &variant() const;
    void setVariant(const QString &newVariant);

signals:
    /// Install completed signal
    void completed(Rauc::Result result);

    void operationChanged();
    void lastErrorChanged();
    void compatibleChanged();
    void bootSlotChanged();
    void progressPercentChanged();
    void progressMessageChanged();
    void progressDepthChanged();

    void variantChanged();

public slots:

private:
    RaucProxy raucProxy;

    // BaseDBusObject interface
    QString m_operation;

    QString m_lastError;

    QString m_compatible;

    QString m_bootSlot;

    int m_progressPercent{0};

    QString m_progressMessage;

    int m_progressDepth{0};

    QString m_variant;

protected:
    virtual bool set(const QString &name, const QVariant &value) override;
};

#endif // RAUC_H
