/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "custom_types.h"

#include <QDebug>

QDBusArgument &operator<<(QDBusArgument &argument, const StatusStruct &statusStruct)
{
    argument.beginStructure();
    argument << statusStruct.operation << statusStruct.progress << statusStruct.bundlePath;
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, StatusStruct &statusStruct)
{
    argument.beginStructure();
    argument >> statusStruct.operation >> statusStruct.progress >> statusStruct.bundlePath;
    argument.endStructure();
    return argument;
}

QDBusArgument &operator<<(QDBusArgument &argument, const UpdateInfoStruct &updateInfoStruct)
{
    argument.beginStructure();
    argument << updateInfoStruct.currentVersion <<
                updateInfoStruct.nextVersion <<
                updateInfoStruct.url <<
                updateInfoStruct.digest <<
                updateInfoStruct.date;
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, UpdateInfoStruct &updateInfoStruct)
{
    argument.beginStructure();
    argument >> updateInfoStruct.currentVersion >>
            updateInfoStruct.nextVersion >>
            updateInfoStruct.url >>
            updateInfoStruct.digest >>
            updateInfoStruct.date;
    argument.endStructure();
    return argument;
}

QDBusArgument &operator<<(QDBusArgument &argument, const RaucSlotEntry &slotEntry) {
	argument.beginStructure();
	argument << slotEntry.name;
	argument << slotEntry.data;
	argument.endStructure();
	return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, RaucSlotEntry &slotEntry) {
	argument.beginStructure();
	argument >> slotEntry.name;
	argument >> slotEntry.data;
	argument.endStructure();
	return argument;
}


QDBusArgument &operator<<(QDBusArgument &argument, const RaucProgress &raucProgress) {
	argument.beginStructure();
	argument << raucProgress.percent;
	argument << raucProgress.message;
	argument << raucProgress.depth;
	argument.endStructure();
	return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, RaucProgress &raucProgress) {
	argument.beginStructure();
	argument >> raucProgress.percent;
	argument >> raucProgress.message;
	argument >> raucProgress.depth;
	argument.endStructure();
	return argument;
}
