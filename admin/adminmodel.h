/*
    Copyright 2020, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef ADMINMODEL_H
#define ADMINMODEL_H

#include <QAbstractListModel>
#include "adminitemstore.h"
class AdminModel : public QAbstractListModel
{
    Q_OBJECT
    QPointer<AdminItemStore> m_store;

    int count() const;
public:

    //Q_ENUM(Roles);

    enum ItemTypes {
        XActionItem = 10,
        XIntValueItem,
        XFloatValueItem,
        XBoolValueItem,
        XTextValueItem,
        XFixedValueItem,
        XSelectionValueItem
    };
    Q_ENUM(ItemTypes);

    explicit AdminModel(AdminItemStore * _adminItemStore, QObject *parent = nullptr);


    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    virtual QHash<int, QByteArray> roleNames() const override;
    Q_INVOKABLE void execute(int idx) {

        if(m_store) {
            m_store->execute(idx);
        }
    }
public slots:
    void resetBegin() {  beginResetModel(); }
    void resetEnd() {  endResetModel(); }
    void handleDataChanged(int idx, const QVector<int> roles) {
        emit dataChanged(createIndex(idx, 0), createIndex(idx, 0), roles);
    }
};

#endif // ADMINMODEL_H
