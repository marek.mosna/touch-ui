/*
    Copyright 2020, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "adminitemstore.h"

AdminItemStore::AdminItemStore(QObject *parent) : QObject(parent)
{

}

void AdminItemStore::refresh(const QVector<AdminItemPointer> & _data)
{
    emit resetBegin();
    this->clearItems();
    for(const auto & item : qAsConst(_data)) {
        insert(item);
    }
    emit resetEnd();
}

void AdminItemStore::execute(int idx) {
    auto ptr = qobject_cast<ActionItem*>(get(idx));
    if(ptr) ptr->execute();
    else qWarning() << __PRETTY_FUNCTION__ << "invalid attempt to call execute() on item" << get(idx)->name();
}

void AdminItemStore::handlePropertiesChanged(const QVariantMap &changedProperties)
{
    Item* sender = qobject_cast<Item*>(QObject::sender());
    if(sender) {
        auto idx = m_items.indexOf(sender);
        if(idx != -1) {
            QVector<int> roles;
            if(changedProperties.contains("value")) roles << ItemRoles::Value;
            if(changedProperties.contains("step")) roles << ItemRoles::Step;
            if(changedProperties.contains("name")) roles << ItemRoles::Name;
            if(changedProperties.contains("icon")) roles << ItemRoles::Icon;
            if(changedProperties.contains("enabled")) roles << ItemRoles::Enabled;
            if(changedProperties.contains("fractions")) roles << ItemRoles::Fractions;
            if(changedProperties.contains("selection")) roles << ItemRoles::Selection;
            if(changedProperties.contains("wrap_around")) roles << ItemRoles::WrapAround;
            if(changedProperties.contains("minimum")) roles << ItemRoles::Minimum;
            if(changedProperties.contains("maximum")) roles << ItemRoles::Maximum;
            emit dataChanged(idx, roles);
        }
        else qWarning() << __PRETTY_FUNCTION__ << "called from invalid sender";
    }
    else qWarning() << __PRETTY_FUNCTION__ << "called from invalid sender";
}

bool AdminItemStore::insert(const AdminItemPointer & _item)
{
    if(_item.interface == "cz.prusa3d.sl1.admin0.action") {
        m_items.append(QPointer<Item>(new ActionItem(this, _item.path)));
    } else if(_item.interface == "cz.prusa3d.sl1.admin0.value.int") {
        m_items.append(QPointer<Item>(new IntValueItem(this, _item.path)));
    } else if(_item.interface == "cz.prusa3d.sl1.admin0.value.float") {
        m_items.append(QPointer<Item>(new FloatValueItem(this, _item.path)));
    } else if(_item.interface == "cz.prusa3d.sl1.admin0.value.bool") {
        m_items.append(QPointer<Item>(new BoolValueItem(this, _item.path)));
    } else if(_item.interface == "cz.prusa3d.sl1.admin0.value.text") {
        m_items.append(QPointer<Item>(new TextValueItem(this, _item.path)));
    } else if(_item.interface == "cz.prusa3d.sl1.admin0.value.fixed") {
        m_items.append(QPointer<Item>(new FixedValueItem(this, _item.path)));
    } else if(_item.interface == "cz.prusa3d.sl1.admin0.value.selection") {
        m_items.append(QPointer<Item>(new SelectionValueItem(this, _item.path)));
    }
    else {
        qWarning() << "Unknown admin item interface: " << _item.interface;
        return false;
    }
    connect(m_items.last().data(), &Item::propertiesChanged, this, &AdminItemStore::handlePropertiesChanged);
    return true;
}

void AdminItemStore::clearItems()
{
    for(auto & ptr : m_items) {
        if(ptr) delete ptr.data();
    }
    m_items.clear();
}
