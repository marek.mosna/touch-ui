/*
    Copyright 2020, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef ADMIN_H
#define ADMIN_H

#include <QObject>
#include <QMap>
#include <QList>
#include <QVariant>
#include <QSharedPointer>
#include <QQmlListProperty>

#include "types.h"
#include "admin_proxy.h"
#include "properties_proxy.h"
#include "object_manager_proxy.h"
#include "items.h"
#include "adminitemstore.h"
#include "adminmodel.h"
class Admin: public QObject
{
    Q_OBJECT
public:
    explicit Admin(QObject *parent = nullptr);

    Q_PROPERTY(bool isActive READ isActive NOTIFY isActiveChanged)
    //Q_PROPERTY(QList<Item*> items MEMBER items NOTIFY itemsChanged)

    // TODO: This is not needed ro Qt >= 5.14
    //Q_PROPERTY(QVariantList varItems READ varItems NOTIFY varItemsChanged)
    Q_PROPERTY(AdminItemStore * store READ store CONSTANT)
    Q_PROPERTY(AdminModel * model READ model CONSTANT)

    Q_INVOKABLE void enter();

    bool isActive() const;

    /** Storage for Item-derived objects, backing the AdminModel */
    AdminItemStore *store() const;

    /** Presenting the current list of admin items to the view */
    AdminModel *model() const;

signals:
    void itemsChanged(const QList<Item*> items);
    void varItemsChanged(const QVariantList varItems);
    void enterSysinfo();
    void enterTouchscreenTest();
    void enterFullscreenImage();
    void enterTowerMoves();
    void enterTiltMoves();

    void isActiveChanged(bool isActive);

public slots:
    /** @brief Handle changes to Admin properties */
    void proxyPropertiesChanged(const QString &interface, const QMap<QString, QVariant> &changed_properties,
                                const QStringList &invalidates_properties);


private:
    AdminProxy adminProxy;
    PropertiesProxy propertiesProxy;
    ObjectManagerProxy objectManagerProxy;

    QList<Item*> items;
    QVariantList varItems();

    AdminItemStore *m_store;
    AdminModel *m_model;
};

#endif // ADMIN_H
