/*
    Copyright 2020, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "adminmodel.h"
#include "items.h"

int AdminModel::count() const
{
    if(m_store) return m_store->count();
    else return 0;
}

AdminModel::AdminModel(AdminItemStore *_adminItemStore, QObject *parent)
    : QAbstractListModel(parent),
      m_store(_adminItemStore)
{
}


int AdminModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    else return this->count();
}

QVariant AdminModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || index.column() != 0)
        return QVariant();

    auto idx = index.row();
    if(idx >= 0 && idx < this->count()) {
        switch(role) {
        case ItemRoles::ItemType:
        {
            auto item = m_store->get(idx);
            if(qobject_cast<ActionItem*>(item)) return XActionItem;
            else if(qobject_cast<IntValueItem*>(item)) return XIntValueItem;
            else if(qobject_cast<FloatValueItem*>(item)) return XFloatValueItem;
            else if(qobject_cast<BoolValueItem*>(item)) return XBoolValueItem;
            else if(qobject_cast<TextValueItem*>(item)) return XTextValueItem;
            else if(qobject_cast<FixedValueItem*>(item)) return XFixedValueItem;
            else if(qobject_cast<SelectionValueItem*>(item)) return XSelectionValueItem;
            else return QVariant();
        }
            break;
        case ItemRoles::Name:
            return m_store->get(idx)->name();
        case ItemRoles::Icon:
            return m_store->get(idx)->icon();
        case ItemRoles::Enabled:
            return m_store->get(idx)->property("enabled");
        case ItemRoles::Value:
            return m_store->get(idx)->valueVariant();
        case ItemRoles::Step:
            return m_store->get(idx)->property("step");
        case ItemRoles::Fractions:
            return m_store->get(idx)->property("fractions");
        case ItemRoles::Selection:
            return m_store->get(idx)->property("selection");
        case ItemRoles::WrapAround:
            return m_store->get(idx)->property("wrap_around");
        case ItemRoles::Minimum:
            return m_store->get(idx)->property("minimum");
        case ItemRoles::Maximum:
            return m_store->get(idx)->property("maximum");
        case Qt::DisplayRole:
            return m_store->get(idx)->name();
        default:
            ;
        }
    }

    return QVariant();
}

bool AdminModel::setData(const QModelIndex &index, const QVariant &value, int role)
{

    if (index.row() >= 0 && index.row() < this->count() && data(index, role) != value && m_store) {
        const int idx = index.row();
        switch(role) {
        case ItemRoles::ItemType:
            return false;
            break;
        case ItemRoles::Name:
            return false;
            break;
        case ItemRoles::Icon:
            return false;
            break;
        case ItemRoles::Enabled:
            return false;
        case ItemRoles::Value:
            m_store->get(idx)->setValueVariant(value);
            break;
        case ItemRoles::Step:
            return false;
        case ItemRoles::Fractions:
            return false;
        case ItemRoles::Selection:
            return false;
        case ItemRoles::WrapAround:
            return false;
        case ItemRoles::Minimum:
            return false;
        case ItemRoles::Maximum:
            return false;
        default:
            ;
        }

        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags AdminModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable;
}


QHash<int, QByteArray> AdminModel::roleNames() const
{
    return QHash<int, QByteArray> {
        {ItemRoles::ItemType, "itemType"},
        {ItemRoles::Name, "name"},
        {ItemRoles::Icon, "icon"},
        {ItemRoles::Enabled, "enabled"},
        {ItemRoles::Value, "value"},
        {ItemRoles::Step, "step"},
        {ItemRoles::Fractions, "fractions"},
        {ItemRoles::Selection, "selection"},
        {ItemRoles::WrapAround, "wrap_around"},
        {ItemRoles::Minimum, "minimum"},
        {ItemRoles::Maximum, "maximum"},
    };
}

