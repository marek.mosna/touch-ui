/*
    Copyright 2020, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef ADMINITEMSTORE_H
#define ADMINITEMSTORE_H

#include <QObject>
#include "items.h"
#include "types.h"

class AdminItemStore : public QObject
{
    Q_OBJECT

    QVector<QPointer<Item> > m_items;

    bool insert(const AdminItemPointer & _item);
    /// Delete all m_items and clear the pointer array
    void clearItems();
public:
    explicit AdminItemStore(QObject *parent = nullptr);
    void refresh(const QVector<AdminItemPointer> & _data);
    int count() const {return m_items.count();}

    /// Always returns valid pointer. Pointer will become invalid on the next refresh.
    Item * get(int idx) const { Q_ASSERT(idx >= 0 && idx < m_items.count()); return m_items[idx];}

    //// Run execute on item at index idx, do nothing if invalid
    Q_INVOKABLE void execute(int idx);
public slots:
    void handlePropertiesChanged(const QVariantMap& changedProperties);
signals:
    void resetBegin();
    void resetEnd();
    void dataChanged(int idx, const QVector<int> roles);
};

#endif // ADMINITEMSTORE_H
