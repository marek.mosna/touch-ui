/*
    Copyright 2020, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "admin.h"

#include <QDBusConnection>
#include <QPointer>
#include <QVariant>

Admin::Admin(QObject *parent):
    QObject(parent),
    adminProxy("cz.prusa3d.sl1.admin0", "/cz/prusa3d/sl1/admin0", QDBusConnection::systemBus(), this),
    propertiesProxy("cz.prusa3d.sl1.admin0", "/cz/prusa3d/sl1/admin0", QDBusConnection::systemBus(), this),
    objectManagerProxy("cz.prusa3d.sl1.admin0", "/cz/prusa3d/sl1/admin0", QDBusConnection::systemBus(), this),
    m_store(new AdminItemStore(this)),
    m_model(new AdminModel(m_store, this))
{

    qRegisterMetaType<AdminItemPointer>("AdminItemPointer");
    qDBusRegisterMetaType<AdminItemPointer>();
    qRegisterMetaType<AdminChildren>("AdminChildren");
    qDBusRegisterMetaType<AdminChildren>();
    qRegisterMetaType<ManagedObjects>("ManagedObjects");
    qDBusRegisterMetaType<ManagedObjects>();


    connect(&propertiesProxy, &PropertiesProxy::PropertiesChanged, this, &Admin::proxyPropertiesChanged);
    connect(&adminProxy, &AdminProxy::enter_sysinfo, this, &Admin::enterSysinfo);
    connect(&adminProxy, &AdminProxy::enter_touchscreen_test, this, &Admin::enterTouchscreenTest);
    connect(&adminProxy, &AdminProxy::enter_fullscreen_image, this, &Admin::enterFullscreenImage);
    connect(&adminProxy, &AdminProxy::enter_tower_moves, this, &Admin::enterTowerMoves);
    connect(&adminProxy, &AdminProxy::enter_tilt_moves, this, &Admin::enterTiltMoves);

    connect(m_store, &AdminItemStore::resetBegin, m_model, &AdminModel::resetBegin);
    connect(m_store, &AdminItemStore::resetEnd, m_model, &AdminModel::resetEnd);
    connect(m_store, &AdminItemStore::dataChanged, m_model, &AdminModel::handleDataChanged);

}

void Admin::proxyPropertiesChanged(const QString &interface, const QMap<QString, QVariant> &changedProperties,
                                   const QStringList &invalidatesProperties) {
    Q_UNUSED(interface)
    Q_UNUSED(invalidatesProperties)

    if(changedProperties.contains("children")) {
        //refreshItems();
        if(m_store) m_store->refresh(adminProxy.children().toVector());
        emit itemsChanged(items);
        emit varItemsChanged(varItems());
        emit isActiveChanged(isActive());
    }
}

void Admin::enter() {
    adminProxy.enter();
}

bool Admin::isActive() const
{
    return ! items.empty();
}

QVariantList Admin::varItems() {
    QVariantList list;
    for(auto &item: items) {
        QVariant variant;
        variant.setValue(item);
        list.append(variant);
    }
    return list;
}

AdminItemStore *Admin::store() const
{
    return m_store;
}

AdminModel *Admin::model() const
{
    return m_model;
}
