/*
    Copyright 2020, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QDBusVariant>
#include <QDBusConnection>
#include <QDebug>

#include "items.h"

Item::Item(QObject *parent, QDBusObjectPath dbusPath):
    QObject(parent), propertiesProxy("cz.prusa3d.sl1.admin0", dbusPath.path(), QDBusConnection::systemBus(), this) {
}

void Item::proxyPropertiesChanged(const QString &interface, const QMap<QString, QVariant> &changedProperties,
                                  const QStringList &invalidatesProperties) {
    Q_UNUSED(interface)
    Q_UNUSED(invalidatesProperties)

    if(changedProperties.contains("name")) {
        emit nameChanged(qvariant_cast<QString>(changedProperties["name"]));
    }
    if(changedProperties.contains("enabled")) {
        emit enabledChanged(qvariant_cast<bool>(changedProperties["enabled"]));
    }
    emit propertiesChanged(changedProperties);
}


ActionItem::ActionItem(QObject *parent, QDBusObjectPath dbusPath):
    Item(parent, dbusPath),
    itemProxy("cz.prusa3d.sl1.admin0", dbusPath.path(), QDBusConnection::systemBus(), this) {
    connect(&propertiesProxy, &PropertiesProxy::PropertiesChanged, this, &ActionItem::proxyPropertiesChanged);
}

void ActionItem::execute() {
    itemProxy.execute();
}

QString ActionItem::name() const {
    return itemProxy.name();
}

QString ActionItem::icon() const {
    return itemProxy.icon();
}

bool ActionItem::enabled() const {
    return itemProxy.enabled();
}

QString IntValueItem::name() const {
    return itemProxy.name();
}

QString IntValueItem::icon() const {
    return itemProxy.icon();
}

bool IntValueItem::enabled() const {
    return itemProxy.enabled();
}

void IntValueItem::proxyPropertiesChanged(
        const QString &interface, const QMap<QString, QVariant> &changedProperties,
        const QStringList &invalidatesProperties)
{
    if(changedProperties.contains("value")) {
        emit valueChanged(qvariant_cast<int>(changedProperties["value"]));
    }
    Item::proxyPropertiesChanged(interface, changedProperties, invalidatesProperties);
}

IntValueItem::IntValueItem(QObject *parent, QDBusObjectPath dbusPath):
    Item(parent, dbusPath),
    itemProxy("cz.prusa3d.sl1.admin0", dbusPath.path(), QDBusConnection::systemBus(), this) {
    connect(&propertiesProxy, &PropertiesProxy::PropertiesChanged, this, &IntValueItem::proxyPropertiesChanged);
}

int IntValueItem::value() const {
    return itemProxy.value();
}

void IntValueItem::setValue(int value) {
    itemProxy.setValue(value);
}

int IntValueItem::step() const {
    return itemProxy.step();
}

int IntValueItem::minimum() const {
    return itemProxy.minimum();
}

int IntValueItem::maximum() const {
    return itemProxy.maximum();
}

QString FixedValueItem::name() const {
    return itemProxy.name();
}

QString FixedValueItem::icon() const {
    return itemProxy.icon();
}

bool FixedValueItem::enabled() const {
    return itemProxy.enabled();
}

void FixedValueItem::setValueVariant(QVariant _value)
{
    bool r;
    double val = _value.toDouble(&r);
    if(r) setValue(val);
    else qWarning() << __PRETTY_FUNCTION__ <<"invalid conversion:"  << _value;
}

void FixedValueItem::proxyPropertiesChanged(const QString &interface, const QMap<QString, QVariant> &changedProperties,
                                            const QStringList &invalidatesProperties) {
    Item::proxyPropertiesChanged(interface, changedProperties, invalidatesProperties);

    if(changedProperties.contains("value")) {
        emit valueChanged(qvariant_cast<int>(changedProperties["value"]));
    }
}

FixedValueItem::FixedValueItem(QObject *parent, QDBusObjectPath dbusPath):
    Item(parent, dbusPath),
    itemProxy("cz.prusa3d.sl1.admin0", dbusPath.path(), QDBusConnection::systemBus(), this) {
    connect(&propertiesProxy, &PropertiesProxy::PropertiesChanged, this, &FixedValueItem::proxyPropertiesChanged);
}

int FixedValueItem::value() const {
    return itemProxy.value();
}

void FixedValueItem::setValue(int value) {
    itemProxy.setValue(value);
}

int FixedValueItem::step() const {
    return itemProxy.step();
}

int FixedValueItem::fractions() const {
    return itemProxy.fractions();
}

int FixedValueItem::minimum() const {
    return itemProxy.minimum();
}

int FixedValueItem::maximum() const {
    return itemProxy.maximum();
}

void IntValueItem::setValueVariant(QVariant _value) {
    bool r;
    int val = _value.toInt(&r);
    if(r) setValue(val);
    else qWarning() << __PRETTY_FUNCTION__ <<"invalid conversion:"  << _value;
}


QString FloatValueItem::name() const {
    return itemProxy.name();
}

QString FloatValueItem::icon() const {
    return itemProxy.icon();
}

bool FloatValueItem::enabled() const {
    return itemProxy.enabled();
}

void FloatValueItem::proxyPropertiesChanged(
        const QString &interface, const QMap<QString, QVariant> &changedProperties,
        const QStringList &invalidatesProperties)
{

    if(changedProperties.contains("value")) {
        emit valueChanged(qvariant_cast<double>(changedProperties["value"]));
    }

    Item::proxyPropertiesChanged(interface, changedProperties, invalidatesProperties);
}

FloatValueItem::FloatValueItem(QObject *parent, QDBusObjectPath dbusPath):
    Item(parent, dbusPath),
    itemProxy("cz.prusa3d.sl1.admin0", dbusPath.path(), QDBusConnection::systemBus(), this) {
    connect(&propertiesProxy, &PropertiesProxy::PropertiesChanged, this, &FloatValueItem::proxyPropertiesChanged);
}

double FloatValueItem::value() const {
    return itemProxy.value();
}

void FloatValueItem::setValue(double value) {
    itemProxy.setValue(value);
}

double FloatValueItem::step() const {
    return itemProxy.step();
}

int FloatValueItem::minimum() const {
    return itemProxy.minimum();
}

int FloatValueItem::maximum() const {
    return itemProxy.maximum();
}

void FloatValueItem::setValueVariant(QVariant _value) {
    bool r;
    double val = _value.toDouble(&r);
    if(r) setValue(val);
    else qWarning() << __PRETTY_FUNCTION__ <<"invalid conversion:"  << _value;
}


QString BoolValueItem::name() const {
    return itemProxy.name();
}

QString BoolValueItem::icon() const {
    return itemProxy.icon();
}

bool BoolValueItem::enabled() const {
    return itemProxy.enabled();
}

void BoolValueItem::proxyPropertiesChanged(
        const QString &interface, const QMap<QString, QVariant> &changedProperties,
        const QStringList &invalidatesProperties)
{
    if(changedProperties.contains("value")) {
        emit valueChanged(qvariant_cast<bool>(changedProperties["value"]));
    }

    Item::proxyPropertiesChanged(interface, changedProperties, invalidatesProperties);
}

BoolValueItem::BoolValueItem(QObject *parent, QDBusObjectPath dbusPath):
    Item(parent, dbusPath),
    itemProxy("cz.prusa3d.sl1.admin0", dbusPath.path(), QDBusConnection::systemBus(), this) {
    connect(&propertiesProxy, &PropertiesProxy::PropertiesChanged, this, &BoolValueItem::proxyPropertiesChanged);
}

bool BoolValueItem::value() const {
    return itemProxy.value();
}

void BoolValueItem::setValue(bool value) {
    itemProxy.setValue(value);
}


QString TextValueItem::name() const {
    return itemProxy.name();
}

QString TextValueItem::icon() const {
    return itemProxy.icon();
}

bool TextValueItem::enabled() const {
    return itemProxy.enabled();
}

void TextValueItem::proxyPropertiesChanged(
        const QString &interface, const QMap<QString, QVariant> &changedProperties,
        const QStringList &invalidatesProperties)
{
    if(changedProperties.contains("value")) {
        emit valueChanged(qvariant_cast<QString>(changedProperties["value"]));
    }

    Item::proxyPropertiesChanged(interface, changedProperties, invalidatesProperties);
}

TextValueItem::TextValueItem(QObject *parent, QDBusObjectPath dbusPath):
    Item(parent, dbusPath),
    itemProxy("cz.prusa3d.sl1.admin0", dbusPath.path(), QDBusConnection::systemBus(), this) {
    connect(&propertiesProxy, &PropertiesProxy::PropertiesChanged, this, &TextValueItem::proxyPropertiesChanged);
}

QString TextValueItem::value() const {
    return itemProxy.value();
}

void TextValueItem::setValue(QString value) {
    itemProxy.setValue(value);
}

SelectionValueItem::SelectionValueItem(QObject *parent, QDBusObjectPath dbusPath):
    Item(parent, dbusPath),
    itemProxy("cz.prusa3d.sl1.admin0", dbusPath.path(), QDBusConnection::systemBus(), this)
{
    connect(&propertiesProxy, &PropertiesProxy::PropertiesChanged, this, &SelectionValueItem::proxyPropertiesChanged);
}


QString SelectionValueItem::name() const
{
    return itemProxy.name();
}

QString SelectionValueItem::icon() const
{
    return itemProxy.icon();
}

bool SelectionValueItem::enabled() const
{
    return itemProxy.enabled();
}

QStringList SelectionValueItem::selection() const
{
    return itemProxy.selection();
}

bool SelectionValueItem::wrap_around() const
{
    return itemProxy.wrap_around();
}

void SelectionValueItem::proxyPropertiesChanged(
        const QString &interface, const QMap<QString, QVariant> &changedProperties,
        const QStringList &invalidatesProperties)
{
    if(changedProperties.contains("value")) {
        //emit valueChanged(qvariant_cast<int>(changedProperties["value"]));
        emit valueChanged();
    }

    Item::proxyPropertiesChanged(interface, changedProperties, invalidatesProperties);
}

int SelectionValueItem::value() const
{
    return itemProxy.value();
}

void SelectionValueItem::setValue(int newValue)
{
    itemProxy.setValue(newValue);
}
