/*
    Copyright 2020-2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef ITEMS_H
#define ITEMS_H

#include <QObject>
#include <QString>
#include <QVariantMap>

#include "properties_proxy.h"
#include "admin_action_proxy.h"
#include "admin_int_value_proxy.h"
#include "admin_fixed_value_proxy.h"
#include "admin_float_value_proxy.h"
#include "admin_bool_value_proxy.h"
#include "admin_text_value_proxy.h"
#include "admin_selection_value_proxy.h"

/** Roles shared between item store and model */
enum ItemRoles {
    ItemType = Qt::UserRole + 1,
    Name,
    Icon,
    Enabled,
    Value,
    Step,
    Fractions,
    Selection,
    WrapAround,
    Minimum,
    Maximum,
};

class Item: public QObject {
    Q_OBJECT
public:
    explicit Item(QObject *parent = nullptr, QDBusObjectPath dbusPath = QDBusObjectPath());

    Q_PROPERTY(QString name READ name NOTIFY nameChanged)
    Q_PROPERTY(bool enabled READ enabled NOTIFY enabledChanged)
    virtual QString name() const = 0;
    virtual QString icon() const = 0;
    virtual bool enabled() const = 0;
    /// Universal setter
    /// If value cannot be converted to the correct type, do nothing.
    virtual void setValueVariant(QVariant) = 0;

    /// Universal getter
    virtual QVariant valueVariant() const = 0;

signals:
    void nameChanged(const QString items);
    void enabledChanged(const bool enabled);
    void propertiesChanged(const QVariantMap & changed_properties);

public slots:
    /** @brief Handle changes to Admin item properties */
    virtual void proxyPropertiesChanged(const QString &interface, const QVariantMap &changed_properties,
                                const QStringList &invalidates_properties);
protected:
    PropertiesProxy propertiesProxy;

};

class ActionItem: public Item {
    Q_OBJECT

public:
    explicit ActionItem(QObject *parent = nullptr, QDBusObjectPath dbusPath = QDBusObjectPath());
    QString name() const override;
    QString icon() const override;
    bool enabled() const override;
    Q_INVOKABLE void execute();
    virtual void setValueVariant(QVariant) override {}
    virtual QVariant valueVariant() const override {return QVariant();}

private:
    AdminActionProxy itemProxy;
};


class IntValueItem: public Item {
    Q_OBJECT
public:
    explicit IntValueItem(QObject *parent = nullptr, QDBusObjectPath dbusPath = QDBusObjectPath());

    Q_PROPERTY(int value READ value WRITE setValue NOTIFY valueChanged)
    Q_PROPERTY(int step READ step CONSTANT)
    Q_PROPERTY(int minimum READ minimum CONSTANT)
    Q_PROPERTY(int maximum READ maximum CONSTANT)
    QString name() const override;
    QString icon() const override;
    bool enabled() const override;

    int value() const;
    void setValue(int value);
    int step() const;
    int minimum() const;
    int maximum() const;

    virtual void setValueVariant(QVariant _value) override;
    virtual QVariant valueVariant() const override {return QVariant::fromValue(value());}

signals:
    void valueChanged(const int value);

public slots:
    void proxyPropertiesChanged(const QString &interface, const QVariantMap &changed_properties,
                                const QStringList &invalidates_properties) override;

private:
    AdminIntValueProxy itemProxy;

};

class FixedValueItem: public Item {
    Q_OBJECT
public:
    explicit FixedValueItem(QObject *parent = nullptr, QDBusObjectPath dbusPath = QDBusObjectPath());

    Q_PROPERTY(int value READ value WRITE setValue NOTIFY valueChanged)
    Q_PROPERTY(int step READ step CONSTANT)
    Q_PROPERTY(int fractions READ fractions CONSTANT)
    Q_PROPERTY(int minimum READ minimum CONSTANT)
    Q_PROPERTY(int maximum READ maximum CONSTANT)
    int value() const;
    void setValue(int value);
    int step() const;
    int fractions() const;
    int minimum() const;
    int maximum() const;
    QString name() const override;
    QString icon() const override;
    bool enabled() const override;

    virtual void setValueVariant(QVariant _value) override;
    virtual QVariant valueVariant() const override {return QVariant::fromValue(value());}
signals:
    void valueChanged(const int value);

public slots:
    void proxyPropertiesChanged(const QString &interface, const QVariantMap &changed_properties,
                                const QStringList &invalidates_properties) override;

private:
    AdminFixedValueProxy itemProxy;

};

class FloatValueItem: public Item {
    Q_OBJECT
public:
    explicit FloatValueItem(QObject *parent = nullptr, QDBusObjectPath dbusPath = QDBusObjectPath());

    Q_PROPERTY(double value READ value WRITE setValue NOTIFY valueChanged)
    Q_PROPERTY(double step READ step CONSTANT)
    Q_PROPERTY(int minimum READ minimum CONSTANT)
    Q_PROPERTY(int maximum READ maximum CONSTANT)

    QString name() const override;
    QString icon() const override;
    bool enabled() const override;

    double value() const;
    void setValue(double value);
    double step() const;
    int minimum() const;
    int maximum() const;

    virtual void setValueVariant(QVariant _value) override;
    virtual QVariant valueVariant() const override {return QVariant::fromValue(value());}
signals:
    void valueChanged(const double value);

public slots:
    void proxyPropertiesChanged(const QString &interface, const QVariantMap &changed_properties,
                                const QStringList &invalidates_properties) override;

private:
    AdminFloatValueProxy itemProxy;

};


class BoolValueItem: public Item {
    Q_OBJECT
public:
    explicit BoolValueItem(QObject *parent = nullptr, QDBusObjectPath dbusPath = QDBusObjectPath());

    Q_PROPERTY(bool value READ value WRITE setValue NOTIFY valueChanged)

    QString name() const override;
    QString icon() const override;
    bool enabled() const override;
    bool value() const;
    void setValue(bool value);

    virtual void setValueVariant(QVariant _value) override { setValue(_value.toBool()); }
    virtual QVariant valueVariant() const override {return QVariant::fromValue(value());}

signals:
    void valueChanged(const bool value);

public slots:
    void proxyPropertiesChanged(const QString &interface, const QVariantMap &changed_properties,
                                const QStringList &invalidates_properties) override;

private:
    AdminBoolValueProxy itemProxy;

};


class TextValueItem: public Item {
    Q_OBJECT
public:
    explicit TextValueItem(QObject *parent = nullptr, QDBusObjectPath dbusPath = QDBusObjectPath());

    Q_PROPERTY(QString value READ value WRITE setValue NOTIFY valueChanged)

    QString name() const override;
    QString icon() const override;
    bool enabled() const override;
    QString value() const;
    void setValue(QString value);

    virtual void setValueVariant(QVariant _value) override {setValue(_value.toString());}
    virtual QVariant valueVariant() const override {return QVariant::fromValue(value());}

signals:
    void valueChanged(const QString value);

public slots:
    void proxyPropertiesChanged(const QString &interface, const QVariantMap &changed_properties,
                                const QStringList &invalidates_properties) override;

private:
    AdminTextValueProxy itemProxy;

};


class SelectionValueItem : public Item {
    Q_OBJECT
public:
    explicit SelectionValueItem(QObject * parent = nullptr, QDBusObjectPath dbusPath = QDBusObjectPath());
    Q_PROPERTY(int value READ value WRITE setValue NOTIFY valueChanged)
    Q_PROPERTY(QStringList selection READ selection CONSTANT)
    Q_PROPERTY(bool wrap_around READ wrap_around CONSTANT)

    QString name() const override;
    QString icon() const override;
    bool enabled() const override;
    QStringList selection() const;
    bool wrap_around() const;


    int value() const;
    void setValue(int newValue);

    QVariant valueVariant() const override {return QVariant::fromValue(value());}
    void setValueVariant(QVariant newValue) override {setValue(newValue.toInt());}

public slots:
    void proxyPropertiesChanged(const QString &interface, const QVariantMap &changed_properties,
                                const QStringList &invalidates_properties) override;

signals:
    void valueChanged();

private:
    AdminSelectionValueProxy itemProxy;
};


#endif // ITEMS_H
