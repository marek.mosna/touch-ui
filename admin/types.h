/*
    Copyright 2020, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef ADMIN_TYPES_H
#define ADMIN_TYPES_H

#include <QList>
#include <QVariantMap>
#include <QVariantList>
#include <QDBusArgument>
#include <QDBusObjectPath>

typedef QList<QVariantMap> AdminItemsType;
typedef QVariantMap AdminValuesType;
typedef QMap<QDBusObjectPath,QMap<QString, QMap<QString, QVariant>>> ManagedObjects;

struct AdminItemPointer {
    QString interface;
    QDBusObjectPath path;
    AdminItemPointer(QString _interface = QStringLiteral(""), QDBusObjectPath _path = QDBusObjectPath("/")) : interface(_interface), path(_path) {}
};
Q_DECLARE_METATYPE(AdminItemPointer);
QDBusArgument &operator<<(QDBusArgument &argument, const AdminItemPointer &adminItemPointer);
const QDBusArgument &operator>>(const QDBusArgument &argument, AdminItemPointer &adminItemPointer);

typedef QList<AdminItemPointer> AdminChildren;

#endif // ADMIN_TYPES_H
