#!/usr/bin/python3
import os
import sys
from typing import NamedTuple
import argparse
import pydbus
from subprocess import Popen

class InterfaceData(NamedTuple):
    bus_name: str
    object_path: str
    file_path_source: str
    file_path_bus: str

def assert_interface(interfaceData: InterfaceData, touch_ui_path: str = "."):
    print(f"Checking interface: {interfaceData}")
    obj = pydbus.SystemBus().get(
        bus_name = interfaceData.bus_name,
        object_path = interfaceData.object_path
        )
    with open(interfaceData.file_path_bus, "wb") as file:
        file.write(obj.Introspect().encode("ascii"))
    p = Popen([os.path.join(os.path.dirname(os.path.abspath(__file__)), "diff_interfaces.py"), os.path.join(touch_ui_path, interfaceData.file_path_source), interfaceData.file_path_bus, interfaceData.bus_name])
    rcode = p.wait()
    return True if rcode == 0 else False

checklist = [
    # Hostname1 service on the test machine differs from the printer
    #InterfaceData("org.freedesktop.hostname1", "/org/freedesktop/hostname1", "./hostname/org.freedesktop.hostname1.xml", "/tmp/org.freedesktop.hostname1.xml"),
    InterfaceData("de.pengutronix.rauc", "/", "./sw_update/de.pengutronix.rauc.Installer.xml", "/tmp/de.pengutronix.rauc.Installer.xml"),
    # InterfaceData("cz.prusa3d.Updater1", "/cz/prusa3d/Updater1", "./sw_update/cz.prusa3d.Updater1.xml", "/tmp/cz.prusa3d.Updater1.xml"),
    InterfaceData("cz.prusa3d.sl1.filemanager0", "/cz/prusa3d/sl1/filemanager0", "./filemanager/cz.prusa3d.sl1.filemanager0.xml", "/tmp/cz.prusa3d.sl1.filemanager0.xml"),
    InterfaceData("cz.prusa3d.sl1.printer0", "/cz/prusa3d/sl1/printer0", "./printer_interfaces/cz.prusa3d.sl1.printer0.xml", "/tmp/cz.prusa3d.sl1.printer0.xml"),
    InterfaceData("cz.prusa3d.sl1.config0", "/cz/prusa3d/sl1/config0", "./printer_interfaces/cz.prusa3d.sl1.config0.xml", "/tmp/cz.prusa3d.sl1.config0.xml"),
    # InterfaceData("cz.prusa3d.connect0", "/cz/prusa3d/connect0", "./printer_interfaces/cz.prusa3d.connect0.xml", "/tmp/cz.prusa3d.connect0.xml"),
    # InterfaceData("cz.prusa3d.sl1.exposure0", "/cz/prusa3d/sl1/exposure0", "./printer_interfaces/cz.prusa3d.sl1.exposure0.xml", "/tmp/cz.prusa3d.sl1.exposure0.xml"),
    # InterfaceData("cz.prusa3d.sl1.examples0", "/cz/prusa3d/sl1/examples0", "./printer_interfaces/cz.prusa3d.sl1.examples0.xml", "/tmp/cz.prusa3d.sl1.examples0.xml"),
    InterfaceData("org.freedesktop.locale1", "/org/freedesktop/locale1", "./locale_setting/org.freedesktop.locale1.xml", "/tmp/org.freedesktop.locale1.xml"),
    # InterfaceData("cz.prusa3d.sl1.wizard0", "/cz/prusa3d/sl1/wizard0", "./wizard/cz.prusa3d.sl1.wizard.xml", "/tmp/cz.prusa3d.sl1.wizard.xml"),
    InterfaceData("cz.prusa3d.sl1.logs0", "/cz/prusa3d/sl1/logs0", "./logs/cz.prusa3d.sl1.logs0.xml", "/tmp/cz.prusa3d.sl1.logs0.xml")
]

additional_help = """
Output interpretation:
 + Supperfluous API, present in frontend, but is missing in the backend
 - Missing API, present in  backend, but missing in frontend
"""
if __name__ == "__main__":
    parser = argparse.ArgumentParser("assert_interfaces_equal", f"./assert_interfaces_equal.py --touch-ui-path <path>\n{additional_help}")
    parser.add_argument("--touch-ui-path", type=str, default=".", help="Path to the touch-ui source directory")
    parser.add_argument("--check-interface", nargs=4, metavar=("bus_name", "object_path", "file_path_source", "file_path_bus"), help="<bus_name> <object_name> <file_path_source> <file_path_bus>")
    args = parser.parse_args(sys.argv[1:])
    if args.check_interface:
        print(args.check_interface)
        exit(0 if assert_interface(InterfaceData(*args.check_interface), args.touch_ui_path) else 1)
    if all([assert_interface(interfaceData, args.touch_ui_path) for interfaceData in checklist]):
        print("[ OK ]")
        exit(0)
    else:
        print("[ FAILED ]")
        exit(1)
