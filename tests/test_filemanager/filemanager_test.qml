
import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtMultimedia 5.12
import cz.prusa3d.sl1.filemanager 1.0
ApplicationWindow {
    property bool rotate: false
    id: window
    visible: true

    width:  800
    height: 480

    Audio {
        source: "file:///usr/share/sla-multimedia/chromag_-_the_prophecy.xm"
        autoPlay: true
        onError: (error, errorString) => {console.log("Audio Error: ", errorString); stop()}
        onStatusChanged: () => console.log("Audio status changed: ", JSON.stringify(status))
    }

    FMContext {
        id: context
        Component.onCompleted: {
            console.log(list())
        }
        onCurrentChanged: console.log("signal currentChanged()", list())
        onCurrentPathChanged: console.log("signal currentPathChanged()")
        onIsAtRootChanged: console.log("signal isAtRootChanged()")

    }
    ListView {
        id: view
        anchors.fill: parent
        model: context.current.children
        delegate: Rectangle {
            color: index % 2 == 0 ? "grey" : "yellow"
            width: view.width
            height: 70
            property var filename: {
                var path = modelData.path.split("/")
                return path[path.length - 1]
            }
            property var type: {
                switch(modelData.type) {
                case FMFile.File: return "F"
                case FMFile.Directory: return "D"
                default: return "U"
                }
            }

            RowLayout {
                anchors.fill: parent
                Text {
                    width: 60
                    text: type
                }
                Text {
                    Layout.fillWidth: true
                    text: index === 0 ? ".." : filename
                }
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(modelData.type === FMFile.Directory) {
                        context.cd(index)
                    }
                    else {
                        context.get_metadata(modelData.path, true, function(metadata){
                            console.log("metadata for ", modelData.path, ": ", JSON.stringify(metadata))
                        }, function(err){console.log("Couldn't get metadata")})
                    }
                }
            }
        }

    }
}
