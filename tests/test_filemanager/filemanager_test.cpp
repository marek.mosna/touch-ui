/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QDebug>
#include <QTranslator>
#include <QFile>
#include <QProcessEnvironment>
#include <QtDBus>
#include <QtCore/QProcess>
#include <QtDBus/QtDBus>
#include <QtCore/QObject>
#include <QtDBus/QDBusInterface>
#include <QQmlContext>
#include <QObject>
#include <QtDBus/QDBusConnection>
#include <QQmlDebuggingEnabler>
#include "fmdata.h"
#include "dbusutils.h"
#include "printer_types.h"


inline QDBusArgument &operator<<(QDBusArgument &argument, const QMap<QString,QString> & m)
{
    argument.beginMap();
    for(const auto &key : m.keys()) {
        argument << key << m[key];
    }
    argument.endMap();
    return argument;
}
inline const QDBusArgument &operator>>(const QDBusArgument &argument, QMap<QString,QString> & m)
{
    argument.beginMap();
    QString key, val;
    while(!argument.atEnd()) {
        argument >> key;
        argument >> val;
        m[key] = val;
    }
    argument.endMap();
    return argument;
}
int main(int argc, char *argv[])
{
    QQmlDebuggingEnabler enabler;
    qputenv("QT_IM_MODULE", QByteArray("qtvirtualkeyboard"));

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);
    app.setOrganizationName("Prusa Research");
    app.setOrganizationDomain("prusa3d.cz");
    app.setApplicationName("Filemanager Test");

    registerPrinterTypes();

    QQmlApplicationEngine engine;
    engine.addImportPath(":/.");

    FMData::qmlRegister(engine);

    engine.load(QUrl(QStringLiteral("qrc:/filemanager_test.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    QTimer::singleShot(1000, &app, [&app](){app.quit(); });

    return app.exec();

}
