/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "licenserecord.h"

LicenseRecord::LicenseRecord()
{

}

LicenseRecord::LicenseRecord(const QString &_packageName,
                             const QString &_packageVersion,
                             const QString &_recipeName,
                             const QString &_license) :

    m_packageName(_packageName),
    m_packageVersion(_packageVersion),
    m_recipeName(_recipeName),
    m_license(_license)
{

}

QString LicenseRecord::packageName() const
{
    return m_packageName;
}

QString LicenseRecord::packageVersion() const
{
    return m_packageVersion;
}

QString LicenseRecord::recipeName() const
{
    return m_recipeName;
}

QString LicenseRecord::license() const
{
    return m_license;
}

void LicenseRecord::setPackageName(QString packageName)
{
    if (m_packageName == packageName)
        return;

    m_packageName = packageName;

}

void LicenseRecord::setPackageVersion(QString packageVersion)
{
    if (m_packageVersion == packageVersion)
        return;

    m_packageVersion = packageVersion;
}

void LicenseRecord::setRecipeName(QString recipeName)
{
    if (m_recipeName == recipeName)
        return;

    m_recipeName = recipeName;
}

void LicenseRecord::setLicense(QString license)
{
    if (m_license == license)
        return;

    m_license = license;
}
