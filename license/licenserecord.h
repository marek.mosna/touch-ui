/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef LICENSERECORD_H
#define LICENSERECORD_H

#include <QObject>

class LicenseRecord
{

    QString m_packageName;

    QString m_packageVersion;

    QString m_recipeName;

    QString m_license;

public:
    explicit LicenseRecord();
    LicenseRecord(const QString & _packageName,
                  const QString & _packageVersion,
                  const QString & _recipeName,
                  const QString & _license);

    QString packageName() const;

    QString packageVersion() const;

    QString recipeName() const;

    QString license() const;


    void setPackageName(QString packageName);
    void setPackageVersion(QString packageVersion);
    void setRecipeName(QString recipeName);
    void setLicense(QString license);
};

#endif // LICENSERECORD_H
