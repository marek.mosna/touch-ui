/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "licensemodel.h"

LicenseModel::LicenseModel(QObject *parent)
    : QAbstractListModel(parent), m_dataSource(nullptr)
{
    setDataSource(new LicenseDataSource(this));
}


int LicenseModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid() || ! m_dataSource)
        return 0;

    return m_dataSource->count();
}

QVariant LicenseModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || !m_dataSource || index.row() < 0 || index.row() >= m_dataSource->count())
        return QVariant();

    LicenseRecord * record = m_dataSource->licenses().at(index.row());
    switch(role) {
    case RoleLicense:
        return record->license();
    case RoleRecipeName:
        return record->recipeName();
    case RolePackageName:
        return record->packageName();
    case RolePackageVersion:
        return record->packageVersion();
    default:
        ;
    }
    return QVariant();
}

LicenseDataSource *LicenseModel::dataSource() const
{
    return m_dataSource;
}

QHash<int, QByteArray> LicenseModel::roleNames() const
{
    QHash<int, QByteArray> ret;
    ret[RolePackageName] = "packageName";
    ret[RolePackageVersion] = "packageVersion";
    ret[RoleRecipeName] = "recipeName";
    ret[RoleLicense] = "license";
    return ret;
}

QVariantList LicenseModel::findPackageName(QString _nameBeginning)
{
    QVariantList ret;
    if( ! dataSource()) return ret;
    for(int i = 0; i < dataSource()->count(); ++i) {
        LicenseRecord * rec = dataSource()->licenses().at(i);
        if(rec->recipeName().indexOf(_nameBeginning)  >= 0) {
            ret.push_back(i);
        }
    }
    return ret;
}

int LicenseModel::count() const { return dataSource()->count(); }

void LicenseModel::setDataSource(LicenseDataSource *dataSource)
{
    if (m_dataSource == dataSource)
        return;

    beginResetModel();

    // Disconnect signals from the old dataSource
    if(m_dataSource) {
        m_dataSource->disconnect(this);
    }

    m_dataSource = dataSource;

    // Connect signals from the new source
    connect(m_dataSource, &LicenseDataSource::preResetData, this, [=]() {
        this->beginResetModel();
    });
    connect(m_dataSource, &LicenseDataSource::postResetData, this, [=]() {
        this->endResetModel();
    });

    endResetModel();

    emit dataSourceChanged(m_dataSource);
}
