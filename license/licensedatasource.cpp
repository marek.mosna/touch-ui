/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "licensedatasource.h"
#include <QFile>
#include <QTextStream>
#include <QDebug>

LicenseDataSource::LicenseDataSource(QObject *parent) : QObject(parent)
{
    setManifestPath(QStringLiteral("/usr/share/common-licenses/license.manifest"));
}

QString LicenseDataSource::manifestPath() const
{
    return m_manifestPath;
}

QList<LicenseRecord *> LicenseDataSource::licenses() const
{
    return m_licenses;
}

int LicenseDataSource::count() const {
    return licenses().count();
}

void LicenseDataSource::setManifestPath(QString manifestPath)
{
    if (m_manifestPath == manifestPath)
        return;

    m_manifestPath = manifestPath;
    reloadData();
    emit manifestPathChanged(m_manifestPath);
}

void LicenseDataSource::setLicenses(QList<LicenseRecord *> licenses)
{
    if (m_licenses == licenses)
        return;

    m_licenses = licenses;
    emit licensesChanged(m_licenses);
}

void LicenseDataSource::reloadData()
{
    // Signal to the model that it is being reset
    emit preResetData();

    /// A convenient structure for temporary data during parsing
    struct data_t {
        QString packageName;
        QString packageVersion;
        QString recipeName;
        QString license;
        data_t() {clear();}
        void clear() {
            packageName = packageVersion = recipeName = license = QLatin1String("");
        }
        bool valid() const {
            return packageName != "" &&
                    packageVersion != "" &&
                    recipeName != "" &&
                    license != "";
        }
        bool isClean() const {
            return packageName == "" &&
                    packageVersion == "" &&
                    recipeName == "" &&
                    license == "";
        }

        Q_DECL_UNUSED
        QString strinfigy() const {

            return QString("[ ") + packageName + ", " +
                           packageVersion + ", " +
                           recipeName + ", " +
                           license + "]";
        }


    };


    /* Records in license.manifest file look like this:
        PACKAGE NAME: acl
        PACKAGE VERSION: 2.2.52
        RECIPE NAME: acl
        LICENSE: GPLv2+
     * At first, they will fill the data_t data structure
     * and when it becomes valid, new LicenseRecord will
     * be created.
     *
     * Unrecognized lines are passed over.
     */
    QFile manifestFile(manifestPath());
    manifestFile.open(QFile::ReadOnly | QFile::Text);
    QTextStream in(&manifestFile);
    data_t data;
    while (!in.atEnd()) {
        QString line = in.readLine();
        if(line != "") {
            // Parse line
            QStringList sp = line.split(": ");
            if(sp[0] == "PACKAGE NAME") {
                if( ! data.isClean()) {
                    qWarning() << "parse error(before line " << line << ")";
                    return;
                }
                data.packageName = sp[1];
            }
            else if(sp[0] == "PACKAGE VERSION") {
                data.packageVersion = sp[1];
            }
            else if(sp[0] == "RECIPE NAME") {
                data.recipeName = sp[1];
            }
            else if(sp[0] == "LICENSE") {
                data.license = sp[1];
                if(data.valid()) {
                    m_licenses.append(new LicenseRecord(data.packageName,
                                                        data.packageVersion,
                                                        data.recipeName,
                                                        data.license));
                    //qDebug() << data.strinfigy();
                    data.clear();
                }
            }
        }

    }

    // Add attribution to software that does not have its own package
    m_licenses.append(new LicenseRecord("qqr.js",
                                        "1.0",
                                        "https://github.com/M4rtinK/qqr.js",
                                        "GPLv3 License"));
    m_licenses.append(new LicenseRecord("semver",
                                        "0.3.0",
                                        "https://github.com/Neargye/semver",
                                        "MIT License"));
    m_licenses.append(new LicenseRecord("maxLibQt",
                                        "d1a8dfa85d0",
                                        "https://github.com/mpaperno/maxLibQt/blob/master/src/quick/maxLibQt/controls/MLDoubleSpinBox.qml",
                                        "GPLv3 License"));

    // Signal to the model that reseting has finished
    emit postResetData();
}
